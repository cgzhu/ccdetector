// FuncBndBuild.cpp: implementation of the FuncBndBuild class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FuncBndBuild.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FuncBndBuild::FuncBndBuild()
{

}

FuncBndBuild::~FuncBndBuild()
{

}

void FuncBndBuild::operator ()(CArray<TNODE,TNODE> &TArry)
{
	CArray<int,int> readyTknIdxArry;
	long i;
	int funcBegLine = 0, funcEndLine = 0,
		funcBgnRealLine = 0, funcEndRealLine = 0;
	for( i = 0; i < TArry.GetSize(); i++ )
	{
		FindFuncRang(TArry,readyTknIdxArry,funcBegLine,funcEndLine,funcBgnRealLine,funcEndRealLine,i);
		funcBegLine = 0;
		funcEndLine = 0;
		funcBgnRealLine = 0;
		funcEndRealLine = 0;
		readyTknIdxArry.RemoveAll();
	}
}
void FuncBndBuild::FindFuncRang(CArray<TNODE,TNODE>& TArry, CArray<int,int> &readyTknIdxArry,
							   int& funcBegLine, int& funcEndLine, 
							   int& funcBgnRealLine,int& funcEndRealLine,
							   long& idx)
{
	funcBegLine = TArry[idx].line;
	funcBgnRealLine = TArry[idx].srcLine;
	int flag = 0;
	int flwMatch = -1;
	int wellFlag = 0;
	int circleFlag = 0;

	for( ;idx < TArry.GetSize(); idx++ )
	{
		readyTknIdxArry.Add(idx);
		CString srcWord = TArry[idx].srcWord;
		int key = TArry[idx].key;

		if( key == CN_WELL )
		{//遇到'#'
			wellFlag = 1;
		}

		if( flwMatch == -1 )//函数外部语句
		{
			if( idx >= TArry.GetSize() -1 )
			{
				funcBegLine = 0;
				funcEndLine = 0;
				funcBgnRealLine = funcEndRealLine = 0;
				ChangeFuncRang(TArry,readyTknIdxArry,funcBegLine,funcEndLine,funcBgnRealLine,funcEndRealLine);
				return;
			}
			else if( (key == CN_LINE || wellFlag == 1) && TArry[idx+1].line != TArry[idx].line )
			{
				funcBegLine = 0;
				funcEndLine = 0;
				funcBgnRealLine = funcEndRealLine = TArry[idx].line;
				ChangeFuncRang(TArry,readyTknIdxArry,funcBegLine,funcEndLine,funcBgnRealLine,funcEndRealLine);
				return;
			}
		}//////////////////////////////////

		if( key== CN_DFUNCTION )//|| key== CN_MAIN ) 
		{
			funcBegLine = TArry[idx].line;
			funcBgnRealLine = TArry[idx].srcLine;
			int wellElseFlag = 0;
			while( flwMatch != 0 )
			{
				idx++;
				if( idx < TArry.GetSize() )
				{
					readyTknIdxArry.Add(idx);
					srcWord = TArry[idx].srcWord;
					key = TArry[idx].key;
					int line = TArry[idx].srcLine;
					/**wq-20090712**识别以下情况多个{和一个}匹配的情况***
					 **  if (finfo.filetype != APR_REG &&
					 **  #if defined(WIN32) || defined(OS2) || defined(NETWARE)
					 **      strcasecmp(apr_filepath_name_get(name), "nul") != 0) {
					 **  #else
					 **      strcmp(name, "/dev/null") != 0) {
					 **  #endif 
					 **  ap_log_error(APLOG_MARK, APLOG_ERR, 0, NULL,
					 **               "Access to file %s denied by server: not a regular file",
					 **               name);
					 **  apr_file_close(file);
					 **  return APR_EBADF;
					 **  }
	                 ***************************************************************/
					if( key == CN_WELL )
					{
						idx++;
						if( idx<TArry.GetSize() )
						{
							readyTknIdxArry.Add(idx);
							srcWord = TArry[idx].srcWord;
							key = TArry[idx].key;
							if( srcWord == "else" )
							{
								wellElseFlag = 1;
							}
							else if( srcWord == "endif" && wellElseFlag == 1 )
							{
								wellElseFlag = 0;
							}
						}							
					}//wq-20090712-end
					else if( key == CN_LCIRCLE )
					{
						circleFlag++;
					}
					else if( key == CN_RCIRCLE )
					{
						circleFlag--;
					}
					if( flwMatch == -1 && circleFlag == 0 && (TArry[idx].key == CN_LINE || TArry[idx].key == CN_DOU) )
					{
						break;
					}
					if( key == CN_LFLOWER && wellElseFlag == 0)
					{

/*						if( idx > 0 && TArry[idx-1].key == CN_EQUAL 
							|| flag != 0 )
						{//形如 " ={ "
							flag++;
						}*/

						if( flwMatch == -1 )
						{//第一个"{"
							flwMatch = 1;
						}
						else
						{
							flwMatch++;
						}
					}
					else if( key == CN_RFLOWER)
					{
						flwMatch--;
						if( flwMatch == 0 )
						{
/*							if( flag != 0 )
							{//形如 " ={ }; " 的语句;
								flag--;
							}
							if( flag != 2 )*/
							{
								funcEndLine = TArry[idx].line;
								funcEndRealLine = TArry[idx].srcLine;
								ChangeFuncRang(TArry,readyTknIdxArry,funcBegLine,funcEndLine,funcBgnRealLine,funcEndRealLine);
								BuildContxtInfo(TArry,readyTknIdxArry,funcBegLine,funcEndLine);
								return;
							}
						}
					}//end-else-if
				}//end-if
				else break;
			}//end-while
		}//end-if
	}//end-for
	return;
}

void FuncBndBuild::ChangeFuncRang( CArray<TNODE,TNODE>& TArry, CArray<int,int> &readyTknIdxArry, 
					 int& funcBegLine, int& funcEndLine,int& funcBgnRealLine,int& funcEndRealLine)
{
	int i;
	for( i = 0; i < readyTknIdxArry.GetSize(); i++ )
	{
		int idx = readyTknIdxArry[i];
		if(TArry[idx].line < funcBegLine || TArry[idx].line > funcEndLine)
		{
			TArry[idx].funcBegLine = 0;
			TArry[idx].funcEndLine = 0;
			TArry[idx].funcBgnRealLine = TArry[idx].funcEndRealLine = TArry[idx].srcLine;
		}
		else
		{
			TArry[idx].funcBegLine = funcBegLine;
			TArry[idx].funcEndLine = funcEndLine;
			TArry[idx].funcBgnRealLine = funcBgnRealLine;
			TArry[idx].funcEndRealLine = funcEndRealLine;
		}
	}
}

void FuncBndBuild::BuildContxtInfo( CArray<TNODE,TNODE>& TArry,CArray<int,int> &readyTknIdxArry,int funcBegLine, int funcEndLine )
{
	CArray<int,int> cntxtIdxArry;
	cntxtIdxArry.RemoveAll();
	int i;
	int flwFlag = -1;
	for( i = 0; i < readyTknIdxArry.GetSize(); i++ )
	{
		int idx = readyTknIdxArry[i];
		if(TArry[idx].line >= funcBegLine && TArry[idx].line <= funcEndLine)
		{
			if( flwFlag == -1 && cntxtIdxArry.GetSize() == 0 )
			{
				cntxtIdxArry.Add(idx);
				continue;
			}
			if( TArry[idx].line == funcBegLine )
			{
				TArry[idx].cntxtLine = -1;
				continue;
			}
			int doWhSrch = 0;
			int cntxtLine = TArry[cntxtIdxArry[cntxtIdxArry.GetSize()-1]].line;
			int key = TArry[idx].key;
			int size;
			int flw = 0;
			switch(TArry[idx].key)
			{
				case CN_FOR:
				case CN_IF:
				case CN_ELSE://
				case CN_SWITCH:
					TArry[idx].cntxtLine = cntxtLine;
					cntxtIdxArry.Add(idx);
					break;
				case CN_WHILE:
					if( TArry[idx].cntxtLine == -1 && TArry[idx].dowhileLine == -1 )
					{
						TArry[idx].cntxtLine = cntxtLine;
						cntxtIdxArry.Add(idx);
					}
					break;
				case CN_DO://???记录配对while的token序号?
					TArry[idx].cntxtLine = cntxtLine;
					doWhSrch = idx + 2;
					flw = 1;
					size = readyTknIdxArry.GetSize();
					size = TArry.GetSize();
					while( doWhSrch < readyTknIdxArry[readyTknIdxArry.GetSize()-1] )
					{
						if( TArry[doWhSrch].key == CN_LFLOWER )
						{
							flw++;
						}
						else if( TArry[doWhSrch].key == CN_RFLOWER )
						{
							flw--;
						}
						else if( flw == 0 && TArry[doWhSrch].key == CN_WHILE )
						{
							TArry[doWhSrch].cntxtLine = cntxtLine;
							size = TArry[idx].line;
							TArry[doWhSrch].dowhileLine = TArry[idx].line;
							size = TArry[doWhSrch].line;
							TArry[idx].dowhileLine = TArry[doWhSrch].line;
							break;
						}
						doWhSrch++;
					}
					cntxtIdxArry.Add(idx);
					break;
				case CN_RFLOWER:
					TArry[idx].cntxtLine = cntxtLine;
					cntxtIdxArry.RemoveAt(cntxtIdxArry.GetSize()-1);
					break;
				default:
					TArry[idx].cntxtLine = cntxtLine;
					break;
			}
		}
	}
}
