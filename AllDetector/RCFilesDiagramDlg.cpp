// RCFilesDiagramDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "RCFilesDiagramDlg.h"
#include "afxdialogex.h"
#include "CSeries.h"

// CRCFilesDiagramDlg 对话框

IMPLEMENT_DYNAMIC(CRCFilesDiagramDlg, CDialogEx)

CRCFilesDiagramDlg::CRCFilesDiagramDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRCFilesDiagramDlg::IDD, pParent)
	, m_rc_files_chart()
{

}

CRCFilesDiagramDlg::~CRCFilesDiagramDlg()
{
}

void CRCFilesDiagramDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TCHART1, m_rc_files_chart);
}

//清除所有图线
void CRCFilesDiagramDlg::ClearAllSeries() 
 {
     for(long i = 0;i<m_rc_files_chart.get_SeriesCount();i++)
     {
         ((CSeries)m_rc_files_chart.Series(i)).Clear();
     }
 }

BEGIN_MESSAGE_MAP(CRCFilesDiagramDlg, CDialogEx)
END_MESSAGE_MAP()


// CRCFilesDiagramDlg 消息处理程序
