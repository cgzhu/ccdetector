// CCSegFileDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "CCSegFileDlg.h"
#include "afxdialogex.h"
#include "CSeries.h"

// CCCSegFileDlg 对话框

IMPLEMENT_DYNAMIC(CCCSegFileDlg, CDialogEx)

CCCSegFileDlg::CCCSegFileDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCCSegFileDlg::IDD, pParent)
	, cc_seg_file_column_chart()
{

}

CCCSegFileDlg::~CCCSegFileDlg()
{
}

void CCCSegFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TCHART1, cc_seg_file_column_chart);
}

//清除所有图线
void CCCSegFileDlg::ClearAllSeries()
 {
     for(long i = 0;i<cc_seg_file_column_chart.get_SeriesCount();i++)
     {
         ((CSeries)cc_seg_file_column_chart.Series(i)).Clear();
     }
 }

BEGIN_MESSAGE_MAP(CCCSegFileDlg, CDialogEx)
END_MESSAGE_MAP()


// CCCSegFileDlg 消息处理程序
