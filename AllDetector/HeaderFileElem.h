// HeaderFileElem.h: interface for the HeaderFileElem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEADERFILEELEM_H__60C87430_C9AE_4C97_85EA_2B3EA5D89627__INCLUDED_)
#define AFX_HEADERFILEELEM_H__60C87430_C9AE_4C97_85EA_2B3EA5D89627__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "TNODE.h"
#include "SNODE.h"
#include "FNODE.h"
#include "TypedefRecordElem.h"

struct MACRO_REPALCE_NODE
{
	CString macroName;
	CString macroValue;
};
class HeaderFileElem  
{
public:
	HeaderFileElem();
	virtual ~HeaderFileElem();
public:
	CString hFileName;//字符串
	CString hFileSrcCode;
	CArray<MACRO_REPALCE_NODE,MACRO_REPALCE_NODE> macroReplaceArry;//数组
	CArray<TNODE,TNODE> hFileTknArry;//数组
	CArray<SNODE,SNODE> symbolTable;//数组
	CArray<FNODE,FNODE> funcTable;
	CArray<TypedefRecordElem,TypedefRecordElem> typedefRcdArry;
//	CString strMacroLines;
//	CString strTypedefLines;
public:
	void AddMacroReplaceArry(CString &mcName,CString &mcValue);
/*	bool ISHAVE_WELL(const CString &cprogram, const int &position,const int length);
	CString Deal_SomeLine(int &position, int &pos_inline, CString &program, bool del_flag);
	CString FindDefineLine(CString &program,int start);
	bool BuildStrMacroLines( CString &hProgram );*
	bool SetHFileTknArry(CArray<TNODE,TNODE> &hFlTknArry);
	bool SetTypedefTknArry(CArray<TNODE,TNODE> &tpdfTknArry);
	bool SetStrTypedefLines(CString &typedefLines);
	bool SetStrMacroLines(CString &macroLines);
	bool SetHFileName(CString &fileName);*/
	void Clean();
	HeaderFileElem(const HeaderFileElem &hFileElem);
	HeaderFileElem operator=(const HeaderFileElem &hFileElem);

};

#endif // !defined(AFX_HEADERFILEELEM_H__60C87430_C9AE_4C97_85EA_2B3EA5D89627__INCLUDED_)
