// SciEditView.cpp : implementation file
//

#include "stdafx.h"
#include "SciEditView.h"
#include "ScintillaWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSciEditView

IMPLEMENT_DYNCREATE(CSciEditView, CView)

CSciEditView::CSciEditView()
{
}

CSciEditView::~CSciEditView()
{
}


BEGIN_MESSAGE_MAP(CSciEditView, CView)
	//{{AFX_MSG_MAP(CSciEditView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSciEditView drawing

void CSciEditView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CSciEditView diagnostics

#ifdef _DEBUG
void CSciEditView::AssertValid() const
{
	CView::AssertValid();
}

void CSciEditView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

void CSciEditView::UpdateLineNumberWidth()
{
    TCHAR tchLines[32];
    int  iLineMarginWidthNow;
    int  iLineMarginWidthFit;
    wsprintf(tchLines,_T(" %i "),
		m_ScintillaWnd.SendMessage(SCI_GETLINECOUNT,0,0));
    iLineMarginWidthNow = m_ScintillaWnd.SendMessage(
		SCI_GETMARGINWIDTHN,0,0);
    iLineMarginWidthFit = m_ScintillaWnd.SendMessage(
		SCI_TEXTWIDTH,STYLE_LINENUMBER,(LPARAM)tchLines);
	
    if (iLineMarginWidthNow != iLineMarginWidthFit)
    {
		m_ScintillaWnd.SendMessage(SCI_SETMARGINWIDTHN,0,
			iLineMarginWidthFit);
    }
}

/////////////////////////////////////////////////////////////////////////////
// CSciEditView message handlers

int CSciEditView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	m_ScintillaWnd.Create(   
        WS_EX_CLIENTEDGE,   
        WS_CHILD | WS_VISIBLE,   
        CRect(0,0,lpCreateStruct->cx,lpCreateStruct->cy),   
        this,10000);		

	return 0;
}

void CSciEditView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_ScintillaWnd.MoveWindow(0,0,cx,cy);
}

BOOL CSciEditView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class
	LPNMHDR pnmh = (LPNMHDR)lParam;   
	struct SCNotification* scn = (struct SCNotification*)lParam;   
	
    switch(pnmh->code)   
    {   
	case SCN_MODIFIED:
		//This notification is sent when the text or    
		//styling of the document changes or is about    
		//to change   
	case SCN_ZOOM:   
		//This notification is generated when the user   
		//zooms the display using the keyboard or the   
		//SCI_SETZOOM method is called.   
		UpdateLineNumberWidth();   
		break;   
	case SCN_MARGINCLICK:
		{int line=m_ScintillaWnd.SendMessage(SCI_LINEFROMPOSITION, scn->position);
		m_ScintillaWnd.SendMessage(SCI_TOGGLEFOLD, line);
		}
		break;
	case SCN_UPDATEUI:
		m_ScintillaWnd.UpdateUI();
		break;
    }
	return CView::OnNotify(wParam, lParam, pResult);
}


void CSciEditView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	UpdateLineNumberWidth();
	
	// init
	const TCHAR* g_szKeywords=  _T("ATOM BOOL BOOLEAN BYTE CHAR COLORREF DWORD DWORDLONG DWORD_PTR DWORD32 DWORD64 FLOAT HACCEL HALF_PTR HANDLE HBITMAP HBRUSH HCOLORSPACE HCONV HCONVLIST HCURSOR HDC HDDEDATA HDESK HDROP HDWP HENHMETAFILE HFILE HFONT HGDIOBJ HGLOBAL HHOOK HICON HINSTANCE HKEY HKL HLOCAL HMENU HMETAFILE HMODULE HMONITOR HPALETTE HPEN HRESULT HRGN HRSRC HSZ HWINSTA HWND INT INT_PTR INT32 INT64 LANGID LCID LCTYPE LGRPID LONG LONGLONG LONG_PTR LONG32 LONG64 LPARAM LPBOOL LPBYTE LPCOLORREF LPCSTR LPCTSTR LPCVOID LPCWSTR LPDWORD LPHANDLE LPINT LPLONG LPSTR LPTSTR LPVOID LPWORD LPWSTR LRESULT PBOOL PBOOLEAN PBYTE PCHAR PCSTR PCTSTR PCWSTR PDWORDLONG PDWORD_PTR PDWORD32 PDWORD64 PFLOAT PHALF_PTR PHANDLE PHKEY PINT PINT_PTR PINT32 PINT64 PLCID PLONG PLONGLONG PLONG_PTR PLONG32 PLONG64 POINTER_32 POINTER_64 PSHORT PSIZE_T PSSIZE_T PSTR PTBYTE PTCHAR PTSTR PUCHAR PUHALF_PTR PUINT PUINT_PTR PUINT32 PUINT64 PULONG PULONGLONG PULONG_PTR PULONG32 PULONG64 PUSHORT PVOID PWCHAR PWORD PWSTR SC_HANDLE SC_LOCK SERVICE_STATUS_HANDLE SHORT SIZE_T SSIZE_T TBYTE TCHAR UCHAR UHALF_PTR UINT UINT_PTR UINT32 UINT64 ULONG ULONGLONG ULONG_PTR ULONG32 ULONG64 USHORT USN VOID WCHAR WORD WPARAM char bool short int __int32 __int64 __int8 __int16 long float double __wchar_t clock_t _complex _dev_t _diskfree_t div_t ldiv_t _exception _EXCEPTION_POINTERS FILE _finddata_t _finddatai64_t _wfinddata_t _wfinddatai64_t __finddata64_t __wfinddata64_t _FPIEEE_RECORD fpos_t _HEAPINFO _HFILE lconv intptr_t jmp_buf mbstate_t _off_t _onexit_t _PNH ptrdiff_t _purecall_handler sig_atomic_t size_t _stat __stat64 _stati64 terminate_function time_t __time64_t _timeb __timeb64 tm uintptr_t _utimbuf va_list wchar_t wctrans_t wctype_t wint_t signed unsigned break case catch class const __finally __exception __try const_cast continue private public protected __declspec default delete deprecated dllexport dllimport do dynamic_cast else enum explicit extern if for friend goto inline mutable naked namespace new noinline noreturn nothrow register reinterpret_cast return selectany sizeof static static_cast struct switch template this thread throw true false try typedef typeid typename union using uuid virtual void volatile whcar_t while assert isalnum isalpha iscntrl isdigit isgraph islower isprint ispunct isspace isupper isxdigit tolower toupper errno localeconv setlocale acos asin atan atan2 ceil cos cosh exp fabs floor fmod frexp ldexp log log10 modf pow sin sinh sqrt tan tanh jmp_buf longjmp setjmp raise signal sig_atomic_t va_arg va_end va_start clearerr fclose feof ferror fflush fgetc fgetpos fgets fopen fprintf fputc fputs fread freopen fscanf fseek fsetpos ftell fwrite getc getchar gets perror printf putc putchar puts remove rename rewind scanf setbuf setvbuf sprintf sscanf tmpfile tmpnam ungetc vfprintf vprintf vsprintf abort abs atexit atof atoi atol bsearch calloc div exit free getenv labs ldiv malloc mblen mbstowcs mbtowc qsort rand realloc srand strtod strtol strtoul system wcstombs wctomb memchr memcmp memcpy memmove memset strcat strchr strcmp strcoll strcpy strcspn strerror strlen strncat strncmp strncpy strpbrk strrchr strspn strstr strtok strxfrm asctime clock ctime difftime gmtime localtime mktime strftime time");

	//m_ScintillaWnd.SendMessage(SCI_STYLESETFONT, STYLE_DEFAULT,(sptr_t)"Courier New");
	m_ScintillaWnd.SendMessage(SCI_CLEARDOCUMENTSTYLE);
    m_ScintillaWnd.SendMessage(SCI_STYLESETSIZE, STYLE_DEFAULT,10);

	m_ScintillaWnd.SendMessage(SCI_SETMARGINWIDTHN, 0,20);//    'display Line numbers
	m_ScintillaWnd.SendMessage(SCI_SETMARGINWIDTHN, 1, 20);//    'defaults to non-folding symbols
	m_ScintillaWnd.SendMessage(SCI_SETMARGINWIDTHN, 2, 20);//    'no default
	m_ScintillaWnd.SendMessage(SCI_SETMARGINMASKN,  2, SC_MASK_FOLDERS);
   m_ScintillaWnd.SendMessage(SCI_SETMARGINSENSITIVEN,  2, 1); 

	   //markers/symbols
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDEROPEN, SC_MARK_BOXMINUS);
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDER, SC_MARK_BOXPLUS);
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDERSUB, SC_MARK_VLINE);
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDERTAIL, SC_MARK_LCORNER);
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDEREND, SC_MARK_BOXPLUSCONNECTED);
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDEROPENMID, SC_MARK_BOXMINUSCONNECTED);
	m_ScintillaWnd.SendMessage(SCI_MARKERDEFINE, SC_MARKNUM_FOLDERMIDTAIL, SC_MARK_TCORNER);
	
	//colors
	m_ScintillaWnd.SendMessage(SCI_MARKERSETFORE, SC_MARKNUM_FOLDER, 0xffffff);
	m_ScintillaWnd.SendMessage(SCI_MARKERSETBACK, SC_MARKNUM_FOLDER, 0);
	
	m_ScintillaWnd.SendMessage(SCI_MARKERSETFORE, SC_MARKNUM_FOLDEROPEN, 0xffffff);	
	m_ScintillaWnd.SendMessage(SCI_MARKERSETBACK, SC_MARKNUM_FOLDEROPEN, RGB(0, 0, 0xFF));
	
	m_ScintillaWnd.SendMessage(SCI_MARKERSETFORE, SC_MARKNUM_FOLDEROPENMID, 0xffffff);	
	m_ScintillaWnd.SendMessage(SCI_MARKERSETBACK, SC_MARKNUM_FOLDEROPENMID, 0);
	
	m_ScintillaWnd.SendMessage(SCI_MARKERSETFORE, SC_MARKNUM_FOLDERSUB, 0xffffff);
	m_ScintillaWnd.SendMessage(SCI_MARKERSETBACK, SC_MARKNUM_FOLDERSUB, 0);
	
	m_ScintillaWnd.SendMessage(SCI_MARKERSETFORE, SC_MARKNUM_FOLDERTAIL, 0xffffff);
	m_ScintillaWnd.SendMessage(SCI_MARKERSETBACK, SC_MARKNUM_FOLDERTAIL, 0);

	m_ScintillaWnd.SendMessage(SCI_MARKERSETFORE, SC_MARKNUM_FOLDERMIDTAIL, 0xffffff);
	m_ScintillaWnd.SendMessage(SCI_MARKERSETBACK, SC_MARKNUM_FOLDERMIDTAIL, 0);

	m_ScintillaWnd.SendMessage(SCI_SETLEXER, SCLEX_CPP); //C++语法解析
	m_ScintillaWnd.SendMessage(SCI_SETKEYWORDS, 0, (sptr_t)g_szKeywords);//设置关键字
	// 下面设置各种语法元素前景色
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_DEFAULT, RGB(0, 0, 0));

	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_WORD, RGB(0, 0, 0x80));   //关键字
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_IDENTIFIER, RGB(0, 0, 0));   //关键字
	m_ScintillaWnd.SendMessage(SCI_STYLESETBOLD, SCE_C_WORD, 1);   //关键字	

	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_STRING, RGB(0x80, 0, 0x80));
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_OPERATOR,RGB(0x80, 0x80, 0));
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_NUMBER,RGB(0x80, 0x80, 0));
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_CHARACTER, 0x001515A3); //字符
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_PREPROCESSOR,  RGB(0x80, 0, 0));//预编译开关
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_COMMENT, RGB(0, 0x80, 0));//块注释
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_COMMENTLINE, RGB(0, 0x80, 0));//行注释
	m_ScintillaWnd.SendMessage(SCI_STYLESETFORE, SCE_C_COMMENTDOC, RGB(0, 0x80, 0));//文档注释（/**开头）	

	m_ScintillaWnd.SendMessage(SCI_SETCARETLINEVISIBLE, TRUE);
    m_ScintillaWnd.SendMessage(SCI_SETCARETLINEBACK, 0xb0ffff); 

	m_ScintillaWnd.SendMessage(SCI_SETPROPERTY, (WPARAM)_T("fold"), (LPARAM)_T("1"));
	m_ScintillaWnd.SendMessage(SCI_SETPROPERTY, (WPARAM)_T("fold.symbols"), (LPARAM)_T("1"));
	m_ScintillaWnd.SendMessage(SCI_SETPROPERTY, (WPARAM)_T("fold.comment"), (LPARAM)_T("1"));
	m_ScintillaWnd.SendMessage(SCI_SETPROPERTY, (WPARAM)_T("fold.flags"), (LPARAM)_T("16"));
	m_ScintillaWnd.SendMessage(SCI_SETPROPERTY, (WPARAM)_T("fold.preprocessor"), (LPARAM)_T("1"));

	//m_ScintillaWnd.SendMessage(SCI_SETMARGINWIDTHN, 2, 16);

	m_ScintillaWnd.SendMessage(SCI_SETINDENTATIONGUIDES, TRUE, 0);
	m_ScintillaWnd.SendMessage(SCI_SETTABWIDTH,4);
	// set indention to 3
	m_ScintillaWnd.SendMessage(SCI_SETINDENT,4);

	// set the backgroundcolor of brace highlights
   m_ScintillaWnd.SendMessage(SCI_STYLESETBACK, STYLE_BRACELIGHT, RGB(0,255,0));

   // set end of line mode to CRLF
   m_ScintillaWnd.SendMessage(SCI_CONVERTEOLS, 2, 0);
   m_ScintillaWnd.SendMessage(SCI_SETEOLMODE, 2, 0);


   m_ScintillaWnd.SendMessage(SCI_ANNOTATIONSETSTYLE,STYLE_DEFAULT);
   m_ScintillaWnd.SendMessage(SCI_ANNOTATIONSETVISIBLE,ANNOTATION_BOXED);


   //加载文件?
   CString s = GetDocument()->GetPathName();
   if (s.GetLength()>0){
	   m_ScintillaWnd.Load(s);
   }
}
