// TNODE.h: interface for the TNODE class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TNODE_H__AE8BB308_C8B4_4F1C_963E_B29F4CECAFD6__INCLUDED_)
#define AFX_TNODE_H__AE8BB308_C8B4_4F1C_963E_B29F4CECAFD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class TNODE  
{
public:
	TNODE();
	virtual ~TNODE();
	TNODE(const TNODE& TN);
	TNODE operator=(const TNODE& TN);

public:
	int     key;       //  code which defined in "constdata.h"  
    int     addr;      //  address in symbol table if it is variable of function
	int     paddr;     //  pointer address
	int     deref;//wtt//2006.3.21//指针脱引用*级别

	int     line;      //  text line the token word in    
	double  value;     //  value of const data 
	CString name;      //  name of token word if it is string
	int     srcLine;  //  王倩添加-20080729-记录语句换行标准化后的语句相对行数
	CString srcWord;   //  王倩添加-20080729-记录源程序中token字对应的单词

	int funcBegLine;   //wq-20081220-所在函数的源程序起始行数(标准化后相对)
	int funcEndLine;   //wq-20081220-所在函数的源程序终止行数(标准化后相对)

	int funcBgnRealLine;//wq-20090202-函数起始源程序绝对行数
	int funcEndRealLine;//wq-20090202-函数终止源程序绝对行数

	int cntxtLine;//wq-20090415-记录上下文起始token的序号
	int dowhileLine;//wq-20090415-记录do的配对while的token序号


};

#endif // !defined(AFX_TNODE_H__AE8BB308_C8B4_4F1C_963E_B29F4CECAFD6__INCLUDED_)
