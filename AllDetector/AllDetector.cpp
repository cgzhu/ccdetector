
// AllDetector.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "AllDetector.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "AllDetectorDoc.h"
#include "AllDetectorView.h"
#include "CloneCodeView.h"
#include "MySciEditView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAllDetectorApp

BEGIN_MESSAGE_MAP(CAllDetectorApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CAllDetectorApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
END_MESSAGE_MAP()


// CAllDetectorApp construction

CAllDetectorApp::CAllDetectorApp()
{
	m_bHiColorIcons = TRUE;

	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// If the application is built using Common Language Runtime support (/clr):
	//     1) This additional setting is needed for Restart Manager support to work properly.
	//     2) In your project, you must add a reference to System.Windows.Forms in order to build.
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// TODO: replace application ID string below with unique ID string; recommended
	// format for string is CompanyName.ProductName.SubProduct.VersionInformation
	SetAppID(_T("AllDetector.AppID.NoVersion"));

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CAllDetectorApp object

CAllDetectorApp theApp;


// CAllDetectorApp initialization

BOOL CAllDetectorApp::InitInstance()
{
	//Polaris-20140716
	m_hDll = LoadLibrary(_T("SciLexer.dll"));   
	if (m_hDll==NULL)   
	{   
		AfxMessageBox(_T("加载SciLexer.dll失败..."));
		return FALSE;
	}

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();


	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	EnableTaskbarInteraction();

	// AfxInitRichEdit2() is required to use RichEdit control	
	// AfxInitRichEdit2();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)

	//Polaris-20140716
	void LoadFileInclude(); // 加载预处理目录
	LoadFileInclude();

	void LoadCustomParam();
	LoadCustomParam();
	//

	InitContextMenuManager();

	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_AllDetectorTYPE,
		RUNTIME_CLASS(CAllDetectorDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CMySciEditView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	
	//Polaris-20140714
	pEditTemplate = new CMultiDocTemplate(IDR_AllDetectorTYPE,
		RUNTIME_CLASS(CAllDetectorDoc),
		RUNTIME_CLASS(CChildFrame), // 自定义 MDI 子框架
		RUNTIME_CLASS(CCloneCodeView));
	if (!pEditTemplate)
		return FALSE;
	AddDocTemplate(pEditTemplate);

	//Polaris-20140716
	pSciTemplate = new CMultiDocTemplate(IDR_AllDetectorTYPE,
		RUNTIME_CLASS(CAllDetectorDoc),
		RUNTIME_CLASS(CChildFrame), // 自定义 MDI 子框架
		RUNTIME_CLASS(CMySciEditView));
	if (!pSciTemplate)
		return FALSE;
	AddDocTemplate(pSciTemplate);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;
	// call DragAcceptFiles only if there's a suffix
	//  In an MDI app, this should occur immediately after setting m_pMainWnd

	//Polaris-20140716
	m_pMainWnd->DragAcceptFiles();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;

	//Polaris-20140715
	cmdInfo.m_nShellCommand =CCommandLineInfo::FileNothing;//界面创建时不自动打开空白文档

	ParseCommandLine(cmdInfo);

	//Polaris-20140716
	//启用“DDE 执行”
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// The main window has been initialized, so show and update it
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}

//Polaris-20140714
void CAllDetectorApp::OnFileNew()
{
    //AfxMessageBox("OnMyFileNew ......file new");
    POSITION ps=m_pDocManager->GetFirstDocTemplatePosition();
    CDocTemplate *pDocTemplate=m_pDocManager->GetNextDocTemplate(ps);
    pDocTemplate->OpenDocumentFile(NULL);
}

int CAllDetectorApp::ExitInstance()
{
	//TODO: handle additional resources you may have added

	//Polaris-20140716
	if (m_hDll != NULL)
	{
		FreeLibrary(m_hDll);
	}

	AfxOleTerm(FALSE);

	return CWinAppEx::ExitInstance();
}

// CAllDetectorApp message handlers


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// App command to run the dialog
void CAllDetectorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CAllDetectorApp customization load/save methods

void CAllDetectorApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
	bNameValid = strName.LoadString(IDS_EXPLORER);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EXPLORER);
}

void CAllDetectorApp::LoadCustomState()
{
}

void CAllDetectorApp::SaveCustomState()
{
}

// CAllDetectorApp message handlers



