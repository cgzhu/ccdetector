// SetMinlineDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "SetMinlineDlg.h"
#include "afxdialogex.h"
#include "MainFrm.h"

// CSetMinlineDlg 对话框

IMPLEMENT_DYNAMIC(CSetMinlineDlg, CDialog)

CSetMinlineDlg::CSetMinlineDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetMinlineDlg::IDD, pParent)
{

}

CSetMinlineDlg::~CSetMinlineDlg()
{
}

void CSetMinlineDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//Polaris-20140712
	DDX_Control(pDX, IDC_MINLINE_EDITBOX, minlineEditBox);
}


BEGIN_MESSAGE_MAP(CSetMinlineDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSetMinlineDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSetMinlineDlg 消息处理程序

//Polaris-20140712
/*
 * 函数功能：点击“OK”按钮的消息响应函数
 */
void CSetMinlineDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CMainFrame *pFrame = (CMainFrame *)GetParent();
	CString text("");
	minlineEditBox.GetWindowText(text);
	int minline = atoi(text);
	if( minline <= 0 )
	{
		MessageBox("请输入一个大于0的整数!");
		return;
	}
	pFrame->Min_len = minline;

	//char str[20];
	//itoa(pFrame->Min_len,str,10);
	//MessageBox(str);

	CDialog::OnOK();
}
