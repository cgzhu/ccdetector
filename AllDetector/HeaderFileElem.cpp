// HeaderFileElem.cpp: implementation of the HeaderFileElem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllDetector.h"
#include "HeaderFileElem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "SNODE.h"
#include "FNODE.h"
//#include "dataStruct.h"
#include "Lexical.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

HeaderFileElem::HeaderFileElem()
{
	hFileName = "";
	hFileSrcCode = "";
	macroReplaceArry.RemoveAll();
	hFileTknArry.RemoveAll();
	symbolTable.RemoveAll();
	typedefRcdArry.RemoveAll();
	funcTable.RemoveAll();
}

HeaderFileElem::~HeaderFileElem()
{
	hFileName = "";
	hFileSrcCode = "";
	macroReplaceArry.RemoveAll();
	hFileTknArry.RemoveAll();
	symbolTable.RemoveAll();
	typedefRcdArry.RemoveAll();
	funcTable.RemoveAll();
}

HeaderFileElem::HeaderFileElem(const HeaderFileElem &hFileElem)
{
	hFileName = hFileElem.hFileName;
	hFileSrcCode = hFileElem.hFileSrcCode;
	macroReplaceArry.RemoveAll();
	macroReplaceArry.Copy(hFileElem.macroReplaceArry);
	hFileTknArry.RemoveAll();
	hFileTknArry.Copy(hFileElem.hFileTknArry);
	symbolTable.RemoveAll();
	symbolTable.Copy(hFileElem.symbolTable);
	typedefRcdArry.RemoveAll();
	typedefRcdArry.Copy(hFileElem.typedefRcdArry);
	funcTable.RemoveAll();
	funcTable.Copy(hFileElem.funcTable);
}

HeaderFileElem HeaderFileElem::operator=(const HeaderFileElem &hFileElem)
{
	hFileName = hFileElem.hFileName;
	hFileSrcCode = hFileElem.hFileSrcCode;
	macroReplaceArry.RemoveAll();
	macroReplaceArry.Copy(hFileElem.macroReplaceArry);
	hFileTknArry.RemoveAll();
	hFileTknArry.Copy(hFileElem.hFileTknArry);
	symbolTable.RemoveAll();
	symbolTable.Copy(hFileElem.symbolTable);
	typedefRcdArry.RemoveAll();
	typedefRcdArry.Copy(hFileElem.typedefRcdArry);
	funcTable.RemoveAll();
	funcTable.Copy(hFileElem.funcTable);
	return *this;
}

void HeaderFileElem::Clean()
{
	hFileName = "";
	hFileSrcCode = "";
	macroReplaceArry.RemoveAll();
	hFileTknArry.RemoveAll();
	symbolTable.RemoveAll();
	typedefRcdArry.RemoveAll();
	funcTable.RemoveAll();

}


/*
bool HeaderFileElem::SetHFileName(CString &fileName)
{
	hFileName = fileName;
	if( hFileName == "" )
		return false;
	else
		return true;
}

bool HeaderFileElem::SetStrMacroLines(CString &macroLines)
{
	strMacroLines = macroLines;
	if( strMacroLines == "" )
		return false;
	else
		return true;
}

bool HeaderFileElem::SetStrTypedefLines(CString &typedefLines)
{
	strTypedefLines = typedefLines;
	if( strTypedefLines == "" )
		return false;
	else
		return true;
}

bool HeaderFileElem::SetHFileTknArry(CArray<TNODE,TNODE> &tpdfTknArry)
{
	typedefTknArry.RemoveAll();
	typedefTknArry.Copy(tpdfTknArry);
	if( typedefTknArry.GetSize() == 0 )
		return false;
	else
		return true;
}
bool HeaderFileElem::SetTypedefTknArry(CArray<TNODE,TNODE> &hFlTknArry)
{
	hFileTknArry.RemoveAll();
	hFileTknArry.Copy(hFlTknArry);
	if( hFileTknArry.GetSize() == 0 )
		return false;
	else
		return true;
}

/*
bool HeaderFileElem::BuildStrMacroLines(CString &hProgram)
{
	strMacroLines = "";
	int start=hProgram.Find("define",0);
	while(start>=0 && start<hProgram.GetLength())
	{
		int len=hProgram.GetLength();
		if(ISHAVE_WELL(hProgram,start,6))
		{
			start=start+6;//未发现#，报错！
		}
		else
		{
			strMacroLines += FindDefineLine(hProgram,start);
		}
		start=hProgram.Find("define",start);
	}

	if( strMacroLines == "" )
		return false;
	else
		return true;
}

CString HeaderFileElem::FindDefineLine(CString &program, int start)
{
	bool define_error=0;
	int i=0;
	int pos_inline=0;
	CString del_line=Deal_SomeLine(start,pos_inline,program,1);
	while(del_line.Right(2)=="\\\n" || del_line.Right(3)=="\\\r\n")
	{
		if(del_line.Right(2)=="\\\n")
			del_line.Delete(del_line.GetLength()-2,2);
		else
			del_line.Delete(del_line.GetLength()-3,3);
		del_line=del_line+" "+Deal_SomeLine(start,pos_inline,program,1);//将下一行取出并删除，接在del_line结尾
	}//如果define行结尾处出现行连接符'\'，则将这些行连接起来生成完整的del_line
	
	bool isprint_flag=0;
	for(i=0; i<del_line.GetLength(); i++)
	{
		if(!isprint(del_line[i]))//遇到非可打印字符
		{
			if(isprint_flag==0)//前一个字符也是非可打印字符
			{
				del_line.Delete(i,1);
			}
			else
			{
				isprint_flag=0;
			}
		}
		else
		{
			isprint_flag=1;
		}
	}//格式化del_line，删除无用的非可打印字符
	return del_line;

}

CString HeaderFileElem::Deal_SomeLine(int &position, int &pos_inline, CString &program, bool del_flag)
{
	int head=position,trail=position;
	while(head>=0 && program[head]!='\n')
	{
		head--;
	}
	head++; //该行行头的位置
	pos_inline=position-head;//行头到position的距离
	while(trail<program.GetLength() && program[trail]!='\n')
	{
		trail++;
	}	    
	trail++;//该行行尾的下一个位置

	CString del_line=program.Mid(head,trail-head);
	if(del_flag==1)
	{	
		program.Delete(head,trail-head);
		program.Insert(head,"\r\n");
		if(head+2<program.GetLength())
		{
			position=head+2;
		}
	}
	return del_line;
}

bool HeaderFileElem::ISHAVE_WELL(const CString &cprogram, const int &position, const int length)
//检验position前面是否为'#'，position+length的后面是否为空格或'\t'；
//但是此程序未分析#ifdefined这种情况，并将之归并到此类语法错误中！
{
	bool inc_error=1;//标识头文件include(or define)所在行是否存在语法错误
	int i=position-1;
	while(i>=0 && cprogram[i]!='\n')//检查前面是否有'#'
	{ 
		if(cprogram[i]=='#')
		{
			inc_error=0;
			break;
		}else if(isgraph(cprogram[i]))//检验'#'和include(or define)之间是否有其他可打印的字符
		{
			break;
		}
		i--;
	}
	if(isgraph(cprogram[position+length]))//include(or define)后面是否为可打印字符
	{
		inc_error=1;
	}
	return inc_error;
}
*/


void HeaderFileElem::AddMacroReplaceArry(CString &mcName, CString &mcValue)
{
	MACRO_REPALCE_NODE mcNode;
	mcNode.macroName = mcName;
	mcNode.macroValue = mcValue;
	macroReplaceArry.Add(mcNode);

}
