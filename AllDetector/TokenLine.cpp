// TokenLine.cpp: implementation of the TokenLine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TokenLine.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TokenLine::TokenLine()
{
	tokenLine.RemoveAll();
}

TokenLine::~TokenLine()
{
	tokenLine.RemoveAll();
}

TokenLine::TokenLine(const TokenLine &TL)
{
	tokenLine.RemoveAll();
	tokenLine.Copy(TL.tokenLine);
}

TokenLine& TokenLine::operator =(const TokenLine &TL)
{
	tokenLine.RemoveAll();
	tokenLine.Copy(TL.tokenLine);
	return *this;
}
void TokenLine::RemoveAll()
{
	tokenLine.RemoveAll();
}

void TokenLine::Add(TNODE& TN)
{
	tokenLine.Add(TN);
}

CArray<TNODE,TNODE>& TokenLine::GetTokenLine()
{
	return tokenLine;
}

void TokenLine::Clean()
{
	tokenLine.RemoveAll();
}
