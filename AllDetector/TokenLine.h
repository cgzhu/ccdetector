// TokenLine.h: interface for the TokenLine class.
// 作者：王倩
// 时间：2008-12-22
// 功能：定义tokenFileArray的每个文件TokenedFile的每行token串结构
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TOKENLINE_H__D8D1EF29_E8CC_40C4_99DF_55A1926E00B7__INCLUDED_)
#define AFX_TOKENLINE_H__D8D1EF29_E8CC_40C4_99DF_55A1926E00B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "TNODE.h"
#include "afxtempl.h"

class TokenLine  
{
	friend class FilesToTokens;
	friend class CntxtInconsisDetect;
public:
	void Clean();
	TokenLine();
	virtual ~TokenLine();
	TokenLine(const TokenLine& TL);
	TokenLine& operator =(const TokenLine& TL);
	void RemoveAll();
	void Add(TNODE& TN);
	CArray<TNODE,TNODE>& GetTokenLine();

private:
	CArray<TNODE,TNODE> tokenLine;//基本语句行token流
};

#endif // !defined(AFX_TOKENLINE_H__D8D1EF29_E8CC_40C4_99DF_55A1926E00B7__INCLUDED_)
