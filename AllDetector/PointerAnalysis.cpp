// PointerAnalysis.cpp: implementation of the PointerAnalysis class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "PointerAnalysis.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Expression.h"
#include "IterationUnit.h"
#include "SDGDeclare.h"
#include "SDGBase.h"
#include "SDGCall.h"
#include "AssignmentUnit.h"
#include "dataStruct.h"
#include "ConstData.h"
#include "VarRemove.h"
/*ALIAS* genAlias=NULL;
ALIAS* genalias=NULL;
ALIAS* dereferAlias=NULL;
ALIAS* killAlias=NULL;
ALIAS* addressAlias=NULL;*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PointerAnalysis::PointerAnalysis()
{

}

PointerAnalysis::~PointerAnalysis()
{

}

void PointerAnalysis::operator()(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{

	if(pUSDGHead==NULL||SArry.GetSize()==0)
		return;
	//	AfxMessageBox(pUSDGHead->m_sType);//
	if(pUSDGHead->m_sType=="SDGHEAD")
		for(int i=0;i<pUSDGHead->GetRelateNum();i++)
		{
			CSDGBase*pentry=pUSDGHead->GetNode(i);
			if(pentry)//entry节点
			{
				//AfxMessageBox("pentry");/////////
				for(int j=0;j<pentry->GetRelateNum();j++)
				{
					CSDGBase*pcurrent=pentry->GetNode(j);
					if(pcurrent)
					{    
					//	AfxMessageBox("pcurrent: "+pcurrent->m_sType);////////
						CArray<ALIAS*,ALIAS*>ptSet;
						pointTo(pcurrent,ptSet,SArry);
					}

				}
			}
			
		}
	else if(pUSDGHead->m_sType=="SDGENTRY")
			for(int j=0;j<pUSDGHead->GetRelateNum();j++)
			{
				CSDGBase*pcurrent=pUSDGHead->GetNode(j);
				if(pcurrent)
				{    
				//	AfxMessageBox("pcurrent: "+pcurrent->m_sType);////////
					CArray<ALIAS*,ALIAS*>ptSet;
					pointTo(pcurrent,ptSet,SArry);
				}

			}

}
void PointerAnalysis:: mustAliasReplace(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//必然别名替换
//后根遍历
	if(pHead==NULL)
		return;
//	AfxMessageBox("mustAliasReplace"+pHead->m_sType);
//	if(pHead->m_sType!="ITERATION")//循环体内不进行别名替换
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		
		mustAliasReplace(pHead->GetNode(i),SArry,FArry);

	}
	
	if(pHead->m_sType=="ASSIGNMENT")
	{
		CAssignmentUnit *pAsign=(CAssignmentUnit *)pHead;
	//////////////////////////wtt 2008.4.1/指针自赋值的别名替换///////////////////
		if(pAsign->m_pleft&&pAsign->m_pinfo
			&&pAsign->m_pleft->T.addr==pAsign->m_pinfo->T.addr
			&&pAsign->m_pinfo->info<0
			&&pAsign->m_pleft->T.key==CN_VARIABLE
			&&pAsign->m_pleft->T.addr>=0&&pAsign->m_pleft->T.addr<SArry.GetSize()
			&& SArry[pAsign->m_pleft->T.addr].kind==CN_POINTER)
			{//指针自赋值
			//	AfxMessageBox(TraverseAST(pAsign->m_pleft,SArry)+"="+TraverseAST(pAsign->m_pinfo,SArry));

				bool flag=false;
				int i=0;
				int j=-1;

			
				int srcno=0;
				while(i<pHead->ptSet.GetSize())
				{
					int k=issrc(pAsign->m_pleft,i,pHead->ptSet,SArry);
				  
				    //j=issrc(pAsign->m_pleft,i,pHead->ptSet,SArry);
					if(k>=0)
					{
						if(pAsign->m_pinfo&&GetENodeStr(pAsign->m_pinfo,SArry)!=GetENodeStr(pHead->ptSet[k]->ptar,SArry))
							j=k;
					}
					if(j>=0&&pHead->ptSet[j]->ptar&&pHead->ptSet[j]->ptar->T.addr>=0&&pHead->ptSet[j]->ptar->T.addr<SArry.GetSize()&&SArry[pHead->ptSet[j]->ptar->T.addr].kind==CN_ARRAY	)
					{
						j=-1;
						break;// 是指针与数组的算数运算则不替换
					}
					
					/*if(j>=0)
					{
							i=j;
							if(pHead->ptSet[j]->ptar&&pHead->ptSet[j]->ptar->T.addr>=0&&pHead->ptSet[j]->ptar->T.addr<SArry.GetSize()&&SArry[pHead->ptSet[j]->ptar->T.addr].kind==CN_ARRAY	)
								srcno++;
					}*/
					if(k>=0)
						i=k;
					i++;
				}
			/*	CString st;
				st.Format("%d",srcno);
				AfxMessageBox(st);*/
				if(j>=0&&j<pHead->ptSet.GetSize()&&pHead->ptSet[j]->ptar&&pHead->ptSet[j]->ptar->info>=0)//看看集合中是否有其它别名  防止指针与数组中的指针算数
				{
						ENODE*pl=new ENODE;
						InitialENODE(pl);
						pl=CopyENODE(pl,pHead->ptSet[j]->ptar);
						pl->T.deref--;
						pAsign->m_pleft=pl;

						ENODE*pr=new ENODE;
						InitialENODE(pr);
						pr=CopyENODE(pr,pHead->ptSet[j]->ptar);
						pr->info=pAsign->m_pinfo->info;
						ENODE*pinfo=new ENODE;
						InitialENODE(pinfo);
						pinfo=CopyENODE(pinfo,pAsign->m_pinfo->pinfo[0]);
						pr->pinfo[0]=pinfo;
						pr->T.deref--;
						pAsign->m_pinfo=pr;

				}
				return;
			
			}
	//////////////////////////wtt 2008.4.1/指针自赋值的别名替换///////////////////
		
		replace(pAsign->m_pleft,pAsign->ptSet,SArry,FArry);
		replace(pAsign->m_pinfo,pAsign->ptSet,SArry,FArry);

	}
	else if(pHead->m_sType=="SELECTOR"||pHead->m_sType=="SDGRETURN"||pHead->m_sType=="ITERATION")//
	{
		replace(pHead->m_pinfo,pHead->ptSet,SArry,FArry);


	}
	else if(pHead->m_sType=="SDGCALL" && pHead->m_pinfo && pHead->m_pinfo->T.name=="printf")
	{
		for(int i=0;i<pHead->m_pinfo->info;i++)
		{
			replace(pHead->m_pinfo->pinfo[i],pHead->ptSet,SArry,FArry);
		}

	}

	pHead->ptSet.RemoveAll();
	


}
void PointerAnalysis:: replace(ENODE * &p, CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//表达式中的必然别名替换

	if(p==NULL)
		return;
    if(p->pleft)
		 replace(p->pleft,ptSet,SArry,FArry);
	if(p->pright)
		replace(p->pright,ptSet,SArry,FArry);
//	AfxMessageBox(TraverseAST(p,SArry));
	if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_POINTER)
	{
	//	AfxMessageBox("pointer");
		if(p->T.deref>0)
		{
				CArray<ALIAS*,ALIAS*> Gl,Cl,Kl,G,Gr,S;
				
				ENODE*porigin=p;
				int leftderef=porigin->T.deref;
				lvaluePT(p,ptSet,Gl,Cl,Kl,SArry);
				///////////////////////////////////////
				porigin->T.deref=leftderef;
				if(Gl.GetSize()>0)
				{
					if(Gl[0]->isdefinit==1&&Gl[0]->psrc&&Gl[0]->psrc->T.key!=CN_RANDOMP	)
					{
						//if(Gl[0]->isdefinit==1&&Gl[0]->psrc&&Gl[0]->psrc->T.key==CN_RANDOMP&&Gl[0]->ptar
							//&&Gl[0]->ptar->info<0
						//	)
					//	{ AfxMessageBox(TraverseAST(Gl[0]->psrc,SArry)+","+TraverseAST(Gl[0]->ptar,SArry)+"替换");
					//	}
					//	AfxMessageBox(TraverseAST(Gl[0]->psrc,SArry)+","+TraverseAST(Gl[0]->ptar,SArry)+"替换");
						int i=0;
						int srcno=0;
						while(i<ptSet.GetSize())
						{
							int j=issrc(Gl[0]->psrc,i,ptSet,SArry);
							if(j>=0)
							{
								i=j;
								if(ptSet[j]->ptar&&ptSet[j]->ptar->info<0//指针自赋值
									||ptSet[j]->ptar&&ptSet[j]->ptar->T.addr>=0&&ptSet[j]->ptar->T.addr<SArry.GetSize()&&SArry[ptSet[j]->ptar->T.addr].kind==CN_ARRAY	)//指针指向数组
									srcno++;
								
							}
							i++;
						}
					/*	CString st;
						st.Format("%d",srcno);
						AfxMessageBox(st);*/
						if(srcno<2)//看看集合中是否有其它别名  防止指针与数组中的指针算数

						{
						ENODE*pr=new ENODE;
						InitialENODE(pr);
						pr=CopyENODE(pr,Gl[0]->ptar);
						p=pr;
						//AfxMessageBox("replaced");
						}

					}
					

				}

			//AfxMessageBox("deref>0");
				
		}
		
	}

  

}

void PointerAnalysis::pointTo(CSDGBase *pCurrent,CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry)
{//分析pCurrent指向集合
	if(pCurrent==NULL||SArry.GetSize()==0)
		return;
	
//	AfxMessageBox("pointerto start");//
	
	CSDGBase*pred=GetPrior(pCurrent);//前趋节点
	if(pred!=NULL)
	{
	//	extern CString GetExpString(ENODE*pHead,CArray<SNODE,SNODE> &SArry);
	//	AfxMessageBox(pCurrent->m_sType+" 	"+pred->m_sType+GetExpString(pred->m_pinfo,SArry));
		pCurrent->ptSet.RemoveAll();
		for(int i=0;i<pred->ptSet.GetSize();i++)//用前驱节点指向信息初始化当前节点的指向信息
			{
				pCurrent->ptSet.Add(pred->ptSet[i]);
			}
	}
	//////////////////////////
	/*	CString st;
		st="Init ptSet";
		for(int j=0;j<pCurrent->ptSet.GetSize();j++)
		{
		st+="<";
		st+=GetENodeStr(pCurrent->ptSet[j]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(pCurrent->ptSet[j]->ptar,SArry);
		st+=">";	
		}
		AfxMessageBox(st);*/
		///////////////////////////
	if(pCurrent->m_sType=="ASSIGNMENT")
	{
		CAssignmentUnit *pAsign=(CAssignmentUnit *)pCurrent;
	//	AfxMessageBox(TraverseAST(pAsign->m_pleft,SArry)+"="+TraverseAST(pAsign->m_pinfo,SArry));
		
		assignment(pAsign,SArry);
	}
	else if(pCurrent->m_sType=="SELECTION")
	{
		CArray<ALIAS*,ALIAS*> ptSetSelector;
		for(int i=0;i<pCurrent->GetRelateNum();i++)
		{
			ptSetSelector.RemoveAll();
			CSDGBase*pselector=pCurrent->GetNode(i);
			pointTo(pselector,ptSetSelector,SArry);
			unionPT(pCurrent->ptSet,pselector->ptSet,SArry);
		}

	}
	//else if(pCurrent->m_sType=="SELECTOR"||pCurrent->m_sType=="ITERATION")
	else if(pCurrent->m_sType=="SELECTOR")//wtt 2008.3.31
	//||pCurrent->m_sType=="ITRSUB" ||pCurrent->m_sType=="ESPSUB")// 是其最右子节点的ptSet
	{
		CArray<ALIAS*,ALIAS*> ptSetAssign;

		for(int i=0;i<pCurrent->GetRelateNum();i++)
		{
			ptSetAssign.RemoveAll();
			pointTo(pCurrent->GetNode(i),ptSetAssign,SArry);
			//unionPT(pCurrent->ptSet,ptSetAssign,SArry);
		}
		CSDGBase*p=pCurrent->GetNode(pCurrent->GetRelateNum()-1);
		pCurrent->ptSet.RemoveAll();
		int i;
		for(i=0;p&&i<p->ptSet.GetSize();i++)
		{
			
			pCurrent->ptSet.Add(p->ptSet[i]);

		}
		
	}
	//////////////wtt 2008.3.31/////////////////////////////
	else if(pCurrent->m_sType=="ITERATION")//wtt 2008.3.31迭代循环
	{
		  bool	neediterate=true;
		  CArray<ALIAS*,ALIAS*> ptSetAssign;
		 while(neediterate){
			    neediterate=false;
				for(int i=0;i<pCurrent->GetRelateNum();i++)
				{
					ptSetAssign.RemoveAll();
					pointTo(pCurrent->GetNode(i),ptSetAssign,SArry);
					//unionPT(pCurrent->ptSet,ptSetAssign,SArry);
				}
				CSDGBase*p=pCurrent->GetNode(pCurrent->GetRelateNum()-1);
				if(p)
				{
				//	AfxMessageBox("p");
					if(pCurrent->ptSet.GetSize()!=p->ptSet.GetSize())
					{
						neediterate=true;
				//		AfxMessageBox("size!=");
					}
					else
					{

						for(int j=0;j<p->ptSet.GetSize();j++)
						{
							if(exist(p->ptSet[j],pCurrent->ptSet,SArry)<0)
								neediterate=true;
							//AfxMessageBox("diffrent element");
						}
					}
					if(neediterate==true)
					{
					//	AfxMessageBox("neediterat");
						int i;
						pCurrent->ptSet.RemoveAll();
						for(i=0;p&&i<p->ptSet.GetSize();i++)
						{
			
							pCurrent->ptSet.Add(p->ptSet[i]);

						}


					}
				}

			}
	}
		//////////////wtt 2008.3.31/////////////////////////////


/*	else if(pCurrent->m_sType=="ITERATION")
	{
		CArray<ALIAS*,ALIAS*> ptSetsub;
		for(int i=0;i<pCurrent->GetRelateNum();i++)
		{
			ptSetsub.RemoveAll();
			pointTo(pCurrent->GetNode(i),ptSetsub,SArry);
			if(pCurrent->GetNode(i)&&pCurrent->GetNode(i)->m_sType=="ITRSUB")//与ITRSUB相同
			{	//unionPT(pCurrent->ptSet,ptSetsub,SArry);
				pCurrent->ptSet.RemoveAll();
				CSDGBase*p=pCurrent->GetNode(i);
				for(i=0;p&&i<p->ptSet.GetSize();i++)
				{
					pCurrent->ptSet.Add(p->ptSet[i]);

				}
				
			}
		}

	}*/
//其它节点指向信息集合等于其前驱节点的指向集合
}

void PointerAnalysis::unionPT(CArray<ALIAS*,ALIAS*>&ptSetUnion,CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry)
//将后个集合合并到前个集合中
{    int  *pexist=new int[ptSet.GetSize()];//标记是否已经有的(psrc,ptr)

//////////////////////////
	/*	CString st;
		st="init ptSet";
		for(int j=0;j<ptSet.GetSize();j++)
		{
		st+="<";
		st+=GetENodeStr(ptSet[j]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(ptSet[j]->ptar,SArry);
		st+=">";	
		}
		AfxMessageBox(st);*/
		///////////////////////////
	
	for(int i=0;i<ptSet.GetSize();i++)
	{
		pexist[i]=0;
		
		int j=exist(ptSet[i],ptSetUnion,SArry);
	//	if(j==-1)
	//	{
		//	ptSetUnion.Add(ptSet[i]);
		//	pexist[i]=0;
			//	AfxMessageBox("exist=0");

	//	}
	//	else
		if(j!=-1)
		{
			ptSetUnion[j]->isdefinit*=ptSet[i]->isdefinit;
			pexist[i]=1;
		
		}
	}
	int i;
	for( i=0;i<ptSet.GetSize();i++)
	{
		if(pexist[i]==0)//未处理过
		{
			//AfxMessageBox("exist=0");
			
			int flag=0;
			for(int j=0;j<ptSetUnion.GetSize();j++)
			{
				//AfxMessageBox(GetENodeStr(ptSet[i]->psrc,SArry)+":"+GetENodeStr(ptSetUnion[j]->psrc,SArry));
				if(GetENodeStr(ptSet[i]->psrc,SArry)==GetENodeStr(ptSetUnion[j]->psrc,SArry)
					&&GetENodeStr(ptSet[i]->ptar,SArry)!=GetENodeStr(ptSetUnion[j]->ptar,SArry))//(psrc,ptar1,1)  (psrc, patr2,1)->(psrc,ptar1,0)  (psrc, patr2,0)
				{
					ptSetUnion[j]->isdefinit=0;

					ALIAS* genalias=new ALIAS;						
					genalias->isdefinit=0;		
					genalias->psrc=ptSet[i]->psrc; 
					genalias->ptar=ptSet[i]->ptar; 
					ptSetUnion.Add(genalias);
					flag=1;

				}
			}
			if(flag==0)
			{
					ALIAS* genalias=new ALIAS;						
					genalias->isdefinit=ptSet[i]->isdefinit;		
					genalias->psrc=ptSet[i]->psrc; 
					genalias->ptar=ptSet[i]->ptar; 
					ptSetUnion.Add(genalias);
					

			}


		}
	}

		//////////////////////////
//		CString st;
	/*	st="Union ptSet";
		for( j=0;j<ptSetUnion.GetSize();j++)
		{
		st+="<";
		st+=GetENodeStr(ptSetUnion[j]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(ptSetUnion[j]->ptar,SArry);
		st+=">";	
		}
		AfxMessageBox(st);*/
		///////////////////////////
//	AfxMessageBox("unio ok");
	
delete []pexist;
}
void PointerAnalysis::assignment(CAssignmentUnit *pAsign,CArray<SNODE,SNODE> &SArry)
//分析指针赋值语句
{
	if(pAsign==NULL)
	{
	//	AfxMessageBox("pAsign==NULL");

		return;
	}
	//	AfxMessageBox("assignment start:"+GetExpString(pAsign->m_pleft,SArry)+"="+GetExpString(pAsign->m_pinfo,SArry));


	if(pAsign->m_pleft==NULL||pAsign->m_pinfo==NULL)
	{
	//		AfxMessageBox("pAsign->m_pleft==NULL||pAsign->m_pinfo==NULL");
		return;
	}

	if(pAsign->m_pleft->T.addr<0||pAsign->m_pleft->T.addr>=SArry.GetSize())
	{
	//	AfxMessageBox("pAsign->m_pleft->T.addr<0||pAsign->m_pleft->T.addr>=SArry.GetSize()");
		return;
		
	}
	if(SArry[pAsign->m_pleft->T.addr].kind!=CN_POINTER&&SArry[pAsign->m_pleft->T.addr].kind!=CN_ARRPTR)
	{
	//	AfxMessageBox("SArry[pAsign->m_pleft->T.addr].kind!=CN_POINTER&&SArry[pAsign->m_pleft->T.addr].kind!=CN_ARRPTR");
		return;
	}
	if(pAsign->m_pleft->T.deref==SArry[pAsign->m_pleft->T.addr].arrdim)//是指针解引用赋值而不是修改指针地址
	{
		//AfxMessageBox("pointer deref");
		return;
	}
/*	CString stemp;
	stemp.Format("%d,%d",pAsign->m_pleft->T.deref,SArry[pAsign->m_pleft->T.addr].arrdim);
	AfxMessageBox(stemp);*/
	///////////////////修改为判断右部表达式是否为指针运算（deref<arrdim)或取地址
	//if(!pointerArith(pAsign->m_pinfo))
	//{
	//	return;
	//}
	if(pAsign->m_pinfo->T.key!=CN_VARIABLE)
	{
	//	AfxMessageBox("pAsign->m_pinfo->T.key!=CN_VARIABLE");
		return;
	}

	if(pAsign->m_pinfo->T.addr<0||pAsign->m_pinfo->T.addr>=SArry.GetSize())
		return;
	if(SArry[pAsign->m_pinfo->T.addr].kind==CN_VARIABLE&&pAsign->m_pinfo->T.deref==0)//普通变量赋值
	{
		return;

	}
	if(SArry[pAsign->m_pinfo->T.addr].kind==CN_ARRAY&&pAsign->m_pinfo->T.deref==0&&pAsign->m_pinfo->info!=0&&pAsign->m_pinfo->info>=SArry[pAsign->m_pinfo->T.addr].arrdim)//数组元素给指针脱引用赋值
	{
	//	AfxMessageBox("SArry[pAsign->m_pinfo->T.addr].kind==CN_ARRAY&&pAsign->m_pinfo->T.deref==0&&pAsign->m_pin");
		return;
	}

	if(SArry[pAsign->m_pinfo->T.addr].kind==CN_ARRAY&&pAsign->m_pinfo->T.deref==0&&pAsign->m_pinfo->info==0)
		//标准化将p=s->p=&s[0]其中s为数组名
	{
		for(int i=0;i<SArry[pAsign->m_pinfo->T.addr].arrdim;i++)
		{
			ENODE*pEN=new ENODE;
			InitialENODE(pEN);
			pEN->T.key=CN_CINT;
			pEN->T.addr=0;
			pAsign->m_pinfo->pinfo[i]=pEN;
			pAsign->m_pinfo->info++;
		}
		pAsign->m_pinfo->T.deref=-1;


	}
		

//	AfxMessageBox("assignment start:"+GetExpString(pAsign->m_pleft,SArry)+"="+GetExpString(pAsign->m_pinfo,SArry));

	CArray<ALIAS*,ALIAS*> Gl,Cl,Kl,G,Gr,S;
 
    
    int leftderef=pAsign->m_pleft->T.deref;
	lvaluePT(pAsign->m_pleft, pAsign->ptSet,Gl,Cl,Kl,SArry);
	///////////////////////////////////////
	pAsign->m_pleft->T.deref=leftderef;

//	CString st="lvalue:";
/*	if(pAsign->m_pleft)
		st+=TraverseAST(pAsign->m_pleft,SArry);
	AfxMessageBox(st);
	*/
/*	st+="\r\nGl:";
	for(int k=0;k<Gl.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(Gl[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Gl[k]->ptar,SArry);
		CString str;
		str.Format(",%d",Gl[k]->isdefinit);
		st+=str;
		st+=">";

	}
		st+="\r\nCl:";
/*	for( k=0;k<Cl.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(Cl[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Cl[k]->ptar,SArry);
		st+=">";

	}
			st+="\r\nKl:";
	for( k=0;k<Kl.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(Kl[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Kl[k]->ptar,SArry);
		st+=">";

	}
//	AfxMessageBox(st);*/
	////////////////////////////////////////////
	

    int rightderef=pAsign->m_pinfo->T.deref;
	rvaluePT(pAsign->m_pinfo, pAsign->ptSet,Gr,SArry);
	pAsign->m_pinfo->T.deref=rightderef;
	/*	CString st="rvalue:";
	if(pAsign->m_pinfo)
		st+=GetENodeStr(pAsign->m_pinfo,SArry);
	AfxMessageBox(st);*/
	
////////////////////////////////////
/*	st="\r\nGr:";
	for(  k=0;k<Gr.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(Gr[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Gr[k]->ptar,SArry);
		CString str;
		str.Format(",%d",Gr[k]->isdefinit);
		st+=str;
		st+=">";


	}
	AfxMessageBox(st);*/

	//////////////////////
	 if(pAsign->m_pinfo->info<0)//指针的自加、自减算术运算q=q+1或int **p=&q; q=*p+1;;
	{//Kill空,但change不一定空，如果change为空则gen不空，否则Gen为空
		
		if(Gl.GetSize()>0&&Gr.GetSize()>0)
		{
	/*			st="\r\nGl:";
	for(int k=0;k<Gl.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(Gl[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Gl[k]->ptar,SArry);
		st+=">";

	}
		st+="\r\nGr:";
	for( k=0;k<Gr.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(Gr[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Gr[k]->ptar,SArry);
		CString str;
		str.Format(",%d",Gl[k]->isdefinit);
		st+=">";

	}*/
	

//			if(Gl[0]->ptar&&Gr[0]->ptar&&GetENodeStr(Gl[0]->ptar,SArry)==GetENodeStr(Gr[0]->psrc,SArry))//指针自赋值
			if(Gl[0]->ptar&&Gr[0]->ptar&&Gl[0]->ptar->T.key==Gr[0]->psrc->T.key&&Gl[0]->ptar->T.name==Gr[0]->psrc->T.name)//指针自赋值wtt 2008.3.31

			{
				//AfxMessageBox("指针注销");///
				for(int k=0;k<pAsign->ptSet.GetSize();k++)
				{
					if(GetENodeStr(Gl[0]->ptar,SArry)==GetENodeStr(pAsign->ptSet[k]->psrc,SArry)&&GetENodeStr(Gr[0]->ptar,SArry)!=GetENodeStr(pAsign->ptSet[k]->ptar,SArry))//将已由的指向注掉
					{
					//	AfxMessageBox("指针注销");///
					//	pAsign->ptSet.RemoveAt(k);
						ALIAS* genalias=new ALIAS;
						if(Gl[0]->isdefinit==1&&Gr[0]->isdefinit==1)
						{
			
							genalias->isdefinit=1;
			
						}
						else
						{
							genalias->isdefinit=0;
						}
						 genalias->psrc=new ENODE;
						 InitialENODE(genalias->psrc);
						 genalias->psrc=CopyENODE(genalias->psrc,Gl[0]->ptar);

							  
						 genalias->ptar=Gr[0]->psrc;
			 
						  G.Add(genalias);



					}
				}//for
             	for(int i=0;i<G.GetSize();i++)
				{
					if(exist(G[i],pAsign->ptSet,SArry)<0)
					pAsign->ptSet.Add(G[i]);
				}  
		/*	CString st="\r\nG:";
			for( k=0;k<pAsign->ptSet.GetSize();k++)
			{
		
			st+="<";
			
			st+=GetExpString(pAsign->ptSet[k]->psrc,SArry);
			st+=",";
			st+=GetExpString(pAsign->ptSet[k]->ptar,SArry);
			CString str;
			str.Format(",%d",pAsign->ptSet[k]->isdefinit);
			st+=">";

			}
			AfxMessageBox(st);*/
			
				return;
				
			}//if

		}

	}
	/////////////////////////////////////////////////////////
	 G.RemoveAll();
	  for( int i=0;i<Gl.GetSize();i++)
		for(int j=0;j<Gr.GetSize();j++)
		{
			ALIAS* genalias=new ALIAS;
			if(Gl[i]->isdefinit==1&&Gr[j]->isdefinit==1)
			{
			
				genalias->isdefinit=1;
			
			}
			else
			{
				genalias->isdefinit=0;
			}
			 genalias->psrc=Gl[i]->ptar; 

			 ENODE*ptar=NULL;
			 if(Gr[j]->psrc->T.key==CN_RANDOMP)//&q
			 {
				ptar=new ENODE;
				InitialENODE(ptar);
				ptar=CopyENODE(ptar,Gr[j]->ptar);
				ptar->T.deref=0;
				genalias->ptar=ptar;

			 }
			 else if(Gr[j]->ptar->T.key==CN_RANDOMP)//wtt 2008.3.28指针q
			 {
				ptar=new ENODE;
				InitialENODE(ptar);
				ptar=CopyENODE(ptar,Gr[j]->psrc);
				ptar->T.deref=1;
				genalias->ptar=ptar;
				//AfxMessageBox(TraverseAST(Gr[j]->psrc,SArry));

			 }
			 else
			 {
				genalias->ptar=Gr[j]->ptar;
			 }
			 G.Add(genalias);
			 

		}
    int i;
	for(i=0;i<Cl.GetSize();i++)
	{    int k;
		 k=exist(Cl[i],pAsign->ptSet,SArry);
		 if(Cl[i]->isdefinit==0 && k>=0)
		{
			 ALIAS* genalias=new ALIAS;
			 genalias->psrc=pAsign->ptSet[k]->psrc;
			 genalias->ptar=pAsign->ptSet[k]->ptar;
			 genalias->isdefinit=0;
			 pAsign->ptSet.RemoveAt(k);
			// AfxMessageBox("change insert");
			 pAsign->ptSet.Add(genalias);

			 
		  	//pAsign->ptSet[k]->isdefinit=0;

		}
	}
	for(i=0;i<Kl.GetSize();i++)
	{
		if(Kl[i]->isdefinit==1)
		{
			int k;
			k=exist(Kl[i],pAsign->ptSet,SArry);
			if(k>=0)
			{
				 pAsign->ptSet.RemoveAt(k);

			}
		}
	}
	for(i=0;i<G.GetSize();i++)
	{
		pAsign->ptSet.Add(G[i]);
	}  
	////////////////////////////////////
  /* CString	st="\r\npoint to set:";
	for(int  k=0;k<pAsign->ptSet.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(pAsign->ptSet[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(pAsign->ptSet[k]->ptar,SArry);
		CString str;
		str.Format(",%d",pAsign->ptSet[k]->isdefinit);
		st+=str;
		st+=">";

	}
	AfxMessageBox(st);*/

	//////////////////////

}

void PointerAnalysis::lvaluePT(ENODE*pleft, CArray<ALIAS*,ALIAS*>&ptSet,CArray<ALIAS*,ALIAS*>&Gl,CArray<ALIAS*,ALIAS*>&Cl,CArray<ALIAS*,ALIAS*>&Kl,CArray<SNODE,SNODE> &SArry)
{
	if(pleft==NULL)
		return;
	CArray<ALIAS*,ALIAS*>G,C,K;
    //AfxMessageBox("lvaluePT start");//
	if(pleft->T.deref>0)
	{
		//AfxMessageBox("pleft->T.deref>0");
		pleft->T.deref--;//注意这个地方将表达式的deref修改了，最后修改为0
		lvaluePT(pleft,ptSet,G,C,K,SArry);
		dereference(G,ptSet,Gl,SArry);
		dereference(C,ptSet,Cl,SArry);
		dereference(K,ptSet,Kl,SArry);
	}
	else if(pleft->T.deref==0)
	{
		ENODE*pscr=new ENODE;
		InitialENODE(pscr);
		pscr->T.key=CN_RANDOMP;
		ALIAS* genAlias=new ALIAS;
		/*
		genAlias->psrc=new ENODE;
		genAlias->psrc=CopyENODE(genAlias->psrc,pscr);*/
		genAlias->psrc=pscr;

	/*	genAlias->ptar=new ENODE;
        genAlias->ptar=CopyENODE(genAlias->ptar,pleft);*/
		genAlias->ptar=pleft;
		genAlias->isdefinit=1;
		Gl.Add(genAlias);
		//////////////////////////
	/*	CString st;
		st="";
		for(int j=0;j<Gl.GetSize();j++)
		{
		st+="<";
		st+=GetENodeStr(Gl[j]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Gl[j]->ptar,SArry);
		st+=">";	
		AfxMessageBox("add: to Gl"+st);
		}*/

		///////////////////////////
	//	AfxMessageBox("add");///
		
		int k=0;
		int flag=0;
		while(k!=-1&&k<ptSet.GetSize())
		{
			k=issrc(pleft,k,ptSet,SArry);//返回值记录在ptSet中的下标，-1表示没找到
			if(k!=-1)
			{
				Cl.Add(ptSet[k]);
				Kl.Add(ptSet[k]);
				flag=1;
				k++;
			}

		}
		if(flag==0)
		{
			ALIAS* killAlias=new ALIAS;
		/*	killAlias->psrc=new ENODE;
			killAlias->psrc=CopyENODE(killAlias->psrc,pleft);*/
			killAlias->psrc=pleft;

		/*	killAlias->ptar=new ENODE;
			killAlias->ptar=CopyENODE(killAlias->ptar,pscr);*/
			killAlias->ptar=pscr;

			killAlias->isdefinit=1;
			Cl.Add(killAlias);
			Kl.Add(killAlias);						

		}
			//////////////////////////
	
		/*st="";
		for( j=0;j<Kl.GetSize();j++)
		{
		st+="<";
		st+=GetENodeStr(Kl[j]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Kl[j]->ptar,SArry);
		st+=">";	
		AfxMessageBox("add: to Kl"+st);
		}*/

		///////////////////////////

	}


}
void PointerAnalysis::rvaluePT(ENODE*pright, CArray<ALIAS*,ALIAS*>&ptSet,CArray<ALIAS*,ALIAS*>&Gr,CArray<SNODE,SNODE> &SArry)
{
	if(pright==NULL)
		return;
	CArray<ALIAS*,ALIAS*>G;
	if(pright->T.deref==-1)
	{
		pright->T.deref++;
		rvaluePT(pright,ptSet,G,SArry);
		address(G,Gr,SArry);
	}
	else if(pright->T.deref>0)
	{
//		AfxMessageBox("pright->T.deref>0");
		pright->T.deref--;//注意这个地方将表达式的deref修改了，最后修改为0
		rvaluePT(pright,ptSet,G,SArry);
		dereference(G,ptSet,Gr,SArry);
	}
	else if(pright->T.deref==0)
	{
		
	//	int k;
	//	k=issrc(pright,ptSet,SArry);//返回值记录在ptSet中的下标，-1表示没找到
	//	if(k!=-1)
	//	{
		//	AfxMessageBox("gr.add");
	//		Gr.Add(ptSet[k]);
	//	}
	//	else
	//	{
			ENODE*ptar=new ENODE;
			InitialENODE(ptar);
			ptar->T.key=CN_RANDOMP;
			ALIAS* genAlias=new ALIAS;
			genAlias->psrc=pright;
			genAlias->ptar=ptar;
			genAlias->isdefinit=1;

			Gr.Add(genAlias);					

	//	}
	/*	CString st="before address";
		for(  k=0;k<Gr.GetSize();k++)
		{
		
		st+="<";
		st+=GetENodeStr(Gr[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(Gr[k]->ptar,SArry);
		CString str;
		str.Format(",%d",Gr[k]->isdefinit);
		st+=str;
		st+=">";


		}
		AfxMessageBox(st);*/
		///////////////////////////

	

	}

}
void PointerAnalysis::dereference(CArray<ALIAS*,ALIAS*>&S,CArray<ALIAS*,ALIAS*>&ptSet,CArray<ALIAS*,ALIAS*>&R,CArray<SNODE,SNODE> &SArry)
{
	//////////////////////////////////////
/*	CString st;
	st="dereference G:";
	for(int k=0;k<S.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(S[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(S[k]->ptar,SArry);
		st+=">";

	}
	AfxMessageBox(st);*/
	//////////////////////////////////////////
	for(int i=0;i<S.GetSize();i++)
	{
		
		
		int k=0;
		while(k!=-1&&k<ptSet.GetSize())
		{
			k=issrc(S[i]->ptar,k,ptSet,SArry);//返回值记录在ptSet中的下标，-1表示没找到
			if(k!=-1)
			{
			//	AfxMessageBox("issrc!=1");

				ALIAS* dereferAlias=new ALIAS;
				dereferAlias->psrc=S[i]->ptar;
				dereferAlias->ptar=ptSet[k]->ptar;
				dereferAlias->isdefinit=S[i]->isdefinit* ptSet[k]->isdefinit;
				R.Add(dereferAlias);
				
				k++;
			}	
		}
		//else
		//	AfxMessageBox("empty");
	}
		//////////////////////////////////////
/*	//CString st;
	st="dereferenceed G:";
	for( k=0;k<R.GetSize();k++)
	{
		
		st+="<";
		st+=GetENodeStr(R[k]->psrc,SArry);
		st+=",";
		st+=GetENodeStr(R[k]->ptar,SArry);
		st+=">";

	}
	AfxMessageBox(st);*/
	//////////////////////////////////////////

}


int PointerAnalysis::issrc(ENODE*pEN,int initpos,CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry)
//从下标initpos开始找，返回值记录在ptSet中的下标，-1表示没找到
{
	if(pEN==NULL||ptSet.GetSize()==0||SArry.GetSize()==0)
		return -1;
	int i;
	for(i=initpos;i<ptSet.GetSize();i++)
	{
		if(GetENodeStr(pEN,SArry)==GetENodeStr(ptSet[i]->psrc,SArry))
		{
			return i;

		}
	}
	if(i>=ptSet.GetSize())
		return -1;

}
void PointerAnalysis::InitialENODE(ENODE *pENODE)
{
	pENODE->pleft=NULL;
	pENODE->pright=NULL;
	pENODE->info=0; 
	for(int i=0;i<10;i++)
	{
		pENODE->pinfo[i]=NULL;
	}
	pENODE->T.addr=-1;
	pENODE->T.deref=0;
	pENODE->T.paddr=-1;
	pENODE->T.key=0;
	pENODE->T.line=-1;
	pENODE->T.value=0;
	pENODE->T.name="";

}

void PointerAnalysis::address(CArray<ALIAS*,ALIAS*>&S,CArray<ALIAS*,ALIAS*>&R,CArray<SNODE,SNODE> &SArry)
{
	for(int i=0;i<S.GetSize();i++)
	{
		ENODE*pscr=new ENODE;
		InitialENODE(pscr);
		pscr->T.key=CN_RANDOMP;

		ALIAS* addressAlias=new ALIAS;
		addressAlias->psrc=pscr;
		addressAlias->ptar=S[i]->psrc;
		addressAlias->isdefinit=S[i]->isdefinit;
		R.Add(addressAlias);
	}

}

int PointerAnalysis::exist(ALIAS* aliastriple,CArray<ALIAS*,ALIAS*>& ptSet,CArray<SNODE,SNODE> &SArry)
{
	if(ptSet.GetSize()==0)
		return -1;
	int i;
	for(i=0;i<ptSet.GetSize();i++)
	{
		if(GetENodeStr(aliastriple->psrc,SArry)==GetENodeStr(ptSet[i]->psrc,SArry)
			&&GetENodeStr(aliastriple->ptar,SArry)==GetENodeStr(ptSet[i]->ptar,SArry))
		{
			return i;

		}
	}
	if(i>=ptSet.GetSize())
		return -1;
}
CSDGBase* PointerAnalysis::GetPrior(CSDGBase *p)
{//获取节点p在CDS中的前驱节点，
	//若p是selector节点则其前驱节点是它的父节点selection的左兄弟，
	//若p是iteration节点的第一个子节点，则其前驱节点是其父节点iteration
	//其余类型的节点的前驱节点为其左兄弟
	
	//////////////////////////////////////
	if(p==NULL)
		return NULL;
	
	CSDGBase* pf=NULL;
	
	if(p->m_sType=="ITERATION"||p->m_sType=="SELECTOR"||p->m_sType=="SELECTION"||p->m_sType=="ASSIGNMENT"||p->m_sType=="SDGCALL"||p->m_sType=="SDGRETURN"||p->m_sType=="SDGBREAK"||p->m_sType=="SDGCONTINUE")
	{
	//	AfxMessageBox("if assign");
		pf=p->GetFather();
		if(pf==NULL)
			return NULL;

	}
	/////////////wt 2008.4.2/iteration中第一个分支中的第一个ie节点前驱为iteration节点//////////////////
	CSDGBase*piteration=p;
	while(piteration&&FindIndex(piteration)==0
		&&piteration->m_sType!="ITERATION"&&piteration->m_sType!="SDGENTRY")
	{
		piteration=piteration->GetFather();
		if(piteration&&piteration->m_sType=="ITERATION")
			return piteration;
	}
	//////////////若p是iteration节点的第一个子节点，则其前驱节点是其父节点iteration/////////////////
	//	AfxMessageBox(p->m_sType);
//	AfxMessageBox("get prior ok");/////////////
	if(p->m_sType=="SELECTOR")
	{
		pf=p->GetFather();
		if(pf==NULL)
			return NULL;
		CSDGBase*pselection=pf;
		pf=pf->GetFather();
		if(pf==NULL)
			return NULL;

	/*	if(pf->m_sType=="ITERATION"&&FindIndex(pselection)==0&&FindIndex(p)==0)
		{
		//	AfxMessageBox("special selcector"+pf->m_sType+p->m_sType);
			return pf;//iteration中的第一个slection的第一个selector的前驱是iteration节点
		}*/

	}
/*	CSDGBase*pfather=p->GetFather();
	if(pfather&&pfather->m_sType=="ITERATION"&&FindIndex(p)==0)
	{
	//	AfxMessageBox("the first child of itearaion");
		return pfather;
	}*/
	////////////////////////////////////////////////////
	/*if(pf)
		AfxMessageBox(p->m_sType+" s father"+pf->m_sType);
	else
		AfxMessageBox(p->m_sType+" s father is NULL");*/
	/////////////////////////////////////////////////////

	if(p->m_sType=="SELECTOR"||p->m_sType=="ITERATION"||p->m_sType=="SELECTION"||p->m_sType=="ASSIGNMENT"||p->m_sType=="SDGCALL"||p->m_sType=="SDGRETURN"||p->m_sType=="SDGBREAK"||p->m_sType=="SDGCONTINUE")
	{
		bool flag=true;
		int index;
		CSDGBase*p1=p;
		if(p->m_sType=="SELECTOR")///5.12//
			p1=p->GetFather(); ///5.12
		while(flag&&pf)
		{
			index=FindIndex(p1);
		
			if(index-1>=0)
				flag=false;
			else
			{
				
				p1=pf;
				pf=pf->GetFather(); 

			}
		
		}
		if(flag==false)
		{
		//	AfxMessageBox("prior: "+pf->GetNode(index-1)->m_sType);/////////
			return pf->GetNode(index-1) ;
		}
		else
		{
			//AfxMessageBox("else");/////////
			return NULL;
		}
	}
	else
	{
//			AfxMessageBox("outside else");/////////

		return NULL;
	}

	/////////////////////////////////

}


ENODE * PointerAnalysis:: CopyENODE(ENODE *pnew,ENODE *pint)
{

	// copy node pint to pnew and return it;

	if(pnew==NULL || pint==NULL)
	{
		return NULL;
	}

	pnew->info=pint->info;
	pnew->T.deref=pint->T.deref;
	pnew->pleft=pint->pleft;
	pnew->pright=pint->pright;    
	for(int i=0;i<10;i++)
	{
		pnew->pinfo[i]=NULL; 
	}

	CopyTNODE(pnew->T,pint->T);
	
	int info=pnew->info;
	if(info<0)
		info=-info;
	int i;
	for(i=0;i<info && i<10;i++)
	{
		pnew->pinfo[i]=CopyTree(pnew->pinfo[i],pint->pinfo[i]); 
        
	}
	return pnew;

}

 void PointerAnalysis:: CopyTNODE(TNODE &TNew,TNODE &TInt)
{
	/*****************************************************

	             copy TInt to TNew
	
	*****************************************************/

	TNew.addr=TInt.addr;
	TNew.deref=TInt.deref;
	TNew.key=TInt.key;
	TNew.name=TInt.name;
	TNew.line=TInt.line;  
	TNew.value=TInt.value;

}
  ENODE * PointerAnalysis::CopyTree(ENODE *pnew,ENODE *pint)
{
	/******************************************************
	 FUNCTION:
	 copy a tree "pint" to "pnew" and return the new tree's
	 pointer;
	 PARAMETERS:
	 pint : point to the initial tree.
     pnew : point to the new tree.
	*******************************************************/
	
	
	if(pint==NULL)
	{
		return NULL;
	}
    static int num;
	num++;
	
	CList<ENODE *,ENODE *> STK;
	CList<ENODE *,ENODE *> STACK;
	ENODE *p=NULL,*ptr=NULL,*ptemp=NULL;
	int label=0;
	
	p=pint;
	ptr=pnew;
	
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// add action here
            ptr=new ENODE;
			ptr->info=0; 
			ptr->T.deref=0;//wtt 2008.3.19
			ptr->pleft=NULL;
			ptr->pright=NULL;
			for(int i=0;i<10;i++)
			{
				ptr->pinfo[i]=NULL; 				
			}

			ptr=CopyENODE(ptr,p);
			//end
            
			if(label==-1)
			{
				ptemp->pright=ptr; 
			}
			else if(label==1)
			{
				if(STACK.GetCount()>0)
				{
					STACK.GetTail()->pleft=ptr;

				}

			}
			else if(label==0)
			{
				pnew=ptr;
			}


			STK.AddTail(p);
			STACK.AddTail(ptr); 
             
			p=STK.GetTail()->pleft;  
			label=1;  
		}
		else
		{
			p=STK.GetTail()->pright;
			ptemp=STACK.GetTail(); 

			STACK.RemoveTail(); 
			STK.RemoveTail();

			label=-1;
		}
	}

	return pnew;
    
}
CString PointerAnalysis::TraverseAST(ENODE*pHead,CArray<SNODE,SNODE> &SArry)
{//中根序遍历抽象语法树
	CString s="",stemp="";
	ENODE*p=pHead;

	if(p==NULL)
		return stemp;
  /* CString st;
   st.Format("%d",p->T.deref);
   AfxMessageBox(st);*/

	for(int m=0;m<p->T.deref;m++)
	{
		//	AfxMessageBox("ok");//////////
		stemp=stemp+="*(";
	}


	if(p->pright==NULL)
	{
		

		if((p->T).key>0&&(p->T).key<132)
		{
			switch((p->T).key)
			{
		
			case CN_CINT:
				s.Format("%d",(p->T).addr); 
				//AfxMessageBox(s);
				stemp+=s;
				break;
			case CN_CFLOAT:
				s.Format("%f",(p->T).value);
				//	AfxMessageBox(s);
				stemp+=s;
			case CN_VARIABLE:
			case CN_CSTRING:
			case CN_DFUNCTION:
			case CN_BFUNCTION:
					//////////wtt 2008.3.18////////////////*指针//根据deref记得的值输出*/
			/*	if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_POINTER)
				{	
					//AfxMessageBox("pointer ok");//////////
				
			
					for(int m=0;m<p->T.deref;m++)
					{
					//	AfxMessageBox("ok");//////////
						stemp="*"+stemp;
					}

				}//if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&*/
				if(p->T.key==CN_VARIABLE&&p->T.deref==-1)
				{
					
				//		AfxMessageBox("address of pointer ok");///////
				//			AfxMessageBox((p->T).name);
					stemp+="&";

				}
				///////////////////wtt 2008.3.18//////////////////////////


				stemp+=(p->T).name;
				//	AfxMessageBox((p->T).name);
				break;
			default:
	
				int key=(p->T).key;
			//AfxMessageBox(C_ALL_KEYWORD[key]);
				if(key>=0&&key<=132)
					stemp+=C_ALL_KEYWORD[key];
			}//switch
		}//if((p->T).key>0&&(p->T).key<132)
		else//对应于if((p->T).key>0&&(p->T).key<132)
			stemp+="$";
	/////////////////////wtt////3.2
//		if(p->T.key==CN_VARIABLE&&SArry[p->T.addr].kind==CN_ARRAY||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)
		if(p->T.key==CN_VARIABLE&&(p->T.addr>=0&&p->T.addr<SArry.GetSize()&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_ARRPTR||SArry[p->T.addr].kind==CN_POINTER))||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)// WTT 2008 0318

		{
			int i=0;
			CString str;
			if(p->T.key==CN_VARIABLE)
				str="";
			else
				str="(";
			while(i<10&&p->pinfo[i]!=NULL)
			{
			//	AfxMessageBox(p->T.name);

				if(p->T.key==CN_VARIABLE)
					str+="[";
				
				str+=TraverseAST(p->pinfo[i],SArry);

				if(p->T.key==CN_VARIABLE)
					str+="]";
				else 
					str+=",";

				i++;

			}

		if(p->T.key==CN_VARIABLE)
				str+="";
		else
		{
			str.SetAt(str.GetLength()-1,' ');
			str+=")";
		}
				
			stemp+=str;

		}//		if(p->T.key==CN_VARIABLE&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_POINTER)||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)// WTT 2008 0318

		if(p->pleft)
		{
		//	AfxMessageBox("p->pleft");
			stemp+=TraverseAST(p->pleft,SArry);
		
		}
	for(int m=0;m<p->T.deref;m++)
	{
		//	AfxMessageBox("ok");//////////
		stemp=stemp+=")";
	}
		return stemp;


	}//if(p->pright==NULL)

	
	if(p->pleft)
	{
	//	AfxMessageBox("p->pleft");
		stemp+="("+TraverseAST(p->pleft,SArry);
		
	}
	if((p->T).key>0&&(p->T).key<132)
	{
		switch((p->T).key)
		{
		
		case CN_CINT:
			s.Format("%d",(p->T).addr); 
			//AfxMessageBox(s);
			stemp+=s;
			break;
		case CN_CFLOAT:
			s.Format("%f",(p->T).value);
			//	AfxMessageBox(s);
			stemp+=s;
		case CN_VARIABLE:
		case CN_CSTRING:
		case CN_DFUNCTION:
		case CN_BFUNCTION:
								//////////wtt 2008.3.18////////////////*指针//根据deref记得的值输出*/
			/*	if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_POINTER)
				{	
					//AfxMessageBox("pointer ok");//////////
				
			
					for(int m=0;m<p->T.deref;m++)
					{
					//	AfxMessageBox("ok");//////////
						stemp="*"+stemp;
					}

				}*/
				if(p->T.key==CN_VARIABLE&&p->T.deref==-1)
				{
					
				//		AfxMessageBox("address of pointer ok");///////
				//			AfxMessageBox((p->T).name);
					stemp+="&";

				}
				///////////////////wtt 2008.3.18//////////////////////////

			stemp+=(p->T).name;
			//	AfxMessageBox((p->T).name);
			break;
		default:
	
			int key=(p->T).key;
			//AfxMessageBox(C_ALL_KEYWORD[key]);
            stemp+=C_ALL_KEYWORD[key];
		}
	}
	else
			stemp+="$";
	/////////////////////wtt////3.2
	if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_ARRPTR)||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)
	{
	// AfxMessageBox("array function");
		int i=0;
		CString str;
			if(p->T.key==CN_VARIABLE)
				str="";
			else
				str="(";
		while(i<10&&p->pinfo[i]!=NULL)
		{
			if(p->T.key==CN_VARIABLE)
				str+="[";
			
			str+=TraverseAST(p->pinfo[i],SArry);

				if(p->T.key==CN_VARIABLE)
				str+="]";
			else 
				str+=",";

			i++;

		}
		if(p->T.key==CN_VARIABLE)
				str+="";
		else 
		{	str.SetAt(str.GetLength()-1,' ');
			str+=")";
		}
		stemp+=str;

		}

	///////////////////////////
	if(p->pright)
	{ //   AfxMessageBox("p->pright");
		stemp+=TraverseAST(p->pright,SArry)+")";
	}
	int m;
	for( m=0;m<p->T.deref;m++)
	{
		//	AfxMessageBox("ok");//////////
		stemp=stemp+=")";
	}
	return stemp;


}
int PointerAnalysis::FindIndex(CSDGBase *pson)
{
	/***********************************************
		FUNCTION:
		Find the index of pson in its father;
		PARAMETER:
		pson : ...
	***********************************************/
	
	CSDGBase *p=NULL;
	if(pson)
	{
		p=pson->GetFather();
	}
	else
	{
		return -1;
	}
	
	if(p)
	{
		int i=0;
		while(i<p->GetRelateNum())
		{
			if(p->GetNode(i)&&p->GetNode(i)->idno==pson->idno)
			{
				return i;
			}
			i++;
		}
	}
	return -1;
}


