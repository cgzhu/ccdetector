// SDGIncDec.h: interface for the CSDGIncDec class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGINCDEC_H__29CDFC1E_4FA7_49A8_BC1D_F571195F048B__INCLUDED_)
#define AFX_SDGINCDEC_H__29CDFC1E_4FA7_49A8_BC1D_F571195F048B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGIncDec : public CSDGBase  
{
public:
	CSDGIncDec();
	virtual ~CSDGIncDec();
public:
	int kind;
	int expbeg;
	int expend;
	int lor;
	

};

#endif // !defined(AFX_SDGINCDEC_H__29CDFC1E_4FA7_49A8_BC1D_F571195F048B__INCLUDED_)
