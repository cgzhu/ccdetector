// TokenedFile.h: interface for the TokenedFile class.
// 作者：王倩
// 时间：2008-12-22
// 功能：定义tokenFileArray的每个文件的token串以及文件名等结构
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TOKENEDFILE_H__3A24CD2A_77AB_434A_9D2C_D0646E112011__INCLUDED_)
#define AFX_TOKENEDFILE_H__3A24CD2A_77AB_434A_9D2C_D0646E112011__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "TokenLine.h"
#include "SNODE.h"
#include "FNODE.h"

class TokenedFile  
{
	friend class FilesToTokens;
	friend class CntxtInconsisDetect;
public:
	void Clean();
	TokenedFile(const TokenedFile& TF);
	TokenedFile();
	virtual ~TokenedFile();
	TokenedFile& operator=(const TokenedFile& TF);
	void Assign(const CString &p_fileName,
				const long &p_hashedFileName,
				const CArray<TokenLine,TokenLine> &p_tokenList,
				const CArray<SNODE,SNODE>& p_SArry,
				const CArray<FNODE,FNODE>& p_FArry);
	CString &GetFileName();
	long &GetHashedFileName();
	CArray<TokenLine,TokenLine>& GetTokenList();
	void SetFileName(const CString& p_fileName);
	void SetHashedFileName(const long& hashedFileName);
	void SetTokenList(const CArray<TokenLine,TokenLine>& p_tokenList);
	void AddTokenList(const TokenLine& TL);
	CArray<TNODE,TNODE>& GetTokenLine(const int& index);
private:
	CString fileName;//C源文件的完整路径文件名称
	long hashedFileName;//fileName经散列算法hashpjw后对应的数字
	CArray<TokenLine,TokenLine> tokenList;//C源文件token流的按行存储
	CArray<SNODE,SNODE> SArry;//wq-20090505-符号表
	CArray<FNODE,FNODE> FArry;//wq-20090505-函数列表
};

#endif // !defined(AFX_TOKENEDFILE_H__3A24CD2A_77AB_434A_9D2C_D0646E112011__INCLUDED_)
