// Out_CpPos.cpp: implementation of the Out_CpPos class.
//
//////////////////////////////////////////////////////////////////////
/*
 * 这个类记录了重复代码片段的信息
 */


#include "stdafx.h"
#include "Out_CpPos.h"
#include "direct.h" 

#include "math.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Out_CpPos::Out_CpPos()
{
	CP_OUT_PATH = "D:\\CP_OUT.txt";
	out_file_path = "";

}

Out_CpPos::~Out_CpPos()
{
	Con_SEG_Array1.RemoveAll();
	Con_SEG_Array2.RemoveAll();
	CP_OUT_PATH = "D:\\CP_OUT.txt";
	CP_Seg_Array.RemoveAll();
	int i;
	for( i = 0; i < 1000; i++ )
	{
		CP_POS_Array[i].RemoveAll();
	}
	CP_Useless_Frag.RemoveAll();

}

void Out_CpPos::Out_CpCodePos(class Seq_Mining &Rsm)
{
	OutputMemoryRecord("","",out_file_path,0,"");
	OutputMemoryRecord("Out_CpPos","Begin-Out_CpCodePos",out_file_path,1,"");
	int i=0,j=0;
	for(i=0; i<10000; i++)
	{
		for(j=0; j<Rsm.LS[i].GetSize(); j++)
		{
			CP_Segment CPS;
			if(CPS.Init_CPNode(Rsm, i, j)==1)
			{
				CP_Seg_Array.Add(CPS);
			}
		}
	}//将满足条件的碎片信息加入到数组CP_Seg_Array中


/*	for( i = 0; i < CP_Seg_Array.GetSize(); i++)
	{
			cout<<CP_Seg_Array[i].CP_Array.GetSize()<<endl;
	}*/
	Sort_CPS();//将CP_Seg_Array中的所有元素按照序列长度排序
	Generate_Hvalue();

//	Out_LCP_Pos();

//	int step=0;
//	Out_CPSeg(step);//跟踪测试，观察每次合并情况
	int len = CP_Seg_Array.GetSize();
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		//long beginTime=clock();
		if(CP_Seg_Array[i].Is_Expanded==1)
		{
			continue;//此片段已被扩展过
		}
		for(j=i+1; j<CP_Seg_Array.GetSize(); j++)
		{
			/******wq-20081226-add*****************************/
			if( CP_Seg_Array[i].CP_Array.GetSize() == 0 )
			{
				break;
			}
			if( CP_Seg_Array[j].CP_Array.GetSize() == 0 )
			{
				continue;
			}/**************************************************/

			if(CP_Seg_Array[j].Is_Expanded==1)
			{
				continue;//此片段已被扩展过
			}
			if(Find_Neighbor_Frag(i, j,Rsm.Seq_Arry_G))
			{
				Combine_Frag(i, j);
			//	step++;
			//	Out_CPSeg(step);
//				Out_LCP_Pos();
			}
//			GetMemoryInfo();
		}
		//long endTime=clock();
		//cout<<i<<"<-"<<CP_Seg_Array.GetSize()<<":"<<(endTime-beginTime)<<endl;
	}//合并相邻的碎片	
//	Out_DupCode(Rsm);
	//Test();//输出挖掘结果概况
//	OutputMemoryRecord("","",out_file_path,0,"");
//	OutputMemoryRecord("Out_CpPos","Del_Useless_inf",out_file_path,1,"");
	Del_Useless_inf(Rsm);
//	OutputMemoryRecord("","",out_file_path,0,"");
//	OutputMemoryRecord("Out_CpPos","DelDupLocationItem",out_file_path,1,"");
	DelDupLocationItem();//wq-20081212-去除源程序重复的项
//	OutputMemoryRecord("Out_CpPos","Count_Line",out_file_path,1,"");
	Count_Line();
//	OutputMemoryRecord("Out_CpPos","Out_LCP_Pos",out_file_path,1,"");
//	Out_LCP_Pos();

//	ChangeCpArrayToPairs();
//	Del_Useless_inf(Rsm);
//	Count_Line();
//	Out_LCP_Pos();
//	Out_DupCode(Rsm);
	OutputMemoryRecord("Out_CpPos","End-Out_CpCodePos",out_file_path,1,"");

}


void Out_CpPos::Count_Line()
{
	int i=0, j=0, k=0;
	int Line_Sum=0;
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		if(CP_Seg_Array[i].len < 3)
		{
			continue;
		}
		CArray <CP_SEG, CP_SEG> &ICP_Array=CP_Seg_Array[i].CP_Array;
		for(j=0; j<ICP_Array.GetSize(); j++)
		{
			int index=0;//ICP_Array[j].FN_Hvalue%1000;
			bool Is_have=0;
			for(k=0; k<CP_POS_Array[index].GetSize(); k++)
			{
				if(CP_POS_Array[index][k].filename == ICP_Array[j].filename)
				{
					Is_have=1;
					break;
				}
			}
			if(Is_have==0)
			{
				CP_POS CPP;
				CPP.filename=ICP_Array[j].filename;
				for(int n=0; n<5000; n++)
				{
					CPP.Line_Mark[n]=0;//初始化
				}
				CP_POS_Array[index].Add(CPP);
				k=CP_POS_Array[index].GetSize()-1;
			}
			for(int m=0; m<ICP_Array[j].LPOS.GetLength()/4; m++)
			{
				CString str=ICP_Array[j].LPOS.Mid(m*4,4);
				int line=atoi(str);
				CP_POS_Array[index][k].Line_Mark[line]=1;
			}
		}
	}
	//int countt = 0;
	for(i=0; i<1000; i++)
	{
		for(j=0; j<CP_POS_Array[i].GetSize(); j++)
		{
			//countt++;
			for(k=0; k<5000; k++)
			{
				if(CP_POS_Array[i][j].Line_Mark[k] == 1)
				{
					//countt++;
					Line_Sum++;
				}
			}
		}
	}
	m_cpLineSum = Line_Sum;

	//CString strr;
	//strr.Format("%d",countt);
	//AfxMessageBox(strr);
//	cout<<"重复代码的行数为："<<Line_Sum<<endl;
}

void Out_CpPos::Generate_Hvalue()
{
	int i=0, j=0, k=0;
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		CArray <CP_SEG, CP_SEG> &ICP_Array=CP_Seg_Array[i].CP_Array;
		for(j=0; j<ICP_Array.GetSize(); j++)
		{
			ICP_Array[j].FN_Hvalue = Filename_to_Hvalue(ICP_Array[j].filename);
		}
	}//生成文件路径名的散列值
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		CArray <CP_SEG, CP_SEG> &ICP_Array=CP_Seg_Array[i].CP_Array;
		for(j=0; j<ICP_Array.GetSize(); j++)
		{
			int SEG_index=j;
			int Min_Hvalue=ICP_Array[j].FN_Hvalue;
			for(k=j+1; k<ICP_Array.GetSize(); k++)
			{
				if(ICP_Array[k].FN_Hvalue < Min_Hvalue)
				{
					SEG_index=k;
					Min_Hvalue=ICP_Array[k].FN_Hvalue;				
				}
			}
			ICP_Array.InsertAt(j, ICP_Array[SEG_index]);
			ICP_Array.RemoveAt(SEG_index+1);
		}
	}//按文件路径名散列值的大小排序
}

void Out_CpPos::Sort_CPS()
{
	int longest_index=0;	//最长序列的序号
	int longest_len=0;      //最长序列的长度
	for(int i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		longest_index=i;
		longest_len=CP_Seg_Array[i].len;		
		for(int j=i+1; j<CP_Seg_Array.GetSize(); j++)
		{
			if(CP_Seg_Array[j].len > longest_len)
			{
				longest_index=j;
				longest_len=CP_Seg_Array[j].len;			
			}
		}
		CP_Seg_Array.InsertAt(i,CP_Seg_Array[longest_index]);
		CP_Seg_Array.RemoveAt(longest_index+1);
	}//将碎片由大到小排列（选择分类法）
}

long Out_CpPos::Filename_to_Hvalue(const CString filename)
{
	long int Hsize=1000000000;
	unsigned long h=0, g;
	int len=filename.GetLength();
	if(len<=11)
	{
		return 0;
	}
	CString str=filename.Right(len-11);
    for(int i=0; i<str.GetLength(); i++)
	{
        h = (h << 4)+ str.GetAt(i);
        if(g = h & 0xf0000000)
		{
            h ^= g>>24;
            h ^= g;
        }
    }
	h = h % Hsize;
	return (long)h;
}

bool Out_CpPos::Find_Neighbor_Frag(int F1, int F2,const CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G)
//找出CP_Seg_Array[F1]与CP_Seg_Array[F2]之间有没有相邻的碎片可以合并，
//将可合并的碎片信息存于Con_SEG_Array1和Con_SEG_Array2中
{
//	OutputMemoryRecord("Out_CpPos","Find_Neighbor_Frag",out_file_path,1);
	CArray <CP_SEG, CP_SEG> &FArray1 = CP_Seg_Array[F1].CP_Array;
	CArray <CP_SEG, CP_SEG> &FArray2 = CP_Seg_Array[F2].CP_Array;
	Con_SEG_Array1.RemoveAll();
	Con_SEG_Array2.RemoveAll();//初始化：清空两个数组
	int Gap=0;
	int i=0, j=0;

	if(FArray1.GetSize()<2)
	{
		CP_Seg_Array[F1].Is_Expanded=1;
		return 0;
	}
	if(FArray2.GetSize()<2)
	{
		CP_Seg_Array[F2].Is_Expanded=1;
		return 0;
	}

	for(j=0; j<FArray2.GetSize(); j++)
	{
		FArray2[j].Is_combine=0;
	}//初始化

	for(i=0; i<FArray1.GetSize(); i++)
	{
		for(j=0; j<FArray2.GetSize(); j++)
		{
			if(FArray2[j].Is_combine==1 || FArray1[i].FN_Hvalue > FArray2[j].FN_Hvalue)
			{
				continue;
			}//若两模块出自不同文件
			if(FArray1[i].FN_Hvalue == FArray2[j].FN_Hvalue)
			{
				if(FArray1[i].filename !=  FArray2[j].filename)
				{
					continue;
				}
				/*wq-20081222-如果两个模块不在同一函数范围内，则不合并*/
				else if(FArray1[i].funcBegLine != FArray2[j].funcBegLine//)
						|| FArray1[i].funcEndLine != FArray2[j].funcEndLine)
				{
					continue;
				}
				/**********************************************************/
			}
			if(FArray1[i].FN_Hvalue < FArray2[j].FN_Hvalue)
			{
				break;
			}

			if(FArray1[i].End_index<FArray2[j].Beg_index)
			{
				Gap=FArray2[j].Beg_index-FArray1[i].End_index;
			}
			else if(FArray2[j].End_index<FArray1[i].Beg_index)
			{
				Gap=FArray1[i].Beg_index-FArray2[j].End_index;
			}//计算两碎片所在模块之间的距离Gap
			else
			{
				return 0;//有重叠，不可合并
			}
			if(Gap==1)
			//若两模块出自同一文件，且所属模块相邻
			{
				int Is_Con=Is_Consecutive(FArray1[i],FArray2[j]);

				if(Is_Con != 0)//两碎片间距小于Max_Gap
				{
					Con_SEG CS;
					CS.S1=&FArray1[i];
					CS.S2=&FArray2[j];
					FArray2[j].Is_combine=1;
					if(Is_Con==1)
					{
						Con_SEG_Array1.Add(CS);//存储顺序相联的碎片信息
					}
					if(Is_Con==-1)
					{
						Con_SEG_Array2.Add(CS);//存储逆序相联的碎片信息
					}					
				}
				break;
			}
		}
	}
	if(Con_SEG_Array1.GetSize()<2)
	{
		Con_SEG_Array1.RemoveAll();
	}
	if(Con_SEG_Array2.GetSize()<2)
	{
		Con_SEG_Array2.RemoveAll();
	}//如果能合并的碎片对儿小于二
	if(Con_SEG_Array1.GetSize()>=2 || Con_SEG_Array2.GetSize()>=2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int Out_CpPos::Is_Consecutive(CP_SEG &S1,CP_SEG &S2)
//察看碎片S1，S2是否能够合并：若不能则返回0；否则，若S1在S2前则返回1，S2在S1前则返回-1
{
	int Order=0;
	int Gap=0;
	if(S1.End_index < S2.Beg_index)
	{
		Order=1;
		Gap=S1.E_gap+S2.B_gap;
	}
	if(S2.End_index < S1.Beg_index)
	{
		Order=-1;
		Gap=S2.E_gap+S1.B_gap;
	}
	
	if(Gap>=Max_gap)
	{
		return 0;
	}
	else
	{
		return Order;
	}
}

void Out_CpPos::Combine_Frag(int F1, int F2)//合并两段代码
{
//	OutputMemoryRecord("Out_CpPos","Combine_Frag",out_file_path,1,"");
	CArray <CP_SEG, CP_SEG> &CPA1=CP_Seg_Array[F1].CP_Array;
	CArray <CP_SEG, CP_SEG> &CPA2=CP_Seg_Array[F2].CP_Array;
	class CP_Segment CPS1,CPS2;
	CPS1=Make_Larger_Frag(F1, F2, Con_SEG_Array1);
	CPS2=Make_Larger_Frag(F2, F1, Con_SEG_Array2);
	Con_SEG_Array1.Append(Con_SEG_Array2);

	/***wq-20090715-如果合并后的两个片段有重叠的代码，则取消合并***
	class CP_Segment &CPS = CPS1;
	if( CPS2.seq != "")
	{
		CPS = CPS2;
	}
	int i,j;
	for( i=0; i<CPS.CP_Array.GetSize(); i++ )
	{
		for( j=i+1; j<CPS.CP_Array.GetSize(); j++ )
		{
			if( CPS.CP_Array[i].filename == CPS.CP_Array[j].filename
				&& CPS.CP_Array[i].funcBegLine == CPS.CP_Array[j].funcBegLine)
			{
				CString relaLPOS1 = CPS.CP_Array[i].relaLPOS;
				CString relaLPOS2 = CPS.CP_Array[j].relaLPOS;
				int k;
				for( k=0; k*4 < relaLPOS1.GetLength(); k++ )
				{
					int pos = relaLPOS2.Find(relaLPOS1.Mid(k*4,4),0);
					if( pos%4 == 0 )
					{
						Con_SEG_Array1.RemoveAll();
						Con_SEG_Array2.RemoveAll();
						return;
					}

				}
			}
		}
	}/****20090715****/

	Del_Orig_Frag(F1,1);//wq-20081227-delete
	Del_Orig_Frag(F2,0);//wq-20081227-delete
	/****wq-20081227-add********************************
	if( CP_Seg_Array[F1].sup > CP_Seg_Array[F2].sup )
	{
		Del_Orig_Frag(F2,0);
	}
	else if( CP_Seg_Array[F1].sup < CP_Seg_Array[F2].sup )
	{
		Del_Orig_Frag(F1,1);
	}
	else
	{
		Del_Orig_Frag(F1,1);
		Del_Orig_Frag(F2,0);
	}/***************************************************/
	if(CPS1.seq != "")
	{
		CP_Seg_Array.InsertAt(F1+1,CPS1);
	}
	if(CPS2.seq != "")
	{
		CP_Seg_Array.InsertAt(F1+1,CPS2);
	}
}
/**************************************/
void Out_CpPos::Del_Orig_Frag(int Fi, bool Is_first)//删除合并前的代码
{
//	OutputMemoryRecord("Out_CpPos","Del_Orig_Frag",out_file_path,1,"");
	class CP_Segment &CPS=CP_Seg_Array[Fi];
	CArray <CP_SEG*, CP_SEG*> CS_Array;//这些指针指向将被删除的节点
	int i=0;
	if(Is_first==1)
	{
		for(i=0; i<Con_SEG_Array1.GetSize(); i++)
		{
			CS_Array.Add(Con_SEG_Array1[i].S1);
		}
	}
	else
	{
		for(i=0; i<Con_SEG_Array1.GetSize(); i++)
		{
			CS_Array.Add(Con_SEG_Array1[i].S2);
		}
	}//根据Is_first值的不同，将Con_SEG_Array1中的S1（或S2)全部存入CS_Array

	int len=CPS.len;
	int leavings=CPS.CP_Array.GetSize()-CS_Array.GetSize();
	if(len>Min_len && leavings==1)
	{
		CPS.Is_Expanded=1;//不删除此碎片组合，并将其标识为不可扩展
	}
	else
	{
		for(i=0; i<CS_Array.GetSize(); i++)
		{
			CS_Array[i]->filename=_T("");
		}
		for(i=0; i<CPS.CP_Array.GetSize(); i++)
		{
			if(CPS.CP_Array[i].filename=="")
			{
				CPS.CP_Array.RemoveAt(i);
				i--;
			}
		}
	}//将CS_Array中指针指向的那些节点全部删除
}/***********************/
/***********************
void Out_CpPos::Del_Orig_Frag(int Fi, bool Is_first)//删除合并前的代码
{
	class CP_Segment &CPS=CP_Seg_Array[Fi];
	CArray <CP_SEG*, CP_SEG*> CS_Array;//这些指针指向将被删除的节点
	int i=0;
	if(Is_first==1)
	{
		for(i=0; i<Con_SEG_Array1.GetSize(); i++)
		{
			CS_Array.Add(Con_SEG_Array1[i].S1);
		}
	}
	else
	{
		for(i=0; i<Con_SEG_Array1.GetSize(); i++)
		{
			CS_Array.Add(Con_SEG_Array1[i].S2);
		}
	}//根据Is_first值的不同，将Con_SEG_Array1中的S1（或S2)全部存入CS_Array

	int len=CPS.len;
	int leavings=CPS.CP_Array.GetSize()-CS_Array.GetSize();
//	if(len>Min_len && leavings==1)
	if(len>=1 && leavings==1)//wq-20090310
	{
		CPS.Is_Expanded=1;//不删除此碎片组合，并将其标识为不可扩展
	}
	else
	{

		for(i=0; i<CS_Array.GetSize(); i++)
		{
			CS_Array[i]->combineTimes++;
			if( CPS.CP_Array.GetSize() == 2 
				|| CS_Array[i]->combineTimes >CPS.CP_Array.GetSize() )//wq-20090310///////
			{
				CS_Array[i]->filename=_T("");
			}
		}

		for(i=0; i<CPS.CP_Array.GetSize(); i++)
		{
			if(CPS.CP_Array[i].filename=="")
			{
				CPS.CP_Array.RemoveAt(i);
				i--;
			}
		}
	}//将CS_Array中指针指向的那些节点全部删除

	//wq-20090310
	if(CP_Seg_Array[Fi].sup > 2)
	{
		CP_Seg_Array[Fi].Is_Expanded = 0;
	}/////////////////////
}
/***********************************/
CP_Segment Out_CpPos::Make_Larger_Frag(int F1, int F2, CArray <Con_SEG,Con_SEG> &Con_SEG_Array)
//如果可合并的碎片对数大于等于2，则将CPS1和CPS2中相连的碎片合成新的碎片，加入数组CP_Seg_Array的末尾
{
//	OutputMemoryRecord("Out_CpPos","Make_Larger_Frag",out_file_path,1,"");
	class CP_Segment &CPS1=CP_Seg_Array[F1];
	class CP_Segment &CPS2=CP_Seg_Array[F2];
	class CP_Segment CPS;
	if(Con_SEG_Array.GetSize() >= 2)
	{
		CPS.seq = CPS1.seq+CPS2.seq;
		CPS.len = CPS1.len+CPS2.len;
		CPS.sup = Con_SEG_Array.GetSize();
		CPS.Is_Expanded=0;
		for(int i=0; i<Con_SEG_Array.GetSize(); i++)
		{
			CP_SEG Seg;
			CP_SEG *S1=Con_SEG_Array[i].S1;
			CP_SEG *S2=Con_SEG_Array[i].S2;
			if((S2->End_index) < (S1->Beg_index))
			{
				S1=Con_SEG_Array[i].S2;
				S2=Con_SEG_Array[i].S1;
			}
			Seg.filename=S1->filename;
			Seg.FN_Hvalue=S1->FN_Hvalue;
			Seg.funcBegLine = S1->funcBegLine;//wq-20081226-add
			Seg.funcEndLine = S1->funcEndLine;//wq-20081226-add
			Seg.funcBgnRealLine = S1->funcBgnRealLine;//wq-20090202
			Seg.funcEndRealLine = S1->funcEndRealLine;//ew-20090202
			Seg.LPOS=S1->LPOS+S2->LPOS;
			Seg.relaLPOS = S1->relaLPOS + S2->relaLPOS;
			Seg.Beg_index=S1->Beg_index;
			Seg.End_index=S2->End_index;
			Seg.B_gap=S1->B_gap;
			Seg.E_gap=S2->E_gap;
			CPS.CP_Array.Add(Seg);
		}
		//CP_Seg_Array.Add(CPS);
	}
	return CPS;
}

void Out_CpPos::Del_Useless_inf(Seq_Mining &Rsm)//删除无用的信息，合并一些重复的信息
{
	int i=0,j=0;
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		CP_Seg_Array[i].sup=CP_Seg_Array[i].CP_Array.GetSize();
		if(CP_Seg_Array[i].sup<2)
		{
			CP_Seg_Array.RemoveAt(i);
			i--;
		}
	}//删除无用的碎片
//	OutputMemoryRecord("","",out_file_path,0,"");

	int jj=0;

	OutputMemoryRecord("Out_CpPos","Del_Redun_inf-Begin",out_file_path,1,"");
	int DelTrueCount = 0;//记录Del_Redun_inf()返回1的次数
	int DelFalseCount = 0;//记录Del_Redun_inf()返回0的次数
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		for(j=i+1; j<CP_Seg_Array.GetSize(); j++,jj++)
		{
			if(Del_Redun_inf(i, j, Rsm.Seq_Arry_G)==1)
			{
				DelTrueCount++;
				j--;
			}
			else
			{
				DelFalseCount++;
			}
		}
	}//合并有重复部分的碎片
	CString s_count("");
	CString text("");
	s_count.Format("%d",DelTrueCount);
	text += "( 返回1的次数="+s_count+" , ";

	s_count.Format("%d",DelFalseCount);
	text += "返回0的次数="+s_count+" ) ";

	OutputMemoryRecord("Out_CpPos","Del_Redun_inf-End",out_file_path,1,text);
}

bool Out_CpPos::Del_Redun_inf(int F1, int F2,const CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G)
{
//	OutputMemoryRecord("Out_CpPos","Del_Redun_inf",out_file_path,1,"");
	int i=0;
	if(CP_Seg_Array[F1].sup != CP_Seg_Array[F2].sup)
	{
		return 0;//对应的重复模块数不相等
	}

	CArray <CP_SEG, CP_SEG> &CP_Array1=CP_Seg_Array[F1].CP_Array;
	CArray <CP_SEG, CP_SEG> &CP_Array2=CP_Seg_Array[F2].CP_Array;
	if(CP_Array1.GetSize() != CP_Array2.GetSize())
	{
		return 0;
	}
	CString Seq = _T("");
	CArray <CP_SEG, CP_SEG> CP_Array;
	for(i=0; i<CP_Array1.GetSize(); i++)
	{
		if(CP_Array1[i].filename != CP_Array2[i].filename)
		{
			return 0;//对应的重复模块所在的文件不一致
		}
		else
		{
			//wq-20081121-CP_Array1和CP_Array2相同位置的每对重复代码片段在同一个文件中
			/*wq-20081222-不在同一个函数范围内不合并**/
			if(CP_Array1[i].funcBegLine != CP_Array2[i].funcBegLine//)
				|| CP_Array1[i].funcEndLine != CP_Array2[i].funcEndLine )
			{
				return 0;
			}
			/***************************************/
			
/********wq-20090714-删除此处，仅考虑合并后的密度值**/
			else
			{
				/**wq-20090715-删除完全相同的两组克隆代码集合中的一组**
				if( CP_Array1[i].LPOS == CP_Array2[i].LPOS )
				{
					CP_Seg_Array.RemoveAt(F1);
					return 0;
				}
				else if(CP_Array1[i].LPOS.Find(CP_Array2[i].LPOS,0)>=0)
				{
					CP_Seg_Array.RemoveAt(F2);
					return 0;
				}
				else if(CP_Array2[i].LPOS.Find(CP_Array1[i].LPOS,0)>=0)
				{
					CP_Seg_Array.RemoveAt(F1);
					return 0;
				}/**wq-20090715**/

				//wq-20090705-如果两个克隆代码之间相隔的距离过大，也不合并
				//wq-20090705-记录参与合并的两个序列的相对起始和相对终止位置
				CString seqRelaPos1 = CP_Array1[i].relaLPOS;
				CString seqRelaPos2 = CP_Array2[i].relaLPOS;
				int seqBeg1 = atoi(seqRelaPos1.Mid(0,4));
				int seqBeg2 = atoi(seqRelaPos2.Mid(0,4));
				int seqEnd1 = atoi(seqRelaPos1.Mid(seqRelaPos1.GetLength()-4,4));
				int seqEnd2 = atoi(seqRelaPos2.Mid(seqRelaPos2.GetLength()-4,4));
				if( seqBeg1-seqEnd2>2 || seqBeg2-seqEnd1>2 )
				{
					return 0;
				}
			}
/***********else**********/
			
			CP_SEG CPS;
			CPS.filename = CP_Array1[i].filename;
			CPS.funcBegLine = CP_Array1[i].funcBegLine;//wq-20081222
			CPS.funcEndLine = CP_Array1[i].funcEndLine;//wq-20081222
			CPS.funcBgnRealLine = CP_Array1[i].funcBgnRealLine;//wq-20090202
			CPS.funcEndRealLine = CP_Array1[i].funcEndRealLine;//wq-20090202
			CPS.FN_Hvalue = CP_Array1[i].FN_Hvalue;
			CP_Array.Add(CPS);
		}
	}


	int j=0,k=0;
	int itemNum1 = 0, itemNum2 = 0;//wq-20081121
	int seqNum1 = 0, seqNum2 = 0;//wq-20081121
	int len1=CP_Seg_Array[F1].seq.GetLength()/10;
	int len2=CP_Seg_Array[F2].seq.GetLength()/10;
	
	CString LPOS1,LPOS2,LPOS_1,LPOS_2;


	while(j<len1 && k<len2)
	{
		int Pos1=atoi(CP_Array1[0].LPOS.Mid(j*4,4));
		int Pos2=atoi(CP_Array2[0].LPOS.Mid(k*4,4));

		if(Pos1 <= Pos2)
		{
			Seq = Seq + CP_Seg_Array[F1].seq.Mid(j*10,10);	
			for(i=0; i<CP_Array.GetSize(); i++)
			{
				LPOS1 = CP_Array1[i].LPOS.Mid(j*4,4);
				LPOS_1 = CP_Array[i].LPOS.Right(4);
				CP_Array[i].LPOS = CP_Array[i].LPOS + CP_Array1[i].LPOS.Mid(j*4,4);
				CP_Array[i].relaLPOS += CP_Array1[i].relaLPOS.Mid(j*4,4);
			}
			j++;
			if(Pos1 == Pos2)
			{
				k++;
			}
		}
		else
		{
			Seq = Seq + CP_Seg_Array[F2].seq.Mid(k*10,10);
			for(i=0; i<CP_Array.GetSize(); i++)
			{
				LPOS2 = CP_Array2[i].LPOS.Mid(k*4,4);
				LPOS_2 = CP_Array[i].LPOS.Right(4);
				CP_Array[i].LPOS = CP_Array[i].LPOS + CP_Array2[i].LPOS.Mid(k*4,4);
				CP_Array[i].relaLPOS += CP_Array2[i].relaLPOS.Mid(k*4,4);
			}
			k++;
		}
	}
	if(j<len1)
	{
		int left_len=CP_Seg_Array[F1].seq.GetLength()/10-j;
		Seq = Seq + CP_Seg_Array[F1].seq.Right(left_len*10);
		for(i=0; i<CP_Array.GetSize(); i++)
		{
			CP_Array[i].LPOS = CP_Array[i].LPOS + CP_Array1[i].LPOS.Right(left_len*4);
			CP_Array[i].relaLPOS += CP_Array1[i].relaLPOS.Right(left_len*4);
		}
	}
	if(k<len2)
	{
		int left_len=CP_Seg_Array[F2].seq.GetLength()/10-k;
		Seq = Seq + CP_Seg_Array[F2].seq.Right(left_len*10);
		for(i=0; i<CP_Array.GetSize(); i++)
		{
			CP_Array[i].LPOS = CP_Array[i].LPOS + CP_Array2[i].LPOS.Right(left_len*4);
			CP_Array[i].relaLPOS += CP_Array2[i].relaLPOS.Right(left_len*4);
		}
	}

/***********/
	for( i = 0; i < CP_Array.GetSize(); i++ )
	{//wq-20090705-如果合并后某个克隆代码片段的密度值低于阈值，则合并失败
		double density = ComputeSeqDensity(CP_Array[i].relaLPOS,CP_Array[i].LPOS,CP_Array[i].filename);
		if(density < Min_den)
			return 0;
	}
/***********/

	/**wq-20090715-如果合并后的两个片段有重叠的代码，则合并失败**
	for( i=0; i<CP_Array.GetSize(); i++ )
	{
		for( j=i+1; j<CP_Array.GetSize(); j++ )
		{
			if( CP_Array[i].filename == CP_Array[j].filename
				&& CP_Array[i].funcBegLine == CP_Array[j].funcBegLine)
			{
				CString relaLPOS1 = CP_Array[i].relaLPOS;
				CString relaLPOS2 = CP_Array[j].relaLPOS;
				int k;
				for( k=0; k*4 < relaLPOS1.GetLength(); k++ )
				{
					int pos = relaLPOS2.Find(relaLPOS1.Mid(k*4,4),0);
					if( pos%4 == 0 )
					{
						return 0;
					}

				}
			}
		}
	}/**wq-20090715****/

	CP_Seg_Array[F1].seq=Seq;
//	AfxMessageBox(Seq);
	int FC=0;
/*	for(i=0; i<Seq.GetLength()/10; i++)
	{
		CString Seq_seg=Seq.Mid(i*10,10);
		if(Seq_seg=="      2000" || Seq_seg=="      2032")
		{
			FC++;
		}
	}
	int len = Seq.GetLength()/10;
	len -= FC;*/
	CP_Seg_Array[F1].len=Seq.GetLength()/10;
	CP_Seg_Array[F1].CP_Array.Copy(CP_Array);
	CP_Seg_Array.RemoveAt(F2);
	return 1;
}

void Out_CpPos::Out_CPSeg(int step)//输出每步的合并结果，用于测试！
{
	CString str=_T("");
	for(int i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		CString index;
		index.Format("%d",i);
		str = str + index + ":" + CP_Seg_Array[i].seq + "\r\n";
		for(int j=0; j<CP_Seg_Array[i].CP_Array.GetSize(); j++)
		{
			CString Beg;
			CString End;
			Beg.Format("%d",CP_Seg_Array[i].CP_Array[j].Beg_index);
			End.Format("%d",CP_Seg_Array[i].CP_Array[j].End_index);
			str = str + CP_Seg_Array[i].CP_Array[j].filename + ":"
					  + "(" + Beg + "," + End + ")"
					  + CP_Seg_Array[i].CP_Array[j].LPOS + "\r\n";
		}
		str = str + "\r\n";
	}
	CString tFileName;
	tFileName.Format("%d",step);
	tFileName="D:\\Step_Out\\"+tFileName+".txt";
	CStdioFile Out_CPSC;
	Out_CPSC.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	Out_CPSC.WriteString(str);
	Out_CPSC.Close();
}

void Out_CpPos::Out_LCP_Pos()//输出长度大于Min_len的重复代码位置
{
	CString Out_Str=_T("");
	int CP_count=0;
	int CPLine_Sum=0;
	int cpSegArrySize = CP_Seg_Array.GetSize();
	CString sOutFileNum("");
	CString outFileFullPath("");
	outFileFullPath = out_file_path + "\\CP_OUT";
	::mkdir(outFileFullPath.GetBuffer(outFileFullPath.GetLength()));//
	int i;
	for( i=0; i<cpSegArrySize; i++)
	{
		if( (i+1)/10000 > 0 && (i+1)%10000 == 0 )
		{
			int outFileNum = i/10000;
			sOutFileNum.Format("%d",outFileNum);
			CString fileName = "\\CP_OUT_"+sOutFileNum+".txt";
			outFileFullPath = out_file_path + "\\CP_OUT" + fileName;

			CStdioFile Out_CP;
			CFileException fileException;

			if(Out_CP.Open(outFileFullPath, CFile::modeCreate | CFile::modeWrite, &fileException))
			{
				Out_CP.WriteString(Out_Str);
				Out_CP.Close();
			}
			else
			{
				TRACE( "Can't open file %s, error = %u\n", 
					 CP_OUT_PATH, fileException.m_cause );
			}
			Out_Str = "";

		}
	
		CString str=_T(""),str_temp1=_T(""),str_temp2=_T("");
//		if(CP_Seg_Array[i].len >= Min_len )// && CP_Seg_Array[i].sup == 2)
		{
			class CP_Segment &CPS=CP_Seg_Array[i];
			CString strSeq = CPS.seq;
			int seqlen = strSeq.GetLength();
			str_temp1.Format("%d",CPS.len);
			str_temp2.Format("%d",CPS.sup);
			str="SEQ: " + CPS.seq + "\r\n"+
				"LEN: " + str_temp1 + ";  SUP: " + str_temp2 + "\r\n";
			for(int j=0; j<CPS.CP_Array.GetSize(); j++)
			{
				CString filpath=CPS.CP_Array[j].filename;
				CString strHashedName;
				/*wq-20090202-在LPOS后面加上所在函数的起始终止行数，占8位*/
				CString str_funcBgnLine,str_funcEndLine;
				str_funcBgnLine.Format("%d",CPS.CP_Array[j].funcBgnRealLine);
				while(str_funcBgnLine.GetLength() < 4 )
				{
					str_funcBgnLine = "0" + str_funcBgnLine;
				}
				str_funcEndLine.Format("%d",CPS.CP_Array[j].funcEndRealLine);
				while(str_funcEndLine.GetLength() < 4 )
				{
					str_funcEndLine = "0" + str_funcEndLine;
				}
				strHashedName.Format("%d",CPS.CP_Array[j].FN_Hvalue);
				str=str + "FilePath:" + filpath + " ;\r\n" +
					"HashedFileName:" + strHashedName + "\r\n" +
					"Lines:" + CPS.CP_Array[j].LPOS + "-" + str_funcBgnLine + str_funcEndLine + "\r\n";
				str_funcBgnLine.Format("%d",CPS.CP_Array[j].funcBegLine);
				while(str_funcBgnLine.GetLength() < 4 )
				{
					str_funcBgnLine = "0" + str_funcBgnLine;
				}
				str_funcEndLine.Format("%d",CPS.CP_Array[j].funcEndLine);
				while(str_funcEndLine.GetLength() < 4 )
				{
					str_funcEndLine = "0" + str_funcEndLine;
				}
				str=str + //"FilePath:" + filpath + " ;\r\n" +
					"RELAL:" + CPS.CP_Array[j].relaLPOS+ "-" + str_funcBgnLine + str_funcEndLine  + "\r\n";

			}
			Out_Str = Out_Str + str + "\r\n";
			CP_count++;
		}
		if(CP_Seg_Array[i].len >= 3)
		{
			CPLine_Sum = CPLine_Sum + CP_Seg_Array[i].len*CP_Seg_Array[i].sup;
		}
	}
//	cout<<"长度大于等于"<<Min_len<<"的重复代码共有："<<CP_count<<"组"<<endl;
	//cout<<"重复代码行数为："<<CPLine_Sum<<endl;
	//cout<<Out_Str;
	CStdioFile Out_CP;
	CFileException fileException;

//	if(Out_CP.Open("D:\\CP_OUT.txt", CFile::modeCreate | CFile::modeWrite))

	if( !(i/10000 > 0 && i%10000 == 0) )//克隆代码集合的个数不是10000的整数倍
	{
		int outFileNum = i/10000+1;
		sOutFileNum.Format("%d",outFileNum);
		CString fileName = "\\CP_OUT_"+sOutFileNum+".txt";
		outFileFullPath = out_file_path + "\\CP_OUT" + fileName;
		if(!Out_CP.Open(outFileFullPath, CFile::modeRead, &fileException))
		{
			if(Out_CP.Open(outFileFullPath, CFile::modeCreate | CFile::modeWrite, &fileException))
			{

				Out_CP.WriteString(Out_Str);
				Out_CP.Close();
			}
			else
			{
				TRACE( "Can't open file %s, error = %u\n", 
					 CP_OUT_PATH, fileException.m_cause );
			}
		}
	}
}

void Out_CpPos::Test()//测试函数
{
	int Sup[500];
	int Len[500];
	int i=0;
	for(i=0; i<500; i++)
	{
		Sup[i]=0;
		Len[i]=0;
	}
	for(i=0; i<CP_Seg_Array.GetSize(); i++)
	{
		Sup[CP_Seg_Array[i].sup]++;
		Len[CP_Seg_Array[i].len]++;
	}
	for(i=0; i<10; i++)
	{
		if(Sup[i]!=0)
		{
			cout<<"Sup"<<i<<":"<<Sup[i]<<endl;
		}
	}
	for(i=0; i<10; i++)
	{
		if(Len[i]!=0)
		{
			cout<<"Len"<<i<<":"<<Len[i]<<endl;
		}
	}
	cout<<"剪枝后剩余："<<CP_Seg_Array.GetSize()<<endl;
}
/*-wq-20081212****
//wq-20081123
void Out_CpPos::BuildIdsPlaceSets(class Seq_Mining &Rsm)
{
	int i;
	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		CP_Seg_Array[i].FormIdsPlaceSet(Rsm.Seq_Arry_G);
	}
	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		if(CP_Seg_Array[i].len >= Min_len)
		{
			CP_Seg_Array[i].OutputIdsPlaceSet(i);
//			CP_Seg_Array[i].CpSegIdsMappingSet(Rsm.Seq_Arry_G);
		}
	}
/*	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		if(CP_Seg_Array[i].len >= Min_len)
		{
			CP_Seg_Array[i].OutputIdMappingSet(i);
		}
	}*
}
/**********************************/
//wq-20081227-去除行数重复的项的信息，只保留第一次出现的位置
void Out_CpPos::DelDupLocationItem()
{
	int i;
	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		int j;
		for( j = 0; j < CP_Seg_Array[i].CP_Array.GetSize(); j++ )
		{
			CP_SEG &cpSeg = CP_Seg_Array[i].CP_Array[j];
			CString relaLPOS = cpSeg.relaLPOS;
			int k;
			for( k = 0; k < relaLPOS.GetLength()/4; k++ )
			{
				CString strLine = relaLPOS.Mid(k*4,4);
				int dupPos = relaLPOS.Find(strLine,(k+1)*4);
				if( dupPos != -1 && dupPos%4 == 0)
				{
					if( k+1 < relaLPOS.GetLength()/4 )
					{
						CString strNextLine = relaLPOS.Mid((k+1)*4,4);
						int line = atoi(strLine);
						int nextLine = atoi(strNextLine);
						if( line >= nextLine )
						{
							relaLPOS.Delete(k*4,4);
							CP_Seg_Array[i].seq.Delete(k*10,10);
							CP_Seg_Array[i].len--;
							int jj;
							for( jj = 0; jj < CP_Seg_Array[i].CP_Array.GetSize(); jj++ )
							{
								CP_SEG &cpSeg2 = CP_Seg_Array[i].CP_Array[jj];
								cpSeg2.LPOS.Delete(k*4,4);
								cpSeg2.relaLPOS.Delete(k*4,4);
//								OutputMemoryRecord("Out_CpPos","Del_Redun_inf",out_file_path,1,"");
							}
							k--;
							continue;
						}
					}
					int pos = dupPos/4;
					relaLPOS.Delete(pos*4,4);
					CP_Seg_Array[i].seq.Delete(pos/4*10,10);
					CP_Seg_Array[i].len--;
					int jj;
					for( jj = 0; jj < CP_Seg_Array[i].CP_Array.GetSize(); jj++ )
					{
						CP_SEG &cpSeg2 = CP_Seg_Array[i].CP_Array[jj];
						cpSeg2.LPOS.Delete(pos*4,4);
						cpSeg2.relaLPOS.Delete(pos*4,4);
					}
					k--;
				}
			}
		}
	}
}
//wq-20081212-去除源程序前后位置颠倒的项
void Out_CpPos::DelReverseItem(CP_Segment &CP_Seg)
{
	CArray<CP_SEG,CP_SEG> &CP_Array = CP_Seg.CP_Array;
	CString &seq = CP_Seg.seq;
	int &len = CP_Seg.len;
	int flag = 1;
	int i;
	while( flag == 1 )
	{
		flag = 0;
		for( i = 0; i < CP_Array.GetSize(); i++)
		{
			int prePos = atoi(CP_Array[i].LPOS.Mid(0,4));
			int pos = atoi(CP_Array[i].LPOS.Mid(4,4));
			int j;
			for( j = 2; j < CP_Array[i].LPOS.GetLength()/4; j++ )
			{
				int aftPos = atoi(CP_Array[i].LPOS.Mid(j*4,4));
				if( (pos < prePos && pos < aftPos) 
					|| (pos > prePos && pos > aftPos ))
				{
					DelItem(CP_Seg,j-1);
					flag = 1;
					j--;
					pos = aftPos;
				}
				else
				{
					prePos = pos;
					pos = aftPos;
				}
			}
		}
	}
	CString strPreItem,strPrePos,strItem,strPos;
	for( i = 0; i < CP_Array.GetSize(); i++)
	{
		strPreItem = seq.Mid(0,10);
		strPrePos = CP_Array[i].LPOS.Mid(0,4);
		int k;
		for( k = 1; k < len; k++ )
		{
			strItem = seq.Mid(k*10,10);
			strPos = CP_Array[i].LPOS.Mid(k*4,4);
			if( strPreItem == strItem && strPrePos == strPos )
			{
				DelItem(CP_Seg,k);
				k--;
			}
			else
			{
				strPreItem = strItem;
				strPrePos = strPos;
			}

		}
	}

}
void Out_CpPos::DelItem(CP_Segment &CP_Seg,int j)
{
	CArray<CP_SEG,CP_SEG> &CP_Array = CP_Seg.CP_Array;
	CString &seq = CP_Seg.seq;
	int &len = CP_Seg.len;
	int k;
	for( k = 0; k < CP_Array.GetSize(); k++ )
	{
		CP_Array[k].LPOS.Delete(j*4,4);
		CP_Array[k].relaLPOS.Delete(j*4,4);
/*		int m,n = 0;
		int itemNum = j;
		for( m = 0; m < CP_Array[k].dupCodeLocArry.GetSize(); m++ )
		{
			n = CP_Array[k].dupCodeLocArry[m].itemNumArry.GetSize();
			if(n <= itemNum)
			{
				itemNum -= n;
			}
			else
			{
				CP_Array[k].dupCodeLocArry[m].itemNumArry.RemoveAt(itemNum);
				if(CP_Array[k].dupCodeLocArry[m].itemNumArry.GetSize() == 0)
				{
					CP_Array[k].dupCodeLocArry.RemoveAt(m);
				}
				break;
			}
		}*/

	}
	
	seq.Delete(j*10,10);
	len--;
}
//wq-20081227-add-将片段数量大于3的重复代码片段组中的重复代码片段两两组合成新的重复代码片段组(每个含有两个片段)
void Out_CpPos::ChangeCpArrayToPairs()
{
	CArray <CP_Segment, CP_Segment> CP_Seg_Array2;
	CP_Seg_Array2.RemoveAll();
	int i;
	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		if( CP_Seg_Array[i].sup == 2 )
		{
			CP_Seg_Array2.Add( CP_Seg_Array[i] );
			continue;
		}
		else if( CP_Seg_Array[i].sup > 2 )
		{
			CP_Segment& orgCpSegment = CP_Seg_Array[i];
			int j;
			for( j = 0; j < orgCpSegment.CP_Array.GetSize(); j++ )
			{
				int k;
				for( k = j+1 ; k < orgCpSegment.CP_Array.GetSize(); k++ )
				{
					CP_Segment newCpSegment;
					newCpSegment.CP_Array.Add(orgCpSegment.CP_Array[j]);
					newCpSegment.CP_Array.Add(orgCpSegment.CP_Array[k]);
					newCpSegment.Is_Expanded = orgCpSegment.Is_Expanded;
					newCpSegment.len = orgCpSegment.len;
					newCpSegment.seq = orgCpSegment.seq;
					newCpSegment.sup = 2;
					CP_Seg_Array2.Add(newCpSegment);
				}
			}
			continue;
		}
		else
		{
			AfxMessageBox("error(Out_CpPos::ChangeCpArrayToPairs()):重复代码片段数量少于2");
		}
	}
	CP_Seg_Array.RemoveAll();
	for( i = 0; i < CP_Seg_Array2.GetSize(); i++ )
	{
		CP_Seg_Array.Add(CP_Seg_Array2[i]);
	}
}

void Out_CpPos::Clean()
{
	
	Con_SEG_Array1.RemoveAll();
	Con_SEG_Array2.RemoveAll();
	CP_OUT_PATH = "D:\\CP_OUT.txt";
	CP_Seg_Array.RemoveAll();
	int i;
	for( i = 0; i < 1000; i++ )
	{
		CP_POS_Array[i].RemoveAll();
	}
	CP_Useless_Frag.RemoveAll();

}

bool Out_CpPos::RebuildFormFile(CString fileName)
{
	Clean();
	CStdioFile file;
    CFileException fileException;
	if(file.Open(fileName,CFile::modeRead,&fileException))
	{
		CString strLine;
		while( file.ReadString(strLine))
		{
			if( strLine.Find(SEGBEGIN) != -1 )
			{
				CP_Segment cpSegment;

				ASSERT(GetSeqFromStrLine(strLine,cpSegment));

				file.ReadString(strLine);
				ASSERT(GetSupFromStrLine(strLine,cpSegment));

				int i;
				for( i = 0; i < cpSegment.sup; i++ )
				{
					CP_SEG cpSeg;

					file.ReadString(strLine);
					ASSERT(GetFilePathFromStrLine(strLine,cpSeg));

					file.ReadString(strLine);
					ASSERT(GetHashedNameFromStrLine(strLine,cpSeg));

					file.ReadString(strLine);
					ASSERT(GetLinesFromStrLine(strLine,cpSeg));

					file.ReadString(strLine);
					ASSERT(GetRelaLinsFromStrLine(strLine,cpSeg));

					cpSegment.CP_Array.Add(cpSeg);
				}//for

				CP_Seg_Array.Add(cpSegment);

			}//if( strLine.Find(SEGBEGIN) != -1 )
		}//while( file.ReadString(strLine))
		if(CP_Seg_Array.GetSize() != 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		TRACE(" Can't open file &s, error = %u\n",
			fileName, fileException.m_cause);
		return TRUE;
	}

}

bool Out_CpPos::GetSeqFromStrLine(const CString& strLine,CP_Segment& cpSegment)
{
	int seqBeg = SEGBEGIN.GetLength();
	cpSegment.seq = strLine.Mid(seqBeg,strLine.GetLength()-seqBeg-1);
	cpSegment.len = cpSegment.seq.GetLength()/10;
	if( cpSegment.len > 0 )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

bool Out_CpPos::GetSupFromStrLine(const CString& strLine,CP_Segment& cpSegment)
{
	int supBeg = strLine.Find(SEQSUP) + SEQSUP.GetLength();
	CString strSup = strLine.Mid( supBeg,strLine.GetLength()-supBeg-1 );
	cpSegment.sup = FindNumberFromString(strSup);
	if( cpSegment.sup >= 2 )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
bool Out_CpPos::GetFilePathFromStrLine(const CString& strLine,CP_SEG& cpSeg)
{
	int filePathBeg = strLine.Find(FILEPATH) + FILEPATH.GetLength();
	cpSeg.filename = strLine.Mid(filePathBeg,strLine.GetLength()-filePathBeg-3);
	if( cpSeg.filename != "" )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

bool Out_CpPos::GetHashedNameFromStrLine(const CString& strLine,CP_SEG& cpSeg)
{
	int hashNameBeg = strLine.Find(HASHEDFILENAME) + HASHEDFILENAME.GetLength();
	CString strHashedName = strLine.Mid(hashNameBeg, strLine.GetLength()-hashNameBeg-1);
	cpSeg.FN_Hvalue = FindNumberFromString(strHashedName);
	if( cpSeg.FN_Hvalue > 0 )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

bool Out_CpPos::GetLinesFromStrLine(const CString& strLine,CP_SEG& cpSeg)
{
	int linesBeg = strLine.Find(LINES) + LINES.GetLength();
	cpSeg.LPOS = strLine.Mid(linesBeg,strLine.GetLength()-linesBeg-1-9);
	CString s_realFuncBgnLine = strLine.Mid(strLine.GetLength()-9,4);
	CString s_realFuncEndLine = strLine.Mid(strLine.GetLength()-5,4);
	cpSeg.funcBgnRealLine = atoi(s_realFuncBgnLine);
	cpSeg.funcEndRealLine = atoi(s_realFuncEndLine);
	if( cpSeg.LPOS != "" )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
bool Out_CpPos::GetRelaLinsFromStrLine(const CString& strLine,CP_SEG& cpSeg)
{
	int relaLinesBeg = strLine.Find(RELALINE) + RELALINE.GetLength();
	cpSeg.relaLPOS = strLine.Mid(relaLinesBeg,strLine.GetLength()-relaLinesBeg-1-9);
	CString s_FuncBgnLine = strLine.Mid(strLine.GetLength()-9,4);
	CString s_FuncEndLine = strLine.Mid(strLine.GetLength()-5,4);
	cpSeg.funcBegLine = atoi(s_FuncBgnLine);
	cpSeg.funcEndLine = atoi(s_FuncEndLine);
	if( cpSeg.relaLPOS != "" )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

int Out_CpPos::FindNumberFromString(const CString& str)
{
	int i;
	int charSum = 0;
	CString strNum = "";
	for( i = 0; i < str.GetLength(); i++ )
	{
		char c = str.GetAt(i);
		if( c >= '0' && c <= '9' )
		{
			charSum++;
			strNum += c;
		}
		else if( charSum != 0 )
		{
			break;
		}
	}
	if( strNum != "" )
	{
		return(atoi(strNum));
	}
	else
	{
		return -1;
	}
}

//wq-20090223
void Out_CpPos::UpdateIdsLocArrays(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry)
{
	int i;
	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		CP_Segment &cpSegment = CP_Seg_Array[i];
		if( cpSegment.seq.GetLength() >= Min_len*10 )
		{
			cpSegment.UpdateIdsLocArrayOfCpSegs(headerFilesArry);
		}
	}
}

//wq-20090705-计算序列的密度
//输入：序列对应克隆代码的相对位置行数字符串
//输出：密度(double)
double Out_CpPos::ComputeSeqDensity(const CString &relaLPOS,const CString &LPOS,const CString &filename)
{
	CArray<CString,CString> srcFileList;
	CStdioFile file;
	if( file.Open(filename,CFile::modeRead) )
	{
		CString strLine;
		while(file.ReadString(strLine))
		{
			srcFileList.Add(strLine);
		}
	}

	double density = 0.0;
	int posBeg,posEnd,lineBeg,lineEnd;
	int len = relaLPOS.GetLength()/4;
	int sumLen = len;
	int i;
	if( len>0 )
	{
		posBeg = atoi(relaLPOS.Mid(0,4));
		lineBeg = atoi(LPOS.Mid(0,4));
		posEnd = atoi(relaLPOS.Mid(len*4-4,4));
		lineEnd = atoi(LPOS.Mid(len*4-4,4));
		for( i = 0; i < len; i++ )
		{
			int pos = atoi(relaLPOS.Mid(i*4,4));
			int line = atoi(LPOS.Mid(i*4,4));
			if( pos < posBeg )
			{
				posBeg = pos;
			}
			if( pos > posEnd )
			{
				posEnd = pos;
			}
			if( line < lineBeg )
			{
				lineBeg = line;
			}
			if( line > lineEnd )
			{
				lineEnd = line;
			}
		}
		int base = posEnd-posBeg;

		for( i = lineBeg-1; i<lineEnd; i++ )
		{
			if( i>=0 && i<srcFileList.GetSize() && srcFileList[i].Find("{",0) >= 0 ||srcFileList[i].Find("}",0) >= 0)
			{
/*				CString strLine("");
				strLine.Format("%d",i);
				int l = strLine.GetLength();
				for( ; l <4; l++ )
				{
					strLine = "0" + strLine;
				}*/
//				if( !relaLPOS.Find(strLine) )
				{
					sumLen++;
				}
			}
		}
		if( base > 0)
		{
//			density = double(len)/base;
			density = double(sumLen)/base;
		}
		else if( base == 0 )
		{
			density = 1.0;
		}
	}

	return density;
}
