// SDGContinue.h: interface for the CSDGContinue class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGCONTINUE_H__FE652771_A6ED_4F9D_9B9F_FFB374BA19A5__INCLUDED_)
#define AFX_SDGCONTINUE_H__FE652771_A6ED_4F9D_9B9F_FFB374BA19A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGContinue : public CSDGBase  
{
public:
	CSDGContinue();
	virtual ~CSDGContinue();
public:
	int CPOS;
	CSDGBase *pTO;

};

#endif // !defined(AFX_SDGCONTINUE_H__FE652771_A6ED_4F9D_9B9F_FFB374BA19A5__INCLUDED_)
