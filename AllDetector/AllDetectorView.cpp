
// AllDetectorView.cpp : implementation of the CAllDetectorView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "AllDetector.h"
#endif

#include "AllDetectorDoc.h"
#include "AllDetectorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAllDetectorView

IMPLEMENT_DYNCREATE(CAllDetectorView, CView)

BEGIN_MESSAGE_MAP(CAllDetectorView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CAllDetectorView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CAllDetectorView construction/destruction

CAllDetectorView::CAllDetectorView()
{
	// TODO: add construction code here

}

CAllDetectorView::~CAllDetectorView()
{
}

BOOL CAllDetectorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CAllDetectorView drawing

void CAllDetectorView::OnDraw(CDC* /*pDC*/)
{
	CAllDetectorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CAllDetectorView printing


void CAllDetectorView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CAllDetectorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CAllDetectorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CAllDetectorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CAllDetectorView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CAllDetectorView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CAllDetectorView diagnostics

#ifdef _DEBUG
void CAllDetectorView::AssertValid() const
{
	CView::AssertValid();
}

void CAllDetectorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CAllDetectorDoc* CAllDetectorView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CAllDetectorDoc)));
	return (CAllDetectorDoc*)m_pDocument;
}
#endif //_DEBUG


// CAllDetectorView message handlers
