// FormatTokenLines.h: interface for the FormatTokenLines class.
//
//////////////////////////////////////////////////////////////////////
/*==========================================================================
 * 作者：王倩   2008-07-25
 *
 * 模块功能：语句换行标准化
 *
 * 标准的语句结束标志：
 *     1 ;
 *     2 )
 * 	   3 );
 *	   4 };
 * 非标准的语句换行：
 *     1 statement{
 *     2 statement}
 *	   过滤{和} (1)函数体界限
 *	            (2)控制结构体的界限
 *	   	  	    (3)不包含结构体、联合体、枚举类型的定义和数组的初始化{};
 *	   3 单条语句多行表示        
 *		 解决方法：
 *       (1) 通过配对的()找到语句的结束位置
 *		 (2) ??结构体、联合体、枚举类型的定义, 数组的初始化 {};
 *	   4 多条语句单行表示：
 *		 解决方法：分行识别存储
 *		 (1) '()','{};'的配对
 *		 (2) 查找结束符';'
 *???  switch语句的处理有待商榷
 ==============================================================================*/
#if !defined(AFX_FORMATTOKENLINES_H__4B616297_0C6F_4919_900A_071D1902744A__INCLUDED_)
#define AFX_FORMATTOKENLINES_H__4B616297_0C6F_4919_900A_071D1902744A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"

class FormatTokenLines  
{
public:
	FormatTokenLines();
	virtual ~FormatTokenLines();
	void operator ()(CArray<TNODE,TNODE> &TArry,CArray<SRCLINE,SRCLINE> &srcCode);

private:	
	void OutputSrcToFile(CArray<TNODE,TNODE>& TArry);//(CArray<SRCLINE,SRCLINE> &srcCode)

	/*
	找到基本语句的结束位置（找到非赋值语句中的“{”或“；”，则返回true）
	ix:		从ix开始搜索的token流TArry
	lineEnd:记录返回找到“{”的前一个token序号或者“；”在Tarry中的位置
	TArry:	token流
	*/
	BOOL FindLine   ( int ix, int &lineEnd ,CArray<TNODE,TNODE> &TArry);

	/*
	查找非结构体定义和数组赋值语句中的“{”
	ix:		从ix开始搜索的token流TArry
	lineEnd:记录返回找到“{”的前一个token在TArry中的位置
	TArry:	token流
	*/
	BOOL FindLFlower( int ix, int &lineEnd ,CArray<TNODE,TNODE> &TArry);
	BOOL FindColon  ( int ix, int &lineEnd ,CArray<TNODE,TNODE> &TArry);

	/*
	从ix查找最外层配对的“（”和“）”
	ix:		从ix开始搜索的token流TArry
	lineEnd:记录最外层“）”在TArry中的位置
	TArry:	token流
	*/
	BOOL FindMatchedCircles(int ix,int &lineEnd,CArray<TNODE,TNODE> &TArry);

	/*
	形成标准化基本语句行，将TArry从lineBeg倒lineEnd范围内元素存入tempLine中，
	并将tempLine中所有元素的行数line项更新为lineCout
	lineCount:	行号
	lineBeg:	基本语句行的开始token在TArry中的位置
	lineEnd:	基本语句行的结束token在TArry中的位置
	TArry:		token流
	tempLine:	记录新形成的基本语句行
	srcCode:	无用
	*/
	int FormALine(int lineCount,int lineBeg,int lineEnd,
		          CArray<TNODE,TNODE> &TArry,
				  CArray<TNODE,TNODE>       &tempLine,
				  CArray<SRCLINE,SRCLINE> &srcCode);

	/*
	扫描TArry时跳过switch（）控制体内的语句；int返回switch结束的下一个token的位置
	ix:		从ix开始搜索的token流TArry
	TArry:	token流
	*/
	int DeleteSwitch( int ix, CArray<TNODE,TNODE> &TArry);

};

#endif // !defined(AFX_FORMATTOKENLINES_H__4B616297_0C6F_4919_900A_071D1902744A__INCLUDED_)
