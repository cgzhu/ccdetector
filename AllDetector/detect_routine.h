/********************************************************************
	created:	2011/02/11
	created:	11:2:2011   10:42
	filename: 	D:\BugFinder\BugFinder\detect_routine.h
	file path:	D:\BugFinder\BugFinder
	file base:	detect_routine
	file ext:	h
	author:		qj
	
	purpose:	检测函数
*********************************************************************/
void freeNode(treeNode *);
//void ex(treeNode *);	//遍历语法树
void processTree(treeNode *);	//
void mergeSubNode(treeNode *);  //合并子树为一个节点
string mergeSubNodeContent(treeNode *p);	//只合并子树内容到根节点，不改变树结构，返回该内容
string firstIdExtract(treeNode *p); //提取子树中的最左边的标识符（深度遍历）
int exCountNode(treeNode *p);	//返回子树的节点个数
bool exSelecStm(treeNode *p, string str); //遍历子树查找是否存在str，存在返回true

bool compare_node(treeNode *p1, treeNode *p2); //比较两节点是否为同一节点，同一节点返回true
void jump_node(int n); //跳过栈stack_cds中的n个节点
int line_of_node(treeNode *p); //返回p节点在源代码中的行号

void ideOperDect(treeNode *p);	//幂等缺陷检测
void reduAssignDect(treeNode *p);	//冗余赋值缺陷检测
void deadCodeDect(treeNode *p);	//死代码检测
void reduCondDect(treeNode *p);	//冗余条件表达式检测
void hideOperDect(treeNode *p);	//隐式幂等表达式检测 gdd
void reduParameter(treeNode *p);

void reduCondDect_2(treeNode *p);

bool front_use(treeNode *p, treeNode *pp,string str); //gdd
bool lable_use_str(treeNode *p,treeNode *front_p,treeNode *end_p,string str);//gdd
bool later_lable(treeNode *p, treeNode *pp,treeNode *endp,string str);//gdd
bool lable_use(treeNode *p, treeNode *pp,treeNode *endp,string str);//gdd
bool exSelecStr(treeNode *p, string str); //gdd遍历子树查找是否存在str，存在返回true
bool exSelecStrn(treeNode *p, string str); //gdd遍历子树查找是否存在str，存在返回true
bool global_value(treeNode *p,string str); //gdd
bool extr_use(treeNode *p,string str); //gdd
bool in_iter_stm(treeNode *p,int flag);//gdd 判断结点temp_node是否在循环结构中，如果在，判断选择结构外，循环结构中是否对结点temp_node中的变量重新赋值
bool re_assin(treeNode *p, treeNode *pp,int flag);//gdd
bool exist(treeNode *p, treeNode *pp);//gdd
bool chang_value(treeNode *p);//gdd
bool add_in_lable(treeNode *p);//gdd
bool in_lable_stm(treeNode *p);//gdd
bool later_in_lable(treeNode *p,string str);//gdd