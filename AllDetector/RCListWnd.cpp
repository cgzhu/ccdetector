// RCListWnd.cpp : 实现文件
//

/*
 * 这个类实现一个显示冗余代码信息的列表停靠窗口
 */

#include "stdafx.h"
#include "AllDetector.h"
#include "RCListWnd.h"

using namespace std;
// CRCListWnd

IMPLEMENT_DYNAMIC(CRCListWnd, CDockablePane)

CRCListWnd::CRCListWnd()
{

}

CRCListWnd::~CRCListWnd()
{
}


BEGIN_MESSAGE_MAP(CRCListWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// CRCListWnd 消息处理程序




int CRCListWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码

	//Polaris-20141008
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	if (!RCList.Create(LVS_REPORT,CRect(0, 0, 800, 800),this,77))
	{
		TRACE0("Failed to create file view\n");
		return -1;      // fail to create
	}
	DWORD dwStyle = RCList.GetExtendedStyle();   //添加列表框的网格线
    dwStyle |= LVS_EX_FULLROWSELECT;            
    dwStyle |= LVS_EX_GRIDLINES;                
    RCList.SetExtendedStyle(dwStyle);

	//AfxMessageBox("aa");

	RCList.DeleteAllItems();
	CRect mRect;
	RCList.GetWindowRect(&mRect);
	RCList.InsertColumn(0, _T("文件名"), LVCFMT_CENTER, 200, 0);
	RCList.InsertColumn(1, _T("行数", LVCFMT_CENTER, 200, 1));
	RCList.InsertColumn(2, _T("行"), LVCFMT_CENTER, 200, 2);
	RCList.InsertColumn(3, _T("描述"), LVCFMT_CENTER, 200, 3);

	AdjustLayout();

	return 0;
}

/*
 * 函数功能：填充冗余信息列表
 */
void CRCListWnd::FillRCList(vector<vector<CString>> rc_info_list)
{
	int elem_count = rc_info_list.size();
	for(int i=0;i<elem_count;i++)
	{
		RCList.InsertItem(i,_T(""));
		for(int j=0;j<4;j++)
		{
			RCList.SetItemText(i,j,rc_info_list[i][j]);
		}
	}
}

/*
 * 覆盖绘制函数
 */
void CRCListWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rectTree;
	RCList.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

/*
 * 覆盖焦点消息函数
 */
void CRCListWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

	RCList.SetFocus();
}

void CRCListWnd::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	RCList.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + 1, rectClient.Width() - 2, rectClient.Height() - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CRCListWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	AdjustLayout();
}
