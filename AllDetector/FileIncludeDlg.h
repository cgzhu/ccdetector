#pragma once
#include "afxwin.h"

// CFileIncludeDlg 对话框

class CFileIncludeDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CFileIncludeDlg)

public:
	CFileIncludeDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CFileIncludeDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

protected:
	// 目录
	CEdit m_tbDir;
	CListBox m_wndListBox;
	CButton m_btnAdd;
	CButton m_btnDelete;
public:
	afx_msg void OnClickedButton1();
	afx_msg void OnClickedButton2();
	afx_msg void OnClickedButton3();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
