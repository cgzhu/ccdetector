// FNODE.cpp: implementation of the FNODE class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllDetector.h"
#include "FNODE.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FNODE::FNODE()
{
	bbeg = -1;
	bend = -1;
	callednum = -1;
	int i;
	for( i = 0; i < 20; i++ )
	{
		calleelist[i] = -1;
	}
	calleenum = -1;
	expanded = -1;
	fid = -1;
	name = "";
	pbeg = -1;
	pend = -1;
	for( i = 0; i < 20; i++ )
	{
		plist[i] = -1;
	}
	pnum = -1;
	rkind = -1;
	rtype = -1;

}

FNODE::~FNODE()
{

}

FNODE::FNODE(const FNODE& FN)
{
	bbeg = FN.bbeg;
	bend = FN.bend;
	callednum = FN.callednum;
	int i;
	for( i = 0; i < 20; i++ )
	{
		calleelist[i] = FN.calleelist[i];
	}
	calleenum = FN.calleenum;
	expanded = FN.expanded;
	fid = FN.fid;
	name = FN.name;
	pbeg = FN.pbeg;
	pend = FN.pend;
	for( i = 0; i < 20; i++ )
	{
		plist[i] = FN.plist[i];
	}
	pnum = FN.pnum;
	rkind = FN.rkind;
	rtype = FN.rtype;
}
FNODE FNODE::operator=(const FNODE& FN)
{
	bbeg = FN.bbeg;
	bend = FN.bend;
	callednum = FN.callednum;
	int i;
	for( i = 0; i < 20; i++ )
	{
		calleelist[i] = FN.calleelist[i];
	}
	calleenum = FN.calleenum;
	expanded = FN.expanded;
	fid = FN.fid;
	name = FN.name;
	pbeg = FN.pbeg;
	pend = FN.pend;
	for( i = 0; i < 20; i++ )
	{
		plist[i] = FN.plist[i];
	}
	pnum = FN.pnum;
	rkind = FN.rkind;
	rtype = FN.rtype;
	return *this;
}
