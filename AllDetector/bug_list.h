/********************************************************************
	created:	2011/02/07
	created:	7:2:2011   16:49
	filename: 	D:\BugFinder\BugFinder\bug_list.h
	file path:	D:\BugFinder\BugFinder
	file base:	bug_list
	file ext:	h
	author:		qj
	
	purpose:	bug list
*********************************************************************/
#pragma once
typedef struct {
	int line;//行号
	CString filename;//文件名
	int bug_type;//bug类型
	CString info;//提示语
} BUG_LIST_ITEM;

class BugList
{
protected:
	vector<BUG_LIST_ITEM> m_list;
public:
	DWORD elapsed_time; // 耗时

public:
	void add(int line,LPCTSTR filename,int bug_type,CString& info);
	BUG_LIST_ITEM* get(int index);
	int size();
	void BugList::clearAll(); 
	void BugList::clear(CString filename);// 清除某个文件的错误记录
};