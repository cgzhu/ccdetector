//Polaris-20140711
/*
 * 类名：CCFileView
 * 作用：仿照系统提供的FileView，自己实现一个专门显示克隆代码文件分布的CCFileView停靠窗口
 */
// CFileView.cpp : implementation file
//

#include "stdafx.h"
#include "mainfrm.h"
#include "CFileView.h"
#include "Resource.h"
#include "AllDetector.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileView

CCFileView::CCFileView()
{
}

CCFileView::~CCFileView()
{
}

BEGIN_MESSAGE_MAP(CCFileView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_PROPERTIES, OnProperties)
	ON_COMMAND(ID_OPEN, OnFileOpen)
	ON_COMMAND(ID_OPEN_WITH, OnFileOpenWith)
	ON_COMMAND(ID_DUMMY_COMPILE, OnDummyCompile)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar message handlers

int CCFileView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create view:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS;

	if (!m_wndCCFileView.Create(dwViewStyle, rectDummy, this, 4))
	{
		TRACE0("Failed to create file view\n");
		return -1;      // fail to create
	}

	//
	m_imgList.Create(IDB_CC_TREE,16,1,RGB(255,255,255) );
	m_wndCCFileView.SetImageList (&m_imgList,TVSIL_NORMAL );    
	m_hCTree = m_wndCCFileView.InsertItem(TREE_ITEMS[0],0,1) ; 

	// Load view images:
	m_FileViewImages.Create(IDB_CCFILE_VIEW, 16, 0, RGB(255, 0, 255));
	m_wndCCFileView.SetImageList(&m_FileViewImages, TVSIL_NORMAL);

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_EXPLORER);
	m_wndToolBar.LoadToolBar(IDR_EXPLORER, 0, 0, TRUE /* Is locked */);

	OnChangeVisualStyle();

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	// Fill in some static tree view data (dummy code, nothing magic here)
	//FillFileView();
	AdjustLayout();

	return 0;
}

void CCFileView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CCFileView::FillFileView()
{
	HTREEITEM hRoot = m_wndCCFileView.InsertItem(_T("FakeApp files"), 0, 0);
	m_wndCCFileView.SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);

	HTREEITEM hSrc = m_wndCCFileView.InsertItem(_T("FakeApp Source Files"), 0, 0, hRoot);

	m_wndCCFileView.InsertItem(_T("FakeApp.cpp"), 1, 1, hSrc);
	m_wndCCFileView.InsertItem(_T("FakeApp.rc"), 1, 1, hSrc);
	m_wndCCFileView.InsertItem(_T("FakeAppDoc.cpp"), 1, 1, hSrc);
	m_wndCCFileView.InsertItem(_T("FakeAppView.cpp"), 1, 1, hSrc);
	m_wndCCFileView.InsertItem(_T("MainFrm.cpp"), 1, 1, hSrc);
	m_wndCCFileView.InsertItem(_T("StdAfx.cpp"), 1, 1, hSrc);

	HTREEITEM hInc = m_wndCCFileView.InsertItem(_T("FakeApp Header Files"), 0, 0, hRoot);

	m_wndCCFileView.InsertItem(_T("FakeApp.h"), 2, 2, hInc);
	m_wndCCFileView.InsertItem(_T("FakeAppDoc.h"), 2, 2, hInc);
	m_wndCCFileView.InsertItem(_T("FakeAppView.h"), 2, 2, hInc);
	m_wndCCFileView.InsertItem(_T("Resource.h"), 2, 2, hInc);
	m_wndCCFileView.InsertItem(_T("MainFrm.h"), 2, 2, hInc);
	m_wndCCFileView.InsertItem(_T("StdAfx.h"), 2, 2, hInc);

	HTREEITEM hRes = m_wndCCFileView.InsertItem(_T("FakeApp Resource Files"), 0, 0, hRoot);

	m_wndCCFileView.InsertItem(_T("FakeApp.ico"), 2, 2, hRes);
	m_wndCCFileView.InsertItem(_T("FakeApp.rc2"), 2, 2, hRes);
	m_wndCCFileView.InsertItem(_T("FakeAppDoc.ico"), 2, 2, hRes);
	m_wndCCFileView.InsertItem(_T("FakeToolbar.bmp"), 2, 2, hRes);

	m_wndCCFileView.Expand(hRoot, TVE_EXPAND);
	m_wndCCFileView.Expand(hSrc, TVE_EXPAND);
	m_wndCCFileView.Expand(hInc, TVE_EXPAND);
}

void CCFileView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndCCFileView;
	ASSERT_VALID(pWndTree);

	if (pWnd != pWndTree)
	{
		CDockablePane::OnContextMenu(pWnd, point);
		return;
	}

	if (point != CPoint(-1, -1))
	{
		// Select clicked item:
		CPoint ptTree = point;
		pWndTree->ScreenToClient(&ptTree);

		UINT flags = 0;
		HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
		if (hTreeItem != NULL)
		{
			pWndTree->SelectItem(hTreeItem);
		}
	}

	pWndTree->SetFocus();
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EXPLORER, point.x, point.y, this, TRUE);
}

void CCFileView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndCCFileView.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CCFileView::OnProperties()
{
	AfxMessageBox(_T("Properties...."));

}

void CCFileView::OnFileOpen()
{
	// TODO: Add your command handler code here
}

void CCFileView::OnFileOpenWith()
{
	// TODO: Add your command handler code here
}

void CCFileView::OnDummyCompile()
{
	// TODO: Add your command handler code here
}

void CCFileView::OnEditCut()
{
	// TODO: Add your command handler code here
}

void CCFileView::OnEditCopy()
{
	// TODO: Add your command handler code here
}

void CCFileView::OnEditClear()
{
	// TODO: Add your command handler code here
}

void CCFileView::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rectTree;
	m_wndCCFileView.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CCFileView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

	m_wndCCFileView.SetFocus();
}

void CCFileView::OnChangeVisualStyle()
{
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_EXPLORER_24 : IDR_EXPLORER, 0, 0, TRUE /* Locked */);

	m_FileViewImages.DeleteImageList();

	UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_FILE_VIEW_24 : IDB_FILE_VIEW;

	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("Can't load bitmap: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}

	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);

	UINT nFlags = ILC_MASK;

	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

	m_FileViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_FileViewImages.Add(&bmp, RGB(255, 0, 255));

	m_wndCCFileView.SetImageList(&m_FileViewImages, TVSIL_NORMAL);
}

//Polaris-20140710
/*
 * 函数功能：获得文件名后缀
 * 参数：文件名字符串
 * 返回值：文件名后缀字符串
 */
CString CGetSuffix(CString strFileName) 
{ 
	return strFileName.Right(strFileName.GetLength()-strFileName.ReverseFind('.')-1); 
}

/*
 * 函数功能：添加一个文件夹及其子目录中的文件到文件树中
 * 参数：文件夹目录字符串
 * 返回值：无
 */
void CCFileView::AddFolder(CString strDir,HTREEITEM hParent)
{
	CFileFind ff;//CFileFind类执行本地文件查找

	//HTREEITEM是CTreeCtrl树的子节点项的指针
	HTREEITEM ht = m_wndCCFileView.InsertItem(strDir.Right(strDir.GetLength()-strDir.ReverseFind('\\')-1), 0, 0, hParent);
	m_wndCCFileView.SetItemData(ht, 0);

	BOOL b = ff.FindFile(strDir+_T("\\*.*"));//FindFile查找一个目录中的指定文件
	//_T()是一个宏，作用是让程序支持Unicode编码

	//遍历strDir目录下的所有文件和子目录
	while (b)
	{
		b = ff.FindNextFile();//查找下一个文件
		if (!ff.IsDots())//Windows的每个目录下都有缺省的两个目录，名称分别为 '. '和 '.. '，分别代表本层目录和上一层目录。只有这两个目录的IsDots()结果为真
		{
			if (ff.IsDirectory()) //如果当前项是目录
			{
				AddFolder(ff.GetFilePath(),ht);//递归访问子目录
			}
			else	//当前项是文件
			{
				CString ext = CGetSuffix(ff.GetFileName());//获得文件名后缀
				
				if (ext=="c" || ext=="cpp")//如果是c文件或cpp文件，就添加进文件树中
				{
					HTREEITEM _ht = m_wndCCFileView.InsertItem(ff.GetFileName(), 1, 1, ht);
					m_wndCCFileView.SetItemData(_ht,1);
				}
				else if (ext == "h")//如果是h文件，也添加到文件树中
				{
					HTREEITEM _ht = m_wndCCFileView.InsertItem(ff.GetFileName(), 2, 2, ht);
					m_wndCCFileView.SetItemData(_ht,2);
				}
				
			}//end else
		}//end if
	}//end while
	ff.Close();
}

BEGIN_MESSAGE_MAP(CCFileViewToolBar, CMFCToolBar)
	ON_WM_SIZE()
END_MESSAGE_MAP()


void CCFileViewToolBar::OnSize(UINT nType, int cx, int cy)
{
	CMFCToolBar::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
}
