// SDGDoWhile.cpp: implementation of the CSDGDoWhile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGDoWhile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSDGDoWhile::CSDGDoWhile()
{
	m_sType="SDGDOWHILE";
	visit=0;

}

CSDGDoWhile::~CSDGDoWhile()
{

}

