// FilesToTokens.cpp: implementation of the FilesToTokens class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FilesToTokens.h"
#include "Lexical.h"
#include "direct.h" 
#include "FormatTokenLines.h"
#include "FuncBndBuild.h"
#include <iostream>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FilesToTokens::FilesToTokens()
{
	tokenFileArray.RemoveAll();
	Obj_file_path = _T("");
	out_file_path = _T("");
}

FilesToTokens::~FilesToTokens()
{
	tokenFileArray.RemoveAll();
}
FilesToTokens::FilesToTokens(const FilesToTokens& FTT)
{
	tokenFileArray.RemoveAll();
	tokenFileArray.Append(FTT.tokenFileArray);
}
FilesToTokens& FilesToTokens::operator=(const FilesToTokens& FTT)
{
	tokenFileArray.RemoveAll();
	tokenFileArray.Append(FTT.tokenFileArray);
	return *this;
}
void FilesToTokens::operator()(CString filePath)
{
	ReadAllFiles(filePath);
	TokensOutput();
/*	int i;
	CString strOut;
	for( i = 0; i < tokenFileArray.GetSize(); i++ )
	{
		int j;
		CString &fileName = tokenFileArray[i].GetFileName();
		long hashedFileName = tokenFileArray[i].GetHashedFileName();
		CString str_hashName;
		str_hashName.Format("%d",hashedFileName);
		CArray<TokenLine,TokenLine> &TL = tokenFileArray[i].GetTokenList();

		strOut = strOut + fileName + "\r\n" 
			     + str_hashName + "\r\n\r\n";

		for( j = 0; j < TL.GetSize(); j++ )
		{
			CString strIdx,strRelaline,strSrcLine;
			strIdx.Format("%d",j+1);
			strRelaline.Format("%d",TL[j].tokenLine[0].line);
			strSrcLine.Format("%d",TL[j].tokenLine[0].srcLine);

			strOut = strOut + "##" + strIdx + "##" + strRelaline + "##" + strSrcLine + "     ";
			int k;
			for( k = 0; k < TL[j].tokenLine.GetSize(); k++ )
			{
				strOut += TL[j].tokenLine[k].srcWord + " ";
			}
			strOut += "\r\n";
		}
		strOut += "\r\n\r\n\r\n";
	}
	CStdioFile outsrcf;
	if( outsrcf.Open( "D:\\SouceFile2.txt", CFile::modeCreate | CFile::modeWrite ) )
	{
		outsrcf.WriteString( strOut );
		outsrcf.Close();
	}
*/
}
void FilesToTokens::ReadAllFiles(CString filePath)
{
	CFileFind finder;
	CString curpath=filePath;
	_chdir(filePath);//当前工作目录置成filePath
	
	int cfile_sum=0;
	BOOL bWorking=finder.FindFile("*.*");
	while (bWorking)
	{
		bWorking=finder.FindNextFile();
		CString curname=finder.GetFileName();
		int curname_size=curname.GetLength();
		if(finder.IsDirectory())
		{
			if( curname.GetAt(0)!='.')
			{
				curpath=filePath+"\\"+curname;
				ReadAllFiles(curpath);//递归，读取目录curpath下的所有C文件
			}
			curpath=filePath;
		}
		else if(curname.GetAt(curname_size-2)=='.' && 
			    ( curname.GetAt(curname_size-1)=='c'|| curname.GetAt(curname_size-1)=='C') )//wq-20090213-加入.C后缀的识别
		{//找到后缀名为.c或.C的文件
			cout<<"READ: \""<<LPCTSTR(finder.GetFilePath())<<"\"..."<<endl;
			cfile_sum++;
			ChangeFileToToken(finder.GetFilePath());
		}
	}
	finder.Close();
}

void FilesToTokens::ChangeFileToToken(const CString cFileName)
{
	CString str_token,seq_str,program=_T("");
	if(ReadCfile(cFileName,program)==1)
	{
		//   生成token串，并输出之
		CArray<TNODE,TNODE> TArry;
		CArray<SNODE,SNODE> SArry;
		CArray<FNODE,FNODE> FArry;
		CArray<CERROR,CERROR>   EArry;
		CArray<VIPCS,VIPCS>     VArry;	
		CLexical clc;

		clc.Obj_file_path = Obj_file_path;
		clc(0,program,VArry,EArry,TArry,SArry,FArry);//词法分析：将程序program中的信息存入TArry,SArry,FArry中
//		clc.OutputSrcFileAfterTokened(TArry);/////wq//输出正常
		if(TArry.GetSize() <= Min_Token_Num)
		{
			return;//所含token字太少，不与分析！
		}
		FormatTokenLines ftls;
		ftls(TArry,clc.srcCode);
		FuncBndBuild fbb;
		fbb(TArry);
		ChangTArryToTokenLines(TArry,SArry,FArry,cFileName);

		TArry.RemoveAll();
		SArry.RemoveAll();
		FArry.RemoveAll();

	}
}

int FilesToTokens::ReadCfile(const CString cFileName,CString &program)
{
	//  将名为"cfilename"的程序读入program
	CStdioFile file;
	CString str=_T("");
	
	if( file.Open(cFileName,CFile::modeRead|CFile::typeText) == 0 )
	{
	   //str="读文件"+cfilename+"失败!\n\n"+"请检查是否已创建该文件！";
	   //AfxMessageBox(str);
	   return 0;
	}
	else
	{  
		str=_T("");
	    program=_T("");
		while(file.ReadString(str))
		{
			program=program+str+"\n";
			str=_T("");
		}
		file.Close();
		return 1;
	}
}

void FilesToTokens::ChangTArryToTokenLines( CArray<TNODE,TNODE>& TArry,
										    CArray<SNODE,SNODE>& SArry,//wq-20090505
										    CArray<FNODE,FNODE>& FArry,//wq-20090505
										    CString cFileName)
{
	TokenedFile TF;
	CString fileName = cFileName;
	long hashedFileName = HashedFileName(cFileName);
	CArray<TokenLine,TokenLine> tokenList; 
	TArryToTokenList(TArry,tokenList);
	TF.Assign(fileName,hashedFileName,tokenList,SArry,FArry);
	int i;
	if( tokenFileArray.GetSize() == 0 )
	{
		tokenFileArray.Add(TF);
	}
	else
	{
		int insertFlag = 0;
		for( i = 0; i < tokenFileArray.GetSize(); i++ )
		{
			long tempHashed = tokenFileArray[i].GetHashedFileName();
			if(hashedFileName <= tempHashed)
			{
				tokenFileArray.InsertAt(i,TF);
				insertFlag = 1;
				break;
			}
		}
		if( insertFlag == 0 )
		{
			tokenFileArray.Add(TF);
		}
	}

}
void FilesToTokens::TArryToTokenList( CArray<TNODE,TNODE>& TArry,
									 CArray<TokenLine,TokenLine>& tokenList)
{
	int i;
	int line = 0;
	TokenLine TL;	
	for( i = 0; i < TArry.GetSize(); i++ )
	{
		if( i > 0 && TArry[i].line != TArry[i-1].line )
		{
			tokenList.Add(TL);
			TL.RemoveAll();
		}
		TL.Add(TArry[i]);
	}		
	tokenList.Add(TL);
	//CString str;str.Format("%d", tokenList.GetSize());AfxMessageBox(str);//ww
	//tokenList中每个数组元素为一行token
}
long FilesToTokens::HashedFileName(const CString filename)
{
	long int Hsize=1000000000;
	unsigned long h=0, g;
	int len=filename.GetLength();
	if(len<=11)
	{
		return 0;
	}
	CString str=filename.Right(len-11);
    for(int i=0; i<str.GetLength(); i++)
	{
        h = (h << 4)+ str.GetAt(i);
        if(g = h & 0xf0000000)
		{
            h ^= g>>24;
            h ^= g;
        }
    }
	h = h % Hsize;
	return (long)h;
}
int FilesToTokens::FindFileName(const long& hashedFileName, const CString& fileName)
{
	int begin = 0;
	int end = tokenFileArray.GetSize();
	int equalHashPos = FindEqualHashedValue(begin,end,hashedFileName);
	if( equalHashPos < 0 || equalHashPos >= tokenFileArray.GetSize())
	{
		return -1;
	}
	if( fileName == tokenFileArray[equalHashPos].fileName )
	{
		return equalHashPos;
	}
	else
	{
		int i;
		for( i = equalHashPos-1; i > 0 ; i-- )
		{
			if( hashedFileName == tokenFileArray[i].hashedFileName )
			{
				if( fileName == tokenFileArray[i].fileName )
				{
					return i;
				}
			}
			else
			{
				break;
			}
		}
		for( i = equalHashPos+1; i < tokenFileArray.GetSize(); i++ )
		{
			if( hashedFileName == tokenFileArray[i].hashedFileName )
			{
				if( fileName == tokenFileArray[i].fileName )
				{
					return i;
				}
			}
			else
			{
				break;
			}
		}
		return -1;
	}
}

int FilesToTokens::FindEqualHashedValue(int begin, int end,	const long& hashedFileName)
{
	int rang = end - begin;
	int middle = begin + rang/2;
	if( rang/2 == 0 )
	{
		if( hashedFileName == tokenFileArray[middle].hashedFileName )
		{
			return middle;
		}
		else
		{
			return -1;
		}
	}
	else
	{
		if( hashedFileName == tokenFileArray[middle].hashedFileName )
		{
			return middle;		 
		}
		else if( hashedFileName < tokenFileArray[middle].hashedFileName )
		{
			return FindEqualHashedValue( begin, middle, hashedFileName );
		}
		else
		{
			return FindEqualHashedValue( middle+1, end, hashedFileName );
		}
	}
	return -1;
}
/*wq-20090521-注释掉******************
CString FilesToTokens::GetFuncSourceCode(CString fname, CString relaLPOS, CString LPOS, int funcBgn, int funcEnd)
{
	CString funcSrcCode("");
	long hshFname = HashedFileName(fname);
	int idx = FindFileName( hshFname, fname );
	if( idx < 0 || idx >= tokenFileArray.GetSize() )
	{
		return "";
	}
    TokenedFile &tknFile = tokenFileArray[idx];
	CArray<TokenLine,TokenLine> &tokenList = tknFile.GetTokenList();
	int lflw = 0;
	CArray<int,int> doLineArry;
	doLineArry.RemoveAll();
	int i,k;
//	for( i = funcBgn, k = 0; i <= funcEnd; i++ )
	CString strK;
	int dwFlag = 0;
	for( i = funcBgn; i <= funcEnd; i++ )
	{
		CArray<TNODE,TNODE> &tokenLine = tokenList[i-1].GetTokenLine();
		CString strLine("");

		CString strRPos(""),strLPos("");
		int rPos;
		for( k = 0; k < relaLPOS.GetLength()/4; k++ )
		{
			strRPos = relaLPOS.Mid(k*4,4);
			strLPos = LPOS.Mid(k*4,4);
			rPos = atoi( strRPos );
			if( rPos == i )
			{
				break;
			}
		}
		if( rPos == i )
		{
			strK.Format("%4d",k+1);
			CString strCtxt;
			strCtxt.Format("%d",tokenLine[0].cntxtLine);
			int len = strCtxt.GetLength();
			for( ; len < 4; len++ )
			{
				strCtxt = "0" + strCtxt;
			}
			strLine += strK + "- " +strLPos + " - L" + strRPos + " - CL" +strCtxt + " -    ";
			k++;
		}
		else
		{
			strLPos.Format("%d",tokenLine[0].srcLine);
			strRPos.Format("%d",tokenLine[0].line);
			int len = 4-strLPos.GetLength();
			for(; len > 0; len-- )
			{
				strLPos = "0" + strLPos;
			}
			for( len = 4-strRPos.GetLength(); len > 0; len -- )
			{
				strRPos = "0" + strRPos;
			}
			int kk = atoi(strK);
			strK.Format("%d",kk);
			for( len = strK.GetLength(); len > 0; len-- )
			{
				strLine += "  ";
			}
			for( len = 0; len < 4-strK.GetLength(); len++ )
			{
				strLine += " ";
			}
			strLine += "- " +strLPos + " - L" + strRPos + "                      ";
		}
		int j;
		for( j = 0; j < lflw ; j++ )
		{
			strLine += "    ";
		}
		for( j = 0; j < tokenLine.GetSize(); j++ )
		{
			int key = tokenLine[j].key;
			if(tokenLine[j].key == CN_DO)
			{
				doLineArry.Add(tokenLine[j].line);
				dwFlag = 1;
			}
			if( tokenLine[j].key == CN_WHILE && tokenLine[tokenLine.GetSize()-1].key == CN_LINE )
			{
				dwFlag = 3;
			}
			if( j == 0 && tokenLine[j].key == CN_LFLOWER )
			{
				lflw++;
			}
			if( j == 0 && tokenLine[j].key == CN_RFLOWER )
			{
				strLine = strLine.Mid(0,strLine.GetLength()-4);
				lflw--;
			}
			strLine += tokenLine[j].srcWord + " ";
			if( dwFlag == 1 && j == tokenLine.GetSize()-1 )
			{
				CString whLine;
				whLine.Format("%d",tokenLine[j].dowhileLine);
				int len = whLine.GetLength();
				for( ; len < 4; len++ )
				{
					whLine = "0" + whLine;
				}
				strLine += "//////while = L" + whLine +"//////";
				dwFlag = 0;
			}
			if( dwFlag == 3 && j == tokenLine.GetSize()-1 )
			{
				CString doLine;
				doLine.Format("%d",doLineArry[doLineArry.GetSize()-1]);
				int len = doLine.GetLength();
				for( ; len < 4; len++ )
				{
					doLine = "0" + doLine;
				}
				strLine += "//////do = L" + doLine +"//////";
				dwFlag = 0;
				doLineArry.RemoveAt(doLineArry.GetSize()-1);
			}
		}
		strLine += "\r\n";
		funcSrcCode += strLine;
	}

	return funcSrcCode;

}
/******************************************************/
/*wq-20090521-change******************/
/*
 * 函数功能：获取函数范围的代码，加了一些标记
 */
CString FilesToTokens::GetFuncSourceCode(CString fname, CString relaLPOS, CString LPOS, int funcBgn, int funcEnd,
										 CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry//wq-20090522
										 )
{
	CString funcSrcCode("");

	CString program;
	ReadCFile(fname,program);
	CArray<TNODE,TNODE> TArry;
	CArray<SNODE,SNODE> SArry;
	CArray<FNODE,FNODE> FArry;
	CArray<CERROR,CERROR>   EArry;
	CArray<VIPCS,VIPCS>     VArry;	
	CLexical clc;
	CArray<CH_FILE_INFO,CH_FILE_INFO> filesInfo;//wq-20090610
	clc(0,program,VArry,EArry,TArry,SArry,FArry,headerFilesArry,fname,filesInfo);	
	FormatTokenLines ftls;
	ftls(TArry,clc.srcCode);
	FuncBndBuild fbb;
	fbb(TArry);
	tokenFileArray.RemoveAll();
	ChangTArryToTokenLines(TArry,SArry,FArry,fname);
	if(tokenFileArray.GetSize()<=0)
		return "";
	TokenedFile& tokenedFile = tokenFileArray[0];
	if(tokenedFile.tokenList.GetSize()<=0)
		return "";
	CArray<TokenLine,TokenLine>& tokenList = tokenedFile.GetTokenList();

	int lflw = 0;
	CArray<int,int> doLineArry;
	doLineArry.RemoveAll();
	int i,k;

	CString strK;
	int dwFlag = 0;
	for( i = funcBgn; i <= funcEnd; i++ )
	{
		if( i-1<0 || i-1>=tokenList.GetSize() )
			break;//wq-20090610-有问题!!!
		if( i > tokenList.GetSize() )
		{
			continue;
		}
		CArray<TNODE,TNODE> &tokenLine = tokenList[i-1].GetTokenLine();
		CString strLine("");

		CString strRPos(""),strLPos("");
		int rPos;
		for( k = 0; k < relaLPOS.GetLength()/4; k++ )
		{
			strRPos = relaLPOS.Mid(k*4,4);
			strLPos = LPOS.Mid(k*4,4);
			rPos = atoi( strRPos );
			if( rPos == i )
			{
				break;
			}
		}
		if( rPos == i )
		{
			strK.Format("%4d",k+1);
			CString strCtxt;
			strCtxt.Format("%d",tokenLine[0].cntxtLine);
			int len = strCtxt.GetLength();
			for( ; len < 4; len++ )
			{
				strCtxt = "0" + strCtxt;
			}
			strLine += strK + "- " +strLPos + " - L" + strRPos + " - CL" +strCtxt + " -    ";
			k++;
		}
		else
		{
			strLPos.Format("%d",tokenLine[0].srcLine);
			strRPos.Format("%d",tokenLine[0].line);
			int len = 4-strLPos.GetLength();
			for(; len > 0; len-- )
			{
				strLPos = "0" + strLPos;
			}
			for( len = 4-strRPos.GetLength(); len > 0; len -- )
			{
				strRPos = "0" + strRPos;
			}
			int kk = atoi(strK);
			strK.Format("%d",kk);
			for( len = strK.GetLength(); len > 0; len-- )
			{
				strLine += "  ";
			}
			for( len = 0; len < 4-strK.GetLength(); len++ )
			{
				strLine += " ";
			}
			strLine += "- " +strLPos + " - L" + strRPos + "                      ";
		}
		int j;
		for( j = 0; j < lflw ; j++ )
		{
			strLine += "    ";
		}
		for( j = 0; j < tokenLine.GetSize(); j++ )
		{
			int key = tokenLine[j].key;
			if(tokenLine[j].key == CN_DO)
			{
				doLineArry.Add(tokenLine[j].line);
				dwFlag = 1;
			}
			if( tokenLine[j].key == CN_WHILE && tokenLine[j].dowhileLine > 0
				&& tokenLine[tokenLine.GetSize()-1].key == CN_LINE )
			{
				dwFlag = 3;
			}
			if( j == 0 && tokenLine[j].key == CN_LFLOWER )
			{
				lflw++;
			}
			if( j == 0 && tokenLine[j].key == CN_RFLOWER )
			{
				strLine = strLine.Mid(0,strLine.GetLength()-4);
				lflw--;
			}
			strLine += tokenLine[j].srcWord + " ";
			if( dwFlag == 1 && j == tokenLine.GetSize()-1 )
			{
				CString whLine;
				whLine.Format("%d",tokenLine[j].dowhileLine);
				int len = whLine.GetLength();
				for( ; len < 4; len++ )
				{
					whLine = "0" + whLine;
				}
				strLine += "//////while = L" + whLine +"//////";
				dwFlag = 0;
			}
			if( dwFlag == 3 && j == tokenLine.GetSize()-1 )
			{
				CString doLine;
				//doLine.Format("%d",doLineArry[doLineArry.GetSize()-1]);
				if( doLineArry.GetSize() > 0 )
					doLine.Format("%d",doLineArry[doLineArry.GetSize()-1]);
				int len = doLine.GetLength();
				for( ; len < 4; len++ )
				{
					doLine = "0" + doLine;
				}
				strLine += "//////do = L" + doLine +"//////";
				dwFlag = 0;
				doLineArry.RemoveAt(doLineArry.GetSize()-1);
			}
		}
		strLine += "\r\n";
		funcSrcCode += strLine;
	}

	return funcSrcCode;

}
/******************************************************/
/*****wq-20090520-注释掉*************************
void FilesToTokens::GetFuncCodeLines(CArray<FUNCCODE_IDMAP,FUNCCODE_IDMAP> &funcCodeLinesArry,
									 CString fname, CString relaLPOS, CString LPOS, 
									 int funcBgn, int funcEnd)
{
	funcCodeLinesArry.RemoveAll();

	long hshFname = HashedFileName(fname);
	int idx = FindFileName( hshFname, fname );
	if( idx < 0 || idx >= tokenFileArray.GetSize() )
	{
		return;
	}
    TokenedFile &tknFile = tokenFileArray[idx];
	CArray<TokenLine,TokenLine> &tokenList = tknFile.GetTokenList();
	int lflw = 0;

	int i,k;
	for( i = funcBgn, k = 0; i <= funcEnd; i++ )
	{
		FUNCCODE_IDMAP funcCodeIdmap;
		CString strRPos = relaLPOS.Mid(k*4,4);
		CString strLPos = LPOS.Mid(k*4,4);
		int rPos = atoi( strRPos );
		CArray<TNODE,TNODE> &tokenLine = tokenList[i-1].GetTokenLine();

		CString strLine("");
		if( rPos == i )
		{
			funcCodeIdmap.cpLine = k+1;
			funcCodeIdmap.rline = rPos;//wq-20090512
			CString strK;
			strK.Format("%4d",k+1);
			strLine += " -CP" + strK + "-    ";
			k++;
		}
		else
		{
			funcCodeIdmap.cpLine = 0;

			/****wq-20090512*****
			funcCodeIdmap.rline = -1;
			if( tokenLine.GetSize() > 0 )
			{
				funcCodeIdmap.rline = tokenLine[0].line;
			}/****wq-20090512*****

			strLine += "                ";
		}
		int j;
		for( j = 0; j < lflw ; j++ )
		{
			strLine += "    ";
		}
		for( j = 0; j < tokenLine.GetSize(); j++ )
		{
			if( j == 0 && tokenLine[j].key == CN_LFLOWER )
			{
				lflw++;
			}
			if( j == 0 && tokenLine[j].key == CN_RFLOWER )
			{
				strLine = strLine.Mid(0,strLine.GetLength()-4);
				lflw--;
			}
			strLine += tokenLine[j].srcWord + " ";
		}
		funcCodeIdmap.codeSrcLine = strLine;
		funcCodeLinesArry.Add(funcCodeIdmap);
	}
}
/**************************************************/
/**wq-20090520-change****************/
void FilesToTokens::GetFuncCodeLines(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
									 CArray<FUNCCODE_IDMAP,FUNCCODE_IDMAP> &funcCodeLinesArry,
									 CString fname, CString relaLPOS, CString LPOS, 
									 int funcBgn, int funcEnd)
{
	funcCodeLinesArry.RemoveAll();

	CString program;
	ReadCFile(fname,program);

	CArray<TNODE,TNODE> TArry;
	CArray<SNODE,SNODE> SArry;
	CArray<FNODE,FNODE> FArry;
	CArray<CERROR,CERROR>   EArry;
	CArray<VIPCS,VIPCS>     VArry;	
	CArray<CH_FILE_INFO,CH_FILE_INFO> filesReadInfo;
	CLexical clc;
//	clc(0,program,VArry,EArry,TArry,SArry,FArry);	
	clc(0,program,VArry,EArry,TArry,SArry,FArry,headerFilesArry,fname,filesReadInfo);

	FormatTokenLines ftls;
	ftls(TArry,clc.srcCode);
	FuncBndBuild fbb;
	fbb(TArry);
	tokenFileArray.RemoveAll();
	ChangTArryToTokenLines(TArry,SArry,FArry,fname);
	if(tokenFileArray.GetSize()<=0)
		return;
	TokenedFile& tokenedFile = tokenFileArray[0];
	if(tokenedFile.tokenList.GetSize()<=0)
		return;
	CArray<TokenLine,TokenLine>& tokenList = tokenedFile.GetTokenList();


	int lflw = 0;

	int i,k;
	for( i = funcBgn, k = 0; i <= funcEnd; i++ )
	{
		if( i-1<0 || i-1>=tokenList.GetSize() )
			break;//wq-20090610-有问题!!!
		CArray<TNODE,TNODE> &tokenLine = tokenList[i-1].GetTokenLine();

		FUNCCODE_IDMAP funcCodeIdmap;
		CString strRPos;// = relaLPOS.Mid(k*4,4);
		CString strLPos;// = LPOS.Mid(k*4,4);
		int rPos;
		for( k = 0; k < relaLPOS.GetLength()/4; k++ )
		{
			strRPos = relaLPOS.Mid(k*4,4);
			strLPos = LPOS.Mid(k*4,4);
			rPos = atoi( strRPos );
			if( rPos == i )
			{
				break;
			}
		}

		CString strLine("");
		if( rPos == i )
		{
			funcCodeIdmap.cpLine = k+1;
			funcCodeIdmap.rline = rPos;//wq-20090512
			CString strK;
			strK.Format("%4d",k+1);
			strLine += " -CP" + strK + "-    ";
			k++;
		}
		else
		{
			funcCodeIdmap.cpLine = 0;

			//wq-20090512
			funcCodeIdmap.rline = -1;
			if( tokenLine.GetSize() > 0 )
			{
				funcCodeIdmap.rline = tokenLine[0].line;
			}//wq-20090512

			strLine += "                ";
		}
		int j;
		for( j = 0; j < lflw ; j++ )
		{
			strLine += "    ";
		}
		for( j = 0; j < tokenLine.GetSize(); j++ )
		{
			if( j == 0 && tokenLine[j].key == CN_LFLOWER )
			{
				lflw++;
			}
			if( j == 0 && tokenLine[j].key == CN_RFLOWER )
			{
				strLine = strLine.Mid(0,strLine.GetLength()-4);
				lflw--;
			}
			strLine += tokenLine[j].srcWord + " ";
		}
		funcCodeIdmap.codeSrcLine = strLine;
		funcCodeLinesArry.Add(funcCodeIdmap);
	}

}
/***************************************************/


void FilesToTokens::TokensOutput()
{
	int i;
	CString outText("");
	for( i = 0; i < tokenFileArray.GetSize(); i++ )
	{
		CString fName = tokenFileArray[i].GetFileName();
		outText += fName + "\r\n";
		if(tokenFileArray[i].tokenList.GetSize()<=0)
			continue;
		CArray<TokenLine,TokenLine> &tknList = tokenFileArray[i].GetTokenList();
		int j;
		for( j = 0; j < tknList.GetSize(); j++ )
		{
			CArray<TNODE,TNODE> &tknLine = tokenFileArray[i].GetTokenLine(j);
			int k;
			for( k = 0; k < tknLine.GetSize(); k++ )
			{
				CString strWord,strKey,strSrcLine,strLine,strFuncBeg,strFuncEnd,strCnTxtL,strDWLine,strAddr,strDeref,strName,strPaddr;
				strWord.Format("%30s",tknLine[k].srcWord);
				strKey.Format("%20s",C_ALL_KEYWORD[tknLine[k].key]);
				strSrcLine.Format("%5d",tknLine[k].srcLine);
				strLine.Format("%5d",tknLine[k].line);
				strFuncBeg.Format("%5d",tknLine[k].funcBegLine);
				strFuncEnd.Format("%5d",tknLine[k].funcEndLine);
				strCnTxtL.Format("%5d",tknLine[k].cntxtLine);
				strDWLine.Format("%5d",tknLine[k].dowhileLine);
				strAddr.Format("%5d",tknLine[k].addr);
				strDeref.Format("%5d",tknLine[k].deref);
				strPaddr.Format("%5d",tknLine[k].paddr);
				outText += "WORD=" + strWord + 
						   " | KEY=" + strKey + 
						   " | SRCLINE=" + strSrcLine +
						   " | LINE=" + strLine +
						   " | FUNCB=" + strFuncBeg +
						   " | FUNCE=" + strFuncEnd +
						   " | CNTXTL=" + strCnTxtL +
						   " | DOWHIL=" + strDWLine +
						   " | ADDR=" + strAddr +
						   " | DEREF=" + strDeref +
						   " | PADDR=" + strPaddr +
						   " | NAME=" + tknLine[k].name +
						   "\r\n";
			}
		}
	}
	CStdioFile outfile;
	if( outfile.Open(out_file_path+"\\AllTokensInfo.txt",CFile::modeCreate| CFile::modeWrite))
	{
		outfile.WriteString(outText);
		outfile.Close();
	}
}

void FilesToTokens::Clean()
{
	int i;
	for( i = 0; i < tokenFileArray.GetSize(); i++ )
	{
		tokenFileArray[i].Clean();
	}
	tokenFileArray.RemoveAll();
	Obj_file_path = _T("");
	out_file_path = _T("");

}

bool FilesToTokens::ReadCFile(const CString cfilename, CString &program)
{
	CStdioFile file;
	CString str=_T("");
	
	if( file.Open(cfilename,CFile::modeRead|CFile::typeText) == 0 )
	{
	   //str="读文件"+cfilename+"失败!\n\n"+"请检查是否已创建该文件！";
	   //AfxMessageBox(str);
	   return 0;
	}
	else
	{  
		str=_T("");
	    program=_T("");
		while(file.ReadString(str))
		{
			program=program+str+"\n";
			str=_T("");
		}
		file.Close();
		return 1;
	}

}
