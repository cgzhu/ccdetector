#include "stdafx.h"
#include "Lexical.h"
#include "direct.h" 
#include "io.h"
#include "Out_TokenSeq.h"
#include <iostream>
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


bool CLexical::ISHAVE_WELL(const CString &cprogram, const int &position,const int length)
//检验position前面是否为'#'，position+length的后面是否为空格或'\t'；
//但是此程序未分析#ifdefined这种情况，并将之归并到此类语法错误中！
{
	bool inc_error=1;//标识头文件include(or define)所在行是否存在语法错误
	int i=position-1;
	while(i>=0 && cprogram[i]!='\n')//检查前面是否有'#'
	{ 
		if(cprogram[i]=='#')
		{
			inc_error=0;
			break;
		}else if(isgraph(cprogram[i]))//检验'#'和include(or define)之间是否有其他可打印的字符
		{
			break;
		}
		i--;
	}
	if(isgraph(cprogram[position+length]))//include(or define)后面是否为可打印字符
	{
		inc_error=1;
	}
	return inc_error;
}

CString CLexical::Deal_SomeLine(int &position, int &pos_inline, CString &program, bool del_flag)
//返回program中所在的那一行字符串；
//若del_flag等于1，则用"\r\n"代替这一行字符串，并将position置于次行的行首
{
	int head=position,trail=position;
	while(head>=0 && program[head]!='\n')
	{
		head--;
	}
	head++; //该行行头的位置
	pos_inline=position-head;//行头到position的距离
	while(trail<program.GetLength() && program[trail]!='\n')
	{
		trail++;
	}	    
	trail++;//该行行尾的下一个位置

	CString del_line=program.Mid(head,trail-head);
	if(del_flag==1)
	{	
		program.Delete(head,trail-head);
		program.Insert(head,"\r\n");
		if(head+2<program.GetLength())
		{
			position=head+2;
		}
	}


	return del_line;
}

void CLexical::INLBReplace(CString &cprogram,CString &hprogram,CArray<CString,CString> &Hfp_Array)
{
	OutputMemoryRecord("CLexical","INLBReplace",out_file_path,1,"");
	Delete_Comment(cprogram);
	int count=cprogram.Find("include",0);//查找cprogram中第一个include的位置
	while(count>=0 && count<cprogram.GetLength())
	{
		CString hfilename=_T("");//用于存放未处理的头文件名 (<xxx.h>或"xxx.h")
		if(ISHAVE_WELL(cprogram,count,7))//前面没有"#",不是文件包含语句！
		{
			count=cprogram.Find("include",count+7);
			continue;
		}
		count=count+7;//count指向"include"后面的那个字符
		if(count>0 && count<cprogram.GetLength())
		{
			int line_end=cprogram.Find('\n',count);//查找include所在行的末尾位置，存于line_end
			if(line_end>count)
			{
				int pos_inline=0;
				hfilename=cprogram.Mid(count,line_end-count);//line_end-count是头文件名字的长度
				Deal_SomeLine(count,pos_inline,cprogram,1);//用"\r\n"替换include所在行，
				hprogram=hprogram+READ_FILE_H(hfilename,Hfp_Array)+"\r\n";//将读出的头文件加入到hprogram中
			}
		}
		count=cprogram.Find("include",count);//查找cprogram中下一个include的位置
	}
	DEAL_FILE_H(hprogram,Hfp_Array);//处理hprogram中包含的头文件
}

//wq-20090522-重载INLBReplace()-暂时废弃
void CLexical::INLBReplace(CString &cprogram,
						   CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
						   CArray<CString,CString> &Hfp_Array)
{
	OutputMemoryRecord("CLexical","INLBReplace",out_file_path,1,"");
	Delete_Comment(cprogram);
	int count=cprogram.Find("include",0);//查找cprogram中第一个include的位置
	while(count>=0 && count<cprogram.GetLength())
	{
		CString hfilename=_T("");//用于存放未处理的头文件名 (<xxx.h>或"xxx.h")
		if(ISHAVE_WELL(cprogram,count,7))//前面没有"#",不是文件包含语句！
		{
			count=cprogram.Find("include",count+7);
			continue;
		}
		count=count+7;//count指向"include"后面的那个字符
		if(count>0 && count<cprogram.GetLength())
		{
			int line_end=cprogram.Find('\n',count);//查找include所在行的末尾位置，存于line_end
			if(line_end>count)
			{
				int pos_inline=0;
				hfilename=cprogram.Mid(count,line_end-count);//line_end-count是头文件名字的长度
				Deal_SomeLine(count,pos_inline,cprogram,1);//用"\r\n"替换include所在行，
//				hprogram=hprogram+READ_FILE_H(hfilename,Hfp_Array)+"\r\n";//将读出的头文件加入到hprogram中

			}
		}
		count=cprogram.Find("include",count);//查找cprogram中下一个include的位置
	}
//	DEAL_FILE_H(hprogram,Hfp_Array);//处理hprogram中包含的头文件
}

int CLexical::Find_Comment(CString &program, int Pos)//查找从count开始的下一个注释段的起始位置
{
	int count=program.Find("/*",Pos);	//记录当前"/*"的位置
	while(count>=0 && count<program.GetLength())
	{
		int Quot_Num=0;//统计count前双引号的个数
		int i=count-1;
		while(i>=0 && program[i]!='\n')
		{
			if(program[i]=='\"')
			{
				if((i-1)<0 || program[i-1]!='\\')
				{
					Quot_Num++;
				}
			}
			i--;
		}
		i=count+2;
		if(Quot_Num%2==0)
		{
			break;
		}
		count=program.Find("/*",count+2);
	}	
	return count;
}

bool CLexical::Delete_Comment(CString &program)//删除program中的注释部分
{
	bool Comment_error=0;
	int count=Find_Comment(program, 0);
	while(count>=0 && count<program.GetLength())
	{
		if(count+2<program.GetLength())
		{
			int end=program.Find("*/",count+2);
			if(end>count)
			{
				CString Comment_str=program.Mid(count,end+2-count);
				int Line_count=Comment_str.Remove('\n');
				Comment_str=_T("");
				program.Delete(count,end+2-count);
				for( ;Line_count>0; Line_count--)
				{
					Comment_str+="\r\n";
				}
				program.Insert(count,Comment_str);//为了源程序的行数定位准确，留下对应数量的空行
			}
			else
			{
				return 1;
			}
		}
		count=Find_Comment(program, count);
	}
	if(program.Find("/*")>0 || program.Find("*/")>0)//仍然存在"/*"或"*/"！
	{
		Comment_error=1;
		//AfxMessageBox("注释内容存在语法错误！");
	}	

	/*#######################################################################*/
	/*																	//	AfxMessageBox(program);	//ww 
	int	head = program.Find("//",0);	
	int tail = 0;
	while( head >=0 && head < program.GetLength() )
	{
		tail = program.Find("\n",head);
		program.Delete(head,tail-head+1);
		program.Insert(head,"\r\n");
		head = program.Find("//",head);
	}
*/
	/*######################################################################*/

	return Comment_error;
}

void CLexical::Add_CH_Mark(CArray<TNODE,TNODE> &TArry,CArray<TNODE,TNODE> &INLB_TArry)
//加入头文件和主文件token串之间的间隔节点
{
	TNODE hmark;                        //此节点为头文件与主文件的间隔点
	hmark.addr=-1;
	hmark.key=H_BETWEEN_C;
	hmark.line=-1;
	hmark.name=_T("");
	hmark.value=-1;
	INLB_TArry.Add(hmark);
	INLB_TArry.Append(TArry);
	TArry.Copy(INLB_TArry);           //将头文件内容(INLB_TArry)加入到主文件内容(TArry)中
	INLB_TArry.RemoveAll();

	/*王倩添加-20080729-*
	CStdioFile srcFile;
	CString srcline = _T("");
	CString str_TKIndex = _T("");

	for( int wdix = 0; wdix < TArry.GetSize(); wdix++ )
	{
		if( wdix + 1 < TArry.GetSize() &&
			TArry[wdix].line == TArry[wdix+1].line )
		{
			srcline += TArry[wdix].srcWord;
			str_TKIndex.Format(_T("%d"),wdix);
			srcline += "$" + str_TKIndex + "$ ";
		//	srcline += " ";
		}
		else 
		{		
			srcline += TArry[wdix].srcWord;
			str_TKIndex.Format(_T("%d"),wdix);
			srcline += "$" + str_TKIndex + "$\n";
		//	srcline += "\n";
		}
	}
	CString tFileName="D:\\Token_Out\\sourcecode.txt";	
	srcFile.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	srcFile.WriteString(srcline);
	srcFile.Close();
	/***/
}

void CLexical::DeleteHToken(CArray<TNODE,TNODE> &TArry)//将头文件生成的token串从TArry中删除
{
	int i=0;
	BOOL hflag=0;
	for(i=0; i<TArry.GetSize(); i++)
	{
		if(TArry[i].key == H_BETWEEN_C)//找到头文件与主文件token串的分界点
		{
			hflag=1;
			break;
		}
	}
	if(hflag==1)
	{
		TArry.RemoveAt(0,i+1);
	}
	else
	{
		cout<<"No include!"<<endl;
	}
}

//wq-20090522-重载READ_FILE_H
CString CLexical::READ_FILE_H(CString &hfilename,
							  CArray<CString,CString> &Hfp_Array,
							  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry//wq-20090522
							  )
{
//	OutputMemoryRecord("CLexical","READ_FILE_H",out_file_path,1,"Reading:"+hfilename);
	CString hfile=_T("");		//用于存放名为"hfilename"的头文件的内容
	CString hfilepath=_T("");	//用于存放名为"hfilename"的头文件的路径
	Out_TokenSeq readh;
	
	//删除头文件名字中的无用字符{----
	for(int i=0; i<hfilename.GetLength(); i++)
	{
		if(!isgraph(hfilename[i]))
		{
			hfilename.Delete(i,1);//删除无法打印的字符
		}
	}
	CString outFileName = hfilename;
 	if(hfilename.Find('\"') >= 0)//找到双引号"
	{
		if(hfilename.Remove('\"')!=2)
		{
			return hfile;//报错！
		}
	}
	if(hfilename.Find('<') >= 0)//找到'<'
	{
		if(hfilename.Remove('<')!=1 || hfilename.Remove('>')!=1)
		{
			return hfile;//报错！
		}
	}
	//删除头文件名字中的无用字符----}
	
	CString temp_name=hfilename;
	if(temp_name.Right(2)!=_T(".h"))
	{
		return hfile;//报错！
	}
	else
	{
		temp_name.Delete(temp_name.GetLength()-3,2);//去掉".h"
	}
	
	if(hfilename.Find('/')==-1 && WhichBFunction(temp_name)==-1)//文件名中未发现路径标记'/'，并且不是c语言库文件
	{
		hfilepath=FIND_FILE_H(Obj_file_path,hfilename,"");
		if(hfilepath!="NULL" && !Is_Cstr_Contain(Hfp_Array,hfilepath))
		{
			readh.READ_CHFILE(hfilepath,hfile);
			OutputMemoryRecord("Out_TokenSeq","READ_CHFILE",out_file_path,1,"Reading:"+outFileName+" ("+hfilepath+")" );
			Delete_Comment(hfile);
			//if(hfile.GetLength()==0)//用于测试！
			//	cout<<hfilepath<<endl;
			return hfile;
		}
	}
	if(hfilename.Find('/')>=0)//文件名中存在路径标记'\'
	{
		hfilename.Replace("/","\\");//用'\\'替换'\'
		hfilepath=Obj_file_path+"\\include\\"+hfilename;
		if(hfilename!="" && !Is_Cstr_Contain(Hfp_Array,hfilepath) )
		{
			readh.READ_CHFILE(hfilepath,hfile);
			OutputMemoryRecord("Out_TokenSeq","READ_CHFILE",out_file_path,1,"Reading:"+outFileName+" ("+hfilepath+")" );
			Delete_Comment(hfile);
			//if(hfile.GetLength()==0)//用于测试！
			//	cout<<hfilepath<<endl;
			return hfile;
		}	
	}
	return hfile;
}

//wq-20090522-重载READ_FILE_H()
CString CLexical::READ_FILE_H(CString &hfilename,CArray<CString,CString> &Hfp_Array)
{
//	OutputMemoryRecord("CLexical","READ_FILE_H",out_file_path,1,"Reading:"+hfilename);
	CString hfile=_T("");		//用于存放名为"hfilename"的头文件的内容
	CString hfilepath=_T("");	//用于存放名为"hfilename"的头文件的路径
	Out_TokenSeq readh;
	
	//删除头文件名字中的无用字符{----
	for(int i=0; i<hfilename.GetLength(); i++)
	{
		if(!isgraph(hfilename[i]))
		{
			hfilename.Delete(i,1);//删除无法打印的字符
		}
	}
	CString outFileName = hfilename;
 	if(hfilename.Find('\"') >= 0)//找到双引号"
	{
		if(hfilename.Remove('\"')!=2)
		{
			return hfile;//报错！
		}
	}
	if(hfilename.Find('<') >= 0)//找到'<'
	{
		if(hfilename.Remove('<')!=1 || hfilename.Remove('>')!=1)
		{
			return hfile;//报错！
		}
	}
	//删除头文件名字中的无用字符----}
	
	CString temp_name=hfilename;
	if(temp_name.Right(2)!=_T(".h"))
	{
		return hfile;//报错！
	}
	else
	{
		temp_name.Delete(temp_name.GetLength()-3,2);//去掉".h"
	}
	
	/*wq-20090609-去掉"../include/xxx.h"最前面的两个点*/
    if( hfilename.GetLength() > 2 )
	{
		if( hfilename[0] == '.' && hfilename[1] == '.' && hfilename[2] == '\\' )
		{
			hfilename.Delete(0,2);
			hfilepath=Obj_file_path+hfilename;
			readh.READ_CHFILE(hfilepath,hfile);
			OutputMemoryRecord("Out_TokenSeq","READ_CHFILE",out_file_path,1,"Reading:"+outFileName+" ("+hfilepath+")" );
			Delete_Comment(hfile);
			return hfile;
		}
	}
	if(hfilename.Find('/')==-1 && WhichBFunction(temp_name)==-1)//文件名中未发现路径标记'/'，并且不是c语言库文件
	{
		hfilepath=FIND_FILE_H(Obj_file_path,hfilename,"");
		if(hfilepath!="NULL" && !Is_Cstr_Contain(Hfp_Array,hfilepath))
		{
			readh.READ_CHFILE(hfilepath,hfile);
			OutputMemoryRecord("Out_TokenSeq","READ_CHFILE",out_file_path,1,"Reading:"+outFileName+" ("+hfilepath+")" );
			Delete_Comment(hfile);
			//if(hfile.GetLength()==0)//用于测试！
			//	cout<<hfilepath<<endl;
			return hfile;
		}
	}
	if(hfilename.Find('/')>=0)//文件名中存在路径标记'\'
	{
		hfilename.Replace("/","\\");//用'\\'替换'\'
		hfilepath=Obj_file_path+"\\include\\"+hfilename;
		if(hfilename!="" && !Is_Cstr_Contain(Hfp_Array,hfilepath))
		{
			readh.READ_CHFILE(hfilepath,hfile);
			OutputMemoryRecord("Out_TokenSeq","READ_CHFILE",out_file_path,1,"Reading:"+outFileName+" ("+hfilepath+")" );
			Delete_Comment(hfile);
			//if(hfile.GetLength()==0)//用于测试！
			//	cout<<hfilepath<<endl;
			return hfile;
		}	
	}
	return hfile;
}


CString CLexical::FIND_FILE_H(const CString filepath, const CString filename, const CString curFilePath)
//在路径filepath下查找名为filename的头文件
{
	CFileFind finder;
	CString tempath=filepath;
	CString resultpath;

	/*wq-20090609-先搜索当前路径下有无该头文件*/

	CString tmpFileName = curFilePath + "\\" + filename;
	CFile fopen;
	if(fopen.Open(tmpFileName,CFile::modeRead))
	{
		fopen.Close();
		return tmpFileName;
	}
/*	_chdir(curFilePath);
	BOOL bWorking=finder.FindNextFile();
	while (bWorking)
	{
		CString temname=finder.GetFileName();
		if( temname.CompareNoCase(filename) == 0 )//wq-20090609-大小写不敏感比较
		{
			return finder.GetFilePath();
		}
	}/****************************************************/

	_chdir(filepath);	
	BOOL bWorking=finder.FindFile("*.*");
	while (bWorking)
	{
//		_chdir(tempath);
		bWorking=finder.FindNextFile();
		CString temname=finder.GetFileName();
		if(finder.IsDirectory() && temname!="include")
			//注：此处由于无法传递c文件所在的路径名，为避免搜到重名头文件，
			//    故不搜索"include"文件夹；此函数存在搜不到所需头文件的可能。
		{
			if(temname.GetAt(0)!='.')
			{
				tempath=filepath+"\\"+finder.GetFileName();
				resultpath=FIND_FILE_H(tempath, filename,"");
//				tempath = filepath;//王倩添加-20080827
//				_chdir(tempath);
				if(resultpath != "NULL")
					return resultpath;
			}
		}
//		else if(temname==filename)
		else if( temname.CompareNoCase(filename) == 0 )//wq-20090609-大小写不敏感比较
		{
			return finder.GetFilePath();
		}
	}
	finder.Close();
	return "NULL";
}

void CLexical::DEAL_FILE_H(CString &hprogram,CArray<CString,CString> &Hfp_Array)
{
	OutputMemoryRecord("CLexical","DEAL_FILE_H",out_file_path,1,"");
	OutputMemoryRecord("","","",0,"");
	int count=hprogram.Find("include",0);
	while(count>=0 && count<hprogram.GetLength())
	{
		int pos_inline=0;
		CString del_line=Deal_SomeLine(count,pos_inline,hprogram,1);
		CString hfilename=_T("");
		hfilename=del_line.Right(del_line.GetLength()-pos_inline-7);//从include所在行找到未处理的头文件名，trail-count-7是名字的长度
		CString temp_str=READ_FILE_H(hfilename,Hfp_Array);

		if(temp_str.GetLength()>0)
		{
			if(count==0)
			{
				hprogram=temp_str+"\r\n"+hprogram;
			}
			else
			{
				temp_str="\r\n"+temp_str+"\r\n";
				hprogram.Insert(count,temp_str);
			}
		}		
		count=hprogram.Find("include",count);
	}
}

bool CLexical::Is_Cstr_Contain(CArray<CString,CString> &Hfp_Array,const CString &hfilepath)
{
	for(int i=0; i<Hfp_Array.GetSize(); i++)
	{
		if(Hfp_Array[i]==hfilepath)
		{
			return 1;
		}
	}
	Hfp_Array.Add(hfilepath);
	return 0;
}

//为便于后面的分析，将源程序中所有的字符串替换为"s"		
void CLexical::Del_String(CString &program)
{
									
	int start=0;
	bool Is_String=0;
	for(int i=0; i<program.GetLength(); i++)
	{
		char pg = program[i];
		if(program[i]=='\\')
		{
			i=i+1;
			continue;
		}
		if(Is_String==0 && program[i]=='\'')
		{
			if(i+1<program.GetLength() && program[i+1]=='\\')
			{
				i=i+2;
			}
			else
			{
				i=i+1;
			}
			continue;
		}
		if(Is_String==0 && program[i]=='\"')
		{
			Is_String=1;
			start=i+1;
			//TCHAR c=program[start];
			continue;
		}
		if(Is_String==1)
		{
			if(program[i]=='\"')
			{	
				//AfxMessageBox("nihao");	//ww
				Is_String=0;
				CString cstring;//wq-20081121
				cstring = program.Mid(start-1,i-start+2);//wq-20081121		
			//	AfxMessageBox(cstring);//ww
				program.Delete(start,i-start);
				program.Insert(start,"s");
				i=start+1;
				continue;
			}
			/*wq-20081223-删-可以出现字符串之间换行的情况*
			if(program[i]=='\n')
			{
				//cout<<program.Mid(i-30,60)<<endl;
				Is_String=0;
				AfxMessageBox("存在语法错误！");
			}/*************************************************/
		}
	}

//	AfxMessageBox(program);			//ww
}

void CLexical::DefineReplace(CString &hprogram, CString &cprogram)
{
	int hcount=hprogram.Find("define",0);
	while(hcount>=0 && hcount<hprogram.GetLength())
	{
		int len=hprogram.GetLength();
		/*if(hcount>=9600)
		{
			CStdioFile Out_Token;
			Out_Token.Open("D:\\HP.txt", CFile::modeCreate | CFile::modeWrite);
			Out_Token.WriteString(hprogram);
			Out_Token.Close();
		}*/
		if(ISHAVE_WELL(hprogram,hcount,6))
		{
			hcount=hcount+6;//未发现#，报错！
		}
		else
		{
			CString Macro_Name=_T(""),Macro_Value=_T("");
			if(!Genrate_Macro_NV(hprogram,hcount,Macro_Name,Macro_Value))
			{
				//Span_Define(hprogram,hcount,Macro_Name,Macro_Value);
				Span_Define(cprogram,0,Macro_Name,Macro_Value);
			}
		}
		hcount=hprogram.Find("define",hcount);
	}//对头文件中的宏定义进行替换

	int ccount=cprogram.Find("define",0);
	while(ccount>=0 && ccount<cprogram.GetLength())
	{
		if(ISHAVE_WELL(cprogram,ccount,6))
		{
			ccount=ccount+6;//未发现#，报错！
		}
		else
		{
			CString Macro_Name,Macro_Value;
			if(!Genrate_Macro_NV(cprogram,ccount,Macro_Name,Macro_Value))
			{
				Span_Define(cprogram,ccount,Macro_Name,Macro_Value);
			}
		}
		ccount=cprogram.Find("define",ccount);
	}//对正文中的宏定义进行替换
}

bool CLexical::Genrate_Macro_NV(CString &program,int &count,CString &Macro_Name,CString &Macro_Value)
//删除define所在行，并将的define后面的两个字符串存入Macro_Name和Macro_Value中
//如果未读出两个字符串（如定义单字符串：#define XXX），则在删除此行后返回1
{
	bool define_error=0;
	int i=0;
	int pos_inline=0;
	CString del_line=Deal_SomeLine(count,pos_inline,program,1);
	while(del_line.Right(2)=="\\\n" || del_line.Right(3)=="\\\r\n")
	{
		if(del_line.Right(2)=="\\\n")
			del_line.Delete(del_line.GetLength()-2,2);
		else
			del_line.Delete(del_line.GetLength()-3,3);
		del_line=del_line+" "+Deal_SomeLine(count,pos_inline,program,1);//将下一行取出并删除，接在del_line结尾
	}//如果define行结尾处出现行连接符'\'，则将这些行连接起来生成完整的del_line
	
	bool isprint_flag=0;
	for(i=0; i<del_line.GetLength(); i++)
	{
		if(!isprint(del_line[i]))//遇到非可打印字符
		{
			if(isprint_flag==0)//前一个字符也是非可打印字符
			{
				del_line.Delete(i,1);
			}
			else
			{
				isprint_flag=0;
			}
		}
		else
		{
			isprint_flag=1;
		}
	}//格式化del_line，删除无用的非可打印字符

	if(del_line.GetLength()>800)
	{
		//AfxMessageBox("生成的宏定义字符串过长！");
		//cout<<del_line<<endl;
		define_error=1;
		return define_error;
	}

	if(Genrate_Macro_Name(del_line,Macro_Name,i))////////////////生成Macro_Name
	{
		define_error=1;
		return define_error;
	}
	del_line=del_line.Right(del_line.GetLength()-i-1);//将宏定义名(Macro_Name)从del_line中删去
	///////////////////////////////////生成Macro_Value////////////////////////
	if(del_line.Find('\\',0)>=0)
	{
		define_error=1;
		return define_error;
	}//如果del_line中有'\'字符，则可能存在替换错误！
	define_error=1;
	for(i=0; i<del_line.GetLength(); i++)
	{
		if(isgraph(del_line[i]))
		{
			define_error=0;//如果del_line中有可打印的字符,则可以生成Macro_Value
		}
		else if(del_line[i]=='\r' || del_line[i]=='\n')
		{
			del_line.Delete(i,1);//删去del_line中的'\r'和'\n'
		}
	}
	Macro_Value=del_line;
	if( Macro_Value.Find("(",0) != -1 || Macro_Name.Find("(",0)==-1 )
	{//wq-20090618-只记录简单的宏替换（针对对数值定义的宏）
		Macro_Name = "";
		Macro_Value = "";
		define_error = 1;

	}

	return define_error;
}

bool CLexical::Genrate_Macro_Name(const CString &line_str,CString &Macro_Name,int &inter_mark)
{
	bool define_error=0;
	
	int i=line_str.Find("define",0)+6;
	while(i<line_str.GetLength() && !isgraph(line_str[i]))
	{
		i++;//将i移到宏定义名(Macro_Name)的第一个字符上
	}
	int beg=i;
	while(i<line_str.GetLength() &&  isgraph(line_str[i]))
	{
		i++;
	}
	Macro_Name=line_str.Mid(beg,i-beg);//define后面的第一个字符串

	if(Macro_Name.Find('(')>0)//若为带参数的宏定义名，则需重新生成Macro_Name
	{
		/*wq-20090610-不考虑带参数的宏定义*
		Macro_Name = "";
		define_error=1;
		return define_error;/**************/

		i=line_str.Find('(',0)+1;
		int Circle_count=1;
		while(i<line_str.GetLength() && Circle_count!=0)
		{
			if(line_str[i]=='(')
				Circle_count++;
			if(line_str[i]==')')
				Circle_count--;
			i++;
		}
		if(Circle_count!=0)
		{
			define_error=1;
			return define_error;			//左右括号数目不匹配
		}
		Macro_Name=line_str.Mid(beg,i-beg);//define后面的第一个字符串
	}
	inter_mark=i;
	for(i=0; i<Macro_Name.GetLength(); i++)
	{
		if(!isgraph(Macro_Name[i]))
			Macro_Name.Delete(i,1);
	}//删除宏定义名中所有无法打印的字符
	if(Macro_Name.Find("#define",0)>=0 || Macro_Name.GetLength()==0)
	{
		define_error=1;
		return define_error;
	}//检验宏定义名是否合法	
	return define_error;
}

void CLexical::Span_Define(CString &program,const int start,const CString Macro_Name,const CString Macro_Value)
{
	if(Macro_Name.Find('(')>0)
	{
		CString Macro_Nametemp=Macro_Name;
		if(Macro_Nametemp.Replace('(','(') != Macro_Nametemp.Replace(')',')') )
			return;//如此使用Replace函数是为了方便的得到Macro_Nametemp中'('与')'的个数
		if(ParaDefine_Replace(program,start,Macro_Name,Macro_Value))
			return;//如果ParaDefine_Replace函数能够完成替换，则返回
	}
	
	int count=program.Find(Macro_Name,start);
	int len=Macro_Name.GetLength();
	int pos_inline=0;
	bool Is_Define_flag=1;//此标识符代表该字符是否可以宏替换
	while(count>=0 && count<program.GetLength())
	{
		int i=0;
		Is_Define_flag=1;
		
		if(count-1>=0 && (isalnum(program[count-1]) || program[count-1]=='_'))
			Is_Define_flag=0;
		if(count+len<program.GetLength() && (isalnum(program[count+len]) || program[count+len]=='_'))
			Is_Define_flag=0;//如果"Macro_Name"被包含在一个变量名或函数名里，则不替换！

		CString def_line=Deal_SomeLine(count,pos_inline,program,0);
		if( def_line.Find('#',0)>=0 && def_line.Find('#',0)< def_line.Find("undef",0) && 
			def_line.Find(Macro_Name,0) > def_line.Find("undef",0))
		{
			break;
		}//遇到宏定义的终止符"#undef XXX"，跳出循环终止替换！
		
		i=def_line.Find("define",0);
		if(i>0 && i+6<def_line.Find(Macro_Name,0))
		{
			CString Macro_Nametemp=_T("");
			if(Genrate_Macro_Name(def_line,Macro_Nametemp,i)==0 && pos_inline<i)
			//如果def_line的宏定义名定义正确，且欲替换的位置在宏定义名的范围内
			{
				Is_Define_flag=0;
				//AfxMessageBox("宏定义名重复！");
			}
		}//如果替换的是另一个#define的宏定义名(Macro_Name)的全部或部分字符串，为避免混乱则不予替换！
		
		int mark_count=0;
		for(i=0; i<pos_inline; i++)
		{
			if(def_line[i]=='\"')
				mark_count++;
		}
		if(mark_count%2==1)
		{
			mark_count=0;
			for(i=pos_inline+1; i<def_line.GetLength(); i++)
			{
				if(def_line[i]=='\"')
					mark_count++;
			}
			if(mark_count%2==1)
				Is_Define_flag=0;
		}	//如果字符串Macro_Name两边的双引号个数均为单数，则不需进行宏替换(如"XXXX","Macro_Name")

		if(Is_Define_flag)
		{
			program.Delete(count,Macro_Name.GetLength());
			program.Insert(count,Macro_Value);
			count=count+Macro_Value.GetLength();
		}	//进行宏替换
		else
		{
			count=count+Macro_Name.GetLength();
		}	//跳过此行，继续扫描	
		count=program.Find(Macro_Name,count);
	}
}


bool CLexical::ParaDefine_Replace(CString &program,const int start,const CString Macro_Name,const CString Macro_Value)
{
	bool Is_Replace=0;//用于记录ParaDefine_Replace函数是否进行了替换

	int i=0,para_flag=0;//用于记录Macro_Name的括号中是否有参数
	CString Macro_Nametemp=Macro_Name;
	for(i=Macro_Name.Find('('); i<Macro_Name.GetLength()-1; i++)
	{
		if(isalnum(Macro_Name[i]) || Macro_Name[i]=='_')//isalnum(char c),c是字母或数字则返回true
		{
			para_flag=1;
			break;
		}
	}//查看Macro_Name的括号中是否有参数
	if(para_flag==0 || Macro_Nametemp.Replace('(','(')>1)
	{
		return Is_Replace;
	}//如果查看括号中没有参数，或者括号不止一对，则直接返回，按普通替换处理

	CArray<CString,CString> ParaN_Array;//用于存放宏定义名的参数列表
	i=Macro_Name.Find('(',0);
	CString Part_Name=Macro_Name.Left(i);//Part_Name存放'('前面类似于函数名的那部分字符串
	//if(Macro_Value.Find(Part_Name)>=0)
	//{
	//	return Is_Replace;
	//}//如果Macro_Value中包含Macro_Name里类似于函数名的部分，则按普通替换处理（以免造成死循环！）
	Genrate_ParaArray(Macro_Name,ParaN_Array);//根据宏定义Macro_Name名生成参数列表，存于数组ParaN_Array中
	for(i=0; i<ParaN_Array.GetSize(); i++)
	{
		if(Macro_Value.Find(ParaN_Array[i],0)<0)
			Is_Replace=0;
		else
		{
			Is_Replace=1;
			break;
		}
	}
	if(Is_Replace==0)
	{
		return Is_Replace;
	}//如果Macro_Value中没有对应的参数字符串，则直接返回，按普通替换处理
	
	/*wq-20081223-跳过剩下的#define************************/
	int defineFind = 0,enterFind = 0;
	CString strDefine = "#define";
	defineFind = program.Find("#define",start);
	while( defineFind != -1 )
	{
		defineFind += strDefine.GetLength();
		enterFind = program.Find("\n",defineFind);
		defineFind = program.Find("#define",enterFind);
	}/*************************************************************/

//	int count=program.Find(Part_Name,start);//wq-20081223-delete
	int count=program.Find(Part_Name,enterFind);//wq-20081223-add
	int programLine = program.GetLength();
	while(count>=0 && count<program.GetLength())
	{
//		AfxMessageBox(program);
		programLine = program.GetLength();
		bool Is_Define_flag=1;//该标志记录：扫描到的字符串是否可以进行替换
		int pos_inline=0;
		int pdef_end=0;
		CString def_line=Deal_SomeLine(count,pos_inline,program,0);
		i=pos_inline+Part_Name.GetLength();
		for(;i<def_line.GetLength(); i++)
		{
			if(isprint(def_line[i]))
			{
				if(def_line[i]!='(')
					Is_Define_flag=0;
				break;
			}
		}//如果与Part_Name相邻的可打印字符不是'('，则不予替换
		
		if(Is_Define_flag==1)
		{
			int Circle_count=1;
			i++;//i指向'('后的那个位置
			while(i<def_line.GetLength() && Circle_count!=0)
			{
				if(def_line[i]=='(')
					Circle_count++;
				if(def_line[i]==')')
					Circle_count--;
				if(Circle_count>1)
					Is_Define_flag=0;//如果不止一对括号，则不进行替换
				i++;
			}
			if(Circle_count!=0)
			{
				Is_Define_flag=0;//如果括号无法配对，则不进行替换
			}
			else if(Is_Define_flag==1)
			{
				pdef_end=i;//如果括号匹配，且能够此部分进行替换，则让pdef_end指向替换部分的末尾 （')'后面的位置）
			}
		}
		if(Is_Define_flag==0)//若此部分不可替换，则跳过继续扫描
		{
			count=program.Find(Part_Name,count+Part_Name.GetLength());
			continue;
		}
		
		CString Paradef_str=def_line.Mid(pos_inline,pdef_end-pos_inline);//将欲替换的的部分存入Paradef_str
		def_line.Delete(pos_inline,pdef_end-pos_inline);	
		for(i=0; i<Paradef_str.GetLength(); i++)
		{
			if(!isgraph(Paradef_str[i]))
				Paradef_str.Delete(i,1);
		}//删除与替换的字符串中中所有无法打印的字符

		CArray <CString,CString> ParaR_Array;
		Genrate_ParaArray(Paradef_str,ParaR_Array);//生成欲替换字符串的参数列表
		if(ParaN_Array.GetSize()!=ParaR_Array.GetSize())
		{
			count=program.Find(Part_Name,count+Part_Name.GetLength());
			continue;
		}//如欲替换字符串与宏定义名的参数个数不等，则不予替换，跳过继续扫描
		
		Paradef_str=Macro_Value;
		for(i=0; i<ParaN_Array.GetSize(); i++)
		{
			Paradef_str.Replace(ParaN_Array[i],ParaR_Array[i]);
		}
		def_line.Insert(pos_inline,Paradef_str);//通过两个参数列表，生成最终的字符串替换行
		Deal_SomeLine(count,pos_inline,program,1);//删除原字符串所在行
		count=count-2;
		program.Delete(count,2);//删除Deal_SomeLine函数加入的"\r\n"
		program.Insert(count,def_line);//用生成的新字符串行替换旧字符串行
		count=count+def_line.GetLength();//wq-20081223
	}
	//cout<<"|*|"<<Macro_Name<<" |^| "<<Macro_Value<<endl;
	return Is_Replace;
}

void CLexical::Genrate_ParaArray(const CString &Macro_Name, CArray<CString,CString> &Para_Array)
{
	int i=Macro_Name.Find('(',0);
	CString para_str=_T("");
	while(i+1<Macro_Name.GetLength())
	{
		int para_beg=i+1;
		i=Macro_Name.Find(',',i+1);
		if(i>=0)
		{
			para_str=Macro_Name.Mid(para_beg,i-para_beg);
			Para_Array.Add(para_str);
		}
		else
		{
			para_str=Macro_Name.Mid(para_beg,Macro_Name.GetLength()-para_beg-1);
			Para_Array.Add(para_str);
			break;//最后一个参数
		}
	}
}

void CLexical::ReSet_LineValue(CArray<TNODE,TNODE> &TArry)
//TArry中每个元素的line值加一（本词法分析从第0行计数，而大多数编译平台从第1行计数，故有时需要将所有行数加一）
{
	for(int i=0; i<TArry.GetSize(); i++)
	{
//		int line = TArry[i].line;
//		int srcLine = TArry[i].srcLine;
//		TArry[i].line++;
		TArry[i].line+=1;
	}
}