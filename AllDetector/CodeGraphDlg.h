#include "MovableButton.h"
#pragma once


// CCodeGraphDlg 对话框

class CCodeGraphDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCodeGraphDlg)

public:
	CCodeGraphDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCodeGraphDlg();

//Polaris-20150302
public:
	CMovableButton *p_MyBut;
	CMovableButton *p_MyBut2;
	CMovableButton *p_MyBut3;
	CMovableButton *p_MovableButtons[200]; //按钮数组
	bool isFirstShow;

// 对话框数据
	enum { IDD = IDD_CODE_GRAPH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg LRESULT OnVertexRepaint(WPARAM w,LPARAM l); //响应自定义消息WM_VERTEX_REPAINT
	virtual BOOL OnInitDialog();
};
