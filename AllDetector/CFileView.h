//Polaris-20140711
/*
 * 类名：CCFileView
 * 作用：仿照系统提供的FileView，自己实现一个专门显示克隆代码文件分布的CCFileView停靠窗口
 */
#pragma once

#include "CloneTreeCtrl.h"

class CCFileViewToolBar : public CMFCToolBar
{
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

class CCFileView : public CDockablePane
{
// Construction
public:
	CCFileView();

	void AdjustLayout();
	void OnChangeVisualStyle();

	//Polaris-20140710
	void AddFolder(CString strDir,HTREEITEM hParent);
	//void SetRootDir(CString strRoot);

// Attributes
public:
	CCloneTreeCtrl m_wndCCFileView;		//停靠窗口的核心控件CCloneTreeCtrl，实现了克隆代码片段树
	CImageList   m_imgList;          
	HTREEITEM    m_hCTree;

	CImageList m_FileViewImages;
	CCFileViewToolBar m_wndToolBar;

protected:
	

protected:
	void FillFileView();

// Implementation
public:
	virtual ~CCFileView();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnProperties();
	afx_msg void OnFileOpen();
	afx_msg void OnFileOpenWith();
	afx_msg void OnDummyCompile();
	afx_msg void OnEditCut();
	afx_msg void OnEditCopy();
	afx_msg void OnEditClear();
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	DECLARE_MESSAGE_MAP()
};


