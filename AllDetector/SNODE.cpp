// SNODE.cpp: implementation of the SNODE class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllDetector.h"
#include "SNODE.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SNODE::SNODE()
{
	type = -1;  
	kind = -1;    
	arrdim = -1;  
	int i;
	for( i = 0; i < 4; i++ )
	{
		arrsize[i] = -1;
	}
	addr = -1;     
	layer = -1;    
	value = 0;   
	name = "";	
	initaddr = -1;	
	taddr = -1;
	sv = -1;	  
	mv = -1;	

}

SNODE::~SNODE()
{

}
SNODE::SNODE(const SNODE& SN)
{
	type = SN.type;  
	kind = SN.kind;    
	arrdim = SN.arrdim;  
	int i;
	for( i = 0; i < 4; i++ )
	{
		arrsize[i] = SN.arrsize[i];
	}
	addr = SN.addr;     
	layer = SN.layer;    
	value = SN.value;   
	name = SN.name;	
	initaddr = SN.initaddr;	
	taddr = SN.taddr;
	sv = SN.sv;	  
	mv = SN.mv;	
}
SNODE SNODE::operator=(const SNODE& SN)
{
	type = SN.type;  
	kind = SN.kind;    
	arrdim = SN.arrdim;  
	int i;
	for( i = 0; i < 4; i++ )
	{
		arrsize[i] = SN.arrsize[i];
	}
	addr = SN.addr;     
	layer = SN.layer;    
	value = SN.value;   
	name = SN.name;	
	initaddr = SN.initaddr;	
	taddr = SN.taddr;
	sv = SN.sv;	  
	mv = SN.mv;	
	return *this;
}
