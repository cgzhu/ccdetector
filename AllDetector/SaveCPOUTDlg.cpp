// SaveCPOUTDlg.cpp : implementation file
//
/*
 * 这个类是一个对话框类，“保存克隆代码检测结果”对话框类。
 */

#include "stdafx.h"
#include "AllDetector.h"
#include "SaveCPOUTDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SaveCPOUTDlg dialog


SaveCPOUTDlg::SaveCPOUTDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SaveCPOUTDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SaveCPOUTDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void SaveCPOUTDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SaveCPOUTDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SaveCPOUTDlg, CDialog)
	//{{AFX_MSG_MAP(SaveCPOUTDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SaveCPOUTDlg message handlers

void SaveCPOUTDlg::OnOK() 
{
	// TODO: Add extra validation here
	CMainFrame *pFrame = (CMainFrame *)GetParent();
	pFrame->Ocp.Out_LCP_Pos();
	CString outFilePath = pFrame->outFilePath;
	outFilePath+="\\CP_OUT";
	MessageBox("本次克隆代码检测结果已存入文件夹\""+outFilePath+"\"中。");

	CDialog::OnOK();
}
