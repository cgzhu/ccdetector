/********************************************************************
	created:	2011/02/07
	created:	7:2:2011   16:40
	filename: 	D:\BugFinder\BugFinder\bug_list.cpp
	file path:	D:\BugFinder\BugFinder
	file base:	bug_list
	file ext:	cpp
	author:		qj
	
	purpose:	bug list
*********************************************************************/
#include "stdafx.h"
#include <vector>
using namespace std;
#include "bug_list.h"


BUG_LIST_ITEM* BugList::get(int index)
{
	if (index<0 || index>m_list.size()) return NULL;
	return &m_list[index];
}

void BugList::add(int line,LPCTSTR filename,int bug_type,CString& info)
{
	BUG_LIST_ITEM li;
	li.line = line;
	li.filename = filename;
	li.bug_type = bug_type;
	li.info = info;
	m_list.push_back(li);
}

int BugList::size(){
	return m_list.size();
}

void BugList::clear(CString filename){
	for (vector<BUG_LIST_ITEM>::iterator ite=m_list.begin();ite!=m_list.end();)
	{
		if ((*ite).filename==filename)
			ite = m_list.erase(ite);
		else
			ite++;
	}
}

void BugList::clearAll()
{
	m_list.clear();
}