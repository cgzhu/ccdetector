#pragma once
#include "rc_type_tchart.h"


// CRCFilesDiagramDlg 对话框

class CRCFilesDiagramDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CRCFilesDiagramDlg)

public:
	CRCFilesDiagramDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRCFilesDiagramDlg();

// 对话框数据
	enum { IDD = IDD_RC_FILES_DIAGRAM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart m_rc_files_chart;

	void ClearAllSeries();
};
