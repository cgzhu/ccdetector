// MovableButton.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "MovableButton.h"


// CMovableButton

IMPLEMENT_DYNAMIC(CMovableButton, CButton)

CMovableButton::CMovableButton()
{

}

CMovableButton::~CMovableButton()
{
}


BEGIN_MESSAGE_MAP(CMovableButton, CButton)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()



// CMovableButton 消息处理程序



/*
 * 函数功能：鼠标在按钮上按下时响应，为实现拖动功能
 */
void CMovableButton::OnLButtonDown(UINT nFlags, CPoint point)
{
	bMoving = true;
	
	CButton::OnLButtonDown(nFlags, point);

	SetCapture();//开始捕获鼠标事件
}

/*
 * 函数功能：鼠标在按钮上抬起时响应，为实现拖动功能
 */
void CMovableButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	bMoving = false;

	this->GetParent()->PostMessage(WM_VERTEX_REPAINT);//向父窗口对话框发送消息
	
	//CString str;
	//str.Format(_T("%d --- %d"),now_x,now_y); 
	//AfxMessageBox(str);

	//这部分代码的作用是移动按钮到鼠标位置
	ClientToScreen(&point); 
    GetParent()->ScreenToClient(&point); 
	CRect rcButton; 
	GetClientRect(&rcButton); 
	point.Offset(-rcButton.Width()/2, -rcButton.Height()/2); 
	SetWindowPos(NULL, point.x, point.y, 0, 0, SWP_NOZORDER|SWP_NOSIZE); 

	//更新按钮坐标
	now_x = point.x + 15;
	now_y = point.y + 15;

	CButton::OnLButtonUp(nFlags, point);

	ReleaseCapture(); //停止捕获鼠标事件
}
