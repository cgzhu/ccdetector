// PointerAnalysis.h: interface for the PointerAnalysis class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POINTERANALYSIS_H__EB5076B9_D0B0_4FB9_8BEA_B67B902B6EC0__INCLUDED_)
#define AFX_POINTERANALYSIS_H__EB5076B9_D0B0_4FB9_8BEA_B67B902B6EC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Unit.h"
#include "SDGAssignment.h"

extern CString GetENodeStr(ENODE *p,CArray<SNODE,SNODE> &SArry);


class PointerAnalysis  
{
public:
	PointerAnalysis();
	virtual ~PointerAnalysis();
	void operator()(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void mustAliasReplace(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
protected:
	void assignment(CAssignmentUnit *pAsign,CArray<SNODE,SNODE> &SArry);
	void replace(ENODE * &p, CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);

	void lvaluePT(ENODE*pleft, CArray<ALIAS*,ALIAS*>&ptSet,CArray<ALIAS*,ALIAS*>&Gl,CArray<ALIAS*,ALIAS*>&Cl,CArray<ALIAS*,ALIAS*>&Kl,CArray<SNODE,SNODE> &SArry);

	void rvaluePT(ENODE*pright, CArray<ALIAS*,ALIAS*>&ptSet,CArray<ALIAS*,ALIAS*>&Gr,CArray<SNODE,SNODE> &SArry);
	void dereference(CArray<ALIAS*,ALIAS*>&S,CArray<ALIAS*,ALIAS*>&ptSet,CArray<ALIAS*,ALIAS*>&R,CArray<SNODE,SNODE> &SArry);

	int issrc(ENODE*pEN,int initpos,CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry);//返回值记录在ptSet中的下标，-1表示没找到
	void InitialENODE(ENODE *pENODE);
	void address(CArray<ALIAS*,ALIAS*>&S,CArray<ALIAS*,ALIAS*>&R,CArray<SNODE,SNODE> &SArry);
	int exist(ALIAS* aliastriple,CArray<ALIAS*,ALIAS*>& ptSet,CArray<SNODE,SNODE> &SArry);
	CSDGBase* GetPrior(CSDGBase *p);
	void pointTo(CSDGBase *pCurrent,CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry);
	void unionPT(CArray<ALIAS*,ALIAS*>&ptSetUnion,CArray<ALIAS*,ALIAS*>&ptSet,CArray<SNODE,SNODE> &SArry);//将后个集合合并到前个集合中

	CString TraverseAST(ENODE*pHead,CArray<SNODE,SNODE> &SArry);


	ENODE *  CopyENODE(ENODE *pnew,ENODE *pint);
	void  CopyTNODE(TNODE &TNew,TNODE &TInt);
   ENODE * CopyTree(ENODE *pnew,ENODE *pint);

	int FindIndex(CSDGBase *pson);




};

#endif // !defined(AFX_POINTERANALYSIS_H__EB5076B9_D0B0_4FB9_8BEA_B67B902B6EC0__INCLUDED_)
