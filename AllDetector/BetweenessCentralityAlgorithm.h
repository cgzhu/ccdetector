extern int matrix[1000][1000];

extern int dist[1000][1000];
extern int S[1000];
extern int prev_node[1000][1000];

extern float ratio[1000];
extern int fenmus[1000];

#include "MainFrm.h"

#pragma once
class BetweenessCentralityAlgorithm
{
public:
	BetweenessCentralityAlgorithm(void);
	~BetweenessCentralityAlgorithm(void);
	void start(void);
};

