#pragma once


#include "Out_CpPos.h"
// CCloneTreeCtrl

class CCloneTreeCtrl : public CTreeCtrl
{
	DECLARE_DYNAMIC(CCloneTreeCtrl)

public:
	CCloneTreeCtrl();
	virtual ~CCloneTreeCtrl();

	//
	CString GetFuncSourceCode(int cpGroup,int cpSeg);
	void DeleteChildItems(HTREEITEM &m_hCTree);
	CString FormViewLine(CString strLine,int lineNum,int cpLine);
	bool Find_Code_Line(int curpos , int &cpline, CString &linePos);
	CString GetSourceCode(CString filePath,CString linePos);
	void FillTree(Out_CpPos *Ocp,CImageList &imgList,HTREEITEM &m_hCTree);

public:
	int Min_len;//wq-20090310

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
};