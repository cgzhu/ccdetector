#if !defined(AFX_SCINTILLAWND_H__21F0F213_AE69_4AB5_9DF0_057EA3B170BC__INCLUDED_)
#define AFX_SCINTILLAWND_H__21F0F213_AE69_4AB5_9DF0_057EA3B170BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScintillaWnd.h : header file
//
#include "Scintilla.h"   
#include "SciLexer.h"

/////////////////////////////////////////////////////////////////////////////
// CScintillaWnd window

class CScintillaWnd : public CWnd
{
// Construction
public:
	CScintillaWnd();

// Attributes
public:

// Operations
public:
	virtual BOOL Create(
        DWORD dwExStyle, DWORD dwStyle,const RECT& rect,    
        CWnd* pParentWnd, UINT nID);
	void SetText (LPCSTR szText);
	void GetText (CString &strText);
	LPSTR GetText();
	void UpdateUI();
	void GotoPosition(long lPos);
	void CScintillaWnd::GotoLine(long lLine);
	BOOL Save(LPCTSTR lpszPathName);
	BOOL Load(LPCTSTR lpszPathName);
	int GetLength();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScintillaWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CScintillaWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CScintillaWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCINTILLAWND_H__21F0F213_AE69_4AB5_9DF0_057EA3B170BC__INCLUDED_)
