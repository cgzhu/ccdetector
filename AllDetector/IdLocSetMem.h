// IdLocSetMem.h: interface for the IdLocSetMem class.
// 作者：王倩
// 时间：2008-12-23
// 功能：标识符位置信息结构
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IdLocSetMem_H__5609392F_5808_4DDF_839B_035549E2C134__INCLUDED_)
#define AFX_IdLocSetMem_H__5609392F_5808_4DDF_839B_035549E2C134__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"

struct IDENTF_LOCATION
{
//	int cpLine;//wq-20090305-重复代码相对行数
	int lineIndex;//所在行数(从0开始)(重复代码相对行数)
	int tokenNum; //tokenLine的第几个token字(从0开始)
};

class IdLocSetMem  
{
	friend class CP_SEG;
	friend class CP_Segment;
	friend class DupCodesSet;
	friend class DupPairCnflctRatio;
public:
	IdLocSetMem();
	virtual ~IdLocSetMem();
	IdLocSetMem(const IdLocSetMem& IPSM);
	IdLocSetMem& operator=(const IdLocSetMem& IPSM);
	CString &GetIdName();
	CArray<IDENTF_LOCATION,IDENTF_LOCATION>& GetIdLocArry();
	void Assign(CString& p_idName,CArray<IDENTF_LOCATION,IDENTF_LOCATION>& p_idLocArry);

private:
	CString idName;//标识符名称
	//该标识符在克隆代码片断中的所有出现位置集合
	CArray<IDENTF_LOCATION,IDENTF_LOCATION> idLocArry;
};

#endif // !defined(AFX_IdLocSetMem_H__5609392F_5808_4DDF_839B_035549E2C134__INCLUDED_)
