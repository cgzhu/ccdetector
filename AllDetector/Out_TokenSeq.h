// Out_TokenSeq.h: interface for the Out_TokenSeq class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_Out_TokenSeq_H__609B48DA_3628_433D_B92F_A4A7B943078B__INCLUDED_)
#define AFX_Out_TokenSeq_H__609B48DA_3628_433D_B92F_A4A7B943078B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"
#include "FilesToTokens.h"
#include "HeaderFileElem.h"

class Out_TokenSeq  
{
public:
	long Line_Sum;//用于记录分析的所有c文件的总行数
	int Cfile_Sum;
//	FilesToTokens filesToTokens;//wq-20090319
	CString Obj_file_path;//wq-20090319
	CString out_file_path;//wq-20090402

public:
	bool ReadCFile(const CString cfilename,CString &program);
	Out_TokenSeq();
	virtual ~Out_TokenSeq();

	void CFILE_TO_TFILE(FilesToTokens &filesToTokens,//wq-20090319
		const CString readpath, CArray <Seq_NODE,Seq_NODE> &Seq_Arry_G,
		CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
		CArray<CH_FILE_INFO,CH_FILE_INFO> &filesReadInfo//wq-20090610
		);
	//读取目录readpath下的所有C文件，并输出对应的token文件
	BOOL READ_CHFILE(const CString cfilename,CString &program);
	//读入文件路径为cfilename的c文件，写入program

private:
	CString GENERATE_TOKEN(CArray<TNODE,TNODE> &TArry,const CArray<SNODE,SNODE> &SArry,const CArray<FNODE,FNODE> &FArry);
	//将TArry中的数据专化为token并作为返回值返回，并存于TArry中的name项中
	void VARIABLE_TYPE( CString &str, const int &type,const int &kind);
	//生成各种类型变量的token符（没考虑函数返回类型为void的情况!!!）
	CString GENERATE_SEQ(const CArray<TNODE,TNODE> &TArry, const CArray <FL_NODE,FL_NODE> &FL_Arry, CArray<Seq_NODE,Seq_NODE> &Seq_Arry,const CString c_filename);
	//将所有的TArry[i].name转化为数字序列并作为返回值返回，同时将其存于Seq_Arry
	CString Token_to_Seq(const CString &token);
	//使用散列算法hashpjw，将token串转换为数字序列
	void PUTOUT_TOKENALL(const CArray<TNODE,TNODE> &TArry,const CArray<SNODE,SNODE> &SArry,const CArray<FNODE,FNODE> &FArry);
	//测试函数，无用！
	void OUTPUT_SEQ(FilesToTokens &filesToTokens,//wq-20090319
		const CString c_filename, CArray <Seq_NODE,Seq_NODE> &Seq_Arry_G,
		CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
		CArray<CH_FILE_INFO,CH_FILE_INFO> &filesReadInfo//wq-20090610
		);
	//调用其它函数，生成与名为c_filename文件相对应的数字序列
	void WRITE_SEQFILE(const CString str_token,const CString cFilePath);
	//将序列字符串写入对应名称的txt文件中，存在D:\Seq_Out\目录下（文件命名规则：将与token对应的c文件的路径名作为其文件名，由于文件名中无法包含':'和'\'，故将其替换为'^'和'$'）；
	void WRITE_TOKENFILE(const CString str_token,const CString cFilePath);
    //将Token流写入对应名称的txt文件中，存在D:\Token_Out\目录下（文件命名规则：将与token对应的c文件的路径名作为其文件名，由于文件名中无法包含':'和'\'，故将其替换为'^'和'$'）；
	void Div_Block(CArray <TNODE,TNODE> &TArry,CArray <FL_NODE,FL_NODE> &FL_Arry);
	//分块函数：调用下面9个函数
	void Mark_Flower(int Bbeg,int Bend,const int Flower_layer,CArray <TNODE,TNODE> &TArry,CArray <FL_NODE,FL_NODE> &FL_Arry);
	//根据TArry中的'{''}'对其进行分块，分块信息记录在FL_Arry中
	bool Add_BlockHead(const int &i,CArray <FL_NODE,FL_NODE> &FL_Arry,const CArray <TNODE,TNODE> &TArry);
	//将模块前的条件判断语句加入模块中
	bool Find_LRCIRCLE(int &index, const CArray <TNODE,TNODE> &TArry);
	//定位'{'前面的配对儿'('')'
	bool Add_Semicolon(const int &i,CArray <FL_NODE,FL_NODE> &FL_Arry,const CArray <TNODE,TNODE> &TArry);
	//将某些模块后的分号加入模块中
	int  Count_Line(const int &beg, const int &end, const CArray <TNODE,TNODE> &TArry);
	//统计在TArry中从beg到end的行数
	void Reset_Block(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry);
	//将各模块间的同层散语句合并成块,并将所有的模块排序
	FL_NODE	INIT_FL_NODE(const int beg, const int end, const int layer, const int Blength);
	//返回一个初始值为beg,end,layer,Blength的FL_NODE节点
	void Deal_Flower(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry);
	//重新为Blength赋值（单行的'{'或'}'不计入段长度）;  将'{'合并到下一段，将'}'合并到上一段
	void Combin_Block(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry);
	//合并同层的小模块
	void Combin_NextBlock(int pos, CArray <FL_NODE,FL_NODE> &FL_Arry);
	//将位置为pos和pos+1的模块合并
	bool Deal_DoWhile(const int &i,CArray <FL_NODE,FL_NODE> &FL_Arry,const CArray <TNODE,TNODE> &TArry);
	//处理do—while模块
	void Div_SwitchBlock(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry);
	//对含有switch的模块再分块
//	void OutSeqToken(const CArray <Seq_NODE,Seq_NODE> &Seq_Arry_G);//wq-20081114-输出序列项和对应的每行源程序（由token中得到），测试存储的token信息是否正确


};

#endif // !defined(AFX_Out_TokenSeq_H__609B48DA_3628_433D_B92F_A4A7B943078B__INCLUDED_)