// SDGCall.h: interface for the CSDGCall class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGCALL_H__C0D7C630_648E_4636_877B_A000847DC41F__INCLUDED_)
#define AFX_SDGCALL_H__C0D7C630_648E_4636_877B_A000847DC41F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGCall : public CSDGBase  
{
public:
	CSDGCall();
	virtual ~CSDGCall();
public:
	TNODE m_tFun;

};

#endif // !defined(AFX_SDGCALL_H__C0D7C630_648E_4636_877B_A000847DC41F__INCLUDED_)
