
// AllDetectorDoc.cpp : implementation of the CAllDetectorDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "AllDetector.h"
#endif

#include "AllDetectorDoc.h"
#include "MySciEditView.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAllDetectorDoc

IMPLEMENT_DYNCREATE(CAllDetectorDoc, CDocument)

BEGIN_MESSAGE_MAP(CAllDetectorDoc, CDocument)
END_MESSAGE_MAP()


// CAllDetectorDoc construction/destruction

CAllDetectorDoc::CAllDetectorDoc()
{
	// TODO: add one-time construction code here

}

CAllDetectorDoc::~CAllDetectorDoc()
{
}

BOOL CAllDetectorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CAllDetectorDoc serialization

void CAllDetectorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CAllDetectorDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CAllDetectorDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CAllDetectorDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CAllDetectorDoc diagnostics

#ifdef _DEBUG
void CAllDetectorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CAllDetectorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CAllDetectorDoc commands

//Polaris-20140716
BOOL CAllDetectorDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	//Polaris-20140716
	//检测是否已经打开
	POSITION pos = GetFirstViewPosition();
	CView *pView;
	while ((pView = GetNextView(pos))) {
		if (pView->GetDocument()->GetPathName()==lpszPathName){ //已经打开过？
			 ((CMySciEditView*)pView)->m_ScintillaWnd.SetFocus();
			 ((CMySciEditView*)pView)->RefreshAnnotations();
			 return TRUE;
		}
	}

	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	// TODO:  在此添加您专用的创建代码

	return TRUE;
}

//Polaris-20140716
BOOL CAllDetectorDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	//Polaris-20140716
	POSITION pos = GetFirstViewPosition();
	
	CMySciEditView *pFirstView = (CMySciEditView*)GetNextView( pos ); // assume only one view
	if (pFirstView != NULL)
		return pFirstView->m_ScintillaWnd.Save(lpszPathName);

	return CDocument::OnSaveDocument(lpszPathName);
}
