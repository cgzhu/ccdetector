// SDGAssignment.cpp: implementation of the CSDGAssignment class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGAssignment.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSDGAssignment::CSDGAssignment()
{
	m_sType="SDGASSIGNMENT";
	visit=0;
	vaddr=-1;
	m_pleft=NULL;
//	AfxMessageBox("SDGAssignment");

}

CSDGAssignment::~CSDGAssignment()
{

}
