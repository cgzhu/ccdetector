// MySciEditView.cpp : 实现文件
//

#include "stdafx.h"
#include "MySciEditView.h"
#include "MainFrm.h"
#include "ClassView.h"
#include "resource.h"
#include "routine.h"

// CMySciEditView

IMPLEMENT_DYNCREATE(CMySciEditView, CView)

	CMySciEditView::CMySciEditView()
{
	CMainFrame *pMain = (CMainFrame *) AfxGetMainWnd();
	ASSERT_VALID(pMain);
	pMain->m_edit_view.push_back(this);
}

CMySciEditView::~CMySciEditView()
{
	CMainFrame *pMain = (CMainFrame *) AfxGetMainWnd();
	ASSERT_VALID(pMain);
	for(vector<CMySciEditView*>::iterator ite = pMain->m_edit_view.begin();ite!=pMain->m_edit_view.end();ite++)
	{
		if (*ite==this)
		{
			pMain->m_edit_view.erase(ite);
			break;
		}
	}
}

BEGIN_MESSAGE_MAP(CMySciEditView, CView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateNeedSel)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateNeedPaste)	
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR, OnUpdateNeedTextAndFollowingText)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECT_ALL, OnUpdateNeedText)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_PARSE, &CMySciEditView::OnParseX)
	ON_COMMAND(ID_DETECT_RC, &CMySciEditView::OnFindbugX)
	ON_UPDATE_COMMAND_UI(ID_DETECT_RC, &CMySciEditView::OnUpdateFindbugX)
	ON_UPDATE_COMMAND_UI(ID_PARSE, &CMySciEditView::OnUpdateParseX)
END_MESSAGE_MAP()


// CMySciEditView 绘图

void CMySciEditView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: 在此添加绘制代码
}


// CMySciEditView 诊断

#ifdef _DEBUG
void CMySciEditView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CMySciEditView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


void CMySciEditView::OnSize(UINT nType, int cx, int cy)
{
	CSciEditView::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
}


int CMySciEditView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSciEditView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码	

	return 0;
}


void CMySciEditView::OnUpdateNeedSel(CCmdUI *pCmdUI)
{
	int nStartChar = m_ScintillaWnd.SendMessage(SCI_GETSELECTIONSTART);
	int nEndChar = m_ScintillaWnd.SendMessage(SCI_GETSELECTIONEND);
	pCmdUI->Enable(nStartChar != nEndChar);
}


void CMySciEditView::OnUpdateNeedPaste(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_ScintillaWnd.SendMessage(SCI_CANPASTE));
}

void CMySciEditView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_ScintillaWnd.SendMessage(SCI_CANUNDO));
}

void CMySciEditView::OnEditCut()
{
	m_ScintillaWnd.SendMessage(SCI_CUT);
}


void CMySciEditView::OnEditCopy()
{
	m_ScintillaWnd.SendMessage(SCI_COPY);
}


void CMySciEditView::OnEditPaste()
{
	m_ScintillaWnd.SendMessage(SCI_PASTE);
}


void CMySciEditView::OnEditClear()
{
	m_ScintillaWnd.SendMessage(SCI_CLEAR);
}


void CMySciEditView::OnEditUndo()
{
	m_ScintillaWnd.SendMessage(SCI_UNDO);
}


void CMySciEditView::OnEditRedo()
{
	m_ScintillaWnd.SendMessage(SCI_REDO);
}


void CMySciEditView::OnEditSelectAll()
{
	m_ScintillaWnd.SendMessage(SCI_SELECTALL);
}


void CMySciEditView::OnUpdateNeedTextAndFollowingText(CCmdUI *pCmdUI)
{	  
	int nLength =  m_ScintillaWnd.SendMessage(SCI_GETTEXTLENGTH);
	int nStartChar = m_ScintillaWnd.SendMessage(SCI_GETSELECTIONSTART);

	pCmdUI->Enable(nLength && (nStartChar != nLength));
}

void CMySciEditView::OnUpdateNeedText(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_ScintillaWnd.SendMessage(SCI_GETTEXTLENGTH) != 0);
}

void CMySciEditView::OnUpdateEditCopy(CCmdUI *pCmdUI)
{
	OnUpdateNeedSel(pCmdUI);
}

//保存文件
BOOL CMySciEditView::Save(LPCTSTR lpszPathName)
{
	return m_ScintillaWnd.Save(lpszPathName);
}

static THREAD_INFO g_ti;

/*
 * 函数功能：点击菜单项“语法分析”的消息响应函数
 */
void CMySciEditView::OnParseX()
{
	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
	if (pMain->m_bBeginFindBugThread)
	{
		pMain->m_bBeginFindBugThread = FALSE;
	}
	else
	{
		if (m_ScintillaWnd.GetLength()==0) return ;
		pMain->m_bBeginFindBugThread = TRUE;
		g_ti.pMainFrame = pMain;
		g_ti.strFile = GetFileName();
		AfxBeginThread(GetASTThread, &g_ti);
	}
}

/*
 * 函数功能：点击菜单项“缺陷检测”的消息响应函数
 */
void CMySciEditView::OnFindbugX()
{
	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
	if (pMain->m_bBeginFindBugThread)
	{
		pMain->m_bBeginFindBugThread = FALSE;
	}
	else
	{
		if (m_ScintillaWnd.GetLength()==0) return ;
		pMain->m_bBeginFindBugThread = TRUE;
		g_ti.pMainFrame = pMain;
		g_ti.strFile = GetFileName();
		AfxBeginThread(FindBugThread, &g_ti);
	}
}


void CMySciEditView::OnUpdateFindbugX(CCmdUI *pCmdUI)
{
	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
	pCmdUI->SetText("缺陷检测 "+GetDocument()->GetTitle());

	if (pMain->m_bBeginFindBugFolderThread || pMain->m_bBeginFindBugThread) //正在检测?
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		pCmdUI->Enable(m_ScintillaWnd.GetLength()>0);
	}
}

void CMySciEditView::RefreshAnnotations()
{
	CString filename = GetDocument()->GetPathName();
	if (filename.GetLength()==0) return ;
	m_ScintillaWnd.SendMessage(SCI_ANNOTATIONCLEARALL);
	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
	for (int i=0;i<pMain->m_buglist.size();i++)
	{
		BUG_LIST_ITEM* bl = pMain->m_buglist.get(i);
		if (bl->filename==filename)
		{
			m_ScintillaWnd.SendMessage(SCI_ANNOTATIONSETTEXT,bl->line-1,(LPARAM) bl->info.GetBuffer(bl->info.GetLength()));
		}
	}
}

void CMySciEditView::OnInitialUpdate()
{
	CSciEditView::OnInitialUpdate();

	RefreshAnnotations();
}

CString CMySciEditView::GetFileName()
{
	if (GetDocument()->GetPathName().GetLength()>0) 
	{
		return GetDocument()->GetPathName();
	}
	
	char szTempPath[MAX_PATH];

	GetTempPath(MAX_PATH, szTempPath);
	strcat_s(szTempPath,"TMP.c");
	m_ScintillaWnd.Save(szTempPath);
	GetDocument()->SetPathName(szTempPath);

	return szTempPath;
}

void CMySciEditView::OnUpdateParseX(CCmdUI *pCmdUI)
{
	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
	pCmdUI->SetText("语法分析 "+GetDocument()->GetTitle());

	if (pMain->m_bBeginFindBugFolderThread || pMain->m_bBeginFindBugThread) //正在检测?
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		pCmdUI->Enable(m_ScintillaWnd.GetLength());
	}
}
