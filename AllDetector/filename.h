/********************************************************************
	created:	2011/02/07
	created:	7:2:2011   10:12
	filename: 	D:\BugFinder\BugFinder\filename.h
	file path:	D:\BugFinder\BugFinder
	file base:	filename
	file ext:	h
	author:		qj

	purpose:	文件名
*********************************************************************/
#ifdef _FILE_EXTERN
#define _FILE_INCLUDE_PREFIX extern
#else
#define _FILE_INCLUDE_PREFIX
#endif
_FILE_INCLUDE_PREFIX TCHAR PREPROCESSED_FILE[MAX_PATH];//预处理过的源文件
_FILE_INCLUDE_PREFIX TCHAR IDEOPERDECT_FILE[MAX_PATH];//幂等缺陷结果文件
_FILE_INCLUDE_PREFIX TCHAR REDUASSIGNDECT_FILE[MAX_PATH];//冗余赋值
_FILE_INCLUDE_PREFIX TCHAR DEADCODEDECT_FILE[MAX_PATH];//死代码
_FILE_INCLUDE_PREFIX TCHAR REDUCONDDECT_FILE[MAX_PATH];//冗余条件表达式
_FILE_INCLUDE_PREFIX TCHAR HIDEOPERDECT_FILE[MAX_PATH];//隐式幂等表达式
_FILE_INCLUDE_PREFIX TCHAR BUG_DETECT_IMIDIATE_FILE[MAX_PATH];//幂等缺陷结果文件
_FILE_INCLUDE_PREFIX TCHAR STRUCT_NAME_FILE[MAX_PATH];
_FILE_INCLUDE_PREFIX TCHAR TYPE_NAME_FILE[MAX_PATH];
_FILE_INCLUDE_PREFIX TCHAR AST_FILE[MAX_PATH];//AST文件
_FILE_INCLUDE_PREFIX TCHAR REDUPARAMETER_FILE[MAX_PATH];

void get_filenames();
void delete_files();