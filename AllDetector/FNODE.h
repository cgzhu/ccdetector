// FNODE.h: interface for the FNODE class.
// 王倩-20090505
// 函数列表节点结构
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FNODE_H__2811C520_7F3E_4976_AC56_8861B5D6F36D__INCLUDED_)
#define AFX_FNODE_H__2811C520_7F3E_4976_AC56_8861B5D6F36D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class FNODE  
{
public:
	FNODE();
	virtual ~FNODE();
	FNODE(const FNODE& FN);
	FNODE operator=(const FNODE& FN);
public:
	int     fid;        // id of this function
	int     rtype;      // return type  
	int     rkind;      // return kind 
	int     pnum;       // parameter number
	int     pbeg;       // begnning position(as a token word in token list) of parameter 
	int     pend;       // end....  
	int     plist[20];  // parameter list(stores the parameter symbol table address)
	int     bbeg;       // function body beginning position
	int     bend;       // ....          end   
    int     calleelist[20];// the fid of its callees
	int     calleenum;// the number of its callees//wtt/////
	int     callednum;// the number of called by all its caller///wtt///
	CString name;       // name of this function
	int expanded;
};

#endif // !defined(AFX_FNODE_H__2811C520_7F3E_4976_AC56_8861B5D6F36D__INCLUDED_)
