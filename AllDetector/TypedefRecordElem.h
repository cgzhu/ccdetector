// TypedefRecordElem.h: interface for the TypedefRecordElem class.
// 作者：王倩
// 时间：20090524
// 功能：typedef定义记录节点结构
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TYPEDEFRECORDELEM_H__89733583_DCB7_4945_B2A1_74CB32CBD893__INCLUDED_)
#define AFX_TYPEDEFRECORDELEM_H__89733583_DCB7_4945_B2A1_74CB32CBD893__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "TNODE.h"

class TypedefRecordElem  
{
public:
	TypedefRecordElem();
	virtual ~TypedefRecordElem();

public:
	CString typeRename;
	CArray<TNODE,TNODE> typeTArry;

public:
	void Clean();
	TypedefRecordElem( const TypedefRecordElem &typdfRcdElm);
	TypedefRecordElem operator=(const TypedefRecordElem &typdfRcdElm);

};

#endif // !defined(AFX_TYPEDEFRECORDELEM_H__89733583_DCB7_4945_B2A1_74CB32CBD893__INCLUDED_)
