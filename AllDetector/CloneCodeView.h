#pragma once


// CCloneCodeView 视图

class CCloneCodeView : public CEditView
{
	DECLARE_DYNCREATE(CCloneCodeView)

protected:
	CCloneCodeView();           // 动态创建所使用的受保护的构造函数
	virtual ~CCloneCodeView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	virtual void OnDraw(CDC* /*pDC*/);
};


