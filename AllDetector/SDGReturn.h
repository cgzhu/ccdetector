// SDGReturn.h: interface for the CSDGReturn class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGRETURN_H__195E5903_B3B4_4632_9A77_D298306C5702__INCLUDED_)
#define AFX_SDGRETURN_H__195E5903_B3B4_4632_9A77_D298306C5702__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"
class CSDGReturn : public CSDGBase  
{
public:
	CSDGReturn();
	virtual ~CSDGReturn();
public:
	int m_iValue;

};

#endif // !defined(AFX_SDGRETURN_H__195E5903_B3B4_4632_9A77_D298306C5702__INCLUDED_)
