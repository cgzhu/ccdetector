// SDGFor.h: interface for the CSDGFor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGFOR_H__5B0EF8A6_63C9_4CCD_9EB3_A917224D108E__INCLUDED_)
#define AFX_SDGFOR_H__5B0EF8A6_63C9_4CCD_9EB3_A917224D108E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGFor : public CSDGBase  
{
public:
	CSDGFor();
	virtual ~CSDGFor();
public:
	int FORPOS;
	int expbeg;
	int expend;
	int DOUPOS1;
	int DOUPOS2;
	CArray<TNODE,TNODE> TCnt1;
	CArray<TNODE,TNODE> TCnt2;
	CArray<TNODE,TNODE> TCnt3;

};

#endif // !defined(AFX_SDGFOR_H__5B0EF8A6_63C9_4CCD_9EB3_A917224D108E__INCLUDED_)
