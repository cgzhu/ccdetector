// VarRemove.h: interface for the CVarRemove class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VARREMOVE_H__8D7D4B77_8695_4DC7_957E_B846A55C0BF8__INCLUDED_)
#define AFX_VARREMOVE_H__8D7D4B77_8695_4DC7_957E_B846A55C0BF8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Unit.h"

class CVarRemove  
{
	friend void    DeleteBranch(CSDGBase *phead);
//	friend CString GetExpString(ENODE *pEHead,bool bNIDX=true);//wtt//3.9
	friend bool    IsDepended(CSDGBase *pNode1,CSDGBase *pNode2);
//	friend void  ExDeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);////3.17
  friend void ExSetInOutDataFlow(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);

	friend void ExRenameAndReorder(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);///wtt//3.17
 friend void RemoveEmptyBranchEx(CSDGBase *pHead);
public:
	CVarRemove();
	virtual ~CVarRemove();
public:
	void operator()(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
protected:
	CSDGBase *m_pUSDGHead;
private:
	bool modified;
protected:

	void InitialENODE(ENODE *pENODE);
	//////////////////控制结构标准化///////////////////////////
	void ChangeElseToSelector(CSDGBase *pHead);///wtt//3.7//
	void TrueInSelection(CSDGBase*p);
//	void FalseInSelection(CSDGBase*p);

	void SubstractCommenBranch(CSDGBase*p,CArray<SNODE,SNODE> &SArry);
	bool CompareBranches(CSDGBase*p1,CSDGBase*p2,CArray<SNODE,SNODE> &SArry);
//	bool CompareTrees(ENODE*pHead1,ENODE*pHead2,CArray<SNODE,SNODE> &SArry);
//	void RearrangeSelector(CSDGBase*p);
	void CombineSelection(CSDGBase*p,CArray<SNODE,SNODE> &SArry);
	void MergeSelection(CSDGBase*p,CArray<SNODE,SNODE> &SArry);
	void NestedSelection(CSDGBase *pHead,CArray<SNODE,SNODE>&SArry);
	void DirectNestedSel(const int idx,CSDGBase *pFat,CSDGBase *pNest,CArray<SNODE,SNODE>&SArry);
	//	CSDGBase* IsNestedSelection(CSDGBase *pHead);
	void NestedSelectionNode(CSDGBase *pHead,CArray<SNODE,SNODE>&SArry);
//	void TransformNestedSelection(CSDGBase *pHead,CSDGBase *pnest);
//	int  FindIndex(CSDGBase *pson);
	// end of this part

	void ChangeIteration(CSDGBase*p);
	void FalseIteration(CSDGBase*p);
	void RemoveInvariableInIteration(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
////////////wtt///05.10.20/////
   void   StandLoopCondition(CSDGBase*p);


	void RemoveContinue(CSDGBase*p,CArray<SNODE,SNODE>&SArry);
	int NeedTransformContinue(CSDGBase*p);

	void RemoveReturn(CSDGBase*p,CArray<SNODE,SNODE>&SArry);
	void RemoveReturnInFuction(CSDGBase*p,CArray<SNODE,SNODE>&SArry);
	int NeedTransformReturn(CSDGBase*p);

	void RemoveBreak(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
	int NeedTransformBreak(CSDGBase*p);
	bool IsDependent(CSDGBase*psel,int selpos,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
//////////////////控制结构标准化///////////////////////////	

//////////////////////////////设置def和ref集合//////////////
	// Set or search in-data flow and out-data flow
	void SetInOutDataFlow(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	void SearchChildrenDataFlow(CSDGBase *pFnd,CArray<SNODE,SNODE> &SArry);
	void SearchExpReferData(CSDGBase *pFnd,ENODE *pHead,CArray<TNODE,TNODE> &DArry,CArray<SNODE,SNODE> &SArry);
	void SetFunctionDefinedData(CSDGBase *pFnd,ENODE *pHead,CArray<SNODE,SNODE> &SArry);
	void GetSonsDataInfo(CSDGBase *pNode);
	bool FindTNODEInDArry(TNODE &TD,CArray<TNODE,TNODE> &DArry);
	int  DefOrRefVariable(TNODE &TD,CSDGBase *pNode);
	int  DefOrRefVariableEx(TNODE &TD,CSDGBase *pNode);
	///////////////////////////设置def和ref集合/////////////////////////

	///////////////////////函数内联有关的函数//////////
//	void FindRightSDGNode(CSDGBase*p,CArray<CSDGBase*,CSDGBase*>&prList);
	
//	void RemoveExpInReturn(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);

//	void RemoveCallInExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);

//	void FindCall(ENODE*pExp,CArray<ENODE*,ENODE*>&pCList,CArray<ENODE*,ENODE*> &pFList,CArray<int,int>&posList);


//	void RemoveParaExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);

//	int IsSimple1(ENODE*p);/////////////////


	
	
	///////////////////////////////////////////
	//	void DefAndRefVariable(CArray<TNODE,TNODE> &DArry,CArray<TNODE,TNODE> &RArry,CSDGBase *pNode);
	// end of this part
		
	// Function group: Variations remove from USDG
//	void NoReferedUnit(CSDGBase *pHead);
//	void RemoveNotReferedOfDA(CSDGBase *pHead);

	// some other variation removing rules
//	void RefThanOne();
//	void MoreControl();
//	void RefBySelection();
//	void RefByIteration();
//	void DefineBySelection();
	// end of this part

	// rule:simplify unit
//	void FindIterationSelection(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
//	bool SelItrCanSimplify(CSDGBase *pNode);

	// rule: simplify complex iteration
//	void SimplifyIteration(CSDGBase *pItr);
	// end of this part
	// rule: simplify complex selection
//	void SimplifySelection(CSDGBase *pItr);
	// end of this part




	// 
//	void BoolRedundantRemove(CSDGBase *pItr,CArray<SNODE,SNODE> &SArry);
//	bool FindBoolRedundant(ENODE *ptr1,ENODE *ptr2,ENODE *pftr1,ENODE *pftr2,CArray<SNODE,SNODE> &SArry);
//	bool FindBoolRedundantC(ENODE *ptr1,ENODE *ptr2,ENODE *pftr1,ENODE *pftr2,CArray<SNODE,SNODE> &SArry);
//	void DeleteBoolPart(ENODE *pftr,ENODE * ptr);
	// rename variables
//////////////////////////////变量重命名////////////////////////
	void FindVariableInSDG(CString name,TNODE &TD,CSDGBase *pUSDGHead);
	void VariablesRename(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	void GetFrequency(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void SetFrequencyInfo(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void FrequencyOfExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &Freq);
	void ResetVarName(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void AssignNewName(CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void RenameVarInExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	// end of this part
//////////////////////////////变量重命名////////////////////////


/////////////////////各辅助函数////////////////////////
    void CopyTNODE(TNODE &T,TNODE &TINT);
	bool isConstOfE(ENODE *p);
	bool isIncOrDec(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);
//    void PreOrder(CSDGBase *pHead);
	void DeleteBranch(CSDGBase *phead);
//	void DeleteBranch1(CSDGBase *phead);

	void DeleteExpTree(CSDGBase *ptr);
//	void DeleteExpTree1(ENODE*p);

//	void DeleteExpTree(ENODE *ptr);
	void RemoveEmptyBranch(CSDGBase *pHead);
/////////////////////各辅助函数////////////////////////

	/////////////删除未被引用的变量赋值和声明语句//////////////////
/*	void  DeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry);////3.17

	void RemoveNotReferedVar(CSDGBase *pHead);
	void RemoveNotReferedAssignment(CSDGBase *p,CSDGBase *pHead,CArray<SNODE,SNODE>&SArry);
	void DeleteIdentity(CSDGBase *p,CArray<SNODE,SNODE>&SArry);

	void RemoveSimpleStatement(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
	void RemoveVarStatement(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
	void RemoveConstantStatement(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
*/
//	int CVarRemove::IsSimple(ENODE*p);
//	bool ContainVar(ENODE*p ,ENODE*pExp,CArray<SNODE,SNODE>&SArry);
//	void ReplaceVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry);


	/////////////删除未被引用的变量赋值和声明语句//////////////////

	
	////////////////////////重排语句的顺序//////////////////////
//	void RenameAndReorder(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry);

//	void SetSDGNodeName(CSDGBase *pSHead);
//	CString GetExpString(ENODE*pHead);///////wtt///3.9
//	CString GetENodeStr(ENODE*p);///wtt//3.9
//	CString GetExpString(ENODE *pEHead,bool bNIDX=true);//wtt//3.9
//	CString GetENodeStr(ENODE *pENode);///wtt//3.9//

	// statements order reset
//	void ResetStatementOrder(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
//	void ResetChildrenOrder(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);
//	void ResetRangeOrder(const int bpos,const int epos,CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);	
//	void MaxNoDependentSet(int &ibound,const int bpos,const int epos,CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);
	bool IsDepended(CSDGBase *pNode1,CSDGBase *pNode2);
//	CSDGBase *SearchFirstCertainNode(CSDGBase *pSDG,CString strNode);
////////////////////////重排语句的顺序//////////////////////
	// divide the parameter of function scanf;
	//void ScanfDivide(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);


	/////////////////////////将输入、输出语句进行划分//////////////////
	void ScanfPrintfDivide(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);////wtt//3.9/
/////////////////////////将输入、输出语句进行划分//////////////////
};
int  FindIndex(CSDGBase *pson);///wtt///3.17//
//////////////////wtt2006.3.21/////////////////////
CString GetExpString(ENODE*pHead);///////wtt///3.17//
CString GetENodeStr(ENODE*p);///wtt
CString GetExpString(ENODE*pHead, CArray<SNODE,SNODE>&SArry);///////wtt///3.17//wtt//2006.3.21
CString GetENodeStr(ENODE*p, CArray<SNODE,SNODE>&SArry);///wtt/2006.3.21
////////////////////////////////////////////
int IsSimple(ENODE*p);
void ExSetInOutDataFlow(CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);

#endif // !defined(AFX_VARREMOVE_H__8D7D4B77_8695_4DC7_957E_B846A55C0BF8__INCLUDED_)
