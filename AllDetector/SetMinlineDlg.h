#pragma once


// CSetMinlineDlg 对话框

class CSetMinlineDlg : public CDialog
{
	DECLARE_DYNAMIC(CSetMinlineDlg)

public:
	CSetMinlineDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSetMinlineDlg();

// 对话框数据
	enum { IDD = IDD_SET_MINLINE_DLG };

	//Polaris-20140712
	CEdit minlineEditBox;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
