#pragma once

// SciEditView.h : header file
//
#include "ScintillaWnd.h"

/////////////////////////////////////////////////////////////////////////////
// CSciEditView view

class CSciEditView : public CView
{
protected:
	CSciEditView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSciEditView)

// Attributes
public:
	CScintillaWnd m_ScintillaWnd; 
// Operations
public:
	void UpdateLineNumberWidth();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSciEditView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSciEditView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CSciEditView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.