// SDGContinue.cpp: implementation of the CSDGContinue class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGContinue.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSDGContinue::CSDGContinue()
{
	m_sType="SDGCONTINUE";
	visit=0;

}

CSDGContinue::~CSDGContinue()
{

}
