
// AllDetectorView.h : interface of the CAllDetectorView class
//

#pragma once


class CAllDetectorView : public CView
{
protected: // create from serialization only
	CAllDetectorView();
	DECLARE_DYNCREATE(CAllDetectorView)

// Attributes
public:
	CAllDetectorDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CAllDetectorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in AllDetectorView.cpp
inline CAllDetectorDoc* CAllDetectorView::GetDocument() const
   { return reinterpret_cast<CAllDetectorDoc*>(m_pDocument); }
#endif

