// FormatTokenLines.cpp: implementation of the FormatTokenLines class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FormatTokenLines.h"
#include <iostream>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FormatTokenLines::FormatTokenLines()
{

}

FormatTokenLines::~FormatTokenLines()
{

}

void FormatTokenLines::operator ()(CArray<TNODE,TNODE> &TArry,CArray<SRCLINE,SRCLINE> &srcCode)
{
	CArray<TNODE,TNODE> tempTArry,tempLine;
	tempTArry.RemoveAll();
	tempLine. RemoveAll();
	int ix, lineBeg = 0, lineEnd = 0;
//	int lineCount = TArry[0].line;
	int lineCount = 1;
	for( ix = 0; ix < TArry.GetSize(); ix++ )
	{
		lineBeg = ix;
		int key = TArry[ix].key;
		int line = TArry[ix].line;
/*		if( lineCount < TArry[ix].line )
		{
			lineCount = TArry[ix].line;
		}*/

		/*识别句首token字，找到与之相对应的句尾token字，确定同一行代码的范围*/
		switch(key)
		{
			case CN_WELL:				
				for( lineEnd=ix+1; lineEnd<TArry.GetSize(); lineEnd++)
				{
					if( TArry[lineEnd].line != TArry[lineEnd-1].line )
					{
						--lineEnd;
						break;
					}
				}
				if( lineEnd == TArry.GetSize() )
				{
					--lineEnd;
				}
                ix = FormALine(lineCount,ix,lineEnd,TArry,tempLine,srcCode);
				break;

			case CN_LFLOWER:
				TArry[ix].srcWord = "{";
			case CN_ELSE:
			case CN_DO:
				//自成一行
				ix = FormALine(lineCount,ix,ix,TArry,tempLine,srcCode);
 				break;

			case CN_RFLOWER:
				//如果是"};",则"};"自成一行
				TArry[ix].srcWord = "}";
				if( ix+1 < TArry.GetSize() &&
					TArry[ix+1].key == CN_LINE)
				{
					ix = FormALine(lineCount,ix,ix+1,TArry,tempLine,srcCode);
				}
				//否则}自成一行
				else
				{
					ix = FormALine(lineCount,ix,ix,TArry,tempLine,srcCode);
				}			
				break;

//			case CN_ELSE:
/*				if( TArry[ix+1].key != CN_IF)
				{
					ix = FormALine( lineCount, ix, ix, TArry, tempLine ,srcCode );					
				}
				else //if( TArry[ix+1].key != CN_IF)
				{
					ix++;
					if(FindMatchedCircles( ix, lineEnd, TArry ))
					{
						ix = FormALine( lineCount, lineBeg, lineEnd, TArry, tempLine ,srcCode );
					}
				}
				break;*/
				
			case CN_FOR:
			case CN_WHILE:
			case CN_IF:
//			case CN_SWITCH:
				if(FindMatchedCircles( ix, lineEnd, TArry ))
				{
					ix = FormALine( lineCount, lineBeg, lineEnd, TArry, tempLine ,srcCode );
				}
				else
				{
					//语法错误，()不配对
				}
				//寻找配对的()或();
				break;
			case CN_SWITCH:
			//	ix = DeleteSwitch( ix, TArry );//wq-20081226-delete

				/******wq-20081226-add*************************************************/
				if(FindMatchedCircles( ix, lineEnd, TArry ))
				{
					ix = FormALine( lineCount, lineBeg, lineEnd, TArry, tempLine ,srcCode );
				}
				else
				{
					//语法错误，()不配对
				}
				ix = FormALine( ++lineCount, ix+1, ix+1, TArry, tempLine ,srcCode ) -1 ;//记录switch()控制体的起始"{";
				ix = DeleteSwitch(ix, TArry);//跳过switch()控制体内的语句
				ix = FormALine( ++lineCount, ix, ix, TArry, tempLine ,srcCode );//记录找到switch()控制体的终止"}";
				/*********************************************************************/
				break;
			case CN_CASE:
			case CN_DEFAULT:
				//寻找':'				
				if( FindColon(ix, lineEnd, TArry) )
				{
					ix = FormALine( lineCount, lineBeg, lineEnd,TArry, tempLine ,srcCode);
				}
				else 
				{
					//语法错误，没有':'
				}
				break;

			case CN_STRUCT://???定义时初始化(结构体变量、结构体数组)怎么办???
			case CN_UNION:
			case CN_ENUM:
				//找到{				
				if( FindLFlower( ix, lineEnd, TArry) )//|| //结构体定义头部，{之前的token串成为一行
				//	FindLine( ix, lineEnd, TArry) ) //结构体定义中有结构体变量
				{ 
					
					ix = FormALine( lineCount, lineBeg, lineEnd,TArry, tempLine ,srcCode);
				}
				else if( FindLine( ix, lineEnd, TArry) ) //结构体定义中有结构体变量
				{
					ix = FormALine( lineCount, lineBeg, lineEnd,TArry, tempLine ,srcCode);
				}
				else
				{
					//语法错误，没有{
				}			
				break;
			default:
				//寻找表示结束的;(包括除上述情况之外所有以;结束的statement
				//包括数组初始化[]={};
				//!!!特殊情况：函数定义，找到函数名后的配对()即可
				if( FindLine( ix, lineEnd, TArry ) )
				{
					ix = FormALine( lineCount, lineBeg, lineEnd, TArry, tempLine ,srcCode );
				}
				else
				{
					//语法错误，没有;
				}
				break;
		}
		lineCount++;
	}
	TArry.Copy(tempLine);
//	OutputSrcToFile(TArry);
}
/* 将TArry从lineBeg到lineEnd范围内元素存入tempLine中，*
 * 并将tempLine中所有元素的行数line项更新为lineCout       */
int FormatTokenLines::FormALine(int lineCount,int lineBeg,int lineEnd,
								CArray<TNODE,TNODE> &TArry,
								CArray<TNODE,TNODE>       &tempLine,
								CArray<SRCLINE,SRCLINE> &srcCode)
{
//	tempLine.RemoveAll();
	TNODE TN;
	for(int i = lineBeg; i <= lineEnd; i++)
	{	
/*		if( TArry[i].line > 0 
			&& TArry[i].line-1 < srcCode.GetSize()
			&& srcCode[TArry[i].line-1].codeline == 0 )
		{
			srcCode[TArry[i].line-1].codeline = lineCount;
		}*/
//		TN = TArry[i];
		TN.addr = TArry[i].addr;
		TN.deref = TArry[i].deref;
		TN.key = TArry[i].key;
		TN.line = lineCount;
		TN.srcLine = TArry[i].line;
		TN.name = TArry[i].name;
		TN.paddr = TArry[i].paddr;
		TN.value = TArry[i].value;
		TN.srcWord = TArry[i].srcWord;//wq20080825
		TN.cntxtLine = TArry[i].cntxtLine;
		TN.dowhileLine = TArry[i].dowhileLine;
		TN.funcBegLine = TArry[i].funcBegLine;
		TN.funcBgnRealLine = TArry[i].funcBgnRealLine;
		TN.funcEndLine = TArry[i].funcEndLine;
		TN.funcEndRealLine = TArry[i].funcEndRealLine;
		
		
		tempLine.Add(TN);
		
	}
	return lineEnd;
}


BOOL FormatTokenLines::FindMatchedCircles(int ix,int &lineEnd,CArray<TNODE,TNODE> &TArry)
{
	int lcircles = 0,
		rcircles = 0;
	lineEnd = 0;
	int i;
	for( i = ix; i < TArry.GetSize(); i++ )
	{
		int key = TArry[i].key;
		if( key == CN_LCIRCLE )
		{
			lcircles++;
		}
		if( key == CN_RCIRCLE )
		{
			rcircles++;
		}
		if( lcircles == rcircles && lcircles != 0 )//()已经配对
		{
			lineEnd = i;//记录最外层)在TArry中的位置
			break;
		}
	}
	if( lineEnd > 0 )
	{
		if( lineEnd+1 < TArry.GetSize() )
		{
			int key = TArry[lineEnd+1].key;
			if(TArry[lineEnd+1].key == CN_LINE)//以");"结尾的情况
			{
				lineEnd++;
			}
			else if( lineEnd+2 < TArry.GetSize() )
			{//wq-20090419-将词法分析加入{}时的if{ do{}while() };更正为--->if{ do{}while; }
				if( TArry[lineEnd+1].key == CN_RFLOWER && TArry[lineEnd+2].key == CN_LINE )
				{
//					TArry[lineEnd+1].srcWord = "}";
					TArry[lineEnd+2].line = TArry[lineEnd].line;
					TArry[lineEnd+2].srcLine = TArry[lineEnd].srcLine;
					TArry.InsertAt(lineEnd+1,TArry[lineEnd+2]);
					TArry.RemoveAt(lineEnd+3);
					CString w1,w2,w3,w4;
					w1 = TArry[lineEnd].srcWord;
					w2 = TArry[lineEnd+1].srcWord;
					w3 = TArry[lineEnd+2].srcWord;
					lineEnd++;
				}
			}
		}
		return true;
	}
	return false;
}

BOOL FormatTokenLines::FindColon(int ix, int &lineEnd ,CArray<TNODE,TNODE> &TArry)
{
	int i;
	for( i = ix; i < TArry.GetSize(); i++ )
	{
		int key = TArry[i].key;
		if( key == CN_SHOW )
		{
			lineEnd = i;
			return true;
		}
	}
	return false;
}

BOOL FormatTokenLines::FindLFlower(int ix, int &lineEnd ,CArray<TNODE,TNODE> &TArry)
{
	int i;
	for( i = ix; i < TArry.GetSize(); i++ )
	{
		int key = TArry[i].key;
		if( key == CN_LINE )//定义结构体变量—— struct 结构体类姓名 变量名;
		{
			return false;
		}
		else if( key == CN_RREC && 
			     TArry[i+1].key == CN_EQUAL )//结构体数组赋值—— []={}; 
		{
			return false;
		}
		else if( key == CN_LFLOWER )
		{
			lineEnd = i - 1;
			return true;
		}
	}
	return false;
}

BOOL FormatTokenLines::FindLine(int ix, int &lineEnd ,CArray<TNODE,TNODE> &TArry)
{
	int i;
//	int func_flag = 0;
	for( i = ix; i < TArry.GetSize(); i++ )
	{
		int key = TArry[i].key;
/*		if( key == CN_DFUNCTION )
		{//遇到函数定义的函数头部，
			找到配对的(),返回
			if( FindMatchedCircles( i, lineEnd, TArry) )
			{
				return true;
			}

			//找到最近的{,返回
			func_flag = 1;
		}*/
		if(  key == CN_LFLOWER
			 && i>0 
			 && TArry[i-1].key != CN_EQUAL)
		{
			lineEnd = i-1;
			return true;
		}
		if( key == CN_LINE )
		{
			lineEnd = i;
			return true;
		}
	}
	return false;
}

void FormatTokenLines::OutputSrcToFile(CArray<TNODE,TNODE>& TArry)//(CArray<SRCLINE,SRCLINE> &srcCode)
{
	CStdioFile outsrcf;
	CString str_srcCode  = _T(""),
		    str_codeline = _T(""),
			str_srcline  = _T("");
/*	for( int i = 0; i < srcCode.GetSize(); i++ )
	{
		str_codeline.Format( _T("%d"), srcCode[i].codeline );
		str_srcline. Format( _T("%d"), srcCode[i].srcline+1  );
		str_srcCode += "##" + str_codeline +
			           "##" + str_srcline  +
					   "##" + srcCode[i].str_srcLine + "\n";
					  
	}*/
	int i;
	for( i = 0; i < TArry.GetSize(); i++ )
	{
		if( i == 0 || i > 0 && TArry[i].line != TArry[i-1].line )
		{
			str_srcCode += "\r\n";
			str_codeline.Format( "%d", TArry[i].line );
			str_srcline.Format( "%d", TArry[i].srcLine );
			str_srcCode += "##" + str_codeline + "##" + str_srcline + "##   ";
		}
		str_srcCode += TArry[i].srcWord + " ";

	}
	if( outsrcf.Open( "D:\\SouceFile.txt", CFile::modeCreate | CFile::modeWrite ) )
	{
		outsrcf.WriteString( str_srcCode );
		outsrcf.Close();
	}
}

int FormatTokenLines::DeleteSwitch( int ix, CArray<TNODE,TNODE> &TArry)
{
	int i;
	int brace = 0,
		flag  = 0;
	for( i=ix+1; i<TArry.GetSize(); i++ )
	{
		int key = TArry[i].key;
		if( key == CN_LFLOWER )
		{
			flag = 1;
			brace++;
		}
		else if( key == CN_RFLOWER )
		{
			brace--;
		}
		if( brace == 0 && flag ==1 )
		{
			break;
		}
	}
	return i;

}