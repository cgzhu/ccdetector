// SelectionUnit.h: interface for the CSelectionUnit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SELECTIONUNIT_H__0F91D6A3_FBF9_4268_80F0_499F74A67776__INCLUDED_)
#define AFX_SELECTIONUNIT_H__0F91D6A3_FBF9_4268_80F0_499F74A67776__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSelectionUnit : public CSDGBase  
{
public:
	CSelectionUnit();
	virtual ~CSelectionUnit();
public:
	CString m_sAsi;
};

#endif // !defined(AFX_SELECTIONUNIT_H__0F91D6A3_FBF9_4268_80F0_499F74A67776__INCLUDED_)
