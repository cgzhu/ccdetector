// RCFilesColumnDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "RCFilesColumnDlg.h"
#include "afxdialogex.h"
#include "CSeries.h"

// CRCFilesColumnDlg 对话框

IMPLEMENT_DYNAMIC(CRCFilesColumnDlg, CDialogEx)

CRCFilesColumnDlg::CRCFilesColumnDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRCFilesColumnDlg::IDD, pParent)
	, m_rc_files_col_chart()
{

}

CRCFilesColumnDlg::~CRCFilesColumnDlg()
{
}

void CRCFilesColumnDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TCHART1, m_rc_files_col_chart);
}

//清除所有图线
void CRCFilesColumnDlg::ClearAllSeries()
 {
     for(long i = 0;i<m_rc_files_col_chart.get_SeriesCount();i++)
     {
         ((CSeries)m_rc_files_col_chart.Series(i)).Clear();
     }
 }


BEGIN_MESSAGE_MAP(CRCFilesColumnDlg, CDialogEx)
END_MESSAGE_MAP()


// CRCFilesColumnDlg 消息处理程序
