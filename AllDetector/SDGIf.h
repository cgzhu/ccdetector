// SDGIf.h: interface for the CSDGIf class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGIF_H__91920CE5_FEA8_44C6_8A9C_BCFB3A3BF327__INCLUDED_)
#define AFX_SDGIF_H__91920CE5_FEA8_44C6_8A9C_BCFB3A3BF327__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGIf : public CSDGBase  
{
public:
	CSDGIf();
	virtual ~CSDGIf();
public:
	int IFPOS;
	int ELSEPOS;
	int expbeg;
	int expend;
	int tbeg;
	int tend;
	int fbeg;
	int fend;
};
#endif // !defined(AFX_SDGIF_H__91920CE5_FEA8_44C6_8A9C_BCFB3A3BF327__INCLUDED_)
