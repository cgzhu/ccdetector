#pragma once
#include "SciEditView.h"
#include "ansi.h"

// CMySciEditView 视图

class CMySciEditView : public CSciEditView
{
	DECLARE_DYNCREATE(CMySciEditView)
public:
	vector<treeNode> m_nodes;
protected:
	CMySciEditView();           // 动态创建所使用的受保护的构造函数
	virtual ~CMySciEditView();
	CString GetFileName();

public:	
	void GotoLine(int line);
	BOOL Save(LPCTSTR lpszPathName);//保存文件
	void RefreshAnnotations();

public:	
	virtual void OnDraw(CDC* pDC);      // 重写以绘制该视图
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnUpdateNeedSel(CCmdUI *pCmdUI);
	afx_msg void OnUpdateNeedPaste(CCmdUI *pCmdUI);
//	afx_msg void OnUpdateEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnEditClear();
	afx_msg void OnEditUndo();
	afx_msg void OnEditRedo();
	afx_msg void OnEditSelectAll();
	afx_msg void OnUpdateNeedTextAndFollowingText(CCmdUI *pCmdUI);
	afx_msg void OnUpdateNeedText(CCmdUI *pCmdUI);
	afx_msg void OnUpdateEditCopy(CCmdUI *pCmdUI);
	afx_msg void OnParseX();
	afx_msg void OnFindbugX();
	afx_msg void OnUpdateFindbugX(CCmdUI *pCmdUI);
	virtual void OnInitialUpdate();
	afx_msg void OnUpdateParseX(CCmdUI *pCmdUI);
};


