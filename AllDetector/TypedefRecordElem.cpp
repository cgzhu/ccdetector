// TypedefRecordElem.cpp: implementation of the TypedefRecordElem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllDetector.h"
#include "TypedefRecordElem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TypedefRecordElem::TypedefRecordElem()
{
	typeRename = "";
	typeTArry.RemoveAll();

}

TypedefRecordElem::~TypedefRecordElem()
{
	typeRename = "";
	typeTArry.RemoveAll();

}

TypedefRecordElem::TypedefRecordElem(const TypedefRecordElem &typdfRcdElm)
{
	typeRename = typdfRcdElm.typeRename;
	typeTArry.RemoveAll();
	typeTArry.Copy(typdfRcdElm.typeTArry);

}

TypedefRecordElem TypedefRecordElem::operator=(const TypedefRecordElem &typdfRcdElm)
{
	typeRename = typdfRcdElm.typeRename;
	typeTArry.RemoveAll();
	typeTArry.Copy(typdfRcdElm.typeTArry);
	return *this;

}

void TypedefRecordElem::Clean()
{
	typeRename = "";
	typeTArry.RemoveAll();

}
