/********************************************************************
	created:	2011/02/05
	created:	5:2:2011   16:25
	filename: 	C:\Users\T60\documents\visual studio 2010\Projects\BugFinder\BugFinder\conver_bug_file.cpp
	file path:	C:\Users\T60\documents\visual studio 2010\Projects\BugFinder\BugFinder
	file base:	conver_bug_file
	file ext:	cpp
	author:		topmint@hit.edu.cn
	
	purpose:	转换行号
*********************************************************************/
#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#ifndef _FILE_EXTERN
#define _FILE_EXTERN
#endif
#include "filename.h"

using namespace std;

int get_length( ifstream &infile2 ) 
{
	infile2.seekg (0, ios::end);
	int length = infile2.tellg();
	infile2.seekg (0, ios::beg);

	return length;
}

bool process_bug_file(char *s)
{
	//将缺陷文件的内容按行存入vector<string> str_bug_file;
	ifstream infile1(s);
	if(!infile1)
	{
		cout << "open " << s << " error" << endl;
		return false;
	}
	
	if (get_length(infile1)==0)	return false;

	vector<string> str_bug_file;

	for(string tr; getline(infile1, tr);)
	{
		str_bug_file.push_back(tr);
	}
	infile1.close();

	//将d:\imidiate_file.i内容按行存入vector<string> str_imidiate_file;
	ifstream infile2(PREPROCESSED_FILE);
	if(!infile2)
	{
		return false;
	}

	if (get_length(infile2)==0)	return false;

	vector<string> str_imidiate_file;

	for(string temp; getline(infile2, temp);)	//0513  ###########
	{
		str_imidiate_file.push_back(temp);
	}
	infile2.close();

	//转换缺陷文件中的行号
	int index;
	string current_file_path = "";

	index = str_bug_file.size()-1;	
	
	//从底向上搜索文件名
	while(index > 0 && str_bug_file[index][0] != '*')	//0510
	{
		index--;
	}

	for(; index<str_bug_file.size(); index++) //care
	{
		if(str_bug_file[index][0] == '*') //文件名?
		{
			current_file_path = str_bug_file[index].substr(1, str_bug_file[index].size()-1);
		}
		else
		{
			string temp_line_in_bug_file = str_bug_file[index].substr(0, str_bug_file[index].find(' '));
			
			int line_in_bug_file = atoi(temp_line_in_bug_file.c_str());
			
			if(line_in_bug_file < 0)		//0513
				continue;
			
			for(int i=line_in_bug_file; i>0; i--)
			{
				if(i<str_imidiate_file.size() && str_imidiate_file[i].length()>0 && 
					str_imidiate_file[i][0] == '#')
				{
					string temp = str_imidiate_file[i];
					
					string source_file_path = temp.substr(temp.find('"')+1, temp.find('"', temp.find('"')+1)-temp.find('"')-1);
					//cout << "^^^" << source_file_path << "^^^" << endl;
					
					string str = temp.substr(temp.find(' ')+1, temp.find(' ', temp.find(' ')+1)-temp.find(' ')-1);
					//cout << "^^^" << str << "^^^" << endl;
					int start_in_source_file = atoi(str.c_str());
				
					int line_bug_in_source_file = line_in_bug_file - i + start_in_source_file -2;

					char buf[30];
					str = itoa(line_bug_in_source_file, buf, 10);
					
					cout << "		" << str << endl;//@@@@@@@@@@@@@@@@@@@
					//替换缺陷文件中该缺陷行对应的行号
					str_bug_file[index].erase(0, temp_line_in_bug_file.size());
					str_bug_file[index].insert(0, str);

					//cout << str_bug_file[index] << endl;
					
					break;
				}
			}
		}
	}

	
	//将转换后的存入缺陷文件
	ofstream outfile(s, ios::trunc);
	if(!outfile)
	{
		return false;
	}
	for(index=0; index<str_bug_file.size(); index++)
	{
		outfile << str_bug_file[index] << endl;
	}
	outfile.close();
	str_bug_file.clear();
	str_imidiate_file.clear();
	return true;
}