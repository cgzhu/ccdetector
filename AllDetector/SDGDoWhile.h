// SDGDoWhile.h: interface for the CSDGDoWhile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGDOWHILE_H__51169F2F_D050_4007_8BD1_B652D2BAD84E__INCLUDED_)
#define AFX_SDGDOWHILE_H__51169F2F_D050_4007_8BD1_B652D2BAD84E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGDoWhile : public CSDGBase  
{
public:
	CSDGDoWhile();
	virtual ~CSDGDoWhile();
public:
	int DPOS;
	int WPOS;
	int expbeg;
	int expend;

};

#endif // !defined(AFX_SDGDOWHILE_H__51169F2F_D050_4007_8BD1_B652D2BAD84E__INCLUDED_)
