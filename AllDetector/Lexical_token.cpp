#include "stdafx.h"
#include "Lexical.h"
#include <time.h>
#include <iostream>
using namespace std;
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
void CLexical::INIT_TNODE(TNODE &TD,int key,int addr,int line,double value,CString srcWord)
{
	TD.key=key;
	TD.addr=addr;
	TD.line=line;
	TD.value=value;
	TD.name=_T("");;
	TD.deref=-1;
	TD.paddr=-1;
	TD.srcWord = srcWord;
}
void CLexical::Pro_to_Token(CString &program,CArray<TNODE,TNODE> &TArry,CArray<VIPCS,VIPCS> &VArry,bool HC_flag)
{
	CList<CString,CString>  m_sList;
	if(!TranToLine(program,m_sList))
	{
		program.Empty();
		m_sList.RemoveAll();
	//	AfxMessageBox("程序未能转换成按行存储,词法分析失败！");//wtt
		return;//wtt
	}	

/*
	POSITION pos = m_sList.GetHeadPosition();
	for(int i=0; i<m_sList.GetCount(); ++i)
	{
		AfxMessageBox(m_sList.GetNext(pos));
	}
*/

/////////////此段程序为提高执行效率将头文件的m_sList分段(1000l/p)执行/////////////////
/////////////分段的是否会影响最终结果的正确性，尚未仔细考虑！/////////////////////
	//cout<<m_sList.GetCount()<<endl;
	//long beginTime=clock();
	if(HC_flag==1)
	{
		int count=m_sList.GetCount();
		count=count-1000;
		while(count>0)
		{
			CList<CString,CString>  temp_sList;
			for(int i=0; i<1000; i++)
			{
				temp_sList.AddHead(m_sList.RemoveHead());
			}
			CArray<TNODE,TNODE> Temp_TArry;
			ConstructToken(temp_sList,Temp_TArry,VArry);
			TArry.Append(Temp_TArry);
			count=count-1000;
		}
		if(TArry.GetSize()!=0)
		{
			CArray<TNODE,TNODE> Temp_TArry;
			ConstructToken(m_sList,Temp_TArry,VArry);
			TArry.Append(Temp_TArry);
		}
		else
		{
			ConstructToken(m_sList,TArry,VArry);
		}	
	}
	else
	{
		ConstructToken(m_sList,TArry,VArry);
	}	
//////////////////////////////////////////////////////////////////////////////////////	
	//long endTime=clock();
	//cout<<TArry.GetSize()<<endl;       
    //cout<<endTime-beginTime<<endl;
}

void CLexical::ConstructToken(const CList<CString,CString> &CT_List,CArray<TNODE,TNODE> &TArry,CArray<VIPCS,VIPCS> &VArry)
{
	int linenum=0;
	int nline=0;               //当前行数

	linenum=CT_List.GetCount();//总的行数
	
	while(nline<linenum)
	{ 
		//  scan every line of the input program
		int ix=0;
		int linelen;
		char letter;
		char c,ctmp;
		bool isnum=false;
		CString word("");
		CString tempword("");
		CString sline;
		POSITION pos;
		TNODE TD;
		
		pos=CT_List.FindIndex(nline); 
		sline=CT_List.GetAt(pos); //当前行字符串
		linelen=sline.GetLength();//当前行字符长度
		
		if(!sline.IsEmpty())
		{
			/*王倩添加-20080730-记录每行源代码于srcCode
			SRCLINE srcl;
			srcl.str_srcLine.Format( sline );
			srcl.srcline = nline;
			srcl.codeline = 0;
			srcCode.Add(srcl);
			/*end*/

			//  add some ' ' to avoid system error;
			//  especially when get a char from a string but its index beyond the bound; 
			sline+="     ";
			linelen+=5;
			//------------end of this part------------// 

			letter=sline.GetAt(0);
			ix++; 
		}
		else
		{
			nline++;
			continue;
		}
        		
		while(ix<linelen || linelen==1)
		{

			//  scan every letter

			TD.addr=-1;
			TD.paddr=-1;
			TD.value=0;
			TD.name=_T(""); 
			TD.line=nline;
			int &testline=TD.line;
			word=_T("");
			
			if(( isalpha(letter) ||	(letter=='_'))&&(letter>0))//标识符或关键字
			{
				//  found the string of viriables and keywords 

				word+=letter;								
				letter=sline.GetAt(ix);
				ix++;
				
				
				while( (isalpha(letter) || isdigit(letter) || ( letter=='_')) && (letter>0) && (ix<linelen) ) //LETTER,DIGITAL,AND '_'
				{ 
					word+=letter;
					letter=sline.GetAt(ix);
					ix++;
				}
			   
		        int key=WhichKeyWord(word);                      
				if(key<=0) //  found variable or c base function
				{	
					int fid=WhichBFunction(word);			
					if(fid>=0 // found base function	
//					   && sline.Find("(",ix) != -1 )	 	
					   && letter == '(' )//王倩添加-20080827-防止当变量名与库函数名相同时（如time）被识别为函数名
					{								
						TD.key=CN_BFUNCTION;
						TD.name=word;
						TD.addr=fid; 
						TD.srcWord = word;//wq
					}
					else			//  found variable
					{			
						TD.key=CN_VARIABLE;
						TD.name=word; 	
						TD.srcWord = word;//wq
					}
				} 
				else				//  found keyword 
				{  					
					TD.key=key;				
					if(key==CN_MAIN)
					{ 
						TD.name=_T("main"); 
						TD.srcWord = TD.name;//wq
					}
				}			
				ix--; 
				TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
			    TArry.Add(TD); 
			}
	    	else// 	与if(( isalpha(letter) || (letter=='_')) && (letter>0))配对
			{//if(isdigit(letter)&&(letter>0))//wtt	
			 //found digitals//可识别.98的形式和指数形式//wtt
		     	int Exp_flag=1;
				int Lint_flag=1;
				
				if((isdigit(letter)||letter=='.') && (letter>0))//wtt数值
				{
					if(isdigit(letter))
					{
						word+=letter;
						letter=sline.GetAt(ix);
						ix++;

						/***wq-20081202-识别0x78***/
						if( letter == 'x' && ix+1 < sline.GetLength() && isdigit(sline.GetAt(ix)) )
						{
							word += letter;
							letter=sline.GetAt(ix);
							ix++;
						}
						/******************************/

						while((letter>=48)&&(letter<=57)&&(ix<linelen))
						{
							word+=letter;
							letter=sline.GetAt(ix); 
							ix++;
						} 
	
						if(letter==46)//0.98
						{
							Lint_flag=0;//lx
							word+=letter;
							letter=sline.GetAt(ix);
							ix++;
							while((letter>=48)&&(letter<=57)&&(ix<linelen))
							{
								word+=letter;
								letter=sline.GetAt(ix); 
								ix++;
							}	
												
						}
					}

					else/////letter==.98          ///////////////////////
					{
						Lint_flag=0;//lx
						word+=letter;                            ///////////////////////
						letter=sline.GetAt(ix);
						ix++;
						if(!isdigit(letter))//不是实数          ///////////////////////
						{
							TD.key=CN_DOT;                       ///////////////////////
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词于token节点中
							TArry.Add(TD);
							Exp_flag=0;
						}
						else//是实数                                ///////////////////////
						{
							word+=letter;
							letter=sline.GetAt(ix);	
							ix++;
							while(letter>=48 && letter<=57 && ix<linelen)///////////////////////
							{
								word+=letter;
								letter=sline.GetAt(ix);	                ///////////////////////
								ix++;
							}
							word='0'+word;
						}
					}

					if((letter=='e'||letter=='E') && ix<linelen && Exp_flag!=0)//lx
					{
							word+=letter;
							letter=sline.GetAt(ix);	
							ix++;
							if((letter=='-'||letter=='+') && ix<linelen)///////////////////////
							{
								word+=letter;
								letter=sline.GetAt(ix);	
								ix++;
							}
							while(letter>=48 && letter<=57 && ix<linelen)///////////////////////
							{
								word+=letter;
								letter=sline.GetAt(ix);	           ///////////////////////
								ix++;
							}
								
							if(ix-2>=0 && ix-2<linelen && isdigit(sline.GetAt(ix-2)))        ///////////////////////
							{
								double dValue=atof(word);
						  		TD.value=dValue; 
								TD.key=CN_CFLOAT;
							    TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
								TArry.Add(TD);
							
							}
							else                                   ///////////////////////
							{
								//error错误的指数表示形式
							}
					}
					else
					{   
						/////////////lx
						if((letter=='l'||letter=='L') && ix<linelen && Lint_flag==1)
						{
							ix++; //为123L格式
						}
						/////////////lx
						
						if(word.Find('.')<0)
						{
							long lvalue=atol(word);
							TD.addr=lvalue;
							TD.key=CN_CINT;	
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 
						}
						else if(word.Find('.')>0)
						///wn
						{
							double dValue=atof(word);            ///////////////////////
							TD.value=dValue; 
							TD.key=CN_CFLOAT;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);
						} 
						else
						{
						     /////////////////////////////
							 //wn//考虑结构变量的成员运算符
						    //////////////////////////////
						}
					}
					//////////////////wtt负常数//////1.10/////
					if(Exp_flag!=0)  //lx
					{
						int k=TArry.GetSize()-1;
//						if(k-1>=0&&TArry[k-1].key==CN_ADD||TArry[k-1].key==CN_SUB)//wq-20081223-delete
						if(k-1>=0&& ( TArry[k-1].key==CN_ADD||TArry[k-1].key==CN_SUB ) )//wq-20081223
						{
							if(k-2>=0&&(TArry[k-2].key!=CN_VARIABLE&&TArry[k-2].key!=CN_RCIRCLE&&TArry[k-2].key!=CN_RREC&&TArry[k-2].key!=CN_CINT&&TArry[k-2].key!=CN_CLONG&&TArry[k-2].key!=CN_CFLOAT&&TArry[k-2].key!=CN_CDOUBLE))
							{
								if(TArry[k-1].key==CN_SUB)
								{
									if(TArry[k].key==CN_CINT||TArry[k].key==CN_CLONG)
									TArry[k].addr=0-TArry[k].addr;
									else if(TArry[k].key==CN_CFLOAT||TArry[k].key==CN_CDOUBLE)
										TArry[k].value=0-TArry[k].value; 

								}
								TArry.RemoveAt(k-1);
							}
						}
					}
					////////////////////////////////////////////////
						ix--;										
				}
				////////////////////////wtt///////////////////////////////
				
				else//与if((isdigit(letter)||letter=='.')&&(letter>0))配对
				{
					word+=letter;
					switch(letter)
					{
						case '\n':       // \n
							break;
                        case ' ':
							break;
						case '\t':
							break;
						case '\r':
							break;
						
						case 33:       // !
							letter=sline.GetAt(ix);
							ix++;

							if(letter==61)
							{
								//  found "!="
								word+=letter;
								TD.key=CN_NOTEQUAL;
								TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
								TArry.Add(TD); 
							} 
							else
							{
								//  found "!"
								TD.key=CN_NOT;
								TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
								TArry.Add(TD);

								//  recede a letter  //
								ix--;
								//------------------//
							} 							
							break;
						
						case 34:       // "							
							//  found a string  "..."			
							letter=sline.GetAt(ix); ix++;					
							tempword = word;
							word=_T("");
							
							while((letter != 34) && (ix<linelen))
							{
								word+=letter;			
								letter=sline.GetAt(ix);		//AfxMessageBox(sline);
								ix++;
							}
							tempword += word;	
							tempword += "\"";	
				//			if(ix>=linelen && letter!=34)
							{ 
								// error: a string without the right '\"' or 
								// is so long that one line can't contain it. 

								//error
							}
							
							TD.key=CN_CSTRING;
							TD.name=word;
							TD.srcWord = tempword;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);
						
							break;

						case 35:       // #						
							TD.key=CN_WELL;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 	
							
							break;

						case  37:      // %
							letter=sline.GetAt(ix); 
							ix++;

							if(letter=='=')
							{
								//  found a "%="
								
								TD.key=CN_YUEQUAL; 
								word += "=";	//wq							
							}
							else
							{
								//  found a '%'
								
								TD.key=CN_FORMAT;								
								ix--;
							}
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 	
							
							break;

						case 38:       // &
							
							letter=sline.GetAt(ix); 
							ix++;
							
							if(letter=='&')
							{
								//  found a "&&"

								TD.key=CN_DAND;
								word+="&";//wq
							}
							else
							{
								if(letter=='=')
								{
									//  found a "&="
									
									TD.key=CN_ANDEQUAL; 
									word += "=";//wq
								}
								else
								{
									//  found a '&'
									
									TD.key=CN_AND;
									ix--;
								}
							}
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 
					
							break;
						case 39:       // '
							
							word=_T("");
							
							c='\0';
							c=sline.GetAt(ix);
							ix++;
							if(ix<linelen)
							{
							letter=sline.GetAt(ix); 
							ix++;
							}
							if(letter==39)
							{
								//  found a const char  ' '

								word+=c;
                                
								ctmp='\0';
								ctmp=sline.GetAt(ix);
								ix++;
                           		
								
								if(ctmp==39)
								{
									//TD.addr=39;
									////////wtt////////////////
									if(c=='\\')//  it is '\'';
									{
										TD.addr=39;
										TD.key=CN_CCHAR;
										TD.deref=0;
										TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
										TArry.Add(TD);
									}
									else
									{
										////////////error:
										
										if('a'<=c&&c<='z'||'A'<=c&&c<='Z'||'0'<=c&&c<='9')
										{
											TD.key=CN_CCHAR;
											TD.addr=c;
											TD.deref=0;
											TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
											TArry.Add(TD);
										}
										
									}
									////////////////wtt//////////////



								}
								else
								{
									//  common  const char
								    TD.addr=c; 
								/*//////////////////////////////////
									if(c==' ')
									{ 
										CString str;
										str.Format("%d",TD.addr) ;
										AfxMessageBox("c= "+str);///
									}
									////////////////////////////////////
									*/
									TD.key=CN_CCHAR;
									TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
									TArry.Add(TD);
									//  recede a letter  //
									ix--;
									//-------------------//
								}								
								
							}
							else//	if(letter==39)
							{
								//  '\0' , '\n' , '\t' , '\v' , '\b' , '\r' , '\f' , '\\' , '\''  
								
								if((c==92)&&(letter=='0'||letter=='n'||letter=='t'||letter=='v'||letter=='b'||letter=='r'||letter=='f'||letter=='\\'||letter=='a'||letter=='\"'||letter=='\?'))
								{
									TD.key=CN_CCHAR;
									switch(letter)
									{
									case '0':
										
										TD.addr='\0';//wtt///5.16//

										break;
									case 'n':
										TD.addr='\n';
										break;
									case 't':
										TD.addr='\t';
										break;										
									case 'v':
										TD.addr='\v';
										break;
									case 'b':
										TD.addr='\b';
										break;
									case 'r':
										TD.addr='\r';
										break;
									case 'f':
										TD.addr='\f';
										break;											
									case '\\':
										TD.addr='\\';
										break;
								/////////////////wtt////////////////////
									case 'a':
										TD.addr='\a';
										break;
									case '\"':
										TD.addr='\"';
										break;
									case '\?':
										TD.addr='\?';
										break;
								/////////////////////wtt///////////////////////
									}
									TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
									TArry.Add(TD);
									letter=sline.GetAt(ix);
									ix++;

									if(letter!=39)
									{			
										// error: the format of the const char is wrong
										
										ix--;
									}
								}
								else
								{
									///////////////////wtt////////'\ddd'与'\xhh'形式转化成相应的字符存储///////////////////////////
									if(c=='\\'&&('0'<=letter&&letter<='7'))
									{
										
										int tag=1,ASCnum=0;
										char ch;
										char chs[3];
										
										
										while(tag>0&&tag<4)
										{
											ch=sline.GetAt(ix+tag-2);
											if('0'<=ch&&ch<='7')
											{
											
												chs[tag-1]=ch;

												tag++;
											}
											else 
												tag=-1;
										}
										if(tag>0)
										{
											
											ASCnum=(chs[0]-'0')*4+(chs[1]-'0')*2+(chs[1]-'0');
											TD.key=CN_CCHAR;
											TD.addr=(char)ASCnum;
											TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
											TArry.Add(TD);
										    ix=ix+3;																			
											if(ix-1<linelen&&sline.GetAt(ix-1)!='\'')
											{
												///error://///////////	
											
												ix--;
											
											
											}
																							
										}
										else 
										{ 
											//error://////////////
											if(ix+2<linelen&&sline.GetAt(ix+2)=='\'')
											{
												ix=ix+3;
											}
											else 
											ix--;										

										}
									}
									else if((c==92)&&(letter=='x'))
									{
									
										int tag=1,ASCnum=0;
										char ch;
										char chs[2];
										int n[2];
									
										
										while(tag>0&&tag<3&&ix+tag-1<linelen)
										{
											ch=sline.GetAt(ix+tag-1);
											if('0'<=ch&&ch<='9'||'A'<=ch&&ch<='F')
											{
												
												chs[tag-1]=ch;

												tag++;
											}
											else 
												tag=-1;
										}
										if(tag>0)
										{
											for(int i=0;i<=1;i++)
											{
												if('0'<=ch&&ch<='9')
													n[i]=chs[i]-'0';
												else
													n[i]=chs[i]-'A'+10;
											
											}
											ASCnum=n[0]*16+n[1];
											TD.key=CN_CCHAR;
											TD.addr=(char)ASCnum;
											ix=ix+3;
											TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
											TArry.Add(TD);
											if(ix-1<linelen&&sline.GetAt(ix-1)!='\'')
											{
												///error://///////////	
											
											
											
											}


											
										}
										else 
										{ 
											//error://////////////
										
											if(ix+2<linelen&&sline.GetAt(ix+2)=='\'')
											{
												ix=ix+3;
											}
											else 
											ix--;											

										}
															
									}
								

									else 	
									{
										// error: the format of the const char is wrong
									
										if(ix+2<linelen&&sline.GetAt(ix+2)=='\'')
										{
											ix=ix+3;
										}
										else 
										{
											
											ix--;
											
										}
									
																					
									
									}	
					
								}
                            }
						//	//TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
						//	TArry.Add(TD);
							break;

						////////////////////////wtt/////////////////////////	


						case 40:       // (

							TD.key=CN_LCIRCLE;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 	
							
							break;

						case 41:       // ) 

							TD.key=CN_RCIRCLE;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case 42:       // *
							
							letter=sline.GetAt(ix);
							ix++;

							if(letter=='=')
							{
								// found a "*="

								TD.key=CN_MULEQUAL;
								word += "=";//wq
							}
							else
							{
								// found a '*'

								TD.key=CN_MULTI;
								ix--;
							}
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);

							break;

						case 43:       // +
							
							letter=sline.GetAt(ix);
							ix++;
							
							if(letter==43)
							{
								//  found a "++"

								TD.key=CN_DADD;
								word += "+";//wq
							}
							else 
							{
		             			if(letter=='=')
								{
									// found a "+="

									TD.key=CN_ADDEQUAL;
									word += "=";//wq
								}
								else
								{
									//  found a '+'
								
									TD.key=CN_ADD;
									ix--;
								}
							}

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 
					
							break;

						case 44:       // ,

							TD.key=CN_DOU;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case  45:      // -
						
							letter=sline.GetAt(ix);
							ix++;
							
							if(letter==45)
							{ 	
								//  found a "--"

								TD.key=CN_DSUB;
								word +="-";//wq
							} 
							else
							{
								if(letter=='=')
								{
									//  found a "-="

									TD.key=CN_SUBEQUAL;
									word += "=";//wq
								}
								else
								{
									if(letter=='>')
									{
										//  found a "->"

										TD.key=CN_STRPTR;
										word += ">";//wq
									}
									else
									{
										//  found a '-'
										
										TD.key=CN_SUB;
										ix--;
									}
								}								
							} 

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);
							break;
					//////////////////wtt/////////
						//case  46:
						//	TD.key=CN_DOT;
						//  //TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
						//	TArry.Add(TD); 
						//	break;
							/////////////////wtt/////////////
						case  47:      // / 
							letter=sline.GetAt(ix); ix++;		//sline:当前行字符串

						
							//wq20080925///去除"//"标识的注释
							if(letter == '/')
							{
								ix=linelen;
								break;
							}//wq/////////////////////
											


							if(letter=='*')
							{ 
								//  found an annotation: /*
								//	AfxMessageBox("find an annotation");

								int ipos=-1;
								int ipos1=sline.ReverseFind('/');
								int ipos2=sline.ReverseFind('*');
								if(ipos2==ipos1-1)
									ipos=ipos2;
									////////////////////////
									/*CString s;
									s.Format("ipos=%d,ipos1=%d,ipos2=%d,ix=%d",ipos,ipos1,ipos2,ix);
									AfxMessageBox(s);*/
									//////////////////////////

								// annotation of one line
								if(ipos>=0 && ix<ipos)
								{
									ix=ipos+2;
									
									// be important word
									if(m_nType>BOUND)
									{
										if(sline.Find(STRFPBEG)>=1)
										{
											VD.beg=TArry.GetSize(); 
										}

										if(sline.Find(STRFPEND)>=1)
										{
											VD.end=TArry.GetSize()-1;
											VArry.Add(VD);
										}
									}
									
								}
								// annotation of multi-line
								else
								{
								
								
									int n=nline+1;
									//int n=nline;
									CString stmp;
									/////////////////wtt//////////////
								/*	int ipos=-1;
									while(n<linenum && ipos<0)
									{*/
								//	stmp=CT_List.GetAt(CT_List.FindIndex(n));									
									 //   ipos=stmp.Find("*/");
										
									//	n++;									
									//}*/
							//	AfxMessageBox("multiple lines");////////////

       								   int nfront=1,nend=0;
									   	ipos=-1;
									while(n<linenum && nfront!=nend)
									{
										stmp=CT_List.GetAt(CT_List.FindIndex(n));
									//	AfxMessageBox(stmp);///////////
									//	if(stmp.Find("/*")>=0)
										//	nfront++;
									
											int ipos1=stmp.ReverseFind('/');
											int ipos2=stmp.ReverseFind('*');
											if(ipos2==ipos1-1)
												ipos=ipos2;
										if(ipos>=0)
											nend++;												
										n++;									
									}
									////////////////////////
								/*	CString s;
									s.Format("%d",nend);
									AfxMessageBox(s);*/
									//////////////////////////
									if(nfront!=nend)
									{
										//error comments are not matching
									
										//AfxMessageBox("error comments are not matching");///////////
										return;

									}
									//////////////wtt//////////////////////
                                   // n--;//wtt

									if(ipos>=0&&nfront==nend)
									{//	AfxMessageBox("delete multiple lines");////////////
										n--;
										nline=n;
										sline=CT_List.GetAt(CT_List.FindIndex(n));
										linelen=sline.GetLength();
										ix=ipos+2;
									}

									if(n>=linenum)
									{
										//  found an error
										//  annotation without bound
										ix=linelen;
									

									//	stemp.Format("error in line(%d):",nline); 
									//	m_pError->Add(stemp+"annotation without bound!");
									}
								}
							}
							else 
							{ 
								if(letter=='=')
								{
									//  found "/=" 
									
									TD.key=CN_DIVEQUAL;
									word += "=";//wq
								}
								else
								{
									//  found '/'

									TD.key=CN_DIV;
									ix--;
								}

								TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
								TArry.Add(TD);
							} 

							break;

						case  58:      // : 

							TD.key=CN_SHOW;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case  59:      // ;

							TD.key=CN_LINE;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case  60:      // <

							letter=sline.GetAt(ix); 
							ix++;

							if(letter=='=')
							{ 
								// found a "<="

								TD.key=CN_LANDE;
								word += "=";//wq
							} 
							else
							{ 
								if(letter==60)
								{
									word += "<";//wq
									letter=sline.GetAt(ix); 
									ix++;
									if(letter=='=')
									{
										//  found a "<<="
										TD.key=CN_LMOVEEQUAL;
										word += "=";//wq
									}
									else
									{
										//  found  "<<"
										
										TD.key=CN_LMOVE; 
										ix--;
									}
								}
								else
								{
									//  found   "<"
						
									TD.key=CN_LESS;
									ix--;
								}
							} 

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
						    TArry.Add(TD); 
							
							break;

						case  61:      // = 

							letter=sline.GetAt(ix);
							ix++;

							if(letter==61)
							{
								// found a "=="
								TD.key=CN_DEQUAL;
								word += "=";//wq
							}
							else
							{ 	
								//  found a '='
								
								TD.key=CN_EQUAL;
								ix--;
							}

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case  62:      // >

							letter=sline.GetAt(ix); 
							ix++;

							if(letter=='=' || letter=='>')
							{ 	
								if(letter=='>')
								{
									word += ">";//wq
									letter=sline.GetAt(ix); 
									ix++;

									if(letter=='=')
									{
										//  found a ">>="

										TD.key=CN_RMOVEEQUAL; 
										word += "=";//wq
									}
									else
									{
										//  found a ">>"

										TD.key=CN_RMOVE;
										ix--;
									}
								
								}
								else
								{
									if(letter=='=')
									{
										//  found a '>='
										TD.key=CN_BANDE;
										word += "=";//wq
									}
								}
								
							} 
							else
							{
								//  found a '>'

								TD.key=CN_GREATT;
								ix--;
							} 

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;
						case  63:      // ?
							TD.key=CN_HELP;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 
							break;
						case  91:      // [ 
							TD.key=CN_LREC;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 
							break;
						case  92:      // '\'

							TD.key=CN_WNTOES;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case  93:      // ]

							TD.key=CN_RREC;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

							break;

						case '^':       // ^ 
							
							letter=sline.GetAt(ix);
							ix++;
						
							if(letter=='=')
							{
								//  found a "^="

								TD.key=CN_XOREQUAL;
								word += "=";//wq
							}
							else
							{
								//  found a '^'

								TD.key=CN_XOR;
								ix--;
							}

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);	
							break;

						case 123:      // {

							TD.key=CN_LFLOWER;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD); 

					    	break;

						case 125:      // }

							TD.key=CN_RFLOWER;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);

							break;

						case 124:      // |

							letter=sline.GetAt(ix);
							ix++;

							if(letter==124)
							{
								//  found a "||"

								TD.key=CN_DOR;
								word += "|";//wq
							}
							else
							{ 
								if(letter=='=')
								{
									//  found a "|="

									TD.key=CN_OREQUAL;
									word += "=";//wq
								}
								else
								{
									//  found a '|'
									TD.key=CN_OR;
									ix--;
								}
								
								
							} 

							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);

							break; 

						case '~':    // ~
							
							TD.key=CN_BITBU;
							TD.srcWord = word;//王倩添加-20080729-记录源程序单词与token节点中
							TArry.Add(TD);

							break;
						default:

 							if((letter=='@')||(letter=='$')||(letter=='`')||(letter<0))
							{
								InvalidateLetter(TD.line);
							}
					}

				} 
			} 

			if(ix<linelen)
			{
				letter=sline.GetAt(ix); 
				ix++;
			}
		}
		if(nline<linenum)
		{
			nline++;
		}
	}

	return;  
}