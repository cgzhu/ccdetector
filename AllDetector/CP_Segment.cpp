// CP_Segment.cpp: implementation of the CP_Segment class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CP_Segment.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CP_Segment::CP_Segment()
{
	seq=_T("");
	len=0;
	sup=0;
	Is_Expanded=0;
//	expandedTimes = 0;
	CP_Array.RemoveAll();
}

CP_Segment::~CP_Segment()
{
	seq=_T("");
	len=0;
	sup=0;
	Is_Expanded=0;
//	expandedTimes = 0;
	CP_Array.RemoveAll();

}

CP_Segment::CP_Segment(class CP_Segment &R_CPS)
{
	seq=R_CPS.seq;
	len=R_CPS.len;
	sup=R_CPS.sup;
	Is_Expanded=R_CPS.Is_Expanded;
//	expandedTimes = R_CPS.expandedTimes;
	CP_Array.Copy(R_CPS.CP_Array);
}

CP_Segment& CP_Segment::operator=(CP_Segment &R_CPS)
{
	seq=R_CPS.seq;
	len=R_CPS.len;
	sup=R_CPS.sup;
	Is_Expanded=R_CPS.Is_Expanded;
//	expandedTimes = R_CPS.expandedTimes;
	CP_Array.Copy(R_CPS.CP_Array);
	return *this;
}

bool CP_Segment::Init_CPNode(class Seq_Mining &Rsm, int i, int j)
//取出类Rsm中候选集元素LS[i][j]指向的节点信息，存入类CP_Segment里
//若该节点所存序列的支持度大于等于2，则返回true，否则返回false
{
	class Latt_Node *Pt=Rsm.LS[i][j].LNode;
	seq=Rsm.LNode_To_Seq(Pt);
	Is_Expanded=0;
//	expandedTimes = 0;

	int FC=0;
/*	for(int k=0; k<seq.GetLength()/10; k++)
	{
		CString item=seq.Mid(k*10, 10);
		if(item=="      2000" || item=="      2032")
		{
			FC++;
		}
	}//统计单独一行的'{'或'}'的数目*/
	len=seq.GetLength()/10 - FC;//重置len值，去除单独一行的花括号数
	if(len == 0)//此块除'{'或'}'之外没有实质性内容
	{
		return 0;
	}
	Init_CP_Array(Rsm.Seq_Arry_G, Pt->Ds_Arry, seq);
	sup=CP_Array.GetSize();
	if(sup>=2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void CP_Segment::Init_CP_Array(CArray <Seq_NODE, Seq_NODE> &Seq_Arry_G, CArray<Ds_NODE,Ds_NODE> &Ds_Arry, CString Seq)
//遍历数组Ds_Arry，若某程序碎片的连续性满足要求，则将其信息存入类CP_Segment的CP_Array中
{
	for(int i=0; i<Ds_Arry.GetSize(); i++)
	{
		int ID=Ds_Arry[i].Seq_ID;
		CP_SEG CP_Node;
		CString DB_Seq=Seq_Arry_G[ID].Sequence;
		CString Line_Pos=Seq_Arry_G[ID].Line_Pos;
		CString relaLPOS = Seq_Arry_G[ID].relaLPOS;
//		CString str_TknIndex = Seq_Arry_G[ID].str_tokenIndex;
		if(Is_CPSeg(seq, DB_Seq, Line_Pos,relaLPOS, CP_Node))//, str_TknIndex
		{
			CP_Node.funcBegLine = Seq_Arry_G[ID].funcRang.srcBegLine;//wq-20081222-添加函数范围
			CP_Node.funcEndLine = Seq_Arry_G[ID].funcRang.srcEndLine;//wq-20081222-添加函数范围
			CP_Node.funcBgnRealLine = Seq_Arry_G[ID].funcBgnRealLine;//wq-20090202-添加函数范围(源程序绝对行数)
			CP_Node.funcEndRealLine = Seq_Arry_G[ID].funcEndRealLine;//wq-20090202-添加函数范围(源程序绝对行数)
			CP_Node.filename=Seq_Arry_G[ID].cfilepath;
			CP_Node.Beg_index=Seq_Arry_G[ID].B_index;
			CP_Node.End_index=Seq_Arry_G[ID].B_index;
			CP_Node.Is_combine=0;
			CP_Array.Add(CP_Node);
		}
	}
}

bool CP_Segment::Is_CPSeg(CString Seq, CString DB_Seq, CString Line_Pos, CString relaLPOS, CP_SEG &CN)//wq20080926///添加了参数CString strTknIndex,CString strTknIndex
//通过对比子序列Seq和原数据库序列DB_Seq，判断子序列代表的程序片段是否满足连续性的要求
//若满足，则将其信息存于CN中，并返回true
{
	int i=0, j=0, k=0;
	CArray <int, int> Line_Addr;//记录子序列的项在原数据库序列中的相对位置
	CString Item=_T("");
	for(i=0; i<Seq.GetLength()/10; i++)
	{
		Item=Seq.Mid(i*10,10);
		for(j=k; j<DB_Seq.GetLength()/10; j++)
		{
			if(Item == DB_Seq.Mid(j*10,10))
			{
				k=j+1;
				Line_Addr.Add(j);
				CN.LPOS+=Line_Pos.Mid(j*4,4);//记录子序列的项在原数据库序列对应的源程序中的绝对位置（行数）
				CN.relaLPOS += relaLPOS.Mid(j*4,4);
				break;
			}
		}//对比子序列Seq和原数据库序列DB_Seq，找出相同项对应的位置
		if(j >= DB_Seq.GetLength()/10)
		{
			AfxMessageBox("挖掘结果错误，与数据库中对应序列无包含关系！");
			return 0;
		}
	}//将子序列Seq与序列数据库DB_Seq中的原序列对比，找出对应的位置


	int Line_count= Line_Addr.GetSize();
	if(Line_count==2 && (Line_Addr[1]-Line_Addr[0])>Max_gap)
	{
		return 0;
	}//若碎片只有2个项，且间距大于Max_gap，则不加入候选集
	
	if(Line_count>=3)
	{
		float max_den=0;//记录该模块子序列的最大的密度
		if(Seq.Find(" 110483360")>=0)
		{
			int Useless_Num=0;//记录"case X:"和"break;"的行数
			int Len=Seq.GetLength()/10;
			for(i=0; i < Len; i++)
			{
				CString str=Seq.Mid(i*10,10);
				if(str==" 110483360" || str=="  42940176" || str=="  44623632")
				//" 110483360"对应"break;" //"  42940176"对应"case 'A':"//"  44623632"对应"case 1:"
				{
					Useless_Num++;
				}
			}
			float Ratio=(float)Useless_Num/(float)Len;
			if(Ratio >= Switch_threshold)
			{
				return 0;
			}
		}//去除swtich语句中的无用碎片
		
		for(i=0; i+1<Line_count; i++)
		{
			int former=Line_Addr[i];
			int latter=Line_Addr[i+1];
			CString Item1=DB_Seq.Mid(former*10,10);
			CString Item2=DB_Seq.Mid(latter*10,10);				
			if( (latter-former)==1 && Item1!="      2000" && Item1!="      2032" && 
				 Item2!="      2000" && Item2!="      2032")
			{
				max_den=1;
				break;
			}//如果发现前后两项相邻，且这两项都不是单独一行的话花括号，则将max_den置为最大值1
			
			for(j=i+2; j<Line_count; j++)
			{
				int len=Line_Addr[j]-Line_Addr[i]+1;
				int line_sum=j-i+1;
				float density=(float)line_sum/(float)len;
				if(density>max_den)
				{
					max_den=density;
				}
			}
		}
		if(max_den<Min_den)
		{
			return 0;
		}
	}//若碎片所含项数大于等于三，且没有密度大于Min_den的子序列（子序列至少含三个项），则不加入候选集
	
	CN.B_gap=Line_Addr[0];//子序列的头与原序列的头之间的距离
	CN.E_gap=DB_Seq.GetLength()/10 - (Line_Addr[Line_count-1] + 1);//子序列的尾与原序列的尾之间的距离
	return 1;
}
/*
//wq-20081122-生成代码片段的所有标识符的位置信息集合表
void CP_Segment::FormIdsPlaceSet(CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G)
{
	int m,n,i,j;
	IDENTF_LOCATION idLoc;
	for( m = 0; m < CP_Array.GetSize(); m++)
	{//对于每个重复代码片段
		for( i = 0; i < CP_Array[m].dupCodeLocArry.GetSize(); i++ )
		{//对于代码片段的每个序列
			DupCodeLocation &dupCodeLoc = CP_Array[m].dupCodeLocArry[i];
			idLoc.dupCodeLocIdx = i;//记录重复代码的第几个序列
			int seqIndex = dupCodeLoc.seqIndex;
			for( j = 0; j < dupCodeLoc.itemNumArry.GetSize(); j++ )
			{//对于每个序列的每个项
				idLoc.itmNumArryInx = j;//记录重复代码某序列的第几个项
				int itemNum = dupCodeLoc.itemNumArry[j];
				int k = 0;
				TKN_LINK_NODE *tpHead = Seq_Arry_G[seqIndex].head;
				TKN_LINK_NODE *tpTail = Seq_Arry_G[seqIndex].tail;
				while( tpHead != NULL && tpTail != NULL && tpHead != tpTail->next )
				{
					if( tpHead->m_itemNum == itemNum 
						   && tpHead->tkNode.key != CN_RFLOWER 
						   && tpHead->tkNode.key != CN_LFLOWER )
					{//找到序列数据库中与重复代码片段的序列项对应的每个token节点
						k++;//重复代码中的token序号加一（从1开始）
						if( tpHead->tkNode.key == CN_VARIABLE )
						{//如果该token代表一个标识符
							idLoc.tknLnkNodeCnt = k;//记录该标识符在重复代码中的token序号
							int addFlag = 0;
							for(n = 0; n < CP_Array[m].idsPlaceSet.GetSize(); n++)
							{//在已有的标识符位置表中查找该标志符
								if( CP_Array[m].idsPlaceSet[n].idName == tpHead->tkNode.srcWord )
								{//如果该标识符已被记录，则增加其位置信息
									CP_Array[m].idsPlaceSet[n].idLocArry.Add(idLoc);
									addFlag = 1;
									break;
								}
							}//for-n
							if( addFlag == 0 )
							{//如果该标识符没有被记录，则增加该标识符的位置信息项于标识符位置表中
								IdsPlaceSetMem idPlace;
								idPlace.idName = tpHead->tkNode.srcWord;
								idPlace.idLocArry.Add(idLoc);
								CP_Array[m].idsPlaceSet.Add(idPlace);
							}//if
							
						}//if

					}//if
					tpHead = tpHead->next;

				}//while
				
			}//for-j

		}//for-i

	}//for-m
}
*/
/*wq-20081212*******************

//wq-20081123-根据标识符的位置信息集合表生成标识符映射表集合
void CP_Segment::CpSegIdsMappingSet(CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G)
{
	int i;
	for( i = 0; i < CP_Array.GetSize(); i++ )
	{
		int mapCpSegNum = -1;
		CP_SEG& srcCpSeg = CP_Array[i];
		IniIdMappingSet(srcCpSeg.idsPlaceSet.GetSize(),srcCpSeg.idsMappingSet);

		int j;
		for( j = 0; j < CP_Array.GetSize(); j++ )
		{
			if( j != i )
			{
				mapCpSegNum++;
				FormIdsMappingSet(i,j,mapCpSegNum,Seq_Arry_G);
			}
		}
	}
}

//wq-20081123
void CP_Segment::FormIdsMappingSet(int srcCpSegIndex, int mapCpSegIndex, int mapCpSegNum, CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G)
{
	CP_SEG& srcCpSeg = CP_Array[srcCpSegIndex];
	CP_SEG& mapCpSeg = CP_Array[mapCpSegIndex];
	int i;
	for( i = 0; i < srcCpSeg.idsPlaceSet.GetSize(); i++ )
	{
		IdMapping& idMap = srcCpSeg.idsMappingSet[i];
		idMap.idName = srcCpSeg.idsPlaceSet[i].idName;
		int j;
		for( j = 0; j < srcCpSeg.idsPlaceSet[i].idLocArry.GetSize(); j++ )
		{
			IDENTF_LOCATION &idLoc = srcCpSeg.idsPlaceSet[i].idLocArry[j];

			IDENTF_MAP_NODE idMapNode;
			CopyIdLocNode(idMapNode.idLoc,idLoc);
			idMapNode.mappedId.cpSegIndex = mapCpSegIndex;
			FindMappedIdName(idMapNode.idLoc,mapCpSeg.idsPlaceSet,idMapNode.mappedId.idName);
			idMap.idMappingArry[mapCpSegNum].Add(idMapNode);
			
		}
	}
}

//wq-20081123
void CP_Segment::IniIdMappingSet(int memNum, CArray<IdMapping,IdMapping>& IdMapSet)
{
	int i;
	for( i = 0; i < memNum; i++ )
	{
		IdMapping idMap;
		IdMapSet.Add(idMap);		
	}
}

//wq-20081123
void CP_Segment::FindMappedIdName(const IDENTF_LOCATION& idLoc,
								  const CArray<IdsPlaceSetMem,IdsPlaceSetMem>& dstIdsPlaceSet,
								  CString& mappedIdName)
{
	int i;
	for( i = 0; i < dstIdsPlaceSet.GetSize(); i++ )
	{
		int j;
		for( j = 0; j < dstIdsPlaceSet[i].idLocArry.GetSize(); j++ )
		{
			if( idLoc.dupCodeLocIdx == dstIdsPlaceSet[i].idLocArry[j].dupCodeLocIdx
				&& idLoc.itmNumArryInx == dstIdsPlaceSet[i].idLocArry[j].itmNumArryInx
				&& idLoc.tknLnkNodeCnt == dstIdsPlaceSet[i].idLocArry[j].tknLnkNodeCnt) 
			{
				mappedIdName = dstIdsPlaceSet[i].idName;
			}
		}
	}
}

//wq-20081123
void CP_Segment::OutputIdMappingSet(int cpArrayNum)
{
	int i;
	CString filepath = _T("D:\\IdMap\\");
	CString filename;
	CString str_cpArrayNum;
	str_cpArrayNum.Format(_T("%d"),cpArrayNum+1);

	

	for( i = 0; i < CP_Array.GetSize(); i++ )
	{
		CString str_idMapSet;
		CString str_i;
		str_i.Format(_T("%d"),i+1);
		filename = str_cpArrayNum + "_";
		filename += str_i + ".txt";
		CString completeFilepath = filepath + filename;
		int j;
		for( j = 0; j < CP_Array[i].idsMappingSet.GetSize(); j++ )
		{
			IdMapping& idMapping = CP_Array[i].idsMappingSet[j];
			str_idMapSet += "\r\n-----------------------------------\r\n";
			str_idMapSet += idMapping.idName + "\r\n";
			int k;
			for( k = 0; k < CPSEGNUM; k++)
			{
				CArray<IDENTF_MAP_NODE,IDENTF_MAP_NODE>& idMap = idMapping.idMappingArry[k];
				if(idMap.GetSize() == 0)
				{
					break;
				}
				CString str_mapSeg;
				str_mapSeg.Format(_T("%d"),idMap[0].mappedId.cpSegIndex+1);
				str_idMapSet += "**********  " + str_mapSeg;
				str_idMapSet += "  **********\r\n";
				int l;
				for( l = 0; l < idMap.GetSize(); l++ )
				{
					CString str_l;
					str_l.Format(_T("%5d"),l);
					str_l += "     ";
					str_idMapSet += str_l + idMap[l].mappedId.idName;
					str_idMapSet += "\r\n";
				}//for-l

			}//for-l

		}//for-j
		CStdioFile outfile;
		if( outfile.Open(completeFilepath,CFile::modeCreate | CFile::modeWrite))
		{
			outfile.WriteString(str_idMapSet);
			outfile.Close();
		}

	}//for-i

}
/**************************************************/
//wq-20081123
/*
void CP_Segment::OutputIdsPlaceSet(int cpArrayNum)
{
	int i;
	CString filepath = _T("D:\\IdPlaceSet\\");
	CString filename;
	CString str_cpArrayNum;
	str_cpArrayNum.Format(_T("%d"),cpArrayNum+1);

	

	for( i = 0; i < CP_Array.GetSize(); i++ )
	{
		CString str_IdPlaceSet;
		CString str_i;
		str_i.Format(_T("%d"),i+1);
		filename = str_cpArrayNum + "_";
		filename += str_i + ".txt";
		CString completeFilepath = filepath + filename;
		int j;
		for( j = 0; j < CP_Array[i].idsPlaceSet.GetSize(); j++ )
		{
			IdsPlaceSetMem& idPlace = CP_Array[i].idsPlaceSet[j];
			str_IdPlaceSet += "\r\n-----------------------------------\r\n";
			str_IdPlaceSet += idPlace.idName + "\r\n";
			int k;
			for( k = 0; k < idPlace.idLocArry.GetSize(); k++ )
			{
				CString str1,str2,str3;
				str1.Format(_T("%5d"),idPlace.idLocArry[k].dupCodeLocIdx);
				str2.Format(_T("%5d"),idPlace.idLocArry[k].itmNumArryInx);
				str3.Format(_T("%5d"),idPlace.idLocArry[k].tknLnkNodeCnt);
				str_IdPlaceSet += str1 + "-";
				str_IdPlaceSet += str2 + "-";
				str_IdPlaceSet += str3 + "\r\n";
			}//for-k
		
		}//for-j
		CStdioFile outfile;
		if( outfile.Open(completeFilepath,CFile::modeCreate | CFile::modeWrite))
		{
			outfile.WriteString(str_IdPlaceSet);
			outfile.Close();
		}//if

	}//for-i
}
*/

//wq-20090223
void CP_Segment::UpdateIdsLocArrayOfCpSegs(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry)
{
	int i;
	for( i = 0; i < CP_Array.GetSize(); i++ )
	{
		CP_SEG& cpSeg = CP_Array[i];
//		cpSeg.UpdateIdsLocArray(filesToTokens);
		cpSeg.UpdateIdsLocArray(headerFilesArry);
	}
}