// CntxtInconsisDetect.h: interface for the CntxtInconsisDetect class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CNTXTINCONSISDETECT_H__0DF6EC49_517B_408F_BEDE_6494FA37AB05__INCLUDED_)
#define AFX_CNTXTINCONSISDETECT_H__0DF6EC49_517B_408F_BEDE_6494FA37AB05__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "FilesToTokens.h"
#include "CP_Segment.h"
#include "SDG.h"
#include "ExpMatchVectorElm.h"

struct CNTXT_INFO		//上下文信息
{
	int cpSegIdx;		//SEG在克隆代码集合中的序号
	int cpLine;			//记录重复代码在显示时的行数位置，是重复代码行则>0,非重复代码行则=0
	int ctxtLine;		//上下文克隆代码片段token行表示中的行数
	int doWhileLine;//wq-20090512- >=0 或 ==-2
};
struct CNTXT_INCNSIS	//上下文不一致性缺陷
{
	int cpArryIdx;	//克隆代码对<SEG1,SEG2>所在的克隆代码集合的序号
	CNTXT_INFO cpSeg1;
	CNTXT_INFO cpSeg2;
	double expMatchValue;
};

enum CNTXT_TYPE{
	TP_ERROR = 0,
	TP_PROG = 1,
	TP_FUNDEF = 2,
	TP_IF = 3,
	TP_FOR = 4,
	TP_WHILE = 5,
	TP_DOWHILE = 6,
	TP_SWITCH = 7,
};
class CntxtInconsisDetect  
{
public:					CString str;
	CntxtInconsisDetect();
	virtual ~CntxtInconsisDetect();

	/*上下文不一致性缺陷检测入口函数
	filesToTokens:	源文件token行转化表示类
	CP_Seg_Array:	所有克隆代码集合的集合
	headerFilesArry:头文件信息集合
	*/
	void operator()(FilesToTokens &filesToTokens,
					CArray<CP_Segment,CP_Segment> &CP_Seg_Array,
					CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry);

	/*对克隆代码对<SEG1,SEG2>进行上下文不一致性缺陷的检测
	filesToToken:	源文件token行转化表示类
	cpSeg1:			克隆代码片段SEG1
	cpSeg2:			克隆代码片段SEG2
	seq:			频繁子序列
	cpArryIdx:		克隆代码对<SEG1,SEG2>所在的克隆代码集合的序号
	cpSegIdx1:		SEG1在克隆代码集合中的序号
	cpSegIdx2:		SEG2在克隆代码集合中的序号
	headerFilesArry:头文件信息集合
	*/
	void CPPairCntxtIncnsisDtct(FilesToTokens &filesToTkens,
								CP_SEG &cpSeg1, 
								CP_SEG &cpSeg2, 
								CString seq,
								int cpArryIdx, 
								int cpSegIdx1, 
								int cpSegIdx2,
								CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry);
//	bool GetTknListForCPSeg(CArray<TokenLine,TokenLine> &tknList,FilesToTokens &filesToTokens, CP_SEG &cpSeg);

	/*
	返回一个克隆代码片段的token行表示
	filesToTokens:	源文件token行转化表示类
	cpSeg:			克隆代码片段
	headerFilesArry:头文件信息集合
	*/
	bool GetTknListForCPSeg(TokenedFile &tknFile,
							FilesToTokens &filesToTokens, 
							CP_SEG &cpSeg,
							CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry);//wq-20090505

	/*
	获取某一克隆代码行的上下文信息
	cntxtInfo:	一个克隆代码行的上下文信息
	cpSeg:		克隆代码行所在的克隆代码片段
	lnIdx:		克隆代码行数
	tknList:	克隆代码行对应的token行信息
	*/
	void BuildCPSegCntxtInfo( CNTXT_INFO &cntxtInfo, CP_SEG &cpSeg, int lnIdx, 
							  CArray<TokenLine,TokenLine> &tknList);
	void IniCntxtInfo( CNTXT_INFO &cntxtInfo );
	
	/*初始化一个上下文不一致性缺陷记录
	cntxtIncnsis:	一个上下文不一致性缺陷记录
	*/
	void IniCntxtIncnsis( CNTXT_INCNSIS &cntxtIncnsis );
/*	void CntxtIncnsisJudge( CNTXT_INCNSIS &cntxtIncnsis,
							CArray<TokenLine,TokenLine> &tknList1,
							CArray<TokenLine,TokenLine> &tknList2,
							CP_SEG &cpSeg1, CP_SEG &cpSeg2);*/

	/*
	判别上下文不一致性缺陷
	cntxtIncnsis:	一个上下文不一致性缺陷记录
	tknFile1:		SEG1对应的token行存储
	tknFile2:		SEG2对应的token行存储
	cpSeg1:			克隆代码片段SEG1
	cpSeg2:			克隆代码片段SEG2
	*/
	void CntxtIncnsisJudge( CNTXT_INCNSIS &cntxtIncnsis,
							TokenedFile &tknFile1,
							TokenedFile &tknFile2,
							CP_SEG &cpSeg1, CP_SEG &cpSeg2);//wq-20090505

	/*
	判断某一上下文结构的类别；int返回上下文结构类别的枚举值
	ctxtLine:	上下文克隆代码片段token行表示中的行数
	tknList:	克隆代码片段的token行存储列表
	*/
	int CntxtTypeJudge( int ctxtLine, CArray<TokenLine,TokenLine> &tknList );
	bool FindKey(CArray<TNODE,TNODE> &tknLine, int key);
	bool LoopCntxtJudge( int line, CArray<TokenLine,TokenLine> &tknList );
	bool CompareCntxtIncnsis( CNTXT_INCNSIS &cntxtIncnsis1, CNTXT_INCNSIS &cntxtIncnsis2);
	
	/*
	判断上下文不一致性缺陷cntxtIncnsis是否已经存在于记录cntxtIncnsisArry中
	cntxtIncnsis:	一个上下文不一致性缺陷
	cntxtIncnsisArry:上下文不一致性缺陷记录
	*/
	bool CntxtIncnsisExisted(CNTXT_INCNSIS &cntxtIncnsis,CArray<CNTXT_INCNSIS,CNTXT_INCNSIS> &cntxtIncnsisArry);

	/*
	克隆代码对的两个上下文条件谓词匹配相似度的计算；double返回上下文条件谓词匹配相似度
	ctxtLine1:	上下文1在克隆代码SEG1的token行表示中的行数
	ctxtLine2:	上下文2在克隆代码SEG2的token行表示中的行数
	tknFile1:	克隆代码SEG1的token行表示
	tknFile2:	克隆代码SEG2的token行表示
	ctxtType1:	上下文1的上下文结构类型
	ctxtType2:	上下文2的上下文结构类型
		*利用“机考系统”的系统依赖图节点表达式匹配
			利用机考系统的CSDGBase等与系统依赖图有关的类；
			为C1和C2分别生成系统依赖图节点SDG（C1）和SDG（C2）；
			利用CExpression类为SDG（C1）和SDG（C2）建立表达式语法树并表达式的标准化；
			利用CProgramMatch:;BoolExpressionMatch()函数进行表达式语法树的匹配。
	*/
	double ExpressionMatch(int ctxtLine1,int ctxtLine2,TokenedFile &tknFile1,TokenedFile &tknFile2,
						  int ctxtType1, int ctxtType2);//wq-20090505
	bool IfStatementSDG(CSDGIf *pIf, TokenedFile &tknFile, int ctxtLine);//wq-20090507-为if语句构造SDG结点和表达式语法树
	bool FindIfExpBody(CSDGIf *pIf,CArray<TNODE,TNODE> &tknLine);//wq-20090507
	bool DoWhileStatementSDG(CSDGDoWhile *pDo, TokenedFile &tknFile, int ctxtLine);//wq-20090507-为do-while语句构造SDG结点和表达式语法树
	bool FindDoWhileExpBody(CSDGDoWhile *pDo,CArray<TNODE,TNODE> &tknLine);//wq-20090507
	bool WhileStatementSDG(CSDGWhile *pWhile, TokenedFile &tknFile,  int ctxtLine );//wq-20090507-为while语句构造SDG结点和表达式语法树
	bool FindWhileExpBody(CSDGWhile *pWhile,CArray<TNODE,TNODE> &tknLine);//wq-20090507
	bool ForStatementSDG(CSDGFor *pFor, TokenedFile &tknFile, int ctxtLine);//wq-20090507-为for语句构造SDG结点和表达式语法树
	bool FindForExpBody(CSDGFor *pFor,CArray<TNODE,TNODE> &tknLine);//wq-20090507
public:
	double ComputeVectorsSimilarity(ExpMatchVectorElm &expMV1,ExpMatchVectorElm &expMV2);
	int ComputeFuncParaNum(int &idx,CArray<TNODE,TNODE> &expTArry);
	bool GenerateExpVector(CArray<TNODE,TNODE> &expTArry,ExpMatchVectorElm &expMV);
	double GetMatchVectorElmType(int key,int &vecPos);
	bool ReadCFile(const CString cfilename, CString &program);

	//记录所有上下文结构类别不一致性缺陷
	CArray<CNTXT_INCNSIS,CNTXT_INCNSIS> cntxtIncnsisType1;

	//记录所有上下文条件谓词不一致性缺陷
	CArray<CNTXT_INCNSIS,CNTXT_INCNSIS> cntxtIncnsisType2;

	//克隆代码最小行数阈值
	int Min_len;

	double mchValThrhld;//wq-20090911-表达式相似度匹配阈值

};

#endif // !defined(AFX_CNTXTINCONSISDETECT_H__0DF6EC49_517B_408F_BEDE_6494FA37AB05__INCLUDED_)
