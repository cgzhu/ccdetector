// SDGEntry.h: interface for the CSDGEntry class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGENTRY_H__0F4A84E0_9AE7_4DB1_A889_4261AE5EB0CC__INCLUDED_)
#define AFX_SDGENTRY_H__0F4A84E0_9AE7_4DB1_A889_4261AE5EB0CC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGEntry : public CSDGBase
{
public:
	CSDGEntry();
	virtual ~CSDGEntry();
public:
	CString m_sFName;
	int     m_iAddr;
	int     FPOS;
public:
	
};

#endif // !defined(AFX_SDGENTRY_H__0F4A84E0_9AE7_4DB1_A889_4261AE5EB0CC__INCLUDED_)
