/*
 * 这个类存放了一些自定义的数据结构
 */

#include "stdafx.h"
#include "dataStruct.h"

//Polaris-20150216
#include "MainFrm.h"

CString GetMemoryInfo()//DWORD processID )
{
    HANDLE hProcess;
	hProcess = GetCurrentProcess();
	DWORD processID;
    processID = GetCurrentProcessId();
    PROCESS_MEMORY_COUNTERS pmc;

    // Print the process identifier.

    printf( "\nProcess ID: %u\n", processID );

    // Print information about the memory usage of the process.

	CString sMemSize("");

    if (NULL == hProcess)
        return sMemSize;

    if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
    {
		long memSize = pmc.WorkingSetSize/1024;/*WorkingSetSize/1024 = 任务管理器看到的内存量*/
		sMemSize.Format("%10d",memSize);
		sMemSize += "KB";
    }

    CloseHandle( hProcess );
	return sMemSize;
}

/*
 * 函数功能：写MemoryRecord文件到输出目录中
 */
void OutputMemoryRecord(CString className, CString funcName, CString outPath, int timeClean, CString funcInfo)
{
	static long times;
	static long lines = 0;
	SYSTEMTIME sys;
	GetLocalTime( &sys );
	CString stime;
	stime.Format("%2d时%2d分%2d秒",sys.wHour,sys.wMinute,sys.wSecond);
//	stime += "\r\n";

    int txtNum = lines/500000 +1;
	CString sTxtNum;
	sTxtNum.Format("%d",txtNum);
	CString txtName = "\\MemoryRecord" + sTxtNum + ".txt";
	if( timeClean == 0 )
	{
		stime += "\r\n";
		times = 0;
		CStdioFile file;
//		if(file.Open( outPath+"\\MemoryRecord.txt",CFile::modeWrite))
		if(file.Open( outPath+txtName,CFile::modeWrite))
		{
			file.SeekToEnd();
			file.WriteString(stime);
			file.Close();
		}
		else if(file.Open( outPath+txtName,CFile::modeCreate|CFile::modeWrite))
		{
			file.WriteString(stime);
			file.Close();
		}
	}
	else
	{
		times++;
		CString sTimes;
		sTimes.Format("%12d",times);
		CString sClassFunc;
		sClassFunc = className + "::" + funcName;
		CString sFormat;
		sFormat.Format("%50s",sClassFunc);
		CString outText("");
//		outText += sTimes + ": " + GetMemoryInfo() + " ---> " + className + "::" + funcName + "()-------------" + stime;
		outText += sTimes + ": " + GetMemoryInfo() + " ---> " + sFormat + "()-------------" + stime;
		outText += "    " + funcInfo + "\r\n";

		//Polaris-在这里添加发送写输出窗口的消息的语句-20150216
        AfxGetMainWnd()->SendMessage(WM_UPDATE_OUTPUT,WPARAM(TRUE),(LPARAM)outText.GetBuffer(20));
		//

		CStdioFile file;
		if(file.Open( outPath+txtName,CFile::modeWrite))
		{
			file.SeekToEnd();
			file.WriteString(outText);
			file.Close();
		}
		else if(file.Open( outPath+txtName,CFile::modeCreate|CFile::modeWrite))
		{
			file.WriteString(outText);
			file.Close();
		}
	}
	lines++;
}