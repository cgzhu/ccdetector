// RCListCtrl.cpp : 实现文件
//
/*
 * 这个类是自己实现的显示冗余代码结果的List，支持双击打开对应文件
 */

#include "stdafx.h"
#include "AllDetector.h"
#include "RCListCtrl.h"


// CRCListCtrl

IMPLEMENT_DYNAMIC(CRCListCtrl, CListCtrl)

CRCListCtrl::CRCListCtrl()
{

}

CRCListCtrl::~CRCListCtrl()
{
}


BEGIN_MESSAGE_MAP(CRCListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CRCListCtrl::OnNMDblclk)
END_MESSAGE_MAP()



// CRCListCtrl 消息处理程序



/*
 * 函数功能：列表双击事件的响应函数
 */
void CRCListCtrl::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	
	for(int i=0; i<this->GetItemCount(); i++)
    {
         if( this->GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED )
         {
              CString filename = this->GetItemText(i,0);
			  AfxMessageBox(filename);
			  AfxGetApp()->OpenDocumentFile(filename);
         }
    }

	*pResult = 0;
}
