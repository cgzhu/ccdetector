// ExpMatchVectorElm.cpp: implementation of the ExpMatchVectorElm class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllDetector.h"
#include "ExpMatchVectorElm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ExpMatchVectorElm::ExpMatchVectorElm()
{
	var = 0;//变量的个数-------------------------0
	cstf = 0;;//数值常量的个数-------------------1
	cstc = 0;;//字符常量的个数-------------------2
	csts = 0;;//字符串常量的个数-----------------3
	rlt = double(0);//关系运算符，=权值之和------4
	lgc = double(0);//逻辑运算符，=权值之和------5
	mth = double(0);//算术运算符，=权值之和------6
	bfnc = 0;//C库函数个数-----------------------7
	dfnc = 0;//自定义函数函数个数----------------8
	bfncPara = 0;//C库函数所有参数总数-----------9
	dfncPara = 0;//自定义函数所有参数总数--------10

}

ExpMatchVectorElm::~ExpMatchVectorElm()
{

}
