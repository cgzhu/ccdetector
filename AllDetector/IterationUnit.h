// IterationUnit.h: interface for the CIterationUnit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITERATIONUNIT_H__597F61AD_1EDA_4ED9_95F1_B6E02E038FBC__INCLUDED_)
#define AFX_ITERATIONUNIT_H__597F61AD_1EDA_4ED9_95F1_B6E02E038FBC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CIterationUnit : public CSDGBase  
{
public:
	CIterationUnit();
	virtual ~CIterationUnit();
public:
	CString m_sAsi;
	ENODE *GetExpression();
};

#endif // !defined(AFX_ITERATIONUNIT_H__597F61AD_1EDA_4ED9_95F1_B6E02E038FBC__INCLUDED_)
