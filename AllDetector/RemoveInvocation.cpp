// RemoveInvocation.cpp: implementation of the RemoveInvocation class.
//关于函数内联的操作//
//wtt////////////3.17日开始编写///////////////////////////
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RemoveInvocation.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Expression.h"
#include "IterationUnit.h"
#include "SDGDeclare.h"
#include "SDGBase.h"
#include "SDGCall.h"
#include "AssignmentUnit.h"
#include "PointerAnalysis.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//extern void  ExDeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);////3.17
//extern void ExRenameAndReorder(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);///wtt//3.17
extern int FindIndex(CSDGBase*p);
extern CString GetExpString(ENODE*pHead, CArray<SNODE,SNODE> &SArry);
extern int IsSimple(ENODE*p);
void ExSetInOutDataFlow(CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
extern  void RemoveEmptyBranchEx(CSDGBase *pHead);

RemoveInvocation::RemoveInvocation()
{


}

RemoveInvocation::~RemoveInvocation()
{

}

void RemoveInvocation::operator()(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
//数据流设置等一些预处理操作
		if(SArry.GetSize()<=0||FArry.GetSize()<=0)
		return ;
			
	SetInitAddr(SArry);
	

	preatreatment(pUSDGHead,SArry,FArry);
		
	
	SetDataFlow(pUSDGHead,SArry,FArry);


//以下为函数内联操作部分
///////////////////////////////////////////////////////////////////////////////////	
	if(FArry.GetSize()<=1)
	{	
		PostTreatment(pUSDGHead,SArry,FArry);//wtt//6.3//把全局变量的声明和初始化语句加入内联后的main函数中，删除除main函数外其它的PDG
		
//		
//		DeleteUnrefered(pUSDGHead,SArry,FArry);///////wtt   2007.11.13///
/////////////////wtt  2008.3.24//////////////////////////////////
		PointerAnalysis pointeranalysis;
		pointeranalysis(pUSDGHead,SArry,FArry);//对被调函数过程内指针分析
		pointeranalysis.mustAliasReplace(pUSDGHead,SArry,FArry);//必然别名替换
		DeleteUnrefered(pUSDGHead,SArry,FArry);
		/////////////////wtt  2008.3.24//////////////////////////////////

	 	ExSetInOutDataFlow(pUSDGHead,SArry);//保留李永浩

		VariablesRename(pUSDGHead,SArry);

		SetSDGNodeName(pUSDGHead,SArry);
		
		
		

		ResetStatementOrder(pUSDGHead,SArry);//保留李永浩
		
		SetDataFlow(pUSDGHead,SArry,FArry);
		


		return;
	}//只有一个函数不需内联
//	AfxMessageBox("function invocation");/////

//建立PDG列表便于查找各PDG
	CArray<CSDGBase*,CSDGBase*>SDGList;
	BuildSDGList(pUSDGHead,SDGList);
	/////////////////////////////////////////////
/*	CString s;
	s.Format("SDGList->GetSize()=%d",SDGList.GetSize());
	AfxMessageBox(s);*/
	//////////////////////////////////////////
//建立调用位置列表和FArry中的calleeList	 
	CSDGBase*CallPosList[20][20];
	for(int i=0;i<20;i++)
		for(int j=0;j<20;j++)
			CallPosList[i][j]=NULL;	

	
    for(int i=0;i<SDGList.GetSize()&&i<20;i++)
	{	
		int faddr=-1;
		if(SDGList[i]->TCnt.GetSize()>0)
			 faddr=SDGList[i]->TCnt[0].addr;
		if(faddr>=FArry.GetSize()||faddr<0)
			continue;
	
 		for(int k=0;k<20;k++)
		{
			FArry[faddr].calleelist[k]=-1;

		}
	//	AfxMessageBox(SDGList[i]->TCnt[0].name+"build call pos list");///////

		BuildCallPosList(SDGList[i],CallPosList[i]);

        FArry[faddr].calleenum=0;
		for(int k=0;k<20&&CallPosList[i][k]!=NULL;k++)
		{
					
			 CSDGBase*ptemp=CallPosList[i][k];
			 if(ptemp&&ptemp->m_pinfo)
			 {
			   FArry[faddr].calleelist[k]=ptemp->m_pinfo->T.addr;  
			   FArry[faddr].calleenum++;
			 }
		}

	//////////////////////////测试输出函数调用列表//////////////////////////////////
	/*	CString str,stemp=SDGList[i]->TCnt[0].name+" call: ";
	
		for(int j=0;j<20;j++)
		{
			if(CallPosList[i][j]!=NULL)
			{				
					stemp+=CallPosList[i][j]->m_pinfo->T.name+"; ";    
			}
		}
		AfxMessageBox(stemp);*/
		////////////////////////////////////////////////////
	}
	
	//////建立简单函数调用树////
	bool flag=false;
	FCNODE*	pCTHead=NULL;

	flag=BuildCallGraph(pCTHead,SArry,FArry);
	
	if(flag==false)
	{//是递归函数调用不进行内联展开
	
	//	AfxMessageBox("iteration invocation");
		DeleteUnrefered(pUSDGHead,SArry,FArry);

		DeleteCallTree(pCTHead);
		
		FunctionRename(pUSDGHead,SDGList,CallPosList,SArry,FArry);
///////////////////////wtt  2008.3.28//////////////////		
		PointerAnalysis pointeranalysis;
		pointeranalysis(pUSDGHead,SArry,FArry);//对被调函数过程内指针分析
		pointeranalysis.mustAliasReplace(pUSDGHead,SArry,FArry);//必然别名替换
		DeleteUnrefered(pUSDGHead,SArry,FArry);
		////////////////////////////////////////////
		
		ExSetInOutDataFlow(pUSDGHead,SArry);

		VariablesRename(pUSDGHead,SArry);

		SetSDGNodeName(pUSDGHead,SArry);
		
	

		ResetStatementOrder(pUSDGHead,SArry);
		
		SetDataFlow(pUSDGHead,SArry,FArry);

		SDGList.RemoveAll(); 
		return;
	}

		

	

	Inline(pCTHead,NULL,pUSDGHead,0,SDGList,CallPosList,SArry,FArry);

	
	PostTreatment(pUSDGHead,SArry,FArry);//把全局变量的声明和初始化语句加入内联后的main函数中，删除除main函数外其它的PDG
///////////////////////////////////////////////////////////////////////////	
	///其它的操作//变量重命名和重新排列语句的顺序////

		///////////wtt  2008.3.28////把内联后的main函数也进行指针分析，执行必然别名替换/////////

	PointerAnalysis pointeranalysis;
    pointeranalysis(pUSDGHead,SArry,FArry);//对被调函数过程内指针分析
	pointeranalysis.mustAliasReplace(pUSDGHead,SArry,FArry);//必然别名替换
   
	//////////////////////////////////////////
    pointeranalysis(pUSDGHead,SArry,FArry);//对被调函数过程内指针分析


	SetDataFlow(pUSDGHead,SArry,FArry);

	DeleteUnrefered(pUSDGHead,SArry,FArry);
	
	
	ExSetInOutDataFlow(pUSDGHead,SArry);


	VariablesRename(pUSDGHead,SArry);

	SetSDGNodeName(pUSDGHead,SArry);
		
		

	ResetStatementOrder(pUSDGHead,SArry);
		
	SetDataFlow(pUSDGHead,SArry,FArry);

	SDGList.RemoveAll(); 
	DeleteCallTree(pCTHead);

}


//////////////////////////以下为////////函数内联的预处理部分/////////////
void RemoveInvocation::preatreatment(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//函数内联的预处理部分:将函数调用语句、return中的表达式、实参中的表达式单独提出用临时变量替换并将各变量重命名使各变量名称没有相同的
	 do
   {
	modified=false;
	RemoveExpInReturn(pUSDGHead,SArry,FArry);
   	RemoveParaExp(pUSDGHead,SArry,FArry);
	RemoveCallInExp(pUSDGHead,SArry,FArry);//
   }while(modified==true);


}
void RemoveInvocation::SetInitAddr(CArray<SNODE,SNODE> &SArry)
{//设置符号表中初始的initaddr
	for(int k=0;k<SArry.GetSize();k++)
	{
		SArry[k].initaddr=k; 
	}
}
//extern CString GetENodeStr(ENODE*p);
void RemoveInvocation::RemoveParaExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//wtt编写：//3.13///将函数调用中实参表达式用临时变量替换
	if(p==NULL)
	{
		return ;
	}

	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveParaExp(p->GetNode(j),SArry,FArry);
		
	}	

	if(!(p->m_sType=="SDGCALL"||p->m_sType=="ASSIGNMENT"&&p->m_pinfo&&p->m_pinfo->T.key==CN_DFUNCTION&&p->m_pinfo->pleft==NULL&&p->m_pinfo->pright==NULL ))
		return ;
	if(p->m_pinfo==NULL)
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int index;
	
	for(int i=0;i<10;i++)
	{
		if(p->m_pinfo&&p->m_pinfo->pinfo[i]!=NULL&&IsSimple(p->m_pinfo->pinfo[i])<0 )
		{
		
			SNODE SN;
	
			if(p->m_pinfo->T.key==CN_BFUNCTION)
			SN.type=CN_INT;
			else
			{
				if(p->m_pinfo->T.addr>=0&&p->m_pinfo->T.addr<FArry.GetSize()&&FArry[p->m_pinfo->T.addr].plist[i]>=0&&FArry[p->m_pinfo->T.addr].plist[i]<SArry.GetSize())
				SN.type= SArry[FArry[p->m_pinfo->T.addr].plist[i]].type;  
			}

			SN.kind=CN_VARIABLE;
			SN.arrdim=0;
			for(int j=0;j<4;j++)
				SN.arrsize[j]=0 ;
			SN.addr=-1;
			SN.value=0;
			CSDGBase*pf1;
			pf1=p;
		
			while(pf1&&pf1->m_sType!="SDGENTRY" )
			{
				pf1=pf1->GetFather(); 
			}
			if(pf1==NULL)
				return;
			if(pf1->TCnt.GetSize()<=0 )
				return;
			SN.layer=pf1->TCnt[0].addr+1;
		
			CString s;
			s.Format("_%d",SArry.GetSize());
			SN.name="para_"+s;
	
			SN.initaddr=SArry.GetSize(); 
			SArry.Add(SN);

			CSDGDeclare*pdec=new CSDGDeclare;
			pdec->m_sType="#DECLARE";		

			TNODE T;
			T.key=CN_VARIABLE;
			T.addr=SArry.GetSize()-1;
			T.name=SN.name;
			T.deref=0;//wtt 2008 3.19
			pdec->TCnt.Add(T); 
			pf1->InsertNode(0,pdec);
			pdec->SetFather(pf1); 

			CAssignmentUnit*pas=new CAssignmentUnit;
			pas->m_sType="ASSIGNMENT";
			ENODE*pExpl=new ENODE;
			InitialENODE(pExpl);
			pExpl->T.key=CN_VARIABLE;
			pExpl->T.addr=SArry.GetSize()-1;
			pExpl->T.name=SN.name;
			pExpl->info=0;
			pExpl->T.deref=0;//wtt 2008 3.19
			pExpl->pleft=NULL;
			pExpl->pright=NULL; 
		
			pas->m_pleft=pExpl; 
			pas->m_pinfo=p->m_pinfo->pinfo[i];

			ENODE*pExp=new ENODE;
			InitialENODE(pExp);
			pExp->T.key=CN_VARIABLE;
			pExp->T.addr=SArry.GetSize()-1;
			pExp->T.deref=0;//wtt 2008 3.19
			pExp->T.name=SN.name;
			pExp->pleft=NULL;
			pExp->pright=NULL;

			p->m_pinfo->pinfo[i]=pExp;

			index=FindIndex(p);
			pf->InsertNode(index,pas);
			pas->SetFather(pf); 
			modified=true;

		}
	}

}
void RemoveInvocation::RemoveCallInExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//wtt编写：//3.14///将表达式中函数调用语句用临时变量替换
	if(p==NULL)
	{
		return ;
	}	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveCallInExp(p->GetNode(j),SArry,FArry);
		
	}
	
	if(!(p->m_sType=="ASSIGNMENT"||p->m_sType=="SDGRETURN"||p->m_sType=="SELECTOR"||p->m_sType=="ITERATION"))
		return ;
	if(p->m_pinfo==NULL)
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int index;


	if(pf==NULL)
		return;
	
	if(p->m_sType=="ASSIGNMENT"&&p->m_pinfo->pleft==NULL&&p->m_pinfo->pright==NULL )
		return;///只有一个函数调用的 赋值语句或不含函数调用的赋值语句不需处理
	else if((p->m_sType=="SDGRETURN"||p->m_sType=="SELECTOR"||p->m_sType=="ITERATION")&&p->m_pinfo->pleft==NULL&&p->m_pinfo->pright==NULL)
	{//只有一个函数调用语句
		if(p->m_pinfo->T.key!=CN_DFUNCTION&& p->m_pinfo->T.key!=CN_BFUNCTION )
			return;
	//	AfxMessageBox("find single function call");/////////////

	//	if(pf->m_sType=="SELECTION")
	//	{
	//		AfxMessageBox("SELection sub declare");
	//	}
		SNODE SN;
		SN.type=CN_INT;/////
		
		if(p->m_pinfo->T.key==CN_BFUNCTION)
		SN.type=CN_INT;
		else if(p->m_pinfo->T.key==CN_DFUNCTION)
		{
		/*	AfxMessageBox("dfunction");
			//////////////////////////////////////////////
			SN.type= FArry[p->m_pinfo->T.addr].rtype;
			CString s;
			s.Format(FArry[p->m_pinfo->T.addr].name+"rtype=%d",FArry[p->m_pinfo->T.addr].rtype) ;
			AfxMessageBox(s);*/
		//////////////////////////////////////////////
		}

		SN.kind=CN_VARIABLE;
		SN.arrdim=0;
		int j;
		for( j=0;j<4;j++)
			SN.arrsize[j]=0 ;
		SN.addr=-1;
		SN.value=0;
		CSDGBase*pf1=NULL;
		pf1=p;
		
		while(pf1&&pf1->m_sType!="SDGENTRY" )
		{
		pf1=pf1->GetFather(); 
		}
		if(pf1==NULL)
			return;
		if(pf1->TCnt.GetSize()<=0 )
			return;
		SN.layer=pf1->TCnt[0].addr+1;
	
		CString str;
		str.Format("_%d",SArry.GetSize());
		SN.name="call_"+p->m_pinfo->T.name+str;	
		////////////////////////
	///////////////////////////3.25/////////////////////
	bool flag=true;
	int idx;
	CSDGBase*psbl=NULL;
	if(p->m_sType=="SELECTOR")
		{//只为第一个selector提出exp付给var，其他selector用var替换exp即可
			CSDGBase*psf=p->GetFather();
			
			if(psf==NULL)
				return;
			 idx=FindIndex(p);
			if(idx>0)
			{
				psbl=psf->GetNode(idx-1);
				if(psbl&&psbl->m_sType=="SELECTOR")
				{
					flag=false;
				//	AfxMessageBox("already substracted");////////////////
				}

			}
		}
		//////////////////3/25///////////////////////////////
	

//		if(pf->m_sType!="SELECTION"||!(pf->m_sType=="SELECTION"&&pf1->GetNode(0)&&pf1->GetNode(0)->m_sType=="#DECLARE"&&pf1->GetNode(0)->TCnt[0].name==SN.name))
		if(pf->m_sType!="SELECTION"||flag)

		{	//创建变量声明节点
		SN.initaddr=SArry.GetSize(); 
	//	AfxMessageBox("add SN:"+SN.name);//////////////
		SArry.Add(SN);
		CSDGDeclare*pdec=new CSDGDeclare;
		pdec->m_sType="#DECLARE";		
		TNODE T;
		T.key=CN_VARIABLE;
		T.addr=SArry.GetSize()-1;
		T.deref=0;//wtt 2008 3.19
		
		T.name=SN.name;
		pdec->TCnt.Add(T); 
		pf1->InsertNode(0,pdec);
		pdec->SetFather(pf1); 
	

		CAssignmentUnit*pas=new CAssignmentUnit;
		pas->m_sType="ASSIGNMENT";
		ENODE*pExpl=new ENODE;
		InitialENODE(pExpl);
		pExpl->T.key=CN_VARIABLE;
		pExpl->T.addr=SArry.GetSize()-1;
		pExpl->T.name=SN.name;
		pExpl->T.deref=0;//wtt 2008 3.19
		pExpl->info=0;
		pExpl->pleft=NULL;
		pExpl->pright=NULL; 
		
		pas->m_pleft=pExpl; 
		pas->m_pinfo=p->m_pinfo;

		ENODE*pExp=new ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_VARIABLE;
		pExp->T.addr=SArry.GetSize()-1;
		pExp->T.deref=0;//wtt 2008 3.19
		pExp->T.name=SN.name;
		pExp->pleft=NULL;
		pExp->pright=NULL;

		p->m_pinfo=pExp;
		///////////////////////////
		if(p->m_sType=="ITERATION")
		{
			int j=p->GetRelateNum(); 
			p->InsertNode(j,pas); 
		}
	//////////////////////
		
	    if(pf->m_sType!="SELECTION")
		{
		index=FindIndex(p);	
		pf->InsertNode(index,pas);
		pas->SetFather(pf); 
		}
		else
		{

			CSDGBase*pff=NULL;
			pff=pf->GetFather();
			if(!pff)
				return;
		//	AfxMessageBox("ok");////
			index=FindIndex(pf);
			pff->InsertNode(index,pas);
		    pas->SetFather(pf); 
 
		}
		if(pf->m_sType=="SELECTION")
		{//记录各个临时变量在符号表中的位置,以便其它selector进行设置
		TNODE T;
		T.addr=SArry.GetSize()-1; 
		T.name=SN.name; 
		T.deref=0;//wtt 2008 3.19
		pf->TCnt.Add(T);  
		}
	}
	else if(flag==false)
	{
	//	AfxMessageBox()
		ENODE*pExp=new ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_VARIABLE;
		pExp->T.addr=pf->TCnt[0].addr  ;//////
		pExp->T.name=pf->TCnt[0].name;//////
        pExp->T.deref=pf->TCnt[0].deref;//////
		pExp->pleft=NULL;
		pExp->pright=NULL;

		p->m_pinfo->pleft=pExp;

	}

		modified=true;
		return;
		

	}

//表达式中既含有函数调用语句也含其它表达式	
	CArray<ENODE*,ENODE*>pCList;
	CArray<ENODE*,ENODE*> pFList;
	CArray<int,int>posList;
	pCList.RemoveAll();
	pFList.RemoveAll();
	posList.RemoveAll(); 
	FindCall(p->m_pinfo,pCList,pFList,posList);/////
	if(pCList.GetSize()==0)
	{
	//	AfxMessageBox("pCList.GetSize()==0"+p->m_sType );
		return;
	}
	
	///////////////////////
	int num=pCList.GetSize();
	/////////////// 测试用//////////////
    /* AfxMessageBox("function in"+p->m_sType);
      for(int k=0;k<pCList.GetSize();k++)
	  {
		 AfxMessageBox( pCList[k]->T.name );
			 
	  }*/

	//////////////////////////////
	int i=0;
	while(i<num)
	{

		SNODE SN;
		SN.type=CN_INT; 
		if(p->m_pinfo&&p->m_pinfo->T.key==CN_BFUNCTION)
		{
			SN.type=CN_INT;
	
		}
	
		else
		{
			
			if(i<pCList.GetSize()&&pCList[i]->T.addr>=0&&pCList[i]->T.addr<FArry.GetSize() )//wtt 2007.11.13//
			SN.type= FArry[pCList[i]->T.addr].rtype;  
		}

		SN.kind=CN_VARIABLE;
		SN.arrdim=0;
		for(int j=0;j<4;j++)
			SN.arrsize[j]=0 ;
		SN.addr=-1;
		SN.value=0;
		CSDGBase*pf1;
		pf1=p;
		
		while(pf1&&pf1->m_sType!="SDGENTRY" )
		{
			pf1=pf1->GetFather(); 
		}
		if(pf1==NULL)
			return;
		if(pf1->TCnt.GetSize()<=0 )
			return;
		SN.layer=pf1->TCnt[0].addr+1;
	
		CString s;
		s.Format("_%d",SArry.GetSize());
		SN.name="call_"+pCList[i]->T.name+s;
	

	//////////////////////////
	///////////////////////////3.25/////////////////////
		bool flag=true;
		int idx;
		CSDGBase*psbl;
		if(p->m_sType=="SELECTOR")
		{
			CSDGBase*psf=p->GetFather();
			
			if(psf==NULL)
				return;
			 idx=FindIndex(p);
			if(idx>0&&psf&&idx<psf->GetRelateNum())
			{
				psbl=psf->GetNode(idx-1);
				if(psbl->m_sType=="SELECTOR"&&FindFun(pCList[i],psbl->m_pinfo))
				{
					flag=false;
				//	AfxMessageBox("already substracted");////////////////
				}

			}
		}
		//////////////////3/25///////////////////////////////




	//////////////////////////////////

	//if(pf->m_sType!="SELECTION"||!(pf->m_sType=="SELECTION"&&pf1->GetNode(0)&&pf1->GetNode(0)->m_sType=="#DECLARE"&&pf1->GetNode(0)->TCnt[0].name==SN.name))///////////
		if(pf->m_sType!="SELECTION"||flag)///////////

		{//注意：为各个Selector中条件表达式中的公共的函数表达式建立一个赋值语句，和变量声明节点
			SN.initaddr=SArry.GetSize(); 
		//	AfxMessageBox("add SN for selector:"+SN.name);//////////////

			SArry.Add(SN);
			CSDGDeclare*pdec=new CSDGDeclare;
			pdec->m_sType="#DECLARE";		
			TNODE T;
			T.key=CN_VARIABLE;
			T.addr=SArry.GetSize()-1;
			T.name=SN.name;
			T.deref=0;//wtt 2008 3.19
			pdec->TCnt.Add(T); 
			pf1->InsertNode(0,pdec);
			pdec->SetFather(pf1); 

			CAssignmentUnit*pas=new CAssignmentUnit;
			pas->m_sType="ASSIGNMENT";
			ENODE*pExpl=new ENODE;
			InitialENODE(pExpl);
			pExpl->T.key=CN_VARIABLE;
			pExpl->T.addr=SArry.GetSize()-1;
			pExpl->T.deref=0;//wtt 2008 3.19
			pExpl->T.name=SN.name;
			pExpl->info=0;
			pExpl->pleft=NULL;
			pExpl->pright=NULL; 
		
			pas->m_pleft=pExpl; 
			pas->m_pinfo=pCList[i];
			
			//AfxMessageBox("new assigment"+pCList[i]->T.name);///////////////////

			ENODE*pExp=new ENODE;
			InitialENODE(pExp);
			pExp->T.key=CN_VARIABLE;
			pExp->T.addr=SArry.GetSize()-1;
			pExp->T.deref=0;//wtt 2008 3.19
			pExp->T.name=SN.name;
			pExp->pleft=NULL;
			pExp->pright=NULL;

			if(posList[i]==1)
				pFList[i]->pleft=pExp;
			else
				pFList[i]->pright=pExp; 
	///////////////////////////
			if(p->m_sType=="ITERATION")
			{

		//////wtt///05.10.20///  判断一下函数中实参表达式是否与循环体中语句数据依赖/
				CArray<TNODE,TNODE>DArry;
				SearchExpReferData(pas,pas->m_pinfo,DArry,SArry);
				int k=0;
				int flag1=1;
				while(k<DArry.GetSize()&&flag)
				{
			
					for(int l=0;l<p->GetRelateNum()&&flag;l++)
					{
						if(p->GetNode(l))
						{
							CSDGBase*pl=p->GetNode(l);
							if(FindTNODEInDArry(DArry[k],pl->m_aDefine))
							{
								flag1=0;
							}
						}
					}
					k++;

				}

		/////////////////如果存在数据依赖则需在循环体重加入该函数调用语句///////
				if(flag1==0)
				{
					int j=p->GetRelateNum(); 
					p->InsertNode(j,pas);
					pas->SetFather(p); 
				}
			}
	//////////////////////

		/*index=FindIndex(p);		
		pf->InsertNode(index,pas);
		pas->SetFather(pf); */
		if(pf->m_sType!="SELECTION")
		{
			index=FindIndex(p);	
			pf->InsertNode(index,pas);
			pas->SetFather(pf); 
		}
		else
		{	

			CSDGBase*pff=NULL;
			pff=pf->GetFather();
			if(!pff)
				return;
		//	AfxMessageBox("ok");////
			index=FindIndex(pf);
			pff->InsertNode(index,pas);
		    pas->SetFather(pf); 

           /* extern CString GetExpString(ENODE*pHead, CArray<SNODE,SNODE> &SArry );

			AfxMessageBox("insert assignement to selelction"+GetExpString(pas->m_pinfo,SArry));///////////////////////*/

 
		}
		if(pf->m_sType=="SELECTION")
		{//记录各个临时变量在符号表中的位置,以便其它selector进行设置
			TNODE T;
			T.addr=SArry.GetSize()-1; 
			T.deref=0;//wtt 2008 3.19
			T.name=SN.name; 
			pf->TCnt.Add(T);  
		}
	}
	else if(flag==false)
	{
	//	AfxMessageBox()
		ENODE*pExp=new ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_VARIABLE;
		if(pf&&i<pf->TCnt.GetSize())
		{
		pExp->T.addr=pf->TCnt[i].addr  ;//////
		pExp->T.name=pf->TCnt[i].name;//////
		pExp->T.deref=pf->TCnt[i].deref;//////

		}

		pExp->pleft=NULL;
		pExp->pright=NULL;

		if(posList[i]==1)
			pFList[i]->pleft=pExp;
		else
			pFList[i]->pright=pExp; 

	}

	////////////////////////
	i++;
	
	}//////
	
//	modified=true;


}
/////////////wtt/////05.10.21//////////////////////////////////////

bool RemoveInvocation::FindFun(ENODE*pE,ENODE*pT)
{//查找表达式树pT是否包含pE节点，用于selector表达式中函数调用的提出操作
	bool flag=false;
	if(pE&&pT)
	{
		if(pT->T.name.Find( pE->T.name)>=0)
			flag=true;
		else
			flag=FindFun(pE,pT->pleft)||FindFun(pE,pT->pright);


	}
		return flag;

}
///////////////////////////////////////////////////////////////////
void RemoveInvocation::FindCall(ENODE*pExp,CArray<ENODE*,ENODE*>&pCList,CArray<ENODE*,ENODE*> &pFList,CArray<int,int>&posList)
{///wtt编写：//3.14///查找表达式中的函数调用节点保存到pCList中，其父节点保存在pFList中，其是父节点的左孩子还是右孩子保存在posList中
	if(pExp==NULL)
		return;
	FindCall(pExp->pleft,pCList,pFList,posList);
	if(pExp->pleft&&(pExp->pleft->T.key==CN_BFUNCTION||pExp->pleft->T.key==CN_DFUNCTION ) )
	{
		pCList.Add (pExp->pleft);
		pFList.Add (pExp);
		posList.Add(1); //是父节点的左孩子
	}
	if(pExp->pright&&(pExp->pright->T.key==CN_BFUNCTION||pExp->pright->T.key==CN_DFUNCTION ) )
	{
		pCList.Add (pExp->pright);
		pFList.Add (pExp);
		posList.Add(2); //是父节点的右孩子
	}
	FindCall(pExp->pright,pCList,pFList,posList);


}

void RemoveInvocation::RemoveExpInReturn(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//wtt编写：//3.14///将return语句后的表达式用临时变量替换
	if(p==NULL)
	{
		return ;
	}	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveExpInReturn(p->GetNode(j),SArry,FArry);
		
	}
	
	if(p->m_sType!="SDGRETURN")
		return ;
	if(p->m_pinfo==NULL)
		return;
	if(IsSimple(p->m_pinfo)>=0)
		return;
	CSDGBase *pf=p->GetFather(); 
	if(pf==NULL)
		return;
	int index;
	SNODE SN;
	SN.kind=CN_VARIABLE;
	SN.arrdim=0;
	for(int j=0;j<4;j++)
		SN.arrsize[j]=0 ;
	SN.addr=-1;
	SN.value=0;
	CSDGBase*pf1;
	pf1=p;
	while(pf1&&pf1->m_sType!="SDGENTRY" )
	{
		pf1=pf1->GetFather(); 
	}
	if(pf1==NULL)
		return;
	if(pf1->TCnt.GetSize()<=0 )
		return;
	SN.layer=pf1->TCnt[0].addr+1;
	

	CString s;
	s.Format("_%d",SArry.GetSize());

	SN.name="return_"+s;
	if(p->m_pinfo->T.key==CN_DFUNCTION&&pf1->TCnt[0].addr<FArry.GetSize())
		
			SN.type= FArry[pf1->TCnt[0].addr].rtype;  
	else 
		SN.type=CN_INT; 
 	SN.initaddr=SArry.GetSize(); 
	SArry.Add(SN);

	CSDGDeclare*pdec=new CSDGDeclare;
	pdec->m_sType="#DECLARE";		
	TNODE T;
	T.key=CN_VARIABLE;
	T.addr=SArry.GetSize()-1;
	T.deref=0;//wtt 2008 3.19
	T.name=SN.name;
	pdec->TCnt.Add(T); 
	pf1->InsertNode(0,pdec);
	pdec->SetFather(pf1); 

	CAssignmentUnit*pas=new CAssignmentUnit;
	pas->m_sType="ASSIGNMENT";
	ENODE*pExpl=new ENODE;
	InitialENODE(pExpl);
	pExpl->T.key=CN_VARIABLE;
	pExpl->T.addr=SArry.GetSize()-1;
	pExpl->T.deref=0;//wtt 2008 3.19
	pExpl->T.name=SN.name;
	pExpl->info=0;
	pExpl->pleft=NULL;
	pExpl->pright=NULL; 
		
	pas->m_pleft=pExpl; 
	pas->m_pinfo=p->m_pinfo;

	ENODE*pExp=new ENODE;
	InitialENODE(pExp);
	pExp->T.key=CN_VARIABLE;
	pExp->T.addr=SArry.GetSize()-1;
	pExp->T.deref=0;//wtt 2008 3.19
	pExp->T.name=SN.name;

	pExp->pleft=NULL;
	pExp->pright=NULL;

	p->m_pinfo=pExp;
	index=FindIndex(p);
	pf->InsertNode(index,pas);
	pas->SetFather(pf); 
	modified=true;	

}
void RemoveInvocation::InitialENODE(ENODE *pENODE)
{
	pENODE->pleft=NULL;
	pENODE->pright=NULL;
	pENODE->info=0; 
	for(int i=0;i<10;i++)
	{
		pENODE->pinfo[i]=NULL;
	}
	pENODE->T.addr=-1;
	pENODE->T.paddr=-1;
	pENODE->T.key=0;
	pENODE->T.line=-1;
	pENODE->T.value=0;
	pENODE->T.name="";
	pENODE->T.deref=0;//wtt 2008 3.19

}
//////////////////////////以上为////////函数内联的预处理部分/////////////


//////////////////////////////函数调用树部分/////////////////////////////
bool RemoveInvocation::BuildCallGraph(FCNODE *& pHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//创建简单函数调用树
	for(int k=0;k<FArry.GetSize();k++ )
		FArry[k].expanded=0;
	
	int i=0;
	while(i<FArry.GetSize())
	{
		if(FArry[i].name=="main")
			break;
		i++;
	}
//AfxMessageBox("find main");
	if(i>=FArry.GetSize())
		return false;

////////construct main FCNODE///////
	pHead=new FCNODE;
	InitialFCNODE(pHead);
	pHead->addr=i; 
	pHead->id=0; 

//////////////////////////////////////
/*	CString s;
	s.Format("calleenum=%d",FArry[i].calleenum); 
	AfxMessageBox(s);*/
/////////////////////////////////////////

	CallTreeNO=0;
	
	for(int j=0;i>=0&&i<FArry.GetSize()&&j<FArry[i].calleenum&&j<20&&FArry[i].calleelist[j]!=-1;j++)
	{
		
	
		CallTreeNO++;
		
		FCNODE*p=NULL;
		p=new FCNODE;
		InitialFCNODE(p);
		p->addr=FArry[i].calleelist[j]; 
		p->id=CallTreeNO; 
		pHead->children[j]=p;
		p->father=pHead;
	 
		if(FArry[i].calleelist[j]>=0&&FArry[i].calleelist[j]<FArry.GetSize()&&FArry[FArry[i].calleelist[j]].expanded==0)
		{
			//	AfxMessageBox(FArry[FArry[i].calleelist[j]].name+" have not been expanded");


		if(!FuncTree(FArry[i].calleelist[j],p,pHead,SArry,FArry))
		{
		//	AfxMessageBox("!FuncTree(FArry[i].calleelist[j],p,pHead,SArry,FArry)");//////////
			return false;
		}
		}
	
		
	////////add child to main/////
	}
	
	FArry[i].expanded=1;
return true;
	
}

bool RemoveInvocation:: FuncTree(int addr,FCNODE* pHead,FCNODE*pf,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//先根序创建函数调用树，并返回所创建的树的头节点的指针pHead
	

	while(pf)
	{
	
		///////////////////////////////////////
	/*	CString s;
		s.Format("pf->addr=%d,addr=%d",pf->addr,addr);
		AfxMessageBox(s);*/
		//////////////////////////////////////////////
		if(pf->addr==addr)
		{	
		//	AfxMessageBox(" find iteration");
		return false;
		}
		pf=pf->father; 
	}

	for(int j=0;j<FArry[addr].calleenum&&j<20&&FArry[addr].calleelist[j]!=-1;j++)
	{
	//	AfxMessageBox("expand"+FArry[addr].name);/////////

		CallTreeNO++;		
		FCNODE*p=NULL;
		p=new FCNODE;
		InitialFCNODE(p);
		p->addr=FArry[addr].calleelist[j]; 
		p->id=CallTreeNO; 
		pHead->children[j]=p;
		p->father=pHead;
		
		if(FArry[FArry[addr].calleelist[j]].expanded==0)
		{
		//	AfxMessageBox(FArry[FArry[addr].calleelist[j]].name+" have not been expanded");

		if(!FuncTree(FArry[addr].calleelist[j],p,pHead,SArry,FArry))
			return false;
		}
				
		
	
	}
	FArry[addr].expanded=1; 
	return true;
}
void  RemoveInvocation::InitialFCNODE(FCNODE*p)
{//初始化函数调用树节点
	if(p==NULL)
		return;
	p->addr=-1;
	p->father=NULL;
	p->id=-1; 
	for(int i=0;i<20;i++)
	{
		p->children[i]=NULL; 
	}	

}
void ExBuildCallGraph(FCNODE *& pHead,CSDGBase *pUSDGHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//全局函数 在standardview中显示函数调用树用到此函数
	RemoveInvocation removeinvocation;
	bool flag=removeinvocation.BuildCallGraph(pHead,SArry,FArry);
	if(flag==false)

		removeinvocation.DeleteCallTree(pHead);

}
void RemoveInvocation::DeleteCallTree(FCNODE*&pCTHead)
{//后跟序遍历函数调用树	删除函数调用树pCTHead
	
	if(pCTHead==NULL)
		return ;
	for(int i=0;i<20&&pCTHead->children[i]!=NULL;i++)
	{
		pCTHead->children[i]->father=NULL;
		DeleteCallTree(pCTHead->children[i]);
		
		pCTHead->children[i]=NULL;	
	}
	if(pCTHead)
	delete pCTHead;
	pCTHead=NULL;
}
/////////////////////////////函数调用树结束//////////////////////////////////


/////////////////////便于查找相应的SDG/////////////////////////////
void RemoveInvocation::BuildSDGList(CSDGBase *pUSDGHead,CArray<CSDGBase*,CSDGBase*>&SDGList)
{//访问PUSDGHead的各个子节点，若该节点为"SDGENTRY"类型则把它的指针加入SDGList中
	if(pUSDGHead==NULL)
		return;
	for(int i=0;i<pUSDGHead->GetRelateNum();i++)
	{
		if(pUSDGHead->GetNode(i)&&pUSDGHead->GetNode(i)->m_sType=="SDGENTRY")
		{
			SDGList.Add(pUSDGHead->GetNode(i));
		}

	}

}

//////////////////////////////////////////////

///////////////////////////便于查找函数调用节点的位置//建立函数调用位置列表/////////////////
void RemoveInvocation::BuildCallPosList(CSDGBase *pHead,CSDGBase*CallPosList[20])
{//后根序遍历各个PDG的CDS,查找pHead所指的函数的调用自定义的函数(不是系统函数)的节点保存到列表CallPosList中
	if(pHead==NULL)
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		BuildCallPosList(pHead->GetNode(i),CallPosList);		

	}
	
	if((pHead->m_sType=="SDGCALL"||pHead->m_sType=="ASSIGNMENT")&&pHead->m_pinfo &&pHead->m_pinfo->T.key==CN_DFUNCTION)
	{
			
		int i;
		for( i=0;i<20;i++)
		{
			if(CallPosList[i]==NULL)
				break;
		}
		if(i<20)
		{
			CallPosList[i]=pHead;
		//	if(pHead->m_sType=="SDGCALL"&&pHead->TCnt.GetSize()>0  )
			//	faddrArry[i]=pHead->TCnt[0].addr ;
		//	else if(pHead->m_sType=="ASSIGNMENT")
			/*	faddrArry[i]=pHead->m_pinfo->T.addr;  
				CString s;
				s.Format("pHead->m_pinfo->T.addr=%d",faddrArry[i]); 
				AfxMessageBox(s);*/

		}

	}
}
///////////////////////////////////////////////////////////////////////////


/////////////////以下为设置数据流信息//建立数据流依赖边、反依赖边、声明依赖边////
void RemoveInvocation::SetDataFlow(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//设置数据流信息//建立数据流依赖边、反依赖边、声明依赖边
	RemoveInitFLow(pUSDGHead);
	//wtt 2008.4.1指针别名分析
	PointerAnalysis pointeranalysis;
	pointeranalysis(pUSDGHead,SArry,FArry);//对被调函数过程内指针分析
////////////////////////////////
	SetDefAndRef(pUSDGHead,SArry);
	SetGen(pUSDGHead);
	SetKill(pUSDGHead,SArry);
	IterateSetInAndOut(pUSDGHead);
	SetDataDependence(pUSDGHead,pUSDGHead);
	SetDecDependence(pUSDGHead,pUSDGHead);

}
void RemoveInvocation::	RemoveInitFLow(CSDGBase *pHead)
{
	if(pHead==NULL)
		return;
	for(int i=pHead->GetRelateNum();i>=0;i--)
	{
		RemoveInitFLow(pHead->GetNode(i) );

	}

if(pHead->m_aDefine.GetSize()>0)	
	pHead->m_aDefine.RemoveAll();
if(pHead->m_aGen.GetSize()>0)
	pHead->m_aGen.RemoveAll();  
if(pHead->m_aIn.GetSize()>0)
	pHead->m_aIn.RemoveAll();
if(pHead->m_aKill.GetSize()>0)
	pHead->m_aKill.RemoveAll ();
if(pHead->m_aOut.GetSize()>0)
	pHead->m_aOut.RemoveAll();
if(pHead->m_aRefer.GetSize()>0)
	pHead->m_aRefer.RemoveAll();
if(pHead->m_pAntiDRNodes.GetSize()>0)
  	pHead->m_pAntiDRNodes.RemoveAll ();
if(pHead->m_pDecRNodes.GetSize()>0)
	pHead->m_pDecRNodes.RemoveAll();
if(pHead->m_pDRNodes.GetSize()>0)
	pHead->m_pDRNodes.RemoveAll();  
}


///////////////////////////求def和ref集合/////////////////////////////////////////////////
void RemoveInvocation::SetDefAndRef(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

		wtt修改了
	    FUNCTION:
		post-order traverse the USDG ,  set nodes' in data 
		flow and out data flow
		
		PARAMETER:
		pHead : USDG head pointer		
		
	******************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;

	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				//  important codes
			//	AfxMessageBox(p->m_sType);
               
				
				if(p&&p->m_sType!="SDGBREAK" && p->m_sType!="SDGCONTINUE")
				{
					if(p->m_aDefine.GetSize()>0)
						p->m_aDefine.RemoveAll();
					if(p->m_aRefer.GetSize()>0)
						p->m_aRefer.RemoveAll();
					SearchChildrenDataFlow(p,SArry);
				}
				
				//   end of this area 

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
}

void RemoveInvocation::SearchChildrenDataFlow(CSDGBase *pFnd,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Search the node and find the variables it referenced
		and defined.
		PARAMETER:
		pFnd  : USDG node pointer;
	******************************************************/
	if(!pFnd)
	{
		return;
	}

	pFnd->m_aDefine.RemoveAll();
	pFnd->m_aRefer.RemoveAll();

	// Add Children's Data to it;
	/*if(pFnd->m_sType!="ASSIGNMENT" && pFnd->m_sType!="SDGRETURN" &&pFnd->m_sType!="SDGCALL")
	{
		GetSonsDataInfo(pFnd);
	}*//////////wtt/////3.18//////

	if(pFnd->m_sType=="ASSIGNMENT")
	{
		// Add referenced data in right expression of assignment statement
	//	AfxMessageBox("SearchExpReferData1");////////////////
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
		CAssignmentUnit *pUt=(CAssignmentUnit *)pFnd;
        
		// Add Defined data flow 
		if(pUt&&pUt->m_pleft && pUt->m_pleft->T.key==CN_VARIABLE)
		{
				//	AfxMessageBox("FindTNODEInDArry");////////////////

			if(!FindTNODEInDArry(pUt->m_pleft->T,pFnd->m_aDefine))
			{
				pFnd->m_aDefine.Add(pUt->m_pleft->T);

			}

		
		}

		// Add referenced data in left expression of assignment statement
		// if the left variable kind is array
	/*	if(pUt->m_pleft && pUt->m_pleft->T.key==CN_VARIABLE 
			&&pUt->m_pleft->T.addr>=0&&pUt->m_pleft->T.addr<SArry.GetSize()
			&&	SArry[pUt->m_pleft->T.addr].kind==CN_ARRAY
			||SArry[pUt->m_pleft->T.addr].kind==CN_POINTER//wtt 2008.4.4
			||SArry[pUt->m_pleft->T.addr].kind==CN_ARRPTR//wtt 2008.4.4*/
				if(pUt->m_pleft && pUt->m_pleft->T.key==CN_VARIABLE 
			&&pUt->m_pleft->T.addr>=0&&pUt->m_pleft->T.addr<SArry.GetSize()
			&&	(SArry[pUt->m_pleft->T.addr].kind==CN_ARRAY
			||SArry[pUt->m_pleft->T.addr].kind==CN_POINTER//wtt 2008.4.4
			||SArry[pUt->m_pleft->T.addr].kind==CN_ARRPTR)//wtt 2008.4.4
			)//wtt 2009 0716
		{
			for(int i=0;i<10 &&pUt->m_pleft->pinfo[i];i++)
			{
					//	AfxMessageBox("SearchExpReferData2");////////////////

				SearchExpReferData(pFnd,pUt->m_pleft->pinfo[i],pFnd->m_aRefer,SArry);
			}

		}

	}
	else if(pFnd->m_sType=="ITERATION")
	{
				//AfxMessageBox("SearchExpReferData3");////////////////

		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
	}
	else if(pFnd->m_sType=="SDGRETURN")
	{
			//	AfxMessageBox("SearchExpReferData4");////////////////

		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
	}
	else if(pFnd->m_sType=="SDGCALL")
	{
		// Add referenced data in parameter expressions of call node
		
		if(pFnd->m_pinfo && (pFnd->m_pinfo->T.key==CN_DFUNCTION||
		   pFnd->m_pinfo->T.key==CN_BFUNCTION))
		{
		    
			for(int i=0;i<10 &&pFnd->m_pinfo->pinfo[i];i++)
			{
					//	AfxMessageBox("SearchExpReferData5");////////////////

				SearchExpReferData(pFnd,pFnd->m_pinfo->pinfo[i],pFnd->m_aRefer,SArry);
			}
			if(pFnd->m_pinfo->T.key==CN_BFUNCTION&&(pFnd->m_pinfo->T.name=="scanf"||pFnd->m_pinfo->T.name=="gets"))/////////wtt//3.7///)
			{
			//	AfxMessageBox(pFnd->m_pinfo->T.name);///
				pFnd->m_aRefer.RemoveAll();  
			}
					//AfxMessageBox("SetFunctionDefinedData");////////////////


			SetFunctionDefinedData(pFnd,pFnd->m_pinfo,SArry);

		}

	}
	else if(pFnd->m_sType=="SELECTOR")
	{
			//	AfxMessageBox("SearchExpReferData6");////////////////

		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);

	}	
/*	else if(pFnd->m_sType=="ITRSUB")
	{
	
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
	}*////wtt////3.18////////////
	else if(pFnd->m_sType=="#DECLARE" && pFnd->TCnt.GetSize()>0)
	{
		pFnd->m_aDefine.Add(pFnd->TCnt[0]); 
	}	
}

void RemoveInvocation::SetFunctionDefinedData(CSDGBase *pFnd,ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Search the call expression and find the variables it 
		defined.
		PARAMETER:
		pFnd  : USDG node pointer;
		pHead : call expression tree;
	******************************************************/

	int ix=0;
	if(pHead && pHead->T.key==CN_BFUNCTION)
	{
		////////////////////
	/*	CString str;
		str.Format(":%d",pHead->T.addr) ;
		AfxMessageBox(pHead->T.name+str);///*/
/////////////////////////////////////
		switch(pHead->T.addr)
		{
		case 68: //scanf
		case 58://gets
			if(pHead->T.addr==68)
			{
				ix=1;
			}
			else
				ix=0;
			for(;ix<10;ix++)
			{
				if(pHead->pinfo[ix] && pHead->pinfo[ix]->T.key==CN_VARIABLE )
				{
					if(!FindTNODEInDArry(pHead->pinfo[ix]->T,pFnd->m_aDefine))
					{
						pFnd->m_aDefine.Add(pHead->pinfo[ix]->T);
					}

					////////////////////读入的数据是数组元素,则其下标若是变量则存在引用关系/wtt /3.29///
					if( pHead->pinfo[ix]&&pHead->pinfo[ix]->T.addr>=0&&pHead->pinfo[ix]->T.addr<SArry.GetSize()&&SArry[pHead->pinfo[ix]->T.addr].kind==CN_ARRAY)
					{
						for(int i=0;i<10;i++)
						{
							if(pHead->pinfo[ix]->pinfo[i]&&pHead->pinfo[ix]->pinfo[i]->T.key==CN_VARIABLE)
							{
								if(!FindTNODEInDArry(pHead->pinfo[ix]->pinfo[i]->T,pFnd->m_aRefer))
								{
									pFnd->m_aRefer.Add(pHead->pinfo[ix]->pinfo[i]->T);
								}

							}
						}
					}

					//////////////////////////////
				}
			}
			
			break;
		}
	}


}

void RemoveInvocation::SearchExpReferData(CSDGBase *pFnd,ENODE *pHead,CArray<TNODE,TNODE> &DArry,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Search the syntax tree of the nodes' expression and 
		find the variables it referenced,then put them into 
		array "DArry"
		
		PARAMETER:				
		pHead : syntax tree head pointer
		DArry : array which stores the data syntax tree ref-
		        erenced
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	ENODE *p=NULL,*ptemp=NULL;
	
	if(pHead==NULL)
	{
		return;
	}

	p=pHead;
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// If this E-node is Variable Insert it to refered Data
			if(p->T.key==CN_VARIABLE)
			{
				if(!FindTNODEInDArry(p->T,DArry))
				{
					DArry.Add(p->T);
					///////////wtt 2008.4.1/////////考虑指针别名对数据流的影响///
					if(pFnd->ptSet.GetSize()>0)
					{
						ENODE*ptar=NULL,*psrc=NULL;
						for(int i=0;i<pFnd->ptSet.GetSize();i++)
						{
							ptar=pFnd->ptSet[i]->ptar;
							psrc=pFnd->ptSet[i]->psrc;
							if(ptar->T.addr==p->T.addr&&ptar->T.name==p->T.name)
							{
								if(!FindTNODEInDArry(psrc->T,DArry))
								{
								//	AfxMessageBox("refer aliais");
									if(ptar->T.addr>=0&&ptar->T.addr<SArry.GetSize()&&SArry[ptar->T.addr].kind==CN_ARRAY)
									{
										//	AfxMessageBox("array refer aliais");
										DArry.Add(psrc->T);
									}
								/*	else if(ptar->T.addr>=0&&ptar->T.addr<SArry.GetSize()&&(SArry[ptar->T.addr].kind==CN_VARIABLE||SArry[ptar->T.addr].kind==CN_POINTER))
									{
										//	AfxMessageBox("varialbe refer aliais");
										TNODE Tnew;
										Tnew.addr=psrc->T.addr;
										Tnew.deref=psrc->T.deref+1;
										Tnew.key=psrc->T.key;
										Tnew.name=psrc->T.name;
										Tnew.paddr=psrc->T.paddr;
										Tnew.value=psrc->T.value;
										Tnew.line=psrc->T.line;
										DArry.Add(Tnew);



									}*/

								}
							}


						}

					}
					///////////////////////////////////////

				}

				if(p->info>0 && p->info<10)
				{
					for(int i=0;i<10&&p->pinfo[i];i++)
					{
						SearchExpReferData(pFnd,p->pinfo[i],DArry,SArry);
					}
				}
				if(p->info<0 && -p->info<10)//问题提2008.4.4指针算术
				{
					for(int i=0;i<10&&p->pinfo[i];i++)
					{
						SearchExpReferData(pFnd,p->pinfo[i],DArry,SArry);
					}
				}
			}
			else if(p->T.key==CN_BFUNCTION || p->T.key==CN_DFUNCTION )
			{
				SetFunctionDefinedData(pFnd,p,SArry);
			/////////////wtt///////////3.9/////////////////函数实参/////
				for(int i=0;i<10&&p->pinfo[i];i++)
				{
						SearchExpReferData(pFnd,p->pinfo[i],DArry,SArry);
				}
				//////////////wtt///////////////////////////////////////
			}
			// end of this part
						
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			p=STK.GetTail()->pright;
			ptemp=STK.GetTail(); 
			STK.RemoveTail();			
		}
	}
}


bool RemoveInvocation::FindTNODEInDArry(TNODE &TD,CArray<TNODE,TNODE> &DArry)
{
	/******************************************************

	    FUNCTION:
		If the Node TD is in array DArry?	in- return true;
	******************************************************/

	for(int i=0;i<DArry.GetSize();i++)
	{
		if(TD.key==CN_VARIABLE && DArry[i].key==CN_VARIABLE&&
		   TD.addr==DArry[i].addr)// && TD.name==DArry[i].name)
		{
			return true;
		}
	}
	return false;
}


int RemoveInvocation::DefOrRefVariable(TNODE &TD,CSDGBase *pNode)
{
	/******************************************************

	    FUNCTION:
		Is the variable TD defined or refered by branch pNode?	
		if defined return 1.
		if refered return 2.
		if both return 3;
		if neither return 0;
	******************************************************/

	if(pNode==NULL)
		return 0;
	int nDR=0;
	for(int i=0;i<pNode->m_aDefine.GetSize();i++)
	{
		if(TD.key==pNode->m_aDefine[i].key && TD.key==CN_VARIABLE &&
		   TD.addr==pNode->m_aDefine[i].addr)
		{
			nDR=1;
			break;
		}
	}

	for(int i=0;i<pNode->m_aRefer.GetSize();i++)
	{
		if(TD.key==pNode->m_aRefer[i].key && TD.key==CN_VARIABLE &&
		   TD.addr==pNode->m_aRefer[i].addr)
		{
			nDR=nDR==1?3:2;
			break;
		}
	}
	return nDR;
}

int RemoveInvocation::DefOrRefVariableEx(TNODE &TD,CSDGBase *pNode)
{
	/******************************************************

	    FUNCTION:
		Is the variable TD defined or refered by pNode's exp?	
		if defined return 1.
		if refered return 2.
		if both return 3;
		if neither return 0;
	******************************************************/
	if(!pNode)
	{
		return 0;
	}

	int nDR=0;
	CArray<TNODE,TNODE> DArry;
	CArray<SNODE,SNODE> SArry;
    CAssignmentUnit *pas=NULL;
    
	int i;
	if(pNode->m_sType=="ASSIGNMENT")
	{
		AfxMessageBox("Assignment");////
		pas=(CAssignmentUnit *)pNode;
		for(i=0;pas->m_pleft&&i<10;i++)
		{
			if(pas->m_pleft->pinfo[i])
			{
				SearchExpReferData(pNode,pas->m_pleft->pinfo[i],DArry,SArry);
			}
		}

	}
	////////////////////////////////////////
//extern CString PrintExp(CArray<TNODE,TNODE>&T);
/////////////////////////////////////
    SearchExpReferData(pNode,pNode->m_pinfo,DArry,SArry);
/////////////////////////////////////////
//AfxMessageBox(PrintExp(DArry));
///////////////////////////////////////
	
	for(i=0;i<DArry.GetSize();i++)
	{
		if(TD.key==DArry[i].key && TD.key==CN_VARIABLE &&
		   TD.addr==DArry[i].addr)
		{
			
			nDR=1;
			break;
		}
	}
	
	if(pNode->m_sType=="ASSIGNMENT")
	{
		pas=(CAssignmentUnit *)pNode;
		if(pas && pas->m_pleft && pas->m_pleft->T.key==TD.key &&
		   TD.key==CN_VARIABLE && pas->m_pleft->T.addr==TD.addr)
		{
			nDR=nDR==1?3:2;
		}
		
	}
	/////////////wtt////////3.8////scanf有定值的变量////////
	if(pNode->m_sType=="SDGCALL"&&pNode->m_pinfo&&(pNode->m_pinfo->T.name=="scanf"|| pNode->m_pinfo->T.name=="gets")   )
			
	{
	
		SetFunctionDefinedData(pNode,pNode->m_pinfo,SArry );
		for(int i=0;i<pNode->m_aDefine.GetSize();i++)
		{
			if(TD.key==DArry[i].key && TD.key==CN_VARIABLE &&  TD.addr==DArry[i].addr)
			{
				nDR=1;
				break;
			}
		}

	}
////////////////////////////////////////wtt////////////////////////////////
	return nDR;


}




////////////////////////////////////////////////////////////////////

void RemoveInvocation::SetGen(CSDGBase *pHead)
{//后根序遍历USDG,设置各节点的GEN集合
	if(pHead==NULL)
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{	
		SetGen(pHead->GetNode(i));
	}

	if(pHead->m_sType!="#DECLARE")

	{
		//AfxMessageBox(pHead->m_sType);

	if(pHead->m_aDefine.GetSize()==0)
		return;
	for(int j=0;j<pHead->m_aDefine.GetSize();j++)
	{
	//	AfxMessageBox("setGEn!");
		FlowNODE fn;
		fn.SDGID=pHead->idno;
		fn.varaddr=pHead->m_aDefine[j].addr;
		pHead->m_aGen.Add(fn);  
	}
	}
}
void RemoveInvocation::SetKill(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{//后根序遍历USDG，设置各个节点的KILL集合
	if(pHead==NULL)
		return;

	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		SetKill(pHead->GetNode(i),SArry);
	}
	if(pHead->m_sType!="ASSIGNMENT"&&!(pHead->m_sType=="SDGCALL"&&pHead->m_pinfo&&(pHead->m_pinfo->T.name=="scanf"||pHead->m_pinfo->T.name=="gets") ))
//	if(pHead->m_sType!="ASSIGNMENT")
		return;
	/////////////////////////3.26////////////////
	if(pHead->m_sType=="ASSIGNMENT")
	{//数组元素的kill集合为空,selector子节点不kill
		CAssignmentUnit*pas=(CAssignmentUnit*)pHead;
		if(pas->m_pleft&&pas->m_pleft->T.key==CN_VARIABLE&&pas->m_pleft->T.addr>=0&&pas->m_pleft->T.addr<SArry.GetSize()&& SArry[pas->m_pleft->T.addr].kind==CN_ARRAY)
		{
			return;
		}
		////wtt 2008.4.1/////////////////////////
		if(pas->m_pleft&&pas->m_pinfo&&pas->m_pleft->T.addr==pas->m_pinfo->T.addr&&pas->m_pinfo->info<0&&pas->m_pleft->T.key==CN_VARIABLE&&pas->m_pleft->T.addr>=0&&pas->m_pleft->T.addr<SArry.GetSize()&& SArry[pas->m_pleft->T.addr].kind==CN_POINTER)
		{
		//	AfxMessageBox("指针自赋值不注销");///
			return;
		}
		if(pas->m_pleft&&pas->m_pinfo&&pas->m_pleft->T.key==CN_VARIABLE&&pas->m_pleft->T.addr>=0&&pas->m_pleft->T.addr<SArry.GetSize()&& SArry[pas->m_pleft->T.addr].kind==CN_POINTER)
		{
			for(int i=0;i<pHead->ptSet.GetSize();i++)
			{
				if(pHead->ptSet[i]->psrc&&pHead->ptSet[i]->ptar&&pHead->ptSet[i]->psrc->T.addr==pas->m_pleft->T.addr&&pHead->ptSet[i]->psrc->T.addr==pHead->ptSet[i]->ptar->T.addr&&pHead->ptSet[i]->ptar->info<0)
				{
				//	AfxMessageBox("指针与数组算数不注销");
					return;
				}
			}
		}



		///////////////////////////////////////
		CSDGBase*pf=NULL;
		pf=pas->GetFather();
		if(pf&&pf->m_sType=="SELECTOR" )//selector子节点不kill
			return;
	}
	if(pHead->m_sType=="SDGCALL"&&pHead->m_pinfo&&(pHead->m_pinfo->T.name=="scanf"||pHead->m_pinfo->T.name=="gets"))
	{
		if(pHead->m_pinfo->pinfo[1]!=NULL&&pHead->m_pinfo->pinfo[1]->T.addr>=0&&pHead->m_pinfo->pinfo[1]->T.addr<SArry.GetSize()&& SArry[pHead->m_pinfo->pinfo[1]->T.addr].kind==CN_ARRAY    )
			return;
	}
	/////////////////////////////////////////////
	CArray<CSDGBase*,CSDGBase*>prList;
	prList.RemoveAll();
	FindLeftSDGNode(pHead,prList);
	CString stemp=pHead->m_sType+":";////////////////
	for(int i=0;i<prList.GetSize();i++)
	{
	
		if(prList[i])
			stemp+=prList[i]->m_sType+";";//////////////////
		
		if(prList[i]&&prList[i]->m_sType=="ASSIGNMENT")
		{
			DetectKill(prList[i],pHead,SArry);

		}
		else if(prList[i]&&(prList[i]->m_sType=="SELECTION"||prList[i]->m_sType=="ITERATION"))
		{
		//	AfxMessageBox("prList[i]->m_sType==SELECTION||prList[i]->m_sType==ITERATION");/////////////////////
			TraverseDetectKill(prList[i],pHead,SArry);
		}
		else if(prList[i]&&prList[i]->m_sType=="SDGCALL"&&prList[i]->m_pinfo&&(prList[i]->m_pinfo->T.name=="scanf"||pHead->m_pinfo->T.name=="gets") )
		{
			DetectKill(prList[i],pHead,SArry);

		}

	}	
//	AfxMessageBox(stemp);/////////////

}

void RemoveInvocation::FindLeftSDGNode(CSDGBase*p,CArray<CSDGBase*,CSDGBase*>&prList)
{//查找p在usdg中左侧可能存在数据依赖关系的节点保存在prList中
	CSDGBase*pf=NULL;
	pf=p->GetFather();
	if(pf==NULL)
		return;
	if(pf->m_sType=="SDGENTRY" )
	{
		int i=FindIndex(p)-1;
		while(i>=0&&pf->GetNode(i)&&pf->GetNode(i)->m_sType!="#DECLARE")
		{
			prList.Add(pf->GetNode(i));
			i--;
		}

	}
	else if(pf->m_sType=="SELECTOR")
	{
		int i=FindIndex(p)-1;
		while(i>=0)
		{
			prList.Add(pf->GetNode(i));
			i--;
		}
		pf=pf->GetFather();
		FindLeftSDGNode(pf,prList);		

	}
	else if(pf->m_sType=="ITERATION")
	{
	//	AfxMessageBox("pf->m_sType==ITERATION");
		CSDGBase*p1=p;
		while(pf&&pf->m_sType=="ITERATION")
		{
			int index=FindIndex(p1);
			int i=index-1;
			while(i>=0)
			{
				prList.Add(pf->GetNode(i));
				i--;
			}
			p1=pf;
			pf=pf->GetFather();
		}
		
		int index=FindIndex(p1);
		int i=index-1;
		while(i>=0)
		{
			prList.Add(pf->GetNode(i));
			i--;
		}

		FindLeftSDGNode(pf,prList);
	}
	
}
void RemoveInvocation::DetectKill(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//检测p与pHead的Gen集合是否有相同的变量，若有将其变量和p的编号的二元组加入pHead的kill集合中

	if(p==NULL||pHead==NULL)
		return;

	/////////////////////3.30/////do 循环体内语句不kill任何其它循环外的语句////
	CSDGBase*pf=NULL,*pf1=NULL;
	pf=pHead->GetFather();
	pf1=p->GetFather(); 

	if(pf!=NULL&&pf1!=NULL&&pf->m_sType=="ITERATION")
	{
		CIterationUnit*pi=(CIterationUnit*)pf;
		if(pi->m_sAsi=="SDGDOWHILE" )
		{
	
			if(p->m_sType=="ASSIGNMENT"&&pHead->m_sType=="ASSIGNMENT"&& pf->idno !=pf1->idno)
			{
							
	
				CAssignmentUnit*pas1=(CAssignmentUnit*)p;
				CAssignmentUnit*pas2=(CAssignmentUnit*)pHead;

				if(CompareTrees(pas1->m_pleft,pas2->m_pleft,SArry))
				{
				//AfxMessageBox("do while do not kill");/////////////
				return;
					
				}

			}
		}
	}
		
	////////////////////////////////////////////

	for(int i=0;i<pHead->m_aGen.GetSize();i++)
	{
		for(int j=0;j<p->m_aGen.GetSize();j++)
		{
			if(p->m_aGen[j].varaddr== pHead->m_aGen[i].varaddr	)
			{
				////////////wtt 2008.4.1///////////指针脱引用级别不同不注销/////////////
					if(p->m_sType=="ASSIGNMENT"&&pHead->m_sType=="ASSIGNMENT")
					{
							
	
						CAssignmentUnit*pas1=(CAssignmentUnit*)p;
						CAssignmentUnit*pas2=(CAssignmentUnit*)pHead;
						if(pas1&&pas2&&pas1->m_pleft&&pas2->m_pleft&&pas1->m_pleft->T.deref==pas2->m_pleft->T.deref)
						{
							pHead->m_aKill.Add(p->m_aGen[j]); 

						}
					//	else 
					//	{
						//	AfxMessageBox("deref!=");
					//	}
					}
					////////////////////////////////////////////
					else
					{


						pHead->m_aKill.Add(p->m_aGen[j]);  
					}

			}
		}

	}
	//	AfxMessageBox("ok");/////////////

}
void RemoveInvocation::TraverseDetectKill(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//遍历分支p检测是否有pHead的GEN集合kill的二元组

	if(p==NULL||pHead==NULL)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		
		TraverseDetectKill(p->GetNode(i),pHead,SArry);

	}
	//if(pHead->m_sType!="ASSIGNMENT"&&!(pHead->m_sType="SDGCALL"&&pHead->m_pinfo&&pHead->m_pinfo->T.name=="scanf") )
	if(p->m_sType!="ASSIGNMENT"&&!(p->m_sType=="SDGCALL"&&p->m_pinfo&&p->m_pinfo->T.name=="scanf" ))
		return;
	DetectKill(p,pHead,SArry);

}
void RemoveInvocation::IterateSetInAndOut(CSDGBase *pHead)
{//采用迭代算法设置各个节点的IN和OUT集合 ，直到各节点的IN和OUT集合不发生变化 
	change=true;
	while(change)
	{
//		AfxMessageBox("SET in and out");////////////////
		change=false;
		SetInAndOut(pHead);
	}

}
void RemoveInvocation::SetInAndOut(CSDGBase *pHead)
{//设置各个节点的IN和OUT集合 
	if(pHead==NULL)
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		SetInAndOut(pHead->GetNode(i));
			
	}
	//设置IN集合
	CSDGBase*prior=NULL;
	prior=GetPrior(pHead);
	////////////////////////////////////////5.12//test//
/*	if(pHead->m_sType=="SELECTOR")
	{
		AfxMessageBox("SElector");
		if(prior!=NULL)
			AfxMessageBox("SElector priror!=NULL"+prior->m_sType);
		else
           AfxMessageBox("SElector priror==NULL");

	}*/
		/////////////////////////////////////////
	if(prior!=NULL)
	{
	//	AfxMessageBox("prior!=NULL");//////////////////////

		for(int j=0;j<prior->m_aOut.GetSize();j++)
		{
		
			if(!IsFlowNODEInArry(prior->m_aOut[j],pHead->m_aIn))
				pHead->m_aIn.Add(prior->m_aOut[j]);  
		}
	}	
	
	//设置OUT集合
	SetOut(pHead);

}
CSDGBase* RemoveInvocation::GetPrior(CSDGBase *p)
{//获取节点p在CDS中的前驱节点，若p是selector节点则其前驱节点是它的父节点selection的左兄弟，其余类型的节点的前驱节点为其左兄弟
	
	//////////////////////////////////////
	if(p==NULL)
		return NULL;
	CSDGBase* pf=NULL;
	
	if(p->m_sType=="ITERATION"||p->m_sType=="SELECTION"||p->m_sType=="ASSIGNMENT"||p->m_sType=="SDGCALL"||p->m_sType=="SDGRETURN"||p->m_sType=="SDGBREAK"||p->m_sType=="SDGCONTINUE")
	{
		pf=p->GetFather();
		if(pf==NULL)
			return NULL;

	}
	if(p->m_sType=="SELECTOR")
	{
		pf=p->GetFather();
		if(pf==NULL)
			return NULL;
		pf=pf->GetFather();
		if(pf==NULL)
			return NULL;
	//	AfxMessageBox(pf->m_sType);
		

	}
	if(p->m_sType=="SELECTOR"||p->m_sType=="ITERATION"||p->m_sType=="SELECTION"||p->m_sType=="ASSIGNMENT"||p->m_sType=="SDGCALL"||p->m_sType=="SDGRETURN"||p->m_sType=="SDGBREAK"||p->m_sType=="SDGCONTINUE")
	{
		bool flag=true;
		int index;
		CSDGBase*p1=p;
		if(p->m_sType=="SELECTOR")///5.12//
			p1=p->GetFather(); ///5.12
		while(flag&&pf)
		{
			index=FindIndex(p1);
		
			if(index-1>=0)
				flag=false;
			else
			{
				
				p1=pf;
				pf=pf->GetFather(); 

			}
		
		}
		if(flag==false)
			return pf->GetNode(index-1) ;
		else return NULL;
	}
	else
		return NULL;

	/////////////////////////////////

}


void RemoveInvocation::SetOut(CSDGBase *p)
{//设置一个节点p的OUT集合
	if(p==NULL)
		return;
//	AfxMessageBox("SetOut is called");//////////////////////////
	if(p->m_sType=="ITERATION"||p->m_sType=="SELECTOR")
	{//"ITERATION"和"SELECTOR"的OUT集合=最右孩子的OUT集合
		CSDGBase*pr=GetRightChild(p);
		if(pr==NULL)
			return;
		for(int j=0;j<pr->m_aOut.GetSize();j++)
		{
			//if(原集合中没有该元素)
			if(!IsFlowNODEInArry(pr->m_aOut[j],p->m_aOut))
			p->m_aOut.Add(pr->m_aOut[j]);  
		}

	}
	else if(p->m_sType=="SELECTION")
	{//"SELECTION"的OUT集合=所有SELECTOR的OUT的并集
		for(int i=0;i<p->GetRelateNum();i++)
		{
			CSDGBase*pc=p->GetNode(i);
			if(pc)
			{
				for(int j=0;j<pc->m_aOut.GetSize();j++)
				{
					//if(原集合中没有该元素)
					if(!IsFlowNODEInArry(pc->m_aOut[j],p->m_aOut))
					p->m_aOut.Add(pc->m_aOut[j]);  
				}
			}
		}
	}
	else if(p->m_sType=="ASSIGNMENT"||(p->m_sType=="SDGCALL"&&p->m_pinfo&&p->m_pinfo->T.name=="scanf") )
	{//赋值语句与scanf语句的OUT集合=GEN[n]+(IN[n]-KILL[n])
	//	AfxMessageBox("Set assignment");////////////////////////////////
		CArray<FlowNODE,FlowNODE> OLDOUT;
		OLDOUT.RemoveAll();
		for(int i=0;i<p->m_aOut.GetSize();i++)
		{
			OLDOUT.Add(p->m_aOut[i]);  
		}
		p->m_aOut.RemoveAll();
		for(int j=0;j<p->m_aGen.GetSize();j++)
		{
			p->m_aOut.Add(p->m_aGen[j]);

		}
		for(int j=0;j<p->m_aIn.GetSize();j++)
		{
			if(!IsFlowNODEInArry(p->m_aIn[j],p->m_aKill)&&!IsFlowNODEInArry(p->m_aIn[j],p->m_aGen))
					p->m_aOut.Add(p->m_aIn[j]);  
		}
		if(OLDOUT.GetSize()!=p->m_aOut.GetSize())
			change=true;
		else
		{
			int j;
			for( j=0;j<p->m_aOut.GetSize();j++)
			{
				if(!IsFlowNODEInArry(p->m_aOut[j],OLDOUT))
					change=true;  
			}
		}
	
	}
	else
	{//其他类型节点IN与OUT集合相等
		for(int  j=0;j<p->m_aIn.GetSize();j++)
		{
			if(!IsFlowNODEInArry(p->m_aIn[j],p->m_aOut))
				p->m_aOut.Add(p->m_aIn[j]); 
		}

	}


}





CSDGBase* RemoveInvocation::GetRightChild(CSDGBase *p)
{//获得p的最右孩子
	if(p==NULL)
		return NULL;
	int i=p->GetRelateNum()-1;
	return p->GetNode(i);
}
bool RemoveInvocation::IsFlowNODEInArry(FlowNODE TF,CArray<FlowNODE,FlowNODE> &DArry)
{//判定节点TF是否在数组DArry中
	/******************************************************

	    FUNCTION:
		If the Node TF is in array DArry?	in- return true;
	******************************************************/

	for(int i=0;i<DArry.GetSize();i++)
	{
		if(TF.varaddr==DArry[i].varaddr&&TF.SDGID==DArry[i].SDGID)// && TD.name==DArry[i].name)
		{
			return true;
		}
	}
	return false;
}
void RemoveInvocation::SetDataDependence(CSDGBase *p,CSDGBase*pHead)
{//设置数据依赖信息和反依赖信息
	for(int i=0;i<p->GetRelateNum();i++)
	{
		SetDataDependence(p->GetNode(i),pHead);

	}
	if(p->m_aRefer.GetSize()<=0)
		return;

	for(int i=0;i<p->m_aRefer.GetSize();i++ )
	{
		int addr=p->m_aRefer[i].addr;
		for(int j=0;j<p->m_aIn.GetSize();j++)
		{
		
			if(p->m_aIn[j].varaddr==addr)
			{
					
				CSDGBase*pf=NULL;
				pf=FindSDGNODE(p->m_aIn[j].SDGID,pHead);
				if(pf==NULL||pf->m_sType=="#DECLARE")
					return;
				
				pf->AddDataFlow(p); 
				p->AddAntiDataFlow(pf); 
			}
		}

	}

}
void RemoveInvocation::SetDecDependence(CSDGBase *p,CSDGBase*pHead)
{//设置声明依赖信息
	

	for(int i=0;i<p->GetRelateNum();i++)
	{
		SetDecDependence(p->GetNode(i),pHead);

	}
	if(p->m_aDefine.GetSize()<=0)
		return;
	if(p->m_sType=="#DECLARE")
		return;
/*	if(p->m_sType!= "SDGHEAD"&&p->m_sType!= "SDGENTRY")
	{
	 CSDGBase*ptemp=p;
	 while(ptemp&&ptemp->m_sType!="SDGENTRY" )
	 {
		ptemp=ptemp->GetFather(); 
	 }
	if(ptemp==NULL)
		return;
	}*/
////////////wtt 2008.4.4////////只要有引用该变量的则不删除该变量///////////////
/*	for(i=0;i<p->m_aDefine.GetSize();i++ )
	{
		int addr=p->m_aDefine[i].addr;
		CSDGBase*pf=NULL;
		pf=FindDecNODE(addr,pHead);
		if(pf==NULL )
			return;
		pf->AddDecFlow(p); 
		p->AddDecFlow(pf); 
	

	}*/
	int i;
	for(i=0;i<p->m_aRefer.GetSize();i++ )
	{
		int addr=p->m_aRefer[i].addr;
		CSDGBase*pf=NULL;
		pf=FindDecNODE(addr,pHead);
		if(pf==NULL )
			return;
		pf->AddDecFlow(p); 
		p->AddDecFlow(pf); 
	

	}
	/////////////////////////////////////////////////

}
CSDGBase* RemoveInvocation::FindSDGNODE(int SDGID,CSDGBase*pHead)
{//查找编号为SDGID的节点
	if(pHead==NULL)
		return NULL;
	if(pHead->idno==SDGID)
		return pHead;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		CSDGBase*p=FindSDGNODE(SDGID,pHead->GetNode(i));
		if(p!=NULL)
		return p;
	}

	return NULL;
}
CSDGBase* RemoveInvocation::FindDecNODE(int addr,CSDGBase*pHead)
{//查找声明变量addr(变量在符号表中的地址)的变量声明节点
	if(pHead==NULL)
		return NULL;
	if(pHead->m_sType=="#DECLARE"&&pHead->m_aDefine.GetSize()>0&&pHead->m_aDefine[0].addr==addr)
		return pHead;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		CSDGBase*p=FindDecNODE(addr,pHead->GetNode(i));
		if(p!=NULL)
		return p;
	}

	return NULL;
}
////////////////以上为设置数据流信息//建立数据流依赖边、反依赖边、声明依赖边////



///////////////////以下为///////删除未被引用的赋值语句和变量声明节点//恒等式和简单语句////

 void RemoveInvocation:: DeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)////3.17
 {//删除未被引用的赋值语句和变量声明节点
	deleteunrefered=true;//wtt 2008.2.28
	while(deleteunrefered)//wtt 2008.2.28
	{
	    deleteunrefered=false;//wtt 2008.2.28
    	SetDataFlow(pUSDGHead,SArry,FArry);
		DeleteIdentity(pUSDGHead,SArry);/////////wtt////3.12////
	
    	SetDataFlow(pUSDGHead,SArry,FArry);

		RemoveSimpleStatement(pUSDGHead,SArry,FArry);////////////wtt///3.12//
		SetDataFlow(pUSDGHead,SArry,FArry);

		RemoveNotReferedAssignment(pUSDGHead,pUSDGHead,SArry,FArry);///wtt////3.9////
		RemoveEmptyBranchEx(pUSDGHead);
		SetDataFlow(pUSDGHead,SArry,FArry);

    	MarkFormParameter(pUSDGHead,SArry,FArry);//标识pUSDGHead的形参和普通变量
		RemoveNotReferedVar(pUSDGHead); 
		SetDataFlow(pUSDGHead,SArry,FArry);
	}

 }


void RemoveInvocation::RemoveNotReferedVar(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG and delete the Empty
		selector node of else node
			    
		PARAMETER:
		pHead : Head pointer of USDG
		
	******************************************************/
	if(pHead==NULL||pHead->m_sType!="SDGHEAD" )
		return;

	int i=pHead->GetRelateNum()-1;
	bool bused=false;
	CSDGBase *p=NULL;


	// remove global not used variables; 
	while(i>=0)
	{
		p=pHead->GetNode(i);

		if(p&&p->m_sType=="#DECLARE")
		{
		//	j=i+1;
			bused=false;
			if(p->m_pDecRNodes.GetSize()>0)
				bused=true;

			if(!bused)
			{
		
				if(p)
				delete p;
				p=NULL;
				pHead->Del(i);
		

			}
		}
		i--;
	}

	// remove local not used variables
    int k=0;
	while(k<pHead->GetRelateNum())
	{
		CSDGBase* phead1=pHead->GetNode(k);
		if(phead1 && phead1->m_sType=="SDGENTRY")
		{
			i=phead1->GetRelateNum()-1; 
			while(i>=0)
			{
				p=phead1->GetNode(i);
                
				if(p&&p->m_sType=="#DECLARE")
				{
				//	j=i+1;
					bused=false;
					if(p->m_pDecRNodes.GetSize()>0)
						bused=true;
					///////////////////////////
				/*	CString str;
					str.Format(":%d",p->m_pDecRNodes.GetSize()) ;
					AfxMessageBox(p->TCnt[0].name+str);/////*/
					////////////////////////////////
					///////////////////////////



					//////////////////////////////
					//CSDGDeclare*p1=(CSDGDeclare*)p;////wtt//3.24///
					if(!bused&&p->no==-1)/////wtt//3.24////
				//	if(!bused)
					{
					//	AfxMessageBox("delete"+p->TCnt[0].name);////////////
						if(p)
						delete p;
						p=NULL;
						phead1->Del(i);						
					}
				}
				i--;
			}
		} // if(phead && ph
		k++;
	} // while(k<pHead->

}


//////////////////////wtt/////////////3.9///////////////////////

void RemoveInvocation::RemoveNotReferedAssignment(CSDGBase *p,CSDGBase *pHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE> &FArry)
{
	/******************************************************

	    FUNCTION:
	     wtt编写删除没有被引用的赋值语句表达式
			    
		PARAMETER:
		pHead : Head pointer of USDG

		
	******************************************************/
	if(p==NULL)
		return;
  
	int m=p->GetRelateNum();
	for(int n=0;n<m;n++)
	{
	for(int i=0;i<p->GetRelateNum();i++)
	{
		RemoveNotReferedAssignment(p->GetNode(i),pHead,SArry,FArry);
	}
	}
	if(p->m_sType!="ASSIGNMENT")
		return;
	CAssignmentUnit*pas=(CAssignmentUnit*)p;

//	AfxMessageBox("deal"+GetExpString(pas->m_pleft,SArry)+"="+GetExpString(pas->m_pinfo,SArry));

	if(p->m_pDRNodes.GetSize()>0 )//有数据依赖边
	{
//		AfxMessageBox("p->m_pDRNodes.GetSize()>0 ");
		return;
	}
///////wtt//////05.10.24//////////////
	int	index=FindIndex(p);
    bool inloop=false;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
	{
	//	AfxMessageBox("pf==NULL ");

		return;
	}
	CSDGBase*pff=p;
	do
	{
		pff=pff->GetFather();
		if(pff&&pff->m_sType=="ITERATION" )
			inloop=true;

	}while(pff!=NULL&&!inloop);
	

	if(inloop )//循环内语句暂不处理
	{
		//		AfxMessageBox("inloop ");

	 return;
	}
	///////wtt//////05.10.24//////////////
	if(pas->m_pleft&&pas->m_pleft->T.addr>=0&&pas->m_pleft->T.addr<SArry.GetSize()&&SArry[pas->m_pleft->T.addr].layer==0)//全局变量暂时不做处理
	{	
			//	AfxMessageBox("SArry[pas->m_pleft->T.addr].layer==0");

		return;
	}
	/////////////////3.28/////数组名作为形参不删除其赋值语句//
	if(pas->m_pleft&&pas->m_pleft->T.addr>=0&&pas->m_pleft->T.addr<SArry.GetSize()&&SArry[pas->m_pleft->T.addr].kind==CN_ARRAY)//数组变量暂时不做处理
	{
	//	return;
		//if(该数组名是形参)
	//	AfxMessageBox("DEtect array to delete");

		CSDGBase*p1=p;
		while(p1&&p1->m_sType!="SDGENTRY")
		{
			p1=p1->GetFather(); 
		}
		if(p1==NULL)
		return;
		if(p1->TCnt.GetSize() <0 )
			return;
		int addr=p1->TCnt[0].addr;
		//////////////////////////////////
	/*	CString s;
		s.Format("entry addr=%d",addr);
		AfxMessageBox(s);*/
	///////////////////
		if(addr<0||addr>=FArry.GetSize())
		{
			//				AfxMessageBox("addr<0||addr>=FArry.GetSize()");
			return;
		}
		bool flag=false;
		for(int k=0;k<20;k++)
		{
	/*	CString s;
			s.Format("parameter=%d",FArry[addr].plist[k]);
			AfxMessageBox(s);
			s.Format("SArry[pas->m_pleft->T.addr].initaddr=%d",SArry[pas->m_pleft->T.addr].initaddr);
			AfxMessageBox(s);*/
		if(FArry[addr].plist[k]==SArry[pas->m_pleft->T.addr].initaddr)
			{
				flag=true;
				break;
			
			}

		}
		if(flag)
		{
			//	AfxMessageBox("flag");
			return;
		}
		//	return;
		
		
	}

////////////////////////////////////////////////////////
//	AfxMessageBox("delete"+GetExpString(pas->m_pleft,SArry)+"="+GetExpString(pas->m_pinfo,SArry));

//	DeleteExpTree(pas->m_pleft);
//	DeleteExpTree(pas->m_pinfo);
	pas->m_pleft=pas->m_pinfo=NULL;
//	if(pas)
//	delete pas;
	pas=NULL;
	p=NULL;
	pf->Del(index); 
	deleteunrefered=true;//wtt 2008.2.28
//	AfxMessageBox("remove");

}

//////////////////////////////////////wtt//////////////////////////////

extern bool CompareTrees(ENODE*pHead1,ENODE*pHead2,CArray<SNODE,SNODE> &SArry);
extern void DeleteBranch(CSDGBase *pHead); 
void RemoveInvocation::DeleteIdentity(CSDGBase *p,CArray<SNODE,SNODE>&SArry)
{//wtt编写//3.12//删除恒等的赋值语句例如：n=n;
	if(p==NULL)
		return;
	CSDGBase *pf=NULL;
	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		DeleteIdentity(p->GetNode(i),SArry);
	}
	if(p->m_sType!="ASSIGNMENT")
		return;
//	if(!CompareTrees(pas->m_pleft,pas->m_pinfo,SArry))
	if(GetExpString(pas->m_pleft,SArry)!=GetExpString(pas->m_pinfo,SArry))//wtt 2008.4.2考虑指针脱引用
		return;
	pf=p->GetFather();
	int index=FindIndex(p);
	if(pf==NULL)
		return;
/*	if(p->m_pDecRNodes.GetSize()>0)
	{//删除声明依赖关系
		CSDGBase*pd=p->m_pDecRNodes[0];
		if(pd)
		{
			for(int i=0;i<pd->m_pDecRNodes.GetSize();i++)
			{
				if(pd->m_pDecRNodes[i]&&pd->m_pDecRNodes[i]->idno==p->idno)
				{
					pd->m_pDecRNodes.RemoveAt(i);
					p->m_pDecRNodes.RemoveAt(0); 
					break;
				
				}
			}
		}
	}*/





	pf->Del(index);
	DeleteBranch(p);
	p=NULL;
	deleteunrefered=true;//wtt 2008.2.28
	
}
void RemoveInvocation::DeleteExpTree(ENODE *ptr)
{
	if(ptr==NULL)
		return;
	
	if(ptr->pleft)
	DeleteExpTree(ptr->pleft);
	ptr->pleft=NULL;
	if(ptr->pright)
	DeleteExpTree(ptr->pright);
	ptr->pright=NULL;
	for(int i=0;i<10;i++)
	{
		if(ptr->pinfo[i])
		{
			DeleteExpTree(ptr->pinfo[i]);
			ptr->pinfo[i]=NULL;
		}
	}
	if(ptr)
	delete ptr;
	ptr=NULL;

}



void RemoveInvocation::RemoveSimpleStatement(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE> &FArry)
{//wtt编写：//3.12 ////消除简单语句例如： c = 1; d = c; e = d + 2;--->c = 1；e = c + 2；
	///////////或z = 1； y = a + z；-->y := a + 1

	modified=true;
	while(modified)
	{
		modified=false;
		RemoveVarStatement(p,SArry);
		SetDataFlow(p,SArry,FArry);
	
	
	//	RemoveConstantStatement(p,SArry);//wtt 2007.5.20
	}	


}

void RemoveInvocation::RemoveVarStatement(CSDGBase*p,CArray<SNODE,SNODE>&SArry)
{//wtt编写：//3.12 ////消除简单语句例如： c = 1; d = c; e = d + 2;--->c = 1；e = c + 2；
	if(p==NULL)
	{
		return ;
	}

	for(int j=0;j<p->GetRelateNum();j++)
	{
		
		RemoveVarStatement(p->GetNode(j),SArry);
		
	}
	
	if(p->m_sType!="ASSIGNMENT")
		return;

	if(p->m_pDRNodes.GetSize()!=1 )
	{	return;
	}
	if(p->m_pDRNodes[0]==NULL||p->m_pDRNodes[0]->m_sType!="ASSIGNMENT")
		return;
	
	if(p->m_pAntiDRNodes.GetSize()!=1)
		return;
	if(p->m_pAntiDRNodes[0]==NULL||p->m_pAntiDRNodes[0]->m_sType!="ASSIGNMENT")
		return;
	if(p->m_pinfo&&p->m_pinfo->info<0)//wtt2008.4.2指针运算不替换
		return;

	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	//		    AfxMessageBox(GetENodeStr(pas->m_pleft,SArry)+"="+GetENodeStr(pas->m_pinfo,SArry));

	if(IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=1)
	{
	//	AfxMessageBox("IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=1");
		return ;
	}
	//////////3.28//////////数组不处理////////////
	if(pas->m_pleft->T.addr>0&&pas->m_pleft->T.addr<SArry.GetSize()&& SArry[pas->m_pleft->T.addr].kind==CN_ARRAY)
		return;
	if(pas->m_pinfo->T.addr>=0&&pas->m_pinfo->T.addr<SArry.GetSize()&& SArry[pas->m_pinfo->T.addr].kind==CN_ARRAY)
		return;
///////////////////////////////////////////
//	AfxMessageBox("ok");///////////////////

	if(p->m_pDRNodes[0]->m_aDefine.GetSize()>0&& p->m_pDRNodes[0]->m_aDefine[0].addr==pas->m_pleft->T.addr)
		return;
	


		
	CSDGBase*pf=p->GetFather();
	if(!pf)
		return ;
/////wtt///////5.19/////循环中语句不替换/////
  if(p->m_pDRNodes[0]->m_aDefine.GetSize()>0&& p->m_pDRNodes[0]->GetFather()&&p->m_pDRNodes[0]->GetFather()->m_sType=="ITERATION"  )
  {

		return;////
  }	
  ////////////////////////////////

////////////////////wtt 2008.3.28/////////temp=a;a=b;b=temp; 不处理 ///////////////////
  		//	CAssignmentUnit *pAs=(CAssignmentUnit *)p->m_pAntiDRNodes[0];
		//	    AfxMessageBox(GetENodeStr(pAs->m_pleft,SArry)+"="+GetENodeStr(pAs->m_pinfo,SArry));

  CSDGBase*pf1=p->m_pAntiDRNodes[0]->GetFather();
  if(pf==pf1)
 {
	 // AfxMessageBox("ok");
    int i=FindIndex(p);
	int j=FindIndex(p->m_pDRNodes[0]);
	//CString st;
	//st.Format("%d,%d",i,j);
	//AfxMessageBox(st);
   for(int k=i+1;k<j;k++)
	{
		CSDGBase*psib=pf->GetNode(k);
		if(psib->m_sType=="ASSIGNMENT")
		{ 
			//AfxMessageBox("assignment");

			CAssignmentUnit *pAsign=(CAssignmentUnit *)psib;
			CAssignmentUnit *pAsignd=(CAssignmentUnit *)p->m_pDRNodes[0];


		   // AfxMessageBox(GetENodeStr(pAsign->m_pleft,SArry)+"="+GetENodeStr(pAsign->m_pinfo,SArry));
			//  AfxMessageBox(GetENodeStr(pAsignd->m_pleft,SArry)+"="+GetENodeStr(pAsignd->m_pinfo,SArry));


			if(GetENodeStr(pAsign->m_pinfo,SArry)==GetENodeStr(pAsignd->m_pleft ,SArry)&&GetENodeStr(pAsign->m_pleft,SArry)==GetENodeStr(pas->m_pinfo,SArry ))
			{
				//AfxMessageBox("not transformation");
				return;
			}
		}
	}
  }

  ///////////////////////////////////////


 //  AfxMessageBox(GetExpString(pas->m_pleft,SArry)+" "+GetExpString(p->m_pinfo,SArry)+" "+GetExpString(p->m_pDRNodes[0]->m_pinfo,SArry));
    if(p->m_pinfo&&p->m_pDRNodes[0]->m_pinfo&&p->m_pinfo->T.deref!=p->m_pDRNodes[0]->m_pinfo->T.deref)//指针脱引用级别不同不替换
		return;
	ReplaceVar(p->m_pinfo,pas->m_pleft,p->m_pDRNodes[0]->m_pinfo,SArry);

	AExpStandard(p->m_pDRNodes[0]->m_pinfo,SArry);



	int index=FindIndex(p);
	pf->Del(index);///////////
	
//	DeleteBranch(p);//////////// 有问题//

	p=NULL;

	modified=true;
	deleteunrefered=true;//wtt 2008.2.28


 }
void RemoveInvocation::RemoveConstantStatement(CSDGBase*p,CArray<SNODE,SNODE>&SArry)
{	////////wtt编写：//3.12 ////消除简单语句例如:z = 1； y = a + z；-->y := a + 1

	if(p==NULL)
	{
		return ;
	}
	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveConstantStatement(p->GetNode(j),SArry);
		
	}
	
	if(p->m_sType!="ASSIGNMENT")
		return ;
	
	if(p->m_pDRNodes.GetSize()!=1 )
	{	return;
	}
	if(p->m_pDRNodes[0]==NULL||p->m_pDRNodes[0]->m_sType!="ASSIGNMENT")
		return;
	/////wtt///////5.19/////循环中语句不替换/////
  if(p->m_pDRNodes[0]->m_aDefine.GetSize()>0&& p->m_pDRNodes[0]->GetFather()&&p->m_pDRNodes[0]->GetFather()->m_sType=="ITERATION"  )
  {

		return;////
  }	
  ////////////////////////////////
	
	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	if(IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=0)
	{
	//	AfxMessageBox("IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=0");
		return ;
	}
	if(p->m_pDRNodes[0]->m_aDefine.GetSize()>0&& p->m_pDRNodes[0]->m_aDefine[0].addr==pas->m_pleft->T.addr)
		return;
     
	ReplaceVar(p->m_pinfo,pas->m_pleft,p->m_pDRNodes[0]->m_pinfo,SArry);
//	AfxMessageBox("ReplaceVar is done!");/////////////////////
	AExpStandard(p->m_pDRNodes[0]->m_pinfo,SArry);


	
	CSDGBase*pf=p->GetFather();
	if(!pf)
		return ;
	int index=FindIndex(p);
		
	pf->Del(index);///////////

//	DeleteBranch(p);//////// 加上就有问题//////
	
	p=NULL;

//	AfxMessageBox("ok");////////////////////

	modified=true;

	

	
 }
//////////////////////////////
void RemoveInvocation::ReplaceArrVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry)
{//////wtt编写：//3.12 //用p1替换表达式pExp中的p2

	if(pExp==NULL||p1==NULL||p2==NULL)
		return;

	
	if(pExp->T.key==p2->T.key&&pExp->T.addr>=0&&pExp->T.addr<SArry.GetSize()&&p2->T.addr>=0&&p2->T.addr<SArry.GetSize()&&SArry[pExp->T.addr].initaddr==SArry[p2->T.addr].initaddr)

	{
//		AfxMessageBox("find var to replace!");//////////////
		
		
		//	AfxMessageBox("replace variarant!");
		//	AfxMessageBox("replace variarant!"+	pExp->T.name+";"+p1->T.name );/////////////////////
	
		pExp->T.key=p1->T.key;
		pExp->T.addr=p1->T.addr;
		pExp->T.name=p1->T.name ;
		pExp->T.paddr=p1->T.paddr;
		pExp->T.value =p1->T.value;	
		pExp->T.deref=p1->T.deref;//wtt 2008 3.19
	//	pExp->info=p1->info;//wtt  2008.3.24
	
	}

	/////////////3.24//////////////////////////////////
	if(pExp->T.key==CN_BFUNCTION||pExp->T.key==CN_DFUNCTION)
	{
		for(int i=0;i<pExp->info&&i<10;i++)
		{
				ReplaceArrVar(p1,p2,pExp->pinfo[i],SArry );

		}

	}
	////////////3.24////////////////////////////////////
	ReplaceArrVar(p1,p2,pExp->pleft,SArry );
	ReplaceArrVar(p1,p2,pExp->pright ,SArry);

}



////////////////////////////////////////////////////
void RemoveInvocation::ReplaceVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry)
{//////wtt编写：//3.12 //用p1替换表达式pExp中的p2

	if(pExp==NULL||p1==NULL||p2==NULL)
		return;
	if(pExp->T.key==p2->T.key&&pExp->T.name==p2->T.name&&pExp->T.addr==p2->T.addr&&pExp->info==p2->info)
	{
	//	AfxMessageBox("find var to replace!");//////////////
		
		bool flag=true;
		if(p2->info!=0)
		{
			for(int i=0;i<10&&p2->pinfo[i];i++)//数组元素下标必须相同
			{
				if(!CompareTrees(p2->pinfo[i],pExp->pinfo[i],SArry))
					flag=false;
			}

		}
		if(flag)
		{
		//	AfxMessageBox("replace variarant!");
		//	AfxMessageBox("replace variarant!"+	pExp->T.name+";"+p1->T.name );/////////////////////
	
		pExp->T.key=p1->T.key;
		pExp->T.addr=p1->T.addr;
		pExp->T.name=p1->T.name ;
		pExp->T.deref=p1->T.deref;//wtt 2008 3.19
		pExp->T.paddr=p1->T.paddr;
		pExp->T.value =p1->T.value;	
		pExp->info=p1->info;
		}
	}
	if(p1->T.key==CN_VARIABLE)
	{
		 //数组元素下标的拷贝
		for(int i=0;i<10&&p1->pinfo[i];i++)
		{
			pExp->pinfo[i]=p1->pinfo[i];
			
			p1->pinfo[i]=NULL;
		}

	}
	/////////////3.24//////////////////////////////////
	if(pExp->T.key==CN_BFUNCTION||pExp->T.key==CN_DFUNCTION||pExp->T.key==CN_VARIABLE)
	{
		for(int i=0;i<10&&pExp->pinfo[i]&&i<10;i++)
		{
				ReplaceVar(p1,p2,pExp->pinfo[i],SArry );

		}

	}
	////////////3.24////////////////////////////////////
	ReplaceVar(p1,p2,pExp->pleft,SArry );
	ReplaceVar(p1,p2,pExp->pright ,SArry);

}
////////////////////////////////////////////////////

///////////////////以上为///////删除未被引用的赋值语句和变量声明节点//恒等式和简单语句////

///////////////////////// 以下为////变量重命名//////////////////////////////////////////////////

void RemoveInvocation::VariablesRename(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************
		FUNCTION:
        Rename Variables;
		PARAMETER:
		pHead : head pointer of USDG
		SArry : ...
	***********************************************/

	FREQ FNode;
	CArray<FREQ,FREQ> vFreq;
 
	for(int i=0;i<SArry.GetSize();i++)
	{
		if(SArry[i].kind!=CN_DFUNCTION && SArry[i].kind!=CN_BFUNCTION)
		{
			FNode.addr=i;
			FNode.oldname=SArry[i].name;
			FNode.type=0;
			FNode.vcount=0;
			FNode.vfrequency=0;
			FNode.newname=_T("");
			vFreq.Add(FNode);
		}
	}
  
	GetFrequency(pHead,SArry,vFreq);
	/////////////////////////////5.15///test//
/*	CString stemp="";
	int j=0;
	while(j<vFreq.GetSize())
	{
		CString str;
		str.Format(":FNode.vcount=%d,FNode.vfrequency=%d",vFreq[j].vcount,vFreq[j].vfrequency);
		stemp+=vFreq[j].oldname+str+"\r\n";
		//AfxMessageBox(vFreq[j].oldname+str);
		j++;
	}
	AfxMessageBox(stemp);*/
	//////////////////////////////////////
	AssignNewName(SArry,vFreq);
	ResetVarName(pHead,SArry,vFreq);
	vFreq.RemoveAll();
}


void RemoveInvocation::GetFrequency(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		search USDG and get Statistic result;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/
   
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
    p=pHead;
   

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p->visit==p->GetRelateNum())
			{
				// Add action here
                
			/*	if(p->m_sType=="ASSIGNMENT" || p->m_sType=="ITERATION" ||
				   p->m_sType=="SELECTOR"   || p->m_sType=="_ELSE"     ||
				   p->m_sType=="SDGRETURN"  || p->m_sType=="SDGCALL" )*/
				if(p->m_sType=="ASSIGNMENT" || p->m_sType=="ITERATION" ||
				   p->m_sType=="SELECTOR"   ||
				   p->m_sType=="SDGRETURN"  || p->m_sType=="SDGCALL" )
				{
					SetFrequencyInfo(p,SArry,vFreq);					
				}
				
				// end of this part
				S.RemoveTail();
			}
					   			
			if(p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

}
void RemoveInvocation::CopyTNODE(TNODE &T,TNODE &TINT)
{
	T.addr=TINT.addr;
	T.key=TINT.key;
	T.line=TINT.line;
	T.name=TINT.name;
	T.value=TINT.value; 
	T.deref=TINT.deref; 
	
	
}
void RemoveInvocation::SetFrequencyInfo(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		Analyze the node and add variable Statistic 
		information to vFreq;
			    
		PARAMETER:
		pNode : pointer of USDG
		vFreq : ...
		
	******************************************************/
//	extern CString PrintExp(CArray<TNODE,TNODE>&T);///
	int itype=0;
	if(pNode==NULL)
		return;
	
	if(pNode->m_sType=="ASSIGNMENT" )
	{
		CAssignmentUnit *pas=(CAssignmentUnit *)pNode;
		FrequencyOfExp(pas->m_pinfo,SArry,vFreq);
		FrequencyOfExp(pas->m_pleft,SArry,vFreq);
		
		if(isIncOrDec(pNode,SArry))
		{
			TNODE T;
			CopyTNODE(T,pas->m_pinfo->pleft->T);
         
			//AfxMessageBox("ok");////////
            CSDGBase *ptemp=pNode;
			while(ptemp && ptemp->m_sType!="SDGENTRY" && ptemp->m_sType!="SDGHEAD" )
			{
				if(ptemp->m_sType=="ITERATION")
				{
				//	AfxMessageBox(PrintExp(ptemp->m_aRefer));/////
    			//	AfxMessageBox(PrintExp(ptemp->m_aDefine  ));/////

		
				//	if(DefOrRefVariableEx(T,ptemp)>0)

					if(DefOrRefVariable(T,ptemp)>0)//wtt///
					{
						//	AfxMessageBox("ok");////////
						itype=2; 				
						break;
					}
				}
				ptemp=ptemp->GetFather(); 

			}

			if(itype>0)
			{
				for(int i=0;i<vFreq.GetSize();i++)
				{
					if(vFreq[i].addr==T.addr)
					{
						vFreq[i].type+=itype;
					}
				}
			}
			
		}
	}
	else 
	{
		FrequencyOfExp(pNode->m_pinfo,SArry,vFreq);

	}		
    
}
bool RemoveInvocation::isIncOrDec(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		the assignment node is one of the kinds i=i+1 ,i=i-1; 
			    
		PARAMETER:
		pNode   : ...
		
	******************************************************/

	if(pNode==NULL)
		return false;
	if(pNode->m_sType!="ASSIGNMENT")
	{
		return false;
	}

	CAssignmentUnit *pas=(CAssignmentUnit *)pNode;

	int addr=-1;
	int itype=-1;

	if(pas->m_pleft && pas->m_pleft->T.key==CN_VARIABLE)
	{
		addr=pas->m_pleft->T.addr;
		if( addr>=0&&addr<SArry.GetSize()&&SArry[addr].kind==CN_VARIABLE &&
			SArry[addr].type==CN_INT)
		{
			itype=1;
		}

		if(addr>=0&&addr<SArry.GetSize()&&SArry[addr].kind==CN_POINTER)
		{
			itype=2;
		}

		if(itype<1 || itype>2)
		{
			return false;
		}
        
		if(pas->m_pinfo && pas->m_pinfo->pleft && pas->m_pinfo->pright &&
		   (pas->m_pinfo->T.key==CN_ADD || pas->m_pinfo->T.key==CN_SUB) )
		{
			/////////////////////////
	/*	//	AfxMessageBox("add or sub");//////
			CString str;
			str.Format("leftaddr=%d,rightaddr=%d",addr,pas->m_pinfo->pleft->T.addr);///
			AfxMessageBox("add or sub:"+str);////*/
///////////////////////////////////
			ENODE *pleft=NULL,*pright=NULL;
			pleft=pas->m_pinfo->pleft;
			pright=pas->m_pinfo->pright;

			if(pleft->T.key==CN_VARIABLE && pleft->T.addr==addr &&
			   pleft->info==pas->m_pleft->info &&   
			   pleft->pleft==NULL && pleft->pright==NULL &&
			   isConstOfE(pright))
			{
			//	AfxMessageBox("true");
				return true;
			}
		}
	}

	return false;
}
bool RemoveInvocation::isConstOfE(ENODE *p)
{
	/******************************************************

	    FUNCTION:
		the node is CN_CINT,CN_CCHAR or CN_CFLOAT(including 
		-/+ const);
			    
		PARAMETER:
		p   : ENODE pointer
		
	******************************************************/

	if(!p)
	{
		return false;
	}

	if(p->T.key==CN_CINT || p->T.key==CN_CCHAR || p->T.key==CN_CFLOAT)
	{
		return true;
	}

	if((p->T.key==CN_ADD || p->T.key==CN_SUB ) && p->pleft && p->pright==NULL)  
	{
		if(p->pleft->T.key==CN_CINT || p->pleft->T.key==CN_CCHAR || p->pleft->T.key==CN_CFLOAT)
		{
			if(p->T.key==CN_SUB)
			{
				CopyTNODE(p->T,p->pleft->T);
				if(p->pleft->T.key==CN_CFLOAT)
				{
					p->T.value=-p->T.value;  
				}
				else
				{
					p->T.addr=-p->T.addr;
				}
			}
			else
			{
				CopyTNODE(p->T,p->pleft->T);
			}
			if(p->pleft )
			delete p->pleft;
			p->pleft=NULL;
			return true;
		}
		
	}	

	return false;
}

void RemoveInvocation::FrequencyOfExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		search syntax tree and add variable Statistic 
		information to vFreq;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=pENode;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;
				if(p->T.key==CN_VARIABLE)
				{
				//	AfxMessageBox(p->T.name);//

					for(int i=0;i<vFreq.GetSize(); i++)
					{
						if(p->T.addr==vFreq[i].addr)
						{
							vFreq[i].vcount++; 
	                        ////////////////////////////////////////
						/*	CString str;
							str.Format(":%d",vFreq[i].vcount) ;
							
							AfxMessageBox(vFreq[i].oldname+str);///*/
                             ////////////////////////
						}
					}
					int i;
					for(i=0;i<10&& p->pinfo[i] ; i++)
					{
						FrequencyOfExp(p->pinfo[i],SArry,vFreq);						
					}
				}
			///////////////////////wtt//////////3.8/////////////////函数参数中的变量///
				if(p->T.key==CN_BFUNCTION||p->T.key==CN_DFUNCTION)
				{
						
					for(int i=0;i<10&& p->pinfo[i] ; i++)
					{
						FrequencyOfExp(p->pinfo[i],SArry,vFreq);						
					}

				}
				////////////////////////////////////

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);
}

void RemoveInvocation::AssignNewName(CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		Assign new name to variable based on Statistic result;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/
	/*
	TRACE("\n");
    for(int j=0;j<vFreq.GetSize();j++)
	{
		TRACE("%s= addr=%d vcount=%d type=%d\n",vFreq[j].oldname,vFreq[j].addr,vFreq[j].vcount,vFreq[j].type );
	}
	*/

	CArray<bool,bool> aLab;
	for(int i=0;i<vFreq.GetSize();i++)
	{
		if(vFreq[i].vcount<=0)
		{
			aLab.Add(true);
		}
		else
		{
		
			aLab.Add(false); 
		}
	}

	bool bEnd=false;
	int index=0;
	int value=0;
	CString str,s;
	int inum=0,cnum=0,dnum=0,fnum=0,itr=0;
	int i;
	while(!bEnd)
	{
		i=0;
		while(i<vFreq.GetSize() && aLab[i])
		{
			i++;
		}
		if(i>=vFreq.GetSize())
		{
			break;
		}

		index=i;
		value=vFreq[i].vcount;

		while(i<vFreq.GetSize())
		{
			if(aLab[i]==false && vFreq[i].vcount>value)
			{
				index=i;
				value=vFreq[i].vcount;
			}
			i++;
		}

		if(index<vFreq.GetSize() && aLab[index]==false)
		{
			str="";
			s="";
/////////////////////////////////////////////////////////////////////////
		/*	if(vFreq[index].addr>=0&&vFreq[index].addr<SArry.GetSize())
				AfxMessageBox(vFreq[index].oldname+" is renamed");*/
			
/////////////////////////////////////////////////////////////////////////
			if(vFreq[index].addr>=0&&vFreq[index].addr<SArry.GetSize())
			switch(SArry[vFreq[index].addr].type)
			{
			case CN_LONG:
			case CN_SHORT:
			case CN_UNSIGNED:
			case CN_INT:
				if(vFreq[index].type>=2)
				{
				//	AfxMessageBox("int inter");
					itr++;
					if(itr<10)
					{
						s.Format("_0%d",itr); 
					}
					else
					{
						s.Format("_%d",itr); 
					}
					str="x";
				}
				else
				{
				//	AfxMessageBox("normal int");////
					inum++;
				    if(inum<10)
					{
						s.Format("_0%d",inum); 
					}
					else
					{
						s.Format("_%d",inum); 
					}
					str="i";
				}

				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			case CN_CHAR:
				cnum++;
				if(cnum<10)
				{
					s.Format("_0%d",cnum); 
				}
				else
				{
					s.Format("_%d",cnum); 
				}
				str="c";
				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			
			case CN_DOUBLE:
				dnum++;
				if(dnum<10)
				{
					s.Format("_0%d",dnum); 
				}
				else
				{
					s.Format("_%d",dnum); 
				}
				str="d";
				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			case CN_FLOAT:
				fnum++;
				if(fnum<10)
				{
					s.Format("_0%d",fnum); 
				}
				else
				{
					s.Format("_%d",fnum); 
				}
				str="f";
				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
		////////////////////////////////////////////////////
			default:
			
				if(vFreq[index].type>=2)
				{
					itr++;
					if(itr<10)
					{
						s.Format("_0%d",itr); 
					}
					else
					{
						s.Format("_%d",itr); 
					}
					str="x";
				}
				else
				{
					inum++;
				    if(inum<10)
					{
						s.Format("_0%d",inum); 
					}
					else
					{
						s.Format("_%d",inum); 
					}
					str="i";
				}

				if(vFreq[index].addr>=0&&vFreq[index].addr<SArry.GetSize()&& SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(vFreq[index].addr>=0&&vFreq[index].addr<SArry.GetSize()&&SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			}
//////////////////////////////////////////////////////////
			vFreq[index].newname=str+s;
			aLab[index]=true;

		}		
		
		// is every variable assigned a new name?
		bEnd=true;
		for(i=0;i<aLab.GetSize();i++)
		{
			if(!aLab[i])
			{
				bEnd=false;
			}
		}
		// end of this part
	}
    /*
	for(i=0;i<vFreq.GetSize();i++)
	{
		TRACE("%s addr=%d vcount=%d type=%d newname=%s\n",vFreq[i].oldname,vFreq[i].addr,vFreq[i].vcount,vFreq[i].type,vFreq[i].newname);
	}
	*/
}

void RemoveInvocation::ResetVarName(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		Reset Variable  name based on vFreq's elements;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
    p=pHead;

	for(int i=0;i<vFreq.GetSize();i++)
	{
		if(vFreq[i].addr>=0&&vFreq[i].addr<SArry.GetSize())
		{
		SArry[vFreq[i].addr].name=vFreq[i].newname;
		//AfxMessageBox(vFreq[i].newname+" "+vFreq[i].oldname);////
		}
	}

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p->visit==p->GetRelateNum())
			{
				// Add action here
                
				if(p->m_sType=="#DECLARE" && p->TCnt.GetSize()>=1 && 
				   p->TCnt[0].key==CN_VARIABLE&&p->TCnt[0].addr>=0&&p->TCnt[0].addr<SArry.GetSize())
				{
					p->TCnt[0].name=SArry[p->TCnt[0].addr].name; 
				}
				/////////////////////////////////////////
			/*	if(p->m_sType=="ASSIGNMENT")
				{
						CAssignmentUnit *pas=(CAssignmentUnit *)p;
					AfxMessageBox("assignment rename:"+GetExpString(pas->m_pleft)+"="+GetExpString(p->m_pinfo));///////
					

				}*/
				///////////////////////////////////////////////

				RenameVarInExp(p->m_pinfo,SArry,vFreq);

				if(p->m_sType=="ASSIGNMENT")
				{
				//	AfxMessageBox("assignment rename");///////
					GetExpString(p->m_pinfo,SArry);

					CAssignmentUnit *pas=(CAssignmentUnit *)p;
					RenameVarInExp(pas->m_pleft,SArry,vFreq);
				}

				// end of this part
				S.RemoveTail();
			}
					   			
			if(p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    
}

void RemoveInvocation::RenameVarInExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		search syntax tree and add variable Statistic 
		information to vFreq;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=pENode;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;
				if(p->T.key==CN_VARIABLE || p->T.key==CN_DFUNCTION ||p->T.key==CN_BFUNCTION)
				{
					for(int i=0;i<vFreq.GetSize(); i++)
					{
						if(p->T.addr==vFreq[i].addr && p->T.key==CN_VARIABLE)
						{
							p->T.name=vFreq[i].newname; 
						}
					}
					/////////////////
				/*	if(p->T.key==CN_VARIABLE&&SArry[p->T.addr].kind==CN_ARRAY)
					{
						AfxMessageBox("array:"+p->T.name );
						CString str;
						str.Format("p->info=%d",p->info);
						AfxMessageBox(str );
					}*/
					//////////////////////

				//	for(i=0;i<p->info&& p->info<10 && p->pinfo[i]  ; i++)
					int i;
					for(i=0;i<10 && p->pinfo[i]  ; i++)
					{
						/*if(p->T.key==CN_VARIABLE)
						{
							AfxMessageBox("array index:"+p->pinfo[i]->T.name);////////
						}*/
						RenameVarInExp(p->pinfo[i],SArry,vFreq);						
					}
				}

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);
}


////////////////////////////////////////// 以上为////变量重命名//////////////////////////////////////////////////
/////////////////////////////以下/////重新排列语句顺序/////////////////////////////////////////////////////////////////////////////////////
void RemoveInvocation::SetSDGNodeName(CSDGBase *pHead, CArray<SNODE,SNODE>&SArry)
{
	/***********************************************************************
		FUNCTION:
		set the string of every node  in SDGthe
		PARAMETER:
		pHead : head pointer of SDG
		
	***********************************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	int depth=0;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);
			
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p&&p->visit==p->GetRelateNum())
			{
				// Add your code here
				p->m_sName="";//wtt///3.17

				p->m_sName+=p->m_sType+":";
				//AfxMessageBox(p->m_sType);

				if(p->m_sType=="ASSIGNMENT")
				{
					CAssignmentUnit *pas=(CAssignmentUnit *)p;
					p->m_sName+=GetExpString(pas->m_pleft,SArry);
			
					p->m_sName+="=";
					p->m_sName+=GetExpString(p->m_pinfo,SArry);
				

				}			
				else if(p->m_sType=="SELECTOR")
				{
					p->m_sName+=GetExpString(p->m_pinfo,SArry);
				
				}
				else if(p->m_sType=="_ELSE")
				{
					p->m_sName+=GetExpString(p->m_pinfo,SArry);	
				
				}
				else if(p->m_sType=="ITERATION")
				{
					p->m_sName+=GetExpString(p->m_pinfo,SArry);
					
				}
				else if(p->m_sType=="#DECLARE")
				{
					p->m_sName+=p->TCnt.GetSize()?p->TCnt[0].name:"";
				}
				else if(p->m_sType=="SDGCALL")
				{
					p->m_sName+=GetExpString(p->m_pinfo,SArry);
				
				}
				else if(p->m_sType=="SDGRETURN")
				{
					p->m_sName+=GetExpString(p->m_pinfo,SArry);	
					
				}
				else if(p->m_sType=="SDGENTRY")
				{
					p->m_sName+=p->TCnt.GetSize()?p->TCnt[0].name:"";					
				}


				//TRACE("ID=%d:%s\n",p->idno,p->m_sName);

				// End of this part
				S.RemoveTail();
			}
			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else if(p)
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
	
}


void RemoveInvocation::ResetStatementOrder(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************************************
		FUNCTION:
		Rearrange the statements order in SDG under the condition that the 
		program semantic is not changed.
		PARAMETER:
		as above.
	***********************************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p&&p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else if(p)
			{
				p->visit=0;
				// Add your code here
				if(p->GetRelateNum()>=2)
				{
					ResetChildrenOrder(p,SArry);
				}				
				// end of this part

				p=NULL;
			}
		}
    }
	
}

void RemoveInvocation::ResetChildrenOrder(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************************************
		FUNCTION:
		Rearrange the sub-statements order of pNode under the condition that
		the program semantic is not changed.
		PARAMETER:
		as above.
	***********************************************************************/

	int ix=0;
	int beg=0,end=0;
	if(pNode==NULL)
		return;
	while(ix<pNode->GetRelateNum())
	{
		beg=ix;
		end=ix;
		while(ix<pNode->GetRelateNum() &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGBREAK")==NULL &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGRETURN")==NULL &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGENTRY")==NULL &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGCONTINUE")==NULL)
		{
			ix++;
		}
		
		if(ix>=end+2)
		{
			end=ix-1;
			ResetRangeOrder(beg,end,pNode,SArry);
		}
		
		ix++;
	}

}

void RemoveInvocation::ResetRangeOrder(const int bpos,const int epos,CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************************************
		FUNCTION:
		Rearrange the sub-statements(the index form bpos to epos) order of 
		pNode under the condition that the program semantic is not changed.
		PARAMETER:
		as above.
	***********************************************************************/	

	if(bpos>=epos || pNode==NULL||pNode->GetRelateNum()<=epos)
	{
		return;
	}

	int nbound=bpos;
	CSDGBase *pTemp=NULL;
    	   
	for(int ix=bpos+1;ix<=epos;ix++)
	{
		int i;
		for( i=ix-1;i>=bpos;i--)
		{
			nbound=i;
			if(IsDepended(pNode->GetNode(i),pNode->GetNode(ix)))
			{
				nbound=i+1; 
				break;
			}
			
		}

		if(nbound<ix)
		{
			for(i=nbound;i<ix;i++)
			{
				///////////////////wtt/////5.12///////////
				CString str1="",str2="";

				if(pNode->GetNode(ix)&&pNode->GetNode(i))
				{
				str1=pNode->GetNode(ix)->m_sName;
				str2=pNode->GetNode(i)->m_sName;
				if(str1.Left(13)=="SDGCALL:scanf"&& str2.Left(13)=="SDGCALL:scanf")
				{//scanf语句按照输入的变量名大小排序
					int npos1=str1.GetLength()-1;
					while(npos1>=0 && str1.GetAt(npos1)!=',')
						npos1--;
					if(npos1>0)
					{
						str1=str1.Mid(npos1,str1.GetLength()-1) ;
					}
					int npos2=str2.GetLength()-1;
					while(npos2>=0 && str2.GetAt(npos2)!=',')
						npos2--;
					if(npos2>0)
					{
						str2=str2.Mid(npos2,str2.GetLength()-1) ;
					}
				//	AfxMessageBox("scanf "+str1+" "+str2);/////
					if(str1<str2)
					{
					//	AfxMessageBox("change");
					pTemp=pNode->GetNode(ix);
					pNode->InsertNode(i,pTemp);
					pNode->Del(ix+1);
					break;

					}
				}
				///////////////////////////////////////
				///wtt//5.16/////////////
				else if(str1=="SELECTION:"&&str2=="SELECTION:"&&pNode->GetNode(ix)->GetRelateNum()==1 &&pNode->GetNode(i)->GetRelateNum()==1 )
				{//只有一个selector 的selection语句按照selector的表达式大小排序
					str1=pNode->GetNode(ix)->GetNode(0)->m_sName;
					str2=pNode->GetNode(i)->GetNode(0)->m_sName;
				//	AfxMessageBox(str1);
					//	AfxMessageBox(str2);

					if(str1<str2)
					{
					//	AfxMessageBox("selection change");
					pTemp=pNode->GetNode(ix);
					pNode->InsertNode(i,pTemp);
					pNode->Del(ix+1);
					break;

					}
					

				}
				/////////////////////
				
				else if(pNode->GetNode(ix)->m_sName<pNode->GetNode(i)->m_sName)
				{   
					
					pTemp=pNode->GetNode(ix);
					pNode->InsertNode(i,pTemp);
					pNode->Del(ix+1);
					break;
				}
			}
		}
		}
	}
  
}

bool RemoveInvocation::IsDepended(CSDGBase *pNode1,CSDGBase *pNode2)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the two node have data dependent relation.
		PARAMETER:
		as above.
	***********************************************************************/

	bool rvalue=false;
 if(pNode1==NULL||pNode2==NULL)
	 return false;
	for(int i=0;i<pNode1->m_aDefine.GetSize();i++)
	{
		if(pNode1->m_aDefine[i].key==CN_VARIABLE)
		{
			for(int j=0;j<pNode2->m_aRefer.GetSize();j++)
			{
				if((pNode2->m_aRefer[j].key==CN_VARIABLE) && 
				   (pNode1->m_aDefine[i].name==pNode2->m_aRefer[j].name || 
				    pNode1->m_aDefine[i].addr==pNode2->m_aRefer[j].addr))
				{
					return true;
				}
			}
		}
	}
	int i;
	for(i=0;i<pNode2->m_aDefine.GetSize();i++)
	{
		if(pNode2->m_aDefine[i].key==CN_VARIABLE)
		{
			for(int j=0;j<pNode1->m_aRefer.GetSize();j++)
			{
				if((pNode1->m_aRefer[j].key==CN_VARIABLE) && 
				   (pNode2->m_aDefine[i].name==pNode1->m_aRefer[j].name || 
				    pNode2->m_aDefine[i].addr==pNode1->m_aRefer[j].addr))
				{
					return true;
				}
			}
		}
	}

	return rvalue;
}

CSDGBase *RemoveInvocation::SearchFirstCertainNode(CSDGBase *pSDG,CString strNode)
{
	/******************************************************

	    FUNCTION:
		Pre-order search node with the type specified by 
		strNode in pSDG.

		(此函数调用时必须确保各节点的visit值为零)
	    
		PARAMETER:
		....		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p,*pTemp=NULL;
	bool bFind=false;

	p=pSDG;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			if(!bFind)
			{
				if(p->m_sType==strNode)
				{
					pTemp=p;
					bFind=true;
				}
			}
			
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p&&p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else if(p)
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return pTemp;
}

///////////以上/////重新排列语句顺序////////////////////////////////////

//////////////////以下为//////函数内联操作//////////////////
void RemoveInvocation::Inline(FCNODE*p,FCNODE*pf,CSDGBase*pHead,int num,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//后根序遍历函数调用树对各函数调用进行内联
	if(p==NULL)
		return;
	if(SDGList.GetSize()==0)
		return;
	for(int j=0;j<20&&p->children[j]!=NULL;j++)
	{
		Inline(p->children[j],p,pHead,j,SDGList,CallPosList,SArry,FArry);
	}
	if(pf==NULL)
	{
		return;
	}
	int addr=p->addr;
	if(SDGList.GetSize()<=addr)
		return;
	CSDGBase*pSDG=SDGList[addr];
	
	addr=pf->addr;
	if(SDGList.GetSize()<=addr)
		return;
	CSDGBase*pSDGf=SDGList[addr];

	ModifyPDG(pSDG,pSDGf,pHead,num,SDGList,CallPosList,SArry,FArry);//建立被调函数中的节点与主调函数中节点的控制依赖关系
	///////////wtt  2008.3.24////把主调函数也进行指针分析，执行必然别名替换/////////

	PointerAnalysis pointeranalysis;
    pointeranalysis(pSDGf,SArry,FArry);//对被调函数过程内指针分析
	pointeranalysis.mustAliasReplace(pSDGf,SArry,FArry);//必然别名替换
  //  
	//////////////////////////////////////////


	pSDG=NULL;





}

extern CSDGBase *CopyBranchEx(CSDGBase *pint);//拷贝pint所指的分支
void RemoveInvocation::ModifyPDG(CSDGBase*pSDG,CSDGBase*pSDGf,CSDGBase*pUSDGHead,int num,	CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//将pSDG内联到pSDGf的第num个调用位置
	CSDGBase*p=CopyBranchEx(pSDG);//复制被调函数的PDG

	RenamePDG(p,pSDGf,num,SArry,FArry);//将复制的PDG中的变量重名命

   AssignActuralToFormal(p,pSDGf,num,SDGList,CallPosList,SArry,FArry);//将pSDGf中的实参赋给p中相应的形参

	///////////wtt  2008.3.24/////////////

	PointerAnalysis pointeranalysis;
    pointeranalysis(p,SArry,FArry);//对被调函数过程内指针分析
	pointeranalysis.mustAliasReplace(p,SArry,FArry);//必然别名替换

	//////////////////////////////////////////
	SetDataFlow(p,SArry,FArry);//重新设置复制的PDG中的数据流
	PassParameter(p,SArry);//用实参替换被调函数PDG中与其等价的形参
	SetDataFlow(p,SArry,FArry);//重新设置数据依赖关系

	PassReturnValue(p,pSDGf, num,SDGList,CallPosList,SArry,FArry);//用主调函数中接收返回值的变量替换被调函数中的返回值
	SetDataFlow(p,SArry,FArry);//重新设置数据依赖关系

	ModifyControlDependence(p,pSDGf, num,SDGList,CallPosList,SArry,FArry);
	p=NULL;

//	DeleteUnrefered(pUSDGHead,SArry,FArry);//删除简单语句及未被引用的赋值语句和变量声明节点



////////////////////测试用的打印复制的PDG//////////////
	/*	SetSDGNodeName(pSDG,SArry);//重新设置复制的PDG中的节点的名字	
	CString s="Post traverse  copy PDG:\r\n";
    s+="ID |type |dfd | ref|  GEN |KILL| IN| OUT |dataflow |declaration |name |\r\n"; 
	s+=PrintPDG(pSDG,SArry);
	AfxMessageBox(s);*/


/*	SetSDGNodeName(p,SArry);//重新设置复制的PDG中的节点的名字	
	CString s="Post traverse  copy PDG:\r\n";
    s+="ID |type |dfd | ref|  GEN |KILL| IN| OUT |dataflow |declaration |name |\r\n"; 
	s+=PrintPDG(p,SArry);
	AfxMessageBox(s);
	SetSDGNodeName(pSDGf,SArry);//重新设置复制的PDG中的节点的名字	
	 s="Post traverse  copy PDG:\r\n";
    s+="ID |type |dfd | ref|  GEN |KILL| IN| OUT |dataflow |declaration |name |\r\n"; 
	s+=PrintPDG(pSDGf,SArry);

	int pos=0;
	
	do
	{
		CString str=s.Mid(pos,3000);
		AfxMessageBox(str);
		pos=pos+3000;
	}while(pos<s.GetLength()); */
////////////////////////////////////////////////////////////////
	
	

}
//extern CString PrintExp(CArray<TNODE,TNODE> &T);
CString RemoveInvocation::PrintPDG(CSDGBase *p,CArray<SNODE,SNODE>&SArry)
{//测试用的打印复制的PDG
	CString strr="";
	if(p==NULL)
		return "";
	for(int i=0;i<p->GetRelateNum();i++)
	{
		strr+=PrintPDG(p->GetNode(i),SArry);

	}
	CString s,st,stemp;
	s.Format("%-2d %-8s ",p->idno,p->m_sType);
	st=_T("");
                //s+=stemp;
				
	if(p->GetFather())
	{
		st.Format("%4d ",p->GetFather()->idno);
	}
	s+=st;
	st="";
	/////////////definition and reference
				//s+=" Dfd:";
	stemp="";
	int i;
	for( i=0;i<p->m_aDefine.GetSize();i++)
	{   
		stemp+="["+p->m_aDefine[i].name+"]";
					//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_aDefine.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";

	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st;
	//s+=" Rfd:";
	stemp="";
	for(i=0;i<p->m_aRefer.GetSize();i++)
	{   
		stemp+="["+p->m_aRefer[i].name+"]";
		//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_aRefer.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";
	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 

	stemp="";
	for(i=0;i<p->m_aGen.GetSize();i++)
	{   
		CString s;
		s.Format("[%d,%d]",p->m_aGen[i].SDGID,p->m_aGen[i].varaddr);
		stemp+=s;
		//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_aGen.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";

	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 


	stemp="";
	for(i=0;i<p->m_aKill.GetSize();i++)
	{   
		CString s;
		s.Format("[%d,%d]",p->m_aKill[i].SDGID,p->m_aKill[i].varaddr);
		stemp+=s;
					//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_aKill.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";
	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 
	stemp="";
	for(i=0;i<p->m_aIn.GetSize();i++)
	{   
		CString s;
		s.Format("[%d,%d]",p->m_aIn[i].SDGID,p->m_aIn[i].varaddr);
		stemp+=s;
		//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_aIn.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";
	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 
	stemp="";
	for(i=0;i<p->m_aOut.GetSize();i++)
	{   
			CString s;
		s.Format("[%d,%d]",p->m_aOut[i].SDGID,p->m_aOut[i].varaddr);
		stemp+=s;
		//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_aOut.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";
	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 

				
	///////////////////////////
	stemp="";
	for(i=0;i<p->m_pAntiDRNodes.GetSize();i++)
	{   
		
		if(p->m_pAntiDRNodes[i]!=NULL)
		{
			CString s;
			s.Format("[%d]",p->m_pAntiDRNodes[i]->idno);
			stemp+=s;
		}
					//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_pAntiDRNodes.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";
	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 
	///////////////

	stemp="";
	for(i=0;i<p->m_pDecRNodes.GetSize();i++)
	{   
		if(p->m_pDecRNodes[i]!=NULL)
		{
			CString s;
			s.Format("[%d]",p->m_pDecRNodes[i]->idno);
			stemp+=s;
		}
			//s+="["+p->m_aDefine[i].name+"]";
	}
	if(p->m_pDecRNodes.GetSize()>0)
	{
		st.Format(" %-s",stemp);
		st+="||";
	}
	else
	{
		st.Format(" %-s"," @");
		st+="||";
	}			
	s+=st; 
//////////////////////expression and name//
		
				//st.Format(" %-20",p->m_sName);
				s+=p->m_sName; 


				s+="{";
					
					for(i=0;i<p->ptSet.GetSize();i++)
					{
					//	AfxMessageBox("pointer alias");
						st="<";
				
						st+=GetExpString(p->ptSet[i]->psrc,SArry);
					//	st+=GetENodeStr(p->ptSet[i]->psrc,SArry);
			
						st+=",";
			
					//	st+=GetENodeStr(p->ptSet[i]->ptar,SArry);
						st+=GetExpString(p->ptSet[i]->ptar,SArry);


						st+=",";
						if(p->ptSet[i]->isdefinit==1)
						{
								st+="1";

						}
						else
						{
								st+="0";
						}
						st+=">";
						s+=st;

					}
					s+="}";



				s+="\r\n";
				s+="------------------------------\r\n";

		strr+=s;


                return strr;


}

///////////////////////////////////以下将复制的PDG中的变量重名命，在符号表中相应增加新的节点//////////////
void RemoveInvocation::RenamePDG(CSDGBase*p,CSDGBase*pf,int num,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//将复制的PDG中的变量重名命，在符号表中相应增加新的节点
	if(p==NULL||pf==NULL||p->m_sType!="SDGENTRY"||pf->m_sType!="SDGENTRY")
		return;
	if(p->TCnt.GetSize()==0||pf->TCnt.GetSize()==0)
		return;
	int layer=p->TCnt[0].addr;
	int layer1=pf->TCnt[0].addr;
	/////////////////////////////
/*	CString str;
	str.Format("p.addr=%d,pf.addr=%d",layer,layer1);
	AfxMessageBox(str);*/
	/////////////////////////////
///////////6.6/////////////////////////////////////////////////////

		layer=layer+1;//函数名在符号表中的layer比它在函数列表中的下标大1
		layer1=layer1+1;

	
	
	/*


	for(int i=0;i<SArry.GetSize();i++)
	{
		
		if(SArry[i].addr==addr)
			break;
		
		
	}
	if(i==SArry.GetSize())
		return;
	int layer=SArry[i].layer;
	
	for(i=0;i<SArry.GetSize();i++)
	{
		if(SArry[i].addr==addr1)
			break;
		
	}
	if(i==SArry.GetSize())
		return;
	int layer1=SArry[i].layer;*/
	///////////////////////////6.6/////////////////////////
	
	CArray<AddrNODE,AddrNODE>AddrList;
	AddrList.RemoveAll(); 

	int no=SArry.GetSize();
	for(int i=0;i<no;i++)
	{	
		if(SArry[i].layer==layer)
		{
			SNODE SN;
		
			SN.addr=SArry[i].addr;
			SN.arrdim=SArry[i].arrdim;
			for(int j=0;j<4;j++)
			{
			SN.arrsize[j]= SArry[i].arrsize[j];
			}
			SN.initaddr=SArry[i].initaddr;
			SN.kind=SArry[i].kind;
			SN.type=SArry[i].type;
			SN.value=SArry[i].value;			

			SN.layer=layer1;//层次与其主调函数的层次相同
		

			if(SArry[i].kind==CN_DFUNCTION)//函数名不变
			{
				SN.name=SArry[i].name;
			}
			else //变量名前加li
			{
				CString s;
				s.Format("l%d_",num);
				s+=SArry[i].name;
				SN.name=s; 
			}

			AddrNODE AN;
			AN.curaddr= SArry.GetSize();
			AN.initaddr=SN.initaddr;
			AddrList.Add(AN);
			
			SArry.Add(SN); 
			
			
		}
	}
	RenameVar(p,SArry,AddrList);

	
	
}
void RemoveInvocation::RenameVar(CSDGBase*p,CArray<SNODE,SNODE> &SArry,CArray<AddrNODE,AddrNODE>&AddrList)
{//将复制的PDG中p中的变量重名命
	if(p==NULL)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		RenameVar(p->GetNode(i),SArry,AddrList);

	}
	
	if(p->m_sType=="#DECLARE" && p->TCnt.GetSize()>=1 &&p->TCnt[0].key==CN_VARIABLE)
	{
		int initaddr=p->TCnt[0].addr;
		int curaddr;
		int j;
		for( j=0;j<AddrList.GetSize();j++)
		{
			if(AddrList[j].initaddr==initaddr)
			{	
				curaddr=AddrList[j].curaddr;
				break;
			}
		}
		if(j<AddrList.GetSize()&&curaddr>=0&&curaddr<SArry.GetSize())
		{
			p->TCnt[0].name=SArry[curaddr].name;
			p->TCnt[0].addr= curaddr;//5.12

		}
	}
	
	RenameVarInExp1(p->m_pinfo,SArry,AddrList);

	if(p->m_sType=="ASSIGNMENT")
	{
		//AfxMessageBox("rename assignment");///////////////
		CAssignmentUnit *pas=(CAssignmentUnit *)p;
		RenameVarInExp1(pas->m_pleft,SArry,AddrList);
	}

}
void RemoveInvocation::RenameVarInExp1(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<AddrNODE,AddrNODE>&AddrList)
{//重新将表达式中的变量重名命
	

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=pENode;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;
				if(p->T.key==CN_VARIABLE || p->T.key==CN_DFUNCTION ||p->T.key==CN_BFUNCTION)
				{
					
					int initaddr=p->T.addr;
					int curaddr;
					int j;
					for( j=0;j<AddrList.GetSize();j++)
					{
						if(AddrList[j].initaddr==initaddr)
						{	
							curaddr=AddrList[j].curaddr;
							break;
						}
					}
					if(j<AddrList.GetSize()&& p->T.key==CN_VARIABLE&&curaddr>=0&&curaddr<SArry.GetSize())
					{
						p->T.name=SArry[curaddr].name;
						p->T.addr=curaddr;//5.12 
					}
					

					for(int i=0;i<10 && p->pinfo[i] ; i++)
					{
					
						RenameVarInExp1(p->pinfo[i],SArry,AddrList);						
					}
				}

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);
}

///////////////////////////////////以上将复制的PDG中的变量重名命，在符号表中相应增加新的节点//////////////

/////////////////////////////////
extern ENODE *ExCopyENODE(ENODE *pnew,ENODE *pint);
void RemoveInvocation::AssignActuralToFormal(CSDGBase*p,CSDGBase*pf,int num,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//将pf中的实参赋给p中相应的形参
	if(p==NULL||pf==NULL||p->m_sType!="SDGENTRY"||pf->m_sType!="SDGENTRY")
	return;

//	AfxMessageBox("start pass parameter");


	if(p->TCnt.GetSize()<=0||pf->TCnt.GetSize()<=0)
		return;
	
//	AfxMessageBox("start pass parameter");//////////////////

	int addr=p->TCnt[0].addr;
	int addr1=pf->TCnt[0].addr;
	if(addr<0||addr>=FArry.GetSize()||addr1<0||addr1>=FArry.GetSize() ||addr1>=20||num>=20)//wtt  2007.11.13
		return;


	CSDGBase*pcall=CallPosList[addr1][num];

	if(pcall==NULL)
		return;

	//	AfxMessageBox("start pass parameter");//////////////////

	int sum=0;
	for(int i=0;i<20;i++)
	{
		if(addr>=0&&addr<FArry.GetSize()&&FArry[addr].plist[i]!=-1)
			sum++;
	}
	int sum1=0;
	int i;
	for( i=0;i<10;i++)
	{
		if(pcall->m_pinfo&&pcall->m_pinfo->pinfo[i]!=NULL)
			sum1++;
	}

	if(sum!=sum1||sum==0)//形参、实参个数不等或没有实参则不处理
		return;


	CArray<CSDGBase*,CSDGBase*>FormParaList;//保存p中的形参声明节点
	FormParaList.RemoveAll(); 
	
	for( i=0;i<20;i++)
	{
		if(addr>=0&&addr<FArry.GetSize()&&FArry[addr].plist[i]!=-1)
		{
			for(int j=0;j<p->GetRelateNum();j++)
			{
				if(p->GetNode(j)&&p->GetNode(j)->m_sType=="#DECLARE"&&p->GetNode(j)->TCnt.GetSize()>=1&&p->GetNode(j)->TCnt[0].addr>=0&&p->GetNode(j)->TCnt[0].addr<SArry.GetSize()&&addr>=0&&addr<FArry.GetSize()&&SArry[p->GetNode(j)->TCnt[0].addr].initaddr==FArry[addr].plist[i])
			{
					//标记DECLARE节点中的no编号
					/////////////////
			/*	CString s;
				s.Format("FArry[addr].plist[i]=%d",FArry[addr].plist[i]);
				AfxMessageBox(s);*/
	/////////////////////////
					CSDGBase*p1=p->GetNode(j);
				
					p1->no=i;
					FormParaList.Add(p1); 

				}
			}
		}
	}

	for(i=0;i<p->GetRelateNum();i++)
	{
		if(p->GetNode(i)&&p->GetNode(i)->m_sType!="#DECLARE" )
			break;
	}
	int insertpos=i;

	i=FormParaList.GetSize()-1;
	/////////////////
	/*CString s;
	s.Format("FormParaList.GetSize()=%d",FormParaList.GetSize());
	AfxMessageBox(s);*/
	/////////////////////////
	while(i>=0)
	{
			//	AfxMessageBox("ok");///////////////////

			/*	if(FormParaList[i]&&FormParaList[i]->TCnt[0].addr>=0&&FormParaList[i]->TCnt[0].addr<SArry.GetSize()
					&&pcall->m_pinfo->pinfo[i]->T.addr>=0&&pcall->m_pinfo->pinfo[i]->T.addr<SArry.GetSize()
					&&pcall->m_pinfo->pinfo[i]&&pcall->m_pinfo->pinfo[i]->T.key==CN_VARIABLE
					&&pcall->m_pinfo->pinfo[i]->T.addr<SArry.GetSize() 
					&& SArry[FormParaList[i]->TCnt[0].addr].kind!=SArry[pcall->m_pinfo->pinfo[i]->T.addr].kind )//形参与实参数据种属不同则不传参
				{
					AfxMessageBox("wrong parameter");//////////
					return;
				} wtt 2008.3.24这个不对，数组可以给指针赋值，普通变量取地址也可以给指针赋值*/

			////////////// 数组名作为函数参数处理，用实参数组名替换形参数组名/////////////////////////
	
		if(FormParaList[i]->TCnt[0].addr>=0&&FormParaList[i]->TCnt[0].addr<SArry.GetSize()&& SArry[FormParaList[i]->TCnt[0].addr].kind==CN_ARRAY)
		{
			//AfxMessageBox("formal array");
			CList<CSDGBase *,CSDGBase *> S;
			CSDGBase *p1=p;
			while(p1!=NULL || S.GetCount()>0)
			{
				if(p1!=NULL)
				{
						
					S.AddTail(p1); 
					if(p1->visit<p1->GetRelateNum())
					{
						p1->visit++;
						p1=p1->GetNode(p1->visit-1);
					}
					else
					{
						p1=NULL; 
					}
				}
				else
				{
					p1=S.GetAt(S.GetTailPosition());
					if(p1->visit==p1->GetRelateNum())
					{
					// Add action here
						ENODE*pe;
						pe=new ENODE;
						InitialENODE(pe);
						pe->pleft=NULL;
						pe->pright=NULL;
						CopyTNODE(pe->T,FormParaList[i]->TCnt[0]);

        				if(pcall->m_pinfo)
						ReplaceArrVar(pcall->m_pinfo->pinfo[i],pe,p1->m_pinfo,SArry);//有问题
	
						if(p1->m_sType=="ASSIGNMENT"&&pcall->m_pinfo)
						{
							CAssignmentUnit *pas2=(CAssignmentUnit *)p1;
							ReplaceArrVar(pcall->m_pinfo->pinfo[i],pe,pas2->m_pleft,SArry);
						}

				// end of this part
						S.RemoveTail();
					}
					   			
					if(p1->visit<p1->GetRelateNum())
					{		
						p1->visit++;
						p1=p1->GetNode(p1->visit-1);				
					}
					else
					{
						p1->visit=0;
						p1=NULL;
					}
		
			}
   	
		}
		
		i--;
		continue;
		}



	///////////////////////////////////////////////////////////////////////////
		CAssignmentUnit*pas=new CAssignmentUnit;
		pas->m_sType="ASSIGNMENT";
		pas->b_formal=true; //////////////////////
		ENODE*pExpl=new ENODE;
		InitialENODE(pExpl);
		pExpl->T.key=CN_VARIABLE;
		pExpl->T.addr=FormParaList[i]->TCnt[0].addr;
		pExpl->T.name=FormParaList[i]->TCnt[0].name;
		pExpl->T.deref=FormParaList[i]->TCnt[0].deref;///wtt  2008.3.24

		pExpl->info=0;
		pExpl->pleft=NULL;
		pExpl->pright=NULL; 
		
		pas->m_pleft=pExpl; 


		ENODE*pexpr=new ENODE;
		if(pcall->m_pinfo)
		pexpr=ExCopyENODE(pexpr,pcall->m_pinfo->pinfo[i]);
		pas->m_pinfo=pexpr;
        
	//	AfxMessageBox("add assignment");/////
		p->InsertNode(insertpos,pas); 
		pas->SetFather(p); 
		
		i--;
	}


}
void RemoveInvocation::MarkFormParameter(CSDGBase*pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//标示出形参的声明节点，与普通变量的声明节点区别：普通变量的声明节点no=-1;形参的声明节点no=是第几个形参(从0开始)
	if(pHead==NULL)
		return;
	if(pHead->m_sType!="SDGHEAD")
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		if(pHead->GetNode(i)&&pHead->GetNode(i)->m_sType=="SDGENTRY")
		{
			CSDGBase*pentry=pHead->GetNode(i);
			if(pentry&&pentry->TCnt.GetSize()<=0)
			continue;
			for(int k=0;k<pentry->GetRelateNum();k++)
			{
				if(pentry->GetNode(k)&&pentry->GetNode(k)->m_sType=="#DECLARE")
				{
					////////////////////////////////////////
				/*	CSDGBase*p1=pentry->GetNode(k);
					AfxMessageBox(pentry->GetNode(k)->TCnt[0].name);/////
					CString s;
					s.Format("addr=%d,initaddr=%d",pentry->GetNode(k)->TCnt[0].addr,SArry[pentry->GetNode(k)->TCnt[0].addr].initaddr);
					AfxMessageBox(s);*/
					///////////////////////////////////////
				//	CSDGDeclare* pd=(CSDGDeclare*) p1;
				pentry->GetNode(k)->no=-1;	
				

				}
			}

			int addr=pentry->TCnt[0].addr;
			////////////////////////////////////////
			//	CSDGBase*p1=pentry->GetNode(k);
				//	AfxMessageBox(pentry->GetNode(k)->TCnt[0].name);/////
				/*	CString s;
					s.Format("entery addr=%d",pentry->TCnt[0].addr);
					AfxMessageBox(s);*/
					///////////////////////////////////////
			for(int j=0;j<pentry->GetRelateNum();j++)
			{
				/*	if(pentry->GetNode(j)&&pentry->GetNode(j)->m_sType=="#DECLARE"&&pentry->GetNode(j)->TCnt.GetSize()>=1)//error没标记上
					{
					CString s;
					s.Format("initaddr=%d,list[i]=%d",SArry[pentry->GetNode(j)->TCnt[0].addr].initaddr,FArry[addr].plist[i]);
					AfxMessageBox(s);
					}*/
				for(int k=0;k<20&&FArry[addr].plist[k]!=-1;k++)
				{
				if(pentry->GetNode(j)&&pentry->GetNode(j)->m_sType=="#DECLARE"&&pentry->GetNode(j)->TCnt.GetSize()>=1&&SArry[pentry->GetNode(j)->TCnt[0].addr].initaddr==FArry[addr].plist[k])//error没标记上
				{
					//标记DECLARE节点中的no编号
					CSDGBase*p1=pentry->GetNode(j);
					//CSDGDeclare* pd=(CSDGDeclare*) p1;
					//pd->no=i;	
					p1->no=k;

				

				}
				}
			
			}
		}
	}

}
void RemoveInvocation::PassParameter(CSDGBase*p,CArray<SNODE,SNODE> &SArry)
{//用实参替换被调函数中与其等价的形参，注意数据依赖关系
	if(p==NULL)
		return;
	if(p->m_sType!="SDGENTRY")
		return;


	for(int i=0;i<p->GetRelateNum();i++)
	{

		if(p->GetNode(i)&&p->GetNode(i)->m_sType=="ASSIGNMENT")
		{	
			CSDGBase*pc=p->GetNode(i);
			CAssignmentUnit*pas=(CAssignmentUnit*)pc;
			/////wtt 2008.4.4//////指针赋值已经别名替换过了，因此不进行传参////////
			bool flag=true;
			if(pas->m_pleft&&pas->m_pleft->T.addr>=0&&pas->m_pleft->T.addr<SArry.GetSize()&&SArry[pas->m_pleft->T.addr].kind==CN_POINTER)
				flag=false;
/////////////////////////////////////////
			if(flag&&pas->b_formal==true&&pc)
			{
				//	AfxMessageBox("ok");///////////////


				//用实参替换形参
				for(int j=0;j<pc->m_pDRNodes.GetSize();j++ )
				{
					if(pc->m_pDRNodes[j]!=NULL&&pc->m_pDRNodes[j]->m_pinfo)
					{
						ReplaceVar(pc->m_pinfo,pas->m_pleft,pc->m_pDRNodes[j]->m_pinfo,SArry);

						AExpStandard(pc->m_pDRNodes[j]->m_pinfo,SArry);
					}
				}

			}
		}
		
	}

}
void RemoveInvocation::PassReturnValue(CSDGBase*pc,CSDGBase*pf,int num,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//用主调函数中接收返回值的变量替换被调函数中的返回值//需要修改

	if(pc==NULL||pf==NULL||pc->m_sType!="SDGENTRY"||pf->m_sType!="SDGENTRY")
	return;


	if(pc->TCnt.GetSize()<=0||pf->TCnt.GetSize()<=0)
		return;



	int addr=pc->TCnt[0].addr;
	int addr1=pf->TCnt[0].addr;
	if(addr<0||addr>=FArry.GetSize()||addr1<0||addr1>=FArry.GetSize() ||addr1>=20||num>=20)//wtt  2007.11.13
		return;


	CSDGBase*pcall=CallPosList[addr1][num];

	if(pcall==NULL)
		return;


	if(pcall->m_sType!="ASSIGNMENT")
		return;//没有接受返回值得变量则不处理


	CAssignmentUnit*pas1=(CAssignmentUnit*)pcall;
	if(IsSimple(pas1->m_pleft)!=1)
		return;
    //AfxMessageBox("pass return value");////
	///////////////////5.12///////////////////
	 CSDGBase *p=NULL;
	 CList<CSDGBase *,CSDGBase *> S;
	 p=pc;
	// AfxMessageBox(p->m_sType+p->TCnt[0].name   );///
	 while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p&&p->visit==p->GetRelateNum())
			{
				// Add action here
                
		
				if(p->m_sType=="SDGRETURN")
				{
						//					AfxMessageBox("replace return");////

						CSDGBase*preturn=p;
					//	if(IsSimple(preturn->m_pinfo)!=1)
						if(IsSimple(preturn->m_pinfo)==-1)
						return;

						//创建复值语句替换return 语句
						////////////////////////////////////
							CAssignmentUnit*pas=new CAssignmentUnit;
							pas->m_sType="ASSIGNMENT";
							pas->b_formal=false; //////////////////////
							ENODE*pexpl=new ENODE;
							pexpl=ExCopyENODE(pexpl,pas1->m_pleft);	
							ENODE*pexpr=new ENODE;						
							pexpr=ExCopyENODE(pexpr,preturn->m_pinfo);
							pas->m_pleft=pexpl; 
							pas->m_pinfo=pexpr;


							//传递返回值//
							////////////////////////
							int j=0;
							bool flag=true;
							while(j<p->m_pAntiDRNodes.GetSize())
							{
								CSDGBase*pd=NULL;
								pd=p->m_pAntiDRNodes[j];
								if(pd&&pd->m_sType=="ASSIGNMENT" )
								{
									//表达式中变量替换
								//	AfxMessageBox("ok");
									CAssignmentUnit*pas2=(CAssignmentUnit*)pd;
									ReplaceVar(pas->m_pleft,pas->m_pinfo,pas2->m_pleft,SArry);
									ReplaceVar(pas->m_pleft,pas->m_pinfo,pas2->m_pinfo,SArry);
									flag=false;
									////wtt//////2008.3.24////也替换selctor和iteration条件表达式中的相应变量///
									CSDGBase*pft=pd->GetFather();
									if(pft)
									{
										if(pft->m_sType=="SELECTOR"||pft->m_sType=="ITERATION")
										{	
										//	AfxMessageBox("replace");
											ReplaceVar(pas->m_pleft,pas->m_pinfo,pft->m_pinfo,SArry);

											
										}

									}
									/////////////////////////////


								}
								/////////////
								int k=0;
								while(pd&&k<pd->m_pAntiDRNodes.GetSize())
								{
									CSDGBase*pd1=NULL;
									pd1=pd->m_pAntiDRNodes[k];
									if(pd1&&pd1->m_sType=="ASSIGNMENT" )
									{
									//表达式中变量替换
								//	AfxMessageBox("ok");
										CAssignmentUnit*pas2=(CAssignmentUnit*)pd1;
										ReplaceVar(pas->m_pleft,pas->m_pinfo,pas2->m_pleft,SArry);
										ReplaceVar(pas->m_pleft,pas->m_pinfo,pas2->m_pinfo,SArry);
										flag=false;
									////wtt//////2008.3.24////
																			////wtt//////2008.3.24////
									CSDGBase*pft=pd1->GetFather();
									if(pft)
									{
										if(pft->m_sType=="SELECTOR"||pft->m_sType=="ITERATION")
										{	
											ReplaceVar(pas->m_pleft,pas->m_pinfo,pft->m_pinfo,SArry);

											
										}

									}
									/////////////////////////////


									}
									k++;
								}


								//////////////////////

								j++;
							}



							///////////////////////////////////////
							CSDGBase*pft=p->GetFather();
							if(pft==NULL)
								return;
							int index;							
							index=FindIndex(p);
							if(flag)
							{		
						
							pft->InsertNode(index+1,pas);
							pas->SetFather(pft); 
							}
							else
							{
								if(pas)
									DeleteBranch( pas);
							}
							DeleteBranch(pft->GetNode(index));
							pft->Del(index); 
							//AfxMessageBox("add assingment");/////
							

						////////////////////////////////////////

					
				}

				// end of this part
				S.RemoveTail();
			}
					   			
			if(p&&p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else if(p)
			{
				p->visit=0;
				p=NULL;
			}
		}
   }


	////////////////////5.12///////////////////
/*	int i=pc->GetRelateNum()-1;
	while(i>=0)
	{
		if(pc->GetNode(i)&&pc->GetNode(i)->m_sType=="SDGRETURN")
		{
		
			break;
		}
		i--;
	}
	if(i<0)
		return;//没有返回值则不处理


	int pos=i;
	CSDGBase*preturn=pc->GetNode(i);
	if(IsSimple(preturn->m_pinfo)!=1)
		return;



	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;
    p=pc;



    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p&&p->visit==p->GetRelateNum())
			{
				// Add action here
                
		
				ReplaceVar(pas1->m_pleft,preturn->m_pinfo,p->m_pinfo,SArry);

			
				if(p->m_sType=="ASSIGNMENT")
				{
					CAssignmentUnit *pas2=(CAssignmentUnit *)p;
					ReplaceVar(pas1->m_pleft,preturn->m_pinfo,pas2->m_pleft,SArry);
				
				}

				// end of this part
				S.RemoveTail();
			}
					   			
			if(p&&p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else if(p)
			{
				p->visit=0;
				p=NULL;
			}
		}
   }*/
 }


void RemoveInvocation::ModifyControlDependence(CSDGBase*pc,CSDGBase*pf,int num,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//建立被调函数中的节点与主调函数中节点的控制依赖关系
	if(pc==NULL||pf==NULL||pc->m_sType!="SDGENTRY"||pf->m_sType!="SDGENTRY")
	return;


	if(pc->TCnt.GetSize()<=0||pf->TCnt.GetSize()<=0)
		return;



	int addr=pc->TCnt[0].addr;
	int addr1=pf->TCnt[0].addr;
	if(addr<0||addr>=FArry.GetSize()||addr1<0||addr1>=FArry.GetSize() ||addr>=20||num>=20)//wtt 2007.11.13
		return;


	CSDGBase*pcall=CallPosList[addr1][num];

	if(pcall==NULL)
		return;
	//将被调函数中剩余的变量声明节点加入主调函数的变量声明部分
//////////////////5.15//////////////////////
	int ind=0;
	while(ind<pf->GetRelateNum())
	{
		if(pf->GetNode(ind)&&pf->GetNode(ind)->m_sType!="#DECLARE")
			break;
		ind++;

	}
	////////////////////////////////////////
	int no= pc->GetRelateNum()-1;
	for(int i=no;i>=0;i--)
	{
		CSDGBase*p=pc->GetNode(i); 
		if(p&&p->m_sType=="#DECLARE" )
		{
	//	AfxMessageBox("add declare node"+p->TCnt[0].name);////
		//	CSDGDeclare*p1=(CSDGDeclare*)p;
		//	p1->no=-1;
		//	p->no=-1;

		//	pf->InsertNode(0,p); //5.15
			pf->InsertNode(ind,p); //5.15
			p->SetFather(pf); 
			p=NULL;
			pc->Del(i); 

		}
	}
//把被调函数中剩余的非变量声明节点和return节点加入到主调函数中
	int index=FindIndex(pcall);
	CSDGBase*pcf=pcall->GetFather();
	if(pcf==NULL)
		return;
	pcf->Del(index);
	
	 no= pc->GetRelateNum()-1;
	int i;
	for(i=no;i>=0;i--)
	{
		CSDGBase*p=pc->GetNode(i); 
		if(p&&p->m_sType!="#DECLARE" &&p->m_sType!="SDGRETURN")
		{
			pcf->InsertNode(index,p);
			p->SetFather(pcf); 
			p=NULL;
			pc->Del(i); 
		}
	}
	DeleteBranch(pc);
//删除函数调用节点
	pc=NULL;
//	DeleteBranch(pcall);//
	pcall=NULL;
	CallPosList[addr1][num]=NULL;

}

void RemoveInvocation::PostTreatment(CSDGBase*pUSDGHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE> &FArry)
{//把全局变量的声明和初始化语句加入内联后的main函数中，删除除main函数外其它的PDG
	


	if(pUSDGHead==NULL||pUSDGHead->m_sType!="SDGHEAD")
		return;
	int i;
	for( i=0;i<pUSDGHead->GetRelateNum();i++)
	{
		if(pUSDGHead->GetNode(i)&&pUSDGHead->GetNode(i)->m_sType=="SDGENTRY"&&pUSDGHead->GetNode(i)->TCnt.GetSize()>0&&pUSDGHead->GetNode(i)->TCnt[0].name=="main")
		{
			break;
		}
	}
	if(i==pUSDGHead->GetRelateNum())
		return;
	CSDGBase*pmain=pUSDGHead->GetNode(i);
	

	int num=pUSDGHead->GetRelateNum()-1;
///把全局变量的声明语句加入内联后的main函数中
	for( i=num;i>=0;i--)
	{
		if(pUSDGHead->GetNode(i)&&pUSDGHead->GetNode(i)->m_sType=="#DECLARE")
		{
		//	CSDGBase*p2=pUSDGHead->GetNode(i);
			//CSDGDeclare*p1=(CSDGDeclare*)p2;
			//p1->no=-1;
		//	p2->no=-1;
			pmain->InsertNode(0,pUSDGHead->GetNode(i));
			pUSDGHead->GetNode(i)->SetFather(pmain); 
			if(pUSDGHead->GetNode(i)->TCnt.GetSize()>0 &&pUSDGHead->GetNode(i)->TCnt[0].addr>=0&&pUSDGHead->GetNode(i)->TCnt[0].addr<SArry.GetSize())
			{
				/*CString s;
				s.Format("addr=%d,layer before change=%d,after change=%d",pUSDGHead->GetNode(i)->TCnt[0].addr,SArry[pUSDGHead->GetNode(i)->TCnt[0].addr].layer,pmain->TCnt[0].addr+1);
			
				AfxMessageBox(s);*/
				SArry[pUSDGHead->GetNode(i)->TCnt[0].addr].layer=pmain->TCnt[0].addr+1;    

			}
			pUSDGHead->Del(i); 

		}

	}
////把全局变量的初始化语句加入内联后的main函数中
	for(i=0;i<pmain->GetRelateNum();i++)
	{
		if(pmain->GetNode(i)&&pmain->GetNode(i)->m_sType!="#DECLARE"  )
		{
			break;
		}
	}
	int pos=i;

	 num=pUSDGHead->GetRelateNum()-1;
	 for( i=num;i>=0;i--)
	{
		if(pUSDGHead->GetNode(i)&&pUSDGHead->GetNode(i)->m_sType=="ASSIGNMENT")
		{
			
			pmain->InsertNode(pos,pUSDGHead->GetNode(i));
			pUSDGHead->GetNode(i)->SetFather(pmain); 
			pUSDGHead->Del(i); 

		}

	}
	 ///删除除main函数外其它的PDG
	 num=pUSDGHead->GetRelateNum()-1;
	 for( i=num;i>=0;i--)
	{
		if(pUSDGHead->GetNode(i)&&pUSDGHead->GetNode(i)->m_sType=="SDGENTRY"&&pUSDGHead->GetNode(i)->TCnt.GetSize()>0&&pUSDGHead->GetNode(i)->TCnt[0].name!="main")
		{
			CSDGBase*pd=pUSDGHead->GetNode(i);
		//	DeleteBranch(pd);///wtt//5.10
			pd=NULL;
			
			pUSDGHead->Del(i); 

		}

	}

}


void RemoveInvocation::FunctionRename(CSDGBase*pUSDGHead,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//将含有递归函数调用的函数重命名以便于以后的匹配
	int i;
	for( i=0;i<FArry.GetSize();i++ )
	{
		if(FArry[i].name!="main")
		{
			CString s;
			s.Format("%d",i);
			FArry[i].name="Sub_Func"+s;
		}
	}
	for(i=0;i<SArry.GetSize();i++)
	{
		if(SArry[i].kind==CN_DFUNCTION&&SArry[i].addr>=0&&SArry[i].addr<FArry.GetSize())
		{
			SArry[i].name=FArry[SArry[i].addr].name ;
		}
	}
	for(i=0;i<SDGList.GetSize();i++ )
	{
		CSDGBase*p=SDGList[i];
		if(p&&p->TCnt.GetSize()>=0&&p->TCnt[0].addr>=0&&p->TCnt[0].addr<FArry.GetSize())
		p->TCnt[0].name=FArry[p->TCnt[0].addr].name;  
		for(int j=0;j<20;j++)
		{
			if(CallPosList[i][j])
			{
				p=CallPosList[i][j];
			//	p->m_pinfo->T.name=FArry[SArry[p->m_pinfo->T.addr ].addr].name; 
				if(p&&p->m_pinfo&&p->m_pinfo->T.addr >=0&&p->m_pinfo->T.addr <FArry.GetSize())
				p->m_pinfo->T.name=FArry[p->m_pinfo->T.addr ].name; 
			}
		}
	}
}	