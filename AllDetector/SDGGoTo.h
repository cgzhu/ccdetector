// SDGGoTo.h: interface for the CSDGGoTo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGGOTO_H__798BB245_198C_4F53_8749_28172F0D940D__INCLUDED_)
#define AFX_SDGGOTO_H__798BB245_198C_4F53_8749_28172F0D940D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGGoTo : public CSDGBase  
{
public:
	CSDGGoTo();
	virtual ~CSDGGoTo();
public:
	int GTPOS;

};

#endif // !defined(AFX_SDGGOTO_H__798BB245_198C_4F53_8749_28172F0D940D__INCLUDED_)
