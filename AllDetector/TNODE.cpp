// TNODE.cpp: implementation of the TNODE class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TNODE.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TNODE::TNODE()
{
	key = -1;       //  code which defined in "constdata.h"  
    addr = -1;      //  address in symbol table if it is variable of function
	paddr = -1;     //  pointer address
	deref = -1;//wtt//2006.3.21//指针脱引用*级别

	line = -1;      //  text line the token word in    
	value = -1;     //  value of const data 
	name = "";      //  name of token word if it is string
	srcLine = -1;  //  王倩添加-20080729-记录语句换行标准化后的语句相对行数
	srcWord = "";   //  王倩添加-20080729-记录源程序中token字对应的单词

	funcBegLine = -1;   //wq-20081220-所在函数的源程序起始行数
	funcEndLine = -1;   //wq-20081220-所在函数的源程序终止行数

	funcBgnRealLine = -1;
	funcEndRealLine = -1;
	cntxtLine = -1;
	dowhileLine = -1;

}

TNODE::~TNODE()
{

}

TNODE::TNODE(const TNODE &TN)
{
	if( TN.key <= 0 )
	{
		return;
	}
	key = TN.key;       //  code which defined in "constdata.h"  
    addr = TN.addr;      //  address in symbol table if it is variable of function
	paddr = TN.paddr;     //  pointer address
	deref = TN.deref;//wtt//2006.3.21//指针脱引用*级别

	line = TN.line;      //  text line the token word in    
	value = TN.value;     //  value of const data 
	name = TN.name;      //  name of token word if it is string
	srcLine = TN.srcLine;  //  王倩添加-20080729-记录语句换行标准化后的语句相对行数
	srcWord = TN.srcWord;   //  王倩添加-20080729-记录源程序中token字对应的单词

	funcBegLine = TN.funcBegLine;   //wq-20081220-所在函数的源程序起始行数
	funcEndLine = TN.funcEndLine;   //wq-20081220-所在函数的源程序终止行数

	funcBgnRealLine = TN.funcBgnRealLine;
	funcEndRealLine = TN.funcEndRealLine;
	cntxtLine = TN.cntxtLine;
	dowhileLine = TN.dowhileLine;

}

TNODE TNODE::operator =(const TNODE &TN)
{
	if( TN.key <= 0 )
	{
		return *this;
	}
	key = TN.key;       //  code which defined in "constdata.h"  
    addr = TN.addr;      //  address in symbol table if it is variable of function
	paddr = TN.paddr;     //  pointer address
	deref = TN.deref;//wtt//2006.3.21//指针脱引用*级别

	line = TN.line;      //  text line the token word in    
	value = TN.value;     //  value of const data 
	if( TN.name != "" )
	{
		name = TN.name;      //  name of token word if it is string
	}
	srcLine = TN.srcLine;  //  王倩添加-20080729-记录语句换行标准化后的语句相对行数
	srcWord = TN.srcWord;   //  王倩添加-20080729-记录源程序中token字对应的单词

	funcBegLine = TN.funcBegLine;   //wq-20081220-所在函数的源程序起始行数
	funcEndLine = TN.funcEndLine;   //wq-20081220-所在函数的源程序终止行数

	funcBgnRealLine = TN.funcBgnRealLine;
	funcEndRealLine = TN.funcEndRealLine;

	cntxtLine = TN.cntxtLine;
	dowhileLine = TN.dowhileLine;

	return *this;
}
