// Lexical.cpp: implementation of the CLexical class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Lexical.h"
#include "Out_TokenSeq.h"
#include "FormatTokenLines.h"
#include <iostream>
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CLexical::CLexical()
{
	VD.beg=0;
	VD.end=0;
	srcCode.RemoveAll();
	Obj_file_path = "";//wq-20090319
	out_file_path = "";//wq-20090402
}

CLexical::~CLexical()
{

}

/////////////////////////类使用接口"()"/////////////////////////////////
int CLexical::operator()(int nType,const CString cprogram,CArray<VIPCS,VIPCS> &VArry,CArray<CERROR,CERROR> &EArry,
						  CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry )
{
	// initialte data
	program=_T("");
	m_nTLen=0;
	m_nType=nType;
	m_pError=&EArry;
	TArry.RemoveAll();
	SArry.RemoveAll();
	FArry.RemoveAll();
	CString hprogram=_T("");
	CArray<CString,CString> Hfp_Array;//此数组记录所包含全部头(H)文件(File)的路径名(Path)
	CArray<TNODE,TNODE> INLB_TArry;   //此数组记录全部头文件的token串
	// end
	
	int Line_sum;
	if(cprogram.IsEmpty())
	{
		return 0;
	}
	else
	{
		program=cprogram + "      ";
		CString str_srcCode = cprogram;
		/*王倩添加-20080730-记录每行源代码于srcCode*/
		CList<CString,CString> T_List;
		TranToLine(str_srcCode,T_List);
		CString str_line;
	
		for( Line_sum = 0; Line_sum < T_List.GetCount(); Line_sum++ )
		{
			POSITION pos;
			pos=T_List.FindIndex(Line_sum);
			
			SRCLINE srcl;
			srcl.str_srcLine = T_List.GetAt(pos);
			srcl.srcline = Line_sum;
			srcl.codeline = 0;
			srcCode.Add(srcl);
		
		}
		/*end*/
			
	}

/*王倩删除-20080827-文件行数在上面记录每行源代码时计算出
	int Line_sum=0;//统计c文件的行数
	int len=program.GetLength();
	TCHAR C;
	for(int i=0; i<len; i++)
	{
		C=program.GetAt(i);
		if(C == '\n')
		{
			Line_sum++;
		}
    }*/
	//王倩添加-20080827-用于测试program的变化
	CStdioFile prog_File;
/*	if( prog_File.Open("D:\\program_Original.c",CFile::modeCreate | CFile::modeWrite) )
	{
		prog_File.WriteString(program);
		prog_File.Close();
	}*/
	
	INLBReplace(program,hprogram,Hfp_Array);//加入头文件的内容，同时删除注释行
/*	if( prog_File.Open("D:\\program_HeadAdded.c",CFile::modeCreate | CFile::modeWrite) )
	{//wq
		prog_File.WriteString(program);
		prog_File.Close();
	}
	if( prog_File.Open("D:\\program_HeadFile.c",CFile::modeCreate | CFile::modeWrite) )
	{//wq
		prog_File.WriteString(hprogram);
		prog_File.Close();
	}*/

	Del_String(program);//为便于分析，将程序中所有字符串替换为"s"
//	OutputMemoryRecord("CLexical","Del_String",out_file_path,1,"");
/*	if( prog_File.Open("D:\\program_StringReplaced.c",CFile::modeCreate | CFile::modeWrite) )
	{//wq
		prog_File.WriteString(program);
		prog_File.Close();
	}*/

	DefineReplace(hprogram,program);        //在CString中通过字符串替换实现宏定义
//	OutputMemoryRecord("CLexical","DefineReplace",out_file_path,1,"");
/*	if( prog_File.Open("D:\\program_MacroReplaced.c",CFile::modeCreate | CFile::modeWrite) )
	{//wq
		prog_File.WriteString(program);
		prog_File.Close();
	}*/

	//cout<<program<<endl;//测试！
	Pro_to_Token(hprogram,INLB_TArry,VArry,1);//将头文件的内容hprogram转换成token符存在数组INLB_TArry中
	Pro_to_Token(program,TArry,VArry,0);//将程序正文的内容program转换成token符存在数组TArry中
//	OutputMemoryRecord("CLexical","Pro_to_Token",out_file_path,1,"");
    StatementBoundAnalyze(TArry);//把没有{}的if/while/for/else/do语句加上{} 
//	OutputMemoryRecord("CLexical","StatementBoundAnalyze",out_file_path,1,"");
	Add_CH_Mark(TArry,INLB_TArry);      //LX加入头文件和主文件token串之间的间隔节点
	TypedefReplace(TArry);        //替换typedef定义的类型

	//wq-20090518
	DeleteHToken(TArry);  //去掉与头文件对应的token串
//	OutputMemoryRecord("CLexical","TypedefReplace",out_file_path,1,"");


	ResetToken(0,TArry.GetSize(),TArry);//将程序中结构体部分标准化，修改自wn程序
//	OutputMemoryRecord("CLexical","ResetToken",out_file_path,1,"");
	FormComplexOp(TArry);//把TArry中相邻两个可构成一个复合运算符的节点合并如: ->,&&,*=
//	OutputMemoryRecord("CLexical","FormComplexOp",out_file_path,1,"");
	ConstructSTable(TArry,SArry,FArry);//识别函数定义和函数声明，参数、局部变量、全局变量，存放在符号表中，并建立函数列表，记录函数间的调用关系
//	OutputMemoryRecord("CLexical","ConstructSTable",out_file_path,1,"");
//	OutputSymTable(SArry);
	if(SArry.GetSize()>0)
	{
		// assign the right addresses to every variable in token chain;	
		int layernum=FArry.GetSize();	
		for(int layer=1;layer<=layernum;layer++)
		{			
			VariableAssign(layer,FArry[layer-1],TArry,SArry,FArry);
		}		
	}
//	OutputMemoryRecord("CLexical","VariableAssign",out_file_path,1,"");

//	DeleteHToken(TArry);  //去掉与头文件对应的token串
//	OutputMemoryRecord("CLexical","DeleteHToken",out_file_path,1,"");
	ReSet_LineValue(TArry);//TArry中每个元素的line值加1
//	OutputMemoryRecord("CLexical","ReSet_LineValue",out_file_path,1,"");
//	OutputSrcFileAfterTokened(TArry);//wq20080925  //wq-20081223-delete
	return Line_sum;
}
/////////////////////////////CLexical类使用接口"()"///////////////////////////

//wq-20090522-重载operator()函数
int CLexical::operator()(int nType,const CString cprogram,CArray<VIPCS,VIPCS> &VArry,CArray<CERROR,CERROR> &EArry,
						  CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry,
						  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
						  const CString cFilePath,//wq-20090609
						  CArray<CH_FILE_INFO,CH_FILE_INFO> &filesReadInfo//wq-20090610
						  )
{
	// initialte data
	program=_T("");
	m_nTLen=0;
	m_nType=nType;
	m_pError=&EArry;
	TArry.RemoveAll();
	SArry.RemoveAll();
	FArry.RemoveAll();
	CString hprogram=_T("");
	CArray<CString,CString> Hfp_Array;//此数组记录所包含全部头(H)文件(File)的路径名(Path)
	CArray<TNODE,TNODE> INLB_TArry;   //此数组记录全部头文件的token串
	// end
	
	int Line_sum;
	if(cprogram.IsEmpty())
	{
		return 0;
	}
	else
	{
		program=cprogram + "      ";
		CString str_srcCode = cprogram;  
		/*王倩添加-20080730-记录每行源代码于srcCode*/
		CList<CString,CString> T_List;
		TranToLine(str_srcCode,T_List);			//POSITION pos = T_List.GetHeadPosition();for(int i=0; i<T_List.GetCount(); ++i) AfxMessageBox(T_List.GetNext(pos));//ww
		CString str_line;
	
		for( Line_sum = 0; Line_sum < T_List.GetCount(); Line_sum++ )
		{
			POSITION pos;
			pos=T_List.FindIndex(Line_sum);
			
			SRCLINE srcl;
			srcl.str_srcLine = T_List.GetAt(pos);
			srcl.srcline = Line_sum;
			srcl.codeline = 0;
			srcCode.Add(srcl);
		
		}
		/*end*/
			
	}

/*王倩删除-20080827-文件行数在上面记录每行源代码时计算出
	int Line_sum=0;//统计c文件的行数
	int len=program.GetLength();
	TCHAR C;
	for(int i=0; i<len; i++)
	{
		C=program.GetAt(i);
		if(C == '\n')
		{
			Line_sum++;
		}
    }*/
	//王倩添加-20080827-用于测试program的变化
	CStdioFile prog_File;

	CArray<HeaderFileElem,HeaderFileElem> tempHFileArry;
	CH_FILE_INFO fileInfo;

	CString tmpProg = program;
	int hfileSum = HFileSumCnt(tmpProg,cFilePath);
	long timeBeg = clock();
																									//	AfxMessageBox(program);	//ww 
	/*wq-20090610-完成了：头文件分析，C文件的宏替换*/
//	OutputMemoryRecord("CLexical","DealHeaderFiles",out_file_path,1,cFilePath);
	DealHeaderFiles(program,headerFilesArry,tempHFileArry,cFilePath,fileInfo);
	fileInfo.hFileSum = hfileSum;
	/***********************************************/												//AfxMessageBox(program);	//ww 

	HeaderFileElem tempHFileElm;//将所有包含头文件的符号表和typedef替换表分别统一存储到一个表里
	int i;
	for( i=0; i<tempHFileArry.GetSize(); i++ )
	{//将所有包含头文件的符号表和typedef替换表分别统一存储到一个表里
		tempHFileElm.symbolTable.Append(tempHFileArry[i].symbolTable);
		tempHFileElm.typedefRcdArry.Append(tempHFileArry[i].typedefRcdArry);
	}


//	OutputMemoryRecord("CLexical","DefineReplace",out_file_path,1,"");

	//cout<<program<<endl;//测试！
//	Pro_to_Token(hprogram,INLB_TArry,VArry,1);//将头文件的内容hprogram转换成token符存在数组INLB_TArry中
																										//AfxMessageBox(program);	//ww 
	/*##################################################################
	###################################################################*/
	Pro_to_Token(program,TArry,VArry,0);//将程序正文的内容program转换成token符存在数组TArry中
/*	for(i=0; i<TArry.GetSize(); ++i)
	{
		AfxMessageBox(TArry.GetAt(i).name);
	}
*/	
	OutputMemoryRecord("CLexical","Pro_to_Token",out_file_path,1,"");
    StatementBoundAnalyze(TArry);//把没有{}的if/while/for/else/do语句加上{} 
//	OutputMemoryRecord("CLexical","StatementBoundAnalyze",out_file_path,1,"");
//	Add_CH_Mark(TArry,INLB_TArry);      //LX加入头文件和主文件token串之间的间隔节点
	//TypedefReplace(TArry);        //替换typedef定义的类型

	//wq-20090518
//	DeleteHToken(TArry);  //去掉与头文件对应的token串


	ResetToken(0,TArry.GetSize(),TArry);//将程序中结构体部分标准化，修改自wn程序//wq-20090610-修改，加入了typedef定义与结构体定义分离
//	OutputMemoryRecord("CLexical","ResetToken",out_file_path,1,"");

	FormComplexOp(TArry);//把TArry中相邻两个可构成一个复合运算符的节点合并如: ->,&&,*=
//	OutputMemoryRecord("CLexical","FormComplexOp",out_file_path,1,"");

	NewTypedefReplace(TArry,tempHFileElm);//wq-20090610-C文件token流的typedef替换
//	OutputMemoryRecord("CLexical","NewTypedefReplace",out_file_path,1,"");

	SArry.Copy(tempHFileElm.symbolTable);
	NewConstructSymTable(TArry,SArry,FArry);//识别函数定义和函数声明，参数、局部变量、全局变量，存放在符号表中，并建立函数列表，记录函数间的调用关系
//	OutputMemoryRecord("CLexical","NewConstructSymTable",out_file_path,1,"");
//	OutputSymTable(SArry);

	if(SArry.GetSize()>0)
	{
		// assign the right addresses to every variable in token chain;	
		int layernum=FArry.GetSize();	
		for(int layer=1;layer<=layernum;layer++)
		{			
			VariableAssign(layer,FArry[layer-1],TArry,SArry,FArry);
		}		
	}
//	OutputMemoryRecord("CLexical","VariableAssign",out_file_path,1,"");

//	DeleteHToken(TArry);  //去掉与头文件对应的token串
//	OutputMemoryRecord("CLexical","DeleteHToken",out_file_path,1,"");
	ReSet_LineValue(TArry);//TArry中每个元素的line值加2(wq-20090610-从1改成2)
//	OutputMemoryRecord("CLexical","ReSet_LineValue",out_file_path,1,"");
//	OutputSrcFileAfterTokened(TArry);//wq20080925  //wq-20081223-delete

	long timeEnd = clock();
	fileInfo.cfilePrcssTime = float(timeEnd-timeBeg)/CLOCKS_PER_SEC;
	filesReadInfo.Add(fileInfo);

	return Line_sum;
}
/////////////////////////////CLexical类使用接口"()"///////////////////////////


bool CLexical::TranToLine(CString program, CList<CString,CString> &T_List)
{
	
	int i=0;
	int prglen=0;
	int len=program.GetLength();
	TCHAR C;
	program+="   ";				//	AfxMessageBox(program);
	while(i<len)
	{
		C=program.GetAt(i);
		i++;

		CString st("");
		while(C!='\n'&&i<=len)
		{ 
			if(C!='\r')
			{
				st+=C;
			}
			C=program.GetAt(i);
			i++;
		} 
		T_List.InsertAfter(T_List.GetTailPosition(),st);
		prglen+=st.GetLength();
    }

						/*ww	POSITION pos = T_List.GetHeadPosition();
								for(i=0; i<T_List.GetCount(); ++i)
								{
									AfxMessageBox(T_List.GetNext(pos));
								}
							*/
	if(prglen<8 || T_List.GetCount()<=0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void CLexical::InvalidateLetter(int nline)
{
	//
}

int CLexical::WhichKeyWord(const CString &str)
{
	int i;
	for(i=1;i<=KEYNUM;i++)
	{
		if(C_KEYWORD[i]==str)
		{
			return i;
		}
	}

	if(str=="FILE")///wn
	{
		return CN_FILE;
	}
	if(str=="NULL" || str=="null")
	{ 
		return CN_NULL;
	}

	if(str=="include")
	{
		return CN_INLIB;
	}

	if(str=="define")
	{
		return CN_DEFINE;
	}

	for(i=0;i<=LIBNUM;i++)
	{
		if(str==C_INCLUDE[i])
		{
			return CN_DEFINE+1+i;
		}
	}
	return 0;
}

int CLexical::WhichBFunction(const CString str)
{
	for(int i=0;i<=BFUNNUM;i++)
	{
		if(C_FUNCTION[i]==str)
		{
			return i;
		}
	}

	return -1;
}

void CLexical::IsKuoHaoOK(const CArray<TNODE,TNODE> &TArry)
{

}
//////////////////////

//////////////////////
void CLexical::FormComplexOp(CArray<TNODE,TNODE> &TArry)
{
	// form complex operators in token list
	int nIdx=TArry.GetSize()-1;
	while(nIdx>=0)
	{
		switch(TArry[nIdx].key)
		{
			/////////////////////LX////////////////////////////////////
			case CN_INT:
				if(nIdx>0 && TArry[nIdx-1].key==CN_SHORT)//"? short int"
				{
					if((nIdx-1)>0 && TArry[nIdx-2].key==CN_UNSIGNED)//"unsigned short int"
					{
						TArry.RemoveAt(nIdx);
						TArry.RemoveAt(nIdx-1);
						nIdx=nIdx-2;
					}
					else
					{
						if((nIdx-1)>0 && TArry[nIdx-2].key==CN_SIGNED)//"signed short int"
						{
							TArry.RemoveAt(nIdx-1);
							TArry.RemoveAt(nIdx-2);
							nIdx=nIdx-2;
						}
						else										 //"short int"
						{
							TArry.RemoveAt(nIdx-1);
							nIdx--;
						}
					}
				}
				else
				{
					if(nIdx>0 && TArry[nIdx-1].key==CN_LONG)         //"? long int"
					{
						TArry.RemoveAt(nIdx);
						nIdx--;
						if(nIdx>0 && TArry[nIdx-1].key==CN_UNSIGNED)//"unsigned long int"
						{
							TArry.RemoveAt(nIdx);
							nIdx--;
						}
					}
					else
					{
						if(nIdx>0 && TArry[nIdx-1].key==CN_UNSIGNED)    //"unsigned int"
						{
							TArry.RemoveAt(nIdx);
							nIdx--;
						}
						else
						{
							if(nIdx>0 && TArry[nIdx-1].key==CN_SIGNED)   //"signed int"
							{
								TArry.RemoveAt(nIdx-1);
								nIdx--;
							}
						}
					}
				}
				break;
			case CN_LONG:
			case CN_SHORT:
				if(nIdx>0 && TArry[nIdx-1].key==CN_UNSIGNED)
				{
					TArry.RemoveAt(nIdx);
					nIdx--;
				}
				break;


	}			

		nIdx--;
	}
}
/*************************************construct symbol table************************************/


void CLexical::ConstructSTable( CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	if(TArry.GetSize()<=0)
	{
		return;
	}
    //  construct the symbol table
	PreConstructTable(0,TArry.GetSize(),TArry,SArry);///wn

	int   ix=0;
    int   nlayer=0;
	SNODE SD;
	FNODE FD;
	CString stemp;
	int nflower;//wn
    m_nTLen=TArry.GetSize();///wn

	while( ix>=0 && ix<TArry.GetSize() )
	{
		CString str_key = TArry[ix].srcWord;
		int key = TArry[ix].key;
		//cout<<ix<<endl;
		if( ( TArry[ix].key==CN_STRUCT||TArry[ix].key==CN_UNION )
			&&( (ix+1<TArry.GetSize() && TArry[ix+1].key==CN_STRUCTTYPE
			     && ix+2<TArry.GetSize() && TArry[ix+2].key==CN_LFLOWER)
			   ||(ix+1 < TArry.GetSize() && TArry[ix+1].key==CN_LFLOWER) )
		   )
		{
			nflower=1;
			ix=ix+3;
			for(;nflower>0;ix++)
			{
				if(ix>=TArry.GetSize())
					break;//LX修改!!!
				if(TArry[ix].key==CN_LFLOWER)
					nflower++;
				if(TArry[ix].key==CN_RFLOWER)
					nflower--;
			}
			ix++;
			continue;
		}///wn//跳过结构类型定义

		if( ((ix+1)<m_nTLen) && 
			 ( ix>=0 && ix<TArry.GetSize() && (TArry[ix].key==CN_VARIABLE || TArry[ix].key==CN_MAIN) )
			 && (ix+1<TArry.GetSize() && TArry[ix+1].key==CN_LCIRCLE) )// definition of function
		{
			// initialization of the node "SD".
			SD.addr=-1;
			SD.arrdim=0;
			SD.sv=-1;//wn
			SD.mv=-2;//wn
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			SD.kind=-1;
			SD.type=-1;
			SD.name=TArry[ix].name; 
			SD.kind=CN_DFUNCTION;	 
			
			// initializaton of the node "FD".
			FD.bbeg=-1;
			FD.bend=-1;
			FD.fid=-1;
			FD.pnum=0;
			FD.rkind=-1;
			FD.rtype=-1;
			FD.name=TArry[ix].name;
			//////////////////////wtt/////////////
			FD.callednum =0;
			FD.calleenum=0;
			for(int j=0;j<20;j++)
			{
				FD.calleelist[j]=-1;
				FD.plist[j]=-1;
			}
	        /////////////////////////wtt/////////////////////			
			//  the layer number of the function
            nlayer++;
			SD.layer=nlayer;
			TArry[ix].key=CN_DFUNCTION;
			TArry[ix].addr=FArry.GetSize();
			SD.taddr=ix;///wn//9.12

			ReturnTandK(ix,SD,FD,TArry);// get return type and kind
	
			int nBeg=ix+1;// nBeg point to the "("	
			ix=ix+2;// ix point to token next to "("    (begin of the parameter list in the program).		
			
			int nRec=1;		
			if( ix<TArry.GetSize() && TArry[ix].key==CN_RCIRCLE)//  function without parameter
			{
				nRec=0;
				ix++;
			}

			while( (ix<m_nTLen) && (nRec!=0) )
			{
				if( ix<TArry.GetSize() )
				{
					if(TArry[ix].key==CN_LCIRCLE)
					{
						nRec++;
					}
					if(TArry[ix].key==CN_RCIRCLE)
					{
						nRec--;
					}
				}
				ix++;
			}
			int nEnd=ix-1;//  nEnd point to the ")"   (end of the parameter list in the program).

			//////////////WTT////////////////////////////////////////
			if( ix<TArry.GetSize() && TArry[ix].key==CN_LFLOWER )//函数定义
			{
                FD.pbeg=nBeg;
				FD.pend=nEnd;	
			   	FD.bbeg=ix;
                int nBrace=1;
				ix++;

				while((ix<TArry.GetSize()) && (nBrace!=0))
				{
					if(TArry[ix].key==CN_LFLOWER)
					{
						nBrace++;
					}

					if(TArry[ix].key==CN_RFLOWER)
					{
						nBrace--;
					}
					ix++;
				}

				if(ix>m_nTLen)
				{
					// syntax error
					FD.bend=TArry.GetSize();			
				}
				// find the function end label "}".
				FD.bend=ix-1;
				ix--;
			}
			////////////////////wtt/////////////////////////////////////////
		
			///////wq-20081125-修改识别函数声明-形如："static void clean_child_exit(int code)__attribute__ ((noreturn));"
			else //if(ix<TArry.GetSize()&&TArry[ix].key==CN_LINE)//函数声明
			{	// the declaration of the function(not definition).
				// edit here when extend system 
				int lflw = 0, rflw = 0;
				while( ix>0 && ix<TArry.GetSize() && TArry[ix].line == TArry[ix-1].line)
				{
					if( TArry[ix].key == CN_LFLOWER )
					{
						lflw++;
					}
					else if( TArry[ix].key == CN_RFLOWER )
					{
						rflw++;
					}
					if( TArry[ix].key == CN_LINE && lflw == 0 && rflw == 0)
//						|| (ix == TArry.GetSize()-1 && TArry[ix].key == CN_LINE))
					{
						nlayer--;
						ix++;
						int i;
						for( i=nBeg-1;i<ix-1;i++)
						{
							if( i<TArry.GetSize() && (TArry[i].key==CN_VARIABLE || TArry[i].key==CN_DFUNCTION))
							{
								TArry[i].key=CN_CSTRING;
								break;
							}					
						}
						if(i<TArry.GetSize() && TArry[i].key==CN_CSTRING)
						{
							break;
						}
					}
					ix++;
				}
				
				continue;
			}
			///////////////////////wtt/////////////////////////////
/*			else//错误的函数声明形式
			{						
				return;	
			}*///wq-20081125
		
			/////////////////////////////////////////////wtt/////////////////////////
			//	if(TArry[nEnd+1].key!=CN_LINE)//wtt
			if(nEnd+1<TArry.GetSize() && TArry[nEnd+1].key==CN_LFLOWER)//wtt对函数定义进行参数和局部变量处理
			{
                // analyze the function body to find the definitions of local variables;  
               	SD.addr=FArry.GetSize(); 
				if(SD.type==CN_FLOAT)
				{
					SD.type=CN_DOUBLE;
				}
				SArry.Add(SD);
				ParaAnalysis(nlayer,FD,TArry,SArry);
				localVariables(nlayer,FD,TArry,SArry);
				FD.fid=FArry.GetSize(); 				
				if(FD.rtype==CN_FLOAT)
				{
					FD.rtype=CN_DOUBLE;
				}
				FArry.Add(FD);
			}
		}
        	
		if( (ix+1<TArry.GetSize()&&TArry[ix].key==CN_VARIABLE) && 
			( (TArry[ix+1].key==CN_LINE) || (TArry[ix+1].key==CN_DOU)||
			  (TArry[ix+1].key==CN_EQUAL) || (TArry[ix+1].key==CN_LREC)) )
		{
			// global variable analysis
			SD.addr=-1;
			SD.arrdim=0;
			
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			TArry[ix].addr=SArry.GetSize();
			SD.kind=CN_VARIABLE;
			SD.taddr=ix;     ///wn//9.12
			SD.type=-1;
			SD.name=TArry[ix].name; 
			SD.layer=0; 
			SD.addr =0;//wtt 全局变量默认赋值为0
            SD.value =0;//wtt 全局变量默认赋值为0
			GlobalVariables(ix,SD,TArry,SArry);
			key = TArry[ix].key;
		}
		ix++;
	}

}

void CLexical::GlobalVariables(int &ix,SNODE &SD,const CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{

    if(ix<0||ix>TArry.GetSize())
	{
		return;
	}
	int i;

	i=ix-1;
	while( (i>=0) && (!(IsVariableType(i,TArry))) )
	{
		i--;
	}
	
	if(i<0)
	{//error
		//SD.type=CN_INT;//wtt
		return;//wtt
	}
	else
	{
		if(i>0 && TArry[i-1].key==CN_UNSIGNED)
		{
			SD.type=CN_UNSIGNED; 
		}
		else
		{
			SD.type=TArry[i].key; 
			if(i>=1&&TArry[i].key==CN_STRUCTTYPE&&(TArry[i-1].key==CN_STRUCT||TArry[i-1].key==CN_UNION))
			{
				SD.sv=FindPosition(TArry[i].name,TArry[i-1].key,SArry);///wn
				
			}
			else 
				SD.sv=-1;
		}
	}

    if(ix>0 && TArry[ix-1].key==CN_MULTI)
	{
		SD.kind=CN_POINTER;
		SD.value=1;
	/*	if(ix<TArry.GetSize()-3 && TArry[ix+1].key==CN_RCIRCLE && 
		   TArry[ix+2].key==CN_LREC)
		{
			SD.value+=1;
			if(TArry[ix+3].key==CN_CINT)
			{
				SD.addr=TArry[ix+3].addr;  
			}
		}*///wtt

	}
	if(ix+1<TArry.GetSize()&&(TArry[ix+1].key==CN_LREC))
	{
		if(SD.kind==CN_POINTER)
		{
			//SD.kind=CN_ARRAY; 
			SD.kind=CN_ARRPTR;
		}
		else
		{
			SD.kind=CN_ARRAY; 
		}

		ix++;
		ArrayDim(ix,SD,TArry);
	}
    /////////////////////////////wtt////////给全局变量赋值//////12.29///////////////////
    if(ix+2<TArry.GetSize()&&(SD.kind==CN_VARIABLE)&&(TArry[ix+1].key==CN_EQUAL))
	{
		if(TArry[ix+2].key==CN_CINT||TArry[ix+2].key==CN_CLONG||TArry[ix+2].key==CN_CCHAR)
		{
			SD.addr=TArry[ix+2].addr;//整形数值和字符常数记录在addr中 
		}
		else if(TArry[ix+2].key==CN_CFLOAT||TArry[ix+2].key==CN_CDOUBLE)
		{
			SD.value=TArry[ix+2].value;
		}
	
	}
	////////////////////////////////wtt/////////////////////////////////////////////
	if(!VariableExisted(SD,SArry))
	{
		///////////wtt///////////2005.10.18//将float转换成double///
		if(SD.type==CN_FLOAT)
		{
			SD.type=CN_DOUBLE;
		}
		//////////////////////////////////////
	
		SD.mv=-2;///wn
		SArry.Add(SD);
		
		
	}
	else
	{
		// error;
		return;
	}

}

void CLexical::localVariables(const int nlayer, const FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{
	SNODE SD;
    int ix=FD.bbeg; //##
    int beforvariable;
	int nflower;//wn
	if(ix>=(FD.bend-1))
	{
		// empty function
		return;
	}
	
//	CString s;
//	s.Format("<%d,%d> layer=%d",FD.bbeg,FD.bend,nlayer);
//	AfxMessageBox(FD.name+s);
	
	while(ix<FD.bend&&ix>=0&&ix<TArry.GetSize())
	{
		if( ix>=1 && ix+1<TArry.GetSize()
			&& (TArry[ix-1].key==CN_STRUCT||TArry[ix-1].key==CN_UNION) 
			&& TArry[ix].key==CN_STRUCTTYPE &&TArry[ix+1].key==CN_LFLOWER)
		{ /////跳过结构类型定义////修改layer域
			//AfxMessageBox("");			
			nflower=1;
			ix=ix+2;
			int beg=ix;
		
			while(ix<TArry.GetSize()&&nflower!=0)
			{
				if(TArry[ix].key==CN_LFLOWER)
					nflower++;
				if(TArry[ix].key==CN_RFLOWER)
					nflower--;
				ix++;
			}
			int end=ix;
			for(int ii=0;ii<SArry.GetSize();ii++)
			{/////修改结构类型定义时符号表的layer域
				if(SArry[ii].taddr>beg&&SArry[ii].taddr<end)
					SArry[ii].layer=nlayer;
			}
			ix++;
		}
		if( ix-1>=0&&ix+1<TArry.GetSize()&&(TArry[ix-1].key!=CN_LCIRCLE || ix-1==FD.pbeg)&& IsVariableType(ix,TArry) && TArry[ix+1].key!=CN_RCIRCLE )
	
		{
			// initialization of the SNODE "SD"
			SD.type=TArry[ix].key;
			if(ix>=1&&TArry[ix].key==CN_STRUCTTYPE&&(TArry[ix-1].key==CN_STRUCT||TArry[ix-1].key==CN_UNION))
			{
				SD.sv=FindPosition(TArry[ix].name,TArry[ix-1].key,SArry);///wn
			
			}
			else 
			{	
				SD.sv=-1;
			}
			SD.mv=-2;///wn			
			SD.layer=nlayer; 
			
			///////////wtt///////////2005.10.18//将float转换成double///
			if(SD.type==CN_FLOAT)
			{
				SD.type=CN_DOUBLE;
			}
			//////////////////////////////////////
			SD.name=_T("");
			
			//CString s;
			//s.Format("line=%d,key=%d,nanme=%s",TArry[ix].line,TArry[ix].key,TArry[ix].name);   
            //AfxMessageBox(s);
             
			ix++;

			while(ix>=1&&ix<TArry.GetSize()&&(!IsVariableType(ix,TArry))&&(TArry[ix].key!=CN_LINE)&&(TArry[ix].line==TArry[ix-1].line))
			{
				
				if(TArry[ix].key==CN_VARIABLE || TArry[ix].key==CN_MULTI)
				{
					 beforvariable=ix-1;
					if(ix<FD.bend && ix+1<TArry.GetSize()&&TArry[ix+1].key==CN_LCIRCLE)
					{
						//declare of function
						int nrect=1;
						int line=TArry[ix].line; 
						int pos=ix;

						ix+=2;

						while(ix<FD.bend && ix>=0&&ix<TArry.GetSize()&&TArry[ix].line<=line && nrect!=0)
						{
							if(TArry[ix].key==CN_LCIRCLE)
							{
								nrect++;
							}
							if(TArry[ix].key==CN_RCIRCLE)
							{
								nrect--;
							}

							ix++;
						}

						if(ix>=FD.bend)
						{
							return;
						}

						if(TArry[ix].line>line)
						{
							break;
						}

						ix--;

						for(;pos<ix;pos++)
						{
							if(TArry[pos].key==CN_VARIABLE)
							{///[ ]内为变量时显示“字符串”
								TArry[pos].key=CN_CSTRING;
							} 
						}
						continue;
					}
				
					if(TArry[ix].key==CN_MULTI)
					{
						SD.kind=CN_POINTER;
						SD.value=1;
						
						ix++;

								//////////////wtt//////////////////
                        if(TArry[ix].key!=CN_VARIABLE)
						{	//error	
												
							return ;		
						}
					
						//////////////////////wtt////////
					}
					else
					{
						SD.kind=CN_VARIABLE; 
					}

					if(TArry[ix].key==CN_VARIABLE)
					{

						SD.addr=-1;
						SD.arrdim=0;
						for(int i=0;i<4;i++)
						{
							SD.arrsize[i]=0; 
						}
						SD.name=TArry[ix].name;

						ix++;
						
						if(ix<FD.bend && TArry[ix].key==CN_LREC)
						{
							if(SD.kind==CN_POINTER)
							{
								SD.kind=CN_ARRPTR; 
							}
							else
							{
								SD.kind=CN_ARRAY; 
							}
							ArrayDim(ix,SD,TArry);
							ix--;
							
						}

						
					
                        if(!VariableExisted(SD,SArry))
						{		
							if(beforvariable>=0&&beforvariable<TArry.GetSize()&&TArry[beforvariable].key==CN_DOU||IsVariableType(beforvariable,TArry))
							{////wn
								SD.taddr=beforvariable+1;
							//	if(TArry[SD.taddr-1].key!=CN_DOT)
								
									SArry.Add(SD);
							}
							else
								{
														
								return;
								}

								

						}
						else
						{
							// error;
							if(beforvariable>=0&&beforvariable<TArry.GetSize()&&TArry[beforvariable].key!=CN_EQUAL&&VariableExisted(SD,SArry))
							{			
								return;
							}
						
						}
					}
				}
/////////////////////////////////////////wtt//////////////////////////////////

				if(!IsVariableType(ix,TArry))
				{
					ix++;
				}

			}// while((!IsVariableType(ix,TArry))&...
		

			if( ix>0 && ix<TArry.GetSize() && TArry[ix].line==TArry[ix-1].line || IsVariableType(ix,TArry))
			{
				ix--;
			}

		}// if(TArry[ix-1].key!=CN_LCIRCLE && IsVar...
        
		ix++;
	}
	

}

void CLexical::ParaAnalysis(const int nlayer,FNODE &FD,const CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{
	SNODE SD;
    int ix=FD.pbeg;  
    
	if(ix>=(FD.pend-1))
	{
		// function without parameters
		return;
	}

	FD.pnum=0; 
	
	while(ix<FD.pend&&ix<TArry.GetSize())
	{
		if( (ix+1<FD.bend) &&(IsVariableType(ix,TArry)))
		{
			// initialization of the SNODE "SD"
			SD.type=TArry[ix].key;
			SD.addr=-1;
			SD.arrdim=0;

			if(ix>=1&&TArry[ix].key==CN_STRUCTTYPE&&(TArry[ix-1].key==CN_STRUCT||TArry[ix-1].key==CN_UNION))
			{
				SD.sv=FindPosition(TArry[ix].name,TArry[ix-1].key,SArry);///wn
				
			}
			else 
				SD.sv=-1;

			SD.mv=-2;///wn
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			SD.kind=-1;
			
		
			SD.name=_T("");
			
			// point to the token next to the type. 
			ix++;
          //  while(ix<TArry.GetSize() && TArry[ix].key!=CN_RCIRCLE && 
			//	  TArry[ix].key!=CN_LINE && ix<FD.pend )
		//	{
		/////wn
			if( ix<TArry.GetSize() && TArry[ix].key==CN_MULTI)
				{
					// the variable must be a pointer.
					SD.kind=CN_POINTER;
					ix++;
					//////////////wtt//////////////////
                    if( ix<TArry.GetSize() && TArry[ix].key!=CN_VARIABLE)
					{	//error	
												
							return ;		
					}
					
					//////////////////////wtt////////
				}
				else
				{
					// common variable or array.
					SD.kind=CN_VARIABLE; 
				}

				if(TArry[ix].key==CN_VARIABLE)
				{
					SD.taddr=ix;//wn
					SD.name=TArry[ix].name;	
					//AfxMessageBox(SD.name);
					
					ix++;

					if( ix<TArry.GetSize() && TArry[ix].key==CN_LREC)
					{
						ArrayDim(ix,SD,TArry);
						if(SD.kind==CN_POINTER)
						{
							SD.kind=CN_ARRPTR;
						}
						else
						{
							SD.kind=CN_ARRAY; 
						}
					} 

					SD.layer=nlayer; 
					FD.plist[FD.pnum]=SArry.GetSize();
					FD.pnum++;
				
					if(!VariableExisted(SD,SArry))
					{
						if(SD.type==CN_FLOAT)
						{
							SD.type=CN_DOUBLE;
						}
						
						SArry.Add(SD);
					}
					else
					{
						// error: variable has existed;
						SD.addr=-1;
						SD.arrdim=0;
						SD.kind=CN_VARIABLE;
						SD.type=CN_INT;
						SArry.Add(SD);
					}
				}
			//	ix++;
		//	}
		//	ix--;

		}// if( (ix+1<FD.bend) &&(IsVariableType(ix,TArry)))

		ix++;
	} 
 

}


bool CLexical::IsVariableType(int &ix, const CArray<TNODE,TNODE> &TArry)
{
	if(ix>=TArry.GetSize()||ix<0)
		return false;

	TNODE TD=TArry[ix];

	switch(TD.key)
	{
	case CN_INT:
		break;
	case CN_CHAR:
		break;
	case CN_FLOAT:
		break;
	case CN_DOUBLE:
		break;
	case CN_LONG:
		if(ix<m_nTLen)
		{
			ix++;
			if(ix<TArry.GetSize()&&TArry[ix].key!=CN_INT)
			{
				ix--;
			}
		}
		break;
	case CN_SHORT:
		break;
	case CN_UNSIGNED:
		if(ix<m_nTLen)
		{
			ix++;
			if(ix<TArry.GetSize()&&TArry[ix].key!=CN_INT && TArry[ix].key!=CN_CHAR)
			{
				ix--;
			}
		}
		break;

	case CN_STRUCTTYPE:///wn
		if(ix+1<TArry.GetSize() && TArry[ix+1].key==CN_VARIABLE||TArry[ix+1].key==CN_MULTI)
		break;
		else
			return false;
	case CN_FILE:     //wn
		break;
	default:
		return false;
	}
	return true;
}

void CLexical::ArrayDim(int &ix,SNODE &SD,const CArray<TNODE,TNODE> &TArry)
{
	// ix: the index of token "[" in the token chain.
    if(ix>=TArry.GetSize()||ix<0)
		return ;
	SD.arrdim=0;

	
	while(ix<m_nTLen && (TArry[ix].key==CN_LREC || TArry[ix].key==CN_RREC || TArry[ix].key==CN_CINT))
	{
		if(TArry[ix].key==CN_LREC)
		{
		
			SD.arrdim++; 
			ix++;
		
		
			if(ix<TArry.GetSize()&&TArry[ix].key==CN_CINT)
			{
				if(SD.arrdim<=4)
				{
					SD.arrsize[SD.arrdim-1]=TArry[ix].addr;
				}
			}
			else
			{
				if(ix<TArry.GetSize()&&TArry[ix].key==CN_RREC)
				{
				//	ix--;///wtt3.1//
					SD.arrsize[SD.arrdim-1]=-1;
				}
				else
				{ 
					// error
				}
			}
		}

		ix++;
	}
	

}

bool CLexical::VariableExisted(const SNODE SD,const CArray<SNODE,SNODE> &SArry)
{
//	CString s;
//	s.Format("layer=%d",SD.layer);
//	AfxMessageBox(SD.name+s);
	int i=0;
	while(i>=0&&i<SArry.GetSize())
	{
		////wn
		
		if(SD.layer==SArry[i].layer && SD.name==SArry[i].name&&SD.sv==SArry[i].sv&&SD.mv==SArry[i].mv)
			return true;
		i++;
	}
	return false;

}

void CLexical::ReturnTandK(const int ix,SNODE &SD,FNODE &FD,const CArray<TNODE,TNODE> &TArry)
{
	int i=ix-1;
    	
	if(i>=0 && i<TArry.GetSize() && TArry[i].key==CN_VOID)
	{
		SD.type=CN_VOID;
		FD.rtype=CN_VOID;
		FD.rkind=CN_VOID;		
		return;
	}//void
	
	if(i>0 && i<TArry.GetSize() && TArry[i].key==CN_MULTI)
	{
		FD.rkind=CN_POINTER;
		i--;
	}//指针
	else
	{
		FD.rkind=CN_VARIABLE;
	}//变量
    
	if(i>=0 && i<TArry.GetSize())
	{
		TNODE T=TArry[i];	
		if(T.key==CN_INT || T.key==CN_CHAR || T.key==CN_FLOAT || T.key==CN_DOUBLE || T.key==CN_LONG||T.key==CN_FILE)
		{
			SD.type=T.key;
			FD.rtype=T.key;		
		}
		else
		{
			// error: function without return type;
			FD.rtype=CN_VOID;
			FD.rkind=CN_VOID;
		}
	}
	else
	{
		// error: function without return type;
		FD.rtype=CN_VOID;
		FD.rkind=CN_VOID;
	}		
	
}

void CLexical::VariableAssign(const int nlayer,  FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry, CArray<FNODE,FNODE> &FArry)
{
	// add the address information to the token
	int idx=FD.pbeg+1;
	int i;
	while(idx<FD.bend&&idx<TArry.GetSize())
	{
		if(TArry[idx].key==CN_VARIABLE)
		{
			CString word = TArry[idx].srcWord;

			if(idx+1<TArry.GetSize()&&TArry[idx+1].key!=CN_LCIRCLE)
			{
				// local variables
			
				for(i=0;i<SArry.GetSize();i++ )
				{
//					if(SArry[i].name==TArry[idx].name && SArry[i].layer==nlayer && SArry[i].kind!=CN_DFUNCTION )

					//wq-20090511
					if(SArry[i].name==TArry[idx].name)
					{	
						if(SArry[i].layer==nlayer)
						{
							if(SArry[i].kind!=CN_DFUNCTION)
							{						
								if(idx>1&&TArry[idx-1].key!=CN_DOT)/////wn
								{
									TArry[idx].addr=i;
									
									break;
								}
								else if(idx>1&&TArry[idx-1].key==CN_DOT&&SArry[i].sv>=0&&SArry[i].mv>=-1)
								{
									TArry[idx].addr=i;///设置符号表中变量的addr	
									break;
								}
							}
						}
						
					}
				}

				if(i>=SArry.GetSize())
				{
					// global variable 
					for(i=0;i<SArry.GetSize();i++ )
					{
						if(SArry[i].name==TArry[idx].name && SArry[i].layer==0)
						{
							if(idx>1&&TArry[idx-1].key!=CN_DOT)////wn
							{
								TArry[idx].addr=i;
								break;
							}
							else if(idx>1&&TArry[idx-1].key==CN_DOT&&SArry[i].sv>=0&&SArry[i].mv>=-1)
							{
								TArry[idx].addr=i;///设置token表中变量的addr	
								break;
							}	
						}
					}

					// variable undefined
					if(i>=SArry.GetSize())
					{
						/////////////////////////wtt/9.27///stdin常数//
						if(idx>=0&&idx<TArry.GetSize()&&TArry[idx].name=="stdin")
						{	TArry[idx].key=CN_CINT;
						    TArry[idx].addr=-10000;
						}  
						else
						{
						////////////////////////
						SNODE SD;
						SD.name=TArry[idx].name;
						SD.taddr=idx;/////wn//9.12
						SD.addr=-1; 
						SD.layer=nlayer;
						SD.type=CN_INT;
						SD.arrdim=0;
						SD.sv=-1;//////wn//9.12
						SD.mv=-2;///wn
						for(int k=0;k<4;k++)
						{
							SD.arrsize[k]=0; 
						}
						TArry[idx].addr=SArry.GetSize();  
						if(TArry[idx+1].key==CN_LREC)
						{
							SD.kind=CN_ARRAY;
							SD.arrdim=1; 
						}
						else
						{
							SD.kind=CN_VARIABLE; 
						}
						SArry.Add(SD);
						ER.ID=1;
						ER.line=TArry[idx].line;
						m_pError->Add(ER); 
					}
					}
				}
			//	CString s;
			//	s.Format("%d",TArry[idx].addr);
			//	AfxMessageBox(TArry[idx].name+s);
			}
			else
			{
				// function invoking
				for(i=0;i<FArry.GetSize();i++)
				{
					if(TArry[idx].name==FArry[i].name)
					{
						TArry[idx].addr=FArry[i].fid;
						TArry[idx].key=CN_DFUNCTION;
/////////////////////////////////wtt////////////////////////
				/*		int j=0;
						while(j<20&&FD.calleelist[j]!=-1)
							j++;
						if(j<20)
						{
						FD.calleelist[j]=FArry[i].fid;
						FD.calleenum++;
						FArry[i].callednum++;
						}*/
////////////////////////////////wtt/////////////////////
						break;//wtt
					}		
				}
				if(i>=FArry.GetSize())
				{//error
					return;
				}
			}
		}
		idx++;
	}
}
/*************************************end of this part**********************************/

//insert braces to the proper positions
void CLexical::StatementBoundAnalyze(CArray<TNODE,TNODE> &TArry)
{
	
    //InsertBoundSimple(TArry);
	if(InsertBoundSimple(TArry)) return;//wtt

	while(!InsertBoundComplex(TArry));

	int prepos;
	int aftpos;
	for(int ix=0;ix<TArry.GetSize();ix++ )
	{
		if(ix>0 && TArry[ix].key==CN_WHILE && TArry[ix-1].key==CN_RFLOWER)
		{
			prepos=ix-1;
			ix++;
		    if(! FindBeg(ix,TArry)) return;           

			while(ix<TArry.GetSize() && TArry[ix].key==CN_RFLOWER) ix++;
			if(ix<TArry.GetSize() && TArry[ix].key==CN_LFLOWER)
			{
				aftpos=ix;
				int nbrace=-1;
				int idx=prepos-1;
				while(idx>0 && idx<TArry.GetSize() && nbrace!=0)
				{
					if(TArry[idx].key==CN_LFLOWER)
					{
						nbrace++;
					}
					if(TArry[idx].key==CN_RFLOWER)
					{
						nbrace--;
					}
					idx--;
				}

				if(idx<0) return;
				if(idx>0 && idx<TArry.GetSize() && TArry[idx].key==CN_DO)
				{
					idx=aftpos+1;
					nbrace=1;
					while(idx>=0 && idx<TArry.GetSize() && nbrace!=0)
					{
						if(TArry[idx].key==CN_LFLOWER)
						{
							nbrace++;
						}
						if(TArry[idx].key==CN_RFLOWER)
						{
							nbrace--;
						}
						idx++;
					}

					if(idx>=TArry.GetSize()) return;
					idx--;
					if(idx>=0 && idx<TArry.GetSize() && TArry[idx].key==CN_RFLOWER)
					{
						TArry.RemoveAt(idx);
						TArry.RemoveAt(aftpos); 
					}
				}
			}
		}
	}
}

bool CLexical::InsertBoundSimple(CArray<TNODE,TNODE> &TArry)
{
	int ix=0;
	TNODE TD1,TD2;
	TD1.key=CN_LFLOWER;
	TD1.srcWord = "{";
	TD2.key=CN_RFLOWER;
	TD2.srcWord = "}";
	TD1.addr=TD2.addr=-1;
	TD1.paddr=TD2.paddr-1;
	TD1.value=TD2.value=0;	
	
    while(ix>=0 && ix<TArry.GetSize())//LX此处已删除多处重复代码！
	{
		int key = TArry[ix].key;
		CString word = TArry[ix].srcWord;
		/**wq-20090714-跳过形如#if... 或#else... 的语句***/
		if( TArry[ix].key == CN_WELL )
		{
			while( ix+1<TArry.GetSize() && TArry[ix].line == TArry[ix+1].line )
			{
				ix++;
			}
			ix++;
			continue;
		}/**if( TArry[ix].key == CN_WELL )***/

		if( TArry[ix].key==CN_IF  || TArry[ix].key==CN_ELSE || 
			TArry[ix].key==CN_FOR || TArry[ix].key==CN_WHILE )
		{
			ix++;
			if( ix>=0 && ix < TArry.GetSize() )
			{
				key = TArry[ix].key;
 				word = TArry[ix].srcWord;
			}
			if(TArry[ix-1].key!=CN_ELSE)
			{
				if(!FindBeg(ix,TArry)) 
					return true;//else!
			}
			if(ix>=0 && ix<TArry.GetSize() && !IsSentence(ix,TArry))
			{
				TD1.line=TArry[ix].line;
				TArry.InsertAt(ix,TD1); 				
				ix++;
				if( ix>=0 && ix < TArry.GetSize() )
				{
					key = TArry[ix].key;
					word = TArry[ix].srcWord;
				}
				if(!FindSEnd(ix,TArry))return true;
				if(ix>=0 && ix<TArry.GetSize())
				{
					TD2.line=TArry[ix-1].line;//LX:line值改为TArry[ix-1].line
					TArry.InsertAt(ix,TD2); 
				}
			}
			continue;
		}

		if(TArry[ix].key==CN_DO)
		{
			ix++;
			if( ix>=0 && ix < TArry.GetSize() )
			{
				key = TArry[ix].key;
				word = TArry[ix].srcWord;
			}
			if(ix>0 && ix<TArry.GetSize() && !IsSentence(ix,TArry))
			{
				TD1.line=TArry[ix-1].line;
				TArry.InsertAt(ix,TD1); 			
				while(ix>=0 && ix<TArry.GetSize() && TArry[ix].key!=CN_WHILE)
				{
					ix++;
					if( ix>=0 && ix < TArry.GetSize() )
					{
						key = TArry[ix].key;
						word = TArry[ix].srcWord;
					}
					if(ix>=TArry.GetSize())
					{
						// error
					    // return false;
						return true;//wtt
					}
				}
				TD2.line=TArry[ix-1].line;
				TArry.InsertAt(ix,TD2);
				ix+=2;
				if( ix>=0 && ix < TArry.GetSize() )
				{
					key = TArry[ix].key;
					word = TArry[ix].srcWord;
				}
			}
			continue;
		}
		ix++;
	}
	return false;
}

bool CLexical::FindBeg(int &ix,CArray<TNODE,TNODE> &TArry)
{
	// input ix point to first"("
	// output ix  next to the last ")"
	int n=ix;
	int nRec=1;
    
	if(ix<0 || ix>=TArry.GetSize() || (ix>=0 && ix<TArry.GetSize() && TArry[ix].key!=CN_LCIRCLE))
	{
		// error
		return false;
	}
    
	ix++;
	while(nRec!=0 && ix>=0 && ix<TArry.GetSize())//LX改过此段代码！
	{
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nRec++;
		}
		if(TArry[ix].key==CN_RCIRCLE)
		{
			nRec--;
		}
		ix++;
	}
	
	if(ix>=TArry.GetSize())
	{
		// error
		return false;
	}	
	return true;
}

bool CLexical::FindSEnd(int &ix,CArray<TNODE,TNODE> &TArry)
{
	if(ix<0 || ix>=TArry.GetSize())
		return false;
	int ntemp=ix;
	while(ix<TArry.GetSize())
	{
		if(TArry[ix].key==CN_LINE)
		{
			ix++;
			break;
		}
		/*
		if(ix-1>=0 && TArry[ix].line!=TArry[ix-1].line)//LX删去此段
		{
			break;
		}
		*/
		if(IsSentence(ix,TArry) || TArry[ix].key==CN_RFLOWER)
		{
			break;
		}		
		ix++;		
	}

	if( ix>=TArry.GetSize() && TArry[ix-1].key!=CN_LINE )
	{
		// error
		return false;
	}
	return true;
}

bool CLexical::IsSentence(const int ix,CArray<TNODE,TNODE> &TArry)
{
	if( ix<0 || ix>=TArry.GetSize() )
	{
		return false;
	}
	switch(TArry[ix].key)
	{
		case CN_IF:
		case CN_ELSE:
		case CN_FOR:
		case CN_WHILE:
		case CN_DO:
		case CN_LFLOWER:
		case CN_SWITCH:
			return true;
		default:
			return false;
	}
}

bool CLexical::InsertBoundComplex(CArray<TNODE,TNODE> &TArry)
{
    bool brvalue=true; 
	int ix=0;	    
	TNODE TD1,TD2;
	TD1.key=CN_LFLOWER;
	TD2.key=CN_RFLOWER;
	TD1.srcWord = "{";
	TD2.srcWord = "}";
	TD1.addr=TD2.addr=-1;
	TD1.paddr=TD2.paddr-1;
	TD1.value=TD2.value=0;
	int begpos;
	while(ix<TArry.GetSize())
	{ 
		
		/**wq-20090714-跳过形如#if... 或#else... 的语句***/
		if( TArry[ix].key == CN_WELL )
		{
			while( ix+1<TArry.GetSize() && TArry[ix].line == TArry[ix+1].line )
			{
				ix++;
			}
			ix++;
			continue;
		}/**if( TArry[ix].key == CN_WELL )***/

		if( TArry[ix].key==CN_IF   || TArry[ix].key==CN_WHILE || TArry[ix].key==CN_ELSE ||
			TArry[ix].key==CN_FOR  || TArry[ix].key==CN_DO)
		{ 
			//essential portion
        //AfxMessageBox("");
		    ix++;
			if(TArry[ix-1].key!=CN_ELSE && TArry[ix-1].key!=CN_DO)
			{
				if(ix<TArry.GetSize() && TArry[ix].key!=CN_LCIRCLE)
				{  
					// error
					return true;
				}   

			   if(!	FindBeg(ix,TArry))return true;
            }

			if(ix<TArry.GetSize()&&TArry[ix].key==CN_LFLOWER)
			{ 	
				continue;
			} // match the form : sentence(...)*{...}.
            			
			// not match the form above.
			if( ix<TArry.GetSize()&&(TArry[ix].key==CN_IF  || TArry[ix].key==CN_WHILE || 
				TArry[ix].key==CN_FOR || TArry[ix].key==CN_DO))
			{
				 begpos=ix;
				ix++;
				if(ix<TArry.GetSize()&&begpos<TArry.GetSize()&&TArry[begpos].key!=CN_DO)
				{
					if(TArry[ix].key!=CN_LCIRCLE)
					{ 	  
						// error
						return true;
					}			

				if(!FindBeg(ix,TArry)) return true;
				}
				
                if(ix<TArry.GetSize()&&TArry[ix].key==CN_LFLOWER)
				{
					int i=ix;
					int nBrace=1;
					
					while(nBrace!=0&&i<TArry.GetSize())
					{
						i++;
						if(i>=TArry.GetSize())
						{ 	 
							// error 
							return true;
						}  
						if(TArry[i].key==CN_LFLOWER)
						{
							nBrace++;
						}
						if(TArry[i].key==CN_RFLOWER)
						{
							nBrace--;
						}
					}

					if(TArry[begpos].key==CN_DO)
					{
						if(i+2<TArry.GetSize()&&TArry[i+1].key!=CN_WHILE || TArry[i+2].key!=CN_LCIRCLE)
						{
							// error
							return true;
						}
						i+=2;
						FindBeg(i,TArry);

					}
                    
                    if(i+1<TArry.GetSize()&&TArry[i+1].key==CN_ELSE)
					{ 
						//AfxMessageBox("find if_else");
						i++;
						if(!ExceptionElse(i,TArry))
						{
							continue;
						}
					}
										
					// the position of variable 'ix' is  the index of '}';
					// then we'll insert a pair of '{','}' into the proper position
					// of the token cshain.(begpos and ix)

					TD2.line=TArry[i].line;  
					TArry.InsertAt(i,TD2);
					TD1.line=TArry[begpos].line;   
					TArry.InsertAt(begpos,TD1);
					brvalue=false;			     
				}
				else
				{
					ix=begpos;
					continue;
				}

			}
            
			// end ths partition
		} 
		else
		{ 
			ix++;
		}
	}
	return brvalue;
}

bool CLexical::ExceptionElse(int &ix,CArray<TNODE,TNODE> &TArry)
{
	// input status :ix index of "else"
    
    if(ix<0||ix+1>=TArry.GetSize())
		return false;
	ix++;
	if(TArry[ix].key!=CN_LFLOWER)
	{
		return false;
	}
    
	int nBrace=1;
	while(nBrace!=0)
	{
		ix++;
		if(ix>=TArry.GetSize())
		{ 	 
			// error
			return false;
		}  
		if(ix<TArry.GetSize()&&TArry[ix].key==CN_LFLOWER)
		{
			nBrace++;
		}
		if(ix<TArry.GetSize()&&TArry[ix].key==CN_RFLOWER)
		{
			nBrace--;
		}
	}
	return true;
}

/////////////////wn/9.10/////////////////////////////////////
void CLexical::ResetToken(int index,int end,CArray<TNODE,TNODE> &TArry)
{
	int i=index;
	TNODE TT;
	int nflower;
	int ix=0;
	int bbeg=0,eend=0;
	bool flag=true;////是否存在结构定义嵌套
	int k=0;
	int key;
	CString name=_T("");
	int typedefFlag = -1;
	
	while(i>=index && i<end)
	{
		CString word = TArry[i].srcWord;
		if(TArry[i].key==CN_STRUCT||TArry[i].key==CN_UNION||TArry[i].key==CN_ENUM)
		{	

			/*wq-20090610-将typedef定义的结构体中，struct等定义和typedef定义分离成两个定义***
			 *typede struct NAME{} rename1,rename2;***
			 *------>struct NAME{};
			 *------>typedef struct NAME rename1,rename2;
			 *********************************************/
			if( i>0 && i+1<TArry.GetSize() && TArry[i-1].key==CN_TYPEDEF )
			{
				word = TArry[i-1].srcWord;
				CArray<TNODE,TNODE> tmpTArry;
				tmpTArry.Add(TArry[i-1]);//"typedef"
				tmpTArry.Add(TArry[i]);  //"struct"
				tmpTArry.Add(TArry[i+1]);//"struct name"
				int idx;
				for( idx = 0; idx < tmpTArry.GetSize(); idx++ )
				{
					word = tmpTArry[idx].srcWord;
				}
				TArry.RemoveAt(i-1);  //删除"typedef"
				i--;
				int lflw=-1, rflw=-1;

				/*------>struct NAME{};*/
				bbeg = i;//从"struct"开始，
				for(; i<TArry.GetSize(); i++ )
				{
					word = TArry[i].srcWord;
					if( TArry[i].key == CN_LFLOWER )
					{
						if( lflw == -1 )
						{
							lflw = 1;
						}
						else
						{
							lflw++;
						}
					}
					else if( TArry[i].key == CN_RFLOWER )
					{
						if( rflw == -1 )
						{
							rflw = 1;
						}
						else
						{
							rflw++;
						}
					}
					if( lflw == rflw && lflw != -1 )
					{
						TNODE TN = TArry[i];
						TN.key = CN_LINE;
						TN.srcWord = ";";
						if( i+1 < TArry.GetSize() )
						{
							TArry.InsertAt(i+1,TN,1);//在"}"后面加"}"
						}
						word = TArry[i].srcWord;
						word = TArry[i+1].srcWord;
						i++;
						eend = i;//到定义体结束";"
						ResetToken(bbeg,eend,TArry);//ResetToken()定义体
						break;
					}
				}/*------>struct NAME{};*/

				/*------>typedef struct NAME rename1,rename2;
				         ---->typedef struct NAME rename1;
				         ---->typedef struct NAME rename2;*/
				i++;
				CArray<TNODE,TNODE> tmpTArry2;
				tmpTArry2.Copy(tmpTArry);
				while(i>=0 && i<TArry.GetSize())
				{
					word = TArry[i].srcWord;
					key = TArry[i].key;
					if(key == CN_DOU)
					{
						TArry[i].key = CN_LINE;
						TArry[i].srcWord = ";";
						tmpTArry.Add(TArry[i]); 
						TArry.RemoveAt(i);
						for( idx=0; idx<tmpTArry.GetSize(); idx++ )
						{
							word = tmpTArry[idx].srcWord;
							TArry.InsertAt(i,tmpTArry[idx],1);
							if( i-4>=0 && i-4<TArry.GetSize())word = TArry[i-4].srcWord;
							if( i-3>=0 && i-3<TArry.GetSize())word = TArry[i-3].srcWord;
							if( i-2>=0 && i-2<TArry.GetSize())word = TArry[i-2].srcWord;
							if( i-1>=0 && i-1<TArry.GetSize())word = TArry[i-1].srcWord;
							if(   i>=0 &&   i<TArry.GetSize())word = TArry[i].srcWord;
							i++;
						}
						tmpTArry.Copy(tmpTArry2);
					}
					else
					{
						tmpTArry.Add(TArry[i]); 
						TArry.RemoveAt(i);
					}
					if(key == CN_LINE)
					{
						for( idx=0; idx<tmpTArry.GetSize(); idx++ )
						{
							word = tmpTArry[idx].srcWord;
							TArry.InsertAt(i,tmpTArry[idx],1);
							if( i-4>=0 && i-4<TArry.GetSize())word = TArry[i-4].srcWord;
							if( i-3>=0 && i-3<TArry.GetSize())word = TArry[i-3].srcWord;
							if( i-2>=0 && i-2<TArry.GetSize())word = TArry[i-2].srcWord;
							if( i-1>=0 && i-1<TArry.GetSize())word = TArry[i-1].srcWord;
							if(   i>=0 &&   i<TArry.GetSize())word = TArry[i].srcWord;
							i++;
						}
						break;
					}
				}

				continue;

			}
			/*-end-wq-20090610-将struct等的定义和typedef的定义分开为两个定义*/

			key=TArry[i].key;//key用于区别本次处理的是以上三种情况中的哪一种
			if( i+2<TArry.GetSize() && TArry[i+1].key==CN_VARIABLE && TArry[i+2].key==CN_LFLOWER)
			{//////////////标准类型定义形式//////////////////////////////
			
			//wq///记录当前结构变量类型名称//////////////
				CString v_srcWord = TArry[i+1].srcWord;
			////////////////////////////////////////////
				
				TArry[i+1].key=CN_STRUCTTYPE;
				name=TArry[i+1].name;
				i=i+3;
				nflower=1;				
				bbeg=i;
		
				while(i>=0 && i<TArry.GetSize() && nflower!=0)
				{
					word = TArry[i].srcWord;
					if(TArry[i].key==CN_LFLOWER)
					{	
						flag=false;
						nflower++;
					}
					if(TArry[i].key==CN_RFLOWER)
						nflower--;
					i++;					
				}		
				
				if(nflower!=0)
				{
					return;//花括号数目不匹配
				}			
				eend=i-2;

				if( i>=0 && i+1<TArry.GetSize() && TArry[i].key==CN_VARIABLE && TArry[i+1].key==CN_LINE )
				{////分离类型定义和声明结构变量////////////////
					word = TArry[i].srcWord;
					INIT_TNODE(TT,CN_LINE,-1,TArry[i].line,TArry[i].value,_T(";"));
					TArry.InsertAt(i,TT,1);//在'}'后面增加一个';'
					end++;
					i++;
					int ix=i;

					//构造一个虚拟行，line值均为-1,格式类似于:"STRUCT CN_STRUCTTYPE CN_VARIABLE;"/////
			//wq20080925//虚拟行的line值TT.line = TArry[i].line.
			//		TT.line=TArry[i].line;//wq
			//		INIT_TNODE(TT,key,-1,-1,TArry[i].value);//wq

			 //wq///////////////////////////////////////////////
					CString srcWord = _T("");//wq
					switch(key)
					{
					case CN_STRUCT:
						srcWord = _T("struct");
						break;
					case CN_UNION:
						srcWord = _T("union");
						break;
					case CN_ENUM:
						srcWord = _T("enum");
						break;
					default:
						break;
					}
					INIT_TNODE(TT,key,-1,TArry[i].line,TArry[i].value,srcWord);
			//////////////////////////////////////////////////////

					TArry.InsertAt(i,TT,1);//在虚拟行加入关键字struct/union/enum对应的token符
					i++;
					INIT_TNODE(TT,CN_STRUCTTYPE,-1,TArry[i].line,TArry[i].value,v_srcWord);
					TT.name=name;
					TArry.InsertAt(i,TT,1);	//在虚拟行加入结构体/共用体/枚举类型变量名对应的token符
					end=end+2;
				//	TArry[i+1].line=-1;//wq,行数保持不变
				//	TArry[i+2].line=-1;//wq,函数保持不变
					////////////////////////////////////构造一个虚拟行/////////////////////////////////
				}						
			}			
			else if( i+1<TArry.GetSize() && TArry[i+1].key==CN_LFLOWER)
			{////标准化直接类型定义和变量声明混合的形式/////////////////
				name.Format("%d",k);//k用于为所有无名字的结构体标记序号
				switch(key) {
				case CN_STRUCT:
					name+="Noname_Struct";
					break;
				case CN_UNION:
					name+="Noname_Union";
					break;
				case CN_ENUM:
					name+="Noname_Enum";
					break;
				default:
					break;
				}
				k++;
				INIT_TNODE(TT,CN_STRUCTTYPE,-1,TArry[i].line,TArry[i].value,name);
				TT.name=name;
				TArry.InsertAt(i+1,TT,1);//在本行加入结构体/共用体/枚举类型变量名("Noname_XXX")对应的token符
				end++;
				i=i+3;
				bbeg=i;
				
				nflower=1;
				while(nflower!=0 && i<TArry.GetSize())//LX
				{						
					if(TArry[i].key==CN_LFLOWER)
					{	
						flag=false;
						nflower++;
					}
					if(TArry[i].key==CN_RFLOWER)
						nflower--;
					i++;						
				}
				eend=i-2;
			//	if(i+1<TArry.GetSize() && TArry[i].key!=CN_VARIABLE && TArry[i+1].key==CN_LINE)//wq
				if(i+1<TArry.GetSize() && TArry[i].key==CN_VARIABLE && TArry[i+1].key==CN_LINE)//wq
				//如果'}'后面的标识符是变量名+';'
				{
					INIT_TNODE(TT,CN_LINE,-1,TArry[i].line,TArry[i].value,_T(";"));
					TArry.InsertAt(i,TT,1);//在'}'后面加一个分号
					end++;
					i++;
					/////////构造一个虚拟行，line值均为-1,格式类似于:"STRUCT CN_STRUCTTYPE CN_VARIABLE;"///////	
			//wq20080925//虚拟行的line值TT.line = TArry[i].line.
			//wq///////////////////////////////////////////////
					CString srcWord = _T("");//wq
					switch(key)
					{
					case CN_STRUCT:
						srcWord = _T("struct");
						break;
					case CN_UNION:
						srcWord = _T("union");
						break;
					case CN_ENUM:
						srcWord = _T("enum");
						break;
					default:
						break;
					}
					INIT_TNODE(TT,key,-1,TArry[i].line,TArry[i].value,srcWord);
			//////////////////////////////////////////////////////
			//		INIT_TNODE(TT,key,-1,TArry[i].line,TArry[i].value);//wq
					TArry.InsertAt(i,TT,1);//在虚拟行加入关键字struct/union/enum对应的token符
					i++;		
					INIT_TNODE(TT,CN_STRUCTTYPE,-1,TArry[i].line,TArry[i].value,name);
					TT.name=name;
					TArry.InsertAt(i,TT,1);//在虚拟行加入结构体/共用体/枚举类型变量名("Noname_XXX")对应的token符
					end=end+2;
				//	TArry[i+1].line=-1;//wq行数不变
				//	TArry[i+2].line=-1;//wq
					////////////////////////////////////构造一个虚拟行////////////////////////////////////////
				}
			}	
			if(name!=_T(""))
			{
				for(int id=i;id<end;id++)/////////////从i开始????
				{
					if(TArry[id].name==name)
					{
						TArry[id].key=CN_STRUCTTYPE;//自定义数据类型名在下文中的替换
					}				
				}
			}
			if(!flag)
			{
				ResetToken(bbeg,eend,TArry);
			}
		}
		i++;
	}
}
////////////////////////////////////////////////////////////////////////////////



int FindPosition(CString name,int type,const CArray<SNODE,SNODE>&SArry)
{///查找name和type复合的在符号表中的ID
	int id=0;
	while(id<SArry.GetSize())
	{
		if(SArry[id].name==name&&SArry[id].type==type)
			return id;
		
		id++;
	}
	return -1;

}




void CLexical::PreConstructTable(int index,int eend,CArray<TNODE,TNODE>&TArry,CArray<SNODE,SNODE>&SArry)
{///////建立符号表预处理：在建立符号表前，先将自定义结构类型加入符号表/////
	int i=index;
	SNODE SD;
	bool flag=true;
	int	nflower=1;

	while(i>=index && i<eend)
	{//AfxMessageBox("");
		if( i+2 < TArry.GetSize() &&
			(TArry[i].key==CN_STRUCT||TArry[i].key==CN_UNION) && TArry[i+1].key==CN_STRUCTTYPE && TArry[i+2].key==CN_LFLOWER)
		{
			int key=TArry[i].key;
			SD.addr=-1;
			SD.arrdim=0;
			for(int ix=0;ix<4;ix++)
			{
				SD.arrsize[ix]=0; 
			}
			//SD.initaddr=
			SD.value=1;
			SD.type=key;
			SD.kind=CN_STRUCTTYPE;
			SD.layer=0;/////建立符号表后修改
			SD.mv=-2;
			SD.name=TArry[i+1].name;
			if(key==CN_STRUCT)
				SD.sv=1000;
			else if(key==CN_UNION)
				SD.sv=2000;			
			SD.taddr=i+1;
			TArry[i+1].addr=SArry.GetSize();
			SArry.Add(SD);
			i=i+3;

		    nflower=1;
			int beg=i;
			for(;i<eend && nflower>0 && i<TArry.GetSize();i++)
			{				
				if(TArry[i].key==CN_RFLOWER)
					nflower--;
				if(TArry[i].key==CN_LFLOWER)				
					nflower++;
			}
		//	if(i>=eend)
		//		continue;
		//	AfxMessageBox("");
			i--;
			int end=i;
			
			FillMemberVar(SArry.GetSize()-1,beg,end,TArry,SArry);///将所有成员变量加入符号表
		
			//CString s;
			//s.Format("%d",TArry[i].key);
			//AfxMessageBox(s);
		}
		else if( i+2<TArry.GetSize() && TArry[i].key==CN_ENUM && TArry[i+1].key==CN_STRUCTTYPE && TArry[i+2].key==CN_LFLOWER)
		{
			SD.type=TArry[i].key;
			SD.addr=-1;
			SD.arrdim=0;
			for(int k=0;k<4;k++)
			{
				SD.arrsize[k]=0; 
			}
			//SD.initaddr=
			SD.value=1;
			SD.kind=CN_STRUCTTYPE;
			SD.layer=0;/////建立符号表后修改
			SD.mv=-2;
			SD.name=TArry[i+1].name;
			SD.sv=3000;
			SD.taddr=i+1;
			TArry[i+1].addr=SArry.GetSize();
			SArry.Add(SD);
			for(int ix=0;ix<TArry.GetSize();ix++)
			{
				if(ix+1<TArry.GetSize()&&TArry[ix].key==CN_ENUM&&TArry[ix+1].key==CN_STRUCTTYPE
					&&TArry[ix+1].name==TArry[i+1].name&&TArry[ix+2].key!=CN_LFLOWER)
				{
					TArry[ix].key=CN_INT;   ///translate enum-type into int-type
					TArry.RemoveAt(ix+1);
					eend--;
					
				}				
			}
			
			i=i+2;
			int beg=i;/////TArry[beg-1].key==CN_LFLOWER
			IdentifyEnumConst(beg,TArry);
			while(i<TArry.GetSize() && TArry[i].key!=CN_RFLOWER)
				i++;
		
			int end=i;////TArry[end].key==CN_RFLOWER
			TNODE TL;
			TL.addr=-1;////add a TNODE ";"
			TL.deref=0;
			TL.key=CN_LINE;
			TL.line=TArry[i].line;
			TL.name=_T("");
			TL.paddr=-1;
			TL.value=0;
			TArry.InsertAt(end,TL);		
			//CString s;
			//s.Format("%d",TArry[i].key);
			//AfxMessageBox(s);		
			eend++;	
		}
	i++;
	}

}

void CLexical::FillMemberVar(int ix,int beg,int end,CArray<TNODE,TNODE>& TArry,CArray<SNODE,SNODE>&SArry)
{
	SNODE SD;
	int i=beg;
	
	
	while(i>=beg && i<end)
	{	
		if( i+2<TArry.GetSize() &&
			(TArry[i].key==CN_STRUCT||TArry[i].key==CN_UNION)&&TArry[i+1].key==CN_STRUCTTYPE&&TArry[i+2].key==CN_LFLOWER)
		{//AfxMessageBox("");
			PreConstructTable(beg,end,TArry,SArry);
			i=i+3;
			int nflower=1;
			for(;i<end && nflower>0 && i<TArry.GetSize() ;i++)
			{
				if(TArry[i].key==CN_RFLOWER)
					nflower--;
				if(TArry[i].key==CN_LFLOWER)				
					nflower++;
			}
			i++;
		}
		
		if(IsVariableType(i,TArry))
		{
			SD.type=TArry[i].key;
			///////////////////
			if(SD.type==CN_FLOAT)
			{
				SD.type=CN_DOUBLE;
			}
			////////////////////
			SD.sv=ix;
			if(SD.type==CN_STRUCTTYPE)
				SD.mv=FindPosition(TArry[i].name,TArry[i-1].key,SArry);
			else
				SD.mv=-1;
			
					
		//	for(int i=0;i<4;i++)
		//	{
		//		SD.arrsize[i]=0; 
		//	}
			SD.addr=-1;
			i++;
		
			while(i>0&&i<TArry.GetSize()-1&&!IsVariableType(i,TArry)&&TArry[i].line==TArry[i+1].line)
			{
						
				if(TArry[i].key==CN_VARIABLE)
				{					
					if(TArry[i-1].key==CN_MULTI)
					{			
						SD.kind=CN_POINTER;					
						SD.value=1;	
					}
					else 
					{
						SD.kind=CN_VARIABLE;
						SD.value=0;
					}
					
					SD.taddr=i;
					SD.layer=0;
					SD.name=TArry[i].name;
					
					TArry[i].addr=SArry.GetSize();
					if(TArry[i+1].key==CN_LREC)
					{
						
						if(SD.kind==CN_POINTER)
						{
							
							SD.kind=CN_ARRPTR;
						}
						else
						{
							SD.kind=CN_ARRAY;
						}
						
						i++;
						ArrayDim(i,SD,TArry);
						i--;
					}
				//	AfxMessageBox(SD.name);
					SArry.Add(SD);
			
				}
				i++;	
			}	
			i--;
									
		}
		i++;
	}
}






void CLexical::TypedefReplace(CArray<TNODE,TNODE>&TArry)
{
	CString name;
	int nline,nflower;
	int key;
	int i=0;
	CArray<TNODE,TNODE> Arry;
	Arry.RemoveAll();
	while(i<TArry.GetSize())
	{
		if(i==79)
		{
			int x=0;
		}
		if( TArry[i].key==CN_TYPEDEF && i+1<TArry.GetSize()
			&& (TArry[i+1].key==CN_INT||TArry[i+1].key==CN_CHAR||TArry[i+1].key==CN_FLOAT
			||TArry[i+1].key==CN_DOUBLE||TArry[i+1].key==CN_LONG||TArry[i+1].key==CN_SHORT) )
		{
			int pos=i;
			key=TArry[i+1].key;
			i+=2;
			if(TArry[i].key==CN_VARIABLE&&TArry[i+1].key!=CN_LREC)
			{//////////消除使用typedef定义基本数据类型/////////////
				name=TArry[i].name;
				int ix;
				for(ix=i;ix<TArry.GetSize();ix++)
				{
					if(TArry[ix].name==name)
					{
						TArry[ix].name=_T("");
						TArry[ix].key=key;
					}
				}	
				
			}
			else if(TArry[i].key==CN_VARIABLE&&TArry[i+1].key==CN_LREC)
			{//////////消除使用typedef定义数组类型/////////////
				
				name=TArry[i].name;
				i++;
				for(; i<TArry.GetSize() && TArry[i].key!=CN_RREC; i++)
				{
					
					Arry.Add(TArry.GetAt(i));
				
				}
			
				Arry.Add(TArry.GetAt(i));/////add the ']'
			//	CString s;
			//	s.Format("%d",TArry[i].key);
			//	AfxMessageBox(s);
				int ix;
				for(ix=i+1;ix<TArry.GetSize();ix++)
				{
					if(TArry[ix].name==name)
					{
						TArry[ix].name=_T("");
						TArry[ix].key=key;
						while(ix<TArry.GetSize()&&TArry[ix].key!=CN_LINE)
						{
						
							if(TArry[ix].key==CN_VARIABLE)
							{	
								int line=TArry[ix].line; 
								ix++;
								for(int j=Arry.GetSize()-1;j>=0;j--)
								{		
									Arry[j].line=line;
									TArry.InsertAt(ix,Arry[j]);
								}
								ix+=Arry.GetSize()-1;
							}
							ix++;
						}
						
					}
				}
				
			}
			else if(TArry[i].key==CN_MULTI)
			{//////////消除使用typedef定义指针类型/////////////
				//AfxMessageBox("");
				while(i<TArry.GetSize()&&TArry[i].key!=CN_VARIABLE)
				{
					Arry.Add(TArry[i]);
					i++;
				}
				name=TArry[i].name;	
			
				i++;			
				int ix;
				for(ix=i+1;ix<TArry.GetSize();ix++)
				{
					if(TArry[ix].name==name)
					{
						TArry[ix].name=_T("");
						TArry[ix].key=key;
						while(ix<TArry.GetSize()&&TArry[ix].key!=CN_LINE)
						{//AfxMessageBox("");
							if(TArry[ix].key==CN_VARIABLE)
							{
								int line=TArry[ix].line; 
								for(int j=Arry.GetSize()-1;j>=0;j--)
								{
									Arry[j].line=line;
									TArry.InsertAt(ix,Arry[j]);
								}
								ix+=Arry.GetSize();
							}
							ix++;
						}
					}
				}
			}
			
		////////////////////////删除typedef定义语句////////////////////////
			int k;
			for(k=pos;k>TArry.GetSize()||TArry[k].line==TArry[k+1].line;)
				TArry.RemoveAt(k);				
			TArry.RemoveAt(k);
				
		}
		else if(i+1 < TArry.GetSize() &&
			    TArry[i].key==CN_TYPEDEF&&(TArry[i+1].key==CN_STRUCT||TArry[i+1].key==CN_UNION||TArry[i+1].key==CN_ENUM))
		{	///////////////////////消除使用typedef直接定义结构体、共用体、枚举//////////////////////
			int pos=i;
			TArry.RemoveAt(i);
			key=TArry[i].key;
			nline=TArry[i].line;			
			TNODE TT;
			TT.addr=TArry[i].addr;
			TT.deref=TArry[i].addr;
			TT.key=TArry[i].key;
			TT.line=TArry[i].line;
			TT.name=TArry[i].name;
			TT.paddr=TArry[i].paddr;
			TT.value=TArry[i].value;
			
			i++;
	
			if(TArry[i].key==CN_LFLOWER)
			{	
				int pos=i;
				i++;
				nflower=1;
				while(i<TArry.GetSize()&&nflower!=0)
				{
					if(TArry[i].key==CN_LFLOWER)
						nflower++;
					if(TArry[i].key==CN_RFLOWER)
						nflower--;
					i++;
				}
				if(i>=TArry.GetSize()||nflower!=0)
				{
					////括号不匹配
					return;
				}	
				
				if(i<TArry.GetSize()&&TArry[i].key==CN_VARIABLE)
				{//////结构体类型名称//////////////
					name=TArry[i].name;
				//	AfxMessageBox(name);
					TArry.InsertAt(pos,TArry[i]);
					TArry[pos].line=nline;							
					TArry.RemoveAt(i+1);
					for(int ix=i;ix<TArry.GetSize();ix++)
					{
						if(ix>0&&TArry[ix].name==name&&TArry[ix].key!=key)
						{//
						//	AfxMessageBox(name);
							TT.line=TArry[ix].line;
							TArry.InsertAt(ix,TT);
							//	
							ix++;
						}
					}
				}
				else 
				{
					continue;
					///error：没有结构体类型名
				}
				 	
			}
			if(i+1<TArry.GetSize()&&TArry[i].key==CN_VARIABLE&&TArry[i+1].key==CN_VARIABLE)
			{///////使用结构体定义结构体/////////////////////				
				name=TArry[i].name;
				CString fname=TArry[i+1].name;
				for(int j=i+1;j<TArry.GetSize();j++)
				{
						if(TArry[j].name==fname)
						{
							TArry[j].name=name;
							TT.line=TArry[j].line;
							TArry.InsertAt(j,TT);
							j++;//LX修改！
						}
				}
////////////////////////////删除typedef定义部分/////////////////////
				int k;
				for(k=pos;k>=TArry.GetSize()||TArry[k].line==TArry[k+1].line;)
				{
					TArry.RemoveAt(k);
				}
				TArry.RemoveAt(k);
			}		
		}		
		i++;		
	}

	for(int j=0;j<TArry.GetSize();j++)
	{
		if( j+2 < TArry.GetSize()
			&& TArry[j].key==CN_TYPEDEF
			&& (TArry[j+1].key==CN_STRUCT||TArry[j+1].key==CN_UNION)
			&& TArry[j+2].key==CN_LFLOWER)
			TypedefReplace(TArry);
	}
	Arry.RemoveAll();
}


void CLexical::IdentifyEnumConst(int beg,CArray<TNODE,TNODE>&TArry)
{
	int addr;
	TNODE TT,TD,TI;

	TT.addr=-1;////data type "int "
	TT.deref=0;
	TT.key=CN_INT;
//	TT.line
	TT.name=_T("");
	TT.paddr=-1;
	TT.value=0;

    TD.addr=-1;////a TNODE "="
	TD.deref=0;
	TD.key=CN_EQUAL;
//	TD.line
	TD.name=_T("");
	TD.paddr=-1;
	TD.value=0;

//TI.addr
	TI.deref=0;/////an assign CINT 
	TI.key=CN_CINT;
//	TI.line
	TI.name=_T("");
	TI.paddr=-1;
	TI.value=0;

	int i=beg,j=0;
	TT.line=TArry[i].line;
	TArry.InsertAt(i+1,TT);
	i++;
	while( i<TArry.GetSize() && i>=beg && TArry[i].key!=CN_RFLOWER)
 	{
		if( i+1<TArry.GetSize()
			&& TArry[i].key!=CN_RFLOWER && TArry[i].key==CN_VARIABLE && TArry[i+1].key!=CN_EQUAL)
		{///add type declear and assign a Cint data
				i++;
			TD.line=TArry[i].line;
			TArry.InsertAt(i,TD);
			i++;

			TI.addr=j;
			TI.line=TD.line;
			TArry.InsertAt(i,TI);
			j++;
				
		}

		else if(i+1<TArry.GetSize()
				&& TArry[i].key!=CN_RFLOWER && TArry[i].key==CN_VARIABLE && TArry[i+1].key==CN_EQUAL)
		{///add type declear
			if( i+2<TArry.GetSize()
				&& (TArry[i+2].key!=CN_RFLOWER) && (TArry[i+2].key==CN_CINT))
			{			
				addr=TArry[i+2].addr;
				j=addr+1;				
			}
			else
			{
				i++;
				continue;
			}
			i=i+2;
		}
		i++;
	}
}


void CLexical::OutputSrcFileAfterTokened( CArray<TNODE,TNODE> &TArry )
{
		CStdioFile srcFile;
	CString srcline = _T("");
	CString str_TKIndex = _T("");

	for( int wdix = 0; wdix < TArry.GetSize(); wdix++ )
	{
		if( wdix + 1 < TArry.GetSize() &&
			TArry[wdix].line == TArry[wdix+1].line )
		{
			srcline += TArry[wdix].srcWord;
			str_TKIndex.Format(_T("%d"),TArry[wdix].addr);
			srcline += "$" + str_TKIndex + "$ ";
		//	srcline += " ";
		}
		else 
		{		
			srcline += TArry[wdix].srcWord;
			str_TKIndex.Format(_T("%d"),wdix);
			srcline += "$" + str_TKIndex + "$\n";
		//	srcline += "\n";
		}
	}
	CString tFileName="D:\\sourcecode.txt";	
	srcFile.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	srcFile.WriteString(srcline);
	srcFile.Close();
}

void CLexical::OutputSymTable(CArray<SNODE,SNODE> &SArry)
{
	CString sName,sType,sKind,sAddr,sLayer,sValue,sTaddr,sSv,sMv;
	int i;
	CString outText;
	for( i=0; i<SArry.GetSize(); i++ )
	{
		sName.Format("%30s",SArry[i].name);
		sType.Format("%20s",C_ALL_KEYWORD[SArry[i].type]);
		sKind.Format("%20s",C_ALL_KEYWORD[SArry[i].kind]);
		sAddr.Format("%4d",SArry[i].addr);
		sLayer.Format("%3d",SArry[i].layer);
		sValue.Format("%10f",SArry[i].value);
		sTaddr.Format("%4d",SArry[i].taddr);
		sSv.Format("%4d",SArry[i].sv);
		sMv.Format("%4d",SArry[i].mv);
		outText += "NAME=" + sName + " | " +
				   "TYPE=" + sType + " | " +
				   "KIND=" + sKind + " | " +
				   "ADDR=" + sAddr + " | " +
				   "LAYER=" + sLayer + " | " +
				   "TADDR=" + sTaddr + " | " +
				   "SV="    + sSv    + " | " +
				   "MV="    + sMv    + " | " +
				   "\r\n";
	}
	CStdioFile outfile;
	if( outfile.Open("D:\\SymTable.txt",CFile::modeCreate| CFile::modeWrite))
	{
		outfile.WriteString(outText);
		outfile.Close();
	}


}

void CLexical::DealHeaderFiles(CString &cProgram,
							   CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
							   CArray<HeaderFileElem,HeaderFileElem> &tempHFileArry,
							   CString cFilePath,
							   CH_FILE_INFO &fileInfo//wq-20090610
							   )
{
//	CArray<HeaderFileElem,HeaderFileElem> tempHFileArry;
	fileInfo.cfilePrcssTime = 0.0;
	fileInfo.hFileSum = 0;
	fileInfo.prcss_hFileSum = 0;
 
	Delete_Comment(cProgram);			/*########################################################*/
	int count=cProgram.Find("include",0);//查找cprogram中第一个include的位置
	while(count>=0 && count<cProgram.GetLength())
	{
		CString hfilename=_T("");//用于存放未处理的头文件名 (<xxx.h>或"xxx.h")
		if(ISHAVE_WELL(cProgram,count,7))//前面没有"#",不是文件包含语句！
		{
			count=cProgram.Find("include",count+7);
			continue;
		}
		count=count+7;//count指向"include"后面的那个字符	
		if(count>0 && count<cProgram.GetLength())		
		{
			int line_end=cProgram.Find('\n',count);//查找include所在行的末尾位置，存于line_end
			if(line_end>count)
			{
				int pos_inline=0;
				hfilename=cProgram.Mid(count,line_end-count);//line_end-count是头文件名字的长度
				Deal_SomeLine(count,pos_inline,cProgram,1);//用"\r\n"替换include所在行，
				HeaderFileElem tempHFileElm;
				CArray<CString,CString> hFileNameArry;
				ProcessHFile(hfilename,headerFilesArry,tempHFileElm,cFilePath,hFileNameArry,fileInfo);
				tempHFileElm.hFileName = hfilename;
				tempHFileArry.Add(tempHFileElm);
				tempHFileElm.Clean();
//				hprogram=hprogram+READ_FILE_H(hfilename,Hfp_Array)+"\r\n";//将读出的头文件加入到hprogram中
			}
		}
		count=cProgram.Find("include",count);//查找cprogram中下一个include的位置
	}
																			//AfxMessageBox(program);	//ww 
//	OutputMemoryRecord("CLexical","Del_String",out_file_path,1,"");
	Del_String(cProgram);//为便于分析，将程序中所有字符串替换为"s"			
//	OutputMemoryRecord("CLexical","CFileMacroReplace",out_file_path,1,"");
	CFileMacroReplace(cProgram,tempHFileArry);

}

void CLexical::ProcessHFile(CString hfilename,
							CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
							HeaderFileElem &tempHFileElm, CString filePath,
							CArray<CString,CString> &hFileNameArry,
							CH_FILE_INFO &fileInfo//wq-20090610
							)
{


	CString tempFilepath = filePath;
	GenerateCleanFilePath(tempFilepath);

	CString hprogram;
	Out_TokenSeq readh;
	if(BuildHFileFullPath(hfilename,tempFilepath))
	{
		int sp = 0;
		for( sp = 0; sp<hFileNameArry.GetSize(); sp++ )
		{
			if( hfilename.CompareNoCase(hFileNameArry[sp]) == 0 )
			{
				return;
			}
		}

		hFileNameArry.Add(hfilename);
		int index = -1;
		if(hfilename!="" && hfilename!="NULL" )
		{
			readh.READ_CHFILE(hfilename,hprogram);//读头文件
			
			if( hprogram == "" )
			{
				return;
			}

//			fileInfo.hFileSum++;//头文件总数+1

			if( IsHFileAdded(hfilename,headerFilesArry,index) )
			{
				tempHFileElm = headerFilesArry[index];
				return;
			}
			else
			{
				
/*				readh.READ_CHFILE(hfilename,hprogram);//读头文件
				
				if( hprogram == "" )
				{
					return;
				}*/
//				fileInfo.prcss_hFileSum++;//处理的头文件数+1

				tempHFileElm.hFileName = hfilename;

				//找当前所读头文件所包含的头文件名hfilename2，然后递归调用ProcessHFile()
				Delete_Comment(hprogram);
				int count=hprogram.Find("include",0);//查找cprogram中第一个include的位置
				while(count>=0 && count<hprogram.GetLength())
				{
					CString hfilename2=_T("");//用于存放未处理的头文件名 (<xxx.h>或"xxx.h")
					if(ISHAVE_WELL(hprogram,count,7))//前面没有"#",不是文件包含语句！
					{
						count=hprogram.Find("include",count+7);
						continue;
					}
					count=count+7;//count指向"include"后面的那个字符
					if(count>0 && count<hprogram.GetLength())
					{
						int line_end=hprogram.Find('\n',count);//查找include所在行的末尾位置，存于line_end
						if(line_end>count)
						{
							int pos_inline=0;
							hfilename2=hprogram.Mid(count,line_end-count);//line_end-count是头文件名字的长度
							Deal_SomeLine(count,pos_inline,hprogram,1);//用"\r\n"替换include所在行，
							ProcessHFile(hfilename2,headerFilesArry,tempHFileElm,hfilename,hFileNameArry,fileInfo);
						}
					}
					count=hprogram.Find("include",count);//查找hprogram中下一个include的位置
				}
			}

			/*********递归调用ProcessHFile()返回后****************/

			/***********#define替换********************/
			//按行读取hprogram文件hCodeLine
			//如果hCodeLine是宏定义(或一部分)，则识别宏定义添加tempHFileElm的宏定义列表
			//如果hCodeLine不是宏定义，扫描tempHFileElm的宏定义列表，进行宏定义的替换
			Del_String(hprogram);//为便于分析，将程序中所有字符串替换为"s"
			HFileMacroReplace(hprogram,tempHFileElm);
			/***********#define替换********************/

			//进行词法分析
			CArray<VIPCS,VIPCS> VArry;
			CList<CString,CString>  m_sList;
			TranToLine(hprogram,m_sList);
			ConstructToken(m_sList,tempHFileElm.hFileTknArry,VArry);
			StatementBoundAnalyze(tempHFileElm.hFileTknArry);//把没有{}的if/while/for/else/do语句加上{} 
			ResetToken(0,tempHFileElm.hFileTknArry.GetSize(),tempHFileElm.hFileTknArry);//将程序中结构体部分标准化，修改自wn程序
			FormComplexOp(tempHFileElm.hFileTknArry);//把TArry中相邻两个可构成一个复合运算符的节点合并如: ->,&&,*=
			FormatTokenLines ftls;
			ftls(tempHFileElm.hFileTknArry,srcCode);

			//进行typedef的定义替换，添加tempHFileElm的typedef定义替换列表
			//过程与上述宏替换相似
			//typedef定义统一存于tempHFileElm中，不包含于替换后的token中
			NewTypedefReplace(tempHFileElm.hFileTknArry,tempHFileElm);

			ConstructHFileSymTable(tempHFileElm.hFileTknArry,
								 tempHFileElm.symbolTable,tempHFileElm.funcTable);//识别函数定义和函数声明，参数、局部变量、全局变量，存放在符号表中，并建立函数列表，记录函数间的调用关系

/*			if(tempHFileElm.symbolTable.GetSize()>0)
			{
				// assign the right addresses to every variable in token chain;	
				int layernum=tempHFileElm.funcTable.GetSize();	
				for(int layer=1;layer<=layernum;layer++)
				{			
					VariableAssign(layer,tempHFileElm.funcTable[layer-1],
									tempHFileElm.hFileTknArry,
									tempHFileElm.symbolTable,
									tempHFileElm.funcTable);
				}		
			}*/

			if( tempHFileElm.symbolTable.GetSize() == 0 
				&& tempHFileElm.macroReplaceArry.GetSize() == 0 
				&& tempHFileElm.typedefRcdArry.GetSize() == 0 )
			{
				return;
			}

			tempHFileElm.hFileName = hfilename;
			tempHFileElm.hFileTknArry.RemoveAll();
			headerFilesArry.Add(tempHFileElm);
//			OutputMemoryRecord("CLexical","ProcessHFile",out_file_path,1,hfilename);
			fileInfo.prcss_hFileSum++;//处理的头文件数+1

			

		}
	}
}

//wq-20090522
bool CLexical::IsHFileAdded(CString hFileName,
							CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
							int &index)
{
	int i;
	index = -1;
	for( i = 0; i < headerFilesArry.GetSize(); i++ )
	{
//		if( hFileName == headerFilesArry[i].hFileName )
		CString name = headerFilesArry[i].hFileName;
		if( hFileName.CompareNoCase(headerFilesArry[i].hFileName) == 0 )//wq-20090609-不区分大小写比较
		{
			index = i;
			return true;
		}
	}
	return false;

}

//wq-20090522
bool CLexical::BuildHFileFullPath(CString &hfilename,CString curFilePath)
{
	CString hfilepath=_T("");	//用于存放名为"hfilename"的头文件的路径
	
	//删除头文件名字中的无用字符{----
	for(int i=0; i<hfilename.GetLength(); i++)
	{
		if(!isgraph(hfilename[i]))
		{
			hfilename.Delete(i,1);//删除无法打印的字符
		}
	}
	CString outFileName = hfilename;
 	if(hfilename.Find('\"') >= 0)//找到双引号"
	{
		if(hfilename.Remove('\"')!=2)
		{
			return false;//报错！
		}
	}
	if(hfilename.Find('<') >= 0)//找到'<'
	{
		if(hfilename.Remove('<')!=1 || hfilename.Remove('>')!=1)
		{
			return false;//报错！
		}
	}
	//删除头文件名字中的无用字符----}
	
	CString temp_name=hfilename;
	if(temp_name.Right(2)!=_T(".h")
		&& temp_name.Right(2)!=_T(".H"))
	{
		return false;//报错！
	}
	else
	{
		temp_name.Delete(temp_name.GetLength()-2,2);//去掉".h"
	}
	/*wq-20090609-去掉"../include/xxx.h"最前面的两个点*/
    if( hfilename.GetLength() > 2 )
	{
		if( hfilename[0] == '.' && hfilename[1] == '.' && hfilename[2] == '\\' )
		{
			hfilename.Delete(0,2);
			hfilepath=Obj_file_path+hfilename;
			hfilename = hfilepath;
			return true;
		}
	}

	if(hfilename.Find('/')==-1 && WhichBFunction(temp_name)==-1)//文件名中未发现路径标记'/'，并且不是c语言库文件
	{
		hfilepath=FIND_FILE_H(Obj_file_path,hfilename,curFilePath);
		hfilename = hfilepath;
		return true;
	}
	if(hfilename.Find('/')>=0)//文件名中存在路径标记'\'
	{
		hfilename.Replace("/","\\");//用'\\'替换'\'
		hfilepath=Obj_file_path+"\\include\\"+hfilename;
		hfilename = hfilepath;
		return true;
	}
	return false;

}

void CLexical::DefineReplaceFromHFile(CString &srcProgram, CString &dstProgram)
{
	int hcount=srcProgram.Find("define",0);
	while(hcount>=0 && hcount<srcProgram.GetLength())
	{
		int len=srcProgram.GetLength();
		if(ISHAVE_WELL(srcProgram,hcount,6))
		{
			hcount=hcount+6;//未发现#，报错！
		}
		else
		{
			CString Macro_Name=_T(""),Macro_Value=_T("");
			if(!Genrate_Macro_NV(srcProgram,hcount,Macro_Name,Macro_Value))
			{
				Span_Define(dstProgram,0,Macro_Name,Macro_Value);
			}
		}
		hcount=srcProgram.Find("define",hcount);
	}


}

void CLexical::DefineReplaceFromSelf(CString &program,CString &macroLines)
{
	int ccount=program.Find("define",0);
	while(ccount>=0 && ccount<program.GetLength())
	{
		if(ISHAVE_WELL(program,ccount,6))
		{
			ccount=ccount+6;//未发现#，报错！
		}
		else
		{
			CString Macro_Name,Macro_Value;
			if(!Genrate_Macro_NV(program,ccount,Macro_Name,Macro_Value))
			{
				Span_Define(program,ccount,Macro_Name,Macro_Value);
			}
		}
		ccount=program.Find("define",ccount);
	}

}


void CLexical::ReadHeaderFile(CString &filename, CString &program)
{
	CStdioFile file;
	CString strLine;
	if( !file.Open(filename,CFile::modeRead))
	{
		return;
	}
	while(file.ReadString(strLine))
	{
		strLine=_T("");
	    program=_T("");
		while(file.ReadString(strLine))
		{
			program=program+strLine+"\n";
			strLine=_T("");
		}
		file.Close();
	}

}

void CLexical::NewConstructSymTable(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	if(TArry.GetSize()<=0)
	{
		return;
	}
    //  construct the symbol table

	/*wn-建立符号表预处理：在建立符号表前，先将自定义结构类型加入符号表tpSArry*/
	PreConstructTable(0,TArry.GetSize(),TArry,SArry);

	int   ix=0;
    int   nlayer=0;
	SNODE SD;
	FNODE FD;
	CString stemp;
	int nflower;//wn
    m_nTLen=TArry.GetSize();///wn

	while( ix>=0 && ix<TArry.GetSize() )
	{
		CString str_key = TArry[ix].srcWord;
		int key = TArry[ix].key;
		//cout<<ix<<endl;
		if( ( TArry[ix].key==CN_STRUCT||TArry[ix].key==CN_UNION )
			&&( (ix+1<TArry.GetSize() && TArry[ix+1].key==CN_STRUCTTYPE
			     && ix+2<TArry.GetSize() && TArry[ix+2].key==CN_LFLOWER)
			   ||(ix+1 < TArry.GetSize() && TArry[ix+1].key==CN_LFLOWER) )
		   )
		{
			nflower=1;
			ix=ix+3;
			for(;nflower>0;ix++)
			{
				if(ix>=TArry.GetSize())
					break;//LX修改!!!
				if(TArry[ix].key==CN_LFLOWER)
					nflower++;
				if(TArry[ix].key==CN_RFLOWER)
					nflower--;
			}
			ix++;
			continue;
		}///wn//跳过结构类型定义

		if( ((ix+1)<m_nTLen) && 
			 ( ix>=0 && ix<TArry.GetSize() && (TArry[ix].key==CN_VARIABLE || TArry[ix].key==CN_MAIN) )
			 && (ix+1<TArry.GetSize() && TArry[ix+1].key==CN_LCIRCLE) )// definition of function
		{
			// initialization of the node "SD".
			SD.addr=-1;
			SD.arrdim=0;
			SD.sv=-1;//wn
			SD.mv=-2;//wn
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			SD.kind=-1;
			SD.type=-1;
			SD.name=TArry[ix].name; 
			SD.kind=CN_DFUNCTION;	 
			
			// initializaton of the node "FD".
			FD.bbeg=-1;
			FD.bend=-1;
			FD.fid=-1;
			FD.pnum=0;
			FD.rkind=-1;
			FD.rtype=-1;
			FD.name=TArry[ix].name;
			//////////////////////wtt/////////////
			FD.callednum =0;
			FD.calleenum=0;
			for(int j=0;j<20;j++)
			{
				FD.calleelist[j]=-1;
				FD.plist[j]=-1;
			}
	        /////////////////////////wtt/////////////////////			
			//  the layer number of the function
            nlayer++;
			SD.layer=nlayer;
			TArry[ix].key=CN_DFUNCTION;
			TArry[ix].addr=FArry.GetSize();
			SD.taddr=ix;///wn//9.12

			ReturnTandK(ix,SD,FD,TArry);// get return type and kind
	
			int nBeg=ix+1;// nBeg point to the "("	
			ix=ix+2;// ix point to token next to "("    (begin of the parameter list in the program).		
			
			int nRec=1;		
			if( ix<TArry.GetSize() && TArry[ix].key==CN_RCIRCLE)//  function without parameter
			{
				nRec=0;
				ix++;
			}

			while( (ix<m_nTLen) && (nRec!=0) )
			{
				if( ix<TArry.GetSize() )
				{
					if(TArry[ix].key==CN_LCIRCLE)
					{
						nRec++;
					}
					if(TArry[ix].key==CN_RCIRCLE)
					{
						nRec--;
					}
				}
				ix++;
			}
			int nEnd=ix-1;//  nEnd point to the ")"   (end of the parameter list in the program).

			//////////////WTT////////////////////////////////////////
			if( ix<TArry.GetSize() && TArry[ix].key==CN_LFLOWER )//函数定义
			{
                FD.pbeg=nBeg;
				FD.pend=nEnd;	
			   	FD.bbeg=ix;
                int nBrace=1;
				ix++;

				while((ix<TArry.GetSize()) && (nBrace!=0))
				{
					if(TArry[ix].key==CN_LFLOWER)
					{
						nBrace++;
					}

					if(TArry[ix].key==CN_RFLOWER)
					{
						nBrace--;
					}
					ix++;
				}

				if(ix>m_nTLen)
				{
					// syntax error
					FD.bend=TArry.GetSize();			
				}
				// find the function end label "}".
				FD.bend=ix-1;
				ix--;
			}
			////////////////////wtt/////////////////////////////////////////
		
			///////wq-20081125-修改识别函数声明-形如："static void clean_child_exit(int code)__attribute__ ((noreturn));"
			else //if(ix<TArry.GetSize()&&TArry[ix].key==CN_LINE)//函数声明
			{	// the declaration of the function(not definition).
				// edit here when extend system 
				int lflw = 0, rflw = 0;
				while( ix>0 && ix<TArry.GetSize() && TArry[ix].line == TArry[ix-1].line)
				{
					if( TArry[ix].key == CN_LFLOWER )
					{
						lflw++;
					}
					else if( TArry[ix].key == CN_RFLOWER )
					{
						rflw++;
					}
					if( TArry[ix].key == CN_LINE && lflw == 0 && rflw == 0)
//						|| (ix == TArry.GetSize()-1 && TArry[ix].key == CN_LINE))
					{
						nlayer--;
						ix++;
						int i;
						for( i=nBeg-1;i<ix-1;i++)
						{
							if( i<TArry.GetSize() && (TArry[i].key==CN_VARIABLE || TArry[i].key==CN_DFUNCTION))
							{
								TArry[i].key=CN_CSTRING;
								break;
							}					
						}
						if(i<TArry.GetSize() && TArry[i].key==CN_CSTRING)
						{
							break;
						}
					}
					ix++;
				}
				
				continue;
			}
			///////////////////////wtt/////////////////////////////
/*			else//错误的函数声明形式
			{						
				return;	
			}*///wq-20081125
		
			/////////////////////////////////////////////wtt/////////////////////////
			//	if(TArry[nEnd+1].key!=CN_LINE)//wtt
			if(nEnd+1<TArry.GetSize() && TArry[nEnd+1].key==CN_LFLOWER)//wtt对函数定义进行参数和局部变量处理
			{
                // analyze the function body to find the definitions of local variables;  
               	SD.addr=FArry.GetSize(); 
				if(SD.type==CN_FLOAT)
				{
					SD.type=CN_DOUBLE;
				}
		//		SArry.Add(SD);
				ParaAnalysis(nlayer,FD,TArry,SArry);
				localVariables(nlayer,FD,TArry,SArry);
				FD.fid=FArry.GetSize(); 				
				if(FD.rtype==CN_FLOAT)
				{
					FD.rtype=CN_DOUBLE;
				}
		//		FArry.Add(FD);
			}
		}
        	
		if( (ix+1<TArry.GetSize()&&TArry[ix].key==CN_VARIABLE) && 
			( (TArry[ix+1].key==CN_LINE) || (TArry[ix+1].key==CN_DOU)||
			  (TArry[ix+1].key==CN_EQUAL) || (TArry[ix+1].key==CN_LREC)) )
		{
			// global variable analysis
			SD.addr=-1;
			SD.arrdim=0;
			
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			TArry[ix].addr=SArry.GetSize();
			SD.kind=CN_VARIABLE;
			SD.taddr=ix;     ///wn//9.12
			SD.type=-1;
			SD.name=TArry[ix].name; 
			SD.layer=0; 
			SD.addr =0;//wtt 全局变量默认赋值为0
            SD.value =0;//wtt 全局变量默认赋值为0
			GlobalVariables(ix,SD,TArry,SArry);
			key = TArry[ix].key;
		}
		ix++;
	}


}

//wq-20090524-宏定义替换
//按行读取program文件codeLine
//如果codeLine是宏定义(或一部分)，则识别宏定义添加tempHFileElm的宏定义列表
//如果codeLine不是宏定义，扫描tempHFileElm的宏定义列表，进行宏定义的替换
void CLexical::HFileMacroReplace(CString &program, HeaderFileElem &tempHFileElm)
{
	CArray<MACRO_REPALCE_NODE,MACRO_REPALCE_NODE> &macroReplaceArry = tempHFileElm.macroReplaceArry;

	int lineEnd = 0, lineBeg = 0;

	CString tempProg("");//记录被宏替换过的源程序
	
	lineEnd = program.Find("\n",lineBeg)+1;
	while( lineEnd > 0 && lineEnd < program.GetLength() )
	{
		CString &codeLine = program.Mid(lineBeg,lineEnd-lineBeg);
		int defineFlag = 0;
		int pos =  codeLine.Find("#define",0);

		if( pos >= 0 )
		{//如果找到"#define"，则宏定义标志defineFlag设为1
			defineFlag = 1;
		}
		while( pos > 0 )
		{//如果该行字符串中#define之前出现了非空格字符，则defineFlag设为0，说明该行非宏定义
			pos -= 1;
			if( codeLine.Mid(pos,1) != " " )
			{
				defineFlag = 0;
			}
		}

		//如果codeLine是宏定义(或一部分)，则识别宏定义添加tempHFileElm的宏定义列表
		if( defineFlag == 1 )
		{//进行宏定义的识别和存储
			CString Macro_Name=_T(""),Macro_Value=_T("");
			if(!Genrate_Macro_NV(program,lineBeg,Macro_Name,Macro_Value))
			{//lineBeg自动跳到宏定义结束后的位置
				int i;
				int isExist = 0;
				for( i = 0; i < macroReplaceArry.GetSize(); i++ )
				{
					if( Macro_Name == macroReplaceArry[i].macroName )
					{
						isExist = 1;//该宏定义已经被存储
					}
				}
				if( isExist == 0 )
				{
					tempHFileElm.AddMacroReplaceArry(Macro_Name,Macro_Value);
				}
			}
		}

		//如果codeLine不是宏定义，扫描tempHFileElm的宏定义列表，进行宏定义的替换
		else
		{
/*******************************
			while( codeLine.Replace("(","(") != codeLine.Replace(")",")") )
			{//不断增加codeLine，知道得到完整的()匹配为止
				lineBeg = lineEnd;
				lineEnd = program.Find("\n",lineBeg)+1;
				codeLine += program.Mid(lineBeg,lineEnd-lineBeg);
			}/*************///wq-20090611-只替换单行代码的宏
			int i;
			CString Macro_Name=_T(""),Macro_Value=_T("");
			for(i = 0; i < macroReplaceArry.GetSize(); i++)
			{//扫描tempHFileElm的宏定义列表，进行宏定义的替换
				Macro_Name = macroReplaceArry[i].macroName;
				Macro_Value = macroReplaceArry[i].macroValue;
				Span_Define(codeLine,lineBeg,Macro_Name,Macro_Value);//宏定义替换函数
			}
			if( lineBeg >= lineEnd )
			{
//				break;
			}
			lineBeg = lineEnd;
			tempProg += codeLine;
		}		
		lineEnd = program.Find("\n",lineBeg)+1;
	}
//    program = "";
//	program = tempProg;

}

//wq-20090524-头文件符号表的生成，不对函数定义和函数声明进行存储
void CLexical::ConstructHFileSymTable(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	if(TArry.GetSize()<=0)
	{
		return;
	}
    //  construct the symbol table

	/*wn-建立符号表预处理：在建立符号表前，先将自定义结构类型加入符号表tpSArry*/
	PreConstructTable(0,TArry.GetSize(),TArry,SArry);

	int   ix=0;
    int   nlayer=0;
	SNODE SD;
	FNODE FD;
	CString stemp;
	int nflower;//wn
    m_nTLen=TArry.GetSize();///wn

	while( ix>=0 && ix<TArry.GetSize() )
	{
		CString str_key = TArry[ix].srcWord;
		int key = TArry[ix].key;
		//cout<<ix<<endl;
		if( ( TArry[ix].key==CN_STRUCT||TArry[ix].key==CN_UNION )
			&&( (ix+1<TArry.GetSize() && TArry[ix+1].key==CN_STRUCTTYPE
			     && ix+2<TArry.GetSize() && TArry[ix+2].key==CN_LFLOWER)
			   ||(ix+1 < TArry.GetSize() && TArry[ix+1].key==CN_LFLOWER) )
		   )
		{
			nflower=1;
			ix=ix+3;
			for(;nflower>0;ix++)
			{
				if(ix>=TArry.GetSize())
					break;//LX修改!!!
				if(TArry[ix].key==CN_LFLOWER)
					nflower++;
				if(TArry[ix].key==CN_RFLOWER)
					nflower--;
			}
			ix++;
			continue;
		}///wn//跳过结构类型定义

		if( ((ix+1)<m_nTLen) && 
			 ( ix>=0 && ix<TArry.GetSize() && (TArry[ix].key==CN_VARIABLE || TArry[ix].key==CN_MAIN) )
			 && (ix+1<TArry.GetSize() && TArry[ix+1].key==CN_LCIRCLE) )// definition of function
		{
			// initialization of the node "SD".
			SD.addr=-1;
			SD.arrdim=0;
			SD.sv=-1;//wn
			SD.mv=-2;//wn
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			SD.kind=-1;
			SD.type=-1;
			SD.name=TArry[ix].name; 
			SD.kind=CN_DFUNCTION;	 
			
			// initializaton of the node "FD".
			FD.bbeg=-1;
			FD.bend=-1;
			FD.fid=-1;
			FD.pnum=0;
			FD.rkind=-1;
			FD.rtype=-1;
			FD.name=TArry[ix].name;
			//////////////////////wtt/////////////
			FD.callednum =0;
			FD.calleenum=0;
			for(int j=0;j<20;j++)
			{
				FD.calleelist[j]=-1;
				FD.plist[j]=-1;
			}
	        /////////////////////////wtt/////////////////////			
			//  the layer number of the function
            nlayer++;
			SD.layer=nlayer;
			TArry[ix].key=CN_DFUNCTION;
			TArry[ix].addr=FArry.GetSize();
			SD.taddr=ix;///wn//9.12

			ReturnTandK(ix,SD,FD,TArry);// get return type and kind
	
			int nBeg=ix+1;// nBeg point to the "("	
			ix=ix+2;// ix point to token next to "("    (begin of the parameter list in the program).		
			
			int nRec=1;		
			if( ix<TArry.GetSize() && TArry[ix].key==CN_RCIRCLE)//  function without parameter
			{
				nRec=0;
				ix++;
			}

			while( (ix<m_nTLen) && (nRec!=0) )
			{
				if( ix<TArry.GetSize() )
				{
					if(TArry[ix].key==CN_LCIRCLE)
					{
						nRec++;
					}
					if(TArry[ix].key==CN_RCIRCLE)
					{
						nRec--;
					}
				}
				ix++;
			}
			int nEnd=ix-1;//  nEnd point to the ")"   (end of the parameter list in the program).

			//////////////WTT////////////////////////////////////////
			if( ix<TArry.GetSize() && TArry[ix].key==CN_LFLOWER )//函数定义
			{
                FD.pbeg=nBeg;
				FD.pend=nEnd;	
			   	FD.bbeg=ix;
                int nBrace=1;
				ix++;

				while((ix<TArry.GetSize()) && (nBrace!=0))
				{
					if(TArry[ix].key==CN_LFLOWER)
					{
						nBrace++;
					}

					if(TArry[ix].key==CN_RFLOWER)
					{
						nBrace--;
					}
					ix++;
				}

				if(ix>m_nTLen)
				{
					// syntax error
					FD.bend=TArry.GetSize();			
				}
				// find the function end label "}".
				FD.bend=ix-1;
				ix--;
			}
			////////////////////wtt/////////////////////////////////////////
		
			///////wq-20081125-修改识别函数声明-形如："static void clean_child_exit(int code)__attribute__ ((noreturn));"
			else //if(ix<TArry.GetSize()&&TArry[ix].key==CN_LINE)//函数声明
			{	// the declaration of the function(not definition).
				// edit here when extend system 
				int lflw = 0, rflw = 0;
				while( ix>0 && ix<TArry.GetSize() && TArry[ix].line == TArry[ix-1].line)
				{
					if( TArry[ix].key == CN_LFLOWER )
					{
						lflw++;
					}
					else if( TArry[ix].key == CN_RFLOWER )
					{
						rflw++;
					}
					if( TArry[ix].key == CN_LINE && lflw == 0 && rflw == 0)
//						|| (ix == TArry.GetSize()-1 && TArry[ix].key == CN_LINE))
					{
						nlayer--;
						ix++;
						int i;
						for( i=nBeg-1;i<ix-1;i++)
						{
							if( i<TArry.GetSize() && (TArry[i].key==CN_VARIABLE || TArry[i].key==CN_DFUNCTION))
							{
								TArry[i].key=CN_CSTRING;
								break;
							}					
						}
						if(i<TArry.GetSize() && TArry[i].key==CN_CSTRING)
						{
							break;
						}
					}
					ix++;
				}
				
				continue;
			}
			///////////////////////wtt/////////////////////////////
/*			else//错误的函数声明形式
			{						
				return;	
			}*///wq-20081125
		
			/////////////////////////////////////////////wtt/////////////////////////
			//	if(TArry[nEnd+1].key!=CN_LINE)//wtt
			if(nEnd+1<TArry.GetSize() && TArry[nEnd+1].key==CN_LFLOWER)//wtt对函数定义进行参数和局部变量处理
			{
                // analyze the function body to find the definitions of local variables;  
               	SD.addr=FArry.GetSize(); 
				if(SD.type==CN_FLOAT)
				{
					SD.type=CN_DOUBLE;
				}
	//			SArry.Add(SD);//wq-delte-头文件的函数定义不加入符号表
				NewParaAnalysis(nlayer,FD,TArry,SArry);
				NewLocalVariables(nlayer,FD,TArry,SArry);
				FD.fid=FArry.GetSize(); 				
				if(FD.rtype==CN_FLOAT)
				{
					FD.rtype=CN_DOUBLE;
				}
	//			FArry.Add(FD);//wq-delte-头文件的函数定义不加入函数表
			}
		}
        	
		if( (ix+1<TArry.GetSize()&&TArry[ix].key==CN_VARIABLE) && 
			( (TArry[ix+1].key==CN_LINE) || (TArry[ix+1].key==CN_DOU)||
			  (TArry[ix+1].key==CN_EQUAL) || (TArry[ix+1].key==CN_LREC)) )
		{
			// global variable analysis
			SD.addr=-1;
			SD.arrdim=0;
			
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			TArry[ix].addr=SArry.GetSize();
			SD.kind=CN_VARIABLE;
			SD.taddr=ix;     ///wn//9.12
			SD.type=-1;
			SD.name=TArry[ix].name; 
			SD.layer=0; 
			SD.addr =0;//wtt 全局变量默认赋值为0
            SD.value =0;//wtt 全局变量默认赋值为0
			GlobalVariables(ix,SD,TArry,SArry);
			key = TArry[ix].key;
		}
		ix++;
	}



}

void CLexical::NewParaAnalysis(const int nlayer,FNODE &FD,const CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{
	SNODE SD;
    int ix=FD.pbeg;  
    
	if(ix>=(FD.pend-1))
	{
		// function without parameters
		return;
	}

	FD.pnum=0; 
	
	while(ix<FD.pend&&ix<TArry.GetSize())
	{
		if( (ix+1<FD.bend) &&(IsVariableType(ix,TArry)))
		{
			// initialization of the SNODE "SD"
			SD.type=TArry[ix].key;
			SD.addr=-1;
			SD.arrdim=0;

			if(ix>=1&&TArry[ix].key==CN_STRUCTTYPE&&(TArry[ix-1].key==CN_STRUCT||TArry[ix-1].key==CN_UNION))
			{
				SD.sv=FindPosition(TArry[ix].name,TArry[ix-1].key,SArry);///wn
				
			}
			else 
				SD.sv=-1;

			SD.mv=-2;///wn
			for(int i=0;i<4;i++)
			{
				SD.arrsize[i]=0; 
			}
			SD.kind=-1;
			
		
			SD.name=_T("");
			
			// point to the token next to the type. 
			ix++;
          //  while(ix<TArry.GetSize() && TArry[ix].key!=CN_RCIRCLE && 
			//	  TArry[ix].key!=CN_LINE && ix<FD.pend )
		//	{
		/////wn
			if( ix<TArry.GetSize() && TArry[ix].key==CN_MULTI)
				{
					// the variable must be a pointer.
					SD.kind=CN_POINTER;
					ix++;
					//////////////wtt//////////////////
                    if( ix<TArry.GetSize() && TArry[ix].key!=CN_VARIABLE)
					{	//error	
												
							return ;		
					}
					
					//////////////////////wtt////////
				}
				else
				{
					// common variable or array.
					SD.kind=CN_VARIABLE; 
				}

				if(TArry[ix].key==CN_VARIABLE)
				{
					SD.taddr=ix;//wn
					SD.name=TArry[ix].name;	
					//AfxMessageBox(SD.name);
					
					ix++;

					if( ix<TArry.GetSize() && TArry[ix].key==CN_LREC)
					{
						ArrayDim(ix,SD,TArry);
						if(SD.kind==CN_POINTER)
						{
							SD.kind=CN_ARRPTR;
						}
						else
						{
							SD.kind=CN_ARRAY; 
						}
					} 

					SD.layer=nlayer; 
					FD.plist[FD.pnum]=SArry.GetSize();
					FD.pnum++;
				
					if(!VariableExisted(SD,SArry))
					{
						if(SD.type==CN_FLOAT)
						{
							SD.type=CN_DOUBLE;
						}
						
	//					SArry.Add(SD);
					}
					else
					{
						// error: variable has existed;
						SD.addr=-1;
						SD.arrdim=0;
						SD.kind=CN_VARIABLE;
						SD.type=CN_INT;
	//					SArry.Add(SD);
					}
				}
			//	ix++;
		//	}
		//	ix--;

		}// if( (ix+1<FD.bend) &&(IsVariableType(ix,TArry)))

		ix++;
	} 
}

void CLexical::NewLocalVariables(const int nlayer, const FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{
	SNODE SD;
    int ix=FD.bbeg; //##
    int beforvariable;
	int nflower;//wn
	if(ix>=(FD.bend-1))
	{
		// empty function
		return;
	}
	
//	CString s;
//	s.Format("<%d,%d> layer=%d",FD.bbeg,FD.bend,nlayer);
//	AfxMessageBox(FD.name+s);
	
	while(ix<FD.bend&&ix>=0&&ix<TArry.GetSize())
	{
		if( ix>=1 && ix+1<TArry.GetSize()
			&& (TArry[ix-1].key==CN_STRUCT||TArry[ix-1].key==CN_UNION) 
			&& TArry[ix].key==CN_STRUCTTYPE &&TArry[ix+1].key==CN_LFLOWER)
		{ /////跳过结构类型定义////修改layer域
			//AfxMessageBox("");			
			nflower=1;
			ix=ix+2;
			int beg=ix;
		
			while(ix<TArry.GetSize()&&nflower!=0)
			{
				if(TArry[ix].key==CN_LFLOWER)
					nflower++;
				if(TArry[ix].key==CN_RFLOWER)
					nflower--;
				ix++;
			}
			int end=ix;
			for(int ii=0;ii<SArry.GetSize();ii++)
			{/////修改结构类型定义时符号表的layer域
				if(SArry[ii].taddr>beg&&SArry[ii].taddr<end)
					SArry[ii].layer=nlayer;
			}
			ix++;
		}
		if( ix-1>=0&&ix+1<TArry.GetSize()&&(TArry[ix-1].key!=CN_LCIRCLE || ix-1==FD.pbeg)&& IsVariableType(ix,TArry) && TArry[ix+1].key!=CN_RCIRCLE )
	
		{
			// initialization of the SNODE "SD"
			SD.type=TArry[ix].key;
			if(ix>=1&&TArry[ix].key==CN_STRUCTTYPE&&(TArry[ix-1].key==CN_STRUCT||TArry[ix-1].key==CN_UNION))
			{
				SD.sv=FindPosition(TArry[ix].name,TArry[ix-1].key,SArry);///wn
			
			}
			else 
			{	
				SD.sv=-1;
			}
			SD.mv=-2;///wn			
			SD.layer=nlayer; 
			
			///////////wtt///////////2005.10.18//将float转换成double///
			if(SD.type==CN_FLOAT)
			{
				SD.type=CN_DOUBLE;
			}
			//////////////////////////////////////
			SD.name=_T("");
			
			//CString s;
			//s.Format("line=%d,key=%d,nanme=%s",TArry[ix].line,TArry[ix].key,TArry[ix].name);   
            //AfxMessageBox(s);
             
			ix++;

			while(ix>=1&&ix<TArry.GetSize()&&(!IsVariableType(ix,TArry))&&(TArry[ix].key!=CN_LINE)&&(TArry[ix].line==TArry[ix-1].line))
			{
				
				if(TArry[ix].key==CN_VARIABLE || TArry[ix].key==CN_MULTI)
				{
					 beforvariable=ix-1;
					if(ix<FD.bend && ix+1<TArry.GetSize()&&TArry[ix+1].key==CN_LCIRCLE)
					{
						//declare of function
						int nrect=1;
						int line=TArry[ix].line; 
						int pos=ix;

						ix+=2;

						while(ix<FD.bend && ix>=0&&ix<TArry.GetSize()&&TArry[ix].line<=line && nrect!=0)
						{
							if(TArry[ix].key==CN_LCIRCLE)
							{
								nrect++;
							}
							if(TArry[ix].key==CN_RCIRCLE)
							{
								nrect--;
							}

							ix++;
						}

						if(ix>=FD.bend)
						{
							return;
						}

						if(TArry[ix].line>line)
						{
							break;
						}

						ix--;

						for(;pos<ix;pos++)
						{
							if(TArry[pos].key==CN_VARIABLE)
							{///[ ]内为变量时显示“字符串”
								TArry[pos].key=CN_CSTRING;
							} 
						}
						continue;
					}
				
					if(TArry[ix].key==CN_MULTI)
					{
						SD.kind=CN_POINTER;
						SD.value=1;
						
						ix++;

								//////////////wtt//////////////////
                        if(TArry[ix].key!=CN_VARIABLE)
						{	//error	
												
							return ;		
						}
					
						//////////////////////wtt////////
					}
					else
					{
						SD.kind=CN_VARIABLE; 
					}

					if(TArry[ix].key==CN_VARIABLE)
					{

						SD.addr=-1;
						SD.arrdim=0;
						for(int i=0;i<4;i++)
						{
							SD.arrsize[i]=0; 
						}
						SD.name=TArry[ix].name;

						ix++;
						
						if(ix<FD.bend && TArry[ix].key==CN_LREC)
						{
							if(SD.kind==CN_POINTER)
							{
								SD.kind=CN_ARRPTR; 
							}
							else
							{
								SD.kind=CN_ARRAY; 
							}
							ArrayDim(ix,SD,TArry);
							ix--;
							
						}

						
					
                        if(!VariableExisted(SD,SArry))
						{		
							if(beforvariable>=0&&beforvariable<TArry.GetSize()&&TArry[beforvariable].key==CN_DOU||IsVariableType(beforvariable,TArry))
							{////wn
								SD.taddr=beforvariable+1;
							//	if(TArry[SD.taddr-1].key!=CN_DOT)
								
							//		SArry.Add(SD);
							}
							else
								{
														
								return;
								}

								

						}
						else
						{
							// error;
							if(beforvariable>=0&&beforvariable<TArry.GetSize()&&TArry[beforvariable].key!=CN_EQUAL&&VariableExisted(SD,SArry))
							{			
								return;
							}
						
						}
					}
				}
/////////////////////////////////////////wtt//////////////////////////////////

				if(!IsVariableType(ix,TArry))
				{
					ix++;
				}

			}// while((!IsVariableType(ix,TArry))&...
		

			if( ix>0 && ix<TArry.GetSize() && TArry[ix].line==TArry[ix-1].line || IsVariableType(ix,TArry))
			{
				ix--;
			}

		}// if(TArry[ix-1].key!=CN_LCIRCLE && IsVar...
        
		ix++;
	}

}

void CLexical::NewTypedefReplace(CArray<TNODE,TNODE> &TArry, HeaderFileElem &tempHFileElm)
{
	int i;
	int delBeg = 0, delEnd = 0;
	CArray<TypedefRecordElem,TypedefRecordElem> &tpdfRcdArry = tempHFileElm.typedefRcdArry;
	for( i = 0; i < TArry.GetSize(); i++ )
	{
		int j;
		CString word = TArry[i].srcWord;
		if( TArry[i].key == CN_TYPEDEF )
		{
			delBeg = i;
			CArray<TNODE,TNODE> tmpTpdfArry;//记录一个完整的typedef定义除去关键字"typedef"后所有的token
			i++;
			if(FindCompleteTypedef(i,TArry,tmpTpdfArry))
			{
				delEnd = i;
				int k;
				for( k = 0; k < tmpTpdfArry.GetSize(); k++ )
				{//对新识别的typedef定义部分进行typedef替换
					int jj;
					for( jj = 0; jj < tpdfRcdArry.GetSize(); jj++ )
					{
						if( tmpTpdfArry[k].name == tpdfRcdArry[jj].typeRename )
						{
							//进行替换
							int line = tmpTpdfArry[k].line;
							int srcLine = tmpTpdfArry[k].srcLine;
							if(i-1>=0 && i-1<TArry.GetSize()) word = TArry[i-1].srcWord;
							if(  i>=0 &&   i<TArry.GetSize()) word = TArry[i].srcWord;
							tmpTpdfArry.RemoveAt(k);
							int kj;
							for( kj = 0; kj < tpdfRcdArry[jj].typeTArry.GetSize(); kj++ )
							{
								tpdfRcdArry[jj].typeTArry[kj].line = line;
								tpdfRcdArry[jj].typeTArry[kj].srcLine = srcLine;
								tmpTpdfArry.InsertAt(k++,tpdfRcdArry[jj].typeTArry[kj]);
								if(i-2>=0 && i-2<TArry.GetSize()) word = TArry[i-2].srcWord;
								if(i-1>=0 && i-1<TArry.GetSize()) word = TArry[i-1].srcWord;
								if(  i>=0 &&   i<TArry.GetSize()) word = TArry[i].srcWord;

							}
						}
					}
				}
				IncreaseTypedefRcdArry(tmpTpdfArry,tempHFileElm);
				for( k = delBeg; k <= delEnd; k++ )
				{//从TArry中删除typedef定义对应的token
					word = TArry[delBeg].srcWord;
					TArry.RemoveAt(delBeg);
					i=delBeg-1;
				}
			}
			else
			{
				i = delBeg+1;
			}

		}
		else 
		{
			for( j = 0; j < tpdfRcdArry.GetSize(); j++ )
			{
				if( TArry[i].key == CN_VARIABLE 
					&& TArry[i].name == tpdfRcdArry[j].typeRename )
				{//进行替换
					int line = TArry[i].line;
					int srcLine = TArry[i].srcLine;
					TArry.RemoveAt(i);
					if(i-1>=0 && i-1<TArry.GetSize()) word = TArry[i-1].srcWord;
					if(  i>=0 &&   i<TArry.GetSize()) word = TArry[i].srcWord;
					int preI = i-1;
					int ij;
					for( ij = 0; ij < tpdfRcdArry[j].typeTArry.GetSize(); ij++ )
					{
						tpdfRcdArry[j].typeTArry[ij].line = line;
						tpdfRcdArry[j].typeTArry[ij].srcLine = srcLine;
						TArry.InsertAt(i++,tpdfRcdArry[j].typeTArry[ij]);
						if(i-2>=0 && i-2<TArry.GetSize()) word = TArry[i-2].srcWord;
						if(i-1>=0 && i-1<TArry.GetSize()) word = TArry[i-1].srcWord;
						if(  i>=0 &&   i<TArry.GetSize()) word = TArry[i].srcWord;
					}
					
				}
			}
		}
	}

}

bool CLexical::FindCompleteTypedef(int &i,CArray<TNODE,TNODE> &TArry, CArray<TNODE,TNODE> &tmpTpdfArry)
{
	int lineFlag = 0;
	int lFlwFlag = 0;
	int lFlwCnt = 0, rFlwCnt = 0;
	while( i>=0 && i < TArry.GetSize() )
	{
		CString word = TArry[i].srcWord;
		tmpTpdfArry.Add(TArry[i]);

		if( TArry[i].key==CN_LINE && lFlwFlag == 0 )
		{//typedef和";"之间没有{，或者Typedef和";"之间的{}已经匹配完毕，则已经识别了完整的typedef
			return true;
		}
		else if( TArry[i].key==CN_LFLOWER )
		{
			if( lineFlag == 0 && lFlwFlag == 0 )
			{
				lFlwFlag = 1;
			}
			lFlwCnt ++;
		}
		else if( TArry[i].key == CN_RFLOWER )
		{
			rFlwCnt++;
		}

		if( lFlwCnt == rFlwCnt && lFlwFlag == 1)
		{
			lFlwFlag = 0;
		}
		i++;
	}
	return false;

}

void CLexical::IncreaseTypedefRcdArry(CArray<TNODE,TNODE> &tmpTpdfArry,HeaderFileElem &tempHFileElm)
{
	TypedefRecordElem tpdfRcdElm;
	int i;

	for( i = 0; i < tmpTpdfArry.GetSize(); i++ )
	{
		if( i+3 < tmpTpdfArry.GetSize() )
		{//识别指针定义
			if( tmpTpdfArry[i].key == CN_LCIRCLE
				&& tmpTpdfArry[i+1].key == CN_MULTI
				&& tmpTpdfArry[i+2].key == CN_VARIABLE
				&& tmpTpdfArry[i+3].key == CN_RCIRCLE )
			{
				tmpTpdfArry.RemoveAt(tmpTpdfArry.GetSize()-1);//移除最后的";"
				tpdfRcdElm.typeRename = tmpTpdfArry[i+2].name;
				tmpTpdfArry.RemoveAt(i+2);
				tpdfRcdElm.typeTArry.Copy(tmpTpdfArry);
				tempHFileElm.typedefRcdArry.Add(tpdfRcdElm);
				tpdfRcdElm.Clean();
				return;
			}
		}
	}

	if(tmpTpdfArry[tmpTpdfArry.GetSize()-2].key == CN_RREC)
	{//识别数组定义
		int rRecCnt = 1, lRecCnt = 0;
		i = tmpTpdfArry.GetSize()-3;
		while( i >= 0 )
		{
			if( tmpTpdfArry[i].key == CN_LREC )
			{
				lRecCnt++;
				if( rRecCnt == lRecCnt 
					&& i-1>=0 && tmpTpdfArry[i-1].key == CN_VARIABLE )
				{
					tmpTpdfArry.RemoveAt(tmpTpdfArry.GetSize()-1);//移除最后的";"
					tpdfRcdElm.typeRename = tmpTpdfArry[i-1].name;
					tmpTpdfArry.RemoveAt(i-1);
					tpdfRcdElm.typeTArry.Copy(tmpTpdfArry);
					tempHFileElm.typedefRcdArry.Add(tpdfRcdElm);
					tpdfRcdElm.Clean();
					return;
				}
			}
			else if( tmpTpdfArry[i].key == CN_RREC )
			{
				rRecCnt++;
			}
			i--;
		}
	}
	else
	{//识别其他定义
		i = tmpTpdfArry.GetSize()-2;
		if( tmpTpdfArry[i].key == CN_VARIABLE && tmpTpdfArry[i+1].key == CN_LINE )
		{
			tmpTpdfArry.RemoveAt(tmpTpdfArry.GetSize()-1);//移除最后的";"
			tpdfRcdElm.typeRename = tmpTpdfArry[i].name;
			tmpTpdfArry.RemoveAt(i);
			tpdfRcdElm.typeTArry.Copy(tmpTpdfArry);
			tempHFileElm.typedefRcdArry.Add(tpdfRcdElm);
			tpdfRcdElm.Clean();
			return;
		}
	}
}

void CLexical::GenerateCleanFilePath(CString &filepath)
{
	int head=0;
	head = filepath.Find("\\",0);
	int preHead;
	while( head>=0 && head<filepath.GetLength() )
	{
		preHead = head;
		head = filepath.Find("\\",head+1);
	}
	filepath.Delete(preHead,filepath.GetLength()-preHead);

}

void CLexical::CFileMacroReplace(CString &program, CArray<HeaderFileElem,HeaderFileElem> &tempHFileArry)
{

	CString outPath;
	outPath = out_file_path + "\\行数.txt";
	HeaderFileElem tempHFileElm;
	int idx;
	for( idx = 0; idx < tempHFileArry.GetSize(); idx++ )
	{
		tempHFileElm.macroReplaceArry.Append(tempHFileArry[idx].macroReplaceArry);
	}

	int lineEnd = 0, lineBeg = 0;

	CString tempProg("");//记录被宏替换过的源程序

	lineEnd = program.Find("\n",lineBeg)+1;
	int lineCnt = 0;
	while( lineEnd > 0 && lineEnd < program.GetLength() )
	{
		CString &codeLine = program.Mid(lineBeg,lineEnd-lineBeg);
		lineCnt++;
		int defineFlag = 0;
		int pos =  codeLine.Find("#define",0);

		if( pos >= 0 )
		{//如果找到"#define"，则宏定义标志defineFlag设为1
			defineFlag = 1;
		}
		while( pos > 0 )
		{//如果该行字符串中#define之前出现了非空格字符，则defineFlag设为0，说明该行非宏定义
			pos -= 1;
			if( codeLine.Mid(pos,1) != " " )
			{
				defineFlag = 0;
			}
		}

		//如果codeLine是宏定义(或一部分)，则识别宏定义添加tempHFileElm的宏定义列表
		if( defineFlag == 1 )
		{//进行宏定义的识别和存储
			CString Macro_Name=_T(""),Macro_Value=_T("");
			if(!Genrate_Macro_NV(program,lineBeg,Macro_Name,Macro_Value))
			{//lineBeg自动跳到宏定义结束后的位置
				int i;
				int isExist = 0;
				for( i = 0; i < tempHFileElm.macroReplaceArry.GetSize(); i++ )
				{
					if( Macro_Name == tempHFileElm.macroReplaceArry[i].macroName )
					{
						isExist = 1;//该宏定义已经被存储
					}
				}
				if( isExist == 0 )
				{
					tempHFileElm.AddMacroReplaceArry(Macro_Name,Macro_Value);
				}
			}
			tempProg += "\r\n";

		}

		//如果codeLine不是宏定义，扫描tempHFileElm的宏定义列表，进行宏定义的替换
		else 
		{
			if( codeLine == "\r\n" || codeLine == "\n" )
			{
				lineEnd = program.Find("\n",lineBeg)+1;
				lineBeg = lineEnd;
				tempProg += codeLine;
			}
			else
			{
/*******************************			
				while( codeLine.Replace("(","(") != codeLine.Replace(")",")") )
				{//不断增加codeLine，知道得到完整的()匹配为止
					lineBeg = lineEnd;
					lineEnd = program.Find("\n",lineBeg)+1;
					codeLine += program.Mid(lineBeg,lineEnd-lineBeg);
				}/*************///wq-20090611-只替换单行代码的宏

				int i;
				CString Macro_Name=_T(""),Macro_Value=_T("");
				for(i = 0; i < tempHFileElm.macroReplaceArry.GetSize(); i++)
				{//扫描tempHFileElm的宏定义列表，进行宏定义的替换
					Macro_Name = tempHFileElm.macroReplaceArry[i].macroName;
					Macro_Value = tempHFileElm.macroReplaceArry[i].macroValue;
					Span_Define(codeLine,0,Macro_Name,Macro_Value);//宏定义替换函数
				}
				if( lineBeg >= lineEnd )
				{
	//				break;
				}
				lineBeg = lineEnd;
				tempProg += codeLine;
			}
		}		
//		lineBeg = lineEnd;
		lineEnd = program.Find("\n",lineBeg)+1;
/*		CStdioFile file;
		CString strLCnt;
		strLCnt.Format("%d",lineCnt);
		strLCnt+="\r\n";
		if(file.Open( outPath,CFile::modeWrite))
		{
			file.SeekToEnd();
			file.WriteString(strLCnt);
			file.Close();
		}
		else if(file.Open( outPath,CFile::modeCreate|CFile::modeWrite))
		{
			file.WriteString(strLCnt);
			file.Close();
		}*/
	}
//    program = "";
//	program = tempProg;
}

int CLexical::HFileSumCnt(CString &program, const CString cFilePath)
{
	int hfileSum = 0;
	Delete_Comment(program);
	int count=program.Find("include",0);//查找cprogram中第一个include的位置
	while(count>=0 && count<program.GetLength())
	{
		CString hfilename=_T("");//用于存放未处理的头文件名 (<xxx.h>或"xxx.h")
		if(ISHAVE_WELL(program,count,7))//前面没有"#",不是文件包含语句！
		{
			count=program.Find("include",count+7);
			continue;
		}
		count=count+7;//count指向"include"后面的那个字符
		if(count>0 && count<program.GetLength())
		{
			int line_end=program.Find('\n',count);//查找include所在行的末尾位置，存于line_end
			if(line_end>count)
			{
				int pos_inline=0;
				hfilename=program.Mid(count,line_end-count);//line_end-count是头文件名字的长度
				Deal_SomeLine(count,pos_inline,program,1);//用"\r\n"替换include所在行，
				HeaderFileElem tempHFileElm;
				CArray<CString,CString> hFileNameArry;
				CountHFile(hfilename,cFilePath,hFileNameArry,hfileSum);
			}
		}
		count=program.Find("include",count);//查找cprogram中下一个include的位置
	}
	return hfileSum;

}

void CLexical::CountHFile(CString &hfilename, const CString filePath, CArray<CString,CString> &hFileNameArry, int &hfileCnt)
{

	CString tempFilepath = filePath;
	GenerateCleanFilePath(tempFilepath);

	CString hprogram;
	Out_TokenSeq readh;
	if(BuildHFileFullPath(hfilename,tempFilepath))
	{
		int sp = 0;
		for( sp = 0; sp<hFileNameArry.GetSize(); sp++ )
		{
			if( hfilename.CompareNoCase(hFileNameArry[sp]) == 0 )
			{
				return;
			}
		}

		hFileNameArry.Add(hfilename);

		int index = -1;
		if(hfilename!="" && hfilename!="NULL" )
		{
			readh.READ_CHFILE(hfilename,hprogram);//读头文件
			
			if( hprogram == "" )
			{
				return;
			}
			hfileCnt++;

				
			//找当前所读头文件所包含的头文件名hfilename2，然后递归调用ProcessHFile()
			Delete_Comment(hprogram);
			int count=hprogram.Find("include",0);//查找cprogram中第一个include的位置
			while(count>=0 && count<hprogram.GetLength())
			{
				CString hfilename2=_T("");//用于存放未处理的头文件名 (<xxx.h>或"xxx.h")
				if(ISHAVE_WELL(hprogram,count,7))//前面没有"#",不是文件包含语句！
				{
					count=hprogram.Find("include",count+7);
					continue;
				}
				count=count+7;//count指向"include"后面的那个字符
				if(count>0 && count<hprogram.GetLength())
				{
					int line_end=hprogram.Find('\n',count);//查找include所在行的末尾位置，存于line_end
					if(line_end>count)
					{
						int pos_inline=0;
						hfilename2=hprogram.Mid(count,line_end-count);//line_end-count是头文件名字的长度
						Deal_SomeLine(count,pos_inline,hprogram,1);//用"\r\n"替换include所在行，
						CountHFile(hfilename2,hfilename,hFileNameArry,hfileCnt);
					}
				}
				count=hprogram.Find("include",count);//查找hprogram中下一个include的位置
			}
		}
	}

}
