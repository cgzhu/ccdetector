// AssignmentUnit.h: interface for the CAssignmentUnit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ASSIGNMENTUNIT_H__E3DA19D3_778A_4596_8380_6C31DD59B139__INCLUDED_)
#define AFX_ASSIGNMENTUNIT_H__E3DA19D3_778A_4596_8380_6C31DD59B139__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CAssignmentUnit : public CSDGBase  
{
public:
	CAssignmentUnit();
	virtual ~CAssignmentUnit();
public:
	ENODE *m_pleft;
	int    m_iAdvl;
public:
	ENODE *GetExpression();
	

};

#endif // !defined(AFX_ASSIGNMENTUNIT_H__E3DA19D3_778A_4596_8380_6C31DD59B139__INCLUDED_)
