// CP_SEG.cpp: implementation of the CP_SEG class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CP_SEG.h"
#include "constdata.h"
#include "dataStruct.h"
#include "Lexical.h"
#include "FormatTokenLines.h"
#include "FuncBndBuild.h"
#include "FilesToTokens.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CP_SEG::CP_SEG()
{
	filename = _T("");
	FN_Hvalue = 0;
	LPOS = _T("");
	relaLPOS = _T("");
	Beg_index = 0;  
	End_index = 0;  
	B_gap = 0;
	E_gap = 0;
	Is_combine = FALSE;
	combineTimes = 0;
	//wq-20081224-add
	funcBegLine = 0;
	funcEndLine = 0;

	funcBgnRealLine = 0;
	funcEndRealLine = 0;

	idsLocArray.RemoveAll();//wq-20090223-add
}
CP_SEG::CP_SEG(const CP_SEG& cp_seg)
{
	filename = cp_seg.filename;
	FN_Hvalue = cp_seg.FN_Hvalue;
	LPOS = cp_seg.LPOS;
	relaLPOS = cp_seg.relaLPOS;
	Beg_index = cp_seg.Beg_index;  
	End_index = cp_seg.End_index;  
	B_gap = cp_seg.B_gap;
	E_gap = cp_seg.E_gap;
	Is_combine = cp_seg.Is_combine;
	combineTimes = cp_seg.combineTimes;
	//wq-20081224-add
	funcBegLine = cp_seg.funcBegLine;
	funcEndLine = cp_seg.funcEndLine;
	funcBgnRealLine = cp_seg.funcBgnRealLine;
	funcEndRealLine = cp_seg.funcEndRealLine;

	idsLocArray.RemoveAll();//wq-20090223-add
	idsLocArray.Append(cp_seg.idsLocArray);//wq-20090223-add

}
CP_SEG::~CP_SEG()
{
	filename = _T("");
	FN_Hvalue = 0;
	LPOS = _T("");
	relaLPOS = _T("");
	Beg_index = 0;  
	End_index = 0;  
	B_gap = 0;
	E_gap = 0;
	Is_combine = FALSE;
	combineTimes = 0;
	//wq-20081224-add
	funcBegLine = 0;
	funcEndLine = 0;

	funcBgnRealLine = 0;
	funcEndRealLine = 0;

	idsLocArray.RemoveAll();//wq-20090223-add

}

CP_SEG& CP_SEG::operator=(const CP_SEG& cp_seg)
{
	filename = cp_seg.filename;
	FN_Hvalue = cp_seg.FN_Hvalue;
	LPOS = cp_seg.LPOS;
	relaLPOS = cp_seg.relaLPOS;
	Beg_index = cp_seg.Beg_index;  
	End_index = cp_seg.End_index;  
	B_gap = cp_seg.B_gap;
	E_gap = cp_seg.E_gap;
	Is_combine = cp_seg.Is_combine;
	combineTimes = cp_seg.combineTimes;

	//wq-20081224-add
	funcBegLine = cp_seg.funcBegLine;
	funcEndLine = cp_seg.funcEndLine;
	funcBgnRealLine = cp_seg.funcBgnRealLine;
	funcEndRealLine = cp_seg.funcEndRealLine;

	idsLocArray.RemoveAll();//wq-20090223-add
	idsLocArray.Append(cp_seg.idsLocArray);//wq-20090223-add

	return *this;
}

/*******************wq-20090223-add**********************
void  CP_SEG::UpdateIdsLocArray(FilesToTokens& filesToTokens)
{
	idsLocArray.RemoveAll();
	int fileIndex = filesToTokens.FindFileName(FN_Hvalue,filename);
	CString fileRead;
	ReadCFile(filename,fileRead);
	TokenedFile& tokenedFile = filesToTokens.tokenFileArray[fileIndex];
	ASSERT( filename == tokenedFile.GetFileName() );
	ASSERT( FN_Hvalue == tokenedFile.GetHashedFileName() );
	CArray<TokenLine,TokenLine>& tokenList = tokenedFile.GetTokenList();

	int i;
	for( i = 0; i < relaLPOS.GetLength()/4; i++ )
	{
		int line = atoi(relaLPOS.Mid(i*4,4));
		CArray<TNODE,TNODE>& tokenLine = tokenList[line-1].GetTokenLine();
		int j;
		for( j = 0; j < tokenLine.GetSize(); j++ )
		{
			CString strWord = tokenLine[j].srcWord;
			int key = tokenLine[j].key;

			if( tokenLine[j].key == CN_VARIABLE )
			{
				CString idName = tokenLine[j].srcWord;
				IDENTF_LOCATION idLoc;
				idLoc.lineIndex = i;//line;
				idLoc.tokenNum = j;
				int k;
				int addFlag = 0;
				for( k = 0; k < idsLocArray.GetSize(); k++ )
				{
					if( idName == idsLocArray[k].GetIdName() )
					{
						idsLocArray[k].idLocArry.Add(idLoc);
						addFlag = 1;
						break;
					}
				}
				if( addFlag == 0 )
				{
					IdLocSetMem idLocSetMem;
					idLocSetMem.idName = idName;
					idLocSetMem.idLocArry.Add(idLoc);
					idsLocArray.Add(idLocSetMem);
				}
			}
		}
	}
}
/***************************************************************/

/*******************wq-20090520-changed**********************/
void  CP_SEG::UpdateIdsLocArray(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry)
{
	idsLocArray.RemoveAll();
//	int fileIndex = filesToTokens.FindFileName(FN_Hvalue,filename);
	CString program;
	ReadCFile(filename,program);

	CArray<TNODE,TNODE> TArry;
	CArray<SNODE,SNODE> SArry;
	CArray<FNODE,FNODE> FArry;
	CArray<CERROR,CERROR>   EArry;
	CArray<VIPCS,VIPCS>     VArry;	
	CArray<CH_FILE_INFO,CH_FILE_INFO> filesReadInfo;
//	CArray<HeaderFileElem,HeaderFileElem> headerFilesArry;
	CLexical clc;
//	clc(0,program,VArry,EArry,TArry,SArry,FArry);
	clc(0,program,VArry,EArry,TArry,SArry,FArry,headerFilesArry,filename,filesReadInfo);

	FormatTokenLines ftls;
	ftls(TArry,clc.srcCode);
	FuncBndBuild fbb;
	fbb(TArry);
	FilesToTokens filesToTokens;
	filesToTokens.ChangTArryToTokenLines(TArry,SArry,FArry,filename);
	if( filesToTokens.tokenFileArray.GetSize()<=0 )
	{
		return;
	}
	TokenedFile& tokenedFile = filesToTokens.tokenFileArray[0];
	CArray<TokenLine,TokenLine>& tokenList = tokenedFile.GetTokenList();

	int i;
	for( i = 0; i < relaLPOS.GetLength()/4; i++ )
	{
		int line = atoi(relaLPOS.Mid(i*4,4));
		if( line > tokenList.GetSize() )
		{
			continue;
		}
		CArray<TNODE,TNODE>& tokenLine = tokenList[line-1].GetTokenLine();
		int j;
		for( j = 0; j < tokenLine.GetSize(); j++ )
		{
			CString strWord = tokenLine[j].srcWord;
			int key = tokenLine[j].key;

			if( tokenLine[j].key == CN_VARIABLE )
			{
				CString idName = tokenLine[j].srcWord;
				IDENTF_LOCATION idLoc;
				idLoc.lineIndex = i;//line;
				idLoc.tokenNum = j;
				int k;
				int addFlag = 0;
				for( k = 0; k < idsLocArray.GetSize(); k++ )
				{
					if( idName == idsLocArray[k].GetIdName() )
					{
						idsLocArray[k].idLocArry.Add(idLoc);
						addFlag = 1;
						break;
					}
				}
				if( addFlag == 0 )
				{
					IdLocSetMem idLocSetMem;
					idLocSetMem.idName = idName;
					idLocSetMem.idLocArry.Add(idLoc);
					idsLocArray.Add(idLocSetMem);
				}
			}
		}
	}
}
/***************************************************************/

bool CP_SEG::ReadCFile(const CString cfilename, CString &program)
{
	CStdioFile file;
	CString str=_T("");
	
	if( file.Open(cfilename,CFile::modeRead|CFile::typeText) == 0 )
	{
	   //str="读文件"+cfilename+"失败!\n\n"+"请检查是否已创建该文件！";
	   //AfxMessageBox(str);
	   return 0;
	}
	else
	{  
		str=_T("");
	    program=_T("");
		while(file.ReadString(str))
		{
			program=program+str+"\n";
			str=_T("");
		}
		file.Close();
		return 1;
	}

}
