// ConstData.h: interface for the CConstData class.
// dyc
//////////////////////////////////////////////////////////////////////
#include<afx.h>
#ifndef CONSTDATA_H
#define CONSTDATA_H

#define Min_Token_Num 10 //若源程序所含的Token字小于这个值，则不与分析
#define Block_ThrA 6    //若模块小于这个阈值，就不继续细分
#define Block_ThrB 12   //若模块大于这个阈值，且无法细分，则每行独自成块
#define Block_ThrC 2	  //若前后两模块同层，且规模之和小于等于这个阈值，就将它们合并
#define Max_gap 3             //碎片最大Gap阈值
#define Min_den 0.75		  //碎片最小密度阈值
//#define Min_len 6			  //输出的重复代码至少应含的行数//20090310
#define Switch_threshold 0.6  //switch语句中，只含"case"或"break"的数目与总行数之比，若大于此阈值，删之！
//const CString Obj_file_path="D:\\Obj_Src";//实验目标文件夹的路径

typedef  enum CLANGUAGE{
	
	//BEGIN KEYWORDS
	CN_AUTO=1, 
	CN_BREAK=2,
	CN_CASE=3,
	CN_CHAR=4,
	CN_CONST=5,
	CN_CONTINUE=6,
    CN_DEFAULT=7,
	CN_DO=8,
	CN_DOUBLE=9,
	CN_ELSE=10,
	CN_ENUM=11,
	CN_EXTERN=12,
	CN_FLOAT=13,
    CN_FOR=14,
    CN_GOTO=15,
	CN_IF=16,
	CN_INT=17,
	CN_LONG=18,
	CN_MAIN=19,
	CN_REGISTER=20,
	CN_RETURN=21,
	CN_SHORT=22,  
	CN_SIGNED=23,
	CN_SIZEOF=24,
	CN_STATIC=25,
	CN_STRUCT=26,
	CN_SWITCH=27,
	CN_TYPEDEF=28,
	CN_UNION=29,
	CN_UNSIGNED=30,
	CN_VOID=31,
	CN_VOLATILE=32,
	CN_WHILE=33,  
	//END KEYWORDS

	//OPERATIONAL & BOUNDARY CHARACTERS
	CN_WELL=34,         // #
	CN_FORMAT=35,       // %
	CN_HELP=36,         // ?
	CN_WNTOES=37,       // '\'
	CN_NOT=38,          // !
	CN_OR=39,           // |
	CN_AND=40,          // &
    CN_XOR=41,          // ^
	CN_BITBU=42,        // ~
	CN_DAND=43,         // &&
	CN_DOR=44,          // ||
	CN_LMOVE=45,        // <<
    CN_RMOVE=46,        // >>
    CN_EQUAL=47,        // =
	CN_ADD=48,          // +
	CN_SUB=49,          // -
	CN_MULTI=50,        // *
	CN_DIV=51,          // / 
	CN_UPDOT=52,        // '
	CN_DOT=53,          // .
	CN_INCLUDE=54,      // "	
	CN_DOU=55,          // ,
	CN_SHOW=56,         // : 
	CN_LINE=57,         // ;   
	CN_GREATT=58,       // >
	CN_LESS=59,         // <
	CN_LANDE=60,        // <= 
	CN_BANDE=61,        // >=
	CN_NOTEQUAL=62,     // !=
	CN_DEQUAL=63,       // == 
    CN_DADD=64,         // ++ 
	CN_DSUB=65 ,        // --
	CN_STRPTR=66,       // ->
	CN_LCIRCLE=67,      // (
    CN_RCIRCLE=68,      // ) 
	CN_LREC=69,         // [ 
 	CN_RREC=70,         // ]
	CN_LFLOWER=71,      // {
    CN_RFLOWER=72,      // }
	CN_ADDEQUAL=73,     // +=
	CN_SUBEQUAL=74,     // -=
	CN_MULEQUAL=75,     // *=
	CN_DIVEQUAL=76,     // /=
	CN_YUEQUAL=77,      // %=
	CN_RMOVEEQUAL=78,   // >>=
	CN_LMOVEEQUAL=79,   // <<=
	CN_ANDEQUAL=80,     // &=
	CN_XOREQUAL=81,     // ^=
	CN_OREQUAL=82,      // |=	
	 
    //END OF THIS PART

	CN_VARIABLE=83,     
	CN_ARRAY=84,
	CN_POINTER=85,
	CN_ARRPTR=86,
	CN_STRING=87,
	//常数型
	CN_CINT=88,
	CN_CCHAR=89,
	CN_CLONG=90,
	CN_CFLOAT=91,
	CN_CDOUBLE=92,
	CN_CSTRING=93,

	CN_BE=94,
	CN_AE=95,
    
	////////////////SDG中的语句关键字类型/////////////////
	CN_ASSIGNMENT=96,     // assignment statement		//
	CN_SWITCHYJ=97,       // case statement				//
	CN_DOYJ=98,           // do-while statement			//
	CN_FORYJ=99,          // for statement				//
	CN_IFYJ=100,         // if statement				//
	CN_IFELSEYJ=101,       // if-else statement			//
	CN_WHILEYJ=102,       // while statement			//
	CN_CSTOPYJ=103,       // ? : statement				// 
	CN_GOTOYJ=104,        // goto statement			    //  
	CN_CALLYJ=105,   // function invoking statement// 

	
	CN_DFUNCTION=106,     // functions defined by user  //
	CN_BFUNCTION=107,     // basical function           //
	CN_NULL=108,          // NULL                       // 


    /////////SDG中依赖关系类型的关键字////////////////////
	//--------------the relations in SDG----------------//
	CN_CONTROL=109,		  // control                    //
	CN_FLOW=110,          // folw                       //
	CN_CALL=111,          // call                       //
	CN_AFFECTRETURN=112,  // affection-return           //
	CN_RETURNCONTROL=113, // return-control             //
	CN_RETURNLINK=114,    // return-link                //
	CN_PARAMETERIN=115,   // parameter-in               //
	CN_PARAMETEROUT=116,  // parameter-out              //
	CN_TRANSITIVE=117,    // transitive                 //

	/////////SDG中节点类型关键字//////////////////////////
	//------------------node type-----------------------//
	CN_ENTRY=118,         // entry                      //
	CN_DECLARE=119,       // declare                    // 
	CN_SELECTORLINK=120,  //
	CN_IFELSELINK=121,    ///
	CN_INCORDEC=122,      ////原为121//wtt////6.6
	CN_DFCALL=123,        // call defined functin   
	CN_BFCALL=124,        // call basical function


    //////////头文件相关"标识符"/////////////////////////
	CN_INLIB=125,         // include   
	CN_DEFINE=126,        // define
	CN_STDIOH=127,         // stdio  
	CN_MATHH=128,          // math 
	CN_CTYPEH=129,         // ctype
	CN_STDLIBH=130,        // stdlib
	CN_STRINGH=131,         // string 
   

    CN_STRUCTTYPE=132,				 // wn//the kind of STRUCTTYPE
	CN_SPACE=133,           //空格？
	CN_FUNCDECLARE=134,     //函数声明
	CN_FILEINCLUDE=135,     //文件include?
	CN_MACRODEFINE=136,     //宏定义?

	CN_H=137,               // .h head file

	CN_SDEFINE=138,           //wn// struct define
	CN_FILE=139,					//wn//file type
	//------------------//wn//the function of dealing with file-------//
	H_BETWEEN_C=140,
	
	CN_RANDOMP=141  //wq-20090719-wtt用于指针指向分析 表示任意的变量

}KNODE;

const int BOUND=1000;
const CString STRKEYBEGIN="[@@@answer-begin]";
const CString STRKEYEND=  "[@@@answer-end]"; 
const CString STRPAPEREND="[@@@end-of-the-paper]";
const CString STRDATAEND= "[@@@SEND-DATA-OVER]";
const CString STRRECEIVED="[@@@RECEIVED-DATA]";
   
//----------------keywords------------------//
const int KEYNUM=33;
const CString  C_KEYWORD[34]={
	"####",    //  0
	"auto",    //  1
	"break",   //  2 
	"case",    //  3 
	"char",    //  4 
	"const",   //  5 
	"continue",//  6
	"default", //  7
	"do",      //  8
	"double",  //  9
	"else",    // 10
	"enum",    // 11
	"extern",  // 12  
	"float",   // 13
	"for",     // 14 
	"goto",    // 15 
	"if",      // 16
	"int",     // 17
	"long",    // 18
	"main",    // 19
	"register",// 20 
	"return",  // 21 
	"short",   // 22
	"signed",  // 23
	"sizeof",  // 24 
	"static",  // 25
	"struct",  // 26
	"switch",  // 27
	"typedef", // 28 
	"union",   // 29
	"unsigned",// 30
	"void",    // 31
	"volatile",// 32
	"while",   // 33
		
};

//-----------end of keywords----------------//

const int LIBNUM=4;
const CString C_INCLUDE[10]={
	"stdio",
	"math" ,
	"ctype",
	"stdlib",
	"string",
	/////////////////LX////////
	"local",
	"setjmp",
	"signal",
	"stdarg",
	"time"
	//////////////////LX////////
};
const CString STRFPBEG="<<begin>>";
const CString STRFPEND="<<end>>";

//-----------basical function libary--------//
const int BFUNNUM=85;
const CString C_FUNCTION[86]={
	// #include "math.h"
	"acos",	 // 0
	"asin",  // 1
	"atan",  // 2
	"atan2", // 3  
	"cos",   // 4
	"cosh",  // 5   
	"exp",   // 6  
	"fabs",  // 7   
	"floor", // 8  
	"fmod",  // 9
	"frexp", // 10   
	"log",   // 11  
	"log10", // 12   
	"modf",  // 13  
	"pow",   // 14
	"sin",   // 15   
	"sinh",  // 16  
	"sqrt",  // 17   
	"tan",   // 18  
	"tanh",  // 19 

	// #include "ctype.h"
	"isalnum",   // 20
	"isalpha",   // 21
	"iscntrl",   // 22 
	"isdigit",   // 23
	"isgraph",   // 24
	"islower",   // 25 
	"isprint",   // 26
	"ispunct",   // 27 
	"isspace",   // 28
	"isupper",   // 29
	"isxdigit",  // 30 
	"strcat",    // 31
	"strchr",    // 32
	"strcmp",    // 33
	"strcpy",    // 34
	"strlen",    // 35
	"strstr",    // 36
	"tolower",   // 37
	"toupper",   // 38

	// #include "stdio.h"
	"open",   // 39
	"close",     // 40
	"creat",     // 41
	"read",    // 42
	"write",    // 43
	"lseek",	// 44
	"feof",       // 45 
	"fgetc",     // 46
	"fgets",      // 47
	"fopen",     // 48
	"fprintf",   // 49
	"fputc",     // 50
	"fputs",     // 51
	"fread",     // 52
	"fscanf",    // 53
	"fseek",     // 54
	"ftell",     // 55
	"getw" ,	// 56
	"exit",		// 57
	"fwrite",    // 58 
	"fclose",    // 59
	"printf",  // 60 
	"getc",		// 61
	"ferror",  // 62
	"getchar",   // 63
	"gets",      // 64
	"clearerr",   // 65
	"rewind",    // 66
	"putc",      // 67
	"scanf",   // 68
	"puts",      // 69
	"putw",      // 70
	"inteof",      // 71
	"rename",   // 72	  
	"putchar",      // 73
	    
    // memory operation
	"calloc",    // 74
	"free",     // 75
	"malloc",    // 76
	"realloc",   //77

	// other
	"getch",     //78

	//time.h//wtt//8.29/
	"srand",     //79
	"time",      //80
	"fflush",	 //81
	"rand",      //82
	"perror"     //83
};

//---------end of basical functions---------//

//----------------OPT Array-----------------//
const char OPGARRAY[9][9]={
   
' ',' ',' ',' ',' ',' ',' ',' ',' ', 

' ','>','>','<','<','<','>','<','<', // 1 +运算在前

' ','>','>','<','<','<','>','<','<',// 2 -

' ','>','>','>','>','<','>','<','>', // 3 *

' ','>','>','>','>','<','>','<','>',// 4 /

' ','<','<','<','<','<','=','<', '<',// 5 (

' ','>','>','>','>',' ','>',' ', '>',// 6 )

' ','>','>','>','>',' ','>',' ', '>',// 7 i

' ','>','>','>','>','<','>','<','>' , //8 %//wtt

//   +   -   *   /   (   )   i   %    
//运算在后
};
//----------------end-----------------//

//----------------OPT Array-----------------//
const char BOPARRAY[9][9]={
   
' ',' ',' ',' ',' ',' ',' ',' ',' ',

' ','>','>','<','>','>','<','>','<', // 1 rop关系运算符<,<=,>,>=，修改了!的优先级>关系运算符

' ','<','>','<','>','>','<','>','<', // 2 rop关系运算符==,!=,修改了

' ','>','>','<','>','>','<','>','<', // 3  !

' ','<','<','<','>','>','<','>','<', // 4  &&

' ','<','<','<','<','>','<','>','<', // 5  ||

' ','<','<','<','<','<','<','=','<', // 6  (

' ','>','>','>','>','>',' ','>',' ', //7   )

' ','>','>','>','>','>',' ','>',' ', //8   i

//  rop rop  !   &&  ||  (   )   i
};
//----------------end-----------------//

//----------------Bit OPT Array-----------------//
const char BITOPARRAY[9][9]={
   
' ',' ',' ',' ',' ',' ',' ',' ',' ',

' ','>','<','>','>','>','<','>','<', // 1 <<  >>  

' ','>','<','>','>','>','<','>','<', // 2  ~  

' ','>','<','>','>','>','<','>','<', // 3  &

' ','>','<','>','>','>','<','>','<', // 4  |

' ','>','<','>','>','>','<','>','<', // 5  ^

' ','<','<','<','<','<','<','=','<', // 6  (

' ','>','>','>','>','>',' ','>',' ', //7   )

' ','>','>','>','>','>',' ','>',' ', //8   i
// <<>>  ~   &   |   ^   (   )   i
};
//----------------end-----------------//

//////////////////////////////wtt test//////////////////////////////
const CString  C_ALL_KEYWORD[142]={
    " ",
	"CN_AUTO", 
	"CN_BREAK",
	"CN_CASE",
	"CN_CHAR",
	"CN_CONST",
	"CN_CONTINUE",
    "CN_DEFAULT",
	"CN_DO",
	"CN_DOUBLE",
	"CN_ELSE",
	"CN_ENUM",
	"CN_EXTERN",
	"CN_FLOAT",
    "CN_FOR",
    "CN_GOTO",
	"CN_IF",
	"CN_INT",
	"CN_LONG",
	"CN_MAIN",
	"CN_REGISTER",
	"CN_RETURN",
	"CN_SHORT",  
	"CN_SIGNED",
	"CN_SIZEOF",
	"CN_STATIC",
	"CN_STRUCT",
	"CN_SWITCH",
	"CN_TYPEDEF",
	"CN_UNION",
	"CN_UNSIGNED",
	"CN_VOID",
	"CN_VOLATILE",
	"CN_WHILE",  
	//END KEYWORDS

	//OPERATIONAL & BOUNDARY CHARACTERS
	"#",
	"%",
	"?",
	"\\",
	"!",
	"|",
	"&",
    "^",
	"~",
	"&&",
	"||",
	"<<",
    ">>",
    "=",
	"+",
	"-",
	"*",
	"/", 
	"\'",
	".",
	"\"",	
	",",
	":", 
	";",  
	">",
	"<",
	"<=", 
	">=",
	"!=",
	"==", 
    "++", 
	"--",
	"->",
	"(",
    ")", 
	"[", 
 	"]",
	"{",
    "}",
	"+=",
	"-=",
	"*=",
	"/=",
	"%=",
	">>=",
	"<<=",
	"&=",
	"^=",
	"|=",	
	"CN_VARIABLE",     
	"CN_ARRAY",
	"CN_POINTER",
	"CN_ARRPTR",
	"CN_STRING",
	"CN_CINT",
	"CN_CCHAR",
	"CN_CLONG",
	"CN_CFLOAT",
	"CN_CDOUBLE",
	"CN_CSTRING" ,
	"CN_BE",
	"CN_AE",

	"CN_ASSIGNMENT",     // assignment statement		//
	"CN_SWITCHYJ",       // case statement				//
	"CN_DOYJ",           // do-while statement			//
	"CN_FORYJ",         // for statement				//
	"CN_IFYJ",         // if statement				//
	"CN_IFELSEYJ",       // if-else statement			//
	"CN_WHILEYJ",       // while statement			//
	"CN_CSTOPYJ",       // ? : statement				// 
	"CN_GOTOYJ",        // goto statement			    //  
	"CN_CALLYJ",  // function invoking statement// 

	
	"CN_DFUNCTION",     // functions defined by user  //
	"CN_BFUNCTION",     // basical function           //
	"CN_NULL",          // NULL                       // 


	//--------------the relations in SDG----------------//
	"CN_CONTROL",		  // control                    //
	"CN_FLOW",          // folw                       //
	"CN_CALL",          // call                       //
	"CN_AFFECTRETURN",  // affection-return           //
	"CN_RETURNCONTROL", // return-control             //
	"CN_RETURNLINK",   // return-link                //
	"CN_PARAMETERIN",   // parameter-in               //
	"CN_PARAMETEROUT",  // parameter-out              //
	"CN_TRANSITIVE",    // transitive                 //

	//------------------node type-----------------------//
	"CN_ENTRY",         // entry                      //
	"CN_DECLARE",       // declare                    // 
	"CN_SELECTORLINK",  //
	"CN_IFELSELINK",    //
	"CN_INCORDEC",      //
	"CN_DFCALL",        // call defined functin   
	"CN_BFCALL",        // call basical function

	"CN_INLIB",         // include   
	"CN_DEFINE",        // define
	"CN_STDIOH",         // stdio  
	"CN_MATHH",          // math 
	"CN_CTYPEH",         // ctype
	"CN_STDLIBH",        // stdlib
	"CN_STRINGH"  ,	// string
    "CN_STRUCTTYPE",				 // wn//the kind of STRUCTTYPE
	"CN_SPACE",
	"CN_FUNCDECLARE",
	"CN_FILEINCLUDE",
	"CN_MACRODEFINE",

	"CN_H",              // .h head file

	"CN_SDEFINE",          //wn// struct define
	"CN_FILE",             //wn//file type
	"H_BETWEEN_C",          //LX

	"CN_RANDOMP"//wtt
	
};

////////////////

//////////////////////////wtt/////////////////////////////////////
const CString  CLX_ALL_KEYWORD[141]={
    " ",       //0
	"AUTO",    //1
	"BREAK",   //2
	"CASE",    //3
	"CHAR",//4
	"CONST",//5
	"CONTINUE",//6
    "DEFAULT",//7
	"DO",//8
	"DOUBLE",//9
	"ELSE",//10
	"ENUM",//11
	"EXTERN",//12
	"FLOAT",//13
    "FOR",//14
    "GOTO",//15
	"IF",//16
	"INT",//17
	"LONG",//18
	"MAIN",//19
	"REGISTER",//20
	"RETURN",//21
	"SHORT",  //22
	"SIGNED",//23
	"SIZEOF",//24
	"STATIC",//25
	"STRUCT",//26
	"SWITCH",//27
	"TYPEDEF",//28
	"UNION",//29
	"UNSIGNED",//30
	"VOID",//31
	"VOLATILE",//32
	"WHILE",  //33
	//END KEYWORDS

	//OPERATIONAL & BOUNDARY CHARACTERS
	"#",//34
	"%",//35
	"?",//36
	"\\",//37
	"!",//38
	"|",//39
	"&",//40
    "^",//41
	"~",//42
	"&&",//43
	"||",//44
	"<<",//45
    ">>",//46
    "=",//47
	"+",//48
	"-",//49
	"*",//50
	"/", //51
	"\'",//52
	".",//53
	"\"",	//54
	",",//55
	":", //56
	";",  //57
	">",//58
	"<",	//59
	"<=",	//60
	">=",	//61
	"!=",	//62
	"==",	//63
    "++",	//64
	"--",	//65
	"->",	//66
	"(",	//67
    ")",	//68
	"[",	//69
 	"]",	//70
	"{",	//71
    "}",	//72
	"+=",	//73
	"-=",	//74
	"*=",	//75
	"/=",	//76
	"%=",	//77
	">>=",	//78
	"<<=",	//79
	"&=",	//80
	"^=",	//81
	"|=",		//82
	"VARIABLE",	//83     
	"ARRAY",	//84
	"POINTER",	//85
	"ARRPTR",	//86
	"STRING",	//87
	"CINT",		//88
	"CCHAR",	//89
	"CLONG",	//90
	"CFLOAT",	//91
	"CDOUBLE",	//92
	"CSTRING" ,	//93
	"BE",		//94
	"AE",		//95

	"ASSIGNMENT",     // 96 assignment statement		//
	"SWITCHYJ",       // 97 case statement				//
	"DOYJ",           //  98 do-while statement			//
	"FORYJ",         //  99 for statement				//
	"IFYJ",         // 100 if statement				//
	"IFELSEYJ",       // 101 if-else statement			//
	"WHILEYJ",       // 102 while statement			//
	"CSTOPYJ",       // 103 ? : statement				// 
	"GOTOYJ",        // 104 goto statement			    //  
	"CALLYJ",  // 105 function invoking statement// 

	
	"DFUNCTION",     // 106 functions defined by user  //
	"BFUNCTION",     // 107 basical function           //
	"NULL",          // 108 NULL                       // 


	//--------------the relations in SDG----------------//
	"CONTROL",		  // 109 control                    //
	"FLOW",          // 110 folw                       //
	"CALL",          // 111 call                       //
	"AFFECTRETURN",  // 112 affection-return           //
	"RETURNCONTROL", // 113 return-control             //
	"RETURNLINK",   // 114 return-link                //
	"PARAMETERIN",   // 115 parameter-in               //
	"PARAMETEROUT",  // 116 parameter-out              //
	"TRANSITIVE",    // 117 transitive                 //

	//------------------node type-----------------------//
	"ENTRY",         // 118 entry                      //
	"DECLARE",       // 119 declare                    // 
	"SELECTORLINK",  // 120
	"IFELSELINK",    // 121
	"INCORDEC",      // 122
	"DFCALL",        // 123 call defined functin   
	"BFCALL",        // 124 call basical function

	"INLIB",         // 125 include   
	"DEFINE",        // 126 define
	"STDIOH",         // 127 stdio  
	"MATHH",          // 128 math 
	"CTYPEH",         // 129 ctype
	"STDLIBH",        // 130 stdlib
	"STRINGH"  ,	// 131 string
    "STRUCTTYPE",	// 132			 // wn//the kind of STRUCTTYPE
	"SPACE",		//133
	"FUNCDECLARE",	//134
	"FILEINCLUDE",	//135
	"MACRODEFINE",	//136

	"H",            //137  // .h head file

	"SDEFINE",      //138    //wn// struct define
	"FILE"          //139   //wn//file type
	"H_BETWEEN_C"   //140    //LX
};

/**wq-20090202-树控件项字符串常量**/
const CString TREE_ITEMS[] = {
	"克隆代码文件",
	"克隆代码组",
	"代码片段"
};
/**************************************************/

/*wq-20090210-add*******************************/
const CString SEGBEGIN       = "SEQ: ";
const CString SEQLENGTH      = "LEN: ";
const CString SEQSUP         = "SUP: ";
const CString FILEPATH       = "FilePath:";
const CString LINES       = "Lines:";
const CString HASHEDFILENAME = "HashedFileName:";
const CString RELALINE       = "RELAL:";
/**************************************************/

#endif