// SDGIf.cpp: implementation of the CSDGIf class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGIf.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CSDGIf::CSDGIf()
{
	m_sType="SDGIF";
	m_iNodeVL=-1;
	this->ELSEPOS=-1; 
	visit=0;

}

CSDGIf::~CSDGIf()
{

}
