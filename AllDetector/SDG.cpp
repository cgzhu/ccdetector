// SDG.cpp: implementation of the CSDG class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SDG.h"
#include "SDGAssignment.h"
#include "SDGEntry.h"
#include "SDGDeclare.h"
#include "SDGDoWhile.h"
#include "SDGFor.h"
#include "SDGIf.h"
#include "SDGSwitch.h"
#include "SDGWhile.h"
#include "SDGIncDec.h"
#include "SDGBreak.h"
#include "SDGContinue.h"
#include "SDGReturn.h"
#include "SDGCall.h"
#include "SDG.h"
#include "constdata.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// ----------------Construction/Destruction------------------------ //
//////////////////////////////////////////////////////////////////////
int SDGNODEID=0; 
CSDG::CSDG()
{
	m_pSDG=NULL;
	m_pVArry=NULL;
	
}

CSDG::~CSDG()
{

}

//@@@@@@@@@@@@@@####################$$$$$$$$ Interface $$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

CSDGBase *CSDG::operator()(CArray<VIPCS,VIPCS> &VArry,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	if(TArry.GetSize()<=0||SArry.GetSize()<=0||FArry.GetSize()<=0)
		return NULL;

	SDGNODEID=0;
	m_pSDG=NULL;
	m_pVArry=&VArry;
	STACK.RemoveAll();

	m_pHead=new CSDGBase;
	m_pHead->bodybeg=0;
	m_pHead->bodyend=TArry.GetSize();
	m_pHead->SetNodeID(-1); 

	m_pHead->m_sType="SDGHEAD";
	STACK.AddTail(m_pHead);
	m_pSDG=STACK.GetTail(); 
  

	ConstructSDG(TArry,SArry,FArry);//创建控制依赖子图，wtt在此增加了数组初始化标准化操作

//	SetBreakAndContinue(TArry,SArry,FArry);//wtt//5.16//没什么用

	SetSwitchCondition(m_pHead,SArry);///wtt///5.16///若switch的条件是表达式exp,即switch(exp),则var=exp提取到switch外，switch( var)

	AdjustDeclarePos(m_pHead);/////wtt////3.27/////调整declare语句的位置，把declare节点都放到其它语句的前面，因为在数组初始化标准化时可能将赋值语句写到其它变量声明语句前

//	RenameFunction(m_pHead,TArry,SArry,FArry);//////wtt///以后要进行函数内联匹配与函数名无关，故可不必重命名
	return m_pHead;

}

//@@@@@@@@@@@@@@####################$$$$$ Construct SDG $$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::ConstructSDG(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	int tlen=0,n=-1;
   	tlen=TArry.GetSize();
	int ix=0;
     
		
	//while(TArry[ix].key!=CN_DFUNCTION)
	//{
	//	ix++;
	//}

	while(ix<tlen)
	{
		switch(TArry[ix].key)
		{
		case CN_BREAK:
			BreakStatement(ix,TArry,SArry,FArry);
			break;
		case CN_CHAR:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_CONTINUE:
			ContinueStatement(ix,TArry,SArry,FArry); 
			break;
		case CN_DADD:
		case CN_DSUB:
			IncAndDec(ix,TArry,SArry,FArry);
			break;
		case CN_DO:
			DoWhileStatement(ix,TArry,SArry,FArry);
			break;
		case CN_DOUBLE:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_FLOAT:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_FOR:
			ForStatement(ix,TArry,SArry,FArry);
			break;
		case CN_GOTO:
			GotoStatement(ix,TArry,SArry,FArry);
			break;
		case CN_IF:
			IfStatement(ix,TArry,SArry,FArry);
			break;
		case CN_INT:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_LONG:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_RETURN:
			ReturnStatement(ix,TArry,SArry,FArry);
			break;
		case CN_SHORT:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_SWITCH:
			SwitchStatement(ix,TArry,SArry,FArry);
			break;
		case CN_UNSIGNED:
			DeclareStatement(ix,TArry,SArry,FArry);
			break;
		case CN_WHILE:
			
            //#ifdef DEBUG
			n=STACK.GetCount()-1;
			while(n>=0)
			{
				if(STACK.GetAt(STACK.FindIndex(n))->m_sType=="SDGDOWHILE" )
				{
					CSDGDoWhile *pdo=(CSDGDoWhile *)STACK.GetAt(STACK.FindIndex(n));
					if(ix==pdo->WPOS)
					{
						ix=pdo->expend;
						n=0;
					}				
				}
				n--;
			}
			//#endif

			WhileStatement(ix,TArry,SArry,FArry);
            break; 
				
		case CN_EQUAL:
		case CN_ADDEQUAL:
		case CN_SUBEQUAL:
		case CN_MULEQUAL:
		case CN_DIVEQUAL:
		case CN_YUEQUAL:
			AssignmentStatement(ix,TArry,SArry,FArry);
			break;
		case CN_DFUNCTION:
			if(TArry[ix].addr>=0&&TArry[ix].addr<FArry.GetSize()&&ix==FArry[TArry[ix].addr].pbeg-1)
			{
				FindFunction(ix,TArry,SArry,FArry);			
			}
			else 
			{
				CallNode(ix,TArry,SArry,FArry);
			}
			
			break;
		case CN_BFUNCTION:
			CallNode(ix,TArry,SArry,FArry);
			break;
		}

		ix++;
	}
}


bool CSDG::VariableType(int const key)
{
	switch(key)
	{
	case CN_INT:
	case CN_DOUBLE:
	case CN_FLOAT:
	case CN_LONG:
	case CN_CHAR:
	case CN_UNSIGNED:
		return true;
		break;
	default:
		return false;
	}
}


bool CSDG::NotVDeclare(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{
	int layer=-1;
	if(TArry[idx].addr>=0&&TArry[idx].addr<SArry.GetSize())
   	layer=SArry[TArry[idx].addr].layer;

	int ix=idx-1;    
	
	while(ix>=0)
	{
		if(TArry[ix].key==CN_VARIABLE && TArry[ix].name==TArry[idx].name)
		{
			int lyr=-1;
			if(TArry[ix].addr>=0&&TArry[ix].addr<SArry.GetSize())
			    lyr=SArry[TArry[ix].addr].layer;
			if(lyr==layer)
			{  	
				return true;
			}

		}
		ix--;
	}

	return false;
}

int  CSDG::IsImportant(CSDGBase *pNode)
{
	int nrvalue=0;

	if(m_pVArry && m_pVArry->GetSize()>0)
	{
		for(int ix=0;ix<m_pVArry->GetSize();ix++)
		{
			if(pNode->bodybeg>=m_pVArry->GetAt(ix).beg && pNode->bodyend<=m_pVArry->GetAt(ix).end)
			{
				nrvalue=1;
			}
		}
	}

	return nrvalue;
	
}



void CSDG::GetDirectFather(CSDGBase * &pDF,const int idx)//wtt
//void CSDG::GetDirectFather(CSDGBase *pDF,const int idx)//wtt
{
	// 得到当前位置 ix 所属的最近节点 pDF

	
    if(m_pSDG && m_pSDG->FactorOf(idx))
	{
		
		if(m_pSDG->m_sType=="SDGSWITCH")
		{
			FindSelector(m_pSDG,idx);
		}
		else if(m_pSDG->m_sType=="SDGIF")
		{
			FindIfElse(m_pSDG,idx);
		}
	}
	else
	{
		while((STACK.GetCount()>1) && (!(m_pSDG->FactorOf(idx))) )
		{	
			//CString s;
			//s.Format("%d ",idx); 
			//AfxMessageBox(s+m_pSDG->m_sType);

			STACK.RemoveTail();
			m_pSDG=STACK.GetTail();
		}
		//ageBox("222222222     SDGSWITCH");
		if(m_pSDG->m_sType=="SDGSWITCH")
		{
			FindSelector(m_pSDG,idx);
		}
		else if(m_pSDG->m_sType=="SDGIF")
		{
			FindIfElse(m_pSDG,idx);
		}

	}
	pDF=m_pSDG;
}

void CSDG::FindSelector(CSDGBase *ptr,const int idx)
{
	for(int i=0;i<ptr->GetRelateNum();i++ )
	{
		if(ptr->GetNode(i)&&idx>ptr->GetNode(i)->bodybeg && idx<=ptr->GetNode(i)->bodyend)
		{
			STACK.AddTail(ptr->GetNode(i));
			m_pSDG=STACK.GetTail();
			//CString s;
			//s.Format("selector=%d\ntop=%s",i,m_pSDG->m_sType); 
			//AfxMessageBox(s);
		}
	}
}

void CSDG::FindIfElse(CSDGBase *ptr,const int idx)
{
	if(ptr==NULL)
		return;
	if(ptr->GetRelateNum()>0 && ptr->GetNode(0)&&idx>ptr->GetNode(0)->bodybeg && idx <ptr->GetNode(0)->bodyend)
	{
		STACK.AddTail(ptr->GetNode(0));
	}
	else if(ptr->GetRelateNum()>1 && ptr->GetNode(1)&&idx>ptr->GetNode(1)->bodybeg && idx <ptr->GetNode(1)->bodyend)
	{
		STACK.AddTail(ptr->GetNode(1));
	}
	
	m_pSDG=STACK.GetTail();
	
}


//@@@@@@@@@@@@@@####################$$$$$$$$ function $$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::FindFunction(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:加入一个Entry节点于SDG中，并设置一些节点信息.
    
	int ix=idx+1;
	int nkh=1;

	if(TArry[ix].key!=CN_LCIRCLE)
	{
		return;
	}

    ix++;
	while(nkh!=0)
	{
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nkh++;
		}

		if(TArry[ix].key==CN_RCIRCLE)
		{
			nkh--;
		}
		ix++;
	}

	if(TArry[ix].key==CN_LINE || TArry[ix].key!=CN_LFLOWER )
	{
		return;
	}

   	CSDGEntry *pSDGEntry= new CSDGEntry;
	int addr=TArry[idx].addr;
	pSDGEntry->SetNodeID(CN_ENTRY);
   if(addr>=0&&addr<FArry.GetSize())
   {
	pSDGEntry->bodybeg=FArry[addr].pbeg;
	pSDGEntry->bodyend=FArry[addr].bend;
	pSDGEntry->m_sFName=FArry[addr].name;
   }
	TNODE T;
	T.key=CN_DFUNCTION;
	T.addr=addr;
	T.deref=0;//wtt 2008 3.19
	T.line=TArry[idx].line;
	T.value=0;
	if(addr>=0&&addr<FArry.GetSize())
	T.name=FArry[addr].name;	
	pSDGEntry->TCnt.Add(T);  
	
	

	//CString s;
	//s.Format("beg=%d\nend=%d ",pSDGEntry->bodybeg,pSDGEntry->bodyend); 
	
	pSDGEntry->FPOS=idx;
	CSDGBase *pDF=NULL;
	
	GetDirectFather(pDF,idx);
	if(m_pSDG)
	{
		//AfxMessageBox(s+m_pSDG->m_sType);
		for(int i=FArry[addr].pbeg;i<=FArry[addr].pend;i++)
		{
			if(TArry[i].key==CN_DOU)
			{
				TArry[i].key=CN_LINE; 
			}
		}

        pSDGEntry->important=IsImportant(pSDGEntry);
		m_pSDG->Add(CN_ENTRY,pSDGEntry);
		STACK.AddTail(pSDGEntry);
		m_pSDG=STACK.GetTail(); 
		pSDGEntry->SetFather(m_pHead);
		////////////////////wtt///1.1/////为参数创建声明节点//不用 因为在判别declarestatement时已建立声明节点/////////
/*		int j;
		i=0;
		while(FArry[addr].plist[i]!=-1)
		{
			CSDGDeclare *pSDGDeclare=new CSDGDeclare;
			pSDGDeclare->SetNodeID(CN_DECLARE);
			j=FArry[addr].pbeg;
			while(j<=FArry[addr].pend)
			{
				if(TArry[j].name==SArry[FArry[addr].plist[i]].name)
				{
				
					break;
				}
				else 
					j++;
			}
			if(j>FArry[addr].pend)
			{
				//error
				return;
			}
			pSDGDeclare->VPOS=j;
			pSDGDeclare->bodybeg=j;
			pSDGDeclare->bodyend=j; 
			pSDGDeclare->TCnt.Add(TArry[j]);
		
			if(m_pSDG)
			{
				pSDGDeclare->important=IsImportant(pSDGDeclare);
				m_pSDG->Add(CN_CONTROL,pSDGDeclare); 
				pSDGDeclare->SetFather(m_pSDG);
			}
			i++;
		}*/
		////////////////////////wtt//////////////////////////////////
	} 
}

//@@@@@@@@@@@@@@####################$$$$$$$ Assignment $$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::AssignmentStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:加入赋值语句节点于SDG中，idx:赋值语句标志"="、"+="、"*="......的位置。

	CSDGAssignment *pAssign=new CSDGAssignment;
	pAssign->SetNodeID(CN_ASSIGNMENT);
	pAssign->OP=idx;
	pAssign->expbeg=idx+1;
	pAssign->OPKIND=TArry[idx].key;  
	FindAssignmentBody(pAssign,TArry,SArry);
	pAssign->bodybeg=pAssign->LV;
	pAssign->bodyend=pAssign->expend;
	
//	AfxMessageBox("in assignement statement");
	
    TArry.FreeExtra();
	if(pAssign->LV>=0)
	{
		int ix;

		// 将赋值语句左值存入TCnt中
		ix=pAssign->LV;
		while(ix<pAssign->OP)
		{
			pAssign->TCnt.Add(TArry[ix]);
			/////////////////////wtt///1.1// 创建definition 集合/////////
		/*	if(TArry[ix].key==CN_VARIABLE)
			{
				pAssign->m_aDefine.Add(TArry[ix]);
				if(pAssign->OPKIND==CN_ADDEQUAL||pAssign->OPKIND==CN_SUBEQUAL||pAssign->OPKIND==CN_MULEQUAL||pAssign->OPKIND==CN_DIVEQUAL)
				{
					pAssign->m_aRefer.Add(TArry[ix]);

				}
			
			}*/
////////////////////////////////wtt/////////////////////////

			ix++;
		}

		// 赋值语句右值存入TExp中
		ix=idx+1;
		while(ix<=pAssign->bodyend)
		{
			pAssign->TEXP.Add(TArry[ix]);
			///////////////////wtt//1.1///创建reference集合///////
			/*if(TArry[ix].key==CN_VARIABLE)
			{				
				pAssign->m_aRefer.Add(TArry[ix]); 
							
			}*/
			/////////////////wtt///////////////////
			ix++;
		}		
	}
	

    CSDGBase *pDF=NULL;
	GetDirectFather(pDF,idx);	
	if(m_pSDG)
	{
		///////////////////////wtt3.1////////////数组初始化的标准化//////////
		if(pAssign->TEXP.GetSize()>0&&pAssign->TCnt[0].key==CN_VARIABLE&&pAssign->TCnt[0].addr>=0&&pAssign->TCnt[0].addr<SArry.GetSize()&&SArry[pAssign->TCnt[0].addr].kind==CN_ARRAY&&pAssign->TEXP[0].key==CN_LFLOWER  )
		{
			
			//AfxMessageBox("array initialization!");
			StandardofArrayInit(pAssign,m_pSDG,SArry);
			idx=pAssign->bodyend;
			
		}





		///////////////////////////wtt3.1///////////////////////
		else
		{
		
		
		
		
		
		UnifyAssignment(pAssign);
		
		if(STACK.GetTail()->m_sType=="SDGFOR")
		{
			CSDGFor *pfor=(CSDGFor *)STACK.GetTail();
			if(idx>=pfor->DOUPOS2 && idx<=pfor->expend)
			{
				pAssign->SetNodeVL(CN_FOR);
			}
		}
        
        pAssign->important=IsImportant(pAssign);
		m_pSDG->Add(CN_CONTROL,pAssign);
	//	AfxMessageBox(m_pSDG->m_sType);/////////////////
		pAssign->SetFather(m_pSDG);
		idx=pAssign->bodyend;
		}
	}
	
	/*
	extern CString PrintExp(CArray<TNODE,TNODE> &T);
	CString s;
	s.Format("赋值语句:%s",PrintExp(pAssign->TEXP));
	AfxMessageBox(s);	
	*/

}
///////////////////wtt///////////////3.1数组初始化的标准化////////////
void CSDG::StandardofArrayInit(CSDGAssignment*pAssign,CSDGBase*pfather,CArray<SNODE,SNODE> &SArry)
{
	int dim;
	if(pAssign->TCnt[0].addr>=0&&pAssign->TCnt[0].addr<SArry.GetSize()&&SArry[pAssign->TCnt[0].addr].type==CN_INT||SArry[pAssign->TCnt[0].addr].type==CN_LONG||SArry[pAssign->TCnt[0].addr].type==CN_FLOAT||SArry[pAssign->TCnt[0].addr].type==CN_DOUBLE )
	{//数值数组
		dim=SArry[pAssign->TCnt[0].addr].arrdim;
		//CString stemp;
		//	stemp.Format("%d",dim);
	//	AfxMessageBox(stemp);
		if(dim==1)
		{
		//	AfxMessageBox("one dimention  array(not char type)!");
			int n=1,m=0;
			while(n<pAssign->TEXP.GetSize()&&m<SArry[pAssign->TCnt[0].addr].arrsize[0])
			{
				if(pAssign->TEXP[n].key==CN_CINT||pAssign->TEXP[n].key==CN_CLONG||pAssign->TEXP[n].key==CN_CFLOAT||pAssign->TEXP[n].key==CN_CDOUBLE)
				{
					CSDGAssignment*pas=new CSDGAssignment;
					pas->SetNodeID(CN_ASSIGNMENT);
					pas->OPKIND=CN_EQUAL;
					pas->TCnt.Add(pAssign->TCnt[0]);
					TNODE TL,TI,TR;
					TL.key=CN_LREC;
					TL.addr=-1;
					TL.deref=0;//wtt 2008 3.19
					TL.paddr=-1;
					pas->TCnt.Add(TL);

					TI.key=CN_CINT;
					TI.addr=m;
					TI.paddr=-1;
					TI.deref=0;//wtt 2008 3.19
					pas->TCnt.Add(TI);

					TR.key=CN_RREC;
					TR.addr=-1;
					TR.deref=0;
					TR.paddr=-1;
					pas->TCnt.Add(TR);
				
					pas->TEXP.Add(pAssign->TEXP[n]);

					pfather->Add(CN_CONTROL,pas);
					pas->SetFather(pfather);

					m++;


				}
				n++;
			}
		}
		else if(dim==2)
		{
			//AfxMessageBox("two dimention  array(not char type)!");
			int n=1,m=0,k=0;;
			while(n<pAssign->TEXP.GetSize()&&m<SArry[pAssign->TCnt[0].addr].arrsize[0])
			{
				
				if(pAssign->TEXP[n].key==CN_CINT||pAssign->TEXP[n].key==CN_CLONG||pAssign->TEXP[n].key==CN_CFLOAT||pAssign->TEXP[n].key==CN_CDOUBLE)
				{
					CSDGAssignment*pas=new CSDGAssignment;
					pas->SetNodeID(CN_ASSIGNMENT);
					pas->OPKIND=CN_EQUAL;
					pas->TCnt.Add(pAssign->TCnt[0]);

					TNODE TL,TI,TR;
					TL.key=CN_LREC;
					TL.addr=-1;
					TL.deref=0;//wtt 2008 3.19
					TL.paddr=-1;
					pas->TCnt.Add(TL);

					TI.key=CN_CINT;
					TI.addr=m;
					TI.deref=0;//wtt 2008 3.19
					TI.paddr=-1;
					pas->TCnt.Add(TI);

					TR.key=CN_RREC;
					TR.addr=-1;
					TR.deref=0;//wtt 2008 3.19
					TR.paddr=-1;
					pas->TCnt.Add(TR);


					TNODE TL1,TI1,TR1;
					TL1.key=CN_LREC;
					TL1.addr=-1;
					TL1.deref=0;
					TL1.paddr=-1;
					pas->TCnt.Add(TL1);

					TI1.key=CN_CINT;
					TI1.addr=k;
					TI1.deref=0;//wtt 2008 3.19
					TI1.paddr=-1;
					pas->TCnt.Add(TI1);

					TR1.key=CN_RREC;
					TR1.addr=-1;
					TR1.paddr=-1;
					TR1.deref=0;//wtt 2008 3.19
					pas->TCnt.Add(TR1);
				
					pas->TEXP.Add(pAssign->TEXP[n]);

					pfather->Add(CN_CONTROL,pas);
					pas->SetFather(pfather);

					k++;
					if(k==SArry[pAssign->TCnt[0].addr].arrsize[1])
					{
						m++;
						k=0;
					}
				


				}
				
				
			
				n++;
			}
		}
		else//其他维数暂未作处理
		{
		}
	}
	else if	(pAssign->TCnt[0].addr>=0&&pAssign->TCnt[0].addr<SArry.GetSize()&&SArry[pAssign->TCnt[0].addr].type==CN_CHAR )
	{//字符数组
		dim=SArry[pAssign->TCnt[0].addr].arrdim;
		if(dim==1)
		{
			int n=1,m=0,flag=0;
			if(pAssign->TEXP[1].key==CN_CSTRING)
				flag=2;
			while(n<pAssign->TEXP.GetSize())
			{
				if(pAssign->TEXP[n].key==CN_CCHAR&&pAssign->TEXP[n].addr==0 )
				{
					flag=1;
					break;
				}
				
				n++;
			}
			if(flag==1)
			{
				//AfxMessageBox("string");				
				CSDGAssignment*pas=new CSDGAssignment;
				pas->SetNodeID(CN_ASSIGNMENT);
				pas->OPKIND=CN_EQUAL;
				pas->TCnt.Add(pAssign->TCnt[0]);
				TNODE TL,TI,TR;
				TL.key=CN_LREC;
				TL.addr=-1;
				TL.deref=0;//wtt 2008 3.19
				TL.paddr=-1;
				pas->TCnt.Add(TL);

				TI.key=CN_CINT;
				TI.addr=dim;
				TI.deref=0;//wtt 2008 3.19
				TI.paddr=-1;
				pas->TCnt.Add(TI);

				TR.key=CN_RREC;
				TR.addr=-1;
				TR.deref=0;//wtt 2008 3.19

				TR.paddr=-1;
				pas->TCnt.Add(TR);
				
				TNODE T;
				T.key=CN_CSTRING;
				T.addr=-1;
				T.paddr=-1;
				T.deref=0;//wtt 2008 3.19
				CString str="";
				char ch;
				n=1;
				while(n<pAssign->TEXP.GetSize())
				{
					if(pAssign->TEXP[n].key==CN_CCHAR&&pAssign->TEXP[n].addr!=0)
					{
						ch=char(pAssign->TEXP[n].addr);
						str+=ch;

					}
					if(pAssign->TEXP[n].key==CN_CCHAR&&pAssign->TEXP[n].addr==0)
						break;
					n++;
				}
				str+='\0';
				T.name=str;
				pas->TEXP.Add(T);

				pfather->Add(CN_CONTROL,pas);
				pas->SetFather(pfather);

		}
		else if(flag==0)
		{
			n=1,m=0;
			while(n<pAssign->TEXP.GetSize()&&m<SArry[pAssign->TCnt[0].addr].arrsize[0])
			{
				if(pAssign->TEXP[n].key==CN_CCHAR)
				{
					CSDGAssignment*pas=new CSDGAssignment;
					pas->SetNodeID(CN_ASSIGNMENT);
					pas->OPKIND=CN_EQUAL;
					pas->TCnt.Add(pAssign->TCnt[0]);
					TNODE TL,TI,TR;
					TL.key=CN_LREC;
					TL.addr=-1;
					TL.deref=0;//wtt 2008 3.19

					TL.paddr=-1;
					pas->TCnt.Add(TL);

					TI.key=CN_CINT;
					TI.addr=m;
					TI.deref=0;//wtt 2008 3.19
					TI.paddr=-1;
					pas->TCnt.Add(TI);

					TR.key=CN_RREC;
					TR.addr=-1;
					TR.deref=0;//wtt 2008 3.19

					TR.paddr=-1;
					pas->TCnt.Add(TR);
				
					pas->TEXP.Add(pAssign->TEXP[n]);

					pfather->Add(CN_CONTROL,pas);
					pas->SetFather(pfather);

					m++;


				}
				n++;
			}

		}
		else 
		{
				CSDGAssignment*pas=new CSDGAssignment;
				pas->SetNodeID(CN_ASSIGNMENT);
				pas->OPKIND=CN_EQUAL;
				pas->TCnt.Add(pAssign->TCnt[0]);
				TNODE TL,TI,TR;
				TL.key=CN_LREC;
				TL.addr=-1;
				TL.deref=0;//wtt 2008 3.19

				TL.paddr=-1;
				pas->TCnt.Add(TL);

				TI.key=CN_CINT;
				TI.addr=dim;
				TI.deref=0;//wtt 2008 3.19

				TI.paddr=-1;
				pas->TCnt.Add(TI);

				TR.key=CN_RREC;
				TR.addr=-1;
				TR.deref=0;//wtt 2008 3.19
				TR.paddr=-1;
				pas->TCnt.Add(TR);
				
				TNODE T;
				T.key=CN_CSTRING;
				T.addr=-1;
				T.deref=0;//wtt 2008 3.19

				T.paddr=-1;
				T.name=pAssign->TEXP[1].name;
				pas->TEXP.Add(T);

				pfather->Add(CN_CONTROL,pas);
				pas->SetFather(pfather);



		}

		}
		if(dim==2)
		{
			int n,m;
			if(pAssign->TEXP[1].key==CN_CSTRING)
			{
				n=1,m=0;
				while(n<pAssign->TEXP.GetSize()&&m<SArry[pAssign->TCnt[0].addr].arrsize[0])
				{
					if(pAssign->TEXP[n].key==CN_CSTRING)
					{
						CSDGAssignment*pas=new CSDGAssignment;
						pas->SetNodeID(CN_ASSIGNMENT);
						pas->OPKIND=CN_EQUAL;
						pas->TCnt.Add(pAssign->TCnt[0]);
						TNODE TL,TI,TR;
						TL.key=CN_LREC;
						TL.addr=-1;
						TL.deref=0;//wtt 2008 3.19

						TL.paddr=-1;
						pas->TCnt.Add(TL);

						TI.key=CN_CINT;
						TI.addr=m;
						TI.deref=0;//wtt 2008 3.19

						TI.paddr=-1;
						pas->TCnt.Add(TI);

						TR.key=CN_RREC;
						TR.addr=-1;
						TR.deref=0;//wtt 2008 3.19

						TR.paddr=-1;
						pas->TCnt.Add(TR);

						TNODE TL1,TI1,TR1;
						TL1.key=CN_LREC;
						TL1.addr=-1;
						TL1.deref=0;//wtt 2008 3.19
						TL1.paddr=-1;
						pas->TCnt.Add(TL1);

						TI1.key=CN_CINT;
						TI1.addr=SArry[pAssign->TCnt[0].addr].arrsize[1];
						TI1.paddr=-1;
						TI1.deref=0;//wtt 2008 3.19

						pas->TCnt.Add(TI1);

						TR1.key=CN_RREC;
						TR1.addr=-1;
						TR1.deref=0;//wtt 2008 3.19

						TR1.paddr=-1;
						pas->TCnt.Add(TR1);
				
						pas->TEXP.Add(pAssign->TEXP[n]);
	
						pfather->Add(CN_CONTROL,pas);
						pas->SetFather(pfather);

						m++;

	
					}
					n++;
				}

			}
			else
			{
				int n=1,m=0,k=0;;
				while(n<pAssign->TEXP.GetSize()&&m<SArry[pAssign->TCnt[0].addr].arrsize[0])
				{
				
					if(pAssign->TEXP[n].key==CN_CCHAR)
					{
						CSDGAssignment*pas=new CSDGAssignment;
						pas->SetNodeID(CN_ASSIGNMENT);
						pas->OPKIND=CN_EQUAL;
						pas->TCnt.Add(pAssign->TCnt[0]);

						TNODE TL,TI,TR;
						TL.key=CN_LREC;
						TL.addr=-1;
						TL.deref=0;//wtt 2008 3.19

						TL.paddr=-1;
						pas->TCnt.Add(TL);

						TI.key=CN_CINT;
						TI.addr=m;
						TI.deref=0;//wtt 2008 3.19

						TI.paddr=-1;
						pas->TCnt.Add(TI);

						TR.key=CN_RREC;
						TR.addr=-1;
						TR.deref=0;//wtt 2008 3.19

						TR.paddr=-1;
						pas->TCnt.Add(TR);


						TNODE TL1,TI1,TR1;
						TL1.key=CN_LREC;
						TL1.addr=-1;
						TL1.deref=0;//wtt 2008 3.19

						TL1.paddr=-1;
						pas->TCnt.Add(TL1);

						TI1.key=CN_CINT;
						TI1.addr=k;
						TI1.deref=0;//wtt 2008 3.19

						TI1.paddr=-1;
						pas->TCnt.Add(TI1);

						TR1.key=CN_RREC;
						TR1.addr=-1;
						TR1.deref=0;//wtt 2008 3.19

						TR1.paddr=-1;
						pas->TCnt.Add(TR1);
				
						pas->TEXP.Add(pAssign->TEXP[n]);

						pfather->Add(CN_CONTROL,pas);
						pas->SetFather(pfather);

						k++;
						if(k==SArry[pAssign->TCnt[0].addr].arrsize[1])
						{
							m++;
							k=0;
						}
					}			
					n++;
			}


			}
			
		}
		else
			{//其他维数未作处理 

			}

	}
}
//////////////////////wtt/////////////3.1///3.2//////////////////////////////////////
//////////////wtt///////////////////3.27//////////////////////////
void CSDG::AdjustDeclarePos(CSDGBase*pHead)
{//调整declare语句的位置，把declare节点都放到其它语句的前面，因为在数组初始化标准化时可能将赋值语句写到其它变量声明语句前
	if(pHead==NULL||pHead->m_sType!="SDGHEAD")
		return;
//	AfxMessageBox("pHead->m_sType==SDGHEAD");//
	int pos;
	//全局变量声明语句处理
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		if(pHead->GetNode(i)&&pHead->GetNode(i)->GetNodeID()!=CN_DECLARE)
		{
			
			pos=i;
			break;
		}
	}
	

	for(int i=pos+1;i<pHead->GetRelateNum();i++)
	{
		CSDGBase*p=pHead->GetNode(i);
		if(p&&p->GetNodeID()==CN_DECLARE)
		{
		//	AfxMessageBox("global declare changed");//////////////
			pHead->InsertNode(pos,p);
			pHead->Del(i+1); 

		}
	}
//局部变量声明语句处理
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		CSDGBase*p=pHead->GetNode(i);

		if(p&&p->m_sType=="SDGENTRY")
		{
		//	AfxMessageBox("ENTRY");
			for(int j=0;j<p->GetRelateNum();j++)
			{
				if(p->GetNode(j)&&p->GetNode(j)->GetNodeID()!=CN_DECLARE)
				{
				//	AfxMessageBox("local declare changed");//////////////

					pos=j;
					break;
				}
			}
	
			for( int j=pos+1;j<p->GetRelateNum();j++)
			{
				CSDGBase*p1=p->GetNode(j);
				if(p1&&p1->GetNodeID()==CN_DECLARE)
				{
					p->InsertNode(pos,p1);
					p->Del(j+1); 

				}
			}
		}
	}

}


/////////////////////////////////////////////////
void CSDG::StandardOfArray(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry)
{

}

void CSDG::StandardOfPointer(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry)
{

}

void CSDG::UnifyAssignment(CSDGAssignment *pAssign)
{
	if(pAssign==NULL)
		return;
	TNODE TR,TL,T;
	bool istrans=true;

	TR.key=CN_RCIRCLE;
	TR.addr=-1;
	TR.deref=0;//wtt 2008 3.19

	TR.paddr=-1; 
	TR.line=-1;	
	TR.value=0;

	TL.key=CN_LCIRCLE;
	TL.addr=-1;
	TL.deref=0;//wtt 2008 3.19

	TL.paddr=-1; 
	TL.line=-1;	
	TL.value=0;

	TR.line=pAssign->TEXP.GetSize()?pAssign->TEXP[0].line:0;
	TL.line=TR.line;
	T.line=TR.line;
	
	
	switch(pAssign->OPKIND)
	{
	case CN_ADDEQUAL:
		T.key=CN_ADD;
		pAssign->TEXP.InsertAt(0,T);
		pAssign->OPKIND=CN_EQUAL;
		break;
	case CN_SUBEQUAL:
		T.key=CN_SUB;
		pAssign->TEXP.InsertAt(0,T);
		pAssign->OPKIND=CN_EQUAL;
		break;
	case CN_MULEQUAL:
		pAssign->TEXP.Add(TR);
		pAssign->TEXP.InsertAt(0,TL);
		T.key=CN_MULTI;
		pAssign->TEXP.InsertAt(0,T);
		pAssign->OPKIND=CN_EQUAL;
		break;
	case CN_DIVEQUAL:
		pAssign->TEXP.Add(TR);
		pAssign->TEXP.InsertAt(0,TL);
		T.key=CN_DIV;
		pAssign->TEXP.InsertAt(0,T);
		pAssign->OPKIND=CN_EQUAL;
		break;
	default:
		istrans=false;
	}


	if(istrans)
	{
		for(int i=pAssign->TCnt.GetSize()-1;i>=0;i--)
		{
			pAssign->TEXP.InsertAt(0,pAssign->TCnt[i]);
		}
	}
		

}

void CSDG::FindAssignmentBody(CSDGAssignment *pAssign,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry)
{
	// F:填充*pAssign 的一些信息。

	if(pAssign==NULL)
		return;
   	int ix=pAssign->OP;
	int line=TArry[ix].line;

    
	// 查找赋值语句的左值变量

	pAssign->LV=-1;
	ix--;
    if(ix>=0)
	{
		int rect=0;
		while(ix>0 &&ix<TArry.GetSize()&& TArry[ix].line==line && TArry[ix].key!=CN_LINE &&
			  TArry[ix].key!=CN_LFLOWER && TArry[ix].key!=CN_RFLOWER &&
			  (!VariableType(TArry[ix].key)) && TArry[ix].key!=CN_DOU&&
			  rect>=0)
		{
			if(TArry[ix].key==CN_LCIRCLE)
			{
				rect--;
			}
			
			if(TArry[ix].key==CN_RCIRCLE)
			{
				rect++;
			}

			ix--;
		}

		if(ix<=0)
		{
			ix=pAssign->OP-1;
			while(ix>=0&&ix<TArry.GetSize())
			{
				if(TArry[ix].key==CN_VARIABLE)
				{
					pAssign->LV=ix;
					break;
				}
				ix--;//wtt
			}
		}
        
		if( TArry[ix].key==CN_LINE || TArry[ix].key==CN_LFLOWER ||
			VariableType(TArry[ix].key) || TArry[ix].key==CN_RFLOWER || 
			TArry[ix].key==CN_DOU || rect<0 || TArry[ix].line<line )
		{
			ix++;
			if(rect<0)
			{
				ix++;
			}

			pAssign->LV=ix;
		}
        ///////wtt//2008.3.28/////多级指针
		/*if(pAssign->LV-1>=0&&pAssign->LV+1<TArry.GetSize()&& (VariableType(TArry[pAssign->LV-1].key) || TArry[pAssign->LV-1].key==CN_DOU ) &&
		   TArry[pAssign->LV].key==CN_MULTI && TArry[pAssign->LV+1].key==CN_VARIABLE &&TArry[pAssign->LV+1].addr>=0&&TArry[pAssign->LV+1].addr<SArry.GetSize()&&
		   SArry[TArry[pAssign->LV+1].addr].kind==CN_POINTER && SArry[TArry[pAssign->LV+1].addr].value<=1)
		{
			pAssign->LV++;			
		}*/
		int pos=pAssign->OP-1;
		if(pAssign->LV-1>=0&&pAssign->LV+1<TArry.GetSize()&& (VariableType(TArry[pAssign->LV-1].key) || TArry[pAssign->LV-1].key==CN_DOU ) 
			&&pos>0&&TArry[pos].addr>=0&&TArry[pos].addr<SArry.GetSize()
			&&SArry[TArry[pos].addr].kind==CN_POINTER)
		{
			//AfxMessageBox("ok");
			while(pAssign->LV<pos&&TArry[pAssign->LV].key==CN_MULTI)
			{
				pAssign->LV++;



			}
		}
		//////////////////wtt//2008.3.28/////多级指针//////////////////////////////
		


        /*
		if(TArry[ix].key==CN_VARIABLE)
		{
			pAssign->LV=ix;
		}
		else if(TArry[ix].key==CN_RREC)
		{
			while(ix>=0 && TArry[ix].line==line)
			{
				if(TArry[ix].key==CN_VARIABLE && TArry[ix+1].key==CN_LREC)
				{
					pAssign->LV=ix;
					break;
				}
				ix--;
			}

            ix++;
			if(pAssign->LV<0)
			{
				while(ix<pAssign->OP)
				{
					if(TArry[ix].key=CN_VARIABLE)
					{
						pAssign->LV=ix;
						break;
					}
				}	
			}
		}

		if(pAssign->LV<0)
		{
			while(ix<pAssign->OP)
			{
				if(TArry[ix].key=CN_VARIABLE)
				{
					pAssign->LV=ix;
					break;
				}
			}	
		}

		if(TArry[pAssign->LV].key==CN_VARIABLE && SArry[TArry[pAssign->LV].addr].kind==CN_POINTER && pAssign->LV>0&&TArry[pAssign->LV-1].key==CN_MULTI)
		{
			pAssign->LV--;
		}
		*/		
	}
	
	
	// 查找表达式的结束位置
    
	ix=pAssign->OP;
	int nkh=0;
	int nbk=0;
	
	while(  ix>=0&& ix<TArry.GetSize() && TArry[ix].key>=0 
		  && TArry[ix].key!=CN_LINE && TArry[ix].line==line
		  && TArry[ix].key!=CN_DO && TArry[ix].key!=CN_FOR 
		  && TArry[ix].key!=CN_WHILE && TArry[ix].key!=CN_SWITCH
		  && TArry[ix].key!=CN_IF && TArry[ix].key!=CN_GOTO 
		  && (TArry[ix].key!=CN_DOU || nkh!=0) )
	{
		if(TArry[ix].key==CN_LFLOWER)
		{
			nkh++;
		}

		if(TArry[ix].key==CN_RFLOWER)
		{
			nkh--;
		}
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nbk++;
		}

		if(TArry[ix].key==CN_RCIRCLE)
		{
			nbk--;
		}

		if(TArry[ix].key==CN_BFUNCTION ||TArry[ix].key==CN_DFUNCTION)
		{
			int n;
			ix++;
			if(TArry[ix].key==CN_LCIRCLE)
			{
				nbk++;
				ix++;
				n=1;
				while(ix<TArry.GetSize()&&n!=0&&TArry[ix].line==line&&TArry[ix].key!=CN_LINE)
				{
					if(TArry[ix].key==CN_LCIRCLE)
					{
						nbk++;
						n++;
					}

					if(TArry[ix].key==CN_RCIRCLE)
					{
						nbk--;
						n--;
					}
					if( n==0 || ix>=TArry.GetSize() ||TArry[ix].line!=line || TArry[ix].key==CN_LINE)
					{
						break;
					}
					else
					{
						ix++;
					}
				}
			}			
		}
		
		ix++;
	}
	
	ix--;

	if(nbk<0)
	{
		
		while(nbk!=0 && TArry[ix].key==CN_RCIRCLE )
		{
			nbk++;
			ix--;
		}
	}

	pAssign->expend=ix; 


}

//@@@@@@@@@@@@@@####################$$$$$$$ Declare $$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::DeclareStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:变量申明部分的处理，idx:变量类型的位置
	
	int ix=idx; 
    
	// 排除一些可能是类型转换，而非变量申明的情况
	if(idx==0 && TArry[idx+1].key==CN_RCIRCLE )
	{
		return ;
	}
    
	if(idx+1<TArry.GetSize()&&TArry[idx+1].key==CN_DFUNCTION && TArry[idx+1].addr>=0&&TArry[idx+1].addr<FArry.GetSize()&&FArry[TArry[idx+1].addr].pbeg-1==idx+1)
	{
		return;
	}
	//////////////////////wtt///////////////1.1/////返回值为指针的函数声明或定义//////////
    if(idx+2<TArry.GetSize()&&TArry[idx+1].key==CN_MULTI&&TArry[idx+2].key==CN_DFUNCTION )
	{
		return;
	}
	////////////////////////wtt/////////////////////////
     
	if(idx>0 &&(idx>=1&&TArry[idx-1].key==CN_LCIRCLE || idx+1<TArry.GetSize()&&TArry[idx+1].key==CN_LCIRCLE))
	{
		if(!(idx-1>0 && TArry[idx-1].key==CN_LCIRCLE && TArry[idx-2].key==CN_DFUNCTION && TArry[idx-2].addr>=0&&TArry[idx-2].addr<FArry.GetSize()&&
		   FArry[TArry[idx-2].addr].pbeg!=idx-2 ))
		{
			return;
		}
	}
	// 此部分结束
    
	// 为本行中所有变量的申明创建一个变量申明节点，并加入SDG中
	int line=TArry[idx].line;
	ix++;
	while(ix>=0&&ix <TArry.GetSize()-3 && TArry[ix].line==line && 
		  TArry[ix].key!=CN_LINE && VariableType(TArry[ix].key)!=true )
	{
	
		if( TArry[ix].key==CN_VARIABLE                 &&TArry[ix].addr>=0&&TArry[ix].addr<SArry.GetSize()&&
			(SArry[TArry[ix].addr].kind==CN_VARIABLE ||
			 SArry[TArry[ix].addr].kind==CN_ARRAY    || 
			 SArry[TArry[ix].addr].kind==CN_POINTER)   && 
			 (!NotVDeclare(ix,TArry,SArry) ) )
		{
			
			CSDGDeclare *pSDGDeclare=new CSDGDeclare;
			pSDGDeclare->SetNodeID(CN_DECLARE);
			pSDGDeclare->VPOS=ix;
			pSDGDeclare->bodybeg=ix;
			pSDGDeclare->bodyend=ix; 
			pSDGDeclare->TCnt.Add(TArry[ix]);
			CSDGBase *pDF=NULL;
	
			GetDirectFather(pDF,ix);
			if(m_pSDG)
			{
				pSDGDeclare->important=IsImportant(pSDGDeclare);
				m_pSDG->Add(CN_CONTROL,pSDGDeclare); 
				pSDGDeclare->SetFather(m_pSDG);
			}
		}
		ix++;
	}
	
	// 此部分结束
   
}


//@@@@@@@@@@@@@@####################$$$$$$$ Do-While $$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::DoWhileStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将do-while节点加入到SDG中，并设置一些节点信息.

	CSDGDoWhile *pDo=new CSDGDoWhile;
	pDo->DPOS=idx;
	pDo->SetNodeID(CN_DOYJ); 
	
	if(FindDoWhileBody(pDo,TArry))
	{
		// 如果do-while语句格式正确......

		int ix=pDo->expbeg+1;
		while(ix<pDo->expend)
		{
			pDo->TCnt.Add(TArry[ix]);
			ix++;
		}
        
		CSDGBase *pDF=NULL;
		GetDirectFather(pDF,idx);
		if(m_pSDG)
		{
			pDo->important=IsImportant(pDo);
			m_pSDG->Add(CN_CONTROL,pDo);	
			pDo->SetFather(m_pSDG);		

		}

		
		// 修改栈及栈顶指针
		STACK.AddTail(pDo);
		m_pSDG=STACK.GetTail();

	}

}


bool CSDG::FindDoWhileBody(CSDGDoWhile *pDo,CArray<TNODE,TNODE> &TArry)
{
	// F:添加 *pDo 的一些信息

	int ix;
    int nBrace=0;

    
    ix=pDo->DPOS+1;
     if(TArry[ix].key!=CN_LFLOWER)
    {
		// 缺少 (
		return false;
	}

	pDo->bodybeg=ix;
    	
	nBrace++;
	ix++;
	while((ix<TArry.GetSize())&&(nBrace!=0))
	{
		if(TArry[ix].key==CN_LFLOWER)
			nBrace++;
		if(TArry[ix].key==CN_RFLOWER)
			nBrace--;
		ix++;
	}
	if(ix==TArry.GetSize())
	{
		// 内部越界
		return false;
	}

	pDo->bodyend=ix-1;

	if(ix+1<TArry.GetSize()&&(TArry[ix].key!=CN_WHILE)||(TArry[ix+1].key!=CN_LCIRCLE)) 
	{
		// 格式错误
		return false;
	}
    

	int nCurve=0;
	nCurve=1;
	pDo->WPOS=ix; 
	pDo->expbeg=ix+1;
	ix+=2;
	while((ix<TArry.GetSize())&&(nCurve!=0))
	{
		if(TArry[ix].key==CN_LCIRCLE)
			nCurve++;
		if(TArry[ix].key==CN_RCIRCLE)
			nCurve--;
		ix++;
	}

	if(ix==TArry.GetSize())
	{
		// 内部越界
		return false;
	}
	pDo->expend=ix-1;
	if(TArry[ix].key!=CN_LINE)
	{
		// 格式错误
		return true;
	}
  
	return true;

}

//@@@@@@@@@@@@@@####################$$$$$$$$$$ For $$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::ForStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将for语句节点SDGFor 加入SDG中，并设置一些节点信息.

	CSDGFor *pFor=new CSDGFor;
	pFor->FORPOS=idx;
	pFor->SetNodeID(CN_FORYJ);

	CString s;
	//s.Format("%d,%d,%d,%d,",pAssign->bodybeg,pAssign->bodyend,pAssign->OP ,pAssign->LV );
	

	if(FindForBody(pFor,TArry))
	{
		int ix=pFor->expbeg+1;
		
		for( ; ix<pFor->DOUPOS1&&ix<TArry.GetSize() ; ix++)
		{
			pFor->TCnt1.Add(TArry[ix]); 
		}

		for( ix=pFor->DOUPOS1+1 ; ix<pFor->DOUPOS2&&ix<TArry.GetSize()  ; ix++)
		{
			pFor->TCnt2.Add(TArry[ix]); 
		}
		////wtt 2007.5.28//////////
		if(pFor->TCnt2.GetSize()==0)//for(ex1; ;ex3)
		{
		
			TNODE TRUENODE;
			TRUENODE.key=CN_CINT;
			TRUENODE.addr=1;
			TRUENODE.deref=0;//wtt 2008 3.19

			pFor->TCnt2.Add(TRUENODE);
		}
		///////////////////////////

		for( ix=pFor->DOUPOS2+1 ; ix<pFor->expend&&ix<TArry.GetSize() ; ix++)
		{
			pFor->TCnt3.Add(TArry[ix]); 
		}


		// 如果for语句的初始化语句中有赋值语句，则调用函数：AssignmentStatement().

		for(ix=pFor->expbeg;ix<pFor->DOUPOS1&&ix<TArry.GetSize() ;ix++)
		{
			//if(TArry[ix].key==CN_EQUAL)//wtt
			if(TArry[ix].key==CN_EQUAL||TArry[ix].key==CN_ADDEQUAL||TArry[ix].key==CN_SUBEQUAL||TArry[ix].key==CN_MULEQUAL||TArry[ix].key==CN_DIVEQUAL)//wtt
			{
	//			AfxMessageBox("AssignmentStatement in for");//////
				AssignmentStatement(ix,TArry,SArry,FArry);
			}
		}

		CSDGBase *pDF=NULL;
		GetDirectFather( pDF,idx );
		if(m_pSDG)
		{
			pFor->important=IsImportant(pFor);
			m_pSDG->Add(CN_CONTROL,pFor);		
			pFor->SetFather(m_pSDG);
			idx=pFor->DOUPOS2; 
		}
        
		//s.Format("bbeg=%d\nbend=%d\nebeg=%d\n",pFor->bodybeg,pFor->bodyend,pFor->expbeg);
		//AfxMessageBox(s);
		
		// 修改栈及栈顶指针
		STACK.AddTail(pFor);
		
		m_pSDG=STACK.GetTail();
	}

}

bool CSDG::FindForBody(CSDGFor *pFor,CArray<TNODE,TNODE> &TArry)
{

	// F:填写pFor的一些信息	
    
    int ix;
    
    
    // 搜寻for语句的表达式位置
    ix=pFor->FORPOS+1;
    if(ix<0||ix>=TArry.GetSize()||ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LCIRCLE)
    {

	    // 缺少"("
	    return false;
	}
   
	pFor->expbeg=ix;
	int nCurve=1;
	ix++;
	while(nCurve!=0)
	{
		if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_LCIRCLE)
		{
			nCurve++;
		}
	   
		if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_RCIRCLE)
		{
			nCurve--;
		}
		ix++;
	}
	pFor->expend=ix-1;


	// 搜寻for语句的主体位置
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LFLOWER) 
	{
		// 缺少 "{"
		return false;
	}
	int nBrace=1;
	pFor->bodybeg=ix;
	ix++;
	while((ix<TArry.GetSize())&&(nBrace!=0))
	{
		if(TArry[ix].key==CN_LFLOWER)
		{
			nBrace++;
		}
		if(TArry[ix].key==CN_RFLOWER)
		{
			nBrace--;
		}
		ix++;
	}
   
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}

	pFor->bodyend=ix-1;
    
	//CString s;
	//s.Format("bbeg=%d\nbend=%d\nebeg=%d\n",pFor->bodybeg,pFor->bodyend,pFor->expbeg);
	//AfxMessageBox(s);

	// 搜索for语句每个表达式的分界
	pFor->DOUPOS2=pFor->DOUPOS1=pFor->expend; 
	for(ix=pFor->expbeg;ix<pFor->expend;ix++)
	{
		if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_LINE)
		{
			if(pFor->DOUPOS2>pFor->DOUPOS1)
			{
				pFor->DOUPOS2=ix; 
			}
			if(pFor->DOUPOS2==pFor->DOUPOS1)
			{
				pFor->DOUPOS1=ix; 
			}
		}
	}

	// for语句中的一些可能的书写错误
	if((pFor->DOUPOS2<=pFor->DOUPOS1) || (pFor->DOUPOS2>=0&&pFor->DOUPOS2<TArry.GetSize()&&TArry[pFor->DOUPOS2].key!=CN_LINE)|| (pFor->DOUPOS1&&pFor->DOUPOS1<TArry.GetSize()&&TArry[pFor->DOUPOS1].key!=CN_LINE))
	{
		// for语句写法不正确 
		return false;
	}
     
	pFor->bodybeg=pFor->DOUPOS2;  
	return true;

}

//@@@@@@@@@@@@@@####################$$$$$$$$$ Go-To $$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::GotoStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{

}

void CSDG::FindGotoBody()
{

}

//@@@@@@@@@@@@@@####################$$$$$$$$$$$ If $$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::IfStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将if语句的节点SDGIf加入到SDG中，并设置一些节点信息..

	CSDGIf *pIf=new CSDGIf;
	pIf->SetNodeID(CN_IFYJ);
	pIf->IFPOS=idx;
	
	if(FindIfBody(pIf,TArry))
	{
		int ix=pIf->expbeg;
		while(ix>=0&&ix<TArry.GetSize()&&ix<pIf->expend)
		{
			pIf->TCnt.Add(TArry[ix]);
			ix++;
		}
        
		SetIfElse(pIf);

		CSDGBase *pDF=NULL;
		GetDirectFather(pDF,idx);
		if(m_pSDG)
		{
			pIf->important=IsImportant(pIf);
			m_pSDG->Add(CN_CONTROL,pIf);		
			pIf->SetFather(m_pSDG);
		}

		
		// 修改栈及栈顶指针
		STACK.AddTail(pIf);
		m_pSDG=STACK.GetTail();
		idx=pIf->bodybeg; 

	}
}

bool CSDG::FindIfBody(CSDGIf *pIf,CArray<TNODE,TNODE> &TArry)
{
	// F:添加pFor的一些信息

	int ix;
	ix=pIf->IFPOS+1;
	  
	
	// 搜索if语句表达式的一些信息
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LCIRCLE)
	{
		// 缺少"("
		return false;
	}
	
	pIf->expbeg=ix+1;
	int nCurve=1; 
	ix++;
	while(ix<TArry.GetSize() && nCurve!=0)
	{
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nCurve++;
		}
		if(TArry[ix].key==CN_RCIRCLE)
		{
			nCurve--;
		}
		ix++;
	}
	
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}
	pIf->expend=ix-1;
   

	if(TArry[ix].key!=CN_LFLOWER) 
	{
	   // 缺少"{"
	   return false;
	}
    pIf->bodybeg=ix;
	pIf->tbeg=ix; 

	int nBrace=1;
	ix++;
	while((ix<TArry.GetSize())&&(nBrace!=0))
	{
		if(TArry[ix].key==CN_LFLOWER)
		{
			nBrace++;
		}
		if(TArry[ix].key==CN_RFLOWER)
		{
			nBrace--;
		}
		ix++;
	}
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}
   	pIf->tend=ix-1;
	pIf->bodyend=ix-1;
   

	// 检查是否有else部分
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_ELSE)
	{
		// 存在else部分的信息处理
		pIf->ELSEPOS=ix; 
		ix++;
        nBrace=0; 
		if(TArry[ix].key!=CN_LFLOWER)
		{
			// 缺少"{"
			return false;
		}
		pIf->fbeg=ix; 	

		ix++;
		nBrace=1;
        while((ix<TArry.GetSize())&&(nBrace!=0))
		{ 
			if(TArry[ix].key==CN_LFLOWER)
			{
				nBrace++;
			}

			if(TArry[ix].key==CN_RFLOWER)
			{
				nBrace--;
			}
			ix++;
		} 

   		if(ix>=TArry.GetSize())
		{
			// 内部越界
			return false;
		}

		pIf->fend=ix-1;
		pIf->bodyend=ix-1;

	}  
	else
	{
		// 没有else部分的信息设置
		pIf->ELSEPOS=-1; 
		pIf->fbeg=-1;
		pIf->fend=-1;
	}
	
	return true;

}

void CSDG::SetIfElse(CSDGIf *pIf)
{
	
	int beg,end;
	beg=pIf->bodybeg;
	end=pIf->bodyend;
	
	// 加入if选择分支
	CSDGBase *pLink=new CSDGBase;
	pLink->m_sType="IFLINK";
	pLink->bodybeg=pIf->tbeg;
	pLink->bodyend=pIf->tend;
	pLink->SetNodeVL(1);
	pLink->SetNodeID(CN_IFELSELINK);
	pLink->important=IsImportant(pLink);
	pIf->Add(CN_CONTROL,pLink);
	pLink->SetFather(pIf);
    	
	// 加入else选择分支
	if(pIf->ELSEPOS>pIf->bodybeg && pIf->ELSEPOS<pIf->bodyend)
	{
		pLink=new CSDGBase;
		pLink->m_sType="ELSELINK";
		pLink->bodybeg=pIf->fbeg;
		pLink->bodyend=pIf->fend;
		pLink->SetNodeVL(0);
		pLink->SetNodeID(CN_IFELSELINK);

		pLink->important=IsImportant(pLink);
		pIf->Add(CN_CONTROL,pLink);
		pLink->SetFather(pIf);
	}
		
	

}

//@@@@@@@@@@@@@@####################$$$$$$$$ Switch $$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::SwitchStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将switch语句的节点加入到SDG中，并设置一些节点信息..

	CSDGSwitch *pSwitch=new CSDGSwitch;
	pSwitch->SetNodeID(CN_SWITCHYJ);
	pSwitch->SPOS=idx;
    //extern CString PrintExp(CArray<TNODE,TNODE> &T);////
	if(FindSwitchBody(pSwitch,TArry))
	{
		int ix=pSwitch->expbeg;
		while(ix>=0&&ix<TArry.GetSize()&&ix<pSwitch->expend)
		{
			pSwitch->TCnt.Add(TArry[ix]);
			ix++;
		}
	//	AfxMessageBox(PrintExp(pSwitch->TCnt));///////////

		SetSelectors(pSwitch);

		CSDGBase *pDF=NULL;
		GetDirectFather(pDF,idx);
		if(m_pSDG)
		{
			pSwitch->important=IsImportant(pSwitch);
			m_pSDG->Add(CN_CONTROL,pSwitch);		
			pSwitch->SetFather(m_pSDG);
		}

		
		// 修改栈及栈顶指针
		STACK.AddTail(pSwitch);
		m_pSDG=STACK.GetTail();

	}

}

bool CSDG::FindSwitchBody(CSDGSwitch *pSwitch, CArray<TNODE,TNODE> &TArry)
{
	// F:添加pSwitch的一些信息

	int ix;
   	ix=pSwitch->SPOS+1;
   


	// 搜索switch语句表达式的信息
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LCIRCLE)
	{
	   
		// 缺少'('
		return false;
	}
	pSwitch->expbeg=ix+1;
   
	int nCurve=1;
	ix++;
	while(nCurve!=0&&ix>=0&&ix<TArry.GetSize())
	{
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nCurve++;
		}
		if(TArry[ix].key==CN_RCIRCLE)
		{
			nCurve--;
		}
		ix++;
	}
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}
	pSwitch->expend=ix-1;


	// 搜索switch语句体的信息
   
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LFLOWER) 
	{
		// 缺少'{'
		return false;
	}

	int nBrace=1;
	pSwitch->bodybeg=ix;
	ix++;
	while((ix<TArry.GetSize())&&(nBrace!=0))
	{
		if(TArry[ix].key==CN_LFLOWER)
		{	
			nBrace++;
		}
		if(TArry[ix].key==CN_RFLOWER)
		{
			nBrace--;
		}
		ix++;
	}
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}

   	pSwitch->bodyend=ix-1;


	// 搜索各个选择体的信息
	
	ix=pSwitch->bodybeg+1;
	
	while(ix<pSwitch->bodyend)
	{
		nBrace=0;
		nCurve=0;

		while(ix>=0&&ix<TArry.GetSize()&&ix<pSwitch->bodyend ) 
        {

			if(TArry[ix].key==CN_LFLOWER)
			{
				nBrace++;
			}
			if(TArry[ix].key==CN_RFLOWER)
			{
				nBrace--;
			}
			if(TArry[ix].key==CN_LCIRCLE)
			{
				nCurve++;
			}
			if(TArry[ix].key==CN_RCIRCLE)
			{
				nCurve--;
			}
            if(nBrace==0 && nCurve==0 && (TArry[ix].key==CN_CASE || TArry[ix].key==CN_DEFAULT))
			{
				break;
			}
			else
			{
				ix++;
			}
		}
        
		
		
		if(ix>=pSwitch->bodyend)
		{
			//　switch语句为空语句
			break;
		}

	
		
		if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_CASE )
		{

			// 搜索case选择体的信息

			pSwitch->CASEPOS.Add(ix);
			if(TArry[ix+1].key==CN_CINT || TArry[ix+1].key==CN_CCHAR)
			{
				pSwitch->CASEVL.Add(TArry[ix+1].addr);
				ix+=2;
			}
			//////////////////////wtt////////1.3////case值为负////////
			else if(TArry[ix+1].key==CN_SUB&&TArry[ix+2].key==CN_CINT)
			{
				pSwitch->CASEVL.Add(0-TArry[ix+1].addr);
				ix+=3;
			}
			///////////////////wtt////////////////////////////////////
			else
			{
				pSwitch->CASEVL.Add(TArry[ix+1].addr);
				// 语法错误
				ix+=2;
			}

           // ix+=2;  //wtt
			
			if(TArry[ix].key==CN_SHOW)
			{
				pSwitch->CBEGPOS.Add(ix);  
			}
			else
			{
				pSwitch->CBEGPOS.Add(ix-1);
				// 语法错误
				
			}
			 

		}
		else if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_DEFAULT)
		{
			//AfxMessageBox("CN_DEFAULT");
			pSwitch->CASEPOS.Add(ix);
			pSwitch->HVDefault=true; 
			pSwitch->CASEVL.Add(-1);
			if(TArry[ix+1].key==CN_SHOW)
			{
				pSwitch->CBEGPOS.Add(ix+1);  
			}
			else
			{
				pSwitch->CBEGPOS.Add(ix);
				// 语法错误
				;
			}
		}
		ix++;
	}

	return true;

}

void CSDG::SetSelectors(CSDGSwitch *pSwitch)
{
	// F:将switch的每个选择子加入到父节点是pSwitch的SDG中

	int beg,end;
	beg=pSwitch->bodybeg;
	end=pSwitch->bodyend;

	//CString s;
	//s.Format("se len=%d",pSwitch->CASEPOS.GetSize()) ;
	//AfxMessageBox(s);

    pSwitch->CASEPOS.Add(end);
	for(int ix=0;ix<pSwitch->CASEPOS.GetSize()-1;ix++ )
	{
		CSDGBase *pLink=new CSDGBase;
		pLink->m_sType="SELECTORLINK";
		pLink->bodybeg=pSwitch->CASEPOS[ix];
		pLink->bodyend=pSwitch->CASEPOS[ix+1]-1;
		pLink->SetNodeVL(pSwitch->CASEVL[ix]);
		pLink->SetNodeID(CN_SELECTORLINK);
		
		//CString s;
		//s.Format("selector %d :beg=%d,end=%d",ix,pLink->bodybeg,pLink->bodyend) ;
		//AfxMessageBox(s);

		pLink->important=IsImportant(pLink);
		pSwitch->Add(CN_CONTROL,pLink);
		pLink->SetFather(pSwitch);
	}	

}
//@@@@@@@@@@@@@@####################$$$$$$$$$ While $$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::WhileStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将while语句的节点加入到SDG中，并设置一些节点信息..

	if(TArry[idx].key!=CN_WHILE)
	{
		return;
	}

	CSDGWhile *pWhile=new CSDGWhile;
	pWhile->SetNodeID(CN_WHILEYJ);
	pWhile->WPOS=idx;
	
	if(FindWhileBody(pWhile,TArry))
	{
		
		int ix=pWhile->expbeg;
		while(ix<pWhile->expend)
		{
			pWhile->TCnt.Add(TArry[ix]);
			ix++;
		}

		CSDGBase *pDF=NULL;
		GetDirectFather(pDF,idx);
		if(m_pSDG)
		{
			pWhile->important=IsImportant(pWhile);
			m_pSDG->Add(CN_CONTROL,pWhile);		
			pWhile->SetFather(m_pSDG);
		}
        
		
		// 修改栈及栈顶指针
		STACK.AddTail(pWhile);
		m_pSDG=STACK.GetTail();
		idx=pWhile->expend; 

	}

}

bool CSDG::FindWhileBody(CSDGWhile *pWhile,CArray<TNODE,TNODE> &TArry)
{
	// F:添加pWhile的一些信息
	int ix;
	
	 

	// 搜索while语句的表达式的一些信息
	ix=pWhile->WPOS+1;
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LCIRCLE)
	{
		// 缺少"("
		return false;
	}

	pWhile->expbeg=ix+1;
	int nCurve=1;
	ix++;
	while(ix>=0&&ix<TArry.GetSize()&&nCurve!=0)
	{
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nCurve++;
		}
		if(TArry[ix].key==CN_RCIRCLE)
		{
			nCurve--;
		}
		ix++;
	}
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}
	pWhile->expend=ix-1;



	// 搜索while语句体的一些信息

	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LFLOWER) 
	{
		// 缺少 "{"
		return false;
	}

	int nBrace=1;
	pWhile->bodybeg=ix;
	ix++;
	while((ix<TArry.GetSize())&&(nBrace!=0))
	{
		if(TArry[ix].key==CN_LFLOWER)
		{
			nBrace++;
		}
		if(TArry[ix].key==CN_RFLOWER)
		{
			nBrace--;
		}
		ix++;
	}
	if(ix>=TArry.GetSize())
	{
		// 内部越界
		return false;
	}
   	pWhile->bodyend=ix-1;

	
	return true;


}

//@@@@@@@@@@@@@@####################$$$$$$$$$$$$ increase "++"& descending "--"$$$$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::IncAndDec(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	bool isOK=true;
	int variblePosition;//wtt//1.4
	CSDGIncDec *pIND=new CSDGIncDec;
	pIND->SetNodeID(CN_INCORDEC);
	pIND->kind=TArry[idx].key;
	
	
	
	// 查找递增或递减的变量
	int ix=idx;

    if(   (idx+1<TArry.GetSize() && TArry[idx+1].key==CN_VARIABLE ) 
		||(idx+2<TArry.GetSize() && TArry[idx+1].key==CN_MULTI &&TArry[idx+2].key==CN_VARIABLE ) )
	{

		// 递增或递减运算符在变量的左面。

		pIND->bodybeg=idx+1;
		pIND->expbeg=idx;
		pIND->lor=-1; 
        
		ix++;
		if(TArry[ix].key==CN_VARIABLE && TArry[ix].addr>=0&&TArry[ix].addr<SArry.GetSize()&&(SArry[TArry[ix].addr].kind==CN_VARIABLE||SArry[TArry[ix].addr].kind==CN_POINTER) )
		{
			variblePosition=ix;//wtt//1.4
			pIND->bodyend=ix;
			pIND->expend=ix; 
			
		}
		else if(TArry[ix].key==CN_MULTI &&TArry[ix+1].key==CN_VARIABLE && TArry[ix+1].addr>=0&&TArry[ix+1].addr<SArry.GetSize()&&SArry[TArry[ix+1].addr].kind==CN_POINTER)
		{
			variblePosition=ix+1;//wtt//1.4

			pIND->bodyend=ix+1;
			pIND->expend=ix+1; 			
		}
		else if(TArry[ix].key==CN_VARIABLE && TArry[ix].addr>=0&&TArry[ix].addr<SArry.GetSize() &&SArry[TArry[ix].addr].kind==CN_ARRAY)
		{
			variblePosition=ix;//wtt//1.4

			int n=1;
			ix++;
			if(TArry[ix].key==CN_LREC)
			{
				int i=0;
				ix++;
				while(i<10&&n!=0&&ix<TArry.GetSize())
				{
					if(TArry[ix].key==CN_LREC)
					{
						n++;
					}
					if(TArry[ix].key==CN_RREC)
					{
						n--;
					}
					if(n==0 && ix<TArry.GetSize()-3 && TArry[ix+1].key==CN_LREC )
					{
						n=1;
						ix++;
					}
					ix++;
					i++;
				}

				if( i<10 && n==0 && ix<TArry.GetSize( ))
				{
					//	AfxMessageBox("array");
					pIND->bodyend=ix-1;
					pIND->expend=ix-1; 
				}
				else
				{
					isOK=false;
				}


			}
			else
			{
				isOK=false;
			}

		}
		else
		{
			isOK=false;
		}



	}
	else if(idx+1<TArry.GetSize() &&
		    (TArry[idx+1].key==CN_LINE||TArry[idx+1].key==CN_DOU||TArry[idx+1].key==CN_RCIRCLE||
			 (TArry[idx].line!=TArry[idx+1].line)) )
	{

		// 递增或递减运算符在变量的右面。

		pIND->bodyend=idx-1;
		pIND->expend=idx;
		pIND->lor=1; 
	
		ix--;
		if(ix>=0)
		{
			if(TArry[ix].key==CN_VARIABLE)
			{
				variblePosition=ix;//wtt//1.4

				if(ix>0 && TArry[ix-1].key==CN_MULTI)
				{
					ix--;
				}
				pIND->bodybeg=ix;
				pIND->expbeg=ix; 
			}
			else if(TArry[ix].key==CN_RREC)
			{
				int line=TArry[ix].line; 
				while(ix>=0 && TArry[ix].line==line)
				{
					if(TArry[ix].key==CN_VARIABLE && TArry[ix+1].key==CN_LREC)
					{
						variblePosition=ix;//wtt//1.4

						pIND->bodybeg=ix;
						pIND->expbeg=ix; 
						break;
					}
					ix--;
				}

				ix++;
				if(pIND->bodybeg<0 || pIND->bodybeg>1000 )
				{
					ix=idx+1;
					isOK=false;
						
				}
			}
		}
	}
	else
	{
		isOK=false;
	}
	

    if(isOK)
	{
	//	pIND->m_aDefine.Add(TArry[variblePosition]);//wtt/////1.4///记录++ --表达式的reference definition 集合
	//	pIND->m_aRefer.Add(TArry[variblePosition]); //wtt//1.4
		CSDGBase *pDF=NULL;
		if(pIND->expbeg>1 && pIND->expend<TArry.GetSize()-3)
		{
			int ix;

			// 将整个语句存入TCnt中
			ix=pIND->bodybeg ;
			while(ix<=pIND->bodyend)
			{
				pIND->TCnt.Add(TArry[ix]);
				ix++;
			}
			
			GetDirectFather(pDF,idx);
			
			if(m_pSDG)
			{
				
				if(STACK.GetTail()->m_sType=="SDGFOR")
				{
					
					CSDGFor *pfor=(CSDGFor *)STACK.GetTail();
					if(idx>=pfor->DOUPOS2 && idx<=pfor->expend)
					{
						pIND->SetNodeVL(CN_FOR);
					}

				}
			
				pIND->important=IsImportant(pIND); 
				m_pSDG->Add(CN_CONTROL,pIND); 
				pIND->SetFather(m_pSDG);
			
			}
		}		
	}
}

//@@@@@@@@@@@@@@####################$$$$$$$$$$$ Break statement $$$$$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//
void CSDG::BreakStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将Break语句节点SDGBreak 加入SDG中，并设置一些节点信息.

	CSDGBreak *pBreak=new CSDGBreak;
	pBreak->BPOS=idx;
	pBreak->SetNodeID(CN_BREAK);
	pBreak->bodybeg=idx;
	pBreak->bodyend=idx;
	pBreak->TCnt.Add(TArry[idx]);  
	
	
	
	CSDGBase *pDF=NULL;
	GetDirectFather( pDF, pBreak->BPOS );
	if(m_pSDG)
	{
		pBreak->important=IsImportant(pBreak);
		m_pSDG->Add(CN_CONTROL,pBreak);		
		pBreak->SetFather(m_pSDG);
	}
	
}

//@@@@@@@@@@@@@@####################$$$$$$$$$$$ Continue statement $$$$$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::ContinueStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将continue语句节点SDGContinue 加入SDG中，并设置一些节点信息.

	CSDGContinue *pContinue=new CSDGContinue;
	pContinue->CPOS=idx;
	pContinue->SetNodeID(CN_CONTINUE);
	pContinue->bodybeg=idx;
	pContinue->bodyend=idx;
	pContinue->TCnt.Add(TArry[idx]); 
	
	
	CSDGBase *pDF=NULL;
	GetDirectFather( pDF, pContinue->CPOS );
	if(m_pSDG)
	{
		pContinue->important=IsImportant(pContinue);
		m_pSDG->Add(CN_CONTROL,pContinue);		
		pContinue->SetFather(m_pSDG);
	}
	
}

//@@@@@@@@@@@@@@####################$$$$$$$$$ Return $$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//
void CSDG::ReturnStatement( int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)//wtt//3.13//
//void CSDG::ReturnStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// F:将return语句节点SDGReturn 加入SDG中，并设置一些节点信息.

	CSDGReturn *pReturn=new CSDGReturn;
	pReturn->SetNodeID(CN_RETURN);
	pReturn->bodybeg=idx;
    
	int ix=idx+1;
	int line=0;
	if(ix>=0&&ix<TArry.GetSize())
	line=TArry[ix].line;

	if(ix>=0&&ix+2<TArry.GetSize() && TArry[ix].key==CN_LCIRCLE &&
	   TArry[ix+2].key==CN_RCIRCLE && VariableType(TArry[ix+1].key))
	{
		ix+=3;		
	}

	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key==CN_LCIRCLE)
	{
		ix++;
		int nkh=1;
		pReturn->bodybeg++;;
		while(ix<TArry.GetSize() && nkh!=0 && TArry[ix].key>=0 && TArry[ix].key!=CN_LINE &&TArry[ix].line==line )
		{
			if(TArry[ix].key==CN_LCIRCLE)
			{
				nkh++;
			}

			if(TArry[ix].key==CN_RCIRCLE)
			{
				nkh--;
			}
			ix++;
		}

		pReturn->bodyend=ix-2;

	}
	else
	{
		while(ix<TArry.GetSize() && TArry[ix].key>=0 && TArry[ix].key!=CN_LINE &&TArry[ix].line==line )
		{
			ix++;
		}
		pReturn->bodyend=ix-1;

	}

	for(int i=pReturn->bodybeg+1;i<TArry.GetSize()&&i<=pReturn->bodyend;i++)
	{
		pReturn->TCnt.Add(TArry[i]); 

	}
   

	CSDGBase *pDF=NULL;
	GetDirectFather( pDF, idx );
	if(m_pSDG)
	{
		pReturn->important=IsImportant(pReturn);
		m_pSDG->Add(CN_CONTROL,pReturn);
		pReturn->SetFather(m_pSDG);
	}
	idx=pReturn->bodyend; 

}

//@@@@@@@@@@@@@@###################$$$$$$$$$$$$call function whitout return type$$$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::CallNode(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	
	CSDGCall *pCall=new CSDGCall;
	pCall->SetNodeID(CN_RETURN);
	pCall->bodybeg=idx;

    //AfxMessageBox("call");

	int ix=idx+1;
	//int k;
	int line=0;
	if(ix>=0&&ix<TArry.GetSize())
	line=TArry[ix].line; 
	
	if(ix>=0&&ix<TArry.GetSize()&&TArry[ix].key!=CN_LCIRCLE)
	{		
		// error
		if(pCall)
		delete pCall;
		return;
	}

	// find bound of the user defined function
	ix++;
	int nkh=1;

	while(ix<TArry.GetSize() && nkh!=0 && TArry[ix].key>=0 && TArry[ix].key!=CN_LINE &&TArry[ix].line==line )
	{
		if(TArry[ix].key==CN_LCIRCLE)
		{
			nkh++;
		}
		if(TArry[ix].key==CN_RCIRCLE)
		{
			nkh--;
		}
		ix++;
	}

	pCall->bodyend=ix-1;

    for(int i=pCall->bodybeg+2;i>=0&&i<TArry.GetSize()&&i<pCall->bodyend;i++)
	{
		pCall->TCnt.Add(TArry[i]);
	}
    if(ix>=0&&ix<TArry.GetSize())
	{
	pCall->m_tFun.key=TArry[idx].key;
	pCall->m_tFun.addr=TArry[idx].addr;/////////
	pCall->m_tFun.name=TArry[idx].name;
	pCall->m_tFun.deref=TArry[idx].deref;//wtt 2008 3.19

	
//	CString stemp;
//	stemp.Format("%d",pCall->m_tFun.addr);

//	AfxMessageBox(pCall->m_tFun.name+C_ALL_KEYWORD[pCall->m_tFun.key]+stemp);//////////////
	pCall->m_tFun.line=TArry[idx].line;
	pCall->m_tFun.value=0;
	}
	if(idx>=0&&idx<TArry.GetSize()&&TArry[idx].key==CN_DFUNCTION)
	{
		pCall->SetNodeID(CN_DFCALL);
	
	}
	else
	{
		pCall->SetNodeID(CN_BFCALL);
		///////////////////wtt////1.4/////为printf建立reference集合///
		//////////////////////为scanf建立definition集合///////////
	/*	if(TArry[idx].name=="printf")
		{
			k=pCall->bodybeg ;
			while(k<pCall->bodyend)
			{
				if(TArry[k].key==CN_VARIABLE)
					pCall->m_aRefer.Add(TArry[k]);
				k++;
			}
		}
		if(TArry[idx].name=="scanf")
		{
			k=pCall->bodybeg ;
			while(k<pCall->bodyend)
			{
				if(TArry[k].key==CN_VARIABLE)
					pCall->m_aDefine.Add(TArry[k]);
				k++;
			}
		}*/
		////////////////////////wtt///////////////////////

	}

    CSDGBase * pDF=NULL;
	GetDirectFather(pDF, idx );
	if(m_pSDG)
	{
		pCall->important=IsImportant(pCall);
		m_pSDG->Add(CN_CONTROL,pCall);
		pCall->SetFather(m_pSDG);
		idx=pCall->bodyend; 
	}

}


//@@@@@@@@@@@@@@###################$$$$$$$$$$$$SetControlFlow$$$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//

void CSDG::SetBreakAndContinue(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// set jumping positions to "continue" and "break" statement
	// using inorder traverse method

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
    p=m_pHead;
   

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SDGBREAK" ||p->m_sType=="SDGDEFAULT"|| p->m_sType=="SDGCONTINUE")
				{
					GetBreakAndContinue(p);									
				}
				
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);		
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
}

void CSDG::GetBreakAndContinue(CSDGBase *pBC)
{
    // find the next execute position when "break" or "continue" is executed;

	CSDGBase *ptr=pBC;
	CSDGBreak *pB=(CSDGBreak *   )pBC;
	CSDGContinue *pC=(CSDGContinue *)pBC;

	ptr=ptr->GetFather(); 
	
	while( ptr &&  ptr->m_sType!="SDGSWITCH"  && ptr->m_sType!="SDGFOR" 
		   && ptr->m_sType!="SDGDOWHILE" && ptr->m_sType!="SDGWHILE"
		   && ptr->m_sType!="SDGENTRY"   && ptr->m_sType!="SDGHEAD" )
	{
		ptr=ptr->GetFather(); 
	}

	// error: invalid use of "continue" or "break"
	if(ptr==NULL || ptr->m_sType=="SDGENTRY" || ptr->m_sType=="SDGHEAD" )
	{
		pB->pTO=NULL;
		pC->pTO=NULL; 
		return;
	}


    // "break" or "default" statement...set next executive position
	if(pBC->m_sType=="SDGBREAK" || pBC->m_sType=="SDGDEFAULT")
	{
		if(pBC->m_sType=="SDGDEFAULT"&&ptr->m_sType!="SDGSWITCH")
		{
			//error:misplaced default!
		}
		CSDGBase *p=ptr->GetFather();
		if(p!=NULL)
		{
			int ix=0;
			while(ix<p->GetRelateNum())
			{
				if(p->GetNode(ix)->m_sType==ptr->m_sType && p->GetNode(ix)->bodybeg==ptr->bodybeg &&p->GetNode(ix)->bodyend==ptr->bodyend )
				{
					ix++;
					break;
				}
				ix++;
			}

			if(ix>=p->GetRelateNum())
			{
				if(p->m_sType=="SDGFOR" || p->m_sType=="SDGDOWHILE" || p->m_sType=="SDGWHILE" )//wtt"SDGFOE--SDGFOR"
				{
					pB->pTO=ptr->GetFather(); 

				}
				else
				{
					pB->pTO=NULL; 
				}
			}
			else 
			{
				pB->pTO=p->GetNode(ix);
				//AfxMessageBox(pB->m_sType+" to "+pB->pTO->m_sType); 
			}
		}		
	}
	else
	{
		// "continue" statement...;
		if(ptr->m_sType=="SDGSWITCH")
		{
			// error: invalid use of "continue". it can not be used in the "switch" statement   
			if(ptr->GetFather() &&(ptr->GetFather()->m_sType=="SDGFOR" || ptr->GetFather()->m_sType=="SDGWHILE"|| ptr->GetFather()->m_sType=="SDGDOWHILE" ))
			{
				pC->pTO=ptr->GetFather();  
			}
			else
			{
				pC->pTO=NULL; 
				return;
			}
		}
		else
		{
			pC->pTO=ptr;
		}
	}	
}

void CSDG::RenameFunction(CSDGBase *pHead,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// Rename the functin order based on the calling order

	bool bmain=false;
	for(int ix=0;ix<FArry.GetSize() && ix<1;ix++)
	{

		for(int j=FArry[ix].bbeg;j<=FArry[ix].bend;j++)
		{
			if(j>=0&&j<TArry.GetSize()&&TArry[j].key==CN_DFUNCTION && TArry[j].addr!=ix)
			{
				bmain=true;
				ix=FArry.GetSize();
				break;
			}
		}
	}

	if(FArry.GetSize()>=2)
	{
		if(bmain)
		{
			for(int ix=0;ix<SArry.GetSize();ix++)
			{
				if(ix>=0&&ix<SArry.GetSize()&&SArry[ix].kind==CN_DFUNCTION && SArry[ix].layer==1)
				{
					SArry[ix].name="main";
					break;
				}
			}

			FArry[0].name="main";

			for(int ix=0;ix<SArry.GetSize();ix++)
			{
				if(SArry[ix].kind==CN_DFUNCTION && SArry[ix].layer==2)
				{
					SArry[ix].name="sub-function-01";
					break;
				}
			}

			FArry[1].name="sub-function-01";
		}
		else
		{
			for(int ix=0;ix<SArry.GetSize();ix++)
			{
				if(SArry[ix].kind==CN_DFUNCTION && SArry[ix].layer==2)
				{
					SArry[ix].name="main";
					break;
				}
			}

			FArry[1].name="main";

			for(int ix=0;ix<SArry.GetSize();ix++)
			{
				if(SArry[ix].kind==CN_DFUNCTION && SArry[ix].layer==1)
				{
					SArry[ix].name="sub-function-01";
					break;
				}
			}
			FArry[0].name="sub-function-01";

			int f1=-1,f2=-1;
			int ix;
			for( ix=0;ix<pHead->GetRelateNum();ix++)
			{
				if(pHead->GetNode(ix)&&pHead->GetNode(ix)->m_sType=="SDGENTRY")
				{
					f1=ix;
					ix++;
					break;
				}
			}
		
			for(;ix<pHead->GetRelateNum();ix++)
			{
				if(pHead->GetNode(ix)&&pHead->GetNode(ix)->m_sType=="SDGENTRY")
				{
					f2=ix;					
					break;
				}
			}

			if(f1>=0 && f2>=0)
			{
				CSDGBase *pTemp;
				pTemp=pHead->GetNode(f2);
				
				pHead->InsertNode(f1,pTemp);
				pTemp=pHead->GetNode(f1+1); 
				pHead->Del(f1+1);
				pHead->InsertNode(f2,pTemp);
				pHead->Del(f2+1);
			}
		}
	}
	else if(FArry.GetSize()==1)
	{
		for(int ix=0;ix<SArry.GetSize();ix++)
		{
			if(SArry[ix].kind==CN_DFUNCTION)
			{
				SArry[ix].name="main";
				break;
			}
		}

		FArry[0].name="main"; 
	}

	SearchSDGRenameFunction(pHead,SArry,FArry);
	
}

void CSDG::SearchSDGRenameFunction(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	// search SDG and rename Function

	CSDGBase *p=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	p=pHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here
				SearchExpRenameFunction(p->TCnt,SArry,FArry);
				if(p->m_sType=="SDGASSIGNMENT")
				{
					CSDGAssignment *pct=(CSDGAssignment *)p;
					SearchExpRenameFunction(pct->TEXP,SArry,FArry);
				}				
				// End of this part				
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }	
}

void CSDG::SearchExpRenameFunction(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	for(int ix=0;ix<TEXP.GetSize();ix++)
	{
		if(TEXP[ix].key==CN_DFUNCTION)
		{
			TEXP[ix].name=FArry[TEXP[ix].addr].name;  			
		}
	}
}
//@@@@@@@@@@@@@@############$$$$########$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&//
void CSDG::SetSwitchCondition(CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//wtt//5.16//若switch的条件是表达式exp,即switch(exp),则var=exp提取到switch外，switch( var)
	extern int FindIndex(CSDGBase*pson);
//	extern CString PrintExp(CArray<TNODE,TNODE> &T);
	if(pHead==NULL)
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		SetSwitchCondition(pHead->GetNode(i),SArry);
	}
	if(pHead->m_sType=="SDGSWITCH"&&pHead->TCnt.GetSize()>1)
	{
	//	AfxMessageBox("untransform:"+PrintExp(pHead->TCnt));////
		SNODE SN;
		SN.addr=SArry.GetSize() ;
			///////////////////
		SN.kind=CN_VARIABLE;
		SN.arrdim=0;
		for(int j=0;j<4;j++)
			SN.arrsize[j]=0 ;
		SN.addr=-1;
		SN.value=0;
		CSDGBase*pf1;
		pf1=pHead;
		while(pf1&&pf1->m_sType!="SDGENTRY" )
		{
			pf1=pf1->GetFather(); 
		}
		if(pf1==NULL)
			return;
		if(pf1->TCnt.GetSize()<=0 )
			return;
		SN.layer=pf1->TCnt[0].addr+1;
		CString s;
		s.Format("_%d",SArry.GetSize());
		SN.name="switch_"+s;
		SN.type= CN_INT;   	
	//	AfxMessageBox("add sn");/////
		SArry.Add(SN);

		CSDGDeclare*pdec=new CSDGDeclare;
		pdec->m_sType="#DECLARE";		
		TNODE T,T1,T2;
		T2.key=T1.key=T.key=CN_VARIABLE;
		T2.addr= T1.addr =T.addr=SArry.GetSize()-1;
		T2.name= T1.name=T.name=SN.name;
		T2.line =T1.line=T.line=pHead->TCnt[0].line;
		T2.paddr =T1.paddr=T.paddr=0;
		T2.value= T1.value=T.value=0;  
		T2.deref= T1.deref=T.deref=0;  //wtt 2008 3.19

		pdec->TCnt.Add(T); 
		pf1->InsertNode(0,pdec);
		pdec->SetFather(pf1); 

		CSDGAssignment*pas=new CSDGAssignment;
		pas->m_sType="SDGASSIGNMENT";
		pas->SetNodeID(CN_ASSIGNMENT);
		pas->OPKIND=CN_EQUAL;
		pas->TCnt.Add(T1);
		for(int j=0;j<pHead->TCnt.GetSize();j++ )
		{
			TNODE T;
			T.addr= pHead->TCnt[j].addr;
			T.key=pHead->TCnt[j].key;
			T.line=pHead->TCnt[j].line;
			T.name=pHead->TCnt[j].name;
			T.paddr=pHead->TCnt[j].paddr;
			T.value=pHead->TCnt[j].value; 
			T.deref=pHead->TCnt[j].deref;  //wtt 2008 3.19
			pas->TEXP.Add(T);
		}	
		//	AfxMessageBox("assignment:"+PrintExp(pHead->TCnt));////

		int index=FindIndex(pHead);
		CSDGBase*pf=NULL;
		pf=pHead->GetFather();
		if(pf)
		{
			pf->InsertNode(index,pas); 
			pas->SetFather(pf);
			pHead->TCnt.RemoveAll();
			pHead->TCnt.Add(T2); 
	//		AfxMessageBox("aftertransform:"+PrintExp(pHead->TCnt));////
		}
			////////////////////////////////


	}
}