// CustomParamDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "CustomParamDlg.h"
#include "afxdialogex.h"


// CCustomParamDlg 对话框

IMPLEMENT_DYNAMIC(CCustomParamDlg, CDialog)

CCustomParamDlg::CCustomParamDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCustomParamDlg::IDD, pParent)
	, m_strEdit(_T(""))
{

}

CCustomParamDlg::~CCustomParamDlg()
{
}

void CCustomParamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	//Polaris-20140716
	DDX_Control(pDX, IDC_EDIT1, m_edit);
	DDX_Text(pDX, IDC_EDIT1, m_strEdit);
}


BEGIN_MESSAGE_MAP(CCustomParamDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CCustomParamDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CCustomParamDlg 消息处理程序

//Polaris-20140716
void CCustomParamDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}
