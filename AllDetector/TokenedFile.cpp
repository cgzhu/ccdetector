// TokenedFile.cpp: implementation of the TokenedFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TokenedFile.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TokenedFile::TokenedFile()
{
	fileName = _T("");
	hashedFileName = 0;
	tokenList.RemoveAll();
}

TokenedFile::~TokenedFile()
{
	fileName = _T("");
	hashedFileName = 0;
	tokenList.RemoveAll();
}

TokenedFile::TokenedFile(const TokenedFile &TF)
{
	fileName = TF.fileName;
	hashedFileName = TF.hashedFileName;
	tokenList.RemoveAll();
	tokenList.Copy(TF.tokenList);
	SArry.Copy( TF.SArry );
	FArry.Copy( TF.FArry );
}

TokenedFile &TokenedFile::operator=(const TokenedFile &TF)
{
	fileName = TF.fileName;
	hashedFileName = TF.hashedFileName;
	tokenList.RemoveAll();
	tokenList.Copy(TF.tokenList);
	SArry.Copy( TF.SArry );
	FArry.Copy( TF.FArry );
	return *this;
}
void TokenedFile::Assign(const CString &p_fileName,
						 const long &p_hashedFileName,
						 const CArray<TokenLine,TokenLine> &p_tokenList,
						 const CArray<SNODE,SNODE>& p_SArry,//wq-20090505
						 const CArray<FNODE,FNODE>& p_FArry)//wq-20090505
{
	fileName = p_fileName;
	hashedFileName = p_hashedFileName;
	tokenList.RemoveAll();
	tokenList.Copy(p_tokenList);
	SArry.Copy(p_SArry);
	FArry.Copy(p_FArry);
}

CString& TokenedFile::GetFileName()
{
	return fileName;
}
long& TokenedFile::GetHashedFileName()
{
	return hashedFileName;
}
CArray<TokenLine,TokenLine>& TokenedFile::GetTokenList()
{
	return tokenList;
}
void TokenedFile::SetFileName(const CString& p_fileName)
{
	fileName = p_fileName;
}
void TokenedFile::SetHashedFileName(const long& p_HashedFileName)
{
	hashedFileName = p_HashedFileName;
}
void TokenedFile::SetTokenList(const CArray<TokenLine,TokenLine>& p_tokenList)
{
	tokenList.RemoveAll();
	tokenList.Append(p_tokenList);
}
void TokenedFile::AddTokenList(const TokenLine& TL)
{
	tokenList.Add(TL);
}
CArray<TNODE,TNODE>& TokenedFile::GetTokenLine(const int& index)
{
	return tokenList[index].GetTokenLine();

}

void TokenedFile::Clean()
{
	int i;
	for( i = 0; i < tokenList.GetSize(); i++ )
	{
		tokenList[i].Clean();
	}
	tokenList.RemoveAll();
	SArry.RemoveAll();
	FArry.RemoveAll();
	fileName = "";
	hashedFileName = 0;

}
