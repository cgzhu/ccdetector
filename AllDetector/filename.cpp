/********************************************************************
	created:	2011/02/07
	created:	7:2:2011   10:25
	filename: 	D:\BugFinder\BugFinder\filename.cpp
	file path:	D:\BugFinder\BugFinder
	file base:	filename
	file ext:	cpp
	author:		qj
	
	purpose:	�ļ���
*********************************************************************/
#include "stdafx.h"
#include "filename.h"

void get_filenames(){
	TCHAR szTempPath[MAX_PATH];
	GetTempPath(MAX_PATH, szTempPath);

	wsprintf(PREPROCESSED_FILE,_T("%sPREPROCESSED_FILE"),szTempPath);
	wsprintf(IDEOPERDECT_FILE,_T("%sIDEOPERDECT_FILE"),szTempPath);
	wsprintf(REDUASSIGNDECT_FILE,_T("%sREDUASSIGNDECT_FILE"),szTempPath);
	wsprintf(DEADCODEDECT_FILE,_T("%sDEADCODEDECT_FILE"),szTempPath);
	wsprintf(REDUCONDDECT_FILE,_T("%sREDUCONDDECT_FILE"),szTempPath);
	wsprintf(HIDEOPERDECT_FILE,_T("%sHIDEOPERDECT_FILE"),szTempPath);
	wsprintf(BUG_DETECT_IMIDIATE_FILE,_T("%sBUG_DETECT_IMIDIATE_FILE"),szTempPath);
	wsprintf(AST_FILE,_T("%sAST_FILE"),szTempPath);
	wsprintf(STRUCT_NAME_FILE,_T("%sSTRUCT_NAME_FILE"),szTempPath);
	wsprintf(TYPE_NAME_FILE,_T("%sTYPE_NAME_FILE"),szTempPath);
	wsprintf(REDUPARAMETER_FILE,_T("%sREDUPARAMETER_FILE"),szTempPath);
}

void delete_files(){
	DeleteFile(PREPROCESSED_FILE);
	DeleteFile(IDEOPERDECT_FILE);
	DeleteFile(REDUASSIGNDECT_FILE);
	DeleteFile(DEADCODEDECT_FILE);
	DeleteFile(REDUCONDDECT_FILE);
	DeleteFile(HIDEOPERDECT_FILE);
	DeleteFile(BUG_DETECT_IMIDIATE_FILE);
	DeleteFile(AST_FILE);
	DeleteFile(STRUCT_NAME_FILE);
	DeleteFile(TYPE_NAME_FILE);
}