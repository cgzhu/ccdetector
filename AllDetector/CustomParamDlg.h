#pragma once
#include "afxwin.h"

// CCustomParamDlg 对话框

class CCustomParamDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCustomParamDlg)

public:
	CCustomParamDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCustomParamDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

//Polaris-20140716
public:
	CEdit m_edit;
	CString m_strEdit;
	afx_msg void OnBnClickedOk();
};
