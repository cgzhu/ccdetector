#pragma once
#include "rc_type_tchart.h"


// CCCSegFileDlg 对话框

class CCCSegFileDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCCSegFileDlg)

public:
	CCCSegFileDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCCSegFileDlg();

// 对话框数据
	enum { IDD = IDD_CC_SEG_FILES_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart cc_seg_file_column_chart;
	void ClearAllSeries();
};
