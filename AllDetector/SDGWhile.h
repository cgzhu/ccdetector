// SDGWhile.h: interface for the CSDGWhile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGWHILE_H__6056064D_F902_490A_9F12_C7A8D19277DE__INCLUDED_)
#define AFX_SDGWHILE_H__6056064D_F902_490A_9F12_C7A8D19277DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGWhile : public CSDGBase  
{
public:
	CSDGWhile();
	virtual ~CSDGWhile();
public:
	int WPOS;
	int expbeg;
	int expend;

};

#endif // !defined(AFX_SDGWHILE_H__6056064D_F902_490A_9F12_C7A8D19277DE__INCLUDED_)
