// SDGBase.cpp: implementation of the CSDGBase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGBase.h"

// SDGBase.cpp: implementation of the CSDGBase class.
//
//////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSDGBase::CSDGBase()
{
	extern int SDGNODEID; 
	m_iNodeID=0;
	m_iNodeVL=0;
	visit=0;
	mvisit=0;
	important=0;
	SDGNODEID++;
	idno=SDGNODEID; 
	m_sType="SDGBASE";
	b_formal=false;///////wtt////////3.24///
//	b_return=false;
	no=-1;
	pFather=NULL;
	pCorrspS=NULL;
	pCorrspM=NULL;
    m_pinfo=NULL;
	bodybeg=-1;
	bodyend=-1;
	m_sName="";
	////////////////wtt////////////////////
	m_aRefer.RemoveAll();
	m_aDefine.RemoveAll();
	m_aIn.RemoveAll();
	m_aOut.RemoveAll();
	m_aKill.RemoveAll(); 
	m_pDRNodes.RemoveAll();
	m_pAntiDRNodes.RemoveAll();
	m_pDecRNodes.RemoveAll();
	/////////////////wtt//////////////////////
//	m_aDRArry.RemoveAll();
//	m_aAntiDRArry.RemoveAll();
//	m_aDecRArry.RemoveAll(); 

}

CSDGBase::~CSDGBase()
{
	

}
//////////////////////////////wtt origin from SDGBASE.h
void CSDGBase:: SetFather(CSDGBase * pF)
	{
		pFather=pF;
	}
CSDGBase * CSDGBase::GetFather()
	{
		return pFather;
	}
void CSDGBase:: SetNodeID(const int NID)
	{
		m_iNodeID=NID;
	}

void CSDGBase::SetNodeVL(const int NVL)
	{
		m_iNodeVL=NVL;
	}
int CSDGBase:: GetNodeID()
	{
		return m_iNodeID;
	}
int CSDGBase:: GetNodeVL()
	{
		return m_iNodeVL;
	}
int CSDGBase::GetRelateNum()
	{
		return m_aRArry.GetSize();
	}
//////////////////////////////

bool CSDGBase::Add(const int RID,CSDGBase *pNode)
{
	this->m_aRArry.Add(RID);
	this->m_pRNodes.Add(pNode); 
	return true;
}

CSDGBase *CSDGBase::GetNode(const int idx)
{

	if(idx<m_pRNodes.GetSize()&& idx>=0)
	{
		return m_pRNodes[idx];

	}

	return NULL;
}

int CSDGBase::GetRelation(const int idx)
{
	if(idx<m_aRArry.GetSize()&& idx>=0)
	{
		return m_aRArry[idx];
	}
	else
	{
		return -1;
	}

}

bool CSDGBase::Del(const int idx)
{
	
	if(idx>=0&&idx<m_pRNodes.GetSize())
	m_pRNodes.RemoveAt(idx);
	if(idx<m_aRArry.GetSize()&& idx>=0)
	m_aRArry.RemoveAt(idx);
	return true;
}

bool CSDGBase::SetNode(int idx,CSDGBase *ptr)
{

	
	CSDGBase *p=NULL;
	if(idx>=0&&idx<m_pRNodes.GetSize())
	{
		p=m_pRNodes[idx] ;
		m_pRNodes[idx]=ptr;
	}
	if(idx<m_aRArry.GetSize()&& idx>=0)
    m_aRArry[idx]=ptr->GetNodeID();
	if(p)
	{

		delete p;
		p=NULL;
	}
	
	return true;

}

bool CSDGBase::InsertNode(int idx,CSDGBase *ptr)
{
	if(idx<0)
		return false;
	m_pRNodes.InsertAt(idx,ptr);
    m_aRArry.InsertAt(idx,ptr->GetNodeID());
	return true;
}

void CSDGBase::RemoveAll()
{
		m_pRNodes.RemoveAll();
		m_aRArry.RemoveAll();
}

bool CSDGBase::FactorOf(const int currpos)
{		
	if(currpos>=bodybeg && currpos<=bodyend)
	{			
		return true;
	}
	else
	{
		return false;
	}
}

void CSDGBase::SetNodeCM(const int idx,CSDGBase *ptr)
{
	if(idx>=m_pRNodes.GetSize()|| idx<0)
		return ;
	m_pRNodes[idx]=ptr;
}


void CSDGBase::AddDataFlow(CSDGBase *p)
{
	m_pDRNodes.Add(p);
}
void CSDGBase::AddAntiDataFlow(CSDGBase *p)
{
	m_pAntiDRNodes.Add(p);
}
void CSDGBase::AddDecFlow(CSDGBase *p)
{
	m_pDecRNodes.Add(p);
}

void CSDGBase::Clear()
{
	
	pFather=NULL;
	pCorrspS=NULL;
	pCorrspM=NULL;
    m_pinfo=NULL;
	bodybeg=-1;
	bodyend=-1;
	m_sName="";
	////////////////wtt////////////////////
	m_pRNodes.RemoveAll();
	m_aRArry.RemoveAll(); 
	m_aRefer.RemoveAll();
	m_aDefine.RemoveAll();
	m_aIn.RemoveAll();
	m_aOut.RemoveAll();
	m_aKill.RemoveAll(); 
	m_pDRNodes.RemoveAll();
	m_pAntiDRNodes.RemoveAll();
	m_pDecRNodes.RemoveAll();

}