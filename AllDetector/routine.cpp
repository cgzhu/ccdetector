/********************************************************************
	created:	2011/02/09
	created:	9:2:2011   18:36
	filename: 	D:\BugFinder\BugFinder\routine.cpp
	file path:	D:\BugFinder\BugFinder
	file base:	routine
	file ext:	cpp
	author:		qj
	
	purpose:	检测函数
*********************************************************************/
#include "stdafx.h"

#include <vector>

#include "MainFrm.h"

#include "routine.h"
#include "detect_routine.h"

using namespace std;

#ifndef _FILE_EXTERN
#define _FILE_EXTERN
#endif
#include "filename.h"

#include "convert_bug_file.h"

vector<CString> m_file_include;
CString gcc_param;

//Polaris-20140829
int count_ideoper = 0;
int count_reduassign = 0;
int count_deadcode = 0;
int count_reducond = 0;
int count_hideoper = 0;

map<string,int> rc_file_map;
map<string,map<int,int>> rc_type_file_map;

///////////////////////////////////
Routine routine[] ={
	{
		ideOperDect,_T("幂等缺陷"),IDEOPERDECT_FILE
	},
	{
		reduAssignDect,_T("冗余赋值"),REDUASSIGNDECT_FILE
		},
		{
			deadCodeDect,_T("死代码"),DEADCODEDECT_FILE
		}
		,
		{reduCondDect_2,_T("冗余条件表达式"),REDUCONDDECT_FILE
		},
		{hideOperDect,_T("隐式幂等表达式"),HIDEOPERDECT_FILE
		},
		/*{reduParameter,_T("冗余参数"),REDUPARAMETER_FILE
		}*/
} ;


void LoadAST(vector<treeNode>& m_nodes)
{
	ifstream infile(AST_FILE,ios::binary);	

	if (!infile) return;

	do {
		treeNode tr;
		//type
		infile.read((char*)(&tr.type),sizeof(nodeType));

		if (infile.eof()) break;
		int c;
#define read_int() infile.read((char*)(&c),sizeof(int)); \
	if (infile.eof()) break;

		//str
		read_int();//字符串长度
		if (c<0) 
			break;
		char* t = new char[c+1];
		t[c]=0;
		if (c>0) infile.read(t,c);
		tr.str = t;
		delete [] t;
		//lineno
		read_int();
		tr.lineno = c;

		read_int();//子节点个数
		int len = c;

		for (int i=0;i<len;i++){
			read_int();
			tr._subNode.push_back(c);
		}

		read_int();
		tr.index = c;
		tr.parentNode=NULL;
		m_nodes.push_back(tr);
	} while (!infile.eof());
	infile.close();

	//修复链接
	for (int i=0;i<m_nodes.size();i++)
	{
		//subNode
		for (int j=0;j<m_nodes[i]._subNode.size();j++)
		{
			int index = m_nodes[i]._subNode[j];
			//搜索index
			for (int k=0;k<m_nodes.size();k++)
			{
				if (m_nodes[k].index==index)  //找到了?
				{
					m_nodes[i].subNode.push_back(&m_nodes[k]);
					m_nodes[k].parentNode = &m_nodes[i];
					break;
				}
			}
		}
	}
}

#define ERROR_GETAST_CANNOT_CREATE_PROCESS2 5

// 获取AST
int GetAST(LPCTSTR filename)
{
	get_filenames();
	delete_files();

	STARTUPINFO  si;
	PROCESS_INFORMATION pi;
	
    int status =Preprocess(filename);
	if (status)
	{
		return status;
	}

	ZeroMemory( &si, sizeof(si) );
	si.cb=sizeof(si);   
	si.dwFlags   =   STARTF_USESHOWWINDOW ;
	si.wShowWindow   =   SW_HIDE;
	ZeroMemory( &pi, sizeof(pi) );

	TCHAR cmd[MAX_PATH*2];
	wsprintf(cmd,_T("parser.exe \"%s\" \"%s\""), PREPROCESSED_FILE, AST_FILE);
	
	if (!CreateProcess(NULL, cmd, NULL, NULL, FALSE,  0, NULL, NULL, &si, &pi))
	{
		return ERROR_GETAST_CANNOT_CREATE_PROCESS2;
	}

	WaitForSingleObject(pi.hProcess,  INFINITE);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	return 0;
}

BOOL GetASTGraph(vector<treeNode>& m_nodes)
{
	int i;
	char szTempPath[MAX_PATH],szTempfile[MAX_PATH]; 
	GetTempPath(MAX_PATH, szTempPath);
	GetTempFileName(szTempPath,_T ("my_"),0,szTempfile); 
	
	ofstream o(szTempfile);
	o<<"digraph G {";
	
	// 先输出点
	char t[21];
	t[20]=0;
	for (i=0;i<m_nodes.size();i++)
	{
		 strncpy_s(t,m_nodes[i].str.c_str(),20);

		 //'"' -> '\''
		 for (int j=0; j<m_nodes[i].str.length();j++)
		 {
			if (t[j]=='"') t[j] = '\'';
		 }

		 o << "n" << m_nodes[i].index << "[label=\"" << t << "\"];";
	}

	// 输出关系
	for (i =0;i<m_nodes.size();i++)
	{
		// subNode
		for (int j=0;j<m_nodes[i]._subNode.size();j++)
		{
			o<<"n"<<m_nodes[i].index<<"->n"<<m_nodes[i]._subNode[j]<<";";
		}
	}

	o<<"}";

	o.close();

	// execute
	STARTUPINFO  si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb=sizeof(si);   
	si.dwFlags   =   STARTF_USESHOWWINDOW ;
	si.wShowWindow   =   SW_HIDE;
	ZeroMemory( &pi, sizeof(pi) );
	
	GetTempPath(MAX_PATH, szTempPath);
	strcat_s(szTempPath,"DOT_GRAPH.png");
	DeleteFile(szTempPath);

	TCHAR cmd[MAX_PATH*2];
	wsprintf(cmd,_T("Graphviz2.27\\bin\\dot.exe -Tpng \"%s\" -o \"%s\""),szTempfile,szTempPath);

	if (!CreateProcess(NULL, cmd, NULL, NULL, FALSE,  0, NULL, NULL, &si, &pi))
	{
		return FALSE;
	}

	WaitForSingleObject(pi.hProcess,  INFINITE);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	ShellExecute(AfxGetMainWnd()->m_hWnd,"open",szTempPath,NULL,NULL,SW_SHOWNORMAL);
	return TRUE;
}

void Add_file_path(const char *writed_file,const char* filename)
{
	CFile file;
	file.Open(writed_file, CFile::modeCreate | CFile::modeWrite);		
	file.Write("*",1);
	file.Write(filename, strlen(filename));
	file.Write("\n",1);
	file.Flush();
	file.Close();	
}

#define ERROR_PREPROCESS_CANNOT_CREATE_FILE 1
#define ERROR_PREPROCESS_CANNOT_CREATE_PROCESS 2
#define ERROR_PARSER_CANNOT_CREATE_PROCESS 3
#define ERROR_PARSER_CANNOT_CREATE_PROCESS2 4


// 预处理
int Preprocess( CString strFileName ) 
{
	SECURITY_ATTRIBUTES   sa;   
	sa.nLength   =   sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor   =   NULL;   
	sa.bInheritHandle   =   TRUE; 

	HANDLE   hOutFile=CreateFile(PREPROCESSED_FILE, GENERIC_WRITE, FILE_SHARE_WRITE,  &sa,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hOutFile==INVALID_HANDLE_VALUE){
		return ERROR_PREPROCESS_CANNOT_CREATE_FILE;
	}

	STARTUPINFO  si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb=sizeof(si);
	si.dwFlags   =   STARTF_USESHOWWINDOW   |   STARTF_USESTDHANDLES;   
	si.wShowWindow   =   SW_HIDE;   

	ZeroMemory( &pi, sizeof(pi) );
	si.hStdOutput   =   hOutFile;   
	
	SetCurrentDir();

	CString t(_T("cpp0.exe -nostdinc "));
	
	if (gcc_param.GetLength()>0)
		t += gcc_param;

	for (vector<CString>::iterator ite = m_file_include.begin();ite != m_file_include.end();ite++)
	{
		t += " -I \"" + (*ite) + "\"";	
	}
	t += " \"" + strFileName + "\"";

	TCHAR cmd[MAX_PATH*2];
	strcpy_s(cmd, t);

	if(!CreateProcess(NULL,  cmd,     NULL,   NULL,   TRUE,   0,   NULL,   NULL,   &si,   &pi))  
	{   
		return ERROR_PREPROCESS_CANNOT_CREATE_PROCESS;   
	}   
	int i=GetLastError();
	WaitForSingleObject(pi.hProcess,  INFINITE);

	int size = GetFileSize(hOutFile,NULL);
	CloseHandle(hOutFile);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	return 0;
}

// 语法分析,输出文件AST_FILE
BOOL Parse(CString strFileName)
{
	STARTUPINFO  si;
	PROCESS_INFORMATION pi;

	// analyse
    int status = Preprocess(strFileName);
	if (status) return status;
	
	//对中间文件处理，分析struct_name, typedef_name
	ZeroMemory( &si, sizeof(si) );
	si.cb=sizeof(si);   
	si.dwFlags   =   STARTF_USESHOWWINDOW ;
	si.wShowWindow   =   SW_HIDE;
	ZeroMemory( &pi, sizeof(pi) );
	
	TCHAR cmd[MAX_PATH*2];
	wsprintf(cmd,_T("preprocessing_c_plus.exe \"%s\" \"%s\" \"%s\" \"%s\""),
		PREPROCESSED_FILE,
		STRUCT_NAME_FILE,
		TYPE_NAME_FILE,
		BUG_DETECT_IMIDIATE_FILE);

	if (!CreateProcess(NULL, cmd, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
	{
		return ERROR_PARSER_CANNOT_CREATE_PROCESS;
	}

	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	//对中间文件处理，寻找缺陷
	ZeroMemory( &si, sizeof(si) );
	si.cb=sizeof(si);
	si.dwFlags   =   STARTF_USESHOWWINDOW ;
	si.wShowWindow   =   SW_HIDE;
	ZeroMemory( &pi, sizeof(pi) );
	
	wsprintf(cmd, _T("parser.exe \"%s\" \"%s\" \"%s\""), BUG_DETECT_IMIDIATE_FILE, AST_FILE,TYPE_NAME_FILE);
	if (!CreateProcess(NULL,cmd, NULL, NULL, FALSE,  0, NULL, NULL, &si, &pi))
	{
		return ERROR_PARSER_CANNOT_CREATE_PROCESS2;
	}

	WaitForSingleObject(pi.hProcess,  INFINITE);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	return 0;
}

BOOL FindBug(THREAD_INFO *ti)
{
	delete_files();
		
	if ((!ti->pMainFrame->m_bBeginFindBugFolderThread) && (!ti->pMainFrame->m_bBeginFindBugThread)) // 取消?
	{
		ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("已取消检测"+ti->strFile);
		return TRUE;
	}

	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("分析 " + ti->strFile);

    int status = Parse(ti->strFile);
	if (status)
	{
        char buf[10];
		ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(ti->strFile+" 语法分析失败。状态码:"+itoa(status,buf,10));
		return FALSE;
	}
	vector<treeNode> m_nodes;
	LoadAST(m_nodes);
	if (m_nodes.size()==0) 
	{
		ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(ti->strFile+" 语法分析失败。状态:加载AST失败");
		return FALSE;
	}

	for (int i=0;i<sizeof(routine)/sizeof(Routine);i++)
	{
		Add_file_path(routine[i].filename, ti->strFile);
		routine[i].routine(&m_nodes[0]);
		process_bug_file(routine[i].filename);

		//输出
		CStdioFile file(routine[i].filename, CFile::modeRead);
		CString strLine;
		CString current_c_file_path;

		while(file.ReadString(strLine))
		{
			if(strLine.Left(1) == _T("*"))	//C文件路径
			{
				current_c_file_path = strLine.Mid(1,strLine.GetLength()-1);
			}
			else //缺陷
			{
				int line = atoi(strLine.Left(strLine.Find(' ')));
				
				CString t = strLine.Mid(strLine.Find(' '))+" "+routine[i].label;
				ti->pMainFrame->m_buglist.add(line, ti->strFile, i,t);

				CString log = current_c_file_path+"("+strLine.Left(strLine.Find(' ')) + "): " + t;
				// 日志
				ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(log);
				
				ti->pMainFrame->m_wndOutput.m_wndOutputBuild.SetItemData(ti->pMainFrame->m_wndOutput.m_wndOutputBuild.GetCount()-1,ti->pMainFrame-> m_buglist.size());

				// 检测结果
				ti->pMainFrame->m_wndOutput.m_wndOutputDebug.AddString(log);	
				ti->pMainFrame->m_wndOutput.m_wndOutputDebug.SetItemData(ti->pMainFrame->m_wndOutput.m_wndOutputDebug.GetCount()-1,ti->pMainFrame-> m_buglist.size());

				//Polaris-20140830
				if(i==0)
				{
					count_ideoper++;//幂等缺陷
				}
				else if(i==1)
				{
					count_reduassign++;//冗余赋值
				}
				else if(i==2)
				{
					count_deadcode++;//死代码
				}
				else if(i==3)
				{
					count_reducond++;//冗余条件表达式
				}
				else
				{
					count_hideoper++;//隐式幂等表达式
				}

				string key;
				key = (char*)(LPCTSTR)(ti->strFile);

				if(rc_file_map.count(key) == 0)//集合中不存在当前文件目录
				{
					rc_file_map.insert(pair<string,int>(key ,1));

					map<int,int> temp_map;
					temp_map.insert(pair<int,int>(i,1));

					for(int j=0;j<5;j++)
					{
						if(j != i)
						{
							temp_map.insert(pair<int,int>(j,0));
						}
					}

					rc_type_file_map.insert(make_pair(key,temp_map));
				}
				else
				{
					int pre_count = rc_file_map[key];

					//map<string , int>::iterator itr;
					//itr = rc_file_map.find(key);
					
					int new_count = pre_count + 1;
					rc_file_map[key] = new_count;


					int pre_count2 = rc_type_file_map[key][i];
					int new_count2 = pre_count2 + 1;
					rc_type_file_map[key][i] = new_count2;
				}
			}
		}
	}

	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(ti->strFile+" 分析完毕");
	
	return TRUE;
}

void FindBugFolder(THREAD_INFO *ti)
{
	CFileFind ff;

	if (!ti->pMainFrame->m_bBeginFindBugFolderThread) // 取消?
		return;

	BOOL b = ff.FindFile(ti->strFile +_T("\\*.*"));
	while (b)
	{
		b = ff.FindNextFile();
		if (!ff.IsDots())
		{
			if (ff.IsDirectory()) //目录?
			{
				ti->strFile = ff.GetFilePath();
				FindBugFolder(ti);
			}
			else
			{
				extern CString GetSuffix(CString strFileName) ;
				CString ext = GetSuffix(ff.GetFileName());

				if (ext=="c")
				{
					ti->strFile = ff.GetFilePath();
					FindBug(ti);
					ti->pMainFrame->m_wndProgressBar.StepIt();
					ti->pMainFrame->m_wndOutput.m_wndOutputBuild.SetTopIndex(ti->pMainFrame->m_wndOutput.m_wndOutputBuild.GetCount()-1);
				}
			}
		}
	}
	ff.Close();
}


// 检测文件夹线程
UINT FindBugFolderThread(LPVOID lpParam)//LPVOID是一个没有类型的指针，也就是说你可以将任意类型的指针赋值给LPVOID类型的变量（一般作为参数传递），然后在使用的时候再转换回来。
{
	THREAD_INFO* ti = (THREAD_INFO*) lpParam;
	ASSERT(ti);
	//ti->pMainFrame->m_buglist.clearAll();
	get_filenames();
	delete_files();

	ti->pMainFrame->m_wndProgressBar.SetRange32(0,GetFileCount(ti->pMainFrame->m_wndFileView.m_wndFileView.m_strRootDir));
	ti->pMainFrame->m_wndProgressBar.ShowWindow(SW_SHOW);
	
	DWORD dwStart = GetTickCount();
	FindBugFolder(ti);
	DWORD dwEnd = GetTickCount();
	
	ti->pMainFrame->m_wndProgressBar.ShowWindow(SW_HIDE);
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("分析完毕。");
	
	ti->pMainFrame->m_bBeginFindBugFolderThread = FALSE;

	//刷新错误显示
	//for (int i=0;i<ti->pMainFrame->m_edit_view.size();i++){		
	//	ti->pMainFrame->m_edit_view[i]->RefreshAnnotations();
	//}

	CString t;
	t.Format("耗时: %dms.",dwEnd - dwStart);
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(t);

	ti->pMainFrame->m_buglist.elapsed_time = dwEnd - dwStart;

	//Polaris-20140830
	//CString temp;
	//temp.Format("%d---%d---%d---%d---%d",count_ideoper,count_reduassign,count_deadcode,count_reducond,count_hideoper);
	//AfxMessageBox(temp);

	/*for (map<string, int>::iterator itr=rc_file_map.begin(); itr!=rc_file_map.end(); itr++)  
    {  
		temp += (itr->first).c_str();
		temp += "个数：";
		char s[256];
		itoa(itr->second,s,10);
		temp += s;
		temp += "---";
    }  
	AfxMessageBox(temp);*/

	 /*string temp = "";
	 map<string,map<int,int> >::iterator multitr;  // 以下是如何遍历本multiMap
     map<int,int>::iterator intertr;
     for(multitr=rc_type_file_map.begin();multitr!=rc_type_file_map.end();multitr++)
     {
		 temp += multitr->first + "：";
         for(intertr= multitr ->second.begin(); intertr != multitr ->second.end(); intertr ++)
		 {
			 temp += routine[intertr->first].label;
			 char s[256];
			 itoa(intertr->second,s,10);
			 temp += s;
		 }
		 temp += "\n";
     } 
	 AfxMessageBox(temp.c_str());*/

	// 显示报告
	ti->pMainFrame->OnOutputReport();

	return 0;
}

// 检测文件线程
UINT FindBugThread(LPVOID lpParam)
{
	THREAD_INFO* ti = (THREAD_INFO*) lpParam;
	ASSERT(ti);
	//ti->pMainFrame->m_buglist.clear(ti->strFile);
	get_filenames();
	delete_files();

	DWORD dwStart = GetTickCount();
	FindBug(ti);
	DWORD dwEnd = GetTickCount();

	ti->pMainFrame->m_bBeginFindBugThread = FALSE;
	////刷新错误显示
	//for (int i=0;i<ti->pMainFrame->m_edit_view.size();i++){
	//	ti->pMainFrame->m_edit_view[i]->RefreshAnnotations();
	//}

	CString t;
	t.Format("耗时: %dms.",dwEnd - dwStart);
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(t);

	ti->pMainFrame->m_buglist.elapsed_time = dwEnd - dwStart;

	//显示报告
	ti->pMainFrame->OnOutputReport();

	return 0;
}

int GetFileCount(CString strDir) {
	CFileFind ff;
	int c=0;

	BOOL b = ff.FindFile(strDir+_T("\\*.*"));
	while (b)
	{
		b = ff.FindNextFile();
		if (!ff.IsDots())
		{
			if (ff.IsDirectory()) //目录?
			{
				c += GetFileCount(ff.GetFilePath());
			}
			else
			{
				CString GetSuffix(CString strFileName) ;
				CString ext = GetSuffix(ff.GetFileName());

				if (ext=="c")
				{
					c++;
				}
				
			}
		}
	}
	ff.Close();
	
	return c;
}

void _GetAST(THREAD_INFO* ti)
{
	vector<treeNode> m_nodes;
	// 开始
    get_filenames();
    delete_files();
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("正在语法分析...");	
    int status = Parse(ti->strFile);
	if (status)
	{
        char buf[10];
		ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString(ti->strFile+" 语法分析失败。状态码:"+itoa(status,buf,10));
		return ;
	}
	ti->pMainFrame->m_wndProgressBar.StepIt();
	
	// 从文件加载AST
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("正在加载AST...");
	LoadAST(m_nodes);
	ti->pMainFrame->m_wndProgressBar.StepIt();
	
	if (m_nodes.size()==0)
	{
		ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("语法分析失败");	
		return;
	}

	// 加载到树控件
	ti->pMainFrame->LoadAST(&m_nodes[0]);
	ti->pMainFrame->m_wndProgressBar.StepIt();

	// 生成图形
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("正在生成图形...");
	if (!GetASTGraph(m_nodes))
	{
		ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("生成图形失败");	
		return;
	}
	ti->pMainFrame->m_wndProgressBar.StepIt();
}

UINT GetASTThread(LPVOID lpParam)
{
	THREAD_INFO* ti = (THREAD_INFO*) lpParam;
	ti->pMainFrame->m_wndProgressBar.SetRange(0, 4);
	ti->pMainFrame->m_wndProgressBar.ShowWindow(SW_SHOW);
	
	_GetAST(ti);
	
	// 完成
	ti->pMainFrame->m_wndOutput.m_wndOutputBuild.AddString("操作完成。");
	ti->pMainFrame->m_wndProgressBar.ShowWindow(SW_HIDE);

	ti->pMainFrame->m_bBeginFindBugThread = FALSE;

	return 0;
}

// 加载预处理包含目录
void LoadFileInclude()
{
	m_file_include.clear();

	CWinApp * app = AfxGetApp();
	int c = app->GetProfileInt("dir","d",0);
	for (int i=0;i<c;i++)
	{
		CString t;
		TCHAR tt[20];
		wsprintf(tt,"d%d",i);
		t = app->GetProfileString("dir",tt);
		m_file_include.push_back(t);
	}
}

// 加载自定义预处理参数
void LoadCustomParam()
{
	CWinApp * app = AfxGetApp();
	gcc_param = app->GetProfileString("gcc","param");
}

void SetCurrentDir()
{
	CString    sPath;
	GetModuleFileName(NULL,sPath.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
	sPath.ReleaseBuffer();
	int    nPos;
	nPos=sPath.ReverseFind('\\');
	sPath=sPath.Left(nPos);
	SetCurrentDirectory(sPath);
}