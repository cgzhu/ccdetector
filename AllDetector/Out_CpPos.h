// Out_CpPos.h: interface for the Out_CpPos class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OUT_CPPOS_H__296DE9C8_403E_471C_A47E_68BC9302E07D__INCLUDED_)
#define AFX_OUT_CPPOS_H__296DE9C8_403E_471C_A47E_68BC9302E07D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"
#include "CP_Segment.h"

#include <iostream>
using namespace std;

class Out_CpPos  
{
public:
	CArray <CP_Segment, CP_Segment> CP_Seg_Array;		//CP_Segment:记录重复代码信息的类
	CArray <Con_SEG, Con_SEG> Con_SEG_Array1;//记录两个CP_Segment中顺序相连的碎片信息		
	CArray <Con_SEG, Con_SEG> Con_SEG_Array2;//记录两个CP_Segment中逆序相连的碎片信息
	CArray <CP_SEG*, CP_SEG*> CP_Useless_Frag;
	CArray <CP_POS, CP_POS> CP_POS_Array[1000];			//CP_POS:记录每行代码是否为重复代码的节点
	CString CP_OUT_PATH;//wq-20090204-记录CP_OUT.txt的路径
	CString out_file_path;//wq-20090402
	int m_cpLineSum;
	int Min_len;//wq-20090310
public:
	bool RebuildFormFile(CString fileName);//wq-20090210
	void Clean();
	Out_CpPos();
	virtual ~Out_CpPos();
	void Out_CpCodePos(class Seq_Mining &Rsm);
private:
	void Sort_CPS();    
	//按照从大到小的顺序为所有的碎片排序（选择排序法）

	bool Find_Neighbor_Frag(int F1, int F2,const CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G);
	//找出CP_Seg_Array[F1]与CP_Seg_Array[F2]之间有没有相邻的碎片可以合并；将可合并的碎片信息存于Con_SEG_Array1和Con_SEG_Array2中
	void Generate_Hvalue();
	//为所有的碎片生成与filname对应的散列值
	long Filename_to_Hvalue(const CString filename);
	//返回filname对应的散列值
	int Is_Consecutive(CP_SEG &S1,CP_SEG &S2);
	//察看碎片S1，S2是否能够合并：若不能则返回0；否则，若S1在S2前则返回1，S2在S1前则返回-1

	void Combine_Frag(int F1, int F2);
	//合并两段代码，无返回值
	CP_Segment Make_Larger_Frag(int F1, int F2, CArray <Con_SEG,Con_SEG> &Con_SEG_Array);
	//如果可合并的碎片对数大于等于2，则将CPS1和CPS2中相连的碎片合成新的碎片，加入数组CP_Seg_Array的末尾
	void Del_Orig_Frag(int Fi, bool Is_first);
	//删除合并前碎片中的相关信息

	void Del_Useless_inf(Seq_Mining &Rsm);
	//删除无用的信息，合并一些重复的信息
	bool Del_Redun_inf(int F1, int F2,const CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G);
	//删除碎片中重叠的部分的信息
    

	void Count_Line();  
	//统计重复代码的行数

//	void Out_DupCode(Seq_Mining &Rsm);//将长度大于Min_len的重复代码源码输出到文件夹"D:\\DupCode\\"中

	void Test();
	//测试函数
	void Out_CPSeg(int step);
	//输出每步的合并结果，用于测试！

	void DelReverseItem(CP_Segment &CP_Seg);//wq-20081212-去除源程序前后位置颠倒的项
	void DelItem(CP_Segment &CP_Seg,int j);//wq-20081215-被DelReverseItem调用

	void ChangeCpArrayToPairs();//wq-20081227-add-将片段数量大于3的重复代码片段组中的重复代码片段两两组合成新的重复代码片段组(每个含有两个片段)
	
	void DelDupLocationItem();//wq-20081227-去除行数重复的项的信息，只保留第一次出现的位置

	/*wq-20090210*/
	bool GetSeqFromStrLine(const CString& strLine,CP_Segment& cpSegment);
	bool GetSupFromStrLine(const CString& strLine,CP_Segment& cpSegment);
	bool GetFilePathFromStrLine(const CString& strLine,CP_SEG& cpSeg);
	bool GetHashedNameFromStrLine(const CString& strLine,CP_SEG& cpSeg);
	bool GetLinesFromStrLine(const CString& strLine,CP_SEG& cpSeg);
	bool GetRelaLinsFromStrLine(const CString& strLine,CP_SEG& cpSeg);
	int FindNumberFromString(const CString& str);
	/***************************************************/
public:
	double ComputeSeqDensity(const CString &relaLPOS,const CString &LPOS,const CString &filename);
	void UpdateIdsLocArrays(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry);//wq-20090223
	void Out_LCP_Pos();
	//将长度大于Min_len的重复代码信息写入文本文件"D:\\CP_OUT.txt"中

};

#endif // !defined(AFX_OUT_CPPOS_H__296DE9C8_403E_471C_A47E_68BC9302E07D__INCLUDED_)
