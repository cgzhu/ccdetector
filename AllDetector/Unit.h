// Unit.h: interface for the CUnit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UNIT_H__C69DF462_9284_45A5_B909_508B528065E1__INCLUDED_)
#define AFX_UNIT_H__C69DF462_9284_45A5_B909_508B528065E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"
#include "AssignmentUnit.h"
#include "IterationUnit.h"
#include "SelectionUnit.h"

class CVarRemove;

class CUnit  
{
public:
	CUnit();
	virtual ~CUnit();
public:
	void operator()(CSDGBase *pSDGHead,CArray<SNODE,SNODE> &SArry);
	void PreOrderTraverse(CSDGBase *pHead);
protected:
    CSDGBase *m_pSDGHead;
protected:
	// Function group: Translate the SDG to Unit representation(USDG)
	void TransSDGToUnit(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	CSDGBase *CopyNode(CSDGBase *pNode,CSDGBase *pFather,CArray<SNODE,SNODE> &SArry);
	CSDGBase *CopyNodeEx(CSDGBase *pNode,CSDGBase *pFather);
	void SetSelector(CSDGBase* pSelector,CSDGBase * pSelection,CArray<SNODE,SNODE> &SArry);
	void SetElseSelector(CArray<SNODE,SNODE> &SArry);
	void SetItrBranch(CArray<SNODE,SNODE> &SArry);
	void AddItrSub(CSDGBase *pItr);
	void AddEspSub(CSDGBase *pItr);
public:
	CSDGBase *CopyBranch(CSDGBase *pInt);

	// End of this Function Group
protected:
	void SetDataFlow();
	void ReferencedDataFlow();
	void DefineDataFlow();
	void SetBoolExpToElseExp(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
//	void GetNotOfBoolExp(ENODE *ptr);
//	void NotAndReverse(ENODE *ptr);

	void CaseBreak(CSDGBase *pHead);
	void RemoveCaseBreak(CSDGBase *pHead);
	void InitialENODE(ENODE *pENODE);

};

CSDGBase *CopyBranchEx(CSDGBase *pint);

#endif // !defined(AFX_UNIT_H__C69DF462_9284_45A5_B909_508B528065E1__INCLUDED_)
