// SDGBreak.h: interface for the CSDGBreak class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGBREAK_H__13370F5C_B20C_4D57_9B0E_5DD779236080__INCLUDED_)
#define AFX_SDGBREAK_H__13370F5C_B20C_4D57_9B0E_5DD779236080__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGBreak : public CSDGBase
{
public:
	CSDGBreak();
	virtual ~CSDGBreak();
public:
	int BPOS;
	CSDGBase *pTO;

};

#endif // !defined(AFX_SDGBREAK_H__13370F5C_B20C_4D57_9B0E_5DD779236080__INCLUDED_)
