// SDGBase.h: interface for the CSDGBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGBASE_H__98D4E55D_3C62_454C_91C8_AC46A2CC6EAC__INCLUDED_)
#define AFX_SDGBASE_H__98D4E55D_3C62_454C_91C8_AC46A2CC6EAC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Lexical.h"

class CSDGBase
{
public:
	CSDGBase();
	virtual ~CSDGBase();
protected:

	// node type, labeling the statement type.
	int m_iNodeID;
     
	// node value,indicating the value of the node especially when it is "if" or "switch".
	int m_iNodeVL;

	// relation types array 
	CArray<int,int> m_aRArry;

	// relation nodes array
	CArray<CSDGBase*, CSDGBase*> m_pRNodes; // 控制流(相关)节点
	CSDGBase *pFather;
public:
	// the location of this node.
	int idno; 
	int visit;
	int mvisit;
	int bodybeg;
	int bodyend;
	int important;
	bool b_formal;//是否是用实参给形参赋值的节点
//	bool b_return;//是否是接受返回值的节点
	int no;//是参数声明(>=0)还是普通变量(-1)
	ENODE *m_pinfo;
	CSDGBase *pCorrspS;
	CSDGBase *pCorrspM;
	CArray<TNODE,TNODE> TCnt;
	CArray<TNODE,TNODE> m_aRefer;
	CArray<TNODE,TNODE> m_aDefine;
	CString m_sName;
	CString m_sType;
	////////////wtt///3.18/////用于建立数据依赖的数据结构/////
	
	CArray<FlowNODE,FlowNODE>m_aIn ;
	CArray<FlowNODE,FlowNODE>m_aOut;
	CArray<FlowNODE,FlowNODE>m_aGen;
	CArray<FlowNODE,FlowNODE>m_aKill;

	CArray<CSDGBase*,CSDGBase*>m_pDRNodes;//数据依赖边
//	CArray<int,int>m_aDRArry;

	CArray<CSDGBase*,CSDGBase*>m_pAntiDRNodes;//反依赖边
//	CArray<int,int>m_aAntiDRArry;

	CArray<CSDGBase*,CSDGBase*>m_pDecRNodes;//声明依赖边


//	CArray<int,int>m_aDecRArry;

	////////////wtt///3.18////

	CArray<ALIAS*,ALIAS*> ptSet;//wtt 2008.3.19 指针指向集合
public:
	// operations of this node
////////////////wtt
	/*void SetFather(CSDGBase * pF)
	{
		pFather=pF;
	}

	CSDGBase *GetFather()
	{
		return pFather;
	}
	void SetNodeID(const int NID)
	{
		m_iNodeID=NID;
	}

	void SetNodeVL(const int NVL)
	{
		m_iNodeVL=NVL;
	}

    int GetNodeID()
	{
		return m_iNodeID;
	}

	int GetNodeVL()
	{
		return m_iNodeVL;
	}

	int GetRelateNum()
	{
		return m_aRArry.GetSize();
	}

	*/
    void SetFather(CSDGBase * pF);
	CSDGBase *GetFather();
	void SetNodeID(const int NID);
	void SetNodeVL(const int NVL);
    int GetNodeID();
    int GetNodeVL();
	int GetRelateNum();
	void Clear();

	void AddDataFlow(CSDGBase *p);

void AddAntiDataFlow(CSDGBase *p);
void AddDecFlow(CSDGBase *p);


/////////////////wtt bring the function definition to SDGBASE.cpp


	void Remove(const int idx);
	void RemoveAll();
	
	// operations of child node.
	CSDGBase *GetNode(const int idx);
	int       GetRelation(const int idx);
	bool      Add(const int RID,CSDGBase *pNode);
	bool      SetNode(int idx,CSDGBase *ptr);
	bool      InsertNode(int idx,CSDGBase *ptr);
    bool      Del(const int idx);
	bool      FactorOf(const int currpos);
	void      SetNodeCM(const int idx,CSDGBase *ptr);

};

#endif // !defined(AFX_SDGBASE_H__98D4E55D_3C62_454C_91C8_AC46A2CC6EAC__INCLUDED_)
