#if !defined(AFX_CPFILESLISTVIEW_H__A544EE36_9F78_4799_BF81_FD39FDFC9D74__INCLUDED_)
#define AFX_CPFILESLISTVIEW_H__A544EE36_9F78_4799_BF81_FD39FDFC9D74__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CPFilesListView.h : header file
//
#include "Afxcview.h"
#include "Out_CpPos.h"

/////////////////////////////////////////////////////////////////////////////
// CCPFilesListView view

class CCPFilesListView : public CTreeView
{
protected:
	CCPFilesListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCPFilesListView)

// Attributes
public:
	int Min_len;//wq-20090310

// Operations
public:
	CString GetFuncSourceCode(int cpGroup,int cpSeg);
	void DeleteChildItems(HTREEITEM &m_hCTree);
	CString FormViewLine(CString strLine,int lineNum,int cpLine);
	bool Find_Code_Line( int curpos , int &cpline, CString &linePos);
	CString GetSourceCode(CString filePath,CString linePos);
	void FillTree(Out_CpPos *Ocp,CImageList &imgList,HTREEITEM &m_hCTree);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCPFilesListView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCPFilesListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CCPFilesListView)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPFILESLISTVIEW_H__A544EE36_9F78_4799_BF81_FD39FDFC9D74__INCLUDED_)
