// FuncBndBuild.h: interface for the FuncBndBuild class.
// 作者：王倩
// 时间：2008-12-20
// 功能：根据TArry，为每个token字添加所属函数的源程序范围
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FUNCBNDBUILD_H__E9CAF922_856D_4ECD_A230_C682AB03BDBA__INCLUDED_)
#define AFX_FUNCBNDBUILD_H__E9CAF922_856D_4ECD_A230_C682AB03BDBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "datastruct.h"
#include "constdata.h"

class FuncBndBuild  
{
public:
	/*
	从位置idx开始查找TArry种的一个函数的开始和结束行数
	TArry：	token流
	readyTknIdxArry：依次存储属于本次查找函数所有token在TArry中的位置
	funcBegLine：	函数开始的相对行数（token流经换行标准化后的行数）
	funcEndLine：	函数结束的相对行数（token流经换行标准化后的行数）
	funcBgnRealLine：函数开始行在源程序种的实际行数
	funcEndRealLine：函数结束行在源程序种的实际行数
	idx：	从idx开始搜索TArry，并记录结束时的下一个位置
	*/
	FuncBndBuild();
	virtual ~FuncBndBuild();
	void operator()(CArray<TNODE,TNODE> &TArry);
private:
	void FindFuncRang(CArray<TNODE,TNODE>& TArry, CArray<int,int> &readyTknIdxArry, int& funcBegLine, int& funcEndLine,int& funcBgnRealLine,int& funcEndRealLine, long& idx);

	/*
	修改TArry种对应存储在readyTknIdxArry中所有位置的token的函数范围
	TArry:		依次存储属于本次查找函数所有token在TArry中的位置
	funcBegLine:函数开始的相对行数（token流经换行标准化后的行数）
	funcEndLine:函数结束的相对行数（token流经换行标准化后的行数）
	funcBgnRealLine:函数开始行在源程序中的实际行数
	funcEndRealLine:函数结束行在源程序中的实际行数
	*/
	void ChangeFuncRang( CArray<TNODE,TNODE>& TArry, CArray<int,int> &readyTknIdxArry, int& funcBegLine, int& funcEndLine,int& funcBgnRealLine,int& funcEndRealLine );

	/*
	建立函数内部每个token的上下文信息
	TArry:	token流
	readyTknIdxArry:依次存储属于本次查找函数所有token在TArry中的位置
	funcBegLine:	函数开始的相对行数（token流经换行标准化后的行数）
	funcEndLine:	函数结束的相对行数（token流经换行标准化后的行数）
	*/
	void BuildContxtInfo( CArray<TNODE,TNODE>& TArry,CArray<int,int> &readyTknIdxArry,int funcBegLine, int funcEndLine );//建立函数内部每个token的上下文信息

};

#endif // !defined(AFX_FUNCBNDBUILD_H__E9CAF922_856D_4ECD_A230_C682AB03BDBA__INCLUDED_)
