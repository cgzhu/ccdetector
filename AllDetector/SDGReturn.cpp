// SDGReturn.cpp: implementation of the CSDGReturn class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGReturn.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSDGReturn::CSDGReturn()
{
	m_sType="SDGRETURN";
	m_iValue=0;
	visit=0;

}

CSDGReturn::~CSDGReturn()
{

}
