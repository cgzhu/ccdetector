// RCBugFormDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "RCBugFormDlg.h"
#include "afxdialogex.h"


// CRCBugFormDlg 对话框

IMPLEMENT_DYNAMIC(CRCBugFormDlg, CDialogEx)

CRCBugFormDlg::CRCBugFormDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRCBugFormDlg::IDD, pParent)
{

}

CRCBugFormDlg::~CRCBugFormDlg()
{
}

void CRCBugFormDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_rc_bug_list);
}


BEGIN_MESSAGE_MAP(CRCBugFormDlg, CDialogEx)
END_MESSAGE_MAP()


// CRCBugFormDlg 消息处理程序
