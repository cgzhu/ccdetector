/********************************************************************
	created:	2011/02/09
	created:	9:2:2011   18:44
	filename: 	D:\BugFinder\BugFinder\routine.h
	file path:	D:\BugFinder\BugFinder
	file base:	routine
	file ext:	h
	author:		qj
	
	purpose:	检测函数
*********************************************************************/
#include <map>
#pragma once
typedef struct {
	CMainFrame *pMainFrame;
	CString strFile;
} THREAD_INFO;

typedef void (*find_bug_routine)(treeNode*);
typedef struct {
	find_bug_routine routine;
	TCHAR label[128];
	TCHAR *filename;
} Routine;

void LoadAST(vector<treeNode>& m_nodes); // 从AST_FILE加载AST
void Add_file_path(const char *writed_file,const char* filename);
BOOL FindBug(THREAD_INFO *ti); // 检测文件
void FindBugFolder(THREAD_INFO *ti); // 检测目录
UINT FindBugFolderThread(LPVOID lpParam);// 检测目录线程
UINT FindBugThread(LPVOID lpParam);// 检测文件线程
int GetFileCount(CString strDir);
BOOL GetAST(LPCTSTR filename); // 获取AST
BOOL GetASTGraph(vector<treeNode>& m_nodes);
UINT GetASTThread(LPVOID lpParam);
BOOL Preprocess( CString strFileName ) ;
void LoadFileInclude(); // 加载预处理目录
void SetCurrentDir();

extern int count_ideoper;
extern int count_reduassign;
extern int count_deadcode;
extern int count_reducond;
extern int count_hideoper;

extern map<string,int> rc_file_map;
extern map<string,map<int,int>> rc_type_file_map;