// Expression.cpp: implementation of the CExpression class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Expression.h"

/*#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/
// Expression.cpp: implementation of the CExpression class.
//
//////////////////////////////////////////////////////////////////////


#include "SDGAssignment.h"
#include "SDGCall.h"
#include "SDGReturn.h"

//#define TEST
//#define DEBUG
#define MAX 9999
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern CString GetENodeStr(ENODE *p,CArray<SNODE,SNODE> &SArry);
//extern CString PrintExp(CArray<TNODE,TNODE> &T);
CExpression::CExpression()
{

}

CExpression::~CExpression()
{

}

 void CExpression::CopyTNODE(TNODE &TNew,TNODE &TInt)
{
	/*****************************************************

	             copy TInt to TNew
	
	*****************************************************/

	TNew.addr=TInt.addr;
	TNew.deref=TInt.deref;
	TNew.key=TInt.key;
	TNew.name=TInt.name;
	TNew.line=TInt.line;  
	TNew.value=TInt.value;

}

inline void CExpression::CopyTNODE(TNODE *pNew,TNODE *pInt)
{
	/*****************************************************

	             copy TInt to TNew
	
	*****************************************************/
    if(!pNew || !pInt)
	{
		return;
	}

	pNew->addr=pInt->addr;
	pNew->key=pInt->key;
	pNew->name=pInt->name;
	pNew->line=pInt->line;  
	pNew->value=pInt->value;
	pNew->deref=pInt->deref;//wtt 2008.3.18


}

void CExpression::operator ()(CSDGBase *pSDGHead,
							  CArray<SNODE,SNODE> &SArry,
							  CArray<CERROR,CERROR> &EArry)
{
	
/******************************************************

	    FUNCTION:
		 Template function, interface of class CExpression,
		Its function is to translate the expression in list
		to syntax tree.
	    
		PARAMETER:
		pSDGHead: Head of SDG.
	******************************************************/
	if(SArry.GetSize()<=0)
		return ;

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
    p=pSDGHead;
	m_pError=&EArry;
   
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())	//GetRelateNum:return m_aRArry.GetSize();
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit the node and execute the certain action

			if(p->visit==p->GetRelateNum())
			{
				// the following codes will visit the node   and 
				// translate the expression in it to syntax tree 
				int i;
				if( p->m_sType=="SDGASSIGNMENT")
				{
					CSDGAssignment *pas=NULL;
					pas=(CSDGAssignment *)p;
				//	AfxMessageBox("ok");////
				//	AfxMessageBox(PrintExp(pas->TCnt) );////
				//	AfxMessageBox(PrintExp(pas->TEXP));////
					int rvalue;
					///////////////wtt/////3.3///////////////
				
					for( i=0;i<pas->TEXP.GetSize();i++)
					{
						if(BOperator(pas->TEXP[i])!=0)
						{
						
							i=pas->TEXP.GetSize()+2;
							break;
						}

					}
					if(i==pas->TEXP.GetSize()+2)
					{
					//	AfxMessageBox(PrintExp(pas->TCnt) );////
					//	AfxMessageBox(PrintExp(pas->TEXP));////
						BExpToTree(pas->TEXP,p,SArry);
						
						BoolExpStandard(p->m_pinfo,SArry);
					//	BExpStandardRule01(p);
					}
					else
					{
						rvalue=GetAExpInfo(pas->TEXP);
							//////////wtt 2007.5.28//////////////强制类型转换//////////////////
						if(rvalue>0)
						{
						//	AfxMessageBox("强制类型转");////////
							TNODE T,T1;
							T1.key=CN_ADD;
							pas->TEXP.InsertAt(0,T1);
							T.addr=rvalue;
							T.deref=-1;
							T.key=CN_CINT;
							T.name="cast";
							T.paddr =-1;
							T.value= rvalue;
							pas->TEXP.InsertAt(0,T);
												
						//	pas->m_pinfo->info= -(rvalue-BOUND);
						//	CString strval;
						//	strval.Format(C_ALL_KEYWORD[-pas->expbeg]);
						//	AfxMessageBox(strval);////////
						}

						//////////////////////////////////////////
				
						AExpToTree(pas->TEXP,p,SArry);
					
					}
					//////////////////wtt/////////////3.3/////////

					CSDGBase asTemp;
					asTemp.TCnt.Copy(pas->TCnt);
				//	AfxMessageBox(PrintExp(asTemp.TCnt) );////


					AExpToTree(asTemp.TCnt,&asTemp,SArry);
					pas->m_pleft=asTemp.m_pinfo;
					/////////////////////////////////
			//		int key=(pas->m_pleft->T).key;
			//	AfxMessageBox(C_ALL_KEYWORD[key]);
			 ////////////////////////////////////////


					asTemp.TCnt.RemoveAll();
					
					if(pas->m_pleft)
					{
						pas->m_pleft->T.value+=rvalue;
					}
					
					pas->TCnt.RemoveAll();
					pas->TEXP.RemoveAll();
										
				}
				else if(p->m_sType=="SDGIF")	//########################################
				{
				/*	CString str;
					for(int count=0; count<p->TCnt.GetSize(); ++count)
					{
						str += p->TCnt[count].srcWord;
						str += " ";
					}
					AfxMessageBox(str);
				*/
					BExpToTree(p->TCnt,p,SArry);	//	CSDGBase *p=NULL; p=pSDGHead;		//CArray<TNODE,TNODE> TCnt;
					BoolExpStandard(p->m_pinfo,SArry);
					p->TCnt.RemoveAll();  					
				}
				else if(p->m_sType=="SDGFOR")
				{
				//	AfxMessageBox(PrintExp(p->TCnt) );////
					BExpToTree(p->TCnt,p,SArry);
					//AfxMessageBox(TraverseAST(p->m_pinfo,SArry));/////
					BoolExpStandard(p->m_pinfo,SArry);
					
				//	BExpStandardRule01(p);
					p->TCnt.RemoveAll();  									
				}
				else if(p->m_sType=="SDGDOWHILE")
				{
					BExpToTree(p->TCnt,p,SArry);
					BoolExpStandard(p->m_pinfo,SArry);
				//	BExpStandardRule01(p);
					p->TCnt.RemoveAll();
				}
				else if(p->m_sType=="SDGWHILE")
				{
					BExpToTree(p->TCnt,p,SArry);
					BoolExpStandard(p->m_pinfo,SArry);
//					BExpStandardRule01(p);
					p->TCnt.RemoveAll();
				}
				else if(p->m_sType=="SDGCALL")
				{
					CallParameter(p->TCnt,p,SArry);
					p->TCnt.RemoveAll();
				}
				else if(p->m_sType=="SDGRETURN")
				{
					if(p->TCnt.GetSize()>0)
					{
						for(int i=0;i<p->TCnt.GetSize();i++)
						{
							if(BOperator(p->TCnt[i])!=0)
							{
						
								i=p->TCnt.GetSize()+2;
								break;
							}

						}
						if(i==p->TCnt.GetSize()+2)
						{
						
							BExpToTree(p->TCnt,p,SArry);
						
							BoolExpStandard(p->m_pinfo,SArry);					
						}
						else
						{
						//	AfxMessageBox("before aexptotree");
							AExpToTree(p->TCnt,p,SArry);
						//		AfxMessageBox("after aexptotree");

						}
						#ifdef DEBUG
//						CFile f("data\\return.txt",CFile::modeCreate|CFile::modeWrite);
//					    extern CString PreOrderPrint(ENODE *pHead,CFile &f);
//					    PreOrderPrint(p->m_pinfo,f);
						#endif
					}
					p->TCnt.RemoveAll();
				}
				// end of this action

				S.RemoveTail();
			}
			// end

			// reset the variable---visit;
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	/********************end of this function********************/
}

int  CExpression::GetAExpInfo(CArray<TNODE,TNODE> &TEXP)
{
	/*************************************************************
	  FUNCTION://将强制类型转换语句删除
	  const type translate, delete them from the arithmetic expression
	  and return it type;
	  PARAMETERS:
	  TEXP  : bool expression;	  
	*************************************************************/
	int rvalue=0;

	if(TEXP.GetSize()<4)
	{
		return rvalue;
	}
	
	if(TEXP[0].key==CN_LCIRCLE && TEXP[2].key==CN_RCIRCLE && (TEXP[1].key==CN_INT ||
	   TEXP[1].key==CN_CHAR || TEXP[1].key==CN_DOUBLE || TEXP[1].key==CN_FLOAT))
	{
		rvalue=TEXP[1].key+BOUND;
		TEXP.RemoveAt(2);
		TEXP.RemoveAt(1);
		TEXP.RemoveAt(0);
	}

	int i=TEXP.GetSize()-1; 
	while(i>0)
	{
		if(i>=2 && TEXP[i-2].key==CN_LCIRCLE && TEXP[i].key==CN_RCIRCLE && (TEXP[i-1].key==CN_INT ||
	       TEXP[i-1].key==CN_CHAR || TEXP[i-1].key==CN_DOUBLE || TEXP[i-1].key==CN_FLOAT))
		{
			TEXP.RemoveAt(i);
			TEXP.RemoveAt(i-1);
			TEXP.RemoveAt(i-2);
			i=i-2;
		}
		i--;
	}

	return rvalue;

}


//@@@@@@@@@@@@@#############$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&***********//

							/******************************
                               the following function group:
							translate  bool  expression  to  
							synax tree.
                            ******************************/

void BoolExpToSTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry)
{
	if(TEXP.GetSize()<=0 || ptr==NULL)
	{
		return;
	}
	else
	{
		CExpression cexp;
		cexp.BExpToTree(TEXP,ptr,SArry);
	}
}

void CExpression::BExpToTree (CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry)
{
	/*************************************************************
	  FUNCTION:
	  Analyze the bool expression and translate it to syntax tree.
	  PARAMETERS:
	  TEXP  : bool expression;
	  phead : head pointer of the syntax tree.
	*************************************************************/
	#ifdef DEBUG
//    AfxMessageBox("bool expression:"+PrintExp(TEXP));
	#endif
	
	int icount=0;
	CArray<TNODE,TNODE>   TList[20];
	CArray<ENODE*,ENODE*> EList;
	CSDGBase sdgb;
	    
	ReplaceAExp(icount,TEXP,TList);

	// print
	#ifdef DEBUG
//	CString str,s;
//	str.Format("icount=%d",icount); 
//	AfxMessageBox(str+"bool expression after replace:"+PrintExp(TEXP));
//	str="";
//    for(int j=0;j<TEXP.GetSize();j++)
//	{
//		s.Format("%s(addr=%d,key=%d)\n",TEXP[j].name,TEXP[j].addr,TEXP[j].key );
//		str+=s;
//	}
//	AfxMessageBox(str);
	#endif
	// end

	
	for(int i=0;i<icount;i++)
	{
		#ifdef DEBUG
//		str.Format("Tlist[%d]:",i) ;
//		AfxMessageBox(str+PrintExp(TList[i]));
		#endif

		AExpToTree(TList[i],&sdgb,SArry);
	//	AfxMessageBox(PrintExp(TList[i])+"--"+TraverseAST(sdgb.m_pinfo,SArry));
	/*	if(sdgb.m_pinfo)
			AfxMessageBox(C_ALL_KEYWORD[sdgb.m_pinfo->T.key]);*////
        EList.Add(sdgb.m_pinfo); 
	}
    
	
	BoolExpAnalyze(TEXP,ptr,SArry,EList);
     
	/********************end of this function********************/

}

void CExpression::ReplaceAExp(int &icount,CArray<TNODE,TNODE> &TEXP,CArray<TNODE,TNODE> TList[20])
{	//wtt重新编写了



	/************************************************************
	FUNCTION:
	Replace every arithmetic expression int "TEXP" with a TNODE
	in order to simplify the bool expression
	PARAMETER:
	icount : element number in array TList;
	TEXP   : bool expression
	TLIST[]: stores all the arithmetic expression in TEXP;
	************************************************************/

	int ix=TEXP.GetSize()-1;
//	CList<int,int> BOPPOS;

	
	CArray<TNODE,TNODE> OPERATOR;
	CArray<TNODE,TNODE> TEMP;
	CArray<int,int>INDEX;
	
	for(int i=0;i<TEXP.GetSize();i++)
	{
		TEMP.Add(TEXP[i]);
	}
//	AfxMessageBox("replace expression"+PrintExp(TEMP));//////////////////
	TEXP.RemoveAll();
	
	
	int i;
	for(i=0;i<TEMP.GetSize();i++)
	{
		if(BOperator(TEMP[i])!=0||TEMP[i].key==CN_LCIRCLE||TEMP[i].key==CN_RCIRCLE)
		{
			OPERATOR.Add(TEMP[i]);
			INDEX.Add(i);

		}
	}
	/////////////////////////////////
/*	CString stmp="operator index:",str;
for(i=0;i<INDEX.GetSize();i++)
{
	str.Format("%d",INDEX[i]);
	stmp+=str;
}
AfxMessageBox(stmp);*/
//AfxMessageBox("operator:"+PrintExp(OPERATOR));
/////////////////////////////////////////////////
	i=0;
	if(INDEX.GetSize()>0&&INDEX[i]>0)
	{
			TNODE T;
			T.key=CN_CINT;
			T.addr=icount+1;
			T.name="";
			T.deref=0;//wtt 2008.3.19
			T.line=ix>=0?TEMP[0].line:0; 
			TEXP.Add(T);
			for(int j=0;j<INDEX[0];j++)
			{
			TList[icount].Add(TEMP[j]);
			}
			icount++;
		//	AfxMessageBox("first oprand:"+PrintExp(TList[icount-1]));/////////////

	}
	else if(INDEX.GetSize()==0&&TEMP.GetSize()!=0)
	{
		for(int j=0;j<TEMP.GetSize();j++)
		{
			TList[0].Add(TEMP[j]);
		}
		icount=1;

	}
	
	
	
	while(i<INDEX.GetSize())
	{
	
		if(i<OPERATOR.GetSize()&&OPERATOR[i].key==CN_LCIRCLE)
		{	int nkh=1;
		    int k=i+1;
		//	AfxMessageBox("l circle");//////////
			while(k<OPERATOR.GetSize()&&nkh!=0)
			{
				if(OPERATOR[k].key==CN_LCIRCLE)
					nkh++;
				if(OPERATOR[k].key==CN_RCIRCLE)
					nkh--;
				k++;
			}
			k--;
			int m;
			for( m=i;m<k;m++)
			{
				if(m<OPERATOR.GetSize()&&BOperator(OPERATOR[m])!=0)
					break;
			}
			if(m==k)
			{
			//	AfxMessageBox("() do not contain logical");
				int n=TEXP.GetSize()-1;
				if(n>=0&&TEXP[n].key!=CN_CINT||n<0)
				{
					TNODE T;
					T.key=CN_CINT;
					T.addr=icount+1;
					T.name="";
					T.deref=0;//wtt 2008.3.19
					T.line=ix>=0?TEMP[0].line:0; 
					TEXP.Add(T);
					icount++;
				}
				
				for(int j=INDEX[i];j<=INDEX[k];j++)
				{
					
					TList[icount-1].Add(TEMP[j]);
				}
				
				

				i=k+1;
				
			}
			//	AfxMessageBox("()  contain logical");

		}
		
		
		



		if(i<INDEX.GetSize()-1&&INDEX[i+1]>INDEX[i]+1&&i<OPERATOR.GetSize())
		{
		//	AfxMessageBox("have arith");
		
			TEXP.Add(OPERATOR[i]);

			TNODE T;
			T.key=CN_CINT;
			T.addr=icount+1;
			T.name="";
			T.deref=0;//wtt 2008.3.19
			T.line=ix>=0?TEMP[0].line:0; 
			TEXP.Add(T);
			
			for(int j=INDEX[i]+1;j<INDEX[i+1];j++)
			{
				TList[icount].Add(TEMP[j]);
			}
			icount++;
		}
		else if(i<INDEX.GetSize()-1&&INDEX[i+1]<=INDEX[i]+1&&i<OPERATOR.GetSize())
		{
			TEXP.Add(OPERATOR[i]);

		}
		else if(i==INDEX.GetSize()-1&&i<OPERATOR.GetSize())
		{
			TEXP.Add(OPERATOR[i]);

			if(INDEX[i]<TEMP.GetSize()-1)
			{
				TNODE T;
				T.key=CN_CINT;
				T.addr=icount+1;
				T.name="";
				T.deref=0;//wtt 2008.3.19
				T.line=ix>=0?TEMP[0].line:0; 
				TEXP.Add(T);
				for(int j=INDEX[i]+1;j<TEMP.GetSize();j++)
				{
				TList[icount].Add(TEMP[j]);
				}
				icount++;

			}
		}
		i++;

	}
	///////////////////////////////////
//	CString st="";
//	for(int k=0;k<20;k++)
	{
		
//		st+=PrintExp(TList[k])+", ";
			
	}
//	AfxMessageBox(" oprand:"+st);
	////////////////////////
	
/*	CString stemp="";
	for( i=0;i<20;i++)
	{
		
		if(TList[i].GetSize()!=0)
		{
			stemp+=PrintExp(TList[i]);
			stemp+="\r";
		}
	}
	AfxMessageBox(stemp);
	
	AfxMessageBox("TEXP after replaced"+PrintExp(TEXP));*/


/*	BOPPOS.AddTail(TEXP.GetSize());
    
	while(ix>=0)
	{
		int op=BOperator(TEXP[ix]);
		if(op!=0)
		{
			if(op<0)
			{
				//AfxMessageBox("relation operator");
				int index=ix;
				if(IsAExpression(index+1,BOPPOS.GetTail()-1,TEXP))
				{
					FindLeftExp(index,BOPPOS,TList[icount],TEXP);
					//AfxMessageBox("replace1:"+PrintExp(TList[icount]));
					icount++;

					// replace the arithmetic expression with 0;
					ReplaceOneAEXP(ix+1,index,TEXP);
					TEXP[ix+1].addr=icount; 
					//AfxMessageBox("replaced 1:"+PrintExp(TEXP));
					// end
				}
				else
				{
					TList[icount].Add(T);
					TEXP.InsertAt(ix+1,T);
					icount++;
					ReplaceOneAEXP(ix+1,ix+1,TEXP);
					TEXP[ix+1].addr=icount; 
					
				}
                 
				

                index=ix;
				
				FindRightExp(index,TList[icount],TEXP);
				//AfxMessageBox("replace2:"+PrintExp(TList[icount]));
				icount++;
                BOPPOS.AddTail(index);
				
				if(TList[icount-1].GetSize()==0)
				{
					TList[icount].Add(T);
					TEXP.InsertAt(ix,T);
					index=ix;
					ix++;
				}
			

				// replace the arithmetic expression with 0;
                ReplaceOneAEXP(index,ix-1,TEXP);
				TEXP[index].addr=icount; 
				//AfxMessageBox("replaced 2:"+PrintExp(TEXP));
				ix=index;
				// end

			}
			else if(op>0)
			{
				bool bexist=IsAExpression(ix+1,BOPPOS.GetTail()-1,TEXP);
			//	AfxMessageBox("logic operator");

				if(bexist==true)
				{
					//CString str;
					
					int index=ix;
					//str.Format("beg=%d,end=%d",index+1,BOPPOS.GetTail()-1) ;
					FindLeftExp(index,BOPPOS,TList[icount],TEXP);


				//	AfxMessageBox("1FindrightExp:"+PrintExp(TList[icount])+"\r");

					//AfxMessageBox(str+"|replace 3:"+PrintExp(TList[icount]));
					icount++;

					// replace the arithmetic expression with 0;
				    ReplaceOneAEXP(ix+1,index,TEXP);
					TEXP[ix+1].addr=icount; 
				    // AfxMessageBox("replaced 3:"+PrintExp(TEXP));
					//
					// end	
					//////////wtt////////////////		


					
				}
				BOPPOS.AddTail(ix); 
				
			}
		}

		ix--;
	}
    
	
	if(BOPPOS.GetCount()<=1)
	{
		TList[0].Copy(TEXP);
		ReplaceOneAEXP(0,TEXP.GetSize()-1,TEXP);
		icount=1;
		TEXP[0].addr=icount; 
	}
	else if(BOperator(TEXP[BOPPOS.GetTail()])>0 && TEXP[BOPPOS.GetTail()].key!=CN_NOT)
	{
		
		ix=BOPPOS.GetTail();
		int index=ix;
		FindRightExp(index,TList[icount],TEXP);
		
		
	//	AfxMessageBox("2  FindleftExp:"+PrintExp(TList[icount])+"\r");///////////


		if(TList[icount].GetSize()==0 )
		{
			TList[icount].Add(T); 
			TEXP.InsertAt(ix,T);
			ix++;
		}
        //AfxMessageBox("replace 4:"+PrintExp(TList[icount])+"]");
		icount++;

	    ReplaceOneAEXP(index,ix-1,TEXP);
		TEXP[index].addr=icount; 
		//AfxMessageBox("replaced 4:"+PrintExp(TEXP));
		ix=index;
	}
	////////////////////wtt/////////test////////////////
/*	CString stemp="";
	for(int i=0;i<20;i++)
	{
		
		if(TList[i].GetSize()!=0)
		{
			stemp+=PrintExp(TList[i]);
			stemp+="\r";
		}
	}
	AfxMessageBox(stemp);

*/


	/////////////////////wtt///////////////////////

	/********************end of this function********************/

}

/*bool CExpression::IsAExpression(int beg,int end,CArray<TNODE,TNODE> &TEXP)
{
	bool bexist=false;
	for(int i=beg;i<=end;i++)
	{
		if(TEXP[i].key==CN_VARIABLE ||
		   TEXP[i].key==CN_BFUNCTION||
		   TEXP[i].key==CN_DFUNCTION||
	       TEXP[i].key==CN_CINT     ||
	       TEXP[i].key==CN_CCHAR    ||
	       TEXP[i].key==CN_CFLOAT    )
		{
			bexist=true;
			break;
		}
	}

	return bexist;

}*/
/////////wtt///////////////////////////3.4///////////////////////////
/*void CExpression::ReplaceOneAEXP(int beg,int end,CArray<TNODE,TNODE> &TEXP)
{
	/************************************************************
	FUNCTION:
	Replace a arithmetic expression int "TEXP" with a TNODE
	in order to simplify the bool expression
	PARAMETER:
	beg  : beginning position of the arithmetic expression;;
	end  : end position of the arithmetic expression;
	TEXP : bool expression
	************************************************************/

/*	static int index;
	TNODE T,T1;
	index++;

	T.key=CN_CINT;
	T.addr=0;
	T.name="";
	if(TEXP.GetSize())
	{
		T.line=TEXP[0].line;  
	}
	

 	TEXP.InsertAt(beg,T);


	for(int ix=end+1;ix>beg;ix--)
	{
		TEXP.RemoveAt(ix);
	}

		



}*/

/*void CExpression::FindLeftExp(int &ix,CList<int,int> &Bound,CArray<TNODE,TNODE> &TEMP,CArray<TNODE,TNODE> &TEXP)
{
	/************************************************************
	FUNCTION:
	find the range of the arithmetic expression which begins with 
	the operator position "ix"
	PARAMETER:
	ix   : the operator position of the arithmetic expression;;
	TEMP : Array to storesthe  arithmetic expression;
	TEXP : bool expression
	************************************************************/
	   
/*	int nkh=0;
	ix++;
	while(ix<Bound.GetTail() &&  nkh>=0)
	{
		
		if(TEXP[ix].key==CN_LCIRCLE)
		{
			nkh++;
		}

		if(TEXP[ix].key==CN_RCIRCLE)
		{
			nkh--;
		}

		if(ix<Bound.GetTail() &&  nkh>=0)
		{
			TEMP.Add(TEXP[ix]);
			ix++;
		}
		else
		{
			break;
		}
	}
	
	ix--;
	if(nkh>0)
	{
		for(int i=0;i<TEMP.GetSize();i++)
		{
			if(TEMP[i].key==CN_LCIRCLE)
			{
				TEMP.RemoveAt(i);
				break;
			}

		}
		

	}

	
}*/


/*void CExpression::FindRightExp(int &ix,CArray<TNODE,TNODE> &TEMP,CArray<TNODE,TNODE> &TEXP)
{
	/************************************************************
	FUNCTION:
	find the range of the arithmetic expression which end with 
	the operator position "ix"
	PARAMETER:
	ix   : the operator position of the arithmetic expression;;
	TEMP : Array to storesthe  arithmetic expression;
	TEXP : bool expression
	************************************************************/
	
    
/*	int nkh=0;
	int begpos;
	CArray<TNODE,TNODE> T;
	begpos=ix;
	ix--;
	
	while(ix>=0 &&  nkh<=0 && BOperator(TEXP[ix])==0)
	{
		
		if(TEXP[ix].key==CN_LCIRCLE)
		{
			nkh++;
		}

		if(TEXP[ix].key==CN_RCIRCLE)
		{
			nkh--;
		}
        if(ix>=0 &&  nkh<=0 && BOperator(TEXP[ix])==0)	
		{
			
			T.Add(TEXP[ix]); 
			ix--;
			
		}
		else
		{
			
			break;
		}
		
	}	

	ix++
		;
    for(int i=T.GetSize()-1;i>=0;i--)
	{
		TEMP.Add(T[i]); 
	}
    

	
	
}*/
////////////////////////////////wtt//////////////////////////////////////
void CExpression::BoolExpAnalyze(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry,CArray<ENODE*,ENODE*> &EList)
{
	/*************************************************************
	  FUNCTION:
	  Analyze the bool expression and translate it to syntax tree.
	  PARAMETERS:
	  TEXP  : arithmetic expression;
	  phead : head pointer of the syntax tree.
	*************************************************************/
    static int boolnum;
	boolnum++;
    

    int index=0;
	int error=0;
	int nBottom,nTop,ix=0;
	TNODE T;
	SSTACK st;
	CArray<SSTACK,SSTACK> S;
	
	
    if(TEXP.GetSize()<=0)
	{//////////////////wtt////3.9/////////////
		if( EList.GetSize()>=1)
		{
			ptr->m_pinfo=EList[0];
			return;
		}
		//////////////wtt///////////////////////////
		ENODE *pe=new ENODE;
		for(int i=0;i<10;i++)
		{
			pe->pinfo[i]=NULL;
		}
		pe->pleft=NULL;
		pe->pright=NULL; 
		pe->T.addr=0;
		pe->T.key=CN_CINT;
		pe->info=0; 
		pe->T.deref=0;//wtt 2008.3.19
		ptr->m_pinfo=pe;
		return;
	}

	if(TEXP.GetSize()==1 && EList.GetSize()>=1)
	{
		ptr->m_pinfo=EList[0];
		return;
	}

	st.info=CN_EQUAL;
	st.pEN=NULL;
	st.T.key=CN_EQUAL;  
	S.Add(st);
	T.line=TEXP[0].line;  
	T.key=CN_LINE;
	TEXP.Add(T); 
	nBottom=0;
    
	CString str,s;

	

	// 布尔表达式分析部分
	while((index<TEXP.GetSize())&&((TEXP.GetSize()!=2)||(S.GetSize()>=2&&S[1].info!=0))) 
	{
		if(ix>100)
		{   //error
			error++;
			break;
		}
	    ix++;

		
		nTop=S.GetSize()-1; 
		if(nTop>=0&&S[nTop].info==0)
		{
			// "...info==0" represents the noterminal symbol

			if(S.GetSize()>=1)
			{
				nBottom=nTop-1;
				if((S.GetSize()>=2)&&(S[nBottom].info==0)) 
				{
					error++;
				}
			}		
			else
			{
				error++;
			
			}
		}
		else
		{
			nBottom=nTop;
		}

        if(error>0)
		{
			error=0;
			if(S.GetSize()>1)
			{
				S.RemoveAt(nTop);
				continue;
			}
		}

				 

		// get relation and decide to induce or move
		int np=BGetPrior(S[nBottom].info,TEXP[index].key);

		#ifdef DEBUG
//		s.Format("NO%d bottom[%d] info=%d\nnp=%d,index=%d,len=%d\n(%d,%d)",boolnum,nBottom,S[nBottom].info,np,index,TEXP.GetSize(),S[nBottom].info,TEXP[index].key); 
//		AfxMessageBox(s);
		#endif


		// move to syntax stack
		switch(np)
		{
		case 1:
			//对应产生式规约
			BSTopInduce(index,TEXP,S,SArry);		
		    break;
		case 2:
		case 0:
			BPushToStack(index,TEXP,S,SArry,EList);
		    index++;
		    break;		
		    
		default:
			if(S.GetSize()>1)
			{
				S.RemoveAt(nTop);
				continue;
			}
		}

        if(S.GetSize()==1 && index+1>=TEXP.GetSize())
		{
			break;

		}
		if(S.GetSize()==2 && index+1>=TEXP.GetSize() && S[1].info==0)
		{
			break;
		}
		
	}

	TEXP.RemoveAt(TEXP.GetSize()-1) ;

	if(S.GetSize()>=2)
	{
		ptr->m_pinfo=S[1].pEN;
	}
	/********************end of this function********************/
}


void CExpression::BSTopInduce(int &ix,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry)
{
	/*************************************************************
	  FUNCTION:
	  induce the elements on the top of sytax stack S;
	  PARAMETER:
	  tag  : induce type
	  ix   : index of node in TEXP to push;
	  TEXP : expression
	  S    : syntax stack
	*************************************************************/

    CString str;
    int top=S.GetSize()-1;
	if((top>=3)&&(S[top].info==0)&&(S[top-2].info==0)&&(S[top-1].info!=0)&&(BOperator(S[top-1].T)!=0))
	{
        switch(S[top-1].info)
		{
			case CN_GREATT:       // >
			case CN_LESS:         // <
			case CN_LANDE:        // <= 
			case CN_BANDE:        // >=
			case CN_NOTEQUAL:     // !=
			case CN_DEQUAL:       // == 
			case CN_DAND:
			case CN_DOR:

				#ifdef DEBUG
//				str.Format("premove Slen=%d",S.GetSize()); 
//				AfxMessageBox(str);
				#endif

				S[top-1].info=0;
                S[top-1].pEN->pleft=S[top-2].pEN;
				S[top-1].pEN->pright=S[top].pEN;
				S.RemoveAt(top);
				top=S.GetSize()-1;
				S.RemoveAt(top-1);
				top=S.GetSize()-1; 

				#ifdef DEBUG
//				str.Format("remove Slen=%d(%d,%d,%d)",S.GetSize(),S[top].pEN->T.key,S[top].pEN->pleft->T.key,S[top].pEN->pright->T.key ); 
//				AfxMessageBox(str);
				#endif
				break;
			default:
				 ;
		 }
		 
         return ;
	 }

	 if((S.GetSize()>=3)&&(S[top].info==CN_RCIRCLE)&&(S[top-1].info==0)&&(S[top-2].info==CN_LCIRCLE))
	 {
		 S.RemoveAt(top);
		 top=S.GetSize()-1;
		 S.RemoveAt(top-1);
		 return;
	 }

	 if((S.GetSize()>=2)&&(S[top].info==0)&&(S[top-1].info==CN_NOT))
	 {
		 S[top-1].pEN->pleft=S[top].pEN;   
		 S[top-1].info=0;
		 S.RemoveAt(top);
		 return;
	 }

	 if(S.GetSize()>=2)
	 {
		 S.RemoveAt(S.GetSize()-1);
	 }

}

void CExpression::BPushToStack(int &index,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry,CArray<ENODE*,ENODE*> &EList)
{
	/*************************************************************
	  FUNCTION:
	  Push the node in TEXP(index is ix) to syntax stack S.
	  PARAMETER:
	  ix   : index of node in TEXP to push;
	  TEXP : expression
	  S    : syntax stack
	  EList: arithmetic expression in bool expression
	*************************************************************/

	SSTACK st;
	ENODE  *penode=NULL;

	st.info=0;
	CString str;
	st.info=0;
    int i;
	switch(TEXP[index].key)
	{

		case CN_CINT:
			st.info=0;
			CopyTNODE(st.T,TEXP[index]);
            //CopyTNODE(penode->T,TEXP[index]);
			st.pEN=EList[TEXP[index].addr-1]; 
			break;
		case CN_GREATT:       // >
		case CN_LESS:         // <
		case CN_LANDE:        // <= 
		case CN_BANDE:        // >=
		case CN_NOTEQUAL:     // !=
		case CN_DEQUAL:       // == 
		case CN_NOT:
		case CN_DAND:
		case CN_DOR:
			st.info=TEXP[index].key;
			CopyTNODE(st.T,TEXP[index]);
			penode=new ENODE;
			for(i=0;i<10;i++)
			{
				penode->pinfo[i]=NULL; 
			}
			penode->info=0; 
//			penode->T.deref=0;
			penode->pleft=NULL;
			penode->pright=NULL; 
			CopyTNODE(penode->T,TEXP[index]);
			st.pEN=penode; 
			break;		
        default:
			st.info=TEXP[index].key;
			CopyTNODE(st.T,TEXP[index]);
			st.pEN=NULL; 
		}

	S.Add(st);	

    /******************end of this function*********************/ 
}

int  CExpression::BGetPrior(const int n1,const int n2)
{/////////////wtt//////////////3.4////////
	int m1=-1,m2=-1;
	char op;
	TNODE T;
	
	if(n1==CN_EQUAL)
	{
		return 2;
	}

	if(n2==CN_LINE)
	{
		return 1;
	}
	
	
	

    switch(n1)
	{
	case CN_GREATT:
	case CN_LESS:
	case CN_LANDE:
	case CN_BANDE:
		m1=1;
		break;
	case CN_NOTEQUAL:
	case CN_DEQUAL:
			m1=2;
			break;


	case CN_NOT:
		m1=3;
		break;
	case CN_DAND:
		m1=4;
		break;
	case CN_DOR:
		m1=5;
		break;
    case CN_LCIRCLE:
	    m1=6;
	    break;
	case CN_RCIRCLE:
	    m1=7;
	    break;
	
	case CN_CINT:
	    m1=8;
	    break;
	default:
			return -1;

	}
   
	switch(n2)
	{
	case CN_GREATT:
	case CN_LESS:
	case CN_LANDE:
	case CN_BANDE:
		m2=1;
		break;
	case CN_NOTEQUAL:
	case CN_DEQUAL:
			m2=2;
			break;


	case CN_NOT:
		m2=3;
		break;
	case CN_DAND:
		m2=4;
		break;
	case CN_DOR:
		m2=5;
		break;
    case CN_LCIRCLE:
	    m2=6;
	    break;
	case CN_RCIRCLE:
	    m2=7;
	    break;

	case CN_CINT:
	
	    m2=8;
	    break;
	default:
		return -1;
	}
	
	#ifdef DEBUG
//    CString s;
//	s.Format("(%d,%d)",m1,m2);
//	AfxMessageBox(s);
	#endif

	if(m1>=1&&m1<=8&&m2>=1&&m2<=8)
	{
		op=BOPARRAY[m1][m2];
	}
	else
	{
		return -1;
	}

    if(op=='>')
	{
		return 1;
	}

	if(op=='<')
	{
		return 2;
	}

	if(op=='=')
	{
		return 0;
	}
    else
	{
		return -1;
	}
}
/*int CExpression::BOperator(TNODE T)
{
	switch(T.key)
	{
	case CN_NOT:     // !
	case CN_DAND:    // &&
	case CN_DOR:     // ||
		return 0;
		break;
	case CN_GREATT:  // >
	case CN_LESS:    // <
	case CN_LANDE:   // <=
	case CN_BANDE:   // >=
	case CN_NOTEQUAL:// !=
	case CN_DEQUAL:  // ==  
		return 1;
		break;
	default:
		return -1;

	}

}
*/
///////////////////wtt/////3.4//////////////
int CExpression::BOperator(TNODE T)
{
	switch(T.key)
	{
	case CN_NOT: 
		return 1;// !
	case CN_DAND:
		return 2;// &&
	case CN_DOR:     // ||
		return 3;
		
	case CN_GREATT:
		return -1;// >
	case CN_LESS: 
		return -2;// <
	case CN_LANDE:
		return -3;// <=
	case CN_BANDE:
		return -4;// >=
	case CN_NOTEQUAL:
		return -5;// !=
	case CN_DEQUAL:
		return -6;// ==  
		
		
	default:
		return 0;

	}

}
/////////////wtt//////////3.3/////////////
// extern function 
//////////////wtt////////3.5///////////
void BExpStandard(ENODE*pNode,CArray<SNODE,SNODE>&SArry)
{
	CExpression exp;
	exp.BoolExpStandard(pNode,SArry); 

}
void AExpStandard(ENODE*pNode,CArray<SNODE,SNODE>&SArry)
{
	CExpression exp;
	exp.ArithExpStandard(pNode,SArry); 

}
////////wtt////////////////////////////



							/******************************
                                the above function group:
							translate arithmetic expression
							to synax tree.
                            ******************************/
//@@@@@@@@@@@@@#############$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&***********//




//@@@@@@@@@@@@@#############$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&***********//

							/******************************
                               the following function group:
							translate arithmetic expression
							to synax tree.
                            ******************************/


void CExpression::AExpToTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry)
{
	/*************************************************************
	  FUNCTION:
	  Analyze the arithmetic expression and translate it to syntax 
	  tree.
	  PARAMETERS:
	  TEXP  : arithmetic expression;
	  phead : head pointer of the syntax tree.
	*************************************************************/
    static int num;
	num++;


    int index=0;
	int error=0;
	int nBottom,nTop,ix=0;
	TNODE T;
	SSTACK st;
	CArray<SSTACK,SSTACK> S;
	
	// error 
    if(TEXP.GetSize()<=0)
	{
		ENODE *pe=new ENODE;
		for(int i=0;i<10;i++)
		{
			pe->pinfo[i]=NULL; 
		}
		pe->pleft=NULL;
		pe->pright=NULL; 
		pe->T.addr=0;
		pe->T.deref=0;//wtt 2008.3.19
		pe->T.key=CN_CINT;
		pe->T.name="";
		pe->T.line=0;  
		ptr->m_pinfo=pe;
//		pe->T.deref=0;
		return;
	}
//	CountDeref(TEXP,SArry);///////////////处理指针脱引用

	// array initialize
	if(ptr->m_sType=="SDGASSIGNMENT")
	{
		CSDGAssignment *pas=(CSDGAssignment *)ptr;
	///////////////wtt////////////1.19//////////////////
		/*if(pas->TEXP.GetSize()>0 && 
		  pas->TEXP[0].key==CN_VARIABLE &&
		   SArry[pas->TEXP[0].addr].kind==CN_ARRAY &&
		   TEXP[0].key==CN_LFLOWER)*/
        if(pas->TCnt.GetSize()>0 && 
		   pas->TCnt[0].key==CN_VARIABLE &&pas->TCnt[0].addr>=0&&pas->TCnt[0].addr<SArry.GetSize()&&
		   SArry[pas->TCnt[0].addr].kind==CN_ARRAY 		 
		  &&TEXP[0].key==CN_LFLOWER  )
		  ////////////////////////wtt/////////////////////////
		{	//数组初始化				
		//	AfxMessageBox("Array initialization");////////////
			ENODE *pin=new ENODE;
			InitENode(pin);
			pin->T.line=TEXP[0].line;  
			pin->T.deref=TEXP[0].deref; 
			ptr->m_pinfo=pin;
			return;
		}
	}

	int itemp=GetAExpInfo(TEXP);
	if(ptr->m_sType=="SDGRETURN")
	{
		CSDGReturn *ptemp=(CSDGReturn *)ptr;
		ptemp->m_iValue=itemp;
	}
	//AfxMessageBox(PrintExp(TEXP));
	st.info=CN_EQUAL;
	st.pEN=NULL;
	st.T.key=CN_EQUAL;
	S.Add(st);

    st.info=CN_LCIRCLE;
	st.T.key=CN_LCIRCLE;
	S.Add(st);
    
	T.line=TEXP[0].line;  
	T.key=CN_RCIRCLE; 
	TEXP.Add(T);
	T.key=CN_LINE;
	TEXP.Add(T);

	nBottom=0;    
	CString str,s;	
    

	// 算术表达式分析部分
	while((index<TEXP.GetSize())||(TEXP.GetSize()!=2)||(S.GetSize()>=2&&S[1].info!=0)) 
	{
		if(ix>100)
		{   //error
		//	AfxMessageBox("error");//////////
			error++;
			ErrorInExp(TEXP[0].line,S);
			break;
		}
	    ix++;

		
		nTop=S.GetSize()-1; 
		if(nTop>=0&&S[nTop].info==0)
		{
			// "...info==0" represents the noterminal symbol

			if(S.GetSize()>=1)
			{
				nBottom=nTop-1;
				if((S.GetSize()>=2)&&(S[nBottom].info==0)) 
				{
					error++;
				}
			}		
			else
			{
				error++;			
			}
		}
		else
		{
			nBottom=nTop;
		}

        if(error>0)
		{
			error=0;
			if(S.GetSize()>1)
			{
				ErrorInExp(TEXP[0].line,S);
				break;
			}
		}
        
		// pointer variable assigning;

		if( (index+1<TEXP.GetSize())&&
			(TEXP[index].key==CN_AND) &&
		    (TEXP[index+1].key==CN_VARIABLE))
		{
			//AfxMessageBox("refer"+TEXP[index+1].name);
            TEXP[index+1].deref=-1;///王甜甜2006.3.21指针的脱引用级别  取地址脱引用级别为-1

			index++;
			
		} //wtt//2006.3.21
	
		// get relation and decide to induce or move
		int k=TEXP[index].key;
		k=S[nBottom].info;
		int np=GetPrior(S[nBottom].info,TEXP[index].key);

		if(S.GetSize()>0 &&
		   (S[S.GetSize()-1].T.key==CN_FORMAT||
		    S[S.GetSize()-1].T.key==CN_DIV   ||
			S[S.GetSize()-1].T.key==CN_MULTI ||
			S[S.GetSize()-1].T.key==CN_ADD) 
		    &&
			S[S.GetSize()-1].info!=0 
			&& 
			(TEXP[index].key==CN_ADD ||
			TEXP[index].key==CN_SUB ||
			TEXP[index].key==CN_MULTI))
		{
			//	AfxMessageBox("ok");/////////////	
			np=2;
		}		

		// move to syntax stack
		switch(np)
		{
		case 1:
			//对应产生式规约
		//	AfxMessageBox("规约   "+PrintExp(TEXP) + C_ALL_KEYWORD[TEXP[index].key]);////////////////!!!!

			STopInduce(index,TEXP,S,SArry);	
		//	AfxMessageBox("规约后   "+PrintExp(TEXP) + C_ALL_KEYWORD[TEXP[index].key]);////////////////!!!!
		    break;
		case 2:
		case 0:
		//	AfxMessageBox("入栈   "+PrintExp(TEXP) + C_ALL_KEYWORD[TEXP[index].key]);////////////////!!!!

			PushToStack(index,TEXP,S,SArry);
		//	AfxMessageBox("入栈后   "+PrintExp(TEXP) + C_ALL_KEYWORD[TEXP[index].key]);////////////////!!!!

		    index++;
		    break;		    
		default:
		//	AfxMessageBox("error in exp");//////////////
			ErrorInExp(TEXP[0].line,S);
		//	AfxMessageBox("after error");////////
			index=TEXP.GetSize(); 
			break;
		}

        if(S.GetSize()==1 && index+1>=TEXP.GetSize())
		{
			break;

		}
		if(S.GetSize()==2 && index+1>=TEXP.GetSize() && S[1].info==0)
		{
			break;
		}		
	}

	TEXP.RemoveAt(TEXP.GetSize()-1) ;

	if(S.GetSize()>=2 && S[1].pEN )
	{
		ptr->m_pinfo=S[1].pEN;
	}

	if(!ptr->m_pinfo)
	{
	//	AfxMessageBox("!ptr->m_pinfo");/////////////
		ptr->m_pinfo=new ENODE;
		InitENode(ptr->m_pinfo);
		ptr->m_pinfo->T.line=TEXP.GetSize()?TEXP[0].line:0;     
	}
	
	// standard the arithmetic expression

	if(ptr->m_pinfo)
	{
		SetAPIndex(ptr->m_pinfo,SArry);
		ptr->m_pinfo=ArithExpStandard(ptr->m_pinfo,SArry);  
	}
    
	
	/********************end of this function********************/
}
/*void CExpression::CountDeref(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry)//wtt   2008.3.18  预处理指针脱引用
{
	int i=TEXP.GetSize()-1;
	while(i>=0)
	{
		if(TEXP[i].addr>=0&&TEXP[i].addr<SArry.GetSize()&&SArry[TEXP[i].addr].kind==CN_POINTER)
		{
			TEXP[i].deref=0;
			int k=i-1;
			while(k>=0&&TEXP[k].key==CN_MULTI)
			{
				TEXP[i].deref++;
				TEXP.RemoveAt(k);
				k--;
			}
			AfxMessageBox(TEXP[i].name);
			CString s;
			s.Format("deref=%d",TEXP[i].deref);
			AfxMessageBox(s);
			i=k+1;

		}

		i--;
	}

}*/

void CExpression::ErrorInExp(int line,CArray<SSTACK,SSTACK> &S)
{
	if(S.GetSize()>1)
	{
		for(int i=S.GetSize()-1;i>=1;i--)
		{
			CSDGBase Temp;
			Temp.m_pinfo=S[i].pEN;  
	    	DeleteExpTree(&Temp);
			S.RemoveAt(i); 
		}
	}
            
	ER.ID=8;
	ER.line=line;
	m_pError->Add(ER);
}

void CExpression::STopInduce(int &ix,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry)
{
	/*************************************************************
	  FUNCTION:
	  induce the elements on the top of sytax stack S;
	  PARAMETER:
	  tag  : induce type
	  ix   : index of node in TEXP to push;
	  TEXP : expression
	  S    : syntax stack
	*************************************************************/
    CString str;
	int top=S.GetSize()-1;
	ENODE *ptemp=S[top].pEN;
    ptemp=S[top-1].pEN;
	////////////////////////////////////
/*	int num=top;
	AfxMessageBox("start deduce  stack top:"+	TraverseAST(S[top].pEN,SArry));



	while(num>=0)
	{
	if(S[num].pEN)
	{
		CString st;
		st.Format("%d  S[num].pEN->T.key=%d",num,S[num].pEN->T.key);
		AfxMessageBox(st);
	if(S[num].pEN->T.key<133)
		AfxMessageBox("stack : "+C_ALL_KEYWORD[S[num].pEN->T.key] );//////////
	}
	num--;
	}*/
	///////////////////////////
	if((top>=3)&&(S[top].info==0)&&(S[top-2].info==0)&&(S[top-1].info!=0))
	{
        switch(S[top-1].info)
		{
			case CN_ADD:
			case CN_MULTI:
		    case CN_SUB:
			case CN_DIV:
			case CN_FORMAT:
			

				/*if(S[top].dValue==0)
				{
					AfxMessageBox("严重错误：除零操作");
					exit(0);
				}
				*/
				
                
				S[top-1].info=0;
                S[top-1].pEN->pleft=S[top-2].pEN;
				S[top-1].pEN->pright=S[top].pEN;
				S.RemoveAt(top);
				top=S.GetSize()-1;
				S.RemoveAt(top-1);
				top=S.GetSize()-1; 				
				break;

			default:
				 ;
		 }
		 
         return ;
	 }

	if((top>=1)&&(S[top].info==0)&&(S[top-1].info!=0)&&S[top-1].pEN&&S[top-1].pEN->T.key==CN_MULTI&&(top-2<0||top-2>=0&&S[top-2].info!=0))
	{//wtt 2008.3.18  处理指针算术的解引用
	//	AfxMessageBox("OK");
//	AfxMessageBox(PrintExp(TEXP));
	//	AfxMessageBox(TraverseAST(S[top].pEN));///////
	//	AfxMessageBox("deref");
		S[top-1].pEN=S[top].pEN;
		if(S[top-1].pEN->T.deref<-1||S[top-1].pEN->T.deref>=10)///////////!!!!!!!!!!!
			S[top-1].pEN->T.deref=0;/////////////

		S[top-1].pEN->T.deref=(S[top-1].pEN->T.deref)+1;
	/*	CString st;
		st.Format("  deref=%d",S[top-1].pEN->T.deref);
		AfxMessageBox(TraverseAST(S[top].pEN,SArry)+st);*/
		S.RemoveAt(top);
		top=S.GetSize()-1;
		S[top].info=0;

		return;
	}////////////////这是2008 3.18最后的修改将脱引用号*移除,在其表达式的头结点deref中记录脱引用级别




    
	 if(top-2>=0&&top<S.GetSize()&&(S.GetSize()>=3)&&(S[top].info==CN_RCIRCLE)&&(S[top-1].info==0)&&(S[top-2].info==CN_LCIRCLE))
	 {
//		 AfxMessageBox("before ArrayPointerInExp");
   //      ArrayPointerInExp(S[top-1].pEN,SArry);//wtt 2008.3.18将其去掉了
//AfxMessageBox("after ArrayPointerInExp");
		 CopyTNODE(S[top-1].T,S[top-1].pEN->T);   
		 S.RemoveAt(top);
		 top=S.GetSize()-1;
		 S.RemoveAt(top-1);

		 top=S.GetSize()-1;
			/* if(S.GetSize()>=2 && S[top].T.key==CN_VARIABLE && (S[top].T.addr>=0&&S[top].T.addr<SArry.GetSize()&&(SArry[S[top].T.addr].kind==CN_ARRAY || 
			SArry[S[top].T.addr].kind==CN_POINTER)) && S[top-1].T.key==CN_MULTI && PointerRefer(S))
		 {
			 //ENODE *pet=S[top].pEN;
		//	 AfxMessageBox("ok");/////////////////
			 S[top].pEN->T.value+=CN_MULTI;
			 S[top].info=0;
		 if((SArry[S[top].T.addr].kind==CN_POINTER) && S[top-1].T.key==CN_MULTI)
		 			 S[top].pEN->T.deref=1;//wtt//2006.3.21// 指针脱引用
			  
			 S[top].pEN->info++; 
			 S.RemoveAt(top-1);
		 }*/
	/*	if(S[top].T.addr>=0&&S[top].T.addr<SArry.GetSize()&&SArry[S[top].T.addr].kind==CN_POINTER)
		{
			bool flag=true;
			int pos=top-1;
		//	AfxMessageBox("pointer");////
			while(flag&&pos>=0)
			{
				if(S[pos].T.key==CN_MULTI&&(pos<=0||pos>0&&!(S[pos-1].T.key==CN_RCIRCLE||S[pos-1].T.key==CN_VARIABLE)))
				{
					 S[top].pEN->T.deref=1;//wtt//2006.3.21// 指针脱引用
					 S[top].pEN->info++; 
					 S.RemoveAt(pos);
					 flag=false;

					}
				pos--;

			}
		}*///wtt 2008.3.18将其去掉了
		 
		 		 
		 return;
	 }

	 if((S.GetSize()>=2)&&(S[top].info==0)&&(S[top-1].info==CN_SUB))
	 {
		 S[top-1].pEN->pleft=S[top].pEN;
		 S[top-1].info=0;
		 S.RemoveAt(top);
		 return;
	 }

	 if((S.GetSize()>=2)&&(S[top].info==0)&&(S[top-1].info==CN_ADD))
	 {
		 S.RemoveAt(top-1);
		 return;
	 }


	/* if( S.GetSize()>=2 && S[top].T.key==CN_VARIABLE &&	 PointerRefer(S)
		 &&S[top].T.addr>=0&&S[top].T.addr<SArry.GetSize()&&
		( (SArry[S[top].T.addr].kind==CN_ARRAY && S[top].pEN->info<SArry[S[top].T.addr].arrdim ) 
		|| SArry[S[top].T.addr].kind==CN_POINTER)
		&& S[top-1].T.key==CN_MULTI )
	 {
		 //	 AfxMessageBox("dereference  pointer ok");/////////////////

		 ///////////////////wtt 2008 0318///////////////////////
		/*  if((SArry[S[top].T.addr].kind==CN_POINTER) && S[top-1].T.key==CN_MULTI)
		 {    //AfxMessageBox("dereference  pointer ok");/////////////////
			//S[top].pEN->pinfo[S[top].pEN->info]=new ENODE;
		//	InitENode(S[top].pEN->pinfo[S[top].pEN->info]);
		//	S[top].pEN->pinfo[S[top].pEN->info]->T.line=TEXP.GetSize()?TEXP[0].line:0;    
			S[top].pEN->T.value+=CN_MULTI;
		    S[top].pEN->info++;
			//AfxMessageBox(S[top].pEN->T.name);
		    S[top].pEN->T.deref=1;//wtt//2006.3.21// 指针脱引用
		    // AfxMessageBox("pointer dereference ok");
		 	S[top].info=0;
	
			ENODE*ptop= S[top].pEN;
			S.RemoveAt(top-1);
		    ptop->T.deref=1;
			int t=top-2;
			ENODE*pdereference;
			pdereference=new ENODE;
			while(t>=0&&S[t].T.key==CN_MULTI) 
			 { 			
				ptop->T.deref=ptop->T.deref+1;
				S.RemoveAt(t);
				t--;
			 }
				//	S[top].pEN->pleft=pdereference->pleft;
					 	
		 }
		  else
		  {
			 S[top].pEN->pinfo[S[top].pEN->info]=new ENODE;
			 InitENode(S[top].pEN->pinfo[S[top].pEN->info]);
		     S[top].pEN->pinfo[S[top].pEN->info]->T.line=TEXP.GetSize()?TEXP[0].line:0;    
			 S[top].pEN->T.value+=CN_MULTI;
			 S[top].pEN->info++;
			 S[top].info=0;
			 S.RemoveAt(top-1);

		  //}
				 ///////////////////wtt 2008 0318///记录指针的脱引用级deref////////////////////
	 }*/

	 if(S.GetSize()>=2)
	 {
		 //S.RemoveAt(S.GetSize()-1);
	 }

}


bool CExpression::PointerRefer(CArray<SSTACK,SSTACK> &S)
{
	int top=S.GetSize()-1;
	
	if(top<=3)
	{
		return true;
	}
	else
	{
		if(S[top-2].info==0 || S[top-2].info==CN_RCIRCLE)
		{
			return false;
		}

		if(S[top-2].info==CN_ADD   || S[top-2].info==CN_SUB ||
		   S[top-2].info==CN_MULTI || S[top-2].info==CN_DIV ||
		   S[top-2].info==CN_FORMAT|| S[top-2].info==CN_LCIRCLE)
		{
			return true;
		}
	}

	return true;
}


void CExpression::PushToStack(int &index,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry)
{
	/*************************************************************
	  FUNCTION:
	  Push the node in TEXP(index is ix) to syntax stack S.
	  PARAMETER:
	  ix   : index of node in TEXP to push;
	  TEXP : expression
	  S    : syntax stack
	*************************************************************/

    SSTACK st;
	ENODE  *penode;

	if(index>=TEXP.GetSize())
	{
		return;
	}

	penode=new ENODE;
	InitENode(penode);
	penode->T.line=TEXP.GetSize()?TEXP[0].line:0;  
	penode->T.deref=TEXP.GetSize()?TEXP[0].deref:0;

	st.info=0;

	CString str;
	st.info=0;
	int nTop=0;
    int k;
	int n=TEXP[index].key;
	switch(TEXP[index].key)
	{
		case CN_VARIABLE:
			
			st.info=0;
			CopyTNODE(st.T,TEXP[index]);
            CopyTNODE(penode->T,TEXP[index]);
			st.pEN=penode; 
			// common variable
			            
			if( (index-1>=0) && (TEXP[index-1].key==CN_AND))
			{
				penode->T.value+=CN_AND;

		
			}

			// reference of array type 1:
			if(TEXP[index].addr>=0&&TEXP[index].addr<SArry.GetSize())
			k=SArry[TEXP[index].addr].kind;
		//	if(TEXP[index].addr>=0&&TEXP[index].addr<SArry.GetSize()&&SArry[TEXP[index].addr].kind==CN_ARRAY && TEXP[index+1].key==CN_LREC)
			if(TEXP[index].addr>=0&&TEXP[index].addr<SArry.GetSize()&&(SArry[TEXP[index].addr].kind==CN_ARRAY||SArry[TEXP[index].addr].kind==CN_ARRPTR||SArry[TEXP[index].addr].kind==CN_POINTER) && index+1<TEXP.GetSize()&&TEXP[index+1].key==CN_LREC)//wtt 2008.0318

			{
			//	AfxMessageBox(PrintExp(TEXP));////////////
				ArrayToTree(index,st,TEXP,SArry);										
			}
			
			
			break;
		case CN_DFUNCTION:
		    //待扩充功能时加入
		    st.info=0;
			CopyTNODE(st.T,TEXP[index]);
            CopyTNODE(penode->T,TEXP[index]);
			st.pEN=penode; 

            FunctionToTree(index,st,TEXP,SArry);
			break;
        case CN_BFUNCTION:
			st.info=0;
			CopyTNODE(st.T,TEXP[index]);
            CopyTNODE(penode->T,TEXP[index]);
			st.pEN=penode; 

            FunctionToTree(index,st,TEXP,SArry);

			break;
		case CN_CINT:
		case CN_CFLOAT:
		case CN_CCHAR:
		case CN_CSTRING:
		case CN_CDOUBLE:
			st.info=0;
			CopyTNODE(st.T,TEXP[index]);
            CopyTNODE(penode->T,TEXP[index]);
			st.pEN=penode; 	
			break;
		case CN_ADD:
		case CN_SUB:
		case CN_MULTI:
		case CN_DIV:
		case CN_FORMAT:

			st.info=TEXP[index].key;
			CopyTNODE(st.T,TEXP[index]);
            CopyTNODE(penode->T,TEXP[index]);
			st.pEN=penode; 
	
			break;		
        default:
			st.info=TEXP[index].key;
			st.pEN=NULL; 
			CopyTNODE(st.T,TEXP[index]);
	}

	S.Add(st);

    /******************end of this function*********************/ 
}

void CExpression::ArrayPointerInExp(ENODE *pEHead,CArray<SNODE,SNODE> &SArry)
{
	/************************************************************************
	  FUNCTION:
	  Operation of pointer address
	  PARAMETERS:
	  pEHead : head pointer of syntax sub-tree
	  SArry  : ...
	************************************************************************/
 
    if(!pEHead)
	{
		return;
	}
 
	int  iFind=0;
	int  iDM=0;

	ENODE *p=pEHead;
	ENODE *ptr=NULL,*ptrf=NULL,*ptemp=NULL;
    CList<ENODE *,ENODE *> STK;

   	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
            if(p->T.key==CN_VARIABLE && p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_ARRAY &&
			   p->info<SArry[p->T.addr].arrdim )
			{
				ptr=p;
				ptrf=ptemp;
				iFind++;
			}

			if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize() && SArry[p->T.addr].kind==CN_POINTER && 
				p->info<SArry[p->T.addr].value)
			{
				ptr=p;
				ptrf=ptemp;
				iFind++;
			}

            /*
			if(p->T.key==CN_DIV || p->T.key==CN_MULTI || p->T.key==CN_FORMAT)
			{
				iDM++;
			}
			*/

			STK.AddTail(p);
			p=STK.GetTail()->pleft;
			ptemp=STK.GetTail();
		}
		else
		{
			p=STK.GetTail()->pright;
			ptemp=STK.GetTail();
			STK.RemoveTail();			
		}
	}

	if(iDM>0 || iFind<=0 || iFind>1)
	{
		return;
	}
	
	if(!LegalAddrOperation(ptr,pEHead))
	{
		// warning : error operating of pointer
		return;
	}
	

    if(ptrf==NULL)
	{
		if(ptr->info>=0 && ptr->info<SArry[ptr->T.addr].value)
		{
			if(!ptr->pinfo[ptr->info])
			{
				ptr->pinfo[ptr->info]=new ENODE;
				InitENode(ptr->pinfo[ptr->info]);
				ptr->pinfo[ptr->info]->T.line=ptr->T.line;   

			}
			//ptr->info++; 
		}

		return;
	}

	
    
	int itemp=ptr->info;
    int iLab=0;
	ENODE *pNew=NULL;

	ptr->info=BOUND;
	
	if(ptrf->pleft && ptrf->pleft->info==BOUND)
	{
		iLab=-1;
	}

	if(ptrf->pright && ptrf->pright->info==BOUND)
	{
		iLab=1;
	}
	ptr->info=itemp; 
    
	if(iLab==0)
	{
		return;
	}
    
	if(ptrf->T.key==CN_SUB && iLab>=1)
	{
		// error  *(i-p...)
	}

	itemp=pEHead->info;
	pEHead->info=BOUND;
    
	// exist redundant codes
	if(ptrf->info==BOUND)
	{

		ptrf->info=itemp;
		if(ptrf->T.key==CN_ADD || ptrf->T.key==CN_SUB)
		{
			pNew=new ENODE;
			InitENode(pNew);//&&**??
			pNew->T.line=ptrf->T.line;    
			CopyENODE(pNew,pEHead);
		
			InitENode(pEHead);
			CopyTNODE(pEHead->T,ptr->T);  
			pEHead->info=ptr->info;

			for(int i=0;i<10 && ptr->pinfo[i];i++ )
			{
				pEHead->pinfo[i]=ptr->pinfo[i];
			}

			if(iLab<0)
			{
				pNew->pleft=pNew->pright;				 
			}

			pNew->pright=NULL;
            if(pEHead->info<10&&!pEHead->pinfo[pEHead->info])
			{
				pEHead->pinfo[pEHead->info]=pNew;
			}
			else if(pEHead->info<10)
			{
				ENODE *ptemp=new ENODE;
				InitENode(ptemp);
				ptemp->T.key=CN_ADD;
				ptemp->pleft=pEHead->pinfo[pEHead->info];
				ptemp->pright= pNew;
				pEHead->pinfo[pEHead->info]=ptemp;

			}
			
			//pEHead->info++;
			
			if(ptr)
				delete ptr;
		}
        
		return;
	}
	else
	{
		pEHead->info=itemp;
		if(ptrf->T.key==CN_ADD || ptrf->T.key==CN_SUB)
		{
			pNew=new ENODE;

			CopyENODE(pNew,pEHead);
		
			InitENode(pEHead);
			CopyTNODE(pEHead->T,ptr->T);  
			pEHead->info=ptr->info;

			for(int i=0;ptr->info<10&&ptr->pinfo[i]  ;i++ )
			{
				pEHead->pinfo[i]=ptr->pinfo[i];
			}
            
			if(iLab<0)
			{
				ptrf->pleft=ptrf->pright;				 
			}

			ptrf->pright=NULL;

			if(pEHead->info<10&&!pEHead->pinfo[pEHead->info])
			{
				pEHead->pinfo[pEHead->info]=pNew;
			}
			else if(pEHead->info<10)
			{
				ENODE *ptemp=new ENODE;
				InitENode(ptemp);
				ptemp->T.key=CN_ADD;
				ptemp->pleft=pEHead->pinfo[pEHead->info];
				ptemp->pright= pNew;
			}

			//pEHead->info++;
			if(ptr)
			delete ptr;
		}
        
		return;
	}  

}

void CExpression::SetAPIndex(ENODE *pEHead,CArray<SNODE,SNODE> &SArry)
{

	ENODE *p=pEHead;
    CList<ENODE *,ENODE *> STK;

   	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
            STK.AddTail(p);
            
			// Add action here
			if(p->T.key==CN_VARIABLE &&p->T.addr>=0&&p->T.addr<SArry.GetSize()&& (SArry[p->T.addr].kind==CN_ARRAY || SArry[p->T.addr].kind==CN_POINTER))
			{
				for(int i=0;i<10&&p->pinfo[i]  ; i++ )
				{
					p->info=i+1;
					SetAPIndex(p->pinfo[i],SArry);					
				}
			}
			// end of this part

			p=STK.GetTail()->pleft;
		}
		else
		{
			p=STK.GetTail()->pright;
			STK.RemoveTail();			
		}
	}

}

bool CExpression::LegalAddrOperation(ENODE *p,ENODE *pHead)
{
	if(!p || !pHead)
	{
		return false;
	}

	int tempp;
	bool bLegal=true;

	tempp=p->info;
	p->info=BOUND;
    
	ENODE *ptr=pHead,*ptemp=p;
    CList<ENODE *,ENODE *> STK;

    while(pHead->info!=BOUND)
	{
		ptr=pHead;
		STK.RemoveAll();
   		while(STK.GetCount()>0 || ptr)
		{
			if(ptr)
			{
				if(ptr->pleft && ptr->pleft->info==BOUND)
				{
					ptr->pleft->info=tempp;
					if(ptr->T.key!=CN_SUB && ptr->T.key!=CN_ADD)
					{
						bLegal=false;
						ptr->pleft->info=tempp;
						tempp=pHead->info;
						pHead->info=BOUND;
						break;
					}

					ptemp=ptr;
					tempp=ptr->info;
					ptr->info=BOUND; 
					break;
				}

				if(ptr->pright && ptr->pright->info==BOUND)
				{
					ptr->pleft->info=tempp;
					if(ptr->T.key!=CN_SUB && ptr->T.key!=CN_ADD)
					{
						bLegal=false;
						ptr->pright->info=tempp;
						tempp=pHead->info;
						pHead->info=BOUND;
						break;
					}
					ptemp=ptr;
					tempp=ptr->info;
					ptr->info=BOUND; 
					break;
				}

				STK.AddTail(ptr);
				ptr=STK.GetTail()->pleft;	
			}
			else
			{
				ptr=STK.GetTail()->pright;
				STK.RemoveTail();			
			}

		} // while(STK.Get

	} // while(ptemp->

	pHead->info=tempp; 
    
	return bLegal;
}

void CExpression::InitENode(ENODE *pENode)
{
	if(!pENode)
	{
		return;
	}
	else
	{
		pENode->info=0;
		pENode->T.deref=0;
		pENode->pleft=NULL;
		pENode->pright=NULL;
		pENode->T.addr=0;
		pENode->T.key=CN_CINT;
		pENode->T.line=0;
		pENode->T.name="";
		pENode->T.value=0;

		for(int i=0;i<10;i++)
		{
			pENode->pinfo[i]=NULL; 
		}		
	}

}

void CExpression::ArrayToTree(int &index,SSTACK &st,CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry)
{
	/************************************************************************
	  FUNCTION:
	  Translate the index of the array reference to a sub-syntax tree
	  PARAMETER:
	  index : current index of TEXP ,point to the array variable in TEXP;
	  st    : the element to be pushed to the syntax stack,and in this 
	          function we'll fill it with some information
	  S     : syntax stack
	  SArry : symbol table 
	************************************************************************/

		//	AfxMessageBox(PrintExp(TEXP));

	CSDGBase SN;
    
	if(index+1>TEXP.GetSize()||TEXP[index+1].key!=CN_LREC)
	{
		return;
	}

	int begpos=index;
	int ix=index+2;
	int n=1;
	int dimnum=0;
	CArray<TNODE,TNODE> TEMP;

			

	while(ix< TEXP.GetSize()-1&& n!=0 &&TEXP[index].addr>=0&&TEXP[index].addr<SArry.GetSize())
		//&&dimnum<SArry[TEXP[index].addr].arrdim)//wtt  2008.3.18 增加了指向数组的指针的处理
	{

		//AfxMessageBox("OK");
		if(TEXP[ix].key==CN_LREC)
		{
			n++;
		}

		if(TEXP[ix].key==CN_RREC)
		{
			n--;
			if(ix-1>=0&&TEXP[ix-1].key==CN_LREC)//wtt 2008.3.31缺省的数组大小写为32767
			{
				TNODE dim;
				
				dim.addr=32767;
				dim.deref=0;//wtt 2008 3.19
				dim.paddr=-1;
				dim.key=CN_CINT;
				dim.line=TEXP[ix].line;
				dim.value=0;
				dim.name="";
				
				TEMP.Add(dim);


			}

		}
        
        TEMP.Add(TEXP[ix]);

				

		if(n==0)
		{
			if(TEMP.GetSize()>0&&TEMP[TEMP.GetSize()-1].key==CN_RREC)
			{
				TEMP.RemoveAt(TEMP.GetSize()-1);
			}

			// error: empty index;
            if(TEMP.GetSize()<=0)
			{
			//	AfxMessageBox("ER.ID=9");/////////////
				ER.ID=9;
				ER.line=TEXP[0].line;
				m_pError->Add(ER);
			}
		//	AfxMessageBox(PrintExp(TEMP));
			AExpToTree(TEMP,&SN,SArry);
			
			TEMP.RemoveAll();
			st.pEN->pinfo[dimnum]=SN.m_pinfo;
			st.pEN->T.value+=CN_MULTI;
			dimnum++;

			if(ix<TEXP.GetSize()-1 && TEXP[ix+1].key==CN_LREC)
			{
				n=1;
				ix++;
			}
			
		}
		ix++;
	}

	if(n>=1)
	{
		// Array reference error;
		//		AfxMessageBox("ER.ID=9");/////////////

		ER.ID=9;
		ER.line=TEXP[ix].line;
		m_pError->Add(ER);
		while(ix< TEXP.GetSize()-1 && n!=0 )
		{
			if(TEXP[ix].key==CN_LREC)
			{
				n++;
			}

			if(TEXP[ix].key==CN_RREC)
			{
				n--;
			}
			ix++;
            
			if(ix<TEXP.GetSize()-1 && TEXP[ix+1].key==CN_LREC)
			{
				n=1;
				ix++;
			}			
		}
	}
	
    /* 
	if(dimnum<SArry[TEXP[index].addr].arrdim) //ix>=TEXP.GetSize())
	{
		// syntax error in array reference
		// set all the indexs to "0"
        
		int dim=SArry[TEXP[index].addr].arrdim;  
		
		for(int i=dimnum;i<dim&&i<10;i++)
		{
			ENODE *pe=new ENODE;
			pe->info=0;
			pe->pleft=NULL;
			pe->pright=NULL;
			for(int k=0;k<10;k++)
			{
				pe->pinfo[i]=NULL; 
			}
			pe->T.key=CN_CINT;
			pe->T.addr=0;
			pe->T.line=-1;
			pe->T.name="";
			st.pEN->pinfo[i]=pe;
		}

        dimnum=SArry[TEXP[index].addr].arrdim;
		st.pEN->info=dim;
	}
    */
	
	st.pEN->info=dimnum;
	
	index=ix-1;
	for(int i=index;i>begpos;i--)
	{
		TEXP.RemoveAt(i); 
	}

	index=begpos;

	return;
   
	/***********************************************************/
}
void CExpression::FunctionToTree(int &index,SSTACK &st,CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry)
{
	/************************************************************************
	  FUNCTION:
	  Translate the parameters of the basical function call to a sub-syntax tree
	  PARAMETER:
	  index : current index of TEXP ,point to the array variable in TEXP;
	  st    : the element to be pushed to the syntax stack,and in this 
	          function we'll fill it with some information
	  S     : syntax stack
	  SArry : symbol table 
	************************************************************************/
//    extern CString PrintExp(CArray<TNODE,TNODE> &T);

	CSDGBase SN;
    
	#ifdef DEBUG
//	AfxMessageBox("enter into function tree:"+TEXP[index].name +PrintExp(TEXP));
	#endif

	if(index+1<TEXP.GetSize()-1 && TEXP[index+1].key!=CN_LCIRCLE)
	{
		return;
	}

	int ix=index+2;
	int n=1;
	int parnum=0;
	CArray<TNODE,TNODE> TEMP;
	while(ix< TEXP.GetSize()-1 && n!=0)
	{
		if(TEXP[ix].key==CN_LCIRCLE)
		{
			n++;
		}

		if(TEXP[ix].key==CN_RCIRCLE)
		{
			n--;
		}
		       
        TEMP.Add(TEXP[ix]);

		if(TEXP[ix].key==CN_DOU || n==0 )
		{
			if((TEMP.GetSize()>0 && TEMP[TEMP.GetSize()-1].key==CN_DOU)||(n==0))
			{
				TEMP.RemoveAt(TEMP.GetSize()-1);
			}
            
			#ifdef DEBUG
//			AfxMessageBox("pre TEMP:"+PrintExp(TEMP));
			#endif

			if(TEMP.GetSize()<=0)
			{
				ER.ID=20;
				ER.line=TEXP[0].line;
				m_pError->Add(ER);
			}
			AExpToTree(TEMP,&SN,SArry);
			TEMP.RemoveAll(); 
            
			#ifdef DEBUG
//			AfxMessageBox("end TEMP:"+PrintExp(TEMP));
			#endif

			st.pEN->pinfo[parnum]=SN.m_pinfo;
			parnum++;
			
			/*if(TEXP[ix].key==CN_DOU)
			  {
				ix++;
				continue;
			  }
			*/

			if(n==0 )
			{
				ix++;
				break;
			}
			
		}
		ix++;
	}

	#ifdef DEBUG
//	CString str,s;
//	str.Format("index=%d,ix=%d,TEXPlen=%d ,parnum=%d",index,ix,TEXP.GetSize()-1,parnum) ;
//	AfxMessageBox(str);
	#endif

	if(ix<=TEXP.GetSize()-1 && parnum>0)
	{		
		st.pEN->info=parnum;
         
    	// 在此可以做库函数调用的正确性检查
		// 待功能扩充时再加入 
						
	}
	index=ix-1;
	return;
   
	/***********************************************************/
		
}

int  CExpression::GetPrior(const int n1,const int n2)
{
	// get the prior value 

	int m1,m2;
	char op;
	
	if(n1==CN_EQUAL)
	{
		return 2;
	}

	if(n2==CN_LINE)
	{
		return 1;
	}

    switch(n1)
	{
	case CN_ADD:
		m1=1;
		break;
	case CN_SUB:
		m1=2;
		break;
	case CN_MULTI:
	//case CN_FORMAT://wtt2.21
		m1=3;
		break;
	case CN_DIV:
		m1=4;
		break;
    case CN_LCIRCLE:
	    m1=5;
	    break;
	case CN_RCIRCLE:
	    m1=6;
	    break;
	case CN_DFUNCTION:
	case CN_BFUNCTION:
	case CN_VARIABLE:
	case CN_CINT:
	case CN_CLONG:
	case CN_CFLOAT:
	case CN_CCHAR:
	case CN_CSTRING:
	    m1=7;
	    break;
	case CN_FORMAT://wtt
		m1=8;
	    break;
	case CN_AND://wtt 2006.3.21指针引用
		m1=8;
	    break;
	default:
	   return -1;
	}
   
	switch(n2)
	{
	case CN_ADD:
		m2=1;
		break;
	case CN_SUB:
		m2=2;
		break;
	case CN_MULTI:
	//case CN_FORMAT://wtt
		m2=3;
		break;
	case CN_DIV:
		m2=4;
		break;
    case CN_LCIRCLE:
	    m2=5;
	    break;
	case CN_RCIRCLE:
	    m2=6;
	    break;
	case CN_DFUNCTION:
	case CN_BFUNCTION:
	case CN_VARIABLE:
	case CN_CINT:
	case CN_CLONG:
	case CN_CFLOAT:
	case CN_CCHAR:
	case CN_CSTRING:
	    m2=7;
	    break;
	case CN_FORMAT://wtt
		m2=8;
	    break;
	default:
	   return -1;
	}
    
	op=OPGARRAY[m1][m2];
    if(op=='>')
		return 1;
	if(op=='<')
		return 2;
	if(op=='=')
		return 0;
    else
		return -1;
}




int CExpression::AOperator(TNODE T)
{
	if(T.key==CN_FORMAT|| T.key>=CN_ADD&&T.key<=CN_DIV)
	{
		return T.key; 
	}
	return -1;
}


                            /******************************
                                the above function group:
							translate arithmetic expression
							to synax tree.
                            ******************************/

//@@@@@@@@@@@@@#############$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&***********//

							




//@@@@@@@@@@@@@#############$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&***********//

							/******************************
                               the following function group:
							standard the expression based on
							synax tree.
                            ******************************/



ENODE* CExpression::BoolExpStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/*********************************************************************************

		wtt编写							布尔表达式的标准化算法
	 *********************************************************************************/
   extern CString GetExpString(ENODE*pHead,CArray<SNODE,SNODE> &SArry);

	if(pHead==NULL)
	{
		return pHead;
	}
	b_boolmodified=true;
	while(b_boolmodified)
	{
		b_boolmodified=false;
	
		ENODE *pnode=new ENODE;
		for(int i=0;i<10;i++)
		{
			pnode->pinfo[i]=NULL; 
		}
		pnode->info=0;
//		pnode->T.deref=0;
		pnode->pright=NULL;
		pnode->pleft=pHead;
		pnode->T.key=CN_SUB;
		pHead=pnode;
	//	AfxMessageBox("standard:"+GetExpString(pHead));////////
	

		BExpStandardRule(pHead,SArry);
		pHead=pHead->pleft;
		if(pnode)
		delete pnode;
		
	}
	
	
	return pHead;

}

void   CExpression::BExpStandardRule(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		布尔表达式标准化规则,wtt编写3.4
	    
		PARAMETER:
		pHead : Head of arithmetic expression.
		SArry : ...
	*******************************************************/
   extern CString GetExpString(ENODE*pHead,CArray<SNODE,SNODE> &SArry);
	 	
	CList<ENODE *,ENODE *> STK;
	ENODE *p=NULL;
	
	if(pHead==NULL)
	{
		return ;
	}
   
	p=pHead;
    int num=0;
	int label=0;
	ENODE *ptemp=NULL;
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
		
		//	if((BOperator(p->T)==2||BOperator(p->T)==3||BOperator(p->T)==-5||BOperator(p->T)==-6)&&p->pright&&p->pleft&&(p->pleft->T.key==CN_CINT)))

			if((BOperator(p->T)==2||BOperator(p->T)==3||BOperator(p->T)==-5||BOperator(p->T)==-6)&&p->pright&&p->pleft&&(p->pleft->T.key==CN_CINT)&&(p->pright->T.key!=CN_CINT))//wtt 20090615

			{//Rule ：constant && || == != expression
				
				ENODE *pf=NULL;
				pf=p->pleft;
				p->pleft=p->pright;
				p->pright=pf;
				pf=NULL;
				b_boolmodified=true;
				
			
			}
	
		
		 if(BOperator(p->T )==1&&p->pleft&&BOperator(p->pleft->T )==1&&p->pright==NULL&&p->pleft->pright==NULL)
			{//Rule:!(!E1)→E1；
			  ENODE *pf=NULL;
			  pf=p->pleft;	
			 
			  if(p->pright )
			  delete p->pright;
			  p->pright=NULL;			  
			  
			  CopyENODE(p,pf->pleft);			  
			  if(pf) 
			  delete pf;
			  b_boolmodified=true;						

			}

		 // if(BOperator(p->T)==-5&&p->pleft&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
		  if(BOperator(p->T)==-5&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
		 {//Rule 18：E1 != 0→E1；
			//AfxMessageBox("0 != E1→E1");	
			   if(p->pleft &&p->pleft->T.key==CN_VARIABLE&&p->pleft->T.addr>=0&&p->pleft->T.addr<SArry.GetSize()&&(SArry[p->pleft->T.addr].type==CN_FLOAT||SArry[p->pleft->T.addr].type==CN_DOUBLE) )
			  {//若是变量是算数判断则不转换
			  }
		     else
			 {	
			   ENODE *pf=NULL;
			   pf=p->pleft;
			   if(p->pright )
			      delete p->pright;
			   p->pright=NULL;			  

			   CopyENODE(p,pf);
			   if(pf)
			     delete pf;
			   b_boolmodified=true;
				
			 }
			}
//		  if(BOperator(p->T)==-6&&p->pleft&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
			  if(BOperator(p->T)==-6&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
			  {//Rule 18：E1== 0 →!E1；
			   if(p->pleft &&p->pleft->T.key==CN_VARIABLE&&p->pleft->T.addr>=0&&p->pleft->T.addr<SArry.GetSize()&&(SArry[p->pleft->T.addr].type==CN_FLOAT||SArry[p->pleft->T.addr].type==CN_DOUBLE) )
			   {//若是变量是算数判断则不转换
			   }
		      else
			  {				 
		
			    p->T.key=CN_NOT;
		      /*	if(p->pright )
		        	delete p->pright; //wtt 05.10.24*/
		        	p->pright=NULL;					
		        	b_boolmodified=true;
			  }	
				

			}
	//	  if(BOperator(p->T)==3&&p->pleft&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==1)
		  if(BOperator(p->T)==3&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==1)
		  {//Rule 6：E1 ||1 →1；
		
			 
			  p->T.key=CN_CINT;
			  p->T.addr=1; 
			  p->T.deref=0;//wtt 2008.3.19
		 	if(p->pleft  )
			  delete p->pleft ;
			if(p->pright )
				delete p->pright ;
			  p->pleft =NULL;
			  p->pright=NULL; 		
			  b_boolmodified=true;		
				

			}
	  if(BOperator(p->T)==3&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
		  {//Rule 6：E1 ||0 →E1；//6.4//
		
			 
			  ENODE *pf=NULL;
			  pf=p->pleft;	

			  if(p->pright )
				  delete p->pright;
			  p->pright=NULL; 

			  CopyENODE(p,pf);	
			  if(pf )
			  delete pf;
			  pf=NULL;
			  b_boolmodified=true;	
				

			}
		//  if(BOperator(p->T)==2&&p->pleft&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
       	  if(BOperator(p->T)==2&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
		  {//Rule 7： E1&&0 →0；
		
			 
			  p->T.key=CN_CINT;
			  p->T.addr=0;
			  p->T.deref=0;//wtt 2008.3.19
			if(p->pleft )
			  delete p->pleft;
			//if (p->pright )//////////wtt  2007.11.17
		    //	delete p->pright;//////////wtt  2007.11.17
			  p->pleft =NULL;
			  p->pright=NULL; 		
			  b_boolmodified=true;				

			}
		   if(BOperator(p->T)==2&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==1)
		  {//Rule 7： E1&&1 →E1；//6.4//
		
			 
			 ENODE *pf=NULL;
			  pf=p->pleft;	

			  if(p->pright )
				  delete p->pright;
			  p->pright=NULL; 

			  CopyENODE(p,pf);	
			  if(pf )
			  delete pf;
			  pf=NULL;
			  b_boolmodified=true;				

			}
		 	  if((BOperator(p->T)==3||BOperator(p->T)==2)&&p->pleft&&p->pright&&CompareTrees(p->pleft,p->pright,SArry)  )
		  {//Rule 1：E1 || E1→E1；Rule 2：E1 && E1→E1；
//			  AfxMessageBox("E1||E1");
			  ENODE *pf=NULL;
			  pf=p->pleft;	

			  //if(p->pright )
			//	  delete p->pright;
			  p->pright=NULL; 

			  CopyENODE(p,pf);	
			 // if(pf )
			 // delete pf;
			  pf=NULL;
			  b_boolmodified=true;

		  }
         if((BOperator(p->T)==3||BOperator(p->T)==2)&&p->pright&&BOperator(p->pright->T)==1&&CompareTrees(p->pleft,p->pright->pleft,SArry))
		  {//Rule 3：E1 && (! E1)→0；Rule 4：E1 || (! E1)→1；


			  if(p->T.key==CN_DAND)
				  p->T.addr=0;
			  else
				  p->T.addr=1;
			  p->T.key=CN_CINT;
			  p->T.deref=0;//wtt 2008.3.19
			  p->pleft=NULL;
			  p->pright=NULL;
			  b_boolmodified=true;


		  }
	 
		  
		  /////////////////////////////////////////////////////////////
		  if(p->T.key==CN_LESS || p->T.key==CN_LANDE )
			{//Rule 20：A2<=A1→A1>=A2，A2<A1→A1>A2；
			  ENODE *ptemp;
					
			  ptemp=p->pleft;
			  p->pleft=p->pright;
			  p->pright=ptemp;

			if(p->T.key==CN_LESS)
			{
				p->T.key=CN_GREATT;
			}
			else
			{
				p->T.key=CN_BANDE;
			}
				b_boolmodified=true;
		  }
		  if((BOperator(p->T)==2 && p->pleft!=NULL && BOperator(p->pleft->T)==3))
			{//Rule ：(E1 || E2) && E3→(E1 && E3) || (E2 && E3)；

	//	AfxMessageBox("distribution1");


				
			//	p->info=MAX; 
			//	pf=SelectFather(p,pf,ptemp);
			//	if(pf)
			//	AfxMessageBox(C_ALL_KEYWORD[pf->T.key]);

				//p=LeftChildStandard(pf,p);
				p->T.key=CN_DOR;
				ENODE* pright=p->pright;
				p->pleft->T.key=CN_DAND;
				ENODE*pnew=new ENODE;
				pnew->info=0;
//				pnew->T.deref=0;
				pnew->pleft=NULL;
				pnew->pright=NULL;
				for(int i=0;i<10;i++)
				{
					pnew->pinfo[i]=NULL;
				}
				pnew->T.key=CN_DAND;
				
				pnew->pleft=p->pleft->pright;
				pnew->pright=CopyTree(pnew->pright,pright);				
				p->pright=pnew;				 
				p->pleft->pright=pright;
				b_boolmodified=true;	

			

			}
            
			// 2.the second standard type of two
		  if( (BOperator(p->T)==2 && p->pright!=NULL && BOperator(p->pright->T)==3))

			{//Rule ：E1 && (E2 || E3)→(E1 && E2) || (E1 && E3)；
			//	AfxMessageBox("distribution2");
                
				p->T.key=CN_DOR;
				ENODE* pleft=p->pleft;
				p->pright->T.key=CN_DAND;
				ENODE*pnew=new ENODE;
				pnew->info=0;
//				pnew->T.deref=0;
				pnew->pleft=NULL;
				pnew->pright=NULL;
				for(int i=0;i<10;i++)
				{
					pnew->pinfo[i]=NULL;
				}
				pnew->T.key=CN_DAND;
				
				pnew->pright=p->pright->pleft;
				pnew->pleft=CopyTree(pnew->pleft,pleft);
				
				p->pleft=pnew;
				 
				p->pright->pleft=pleft;
				b_boolmodified=true;
			
		
			}	
			
			////////////////wtt/////////3.2///3.3/////////////////
		  if(BOperator(p->T)==3 && p->pright &&BOperator(p->pright->T)==3||BOperator(p->T)==2 && p->pright &&BOperator(p->pright->T)==2)
			{//E1||(E2||E3)→E1||E2||E3 E1&&(E2&&E3)→(E1&&E2)&&E3


			
				ENODE *pright=NULL;
				                
			
				pright=p->pright; 

				p->T.key=pright->T.key;	
				if(BOperator(p->T)==3 )
					pright->T.key=CN_DOR; 
				else
					pright->T.key=CN_DAND; 

				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;

			}
		 if(BOperator(p->T)==1 && p->pleft!=NULL &&( BOperator(p->pleft->T)==3||BOperator(p->pleft->T)==2))
			{//Rule ：Rule 16：! (E1 || E2)→! E1 && ! E2；Rule 17：! (E1 && E2)→! E1 || ! E2；
		
			//AfxMessageBox(GetExpString(p));///
				
				if(p->pleft->T.key==CN_DOR)
					p->T.key=CN_DAND;
				else
					p->T.key=CN_DOR;

				p->pleft->T.key=CN_NOT;

				ENODE*pnew=new ENODE;
				pnew->info=0;
//				pnew->T.deref=0;
				pnew->pleft=NULL;
				pnew->pright=NULL;
				for(int i=0;i<10;i++)
				{
					pnew->pinfo[i]=NULL;
				}
				pnew->T.key=CN_NOT;

				
				pnew->pleft=p->pleft->pright;

				p->pleft->pright=NULL;
				p->pright=pnew;

				b_boolmodified=true;	

			

			}

			 if(BOperator(p->T)==1 && p->pleft!=NULL &&( BOperator(p->pleft->T)==-5||BOperator(p->pleft->T)==-6))
			{//Rule 21：! (A1==A2)→A1!=A2，! ( A1!=A2)→A1==A2，
		
			
				
				if(p->pleft->T.key==CN_DEQUAL)
					p->T.key=CN_NOTEQUAL;
				else
					p->T.key=CN_DEQUAL;
				ENODE* pleft;
				pleft=p->pleft;			
				p->pleft=pleft->pleft ;

				p->pright=pleft->pright ;

				b_boolmodified=true;				

			}
			
			if(BOperator(p->T)==1 && p->pleft!=NULL &&( BOperator(p->pleft->T)==-1||BOperator(p->pleft->T)==-4))
			{//Rule 22：! (A1>=A2)→A2>A1，! ( A1>A2)→A2>=A1，
		
			
				
				if(p->pleft->T.key==CN_GREATT)
					p->T.key=CN_BANDE;
				else
					p->T.key=CN_GREATT;
				ENODE* pleft;
				pleft=p->pleft;			
				p->pleft=pleft->pright ;

				p->pright=pleft->pleft ;

				b_boolmodified=true;				

			}

		if(BOperator(p->T)==3 && p->pleft!=NULL&&p->pright !=NULL&&( BOperator(p->pleft->T)==-1||BOperator(p->pleft->T)==-4)&&BOperator(p->pright->T)==-6&&CompareTrees(p->pleft->pleft,p->pright->pleft,SArry )&&CompareTrees(p->pleft->pright,p->pright->pright,SArry ))
			{//Rule:A1>A2||A1==A2→A1>=A2, A1>=A2||A1==A2→A1>=A2；
			 ENODE *pf=NULL;
			  pf=p->pleft;
			  pf->T.key=CN_BANDE;

			  if(p->pright )
				  delete p->pright;
			  p->pright=NULL; 

			  CopyENODE(p,pf);
			if(pf)
			  delete pf;
			pf=NULL;
			  b_boolmodified=true;    
			}
			if(BOperator(p->T)==3 && p->pleft!=NULL&&p->pright !=NULL&& BOperator(p->pleft->T)==-5&&BOperator(p->pright->T)==-6&&CompareTrees(p->pleft->pleft,p->pright->pleft,SArry )&&CompareTrees(p->pleft->pright,p->pright->pright,SArry ))
			{//Rule 24：A1!=A2 || A1==A2→1；
			 p->T.key=CN_CINT;
			 p->T.addr=1; 	
			 p->T.deref=0;//wtt 2008.3.19
			if(p->pright )
			 delete p->pright;			 
			if(p->pleft )
			delete p->pleft ;
			 p->pleft=NULL;
			 p->pright=NULL; 
			 b_boolmodified=true;    
			}
			if(BOperator(p->T)==2 && p->pleft!=NULL&&p->pright !=NULL&& BOperator(p->pleft->T)<0&&BOperator(p->pright->T)<0&&p->pleft->T.key!=p->pright->T.key &&CompareTrees(p->pleft->pleft,p->pright->pleft,SArry )&&CompareTrees(p->pleft->pright,p->pright->pright,SArry ))
			{
			 if(InSet(BOperator(p->pleft->T),BOperator(p->pright->T))==1||InSet(BOperator(p->pright->T),BOperator(p->pleft->T))==1)
			 {//Rule ：A1@A2&&A1#A2→0；(@,#)或(#,@)是{(==,!=),(>,==)}中的元素。
				p->T.key=CN_CINT;
			   p->T.addr=0; 
			   p->T.deref=0;//wtt 2008.3.19
			  if(p->pright )
			    delete p->pright;			 
			  if(p->pleft )
			   delete p->pleft ;
			    p->pleft=NULL;
			   p->pright=NULL; 
			   b_boolmodified=true;
			 }
			 else if(InSet(BOperator(p->pleft->T),BOperator(p->pright->T))==2)
			 {//Rule ：A1>=A2&& A1>A2→A1>A2；A1>=A2&& A1==A2→A1==A2；
			  
			  ENODE *pf=NULL;
			  pf=p->pright;	

			  if(p->pleft )
				  delete p->pleft;
			  p->pleft=NULL; 

			  CopyENODE(p,pf);	
			  if(pf )
			  delete pf;
			  pf=NULL;
			  b_boolmodified=true;
			 }
			 else if(InSet(BOperator(p->pright->T),BOperator(p->pleft->T))==2)
			 {	 //Rule ：A1>A2&& A1>=A2→A1>A2；	A1==A2&& A1>=A2→A1==A2；		  
			  ENODE *pf=NULL;
			  pf=p->pleft;	

			  if(p->pright )
				  delete p->pright;
			  p->pright=NULL; 

			  CopyENODE(p,pf);	
			  if(pf )
			  delete pf;
			  pf=NULL;
			  b_boolmodified=true;			 
			 }
			}
			if(BOperator(p->T)==2 && p->pleft!=NULL&&p->pright !=NULL&& BOperator(p->pleft->T)==-4&&BOperator(p->pright->T)==-4&&CompareTrees(p->pleft->pleft,p->pright->pright,SArry )&&CompareTrees(p->pleft->pright,p->pright->pleft,SArry ))
			{//Rule 25：A1@A2&& A1#A2→0；
				ENODE *pf=NULL;
			  pf=p->pleft;	

			  if(p->pright )
				  delete p->pright;
			  p->pright=NULL; 

			  CopyENODE(p,pf);	
			  p->T.key=CN_DEQUAL;
			  if(pf )
			  delete pf;
			  pf=NULL;
			  b_boolmodified=true;

			}
			/////////////////////////////////////yuanlai  zhu diao //
		/*	if((BOperator(p->T)==2||BOperator(p->T)==3) && p->pleft!=NULL&&p->pright !=NULL &&CompareTrees(p->pleft->pleft,p->pright->pright,SArry )&&CompareTrees(p->pleft->pright,p->pright->pleft,SArry ))
			{//Rule 28：A1@A2#A2$A1→A1@A2#A1$A2；#为||或&&，@与$是关系运算符，它们可能相同也可能不同
			
				ENODE *pf=NULL;
				pf=p->pright->pright;
				p->pright->pright=p->pright->pleft;
				p->pright->pleft=pf;
				pf=NULL;


			  b_boolmodified=true;

			}*/
			/////////////////////////////////////////////////////////
				if((BOperator(p->T)==3||BOperator(p->T)==2) && p->pleft!=NULL&&p->pright !=NULL&&( BOperator(p->pleft->T)==-1||BOperator(p->pleft->T)==-4)&&( BOperator(p->pright->T)==-1||BOperator(p->pright->T)==-4)&&CompareTrees(p->pleft->pleft,p->pright->pleft,SArry ))
			{//Rule 26：A1>(或>=)c1 && A1>(或>=)c2→A1>(或>=)c2；c1、c2为常数且c1<=c2 Rule 27：A1>(或>=)c1 || A1>(或>=)c2→A1>(或>=)c1；c1、c2为常数且c1<=c2

				ENODE*pbig,*psmall;
				if(p->pleft->pright&&p->pright->pright&&( p->pleft->pright->T.key==CN_CINT|| p->pleft->pright->T.key==CN_CCHAR||p->pleft->pright->T.key==CN_CFLOAT||p->pleft->pright->T.key==CN_CDOUBLE)&&( p->pright->pright->T.key==CN_CINT||p->pright->pright->T.key==CN_CCHAR||p->pright->pright->T.key==CN_CFLOAT||p->pright->pright->T.key==CN_CDOUBLE) )
				{
						float lvalue,rvalue;
					if(p->pleft->pright->T.key==CN_CINT|| p->pleft->pright->T.key==CN_CCHAR)
					{
						lvalue=float(p->pleft->pright->T.addr);
					}
					else if(p->pleft->pright->T.key==CN_CFLOAT||p->pleft->pright->T.key==CN_CDOUBLE)
					{
						lvalue=float(p->pleft->pright->T.value);

					}
					if( p->pright->pright->T.key==CN_CINT||p->pright->pright->T.key==CN_CCHAR)
					{
						rvalue=float(p->pright->pright->T.addr);  
					}
					else if(p->pright->pright->T.key==CN_CFLOAT||p->pright->pright->T.key==CN_CDOUBLE)
					{
						rvalue=float(p->pright->pright->T.value) ;   
					}
					if(lvalue>rvalue)
					{
						pbig=p->pleft;
						psmall=p->pright; 

					}
					else
					{
						pbig=p->pright;
						psmall=p->pleft; 
					}
					if(p->T.key==CN_DAND  )
					{
						if(lvalue>rvalue)
						{
						
							p->pright=NULL; 
						}
						else
						{
						
							p->pleft=NULL; 
						}
						if(psmall)
						delete psmall;
						CopyENODE(p,pbig);
						if(pbig)
						delete pbig;
						pbig=NULL;
						b_boolmodified=true; 

					}
					else if(p->T.key==CN_DOR)
					{
						if(rvalue>lvalue)
						{
						
							p->pright=NULL; 
						}
						else
						{
							
							p->pleft=NULL; 
						}
						if(pbig)
						delete pbig;
						CopyENODE(p,psmall);
						if(psmall)
						delete psmall;
						b_boolmodified=true;

					}
				}
			}
			///////////////////////////////////////////////
			if((BOperator(p->T)==3||BOperator(p->T)==2) && p->pleft!=NULL&&p->pright !=NULL&&( BOperator(p->pleft->T)==-1||BOperator(p->pleft->T)==-4)&&( BOperator(p->pright->T)==-1||BOperator(p->pright->T)==-4)&&CompareTrees(p->pleft->pright,p->pright->pright,SArry ))
			{//Rule 26：c1>(或>=)A1 && c2>(或>=)A1→c1>(或>=)A1；c1、c2为常数且c1<=c2 Rule 27：c1>(或>=)A1 || c2>(或>=)A1→c2>(或>=)A1；c1、c2为常数且c1<=c2
               
				ENODE*pbig,*psmall;
				if(p->pleft->pleft&&p->pright->pleft&&( p->pleft->pleft->T.key==CN_CINT|| p->pleft->pleft->T.key==CN_CCHAR||p->pleft->pleft->T.key==CN_CFLOAT||p->pleft->pleft->T.key==CN_CDOUBLE)&&( p->pright->pleft->T.key==CN_CINT||p->pright->pleft->T.key==CN_CCHAR||p->pright->pleft->T.key==CN_CFLOAT||p->pright->pleft->T.key==CN_CDOUBLE) )
				{
						float lvalue,rvalue;
					if(p->pleft->pleft->T.key==CN_CINT|| p->pleft->pleft->T.key==CN_CCHAR)
					{
						lvalue=float(p->pleft->pleft->T.addr);
					}
					else if(p->pleft->pleft->T.key==CN_CFLOAT||p->pleft->pleft->T.key==CN_CDOUBLE)
					{
						lvalue=float(p->pleft->pleft->T.value);

					}
					if( p->pright->pleft->T.key==CN_CINT||p->pright->pleft->T.key==CN_CCHAR)
					{
						rvalue=float(p->pright->pleft->T.addr);  
					}
					else if(p->pright->pleft->T.key==CN_CFLOAT||p->pright->pleft->T.key==CN_CDOUBLE)
					{
						rvalue=float(p->pright->pleft->T.value) ;   
					}
					if(lvalue>rvalue)
					{
						pbig=p->pleft;
						psmall=p->pright; 

					}
					else
					{
						pbig=p->pright;
						psmall=p->pleft; 
					}
					if(p->T.key==CN_DOR  )
					{
						if(lvalue>rvalue)
						{
							
							p->pright=NULL; 
						}
						else
						{
							
							p->pleft=NULL; 

						}
						if(psmall)
							delete psmall;
						psmall=NULL;
						CopyENODE(p,pbig);
						if(pbig)
						delete pbig;
						b_boolmodified=true; 

					}
					else if(p->T.key==CN_DAND)
					{
						if(rvalue>lvalue)
						{
						
							//should add	delete p->pright branch; 
						
							p->pright=NULL; 
						}
						else
						{
							//	delete p->pleft branch; 
							p->pleft=NULL; 

						}
						
						CopyENODE(p,psmall);
						if(psmall)
						delete psmall;
						b_boolmodified=true;

					}
				}
			}

			///////////////////////////////////////////////
		if((BOperator(p->T)==2) && p->pleft!=NULL&&p->pright !=NULL&&(BOperator(p->pleft->T)==2)&&( BOperator(p->pright->T)==-1||BOperator(p->pright->T)==-4))
			{//Rule 26：多个A1>(或>=)c1 && A1>(或>=)c2→A1>(或>=)c2；c1、c2为常数且c1<=c2 
               // AfxMessageBox("initial:"+GetExpString(p));////
	
				ENODE *pleft=NULL,*pright=NULL,*ptemp;
				
				ptemp=p;
				pright=ptemp->pright;
				do
				{
				pleft=ptemp->pleft;

				
				if(pright&&pleft&&pleft->pright&&( BOperator(pleft->pright->T)==-1||BOperator(pleft->pright->T)==-4))
				{
				//	AfxMessageBox("need transform:"+GetExpString(ptemp));//////////
					if(pleft->pright->pright&&pleft->pright->pleft&&pright->pright&&pright->pleft&& CompareTrees(pleft->pright->pright,pright->pright,SArry ))
					{
						float lvalue,rvalue;
						if(pleft->pright->pleft->T.key==CN_CINT|| pleft->pright->pleft->T.key==CN_CCHAR)
						{
							lvalue=float(pleft->pright->pleft->T.addr);
						}
						else if(pleft->pright->pleft->T.key==CN_CFLOAT||pleft->pright->pleft->T.key==CN_CDOUBLE)
						{
							lvalue=float(pleft->pright->T.value);

						}
						if( pright->pleft->T.key==CN_CINT||pright->pleft->T.key==CN_CCHAR)
						{
							rvalue=float(pright->pleft->T.addr);  
						}
						else if(pright->pleft->T.key==CN_CFLOAT||pright->pleft->T.key==CN_CDOUBLE)
						{
							rvalue=float(pright->pleft->T.value) ;   
						}
						if(lvalue>rvalue)
						{
														
							ptemp->pleft=pleft->pleft;
							//add delde exptree pleft
                            // AfxMessageBox("transformed1:"+GetExpString(p));////
								b_boolmodified=true; 

						}
						else
						{
							
							//should add	delete ptemp->pright branch;
							ptemp->pright=NULL; 
							CopyENODE(ptemp,pleft);
							pright=ptemp->pright;

								//add delde exptree pleft and pright
						if(pleft)
						delete pleft;
						 // AfxMessageBox("transformed2:"+GetExpString(p));////

						b_boolmodified=true; 
						break;
						}
					}
	


					if(pleft->pright->pright&&pleft->pright->pleft&&pright->pright&&pright->pleft&& CompareTrees(pleft->pright->pleft,pright->pleft,SArry ))
					{
					//	AfxMessageBox(GetExpString(pleft->pright->pleft));
					//	AfxMessageBox(GetExpString(pright->pleft));

						//AfxMessageBox("need");////////
						float lvalue,rvalue;
						if(pleft->pright->pright->T.key==CN_CINT|| pleft->pright->pright->T.key==CN_CCHAR)
						{
							lvalue=float(pleft->pright->pright->T.addr);
						}
						else if(pleft->pright->pright->T.key==CN_CFLOAT||pleft->pright->pright->T.key==CN_CDOUBLE)
						{
							lvalue=float(pleft->pright->pright->T.value);

						}
						if( pright->pright->T.key==CN_CINT||pright->pright->T.key==CN_CCHAR)
						{
							rvalue=float(pright->pleft->T.addr);  
						}
						else if(pright->pright->T.key==CN_CFLOAT||pright->pright->T.key==CN_CDOUBLE)
						{
							rvalue=float(pright->pleft->T.value) ;   
						}
						if(rvalue>lvalue)
						{
							
							ptemp->pleft=pleft->pleft;
							//add delde exptree pleft
							          //      AfxMessageBox("transformed3:"+GetExpString(p));////

								b_boolmodified=true; 

						}
						else
						{
							
								//should add	delete ptemp->pright branch;
							ptemp->pright=NULL; 
							CopyENODE(ptemp,pleft);
							pright=ptemp->pright;
						

								//add delde exptree pleft and pright
						if(pleft)
						delete pleft;
						        //        AfxMessageBox("transformed4:"+GetExpString(p));////

						b_boolmodified=true; 
						break;
						}

					}
				
				}

				if(pleft->pleft&&( BOperator(pleft->pleft->T)==-1||BOperator(pleft->pleft->T)==-4))
				{
					if(pleft->pleft->pright&&pleft->pleft->pleft&&pright->pright&&pright->pleft&& CompareTrees(pleft->pleft->pright,pright->pright,SArry ))
					{
						float lvalue,rvalue;
						if(pleft->pleft->pleft->T.key==CN_CINT|| pleft->pleft->pleft->T.key==CN_CCHAR)
						{
							lvalue=float(pleft->pleft->pleft->T.addr);
						}
						else if(pleft->pleft->pleft->T.key==CN_CFLOAT||pleft->pleft->pleft->T.key==CN_CDOUBLE)
						{
							lvalue=float(pleft->pleft->T.value);

						}
						if( pright->pleft->T.key==CN_CINT||pright->pleft->T.key==CN_CCHAR)
						{
							rvalue=float(pright->pleft->T.addr);  
						}
						else if(pright->pleft->T.key==CN_CFLOAT||pright->pleft->T.key==CN_CDOUBLE)
						{
							rvalue=float(pright->pleft->T.value) ;   
						}
						if(lvalue>rvalue)
						{
							
							ptemp->pleft=pleft->pright;
							//add delde exptree pleft
							//  AfxMessageBox("transformed5:"+GetExpString(p));////

								b_boolmodified=true; 

						}
						else
						{
								//should add	delete ptemp->pright branch;
						
							ptemp->pright=NULL; 
							CopyENODE(ptemp,pleft);
							pright=ptemp->pright;
							
								//add delde exptree pleft and pright
						if(pleft)
						delete pleft;
						//  AfxMessageBox("transformed6:"+GetExpString(p));////

						b_boolmodified=true; 
						break;

						}
					}




					if(pleft->pleft->pright&&pleft->pleft->pleft&&pright->pright&&pright->pleft&& CompareTrees(pleft->pleft->pleft,pright->pleft,SArry ))
					{
						float lvalue,rvalue;
						if(pleft->pleft->pright->T.key==CN_CINT|| pleft->pleft->pright->T.key==CN_CCHAR)
						{
							lvalue=float(pleft->pleft->pright->T.addr);
						}
						else if(pleft->pleft->pright->T.key==CN_CFLOAT||pleft->pleft->pright->T.key==CN_CDOUBLE)
						{
							lvalue=float(pleft->pleft->pright->T.value);

						}
						if( pright->pright->T.key==CN_CINT||pright->pright->T.key==CN_CCHAR)
						{
							rvalue=float(pright->pleft->T.addr);  
						}
						else if(pright->pright->T.key==CN_CFLOAT||pright->pright->T.key==CN_CDOUBLE)
						{
							rvalue=float(pright->pleft->T.value) ;   
						}
						if(rvalue>lvalue)
						{
							
							ptemp->pleft=pleft->pright;
							//add delde exptree pleft
						//  AfxMessageBox("transformed6:"+GetExpString(p));////
								b_boolmodified=true; 

						}
						else
						{
						//should add	delete ptemp->pright branch;
							if(ptemp->pright)
								delete ptemp->pright; 
							ptemp->pright=NULL; 
							CopyENODE(ptemp,pleft);
							pright=ptemp->pright;
						 // AfxMessageBox("transformed7:"+GetExpString(p));////

								//add delde exptree pleft and pright
						if(pleft)
						delete pleft;
						b_boolmodified=true; 
						}

					}
				
				}
				ptemp=ptemp->pleft; 
				}while(ptemp&&ptemp->T.key==CN_DAND);






				
			}
					///////////////

			
////////////////////6.4///////////////////////////
				if((BOperator(p->T)==2) && p->pleft!=NULL&&p->pright !=NULL&&((BOperator(p->pleft->T)==-1)||BOperator(p->pleft->T)==-4)&&( BOperator(p->pright->T)==-1||BOperator(p->pright->T)==-4))
			{//i>100&&90>i->0
				bool flag=false;
				if(p->pleft->pleft&&p->pleft->pright&&p->pright->pleft&&p->pright->pright&&  CompareTrees(p->pleft->pleft,p->pright->pright,SArry ) )
				{
					if(p->pleft->pright->T.key==CN_CINT&& p->pright->pleft->T.key==CN_CINT&& p->pleft->pright->T.addr>= p->pright->pleft->T.addr)
                         flag=true;
					if(p->pleft->pright->T.key==CN_CFLOAT&& p->pright->pleft->T.key==CN_CFLOAT&& p->pleft->pright->T.value >= p->pright->pleft->T.value )
						flag=true;
					if(flag)
					{
						ENODE*pf=p->pleft;
						delete pf;
						pf=p->pright;
						delete pf;
						p->pleft=p->pright=NULL;
						p->T.key=CN_CINT;
						p->T.addr=0;  
						p->T.deref=0;//wtt 2008.3.19
						b_boolmodified=true; 

					}
				}

			}



			
			///////////////////////////////////////////////
			////////////////////6.6///////////////////////////
		if((BOperator(p->T)==2) && p->pleft!=NULL&&p->pright !=NULL&&((BOperator(p->pleft->T)==-1)||BOperator(p->pleft->T)==-4)&&( BOperator(p->pright->T)==-1||BOperator(p->pright->T)==-4))
			{//0>i&&i>2->0
				bool flag=false;
				if(p->pleft->pleft&&p->pleft->pright&&p->pright->pleft&&p->pright->pright&&  CompareTrees(p->pleft->pright,p->pright->pleft,SArry ) )
				{
					if(p->pleft->pleft->T.key==CN_CINT&& p->pright->pright->T.key==CN_CINT&& p->pleft->pleft->T.addr<= p->pright->pright->T.addr)
                         flag=true;
					if(p->pleft->pleft->T.key==CN_CFLOAT&& p->pright->pright->T.key==CN_CFLOAT&& p->pleft->pleft->T.value <= p->pright->pright->T.value )
						flag=true;
					if(flag)
					{
						ENODE*pf=p->pleft;
						delete pf;
						pf=p->pright;
						delete pf;
						p->pleft=p->pright=NULL;
						p->T.key=CN_CINT;
						p->T.addr=0;  
						p->T.deref=0;//wtt 2008.3.19
						b_boolmodified=true; 

					}
				}

			}



			
			///////////////////////////////////////////////
			///////////////////6.9////////////////////
			
			if(BOperator(p->T)==3 && p->pleft!=NULL&&p->pright !=NULL&&p->pleft->pleft&&p->pleft->pright &&p->pright->pleft &&p->pright->pright 
				&&p->pleft->pleft->pleft &&p->pleft->pleft->pright &&p->pleft->pright->pleft&&p->pleft->pright->pright
				&&BOperator(p->pleft->T)==2&&BOperator(p->pright->T)==-6&&BOperator(p->pleft->pleft->T)==-1&&BOperator(p->pleft->pright->T)==-1
				&&p->pright->pright->T.key==CN_CINT&&p->pleft->pleft->pright->T.key==CN_CINT
				&&p->pright->pright->T.addr==p->pleft->pleft->pright->T.addr
				&&CompareTrees(p->pright->pleft,p->pleft->pleft->pleft,SArry)
				)
			{//rule: x>0&&x<2||x==0 -> x>=0&&x<2
				//AfxMessageBox("ok");
				p->pleft ->pleft->T.key=CN_BANDE;
				//delete p->pright branch
				p->pright=NULL; 
				ENODE*pf=NULL;
				pf=p->pleft; 
				CopyENODE(p,pf);	
			  if(pf )
			  delete pf;
			  pf=NULL;
			  b_boolmodified=true;	
				
			}
			/////////////////////////////////////////////
	if((BOperator(p->T)==3||BOperator(p->T)==2)&&p->pleft&&p->pright&&(BOperator(p->pleft->T)==-6||BOperator(p->pleft->T)==-5)&&BOperator(p->pright ->T)<0  )
			{
				ENODE*pf;
				if(BOperator(p->pleft->T)==-6)
				{
					if(BOperator(p->pright->T)!=-6)
					{
						pf=p->pleft;
						p->pleft=p->pright;
						p->pright=pf; 
						b_boolmodified=true;

					}
					if(BOperator(p->pleft->T)==-5)
					{
						if(BOperator(p->pright->T)!=-6&&BOperator(p->pright->T)!=-5)
						{
						pf=p->pleft;
						p->pleft=p->pright;
						p->pright=pf; 
						b_boolmodified=true;

						}

					}
				}
			}



				STK.AddTail(p);
			
				p=STK.GetTail()->pleft; 




		}
		else
		{
			p=STK.GetTail();
			p=p->pright; 
			//		AfxMessageBox("out of stack"+C_ALL_KEYWORD[ptemp->T.key  ]);///
			STK.RemoveTail();			
		}

	}

	/******************* end of thid function **********************/

}

int CExpression::InSet(int i,int j)
{//wtt//5.13///
	if(i==-1&&j==-2||i==-1&&j==-3||i==-1&&j==-6||i==-5&&j==-6||i==-2&&j==-6)
		return 1;
	else if(i==-4&&j==-1||i==-4&&j==-6)//>=&&> >=&&== 
	return 2;
	else
		return 0;
	

}
bool CompareTrees(ENODE*pHead1,ENODE*pHead2,CArray<SNODE,SNODE> &SArry)
{//wtt编写 ：先根序遍历语法树比较pHead1和pHead2，若相同返回true，否则返回false

//	AfxMessageBox("compare trees");
		if(!pHead1&&!pHead2)
		return true;
	else if(!pHead1||!pHead2)
		return false;
	if(pHead1->T.key<0||pHead1->T.key>=132 )
		return false;
	if(pHead2->T.key<0||pHead2->T.key>=132 )
		return false;
	if(pHead1->T.key !=pHead2->T.key)
		return false;
	////////wtt 2008.4.1/////
/*	CString st;
	st.Format("%d,%d",pHead1->T.deref ,pHead2->T.deref);
	AfxMessageBox(st);
	if(pHead1->T.deref !=pHead2->T.deref)
	{
		AfxMessageBox("deref!=");
		return false;
	}*/
	///////////////////////////////
	switch(pHead1->T.key )
	{
		case CN_CINT:
		case CN_LONG:
		case CN_SHORT:
		case CN_UNSIGNED:
		case CN_CCHAR:
			if(pHead1->T.addr!=pHead2->T.addr )
				return false;
				break;
		case CN_CFLOAT:
		case CN_DOUBLE:
	//	case CN_LONG:
			if(pHead1->T.value!=pHead2->T.value )
				return false;
				break;
		case CN_VARIABLE:
		case CN_CSTRING:
		case CN_DFUNCTION:
		case CN_BFUNCTION:
			if(pHead1->T.name!=pHead2->T.name )
			return false;
			break;
	}
	if(pHead1->T.key==CN_VARIABLE||pHead1->T.key==CN_DFUNCTION)
	{
		if(pHead1->T.addr!=pHead2->T.addr)
			return false;
		if(pHead1->T.addr>=0&&pHead1->T.addr<SArry.GetSize()&&(SArry[pHead1->T.addr].kind==CN_ARRAY||SArry[pHead1->T.addr].kind==CN_DFUNCTION))
		{
			int i=0;
			while(i<10&&pHead1->pinfo[i]!=NULL)
			{
				if(!CompareTrees(pHead1->pinfo[i],pHead2->pinfo[i],SArry))
					return false;
				i++;

				}
		}
	}
	if(pHead1->T.key==CN_BFUNCTION)
	{

	
		int i=0;
		while(i<10&&pHead1->pinfo[i]!=NULL)
		{
			if(!CompareTrees(pHead1->pinfo[i],pHead2->pinfo[i],SArry))
				return false;
				i++;

		}

	}
	if(!CompareTrees(pHead1->pleft ,pHead2->pleft ,SArry))
		return false;
	if(!CompareTrees(pHead1->pright ,pHead2->pright ,SArry))
		return false;	
	
	return true;
}







///////////////////////////////////////////////////////////////////////////////////////



ENODE *CExpression::ArithExpStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    wtt重新编写了算术表达式的标准化算法
		pre-order traverse the syntax tree; standard the 
		arithmetic expression
	    
		PARAMETER:
		pHead : Head of arithmetic expression.
		SArry : ...
	*******************************************************/
	// part 0

	

	////////////////////wtt///////////////3.3//////////////////
	b_modified=true;
	while(b_modified)
	{
		b_modified=false;
	
		ENODE *pnode=new ENODE;
		for(int i=0;i<10;i++)
		{
			pnode->pinfo[i]=NULL; 
		}
		pnode->info=0;
//		pnode->T.deref=0;
		pnode->pright=NULL;
		pnode->pleft=pHead;
		pnode->T.key=CN_SUB;
		pHead=pnode;	
		AExpStandardRule(pHead,SArry);
		pHead=pHead->pleft;
		if(pnode)
		delete pnode;


	}
 	
	return pHead;
     
	/********************end of this function********************/

}

void CExpression:: AExpStandardRule(ENODE*pHead,CArray<SNODE,SNODE>&SArry)
{//wtt3.4编写的算术表达式的标准化规则
	CList<ENODE *,ENODE *> STK;
	ENODE *p;
//	AfxMessageBox("Arith standarded");
	
	if(pHead==NULL)
	{
		return ;
	}
   
	p=pHead;
    int num=0;
	int label=0;
	ENODE *ptemp=NULL;
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// add action here

			// 1.standard the functionn parameters or array indexs;

			if(p->info>0&&p->info<10&& (p->T.key==CN_VARIABLE||p->T.key==CN_BFUNCTION || p->T.key==CN_DFUNCTION ))
			{
				bool b_before=b_modified;

				for(int k=0;k<10&& p->pinfo[k] ;k++)
				{			
					
					p->pinfo[k]=ArithExpStandard(p->pinfo[k],SArry);
 			
				}	
				b_modified=b_before||b_modified;
			}

		/////////////////////////////////wtt 2008.3.19 增加指针与数组的标准化///

			if(AOpClassify(p)==-1&&p->T.deref==1
				&&p->pleft!=NULL &&p->pleft->T.addr>=0&&p->pleft->T.addr<SArry.GetSize()
				&&(SArry[p->pleft->T.addr].kind==CN_POINTER||SArry[p->pleft->T.addr].kind==CN_ARRAY))
			{//*(p+i)->p[i]    *(*(a+i)+j)->a[i][j]
				
			//	AfxMessageBox("standard pointer");
			//	AfxMessageBox(TraverseAST(p,SArry));
				int k=0;
				while(k<10&&p->pleft->pinfo[k]!=NULL)
				{
					k++;
				}
				if(k==10)
					return;
				p->pleft->pinfo[k]=p->pright;
				p->pleft->info++;
				p->pright=NULL;
				ENODE*pl;
				pl=p->pleft;
				CopyENODE(p,pl);
				delete pl;

				b_modified=true;
			}
			if(AOpClassify(p)==-1&&p->T.deref==0
				&&p->pleft!=NULL &&p->pleft->T.addr>=0&&p->pleft->T.addr<SArry.GetSize()
				&&(SArry[p->pleft->T.addr].kind==CN_POINTER&&p->pleft->info!=0||SArry[p->pleft->T.addr].kind==CN_ARRAY)&&p->pleft->T.deref==0)
			{//a[i]+j->&a[i][j]           
				
			//	AfxMessageBox("standard pointer");
			//	AfxMessageBox(TraverseAST(p,SArry));
				int k=0;
				while(k<10&&p->pleft->pinfo[k]!=NULL)
				{
					k++;
				}
				if(k==10)
					return;
				p->pleft->pinfo[k]=p->pright;
				p->pleft->info++;
				p->pright=NULL;
				ENODE*pl;
				pl=p->pleft;
				CopyENODE(p,pl);
				p->T.deref=-1;
				delete pl;

				b_modified=true;
			}
       ////////////wtt  2008.3.21/////////////////////
			if((AOpClassify(p)==-1||AOpClassify(p)==-2)
				&&p->T.deref==0
				&&p->pleft!=NULL &&p->pleft->T.addr>=0&&p->pleft->T.addr<SArry.GetSize()
				&&(SArry[p->pleft->T.addr].kind==CN_POINTER
				&&(p->pleft->info<0||p->pleft->info==0)&&p->pleft->T.deref<SArry[p->pleft->T.addr].arrdim
				)
				)
			{//int **p; q=*p+1->int **p;  q=(*p)[1]   指针算术运算           
			/*	CString st;
				st.Format("deref=%d,arrdim=%d",p->T.deref,SArry[p->pleft->T.addr].arrdim);
				AfxMessageBox(TraverseAST(p,SArry)+st);*/
			//	AfxMessageBox(TraverseAST(p,SArry));
			 
				ENODE*pinfo;
				pinfo=new ENODE;
				InitENode(pinfo);
				CopyTNODE(pinfo->T,p->T);
			//	pinfo->pleft=p->pright;
				pinfo->pright=p->pright;

				ENODE*pl;
				pl=p->pleft;
				
				pl->info=-1;

			//	p->pleft->pinfo[0]=p->pright;
			//	p->pleft->info--;/////////////现在需要标记*与P近！！！！！！！负值表示pinfo中存放的是指针算术运算而不是指针下标
			//	p->pright=NULL;
				
				
			//	pl->T.
				
			//	
				CopyENODE(p,pl);

				if(p->pinfo[0]==NULL)
					p->pinfo[0]=pinfo;
				else
				{
					pinfo->pleft=p->pinfo[0];
					p->pinfo[0]=pinfo;				


				}

				delete pl;
				
			//	p=pl;
			//	AfxMessageBox(TraverseAST(p,SArry));
			//	AfxMessageBox(TraverseAST(p->pinfo[0],SArry));

				b_modified=true;
			}



   ///*与&自动抵消因此经过上两个规则*(a[i]+j)->*(&a[i][j])->a[i][j]
		//	*(*(a+i)+j)->a[i][j]

			////////////wtt 2008.3.19 增加指针与数组的标准化//////以上将指针算术转换为数组下标的形式///////////



			
			if((AOpClassify(p)==1||AOpClassify(p)==2) && p->pleft!=NULL && AOpClassify(p->pleft)<0)
			{//Rule ：(A1+A2)/A3→A1/A3+A2/A3, (A1-A2)/A3→A1/A3-A2/A3；Rule ：(A1+A2)*A3→A1*A3+A2*A3, (A1-A2)*A3→A1*A3-A2*A3；

				int key=p->T.key;	
				p->T.key=p->pleft->T.key;
				ENODE* pright=p->pright;
				p->pleft->T.key=key;
				ENODE*pnew=new ENODE;
				pnew->info=0;
				pnew->T.deref=0;//wtt 2008.3.19
				pnew->pleft=NULL;
				pnew->pright=NULL;
				for(int i=0;i<10;i++)
				{
					pnew->pinfo[i]=NULL;
				}
				pnew->T.key=key;
				
				pnew->pleft=p->pleft->pright;
				pnew->pright=CopyTree(pnew->pright,pright);				
				p->pright=pnew;				 
				p->pleft->pright=pright;
				b_modified=true;			

			}            
			
			if( AOpClassify(p)==1 && p->pright!=NULL && AOpClassify(p->pright)<0)

			{//Rule ：A1*(A2+A3)→A1*A2+A1*A3, A1*(A2-A3)→A1*A2-A1*A3；
			             
						
				int key=p->T.key;	
				p->T.key=p->pright->T.key;
				ENODE* pleft=p->pleft;
				p->pright->T.key=key;
				ENODE*pnew=new ENODE;
				pnew->info=0;
//				pnew->T.deref=0;
				pnew->pleft=NULL;
				pnew->pright=NULL;
				for(int i=0;i<10;i++)
				{
					pnew->pinfo[i]=NULL;
				}
				pnew->T.key=key;
				
				pnew->pright=p->pright->pleft;
				pnew->pleft=CopyTree(pnew->pleft,pleft);
				
				p->pleft=pnew;
				 
				p->pright->pleft=pleft;
				b_modified=true;			
		
			}	
	//		if(AOpClassify(p)==-2&&p->pleft&&AOpClassify(p->pleft)==-2&&p->pright==NULL&&p->pleft->pright==NULL)
		if(p->T.key==CN_SUB&&p->pleft&&p->pleft->pleft&&p->pleft->T.key==CN_SUB&&p->pleft->pleft->T.key==CN_SUB &&p->pright==NULL&&p->pleft->pright==NULL&&p->pleft->pleft->pright==NULL)

			{//Rule:-(-A1)→A1；
			//	AfxMessageBox("-(-A1)→A1");///
				 ENODE *pf=NULL;
				 pf=p->pleft->pleft;	
				 CopyENODE(p,pf);
				 //if(pf->pleft)
				 //delete pf->pleft;
				 //pf->pleft=NULL; 
				 //if(pf)
			     //delete pf;
				 pf=NULL;
				 b_modified=true;
			
			}
			 if(AOpClassify(p)==2&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==1 )
			 {//A1/1→A1
				 ENODE *pf=NULL;
				 pf=p->pleft;
				if(p->pright )
				 delete p->pright;
				p->pright=NULL; 
				 CopyENODE(p,pf);
				if(pf)
				 delete pf;
				pf=NULL;
				 b_modified=true;
				
			 }
		//	 if(AOpClassify(p)==1&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT)
			 if(AOpClassify(p)==1&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&(p->pright->T.addr==1||p->pright->T.addr==0))
			 {//Rule ：A1*1→A1；Rule ：A1*0→0；
				ENODE *pf=NULL;
				if(p->pright->T.addr==1)
				{
					pf=p->pleft;
					if(p->pright )
					delete p->pright;
					p->pright=NULL; 
				}
				else if(p->pright->T.addr==0)
				{
					if(p->pleft ) 
					delete p->pleft;
					p->pleft=NULL; 
					pf=p->pright;
				}
				 CopyENODE(p,pf);
				 if(pf)
				 delete pf;
				 pf=NULL;
				 b_modified=true;
				
			 }
			 //if(AOpClassify(p)==-1&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
			 if(AOpClassify(p)==-1&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
			 {//Rule ：A1+0→A1；
				ENODE *pf=NULL;
				pf=p->pleft;
				if(p->pright)
				delete p->pright;
				p->pright=NULL; 
				 CopyENODE(p,pf);
				if(pf)
				 delete pf;
				pf=NULL;
				 b_modified=true;			

			 }
			 if(AOpClassify(p)==-2&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
			 {//Rule ：A1-0→A1；
				 ENODE *pf=NULL;
				 pf=p->pleft;
				 if(p->pright)
				 delete p->pright;
				 p->pright=NULL; 
				 CopyENODE(p,pf);
				 if(pf)
				 delete pf;
				 pf=NULL;
				 b_modified=true;

			 }
			 if(AOpClassify(p)==2&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0 )
			 {//Rule:0/A1→0
				 ENODE *pf=NULL;
				 pf=p->pleft;
				 if(p->pright )
				 delete p->pright;
				 p->pright=NULL; 
				 CopyENODE(p,pf);
				 if(pf)
				 delete pf;
				 pf=NULL;
				 b_modified=true;

			 }
			 	if(AOpClassify(p)==3&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
			 {//Rule ：0%A1→0；
				 ENODE *pf=NULL;
				 pf=p->pleft;
				 if(p->pright )
				  delete p->pright;
				 p->pright=NULL; 
				 CopyENODE(p,pf);
				 if(pf)
				 delete pf;
				 pf=NULL;
				 b_modified=true;

			 }
			if(AOpClassify(p)==3&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==1)
			{//Rule ：A1%1→0

				ENODE *pf=NULL;
				 pf=p->pright;
				 if(p->pleft )
				 delete p->pleft;
				 p->pleft=NULL; 
				 pf->T.addr=0;
				 pf->T.deref=0;//wtt 2008.3.19
				 CopyENODE(p,pf);
				 if(pf)
				 delete pf;
				 pf=NULL;
				 b_modified=true;

			}

			if(AOpClassify(p)==2&&p->pleft&&p->pright&&CompareTrees(p->pleft,p->pright,SArry)  )
		  {//Rule ：A1 / A1→1；
//			  
			  p->T.key=CN_CINT;
			  p->T.addr=1;
			  p->T.deref=0;//wtt 2008.3.19
			  p->T.paddr=-1;
			  for(int i=0;i<10;i++)
			  {
				   p->pinfo[i]=NULL;
			  }		
			 	if(p->pright )
			  delete p->pright;
			
				if(p->pleft )
			   delete p->pleft ;
			
			   p->pleft=NULL;
			   p->pright=NULL; 
			  b_boolmodified=true;

		  }
		
			//if(AOpClassify(p)!=0&&p->pleft&&p->pright&&(p->pright->T.key==CN_CINT||p->pright->T.key==CN_CDOUBLE||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CLONG||p->pright->T.key==CN_CCHAR||p->pright->T.key==CN_CSTRING))
			if(AOpClassify(p)!=0&&p->pleft&&p->pright&&(p->pleft->T.key==CN_CINT||p->pleft->T.key==CN_CDOUBLE||p->pleft->T.key==CN_CFLOAT||p->pleft->T.key==CN_CLONG||p->pleft->T.key==CN_CCHAR||p->pleft->T.key==CN_CSTRING))
			{//Rule ：constant
				
				ENODE *pf=NULL;
			//		ptemp=NULL;
				
			
				if(!(p->pright->T.key==CN_CINT||p->pright->T.key==CN_CDOUBLE||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CLONG||p->pright->T.key==CN_CCHAR||p->pright->T.key==CN_CSTRING))
				{// constant oprator expression
				//	AfxMessageBox("expression oprator constant!");/////////////////
					
					if(AOpClassify(p)==-1||AOpClassify(p)==1)
					{
					//	AfxMessageBox("addition detected");////////
						pf=p->pleft;
						p->pleft=p->pright;
						p->pright=pf;
						b_modified=true;
					
					}
					///////////////////////////////////原来
				/*	else if(AOpClassify(p)==-2)
					{
						
						
							pf=new ENODE;
							pf->T.addr=-1;
							pf->T.paddr=-1;
							pf->T.key=CN_SUB;
							pf->pleft=p->pright;
							pf->pright=NULL;

							AfxMessageBox(C_ALL_KEYWORD[pf->T.key ]);

							for(int i=0;i<10;i++)
							{
								pf->pinfo[i]=NULL; 
							}

							p->T.key=CN_ADD;


							p->pright=p->pleft ; 
							p->pleft=pf; 
							
							
							b_modified=true;
					

					}*/
				}
				///////////////////////////////////////
				else 
				{//constant operator constant
					
					if(p->pright->T.key==CN_CINT&&p->pleft->T.key==CN_CINT)
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.addr=p->pright->T.addr+p->pleft->T.addr;
							b_modified=true;
						
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.addr=p->pleft->T.addr-p->pright->T.addr;
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pright->T.addr=p->pleft->T.addr*p->pright->T.addr;
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.addr!=0)
						{
							p->pright->T.addr=p->pleft->T.addr/p->pright->T.addr;
							b_modified=true;	
						}
						else if(p->T.key==CN_FORMAT )
						{
							p->pright->T.addr=p->pleft->T.addr%p->pright->T.addr;
							b_modified=true;
						}
						pf=p->pright;
						if(p->pleft)
						delete p->pleft;
						p->pleft=NULL; 
						CopyENODE(p,pf);
						if(pf)
						delete pf;
						pf=NULL;
						
					}
					else if((p->pright->T.key==CN_CDOUBLE||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CLONG)&&(p->pleft->T.key==CN_CLONG||p->pleft->T.key==CN_CFLOAT||p->pleft->T.key==CN_CDOUBLE))
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.value=p->pright->T.value+p->pleft->T.value;  
							b_modified=true;
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.value=p->pleft->T.value-p->pright->T.value;
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pright->T.value=p->pleft->T.value*p->pright->T.value;
						
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.value!=0)
						{
							p->pright->T.value=p->pleft->T.value/p->pright->T.value;
							b_modified=true;
						}	
						 pf=p->pright;
						 if(p->pleft)
						delete p->pleft;
						 p->pleft=NULL; 
						CopyENODE(p,pf);
						if(pf)
						delete pf;
						pf=NULL;
					

					}
					else if(p->pright->T.key==CN_CINT&&(p->pleft->T.key==CN_CLONG||p->pleft->T.key==CN_CFLOAT||p->pleft->T.key==CN_CDOUBLE))
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.value=p->pright->T.addr+p->pleft->T.value;  
							b_modified=true;
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.value=p->pleft->T.value-p->pright->T.addr; 
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
								p->pright->T.value=p->pleft->T.value*p->pright->T.addr; 
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.addr!=0)
						{
							p->pright->T.value=p->pleft->T.value/p->pright->T.addr; 
							b_modified=true;
						}
					
							
					pf=p->pright;
					if(p->pleft )
						delete p->pleft;
					p->pleft=NULL; 
						CopyENODE(p,pf);
						if(pf)
						delete pf;
						pf=NULL;
						

					}
					else if(p->pleft->T.key==CN_CINT&&(p->pright->T.key==CN_CLONG||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CDOUBLE))
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.value=p->pright->T.value+p->pleft->T.addr;  
							b_modified=true;
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.value=p->pleft->T.addr-p->pright->T.value;   
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pright->T.value=p->pleft->T.addr*p->pright->T.value;    
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.addr!=0)
						{
							p->pright->T.value=p->pleft->T.addr/p->pright->T.value;  
							b_modified=true;
						}
						pf=p->pright;
						if(p->pleft )
						delete p->pleft;
						p->pleft=NULL; 
						CopyENODE(p,pf);
						if(pf)
						delete pf;
						pf=NULL;
					
					}

				}
			
			}
		if(AOpClassify(p)==2 && p->pright &&(AOpClassify(p->pright)==1||AOpClassify(p->pright)==2) )
			{//Rule ：A1/(A2/A3)→A1*A3/A2 Rule：A1/(A2*A3)→A1/A2/A3

				ENODE *pleft=NULL;
				ENODE *pright=NULL;
				                
				pleft=p;
				pright=p->pright; 

				if(p->T.key==CN_DIV)
				{
					if(p->pright->T.key==CN_MULTI)
					{//A1/(A2*A3)→A1/A2/A3
						pright->T.key=CN_DIV;  
						////////////////wtt//////3.2////////////
						p->pright=pright->pright;
						pright->pright=pright->pleft;
						pright->pleft=p->pleft;					
						p->pleft=pright;
						b_modified=true;


						///////////////////wtt//////////////

					}
					if(p->pright->T.key==CN_DIV)
					{//A1/(A2/A3)→A1*A3/A2
						pright->T.key=CN_MULTI; 							
						p->pright=pright->pleft;
						pright->pleft=p->pleft;
						p->pleft=pright;
						b_modified=true;
					}
					

				}
				
			//	p->pright=pright->pleft;
			//	pright->pleft=p->pleft;
			//	p->pleft=pright;				
			}
			////////////////wtt/////////3.2///3.3/////////////////
			 if(AOpClassify(p)==-1 && p->pright &&AOpClassify(p->pright)<0)
			{//A1+(A2+A3)→A1+A2+A3 A1+(A2-A3)→A1+A2-A3


			
				ENODE *pright=NULL;
				                
			
				pright=p->pright; 

				p->T.key=pright->T.key;					
				pright->T.key=CN_ADD;  
				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;

			}
			 if(AOpClassify(p)==-2 && p->pright &&AOpClassify(p->pright)<0)
			{//Rule ：A1-(A2-A3)→A1-A2+A3 Rule ：A1-(A2+A3)→A1-A2-A3

				ENODE *pright=NULL;
			
				pright=p->pright; 

				if(pright->T.key==CN_ADD)
				{
					p->T.key=CN_SUB;

				}
				else if(pright->T.key==CN_SUB)
					p->T.key=CN_ADD;

							
				pright->T.key=CN_SUB;  

				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;
			}
				 if(AOpClassify(p)==1 && p->pright &&(AOpClassify(p->pright)==1||AOpClassify(p->pright)==2))
			{//Rule ：A1*(A2*A3)→A1*A2*A3 Rule ：A1*(A2/A3)→A1*A2/A3

				
				
				ENODE *pright=NULL;
				                
			
				pright=p->pright; 

				p->T.key=pright->T.key;					
				pright->T.key=CN_MULTI;
				
				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;

			}
		if((AOpClassify(p)==1&&p->pleft&&AOpClassify(p->pleft)==2)||(AOpClassify(p)==-1&&p->pleft&&AOpClassify(p->pleft)==-2&&p->pleft->pleft&&p->pleft->pright))
			{//A1/A3*A2→A1*A2/A3；A1-A2+A3→A1+A3-A2
				int temp;
				temp=p->T.key;
				p->T.key=p->pleft->T.key;
				p->pleft->T.key=temp ;
				ENODE *pright=NULL;
				pright=p->pright;
				p->pright=p->pleft->pright;
				p->pleft->pright=pright;
				b_modified=true;

			}

		  	STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		
		
		}
		else
		{
			p=STK.GetTail();
			p=p->pright; 		
			STK.RemoveTail();			
		}

	}

}

/*void   CExpression::ReMoveParenth(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		pre-order traverse the syntax tree; standard the 
		arithmetic expression,remove the parentheses from 
		syntax tree.
	    
		PARAMETER:
		pHead : Head of arithmetic expression.
		SArry : ...
	*******************************************************/
    	 	
/*	CList<ENODE *,ENODE *> STK;
	ENODE *p;
	
	if(pHead==NULL)
	{
		return ;
	}
   
	p=pHead;
    int num=0;
	int label=0;
	ENODE *ptemp=NULL;
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// add action here

			// 1.standard the functionn parameters or array indexs;

			if(p->info>0&& (p->T.key==CN_VARIABLE||p->T.key==CN_BFUNCTION || p->T.key==CN_DFUNCTION ))
			{
				bool b_before=b_modified;

				for(int k=0; p->pinfo[k] && p->info<=10;k++)
				{			
					
					p->pinfo[k]=ArithExpStandard(p->pinfo[k],SArry);
 			
				}	
				b_modified=b_before||b_modified;
			}

		
			if((AOpClassify(p)==1||AOpClassify(p)==2) && p->pleft!=NULL && AOpClassify(p->pleft)<0)
			{//Rule ：(A1+A2)/A3→A1/A3+A2/A3, (A1-A2)/A3→A1/A3-A2/A3；Rule ：(A1+A2)*A3→A1*A3+A2*A3, (A1-A2)*A3→A1*A3-A2*A3；


				#ifdef DEBUG
				AfxMessageBox("left standard");
                #endif

				ENODE *pf=NULL;
				//ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetTail();//At(STK.FindIndex(STK.GetCount()-2));
				}
				p->info=MAX; 
				pf=SelectFather(p,pf,ptemp);
				p=LeftChildStandard(pf,p);
				b_modified=true;

			

			}
            
			// 2.the second standard type of two
			if(p && AOpClassify(p)==1 && p->pright!=NULL && AOpClassify(p->pright)<0)

			{//Rule ：A1*(A2+A3)→A1*A2+A1*A3, A1*(A2-A3)→A1*A2-A1*A3；
				
                ENODE *pf=NULL;
			//	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
			
				p=RightChildStandard(pf,p);
				b_modified=true;
		
			}	

		// end

			// and test if need moving branch;
			///////////////////////////////////wtt////////3.2////////////
			 if(AOpClassify(p)==-2&&p->pleft&&AOpClassify(p->pleft)==-2&&p->pright==NULL&&p->pleft->pright==NULL)
			{//Rule:-(-A1)→A1；
				ENODE *pf=NULL;
				//	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
			
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pleft->pleft;
					p=p->pleft->pleft;
					b_modified=true;
				}

			}
			 if(AOpClassify(p)==2&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==1 )
			 {//A1/1→A1
				 ENODE *pf=NULL;
			//	 	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
			
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pleft;
					p=p->pleft;
				}
				if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
				{
					
					pf->pright=p->pleft;
					p=p->pleft;
				}
				b_modified=true;

			 }
			 if(AOpClassify(p)==1&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT)
			 {//Rule ：1*A1→A1；Rule ：0*A1→0；
				ENODE *pf=NULL;
				//	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
				if(p->pleft->T.addr==1||p->pleft->T.addr==0)
				{
					if(p->pleft->T.addr==0)
					{
						p->pright->T.key=CN_CINT;
						p->pright->T.addr=0;
						p->pright->pleft=NULL;
						p->pright->pright=NULL;
					}
					if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
					{
					
						pf->pleft=p->pright;
						p=p->pright;
					}
					if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
					{
					
						pf->pright=p->pright;
						p=p->pright;
					}
					b_modified=true;
				}
			 }


			if(AOpClassify(p)==-1&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
			 {//Rule ：0+A1→A1；
				ENODE *pf=NULL;
			//	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
			
				
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pright;
					p=p->pright;
				}
				if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
				{
					
					pf->pright=p->pright;
					p=p->pright;
				}
				b_modified=true;

			 }
			if(AOpClassify(p)==-2&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==0)
			 {//Rule ：A1-0→A1；
				ENODE *pf=NULL;
			//		ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
				
				
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pleft;
					p=p->pleft;
				}
				if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
				{
					
					pf->pright=p->pleft;
					p=p->pleft;
				}
				b_modified=true;

			 }
			if(AOpClassify(p)==2&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0 )
			 {//0/A1→0
				 ENODE *pf=NULL;
			//	 	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
			
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pleft;
					p=p->pleft;
				}
				if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
				{
					
					pf->pright=p->pleft;
					p=p->pleft;
				}
				b_modified=true;

			 }
			if(AOpClassify(p)==3&&p->pleft&&p->pright&&p->pleft->T.key==CN_CINT&&p->pleft->T.addr==0)
			 {//Rule ：0%A1→0；
				ENODE *pf=NULL;
			//		ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
				
				
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pleft;
					p=p->pleft;
				}
				if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
				{
					
					pf->pright=p->pleft;
					p=p->pleft;
				}	
				b_modified=true;

			 }
			if(AOpClassify(p)==3&&p->pleft&&p->pright&&p->pright->T.key==CN_CINT&&p->pright->T.addr==1)
			 {//Rule ：A1%1→0
			
				ENODE *pf=NULL;
				//	ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
				}
				p->info=MAX; 
				
				pf=SelectFather(p,pf,ptemp);
				p->pright->T.addr=0;  
				
				
				if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
				{
					
					pf->pleft=p->pright;
					p=p->pright;
				}
				if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
				{
					
					pf->pright=p->pright;
					p=p->pright;
				}
				b_modified=true;

			 }


			if(AOpClassify(p)!=0&&p->pleft&&p->pright&&(p->pright->T.key==CN_CINT||p->pright->T.key==CN_CDOUBLE||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CLONG||p->pright->T.key==CN_CCHAR||p->pright->T.key==CN_CSTRING))
			 {//Rule ：constant
				
				ENODE *pf=NULL;
			//		ptemp=NULL;
				
				pf=SelectFather(p,pf,ptemp);
				if(!(p->pleft->T.key==CN_CINT||p->pleft->T.key==CN_CDOUBLE||p->pleft->T.key==CN_CFLOAT||p->pleft->T.key==CN_CLONG||p->pleft->T.key==CN_CCHAR||p->pleft->T.key==CN_CSTRING))
				{//expression oprator constant
				//	AfxMessageBox("expression oprator constant!");/////////////////
					
					if(AOpClassify(p)==-1||AOpClassify(p)==1)
					{
						pf=p->pleft;
						p->pleft=p->pright;
						p->pright=pf;
						b_modified=true;
					
					}
					else if(AOpClassify(p)==-2)
					{
						p->T.key=CN_ADD;
						if(p->pright->T.key==CN_CINT)
						{
							p->pright->T.addr=0-p->pright->T.addr;  
							pf=p->pleft;
							p->pleft=p->pright;
							p->pright=pf;
							b_modified=true;

						}
						else if(p->pright->T.key==CN_CDOUBLE||p->pright->T.key==CN_CFLOAT)
						{
							p->pright->T.value=0-p->pright->T.value;
							pf=p->pleft;
							p->pleft=p->pright;
							p->pright=pf;
							b_modified=true;

						}
						

					}
				}
				else 
				{//constant operator constant
					pf=NULL;
					if(STK.GetCount()>=1)
					{
						pf=STK.GetAt(STK.FindIndex(STK.GetCount()-1)); 
					}
					p->info=MAX; 
					if(p->pright->T.key==CN_CINT&&p->pleft->T.key==CN_CINT)
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.addr=p->pright->T.addr+p->pleft->T.addr;
							b_modified=true;
						
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.addr=p->pleft->T.addr-p->pright->T.addr;
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pright->T.addr=p->pleft->T.addr*p->pright->T.addr;
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.addr!=0)
						{
							p->pright->T.addr=p->pleft->T.addr/p->pright->T.addr;
							b_modified=true;	
						}
						else if(p->T.key==CN_FORMAT )
						{
							p->pright->T.addr=p->pleft->T.addr%p->pright->T.addr;
							b_modified=true;
						}

						if(	pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
						{
					
							pf->pleft=p->pright;
							p=p->pright;
						}
						if(	pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
						{
					
							pf->pright=p->pright;
							p=p->pright;
						}	
					
						
					}
					else if((p->pright->T.key==CN_CDOUBLE||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CLONG)&&(p->pleft->T.key==CN_CLONG||p->pleft->T.key==CN_CFLOAT||p->pleft->T.key==CN_CDOUBLE))
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.value=p->pright->T.value+p->pleft->T.value;  
							b_modified=true;
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.value=p->pleft->T.value-p->pright->T.value;
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pright->T.value=p->pleft->T.value*p->pright->T.value;
						
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.value!=0)
						{
							p->pright->T.value=p->pleft->T.value/p->pright->T.value;
							b_modified=true;
						}	
						if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
						{
					
							pf->pleft=p->pright;
							p=p->pright;
						}
						if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
						{
					
							pf->pright=p->pright;
							p=p->pright;
						}	
					

					}
					else if(p->pright->T.key==CN_CINT&&(p->pleft->T.key==CN_CLONG||p->pleft->T.key==CN_CFLOAT||p->pleft->T.key==CN_CDOUBLE))
					{
						if(p->T.key==CN_ADD )	
						{
							p->pleft->T.value=p->pright->T.addr+p->pleft->T.value;  
							b_modified=true;
						}
						else if(p->T.key==CN_SUB )
						{
							p->pleft->T.value=p->pleft->T.value-p->pright->T.addr; 
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pleft->T.value=p->pleft->T.value*p->pright->T.addr; 
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.addr!=0)
						{
							p->pleft->T.value=p->pleft->T.value/p->pright->T.addr; 
							b_modified=true;
						}
					
							
						if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
						{
					
							pf->pleft=p->pleft;
							p=p->pleft;
						}
						if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
						{
					
							pf->pright=p->pleft;
							p->pleft;
						}
						

					}
					else if(p->pleft->T.key==CN_CINT&&(p->pright->T.key==CN_CLONG||p->pright->T.key==CN_CFLOAT||p->pright->T.key==CN_CDOUBLE))
					{
						if(p->T.key==CN_ADD )	
						{
							p->pright->T.value=p->pright->T.value+p->pleft->T.addr;  
							b_modified=true;
						}
						else if(p->T.key==CN_SUB )
						{
							p->pright->T.value=p->pleft->T.addr-p->pright->T.value;   
							b_modified=true;
						}
						else if(p->T.key==CN_MULTI )
						{
							p->pright->T.value=p->pleft->T.addr*p->pright->T.value;    
							b_modified=true;
						}
						else if(p->T.key==CN_DIV&& p->pright->T.addr!=0)
						{
							p->pright->T.value=p->pleft->T.addr/p->pright->T.value;  
							b_modified=true;
						}
					
							
						if(pf!=NULL && pf->pleft!=NULL && pf->pleft->info==MAX)
						{
					
							pf->pleft=p->pright;
							p=p->pright;
						}
						if(pf!=NULL && pf->pright!=NULL && pf->pright->info==MAX)
						{
					
							pf->pright=p->pright;
							p->pright;
						}		
					
					}

				}
			
			}














			/////////////wtt//////////////////////


			if((AOpClassify(p)==1||AOpClassify(p)==2)&& p->pleft && (AOpClassify(p->pleft)==1||AOpClassify(p->pleft)==2))
			{//Rule ：(A1+A2)/A3→A1/A3+A2/A3, (A1-A2)/A3→A1/A3-A2/A3；Rule ：(A1+A2)*A3→A1*A3+A2*A3, (A1-A2)*A3→A1*A3-A2*A3；

				#ifdef DEBUG
				AfxMessageBox("left branch move");
				#endif

				ENODE *pfz=NULL;
			//		ptemp=NULL;
				if(STK.GetCount()>=1)
				{
					pfz=STK.GetTail();//At(STK.FindIndex(STK.GetCount()-2));
				}

				p->info=MAX; 
				pfz=SelectFather(p,pfz,ptemp);
				p=MoveBranch(pfz,p);
				

				STK.AddTail(p);
				p=STK.GetTail()->pleft; 
				
				
			}
			else if((AOpClassify(p)==1||AOpClassify(p)==2) && p->pright && (AOpClassify(p->pright)==1||AOpClassify(p->pright)==2))
			{//Rule ：A1*(A2+A3)→A1*A2+A1*A3, A1*(A2-A3)→A1*A2-A1*A3；
				#ifdef DEBUG
				AfxMessageBox("right branch move");
				#endif

				ENODE *pfy=NULL;
				//	ptemp=NULL;

				if(STK.GetCount()>=1)
				{
					pfy=STK.GetTail();//At(STK.FindIndex(STK.GetCount()-2));
				}

				p->info=MAX; 
				pfy=SelectFather(p,pfy,ptemp);
				p=MoveBranch(pfy,p);
				

				STK.AddTail(p);
				p=STK.GetTail()->pleft; 
				
			
			}
			else
			{
				STK.AddTail(p);
				p=STK.GetTail()->pleft;  
			}
		
		}
		else
		{
			p=STK.GetTail();
			p=p->pright; 
			ptemp=STK.GetTail() ;
			STK.RemoveTail();			
		}

	}



}*/

/*ENODE* CExpression::LeftChildStandard (ENODE *pfather,ENODE *pnode)
{

	/**************************************************************
	 a node--"pnode" has a left son ,and its left son's type is 
	 operator "+" or "-",we should move its left son to the upper
	 level in order to standard the syntax tree.
	**************************************************************/

/*	static int num;
	int label=0;
	if(pfather!=NULL && pfather->pleft!=NULL && pfather->pleft->info==MAX)
	{
		label=1;
		pfather->pleft->info=0;

	}

	if(pfather!=NULL && pfather->pright!=NULL && pfather->pright->info==MAX)
	{
		label=-1;
		pfather->pright->info=0;
	}

	ENODE *pnew=new ENODE;
	pnew->info=0;
	pnew->pleft=NULL;
	pnew->pright=NULL;
	for(int i=0;i<10;i++)
	{
		pnew->pinfo[i]=NULL; 
	}

	pnew=CopyENODE(pnew,pnode);

	ENODE *pnewcld=NULL,*pintcld=NULL;
	
	// copy a new right branch 
	pintcld=pnode->pright;
	pnewcld=pnewcld=CopyTree(pnewcld,pintcld);
	pnew->pright=pnewcld;
	// end
	
	
   

	// add a copy of right branch to the pnode's left
	ENODE *ptemp=pnode->pleft;
	pnew->pleft=ptemp->pleft;
	ptemp->pleft=pnew;  
	// end
	

	if(pfather==NULL)
	{
		if(ptemp->pright!=NULL)
		{
			pnode->pleft=ptemp->pright;
			ptemp->pright=pnode; 
		}
	}
	else
	{
		if(label==1)
		{
			pfather->pleft=ptemp;
		}
		else if(label==-1)
		{
			pfather->pright=ptemp;
		}

        if(ptemp->pright!=NULL)
		{
			pnode->pleft=ptemp->pright;
			ptemp->pright=pnode;  
		}
	}

    

	pnode=ptemp;
	return ptemp;
	
	
}*/


/*ENODE* CExpression::RightChildStandard(ENODE *pfather,ENODE *pnode)
{

	/**************************************************************
	 a node--"pnode" has a right son ,and its right son's type is 
	 operator "+" or "-",we should move it right son to the upper
	 level in order to standard the syntax tree.
	**************************************************************/


/*	static int num;
    int label=0;
	if(pfather!=NULL )
	{
		if(pfather->pleft!=NULL && pfather->pleft->info>=MAX)
		{
			label=1;
		}

		if(pfather->pright!=NULL && pfather->pright->info>=MAX)
		{
			label=-1;
		}
		
	}
    
	pnode->info=0;
	
    
	ENODE *pnew=new ENODE;
	pnew->info=0;
	pnew->pleft=NULL;
	pnew->pright=NULL;
	for(int i=0;i<10;i++)
	{
		pnew->pinfo[i]=NULL; 
	}


	pnew=CopyENODE(pnew,pnode);

	ENODE *pnewcld=NULL,*pintcld=NULL;
	
	// copy a new left branch 
	pintcld=pnode->pleft;
	pnewcld=pnewcld=CopyTree(pnewcld,pintcld);
	pnew->pleft=pnewcld;
	// end

	#ifdef TEST
    CString s;
	s.Format("%d",num);
	num++;
	CFile f("data\\right_subtree"+s+".txt",CFile::modeCreate|CFile::modeWrite);
	extern CString PreOrderPrint(ENODE *pHead,CFile &f);
	#endif
    

	// add a copy of left branch to the pnode->pright's right
	ENODE *ptemp=pnode->pright;
	pnew->pright=ptemp->pleft;
	ptemp->pleft=pnew;  
	// end
    

	if(pfather==NULL)
	{
		if(ptemp->pright!=NULL)
		{
			pnode->pright=ptemp->pright;
			ptemp->pright=pnode; 
		}
	}
	else
	{
		if(label==1)
		{
			pfather->pleft=ptemp;
		}
		else if(label==-1)
		{
			pfather->pright=ptemp;
		}

		if(ptemp->pright!=NULL)
		{
			pnode->pright=ptemp->pright;
			ptemp->pright=pnode;
		}
	}

   
	pnode=ptemp;
	return ptemp;

}*/


/*ENODE* CExpression::MoveBranch(ENODE *pfather,ENODE *pnode)
{
	// move	a branch in the syntax tree;
	// "pnode" the begining position to search 
	// and move, "pfather" pnode's father node;
	
	static int num;

	int label=0;
	if(pfather!=NULL && pfather->pleft!=NULL && pfather->pleft->info==MAX)
	{
		label=1;
		pfather->pleft->info=0;

	}

	if(pfather!=NULL && pfather->pright!=NULL && pfather->pright->info==MAX)
	{
		label=-1;
		pfather->pright->info=0;
	}
    
	pnode->info=2*MAX; 


	ENODE *ptempf=NULL;
	ENODE *ptempff=NULL;
	ENODE *ptemp=FindFirstAddSub(pnode,ptempf);
    ptempf=FindFather(ptemp,ptempf,pnode);
    while(ptemp!=NULL)
	{
		if(ptempf->info==2*MAX)
		{
			#ifdef DEBUG
			AfxMessageBox("enter into move branch");
			#endif

			ptemp->info=MAX;

			ptempf->info=0;
			
			if(ptempf && ptempf->pleft && ptempf->pleft->info==MAX)
			{
				ptemp->info=0;
				
				if((AOpClassify(ptempf)==1||AOpClassify(ptempf)==2) && ptempf->pleft && AOpClassify(ptempf->pleft)<0)
				{
					ptempf->info=MAX;
					ptempf=LeftChildStandard(pfather,ptempf);
					pnode=ptempf;
					pnode->info=2*MAX; 
					b_modified=true;
					
				}
				else if(ptempf && ptempf->pright && ptempf->pright->info==MAX)
				{
					ptemp->info=0;
					if(AOpClassify(ptempf)==1 && ptempf->pright  && AOpClassify(ptempf->pright)<0)
					{
						ptempf=RightChildStandard(pfather,ptempf);
						pnode=ptempf;
						pnode->info=2*MAX;
						b_modified=true;
					}
				}
			}// if(ptempf && ptempf->pleft &&..... 	
			
		}
		else
		{
			ptempff=FindFather(ptempf,ptempff,pnode);
			ptemp->info=MAX; 
			if(ptempf && ptempf->pleft && ptempf->pleft->info==MAX)
			{
				ptemp->info=0; 
				if((AOpClassify(ptempf)==1||AOpClassify(ptempf)==2)  && ptempf->pleft && AOpClassify(ptempf->pleft)<0)
				{
					ptempf->info=MAX;
					ptempf=LeftChildStandard(ptempff,ptempf);
					b_modified=true;
				}
			}
			else if(ptempf && ptempf->pright && ptempf->pright->info==MAX)
			{
				ptemp->info=0;
				if(AOpClassify(ptempf)==1 && ptempf->pright  && AOpClassify(ptempf->pright)<0)
				{
					ptempf=RightChildStandard(ptempff,ptempf);
					b_modified=true;
				}
			}// if(ptempf && ptempf->pleft &&..... 	
			
						
		}// if(ptempf->info==2*MAX)


		while(AOpClassify(pnode)<0)
		{
			pnode->info=0; 
			pnode=pnode->pleft;
			pnode->info=2*MAX; 
			
		}

		
        ptemp=FindFirstAddSub(pnode,ptempf);
		ptempf=FindFather(ptemp,ptempf,pnode);

	}

		 
	#ifdef DEBUG
    AfxMessageBox("return from branch moved");
	#endif
	
	pnode->info=0;
	return pnode;
	
}*/

/*ENODE* CExpression::FindFirstAddSub(ENODE *pnode,ENODE *pfather)
{
	// find the first "+" or "-" operator from syntax tree pnode
	// and return it;

	CList<ENODE *,ENODE *> STK;
	ENODE *p=NULL,*ptemp=NULL;
	
	if(pnode==NULL)
	{
		return NULL;
	}

	p=pnode;
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// 输出p
			if(AOpClassify(p)<0)
			{
				ENODE *pf=NULL;
				if(STK.GetCount()>0)
				{
					pf=STK.GetTail(); 
				}
				p->info=MAX; 
				pfather=SelectFather(p,ptemp,pf);
				p->info=0; 
				return p;
			}
						
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			p=STK.GetTail()->pright;
			ptemp=STK.GetTail(); 
			STK.RemoveTail();			
		}


	}
	return NULL;
}*/


/*ENODE* CExpression::MoveRightBranch(ENODE *pfather,ENODE *pnode)
{
	// not used

	static int num;
    int label=0;
	ENODE* ptemp=NULL;

	if(pfather!=NULL )
	{
		if(pfather->pleft!=NULL && pfather->pleft->info>=MAX)
		{
			label=1;
		}

		if(pfather->pright!=NULL && pfather->pright->info>=MAX)
		{
			label=-1;
		}

		#ifdef DEBUG
		CString str;
		str.Format("label=%d,pnode->info=%d",label,pnode->info); 
		AfxMessageBox(str);
		#endif
	}
    
	pnode->info=0;	

	ptemp=pnode->pright;

	#ifdef DEBUG
	AfxMessageBox("enter into standard MoverightBranch");
	#endif

	if(pfather==NULL)
	{
		if(ptemp->pright!=NULL)
		{
			pnode->pright=ptemp->pright;
			ptemp->pright=pnode; 
		}
	}
	else
	{
		if(label==1)
		{
			pfather->pleft=ptemp;
		}
		else if(label==-1)
		{
			pfather->pright=ptemp;
		}

		if(ptemp->pright!=NULL)
		{
			pnode->pright=ptemp->pright;
			ptemp->pright=pnode;
		}
	}


    pnode=ptemp;
	return ptemp;
}*/

/*ENODE* CExpression::FindFather(ENODE *pson,ENODE *pfather,ENODE *pHead)
{
	// find the node pson from the tree pHead,and return it's father
	// pfather

	CList<ENODE *,ENODE *> STK;
	ENODE *p=NULL;

	if(pson==NULL || pHead==NULL)
	{
		return NULL;
	}

	int info=pson->info;
	pson->info=-MAX;
    
	p=pHead;
	
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// 输出p
			if(p->pleft &&p->pleft->info==-MAX) 
			{
				pfather=p;
				p->pleft->info=info;
				return p;
			}

			if(p->pright && p->pright->info==-MAX)
			{
				pfather=p;
				p->pleft->info=info;
				return p;
			}
									
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			p=STK.GetTail()->pright;
			STK.RemoveTail();			
		}
	}
	pson->info=info;
	return NULL;
}*/


/*ENODE* CExpression::SelectFather(ENODE *pnode,ENODE * ptemp1,ENODE *ptemp2)
{

	/*********************************************************
	 select the node pnode's father node from the two nodes 
	 ptemp1 and ptemp2,and return it 
    *********************************************************/

	/*if(ptemp1&&ptemp1->pleft&&ptemp1->pleft->info==MAX)
	{
		return ptemp1;
	}

	if(ptemp1&&ptemp1->pright&&ptemp1->pright->info==MAX)
	{
		return ptemp1;
	}

	if(ptemp2&&ptemp2->pleft&&ptemp2->pleft->info==MAX)
	{
		return ptemp2;
	}

	if(ptemp2&&ptemp2->pright&&ptemp2->pright->info==MAX)
	{
		return ptemp2;
	}

	return NULL;


}*/
///////////////////////////////////////////////////////////////////////////
ENODE *CopyExpTreeEx(ENODE *pnew,ENODE *pint)
{
	CExpression cexp;
	pnew=cexp.CopyTree(pnew,pint); 
//fxMessageBox(C_ALL_KEYWORD[pint->T.key]);

	return pnew;

}

ENODE * CExpression::CopyTree(ENODE *pnew,ENODE *pint)
{
	/******************************************************
	 FUNCTION:
	 copy a tree "pint" to "pnew" and return the new tree's
	 pointer;
	 PARAMETERS:
	 pint : point to the initial tree.
     pnew : point to the new tree.
	*******************************************************/
	
	
	if(pint==NULL)
	{
		return NULL;
	}
    static int num;
	num++;
	
	CList<ENODE *,ENODE *> STK;
	CList<ENODE *,ENODE *> STACK;
	ENODE *p=NULL,*ptr=NULL,*ptemp=NULL;
	int label=0;
	
	p=pint;
	ptr=pnew;
	
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// add action here
            ptr=new ENODE;
			ptr->info=0; 
			ptr->T.deref=0;//wtt 2008.3.19
			ptr->pleft=NULL;
			ptr->pright=NULL;
			for(int i=0;i<10;i++)
			{
				ptr->pinfo[i]=NULL; 				
			}

			ptr=CopyENODE(ptr,p);
			//end
            
			if(label==-1)
			{
				ptemp->pright=ptr; 
			}
			else if(label==1)
			{
				if(STACK.GetCount()>0)
				{
					STACK.GetTail()->pleft=ptr;

				}

			}
			else if(label==0)
			{
				pnew=ptr;
			}


			STK.AddTail(p);
			STACK.AddTail(ptr); 
             
			p=STK.GetTail()->pleft;  
			label=1;  
		}
		else
		{
			p=STK.GetTail()->pright;
			ptemp=STACK.GetTail(); 

			STACK.RemoveTail(); 
			STK.RemoveTail();

			label=-1;
		}
	}

	return pnew;
    
}
/*ENODE * CExpression::CopyTree(ENODE *pnew,ENODE *pint)
{
	if(pint==NULL)
	{
		return NULL;
	}
    static int num;
	num++;
	ENODE *p=NULL,*ptr=NULL,*ptemp=NULL;
	int label=0;
	
	p=pint;
	ptr=pnew;

	ptr=new ENODE;
	ptr->info=0; 
	ptr->T.deref=0;//wtt 2008.3.19
	ptr->pleft=NULL;
	ptr->pright=NULL;
	for(int i=0;i<10;i++)
	{
				ptr->pinfo[i]=NULL; 				
	}

	ptr=CopyENODE(ptr,p);

	if(pint->pleft)
	{
		ptr->pleft=CopyTree(ptr->pleft,pint->pleft);
	}
	if(pint->pright)
	{
		ptr->pright=CopyTree(ptr->pright,pint->pright);
	}
	return ptr;



}*/
ENODE *CExpression::CopyENODE(ENODE *pnew,ENODE *pint)
{

	// copy node pint to pnew and return it;

	if(pnew==NULL || pint==NULL)
	{
		return NULL;
	}

	pnew->info=pint->info;
	pnew->T.deref=pint->T.deref;
	pnew->pleft=pint->pleft;
	pnew->pright=pint->pright;    
	for(int i=0;i<10;i++)
	{
		pnew->pinfo[i]=NULL; 
	}

	CopyTNODE(pnew->T,pint->T);
	
	int info=pnew->info;//wtt 2008.3.31
	if(pnew->info<0)
		info=-pnew->info;
	//for(i=0;i<pnew->info && i<10;i++)//wtt 2008.3.31
	int i;
	for(i=0;i<info && i<10;i++)//wtt 2008.3.31
	{
		pnew->pinfo[i]=CopyTree(pnew->pinfo[i],pint->pinfo[i]); 
        
	}
	return pnew;

}
ENODE *ExCopyENODE(ENODE *pnew,ENODE *pint)
{
	CExpression cexp;
	pnew=cexp.CopyENODE(pnew,pint); 

	return pnew;

}
int  CExpression::AOpClassify(ENODE *p)
{
	// classify the operators 

	if(!p)
	{
		return 0;
	}

	//if(p->T.key==CN_FORMAT || p->T.key==CN_MULTI || p->T.key==CN_DIV)
	
	if( p->T.key==CN_MULTI )//wtt//3.2
	
	{
		return 1;
	}
	if( p->T.key==CN_DIV)//wtt//3.2
	{
		return 2;
	}
	if(p->T.key==CN_FORMAT)//wtt//3.3
	{
		return 3;

	}


	if(p->T.key==CN_ADD)
	{
		return -1;
	}
	if( p->T.key==CN_SUB)
	{
		return -2;
	}

	return 0;
}

/*void   CExpression::AdvancedAEStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{

	/*************************************************************************

	  FUNCTION:
	  Advanced standard the Arithmetic Expression:standard the syntax sub-tree
	  whose head node is "*" ,"/" or"%",father node is "+"or "-",in function 
	  ReMoveParenth we only let the operators "+" or "-" exclude from parenthesis  
	  but the "*" "/" "%" are not,this function will complete this work
	  PARAMETERS:
	  pHead : syntax tree's head
	  SArry : ...

	*************************************************************************/
	
/*	CList<ENODE *,ENODE *> STK;
	ENODE *p=NULL;

	if( pHead==NULL)
	{
		return ;
	}

	p=pHead;
	
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// action
			if(AOpClassify(p)==2 && p->pright &&(AOpClassify(p->pright)==1||AOpClassify(p->pright)==2) )
			{//Rule ：A1/(A2/A3)→A1*A3/A2 Rule：A1/(A2*A3)→A1/A2/A3

				ENODE *pleft=NULL;
				ENODE *pright=NULL;
				                
				pleft=p;
				pright=p->pright; 

				if(p->T.key==CN_DIV)
				{
					if(p->pright->T.key==CN_MULTI)
					{//A1/(A2*A3)→A1/A2/A3
						pright->T.key=CN_DIV;  
						////////////////wtt//////3.2////////////
						p->pright=pright->pright;
						pright->pright=pright->pleft;
						pright->pleft=p->pleft;					
						p->pleft=pright;
						b_modified=true;


						///////////////////wtt//////////////

					}
					if(p->pright->T.key==CN_DIV)
					{//A1/(A2/A3)→A1*A3/A2
						pright->T.key=CN_MULTI; 							
						p->pright=pright->pleft;
						pright->pleft=p->pleft;
						p->pleft=pright;
						b_modified=true;
					}
					

				}
				
			//	p->pright=pright->pleft;
			//	pright->pleft=p->pleft;
			//	p->pleft=pright;				
			}
			////////////////wtt/////////3.2///3.3/////////////////
			else if(AOpClassify(p)==-1 && p->pright &&AOpClassify(p->pright)<0)
			{//A1+(A2+A3)→A1+A2+A3 A1+(A2-A3)→A1+A2-A3


			
				ENODE *pright=NULL;
				                
			
				pright=p->pright; 

				p->T.key=pright->T.key;					
				pright->T.key=CN_ADD;  
				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;

			}
			else if(AOpClassify(p)==-2 && p->pright &&AOpClassify(p->pright)<0)
			{//Rule ：A1-(A2-A3)→A1-A2+A3 Rule ：A1-(A2+A3)→A1-A2-A3

				ENODE *pright=NULL;
			
				pright=p->pright; 

				if(pright->T.key==CN_ADD)
				{
					p->T.key=CN_SUB;

				}
				else if(pright->T.key==CN_SUB)
					p->T.key=CN_ADD;

							
				pright->T.key=CN_SUB;  

				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;
			}
			else if(AOpClassify(p)==1 && p->pright &&(AOpClassify(p->pright)==1||AOpClassify(p->pright)==2))
			{//Rule ：A1*(A2*A3)→A1*A2*A3 Rule ：A1*(A2/A3)→A1*A2/A3

				
				
				ENODE *pright=NULL;
				                
			
				pright=p->pright; 

				p->T.key=pright->T.key;					
				pright->T.key=CN_MULTI;
				
				p->pright=pright->pright ;
				pright->pright=pright->pleft;
				pright->pleft=p->pleft;
				p->pleft=pright;
				b_modified=true;

			}
			else if((AOpClassify(p)==1&&p->pleft&&AOpClassify(p->pleft)==2)||(AOpClassify(p)==-1&&p->pleft&&AOpClassify(p->pleft)==-2))
			{//A1/A3*A2→A1*A2/A3；A1-A2+A3→A1+A3-A2
				int temp;
				temp=p->T.key;
				p->T.key=p->pleft->T.key;
				p->pleft->T.key=temp ;
				ENODE *pright=NULL;
				pright=p->pright;
				p->pright=p->pleft->pright;
				p->pleft->pright=pright;
				b_modified=true;

			}
		



			////////////wtt///////////////////////////////////////////////
			else
			{
				STK.AddTail(p);
				p=STK.GetTail()->pleft;
			}

		}
		else
		{
			p=STK.GetTail()->pright;
			STK.RemoveTail();			
		}
	}

}*/

void   CExpression::AEOrderStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	
}

                            /******************************
                                the above function group:
							standard the expression based on
							synax tree.
                            ******************************/
//@@@@@@@@@@@@@#############$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&***********//

void CExpression::CallParameter(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry)
{
	/*************************************************************************

	  FUNCTION:
	  Translate function paprameter(s) to syntax tree(s);	  
	
	*************************************************************************/
	if(ptr==NULL)
		return;
	CSDGCall *pCall=(CSDGCall *)ptr;
    if(TEXP.GetSize()<=0)
	{
		/////////wtt///5.13////函数/没有实参/////////////
		//ptr->m_pinfo=NULL; 
		pCall->m_pinfo=new ENODE;
		CopyTNODE(pCall->m_pinfo->T,pCall->m_tFun);
		pCall->m_pinfo->info=0;  
//		pCall->m_pinfo->T.deref=0; 
		pCall->m_pinfo->pleft=NULL;
		pCall->m_pinfo->pright=NULL;
		for(int i=0;i<10;i++)
		{
			pCall->m_pinfo->pinfo[i]=NULL;  
		}
	//	AfxMessageBox(pCall->m_tFun.name +  " texp.getsize==0");
		//////////////////////////////////////////////////////////////
		return;
	}
	//AfxMessageBox(pCall->m_tFun.name+"has para");////
	//else{wtt///5.13/
		pCall->m_pinfo=new ENODE;
		CopyTNODE(pCall->m_pinfo->T,pCall->m_tFun);
		pCall->m_pinfo->info=0;  
//		pCall->m_pinfo->T.deref=0; 
		pCall->m_pinfo->pleft=NULL;
		pCall->m_pinfo->pright=NULL;
		for(int i=0;i<10;i++)
		{
			pCall->m_pinfo->pinfo[i]=NULL;  
		}
//     }wtt///5.13/

	
	int ix=0;
	int count=0;
	CSDGBase sTemp;
	while(ix<TEXP.GetSize()&&count<10)
	{
		sTemp.TCnt.RemoveAll();
		bool flag=true;
		while(ix<TEXP.GetSize()&&flag)
		{
			 
			sTemp.TCnt.Add(TEXP[ix]);
			///////////////wtt/////////5.19///////////////////
		   if(TEXP[ix].key==CN_DFUNCTION||TEXP[ix].key==CN_BFUNCTION)
			{//参数是函数调用
				ix++;
				if(ix>=TEXP.GetSize())
				{
					return;					
				}

				if(TEXP[ix].key!=CN_LCIRCLE)
				{
					return;
				}
				sTemp.TCnt.Add(TEXP[ix]);

				int cno=1;
				ix++;
				while(ix<TEXP.GetSize()&&cno!=0)
				{
					sTemp.TCnt.Add(TEXP[ix]);
					if(TEXP[ix].key==CN_RCIRCLE)
					{
					//	AfxMessageBox(")");
						cno--;
					}
					else if(TEXP[ix].key==CN_LCIRCLE)
						cno++;
					ix++;
				}
			
				if(cno!=0)
					return;
					//AfxMessageBox("ok");///

				flag=false;
			}
			ix++;
			if(ix<TEXP.GetSize()&&TEXP[ix].key==CN_DOU)
			{
				flag=false;

			}
		}
			///////////////wtt/////////5.19///////////////////

		// parameter to tree
	
		AExpToTree(sTemp.TCnt,&sTemp,SArry);
		pCall->m_pinfo->pinfo[count]=sTemp.m_pinfo;

		count++;
		ix++;
	}
	
	pCall->m_pinfo->info=count;
	

}

void CExpression::DeleteExpTree(CSDGBase *ptr)
{   
	/******************************************************

	    FUNCTION:
		post-order traverse the syntax tree and delete it 
			    
		PARAMETER:
		ptr : Unit node which inludes the syntax tree to 
		      delete .
		
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=NULL;

	p=ptr->m_pinfo;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;

				if(p->info>0 && p->info<10 && p->T.key==CN_VARIABLE)
				{
					CSDGBase SDGTemp;
					for(int i=0;i<10 && p->pinfo[i] ; i++)
					{
						SDGTemp.m_pinfo=p->pinfo[i];
						DeleteExpTree(&SDGTemp);
						p->pinfo[i]=NULL; 
					}
				}
				if(p)
				delete p;
				p=NULL;

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);

	ptr->m_pinfo=NULL;
}

CString CExpression::TraverseAST(ENODE*pHead,CArray<SNODE,SNODE> &SArry)
{//中根序遍历抽象语法树
CString s="",stemp="";
ENODE*p=pHead;

if(p==NULL)
return stemp;



	if(p->pright==NULL)
	{
		if((p->T).key>0&&(p->T).key<132)
		{
			switch((p->T).key)
			{
		
			case CN_CINT:
				s.Format("%d",(p->T).addr); 
				//AfxMessageBox(s);
				stemp+=s;
				break;
			case CN_CFLOAT:
				s.Format("%f",(p->T).value);
				//	AfxMessageBox(s);
				stemp+=s;
			case CN_VARIABLE:
			case CN_CSTRING:
			case CN_DFUNCTION:
			case CN_BFUNCTION:
					//////////wtt 2008.3.18////////////////*指针//根据deref记得的值输出*/
				if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_POINTER)
				{	
					//AfxMessageBox("pointer ok");//////////
				
			
					for(int m=0;m<p->T.deref;m++)
					{
					//	AfxMessageBox("ok");//////////
						stemp="*"+stemp;
					}

				}
				if(p->T.key==CN_VARIABLE&&p->T.deref==-1)
				{
					
				//		AfxMessageBox("address of pointer ok");///////
				//			AfxMessageBox((p->T).name);
					stemp+="&";

				}
				///////////////////wtt 2008.3.18//////////////////////////


				stemp+=(p->T).name;
				//	AfxMessageBox((p->T).name);
				break;
			default:
	
				int key=(p->T).key;
			//AfxMessageBox(C_ALL_KEYWORD[key]);
			 stemp+=C_ALL_KEYWORD[key];
			}
		}
		else
			stemp+="$";
	/////////////////////wtt////3.2
//		if(p->T.key==CN_VARIABLE&&SArry[p->T.addr].kind==CN_ARRAY||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)
		if(p->T.key==CN_VARIABLE&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_POINTER)||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)// WTT 2008 0318

		{
			int i=0;
			CString str;
			if(p->T.key==CN_VARIABLE)
				str="";
			else
				str="(";
			while(i<10&&p->pinfo[i]!=NULL)
			{
			//	AfxMessageBox(p->T.name);

				if(p->T.key==CN_VARIABLE)
					str+="[";
				
				str+=TraverseAST(p->pinfo[i],SArry);

				if(p->T.key==CN_VARIABLE)
					str+="]";
				else 
					str+=",";

				i++;

			}

		if(p->T.key==CN_VARIABLE)
				str+="";
		else
		{
			str.SetAt(str.GetLength()-1,' ');
			str+=")";
		}
				
			stemp+=str;

		}
		if(p->pleft)
		{
		//	AfxMessageBox("p->pleft");
			stemp+=TraverseAST(p->pleft,SArry);
		
		}
		return stemp;


	}

	
	if(p->pleft)
	{
	//	AfxMessageBox("p->pleft");
		stemp+="("+TraverseAST(p->pleft,SArry);
		
	}
	if((p->T).key>0&&(p->T).key<132)
	{
		switch((p->T).key)
		{
		
		case CN_CINT:
			s.Format("%d",(p->T).addr); 
			//AfxMessageBox(s);
			stemp+=s;
			break;
		case CN_CFLOAT:
			s.Format("%f",(p->T).value);
			//	AfxMessageBox(s);
			stemp+=s;
		case CN_VARIABLE:
		case CN_CSTRING:
		case CN_DFUNCTION:
		case CN_BFUNCTION:
								//////////wtt 2008.3.18////////////////*指针//根据deref记得的值输出*/
				if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_POINTER)
				{	
					//AfxMessageBox("pointer ok");//////////
				
			
					for(int m=0;m<p->T.deref;m++)
					{
					//	AfxMessageBox("ok");//////////
						stemp="*"+stemp;
					}

				}
				if(p->T.key==CN_VARIABLE&&p->T.deref==-1)
				{
					
				//		AfxMessageBox("address of pointer ok");///////
				//			AfxMessageBox((p->T).name);
					stemp+="&";

				}
				///////////////////wtt 2008.3.18//////////////////////////

			stemp+=(p->T).name;
			//	AfxMessageBox((p->T).name);
			break;
		default:
	
			int key=(p->T).key;
			//AfxMessageBox(C_ALL_KEYWORD[key]);
            stemp+=C_ALL_KEYWORD[key];
		}
	}
	else
			stemp+="$";
	/////////////////////wtt////3.2
	if(p->T.key==CN_VARIABLE&&SArry[p->T.addr].kind==CN_ARRAY||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)
	{
	// AfxMessageBox("array function");
		int i=0;
		CString str;
			if(p->T.key==CN_VARIABLE)
				str="";
			else
				str="(";
		while(i<10&&p->pinfo[i]!=NULL)
		{
			if(p->T.key==CN_VARIABLE)
				str+="[";
			
			str+=TraverseAST(p->pinfo[i],SArry);

				if(p->T.key==CN_VARIABLE)
				str+="]";
			else 
				str+=",";

			i++;

		}
		if(p->T.key==CN_VARIABLE)
				str+="";
		else 
		{	str.SetAt(str.GetLength()-1,' ');
			str+=")";
		}
		stemp+=str;

		}

	///////////////////////////
	if(p->pright)
	{ //   AfxMessageBox("p->pright");
		stemp+=TraverseAST(p->pright,SArry)+")";
	}
	return stemp;

}

/*************************************************************************
                    global fucntion
              print expression synax tree
*************************************************************************/
CString PreOrderPrint(ENODE *pHead,CFile &F,CArray<SNODE,SNODE> &SArry)
{   return "";
	CList<ENODE *,ENODE *> STK;
	ENODE *p;
	
	if(pHead==NULL)
	{
		return "";
	}

	p=pHead;
	CString str,s;
	F.Write(s,s.GetLength() ); 
   	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// 输出p
			
			str+="              (";
			str+=GetENodeStr(p,SArry);
			str+="left:";
			str+=GetENodeStr(p->pleft,SArry);
			str+="right:";
			str+=GetENodeStr(p->pright,SArry);
			str+=")\r";
			F.Write(str,str.GetLength());
			//end
			AfxMessageBox(str);
            str="";
			
			if(p->info>=0&&(p->T.key==CN_VARIABLE||p->T.key==CN_BFUNCTION||p->T.key==CN_DFUNCTION))
			{
				for(int k=0;k<10&&p->pinfo[k] ;k++)
				{
					F.Write("               <<<<<index of "+p->T.name+"\r",30+p->T.name.GetLength());
//					PreOrderPrint(p->pinfo[k],F,SArry);
					F.Write("               index end>>>>>"+p->T.name+"\r",30+p->T.name.GetLength());
				}			
			}
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			p=STK.GetTail()->pright;
			STK.RemoveTail();			
		}


	}
	return "";


}

/*CString GetENodeStr(ENODE *p)
{
	CString str,s;
	
	if(p==NULL)
	{
		return "NULL        \t";
	}

	switch(p->T.key)
		
	{							 	
	case CN_CFLOAT:
		s.Format("%f\t\t   ",p->T.value);
		str+=s;
		break;
	case CN_CINT:
	case CN_CCHAR:
		s.Format("%d\t\t   ",p->T.addr);
		str+=s;
		break;
	case CN_ADD:
		str+="+\t\t   ";
		break;
	case CN_SUB:
		str+="-\t\t   ";
		break;
	case CN_MULTI:
		str+="*\t\t   ";
		break;
	case CN_DIV:
		str+="/\t\t   ";
		break;
	case CN_FORMAT:
		str+="%\t\t   ";
		break;
	case CN_GREATT:       // >
		str+=">\t\t   ";
		break;
	case CN_LESS:         // <
		str+="<\t\t   ";
		break;
	case CN_LANDE:        // <= 
		str+="<=\t\t   ";
		break;
	case CN_BANDE:        // >=
		str+=">=\t\t   ";
		break;
	case CN_NOTEQUAL:     // !=
		str+="!=\t\t   ";
		break;
	case CN_DEQUAL:       // == 
		str+="==\t\t   ";
		break;
	case CN_DAND:
		str+="&&\t\t   ";
		break;
	case CN_DOR:
		str+="||\t\t   ";
		break;
	case CN_NOT:
		str+="!\t\t   ";
		break;
	case CN_DFUNCTION:
	case CN_BFUNCTION:
	case CN_CSTRING:
		s.Format("%s\t",p->T.name,p->info); 
		str+=s;
		break;
	case CN_VARIABLE:
        s.Format("%6s [%4d]v=%4d ",p->T.name,p->info,(int)p->T.value  ); 	
		str+=s;
	default:
		;
	}
	

	return str;
}*/

void PostOrderPrint(ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the syntax tree; standard the 
		arithmetic expression
	    
		PARAMETER:
		pHead : Head of arithmetic expression.
		SArry : ...
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=NULL,*pft=NULL;

	p=pHead;
	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();


			STK.RemoveTail();			
			SBL.RemoveTail(); 


			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;
				
				//end;
				break;
			}

		}

	}while(STK.GetCount()>0);

	
	/********************end of this function********************/
}

