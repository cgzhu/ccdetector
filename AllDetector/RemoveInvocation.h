// RemoveInvocation.h: interface for the RemoveInvocation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REMOVEINVOCATION_H__635EE168_233E_4080_A0FC_9E4D51741622__INCLUDED_)
#define AFX_REMOVEINVOCATION_H__635EE168_233E_4080_A0FC_9E4D51741622__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Unit.h"

class RemoveInvocation  
{

	friend void ExBuildCallGraph(FCNODE *& pHead,CSDGBase *pUSDGHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);

public:
	RemoveInvocation();
	virtual ~RemoveInvocation();
	void operator()(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
protected:
	void SetInitAddr(CArray<SNODE,SNODE> &SArry);

	void preatreatment(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void RemoveExpInReturn(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);
	void RemoveCallInExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);
	bool FindFun(ENODE*pE,ENODE*pT);
	void FindCall(ENODE*pExp,CArray<ENODE*,ENODE*>&pCList,CArray<ENODE*,ENODE*> &pFList,CArray<int,int>&posList);
	void RemoveParaExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);
//	int IsSimple1(ENODE*p);/////////////////
	void InitialENODE(ENODE *pENODE);
	bool BuildCallGraph(FCNODE *&pHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);

	bool FuncTree(int addr,FCNODE* p,FCNODE*pf,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);
	void  InitialFCNODE(FCNODE*p);
	void DeleteCallTree(FCNODE*&pCTHead);
	void BuildSDGList(CSDGBase *pUSDGHead,CArray<CSDGBase*,CSDGBase*>&SDGList);

	void BuildCallPosList(CSDGBase *pUSDGHead,CSDGBase*CallPosList[20]);

	void SetDataFlow(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);


	//////////////////////////////设置def和ref集合//////////////
	// Set or search in-data flow and out-data flow
	void 	RemoveInitFLow(CSDGBase *pHead);

	void SetDefAndRef(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	void SearchChildrenDataFlow(CSDGBase *pFnd,CArray<SNODE,SNODE> &SArry);
	void SearchExpReferData(CSDGBase *pFnd,ENODE *pHead,CArray<TNODE,TNODE> &DArry,CArray<SNODE,SNODE> &SArry);
	void SetFunctionDefinedData(CSDGBase *pFnd,ENODE *pHead,CArray<SNODE,SNODE> &SArry);
//	void GetSonsDataInfo(CSDGBase *pNode);
	bool FindTNODEInDArry(TNODE &TD,CArray<TNODE,TNODE> &DArry);
	int  DefOrRefVariable(TNODE &TD,CSDGBase *pNode);
	int  DefOrRefVariableEx(TNODE &TD,CSDGBase *pNode);
	///////////////////////////设置def和ref集合/////////////////////////
	void SetGen(CSDGBase *pHead);
	void SetKill(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	void FindLeftSDGNode(CSDGBase*p,CArray<CSDGBase*,CSDGBase*>&prList);
	void DetectKill(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);
	void TraverseDetectKill(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);

	void IterateSetInAndOut(CSDGBase *pHead);
	void SetInAndOut(CSDGBase *pHead);
	CSDGBase* GetPrior(CSDGBase *pHead);
	void SetOut(CSDGBase *p);
	CSDGBase* GetRightChild(CSDGBase *p);
	bool IsFlowNODEInArry(FlowNODE TF,CArray<FlowNODE,FlowNODE> &DArry);

	void SetDataDependence(CSDGBase *p,CSDGBase*pHead);
	CSDGBase* FindSDGNODE(int SDGID,CSDGBase*pHead);

	void SetDecDependence(CSDGBase *p,CSDGBase*pHead);
	CSDGBase* FindDecNODE(int addr,CSDGBase*pHead);

	/////////////删除未被引用的变量赋值和声明语句//////////////////



	void  DeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);////3.17

	void RemoveNotReferedVar(CSDGBase *pHead);
	void RemoveNotReferedAssignment(CSDGBase *p,CSDGBase *pHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE> &FArry);
	void DeleteIdentity(CSDGBase *p,CArray<SNODE,SNODE>&SArry);

	
	void RemoveSimpleStatement(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE> &FArry);

	void RemoveVarStatement(CSDGBase*p,CArray<SNODE,SNODE>&SArry);
	void RemoveConstantStatement(CSDGBase*p,CArray<SNODE,SNODE>&SArry);
	void ReplaceVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry);
	void ReplaceArrVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry);

//	int CVarRemove::IsSimple(ENODE*p);
//	bool ContainVar(ENODE*p ,ENODE*pExp,CArray<SNODE,SNODE>&SArry);
//	void ReplaceVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry);
	void DeleteExpTree(ENODE *ptr);


	/////////////删除未被引用的变量赋值和声明语句//////////////////


	//////////////////////////////变量重命名////////////////////////
	void FindVariableInSDG(CString name,TNODE &TD,CSDGBase *pUSDGHead);
	void VariablesRename(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	void GetFrequency(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void SetFrequencyInfo(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void FrequencyOfExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &Freq);
	void ResetVarName(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void AssignNewName(CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	void RenameVarInExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq);
	bool isIncOrDec(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);
	void CopyTNODE(TNODE &T,TNODE &TINT);
	bool isConstOfE(ENODE *p);





	// end of this part
//////////////////////////////变量重命名////////////////////////


	////////////////////////重排语句的顺序//////////////////////

	void SetSDGNodeName(CSDGBase *pSHead, CArray<SNODE,SNODE>&SArry);//wtt//2006.3.21
//	void SetSDGNodeName(CSDGBase *pSHead, CArray<SNODE,SNODE>&SArry);


	void ResetStatementOrder(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry);
	void ResetChildrenOrder(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);
	void ResetRangeOrder(const int bpos,const int epos,CSDGBase *pNode,CArray<SNODE,SNODE> &SArry);	
	bool IsDepended(CSDGBase *pNode1,CSDGBase *pNode2);
	CSDGBase *SearchFirstCertainNode(CSDGBase *pSDG,CString strNode);
////////////////////////重排语句的顺序//////////////////////

	//////////////////以下为//////函数内联操作//////////////////
void Inline(FCNODE*p,FCNODE*pf,CSDGBase*pHead,int i,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
void ModifyPDG(CSDGBase*p,CSDGBase*pf,CSDGBase*pUSDGHead,int num,	CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
CSDGBase*CopyPDG(CSDGBase*p);
CString PrintPDG(CSDGBase *pHead,CArray<SNODE,SNODE>&SArry);
void RenamePDG(CSDGBase*p,CSDGBase*pSDGf,int num,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
void RenameVar(CSDGBase*p,CArray<SNODE,SNODE> &SArry,CArray<AddrNODE,AddrNODE>&AddrList);
void RenameVarInExp1(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<AddrNODE,AddrNODE>&AddrList);
void AssignActuralToFormal(CSDGBase*p,CSDGBase*pf,int num,	CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
void PassParameter(CSDGBase*p,CArray<SNODE,SNODE> &SArry);
void MarkFormParameter(CSDGBase*pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
void PassReturnValue(CSDGBase*p,CSDGBase*pf,int num,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
void ModifyControlDependence(CSDGBase*pc,CSDGBase*pf,int num,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
//void ChangePDG(CSDGBase*p,CSDGBase*pHead,CArray<CSDGBase*,CSDGBase*>&SDGList,CArray<FNODE,FNODE> &FArry);

//void DeleteBranch1(CSDGBase * &phead);

void PostTreatment(CSDGBase*pUSDGHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE> &FArry);


void RemoveInvocation::FunctionRename(CSDGBase*pUSDGHead,CArray<CSDGBase*,CSDGBase*>&SDGList,CSDGBase*CallPosList[20][20],CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);















private:
	bool modified;
	bool change;
	bool deleteunrefered;//wtt 2008.3.28标记是否删除过未引用的变量或表达式


};
void ExBuildCallGraph(FCNODE *& pHead,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry);
static int	CallTreeNO=0;
#endif // !defined(AFX_REMOVEINVOCATION_H__635EE168_233E_4080_A0FC_9E4D51741622__INCLUDED_)
