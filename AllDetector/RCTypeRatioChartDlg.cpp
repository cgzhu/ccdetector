// RCTypeRatioChartDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "RCTypeRatioChartDlg.h"
#include "afxdialogex.h"
#include "CSeries.h"

// CRCTypeRatioChartDlg 对话框

IMPLEMENT_DYNAMIC(CRCTypeRatioChartDlg, CDialogEx)

CRCTypeRatioChartDlg::CRCTypeRatioChartDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRCTypeRatioChartDlg::IDD, pParent)
	, m_rc_type_ratio_chart()
{

}

CRCTypeRatioChartDlg::~CRCTypeRatioChartDlg()
{
}

//清除所有图线
void CRCTypeRatioChartDlg::ClearAllSeries() 
 {
     for(long i = 0;i<m_rc_type_ratio_chart.get_SeriesCount();i++)
     {
         ((CSeries)m_rc_type_ratio_chart.Series(i)).Clear();
     }
 }

void CRCTypeRatioChartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TCHART1, m_rc_type_ratio_chart);
}


BEGIN_MESSAGE_MAP(CRCTypeRatioChartDlg, CDialogEx)
END_MESSAGE_MAP()


// CRCTypeRatioChartDlg 消息处理程序
