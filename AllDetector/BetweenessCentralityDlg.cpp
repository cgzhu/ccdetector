// BetweenessCentralityDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "BetweenessCentralityDlg.h"
#include "afxdialogex.h"
#include "MainFrm.h"

// CBetweenessCentralityDlg 对话框

IMPLEMENT_DYNAMIC(CBetweenessCentralityDlg, CDialogEx)

CBetweenessCentralityDlg::CBetweenessCentralityDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBetweenessCentralityDlg::IDD, pParent)
{

}

CBetweenessCentralityDlg::~CBetweenessCentralityDlg()
{
}

void CBetweenessCentralityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, betweenessCentralityList);
}


BEGIN_MESSAGE_MAP(CBetweenessCentralityDlg, CDialogEx)
END_MESSAGE_MAP()


// CBetweenessCentralityDlg 消息处理程序


BOOL CBetweenessCentralityDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	DWORD dwStyle = betweenessCentralityList.GetExtendedStyle();   //添加列表框的网格线
    dwStyle |= LVS_EX_FULLROWSELECT;            
    dwStyle |= LVS_EX_GRIDLINES;                
    betweenessCentralityList.SetExtendedStyle(dwStyle);

	betweenessCentralityList.InsertColumn(0,"组号",LVCFMT_LEFT,100);    //添加列标题，设置列的宽度，LVCFMT_LEFT用来设置对齐方式
	betweenessCentralityList.InsertColumn(1,"Betweeness Centrality",LVCFMT_LEFT,150);

	//调用关键克隆代码识别算法
	BetweenessCentralityAlgorithm algorithm;
	algorithm.start();

	//在表中显示所有克隆代码组的Betweeness Centrality
	for(int i=0;i<ccGroupNumber;i++)
	{
		char num[256];
		itoa(i+1,num,10);
		int new_row = betweenessCentralityList.InsertItem(i,_T(num));

		CString rate_str;
		rate_str.Format("%f",ratio[i+1]);
		betweenessCentralityList.SetItemText(new_row, 1, rate_str);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
