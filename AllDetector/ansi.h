#ifndef ANSI_H
#define ANSI_H

#include <vector>
#include <iostream>
#include <fstream>
#include <queue>
#include <stack>
#include <string>
using namespace std;

#include <stdarg.h>

//节点类型
typedef enum
{
	typeKeyWord,		//关键字 0
	typeConstant,		//常量
	typeId,				//标识符
	typeOperator,		//操作符
	
	typeExpr,			//表达式（expression）
	typeDeclr,			//声明 declaration
	typeDec,			//declarator
	typeDreDec,			//direct_declarator
	typeAbsDec,			//abstract_declarator
	typeEnumrator,		//枚举内容
	typePt,				//指针
	typeInit,			//初始化
	
	typeLabelStm,		//标号语句
	typeCompStm,		//复合语句或块 13
	typeExprStm,		//表达式语句
	typeSelecStm,		//选择语句
	typeIterStm,		//循环语句
	typeJumpStm,		//跳转语句
	
	typeFuncDef,		//函数定义
	
	typeCastExpr,		//cast_expression
	typeUnaryExpr,		//一元表达式
	typeMulExpr,		//乘法类表达式
	typeAddExpr,		//加法类表达式
	typeShiftExpr,		//移位表达式
	typeRelaExpr,		//关系表达式
	typeEqualExpr,		//相等类表达式
	typeAndExpr,		//按位与表达式
	typeExOrExpr,		//按位异或表达式
	typeInOrExpr,		//按位或表达式
	typeLogicAndExpr,	//逻辑与表达式
	typeLogicOrExpr,	//逻辑或表达式
	typeConditExpr,		//条件表达式
	typeAssignExpr,		//赋值表达式
	
	typePostExpr,		//后缀表达式
	typeArguExprList,	//argument_expression_list
	typeDeclrSpec,		//declaration_specifiers
	typeInitDeclrList,	//init_declarator_list
	typeInitDec,		//init_declarator
	typeStructOrUnionSpe,	//struct_or_union_specifier
	typeStructDeclrList,	//struct_declaration_list
	typeStructDeclr,	//struct_declaration
	typeSpeQuaList,		//specifier_qualifier_list
	typeStructDecList,	//struct_declarator_list
	typeStructDec,		//struct_declarator
	typeEnumSpe,		//enum_specifier
	typeEnuList,		//enumerator_list
	typeTypeQuaList,	//type_qualifier_list
	typeParaTypeList,	//parameter_type_list
	typeParaList,		//parameter_list
	typeParaDeclr,		//parameter_declaration
	typeIdList,			//identifier_list
	typeTypeName,		//type_name
	typeDreAbsDec,		//direct_abstract_declarator
	typeInitList,		//initializer_list
	typeDesignation,	//designation
	typeDesList,		//designator_list
	typeDesignator,		//designator 指示符
	typeBlockItemList,	//block_item_list
	typeTranUnit,		//translation_unit
	typeDeclrList,		//declaration_list 
	typePriExpr,		//primary_expression

	typeOther,			//其他
	typeEntry,
	typeRegionNode,
}nodeType;

typedef struct node	//语法树节点类型
{
	nodeType type;	

	string str;
	
	int lineno;
	
	vector<node*> subNode;	//孩子节点
	
	struct node* parentNode;	//指向父节点
	
	vector<int> _subNode;
	int index;
} treeNode;

treeNode *new_treeNode(nodeType, char *s, int lineno);	//创建一个叶子节点 s:节点名字
treeNode *lianjie(nodeType, char *s, int num, ...);	//s:节点名字 num:孩子个数


void dump(treeNode* root);

#endif

