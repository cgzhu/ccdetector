// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "AllDetector.h"
#include "SetMinlineDlg.h"
#include "MainFrm.h"
#include "CntxtInconsisDetect.h"
#include <time.h>

//Polaris-20140716
#ifndef _FILE_EXTERN
#define _FILE_EXTERN
#endif
#include "filename.h"
#include "routine.h"

#include "FileIncludeDlg.h"
#include "CustomParamDlg.h"
#include "RCTypeDiagramDlg.h"
#include "RCFilesDiagramDlg.h"
#include "RCTypeRatioChartDlg.h"
#include "RCFilesColumnDlg.h"
#include "RCBugFormDlg.h"
#include "CSeries.h"
#include "CLegend.h"
#include "CCSegFileDlg.h"
#include "CCSegRatioDlg.h"
#include "CCLinesFileDlg.h"
#include "CodeGraphDlg.h"
#include "BetweenessCentralityDlg.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

//Polaris-20140830
int main_count_ideoper = 0;
int main_count_reduassign = 0;
int main_count_deadcode = 0;
int main_count_reducond = 0;
int main_count_hideoper = 0;

//Polaris-20140930
UINT CC_Detect_Thread(LPVOID lpParam);

//Polaris-20141008
map<string,int> cc_file_map;
map<string,int> cc_file_lines_map;

//Polaris-20150302
map<string,vector<int>> cc_filename_groupnumber_map;
int ccGroupGraph[100][100];							//克隆代码组关系图存储矩阵
int ccGroupNumber = 0;

vector<vector<CString>> rc_info_list;

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWndEx)
	ON_WM_CREATE()
	ON_COMMAND(ID_WINDOW_MANAGER, &CMainFrame::OnWindowManager)
	ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
	ON_WM_SETTINGCHANGE()
	ON_COMMAND(ID_OPEN_FOLDER, &CMainFrame::OnOpenFolder)
	ON_COMMAND(ID_SET_MINLINE, &CMainFrame::OnSetMinline)
	ON_COMMAND(ID_DETECT_ALL_CLONE, &CMainFrame::OnDetectAllClone)
	ON_COMMAND(ID_OUTPUT_REPORT, &CMainFrame::OnOutputReport)
	ON_COMMAND(ID_INCLUDE_DIR, &CMainFrame::OnIncludeDir)
	ON_COMMAND(ID_DEF_PARAMS, &CMainFrame::OnDefParams)
	ON_COMMAND(ID_DETECT_ALL_RC, &CMainFrame::OnDetectAllRc)
	//	ON_COMMAND(ID_PARSE, &CMainFrame::OnParse)
	ON_UPDATE_COMMAND_UI(ID_DETECT_ALL_RC, &CMainFrame::OnUpdateDetectAllRc)
	ON_UPDATE_COMMAND_UI(ID_DETECT_ALL_CLONE, &CMainFrame::OnUpdateDetectAllClone)
	ON_COMMAND(ID_RC_TYPE, &CMainFrame::OnRcType)
	ON_COMMAND(ID_RC_FILES, &CMainFrame::OnRcFiles)
	ON_COMMAND(ID_RC_TYPE_RATIO, &CMainFrame::OnRcTypeRatio)
	ON_COMMAND(ID_RC_FILES_COL, &CMainFrame::OnRcFilesCol)
	ON_COMMAND(ID_RC_FILES_FORM, &CMainFrame::OnRcFilesForm)
	ON_COMMAND(ID_CC_SEG_FILE_COL, &CMainFrame::OnCcSegFileCol)
	ON_COMMAND(ID_CC_SEG_FILE_PIE, &CMainFrame::OnCcSegFilePie)
	ON_COMMAND(ID_CC_LINES_FILE, &CMainFrame::OnCcLinesFile)
	ON_MESSAGE(WM_UPDATE_OUTPUT, &CMainFrame::OnUpdateOutput)
	ON_COMMAND(ID_SHOW_CODE_GRAPH, &CMainFrame::OnShowCodeGraph)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	Min_len = 4;
	testInt = 18;
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2008);

	//Polaris-20140716
	m_bBeginFindBugFolderThread = FALSE;
	m_bBeginFindBugThread = FALSE;

	//Polaris-20140930
	m_bBeginFindCCThread = FALSE;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	BOOL bNameValid;
	// set the visual manager and style based on persisted value
	OnApplicationLook(theApp.m_nAppLook);

	CMDITabInfo mdiTabParams;
	mdiTabParams.m_style = CMFCTabCtrl::STYLE_3D_ONENOTE; // other styles available...
	mdiTabParams.m_bActiveTabCloseButton = TRUE;      // set to FALSE to place close button at right of tab area
	mdiTabParams.m_bTabIcons = FALSE;    // set to TRUE to enable document icons on MDI taba
	mdiTabParams.m_bAutoColor = TRUE;    // set to FALSE to disable auto-coloring of MDI tabs
	mdiTabParams.m_bDocumentMenu = TRUE; // enable the document menu at the right edge of the tab area
	EnableMDITabbedGroups(TRUE, mdiTabParams);

	if (!m_wndMenuBar.Create(this))
	{
		TRACE0("Failed to create menubar\n");
		return -1;      // fail to create
	}

	m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

	// prevent the menu bar from taking the focus on activation
	CMFCPopupMenu::SetForceMenuFocus(FALSE);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(theApp.m_bHiColorIcons ? IDR_MAINFRAME_256 : IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	CString strToolBarName;
	bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
	ASSERT(bNameValid);
	m_wndToolBar.SetWindowText(strToolBarName);

	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	// Allow user-defined toolbars operations:
	InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	// TODO: Delete these five lines if you don't want the toolbar and menubar to be dockable
	m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndMenuBar);
	DockPane(&m_wndToolBar);


	// enable Visual Studio 2005 style docking window behavior
	CDockingManager::SetDockingMode(DT_SMART);
	// enable Visual Studio 2005 style docking window auto-hide behavior
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// Load menu item image (not placed on any standard toolbars):
	CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

	// create docking windows
	if (!CreateDockingWindows())
	{
		TRACE0("Failed to create docking windows\n");
		return -1;
	}

	m_wndFileView.EnableDocking(CBRS_ALIGN_ANY);
	m_wndClassView.EnableDocking(CBRS_ALIGN_ANY);

	//Polaris-20140711
	m_wndCCFileView.EnableDocking(CBRS_ALIGN_ANY);

	DockPane(&m_wndFileView);
	CDockablePane* pTabbedBar = NULL;
	m_wndClassView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);

	//Polaris-20140711
	m_wndCCFileView.AttachToTabWnd(&m_wndFileView,  DM_SHOW, TRUE, &pTabbedBar);

	m_wndOutput.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndOutput);
	m_wndProperties.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndProperties);
	//Polaris-20141008
	//m_RCListWnd.EnableDocking(CBRS_ALIGN_ANY);
	//DockPane(&m_RCListWnd);

	// Enable enhanced windows management dialog
	EnableWindowsDialog(ID_WINDOW_MANAGER, ID_WINDOW_MANAGER, TRUE);

	// Enable toolbar and docking window menu replacement
	EnablePaneMenu(TRUE, ID_VIEW_CUSTOMIZE, strCustomize, ID_VIEW_TOOLBAR);

	// enable quick (Alt+drag) toolbar customization
	CMFCToolBar::EnableQuickCustomization();

	if (CMFCToolBar::GetUserImages() == NULL)
	{
		// load user-defined toolbar images
		if (m_UserImages.Load(_T(".\\UserImages.bmp")))
		{
			CMFCToolBar::SetUserImages(&m_UserImages);
		}
	}

	// enable menu personalization (most-recently used commands)
	// TODO: define your own basic commands, ensuring that each pulldown menu has at least one basic command.
	CList<UINT, UINT> lstBasicCommands;

	lstBasicCommands.AddTail(ID_FILE_NEW);
	lstBasicCommands.AddTail(ID_FILE_OPEN);
	lstBasicCommands.AddTail(ID_FILE_SAVE);
	lstBasicCommands.AddTail(ID_FILE_PRINT);
	lstBasicCommands.AddTail(ID_APP_EXIT);
	lstBasicCommands.AddTail(ID_EDIT_CUT);
	lstBasicCommands.AddTail(ID_EDIT_PASTE);
	lstBasicCommands.AddTail(ID_EDIT_UNDO);
	lstBasicCommands.AddTail(ID_APP_ABOUT);
	lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
	lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2003);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS_2005);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLUE);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_SILVER);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLACK);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_AQUA);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_WINDOWS_7);
	lstBasicCommands.AddTail(ID_SORTING_SORTALPHABETIC);
	lstBasicCommands.AddTail(ID_SORTING_SORTBYTYPE);
	lstBasicCommands.AddTail(ID_SORTING_SORTBYACCESS);
	lstBasicCommands.AddTail(ID_SORTING_GROUPBYTYPE);

	CMFCToolBar::SetBasicCommands(lstBasicCommands);

	// Switch the order of document name and application name on the window title bar. This
	// improves the usability of the taskbar because the document name is visible with the thumbnail.
	ModifyStyle(0, FWS_PREFIXTITLE);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWndEx::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

BOOL CMainFrame::CreateDockingWindows()
{
	BOOL bNameValid;

	// Create class view
	CString strClassView;
	bNameValid = strClassView.LoadString(IDS_CLASS_VIEW);
	ASSERT(bNameValid);
	if (!m_wndClassView.Create(strClassView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CLASSVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Class View window\n");
		return FALSE; // failed to create
	}

	// Create file view
	CString strFileView;
	bNameValid = strFileView.LoadString(IDS_FILE_VIEW);
	ASSERT(bNameValid);
	if (!m_wndFileView.Create(strFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_FILEVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT| CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create File View window\n");
		return FALSE; // failed to create
	}

	// Create output window
	CString strOutputWnd;
	bNameValid = strOutputWnd.LoadString(IDS_OUTPUT_WND);
	ASSERT(bNameValid);
	if (!m_wndOutput.Create(strOutputWnd, this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_OUTPUTWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Output window\n");
		return FALSE; // failed to create
	}

	//Polaris-20140711
	CString strCCFileView;
	bNameValid = strCCFileView.LoadString(IDS_CCFILE_VIEW);
	ASSERT(bNameValid);
	if (!m_wndCCFileView.Create(strCCFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CCFILEVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT| CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create CCFile View window\n");
		return FALSE; // failed to create
	}

	// Create properties window
	CString strPropertiesWnd;
	bNameValid = strPropertiesWnd.LoadString(IDS_PROPERTIES_WND);
	ASSERT(bNameValid);
	if (!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PROPERTIESWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Properties window\n");
		return FALSE; // failed to create
	}

	//Polaris-20141009
	//CString strRCListWnd;
	//bNameValid = strRCListWnd.LoadString(IDS_RCLIST_WND);
	//ASSERT(bNameValid);
	//if (!m_RCListWnd.Create(strRCListWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_RCLISTWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
	//{
		//TRACE0("Failed to create Properties window\n");
		//return FALSE; // failed to create
	//}

	SetDockingWindowIcons(theApp.m_bHiColorIcons);

	//Polaris-20140716
	CRect   rc; 
	m_wndStatusBar.GetItemRect(0, &rc); 
	m_wndProgressBar.Create( WS_CHILD | PBS_SMOOTH, rc, &m_wndStatusBar, NULL); 	
	m_wndProgressBar.SetStep(1);

	return TRUE;
}

void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
	HICON hFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndFileView.SetIcon(hFileViewIcon, FALSE);

	//Polaris-20140711
	HICON hCCFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndCCFileView.SetIcon(hCCFileViewIcon, FALSE);

	HICON hClassViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndClassView.SetIcon(hClassViewIcon, FALSE);

	HICON hOutputBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndOutput.SetIcon(hOutputBarIcon, FALSE);

	HICON hPropertiesBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

	//Polaris-20141009
	HICON hRCListWndIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

	UpdateMDITabbedBarsIcons();
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnWindowManager()
{
	ShowWindowsDialog();
}

void CMainFrame::OnViewCustomize()
{
	CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* scan menus */);
	pDlgCust->EnableUserDefinedToolbars();
	pDlgCust->Create();
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
	LRESULT lres = CMDIFrameWndEx::OnToolbarCreateNew(wp,lp);
	if (lres == 0)
	{
		return 0;
	}

	CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
	ASSERT_VALID(pUserToolbar);

	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
	return lres;
}

void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

	switch (theApp.m_nAppLook)
	{
	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2008:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_WINDOWS_7:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
	}

	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	// base class does the real work

	if (!CMDIFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}


	// enable customization button for all user toolbars
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	for (int i = 0; i < iMaxUserToolbars; i ++)
	{
		CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
		if (pUserToolbar != NULL)
		{
			pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
		}
	}

	return TRUE;
}


void CMainFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CMDIFrameWndEx::OnSettingChange(uFlags, lpszSection);
	m_wndOutput.UpdateFonts();
}

//Polaris-20140716
/*
 * 函数功能：加载抽象语法树
 */
void CMainFrame::LoadAST(treeNode* root)
{
	m_wndClassView.LoadAST(root);
}

//Polaris-20140710
/*
 * 函数功能：点击“打开文件夹”菜单项的消息响应函数
 */
void CMainFrame::OnOpenFolder()
{
	// TODO: Add your command handler code here

	//BROWSEINFO结构体是MFC的打开文件信息结构
	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(BROWSEINFO));//ZeroMemory用0来填充一块内存区域
	bi.hwndOwner = m_hWnd;
	bi.ulFlags   = BIF_RETURNONLYFSDIRS;

	//LPITEMIDLIST是文件目录结构体
	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	BOOL bRet = FALSE;

/*微软将ANSI编码和Unicode编码这两套字符集及其操作进行了统一，通过条件编译（通过_UNICODE和UNICODE宏）控制实际使用的字符集，这样就有了_T("")这样的字符串，对应的就有了_tcslen这样的函数
为了存储这样的通用字符，就有了TCHAR：
当没有定义_UNICODE宏时，TCHAR = char，_tcslen =strlen
当定义了_UNICODE宏时，TCHAR = wchar_t ， _tcslen = wcslen[1] 
当我们定义了UNICODE宏，就相当于告诉了编译器：我准备采用UNICODE版本。这个时候，TCHAR就会摇身一变，变成了wchar_t。而未定义UNICODE宏时，TCHAR摇身一变，变成了unsignedchar。这样就可以很好的切换宽窄字符集。*/

	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	if (pidl)
	{
		if (SHGetPathFromIDList(pidl, szFolder))  
		{
			bRet = TRUE;
		}
		//释放SHBrowseForFolder函数申请的内存
		IMalloc *pMalloc = NULL;
		if (SUCCEEDED(SHGetMalloc(&pMalloc)) && pMalloc)
		{ 
			pMalloc->Free(pidl); 
			pMalloc->Release();
		}
		
		//为文件树结构设置根目录
		m_wndFileView.SetRootDir(szFolder);
		cc_Folder = szFolder;
	}

}

/*
 * 函数功能：点击菜单项“设置最小检测行数”的消息响应函数
 */
void CMainFrame::OnSetMinline()
{
	// TODO: 在此添加命令处理程序代码
	CSetMinlineDlg mlDlg;
	mlDlg.DoModal();
}

/*
 * 函数功能：点击菜单项“克隆代码检测->检测全部文件”的消息响应函数
 */
static THREAD_INFO cc_ti;//定义的克隆检测线程结构体变量cc_ti
void CMainFrame::OnDetectAllClone()
{
	// TODO: 在此添加命令处理程序代码;
    if (m_bBeginFindCCThread) //取消？
	{
		m_bBeginFindCCThread = FALSE;
	}
	else
	{
		//Polaris-20150302 清空一些全局变量
		for(int i=0;i<ccGroupNumber;i++)
		{
			for(int j=0;j<ccGroupNumber;j++)
			{
				ccGroupGraph[i][j] = 0;
				matrix[i][j] = 0;
				dist[i][j] = 0;
				prev_node[i][j] = 0;
			}
			S[i]=0;
			ratio[i] = 0;
			fenmus[i] = 0;
		}
		ccGroupNumber = 0;

		cc_ti.strFile = "";
		cc_ti.pMainFrame = this;
		
		m_bBeginFindCCThread = TRUE;		//布尔值m_bBeginFindBugFolderThread置为true，表示开始检测所有文件
		AfxBeginThread(CC_Detect_Thread,&cc_ti);	//启动检测进程
	}
}

//Polaris-20140930
/*
 * 实际执行检测克隆代码工作的线程
 */
UINT CC_Detect_Thread(LPVOID lpParam)//LPVOID是一个没有类型的指针，也就是说你可以将任意类型的指针赋值给LPVOID类型的变量（一般作为参数传递），然后在使用的时候再转换回来。
{
	THREAD_INFO* ti = (THREAD_INFO*) lpParam;
	ASSERT(ti);

	//将主线程的变量复制到子线程中，避免过多的线程通信
	CString outFilePath = ti->pMainFrame->outFilePath;
	int Min_len = ti->pMainFrame->Min_len;
	Out_CpPos * Ocp = &(ti->pMainFrame->Ocp);		
	CArray<CH_FILE_INFO,CH_FILE_INFO> * filesReadInfo = &(ti->pMainFrame->filesReadInfo);	
	CArray<HeaderFileElem,HeaderFileElem> * headerFilesArry = &(ti->pMainFrame->headerFilesArry);
	FilesToTokens * filesToTokens = &(ti->pMainFrame->filesToTokens);
	bool fileOutRootChange = ti->pMainFrame->fileOutRootChange;
	CString outText = ti->pMainFrame->outText;
	CString cc_Folder = ti->pMainFrame->cc_Folder;
	CCFileView * m_wndCCFileView = &(ti->pMainFrame->m_wndCCFileView);
	//

	//Polaris-20141009  设置进度条范围
	ti->pMainFrame->m_wndProgressBar.SetRange32(0,Ocp->CP_Seg_Array.GetSize());
	ti->pMainFrame->m_wndProgressBar.ShowWindow(SW_SHOW);

	Ocp->Clean();
	Ocp->CP_OUT_PATH = "";
	Ocp->CP_OUT_PATH.Format("%s",outDir);
	outFilePath.Format("%s",outDir);
	CStdioFile f;
	if(f.Open(Ocp->CP_OUT_PATH,CFile::modeCreate|CFile::modeRead|CFile::modeWrite))
	{
		f.Close();
	}
	if(f.Open(outFilePath+"\\MemoryRecord.txt",CFile::modeCreate|CFile::modeRead|CFile::modeWrite))//向输出目录中输出检测结果
	{
		f.Close();
	}

	//清空文件和包含的头文件数量，处理时间等信息
	filesReadInfo->RemoveAll();

	//清空头文件处理信息
	headerFilesArry->RemoveAll();//wq-20090610-暂时不保留，节省内存

	Ocp->Min_len = Min_len;
	CString	str_cfileSum(""),str_lineSum(""),str_rfUseTime(""),
			str_useTime(""),str_cpLineSum("");
	
	Out_TokenSeq Opt;		//这里使用了Out_TokenSeq类
	Seq_Mining Rsf;			//这里使用了Seq_Mining类

	filesToTokens->Clean();
	long beginTime=clock();

	Opt.Obj_file_path.Format("%s",cc_Folder);//wq-20090319
	Opt.out_file_path.Format("%s",outDir);//wq-20090402

	//这行语句写输出目录的MemoryRecord文件
	OutputMemoryRecord("","",Opt.out_file_path,0,"");

	//MessageBox(Opt.Obj_file_path);

	//wq-20090518
	//Token文件的输出目录
	CString tokenOutPath = Opt.out_file_path + "\\Token_Out";
	::mkdir(tokenOutPath.GetBuffer(tokenOutPath.GetLength()));

	CString seqOutPath = Opt.out_file_path + "\\Seq_Out";
	::mkdir(seqOutPath.GetBuffer(seqOutPath.GetLength()));

	//读取目录dir下的所有C文件，并输出对应的token文件
	//Polaris-20140712
	char *inDir = (LPSTR)(LPCTSTR)cc_Folder;
	Opt.CFILE_TO_TFILE(*filesToTokens,inDir,Rsf.Seq_Arry_G,*headerFilesArry,*filesReadInfo);//wq-20090319

	int i;
	int cfileSum = filesReadInfo->GetSize();	//记录了C文件的个数
	int hfileSum = 0;						//记录了H文件的个数
	int prcss_hfileSum = 0;					//记录了已处理的H文件个数
	long double prcssTime = 0.0;			//记录了处理时间

	//遍历所有C文件，逐个处理
	for(i=0;i<cfileSum;i++)
	{
		hfileSum += (*filesReadInfo)[i].hFileSum;
		prcss_hfileSum += (*filesReadInfo)[i].prcss_hFileSum;		//递归地记录已处理的头文件
		prcssTime+= long double ((*filesReadInfo)[i].cfilePrcssTime);
	}
	CString str("");
	CString strText("");
	str.Format("%d",cfileSum);
	strText += "一共处理C文件数量：" + str +"\r\n";
	str.Format("%d",hfileSum);
	strText += "每个C文件包含的不重复头文件的总和：" + str +"\r\n";
	str.Format("%d",prcss_hfileSum);
	strText += "一共处理的头文件数量：" + str +"\r\n";
	str.Format("%f",prcssTime);
	strText += "头文件预处理和词法分析总时间：" + str +"(s) \r\n";

	CString outPath("");
	outPath.Format("%s",outDir);
	CStdioFile file;
	CFileException fileException;

	//将处理结果信息保存到输出目录下的CHFilesReadInfo.txt文件中
	if(file.Open(outPath+"\\CHFilesReadInfo.txt", CFile::modeCreate | CFile::modeWrite, &fileException))
	{
		file.WriteString(strText);
		file.Close();
	}
	else
	{
		// TRACE宏对于VC下程序调试来说是很有用的东西，有着类似printf的功能；该宏仅仅在程序的DEBUG版本中出现，当RELEASE的时候该宏就完全消失了。
		TRACE( "Can't open file %s, error = %u\n", 
			 outPath, fileException.m_cause );
	}

	filesToTokens->out_file_path.Format("%s",outDir);//wq-20090518
	//将路径名为dir下的所有c文件分析所得的数字序列信息存于数组Rsf.Seq_Arry_G中

	long endTime1=clock(); //计时      

	str_cfileSum.Format("%d",Opt.Cfile_Sum);
	str_lineSum.Format("%d",Opt.Line_Sum);
	str_rfUseTime.Format("%f",float(endTime1-beginTime)/CLOCKS_PER_SEC);
	str_rfUseTime += "s";

	Rsf.out_file_path.Format("%s",outDir);//wq-20090402
	Rsf.DltSingleLnSeq();//wq-20081124-删除函数外的单独行
	Rsf.ClosedMining();

	Ocp->out_file_path.Format("%s",outDir);//wq-20090402
	Ocp->Out_CpCodePos(Rsf);

	//
	//#############################################################
	//ww 1012 解决克隆代码稀疏问题
	//int sum_clones = 0;	//1013
	for(int ii=0; ii<Ocp->CP_Seg_Array.GetSize(); ++ii)//对每一个克隆代码组
	{
		if(Ocp->CP_Seg_Array[ii].seq.GetLength()/10 < Min_len)//如果克隆代码行数小于Min_len则删除
		{
			Ocp->CP_Seg_Array.RemoveAt(ii);
			ii--;	//1015
			continue;
		}
		for(int jj=0; jj<Ocp->CP_Seg_Array[ii].CP_Array.GetSize(); ++jj)//对每组克隆代码中的每个克隆代码片段
		{
			CP_SEG &cpSeg = Ocp->CP_Seg_Array[ii].CP_Array[jj];
			CString temp_seq;
			temp_seq = Ocp->CP_Seg_Array[ii].seq;//克隆代码序列
			int length_clones = temp_seq.GetLength()/10;//克隆代码行数
			
			//1031 
			int first_number = atoi(cpSeg.relaLPOS.Mid(0, 4));//第一行克隆代码行号
			int last_number = first_number;//最后一行克隆代码行号
			for(int num_line=1; num_line<length_clones; num_line++)
			{
				int temp_number = atoi(cpSeg.relaLPOS.Mid(num_line*4, 4));
				if(temp_number < first_number)
					first_number = temp_number;
				else if(temp_number > last_number)
					last_number = temp_number;
			}
			
			int count_cha = last_number - first_number - length_clones + 1;//克隆代码片段中的非克隆代码行数
		
			if(count_cha <= length_clones/2)	//
			{
				continue;
			}
			//获取克隆代码片段的token表示
			TokenedFile tknFile;
			CntxtInconsisDetect temp_cntxt;
			if(!temp_cntxt.GetTknListForCPSeg(tknFile, *filesToTokens, cpSeg, *headerFilesArry))	//time is too much
			{
				continue;
			}
			//克隆代码片段中非克隆代码行的行号存入数组map_line	//1031
			int map_line[300];
			int index_map_line = 0;
			int map_line_temp[10000] = {0};
			for(int nn=0; nn<length_clones; nn++)
			{
				int num_line = atoi(cpSeg.relaLPOS.Mid(nn*4, 4));
				map_line_temp[num_line] = 1;
			}
			for(int mm=first_number; mm<last_number+1; mm++)
			{
				if(map_line_temp[mm] == 0)
				{
					map_line[index_map_line] = mm;
					index_map_line++;
				}
			}
			
			//统计克隆代码片段中非克隆代码行的行数（只含{的行不算）
			int line_rongyu = 0;
			for(int nn=0; nn<index_map_line; ++nn)
			{
				CArray<TNODE,TNODE>& tknLine = tknFile.GetTokenLine(map_line[nn]-1);
				CString temp_output;
				for(int oo=0; oo<tknLine.GetSize(); ++oo)
				{
					temp_output += tknLine[oo].srcWord;
				}
				temp_output.Remove(' ');
				if(temp_output != "{")
				{
					++line_rongyu;
				}
			}
			if(line_rongyu > length_clones/2)	//
			{
				//删除该克隆代码片段
				Ocp->CP_Seg_Array[ii].CP_Array.RemoveAt(jj);
				jj--;
			}
			//end
		}
		if(Ocp->CP_Seg_Array[ii].CP_Array.GetSize() < 2)	//
		{
			Ocp->CP_Seg_Array.RemoveAt(ii);
			ii--;	//1015
		}
		//else
		//	Ocp.CP_Seg_Array[ii].sup = Ocp.CP_Seg_Array[ii].CP_Array.GetSize();

	}




	//消除“对应位置标识符不同”个数较多时的误检	ww 1025
	Ocp->UpdateIdsLocArrays(*headerFilesArry);			//time is too much

	//计算标识符冲突率消除误检 // 1013 ww
	//showBug1.idInconsis.computeIdCnflctRatio(Ocp.CP_Seg_Array);//
	//end



	for(int ii=0; ii<Ocp->CP_Seg_Array.GetSize(); ii++)
	{
		if(Ocp->CP_Seg_Array[ii].len < Min_len )
		{
			continue;
		}

		for(int j=0; j<Ocp->CP_Seg_Array[ii].CP_Array.GetSize(); j++)
		{
			for(int k=j+1; k<Ocp->CP_Seg_Array[ii].CP_Array.GetSize(); k++)
			{
				//保存第一个克隆代码片段中所有标识符的个数，名字，位置
				CString idName_temp1[600];
				int lineIndex_temp1[600];
				int tokenNum_temp1[600];
				int index1 = 0;
				int id_number1 = 0;

				//保存第二个克隆代码片段中所有标识符的个数，名字，位置
				CString idName_temp2[600];
				int lineIndex_temp2[600];
				int tokenNum_temp2[600];
				int index2 = 0;
				int id_number2 = 0;

				//标识符名字及位置存入数组
				for(int m=0; m<Ocp->CP_Seg_Array[ii].CP_Array[j].idsLocArray.GetSize(); ++m)
				{
					if(index1 > 599)	//1104
						break;
					id_number1++;
					for(int n=0; n<Ocp->CP_Seg_Array[ii].CP_Array[j].idsLocArray[m].GetIdLocArry().GetSize(); n++)
					{
						idName_temp1[index1] = Ocp->CP_Seg_Array[ii].CP_Array[j].idsLocArray[m].GetIdName();
						lineIndex_temp1[index1] = Ocp->CP_Seg_Array[ii].CP_Array[j].idsLocArray[m].GetIdLocArry()[n].lineIndex;
						tokenNum_temp1[index1] = Ocp->CP_Seg_Array[ii].CP_Array[j].idsLocArray[m].GetIdLocArry()[n].tokenNum;
						index1++;
					}
				}
			
				//标识符名字及位置存入数组
				for(int m=0; m<Ocp->CP_Seg_Array[ii].CP_Array[k].idsLocArray.GetSize(); ++m)
				{
					if(index2 > 599)	//1104
						break;
					id_number2++;
					for(int n=0; n<Ocp->CP_Seg_Array[ii].CP_Array[k].idsLocArray[m].GetIdLocArry().GetSize(); n++)
					{
						idName_temp2[index2] = Ocp->CP_Seg_Array[ii].CP_Array[k].idsLocArray[m].GetIdName();
						lineIndex_temp2[index2] = Ocp->CP_Seg_Array[ii].CP_Array[k].idsLocArray[m].GetIdLocArry()[n].lineIndex;
						tokenNum_temp2[index2] = Ocp->CP_Seg_Array[ii].CP_Array[k].idsLocArray[m].GetIdLocArry()[n].tokenNum;
						index2++;
					}
				}
			
				//ww 1028
				CString id1_id2[600];
				//两数组比对
				int index_id1_id2 = 0;	//所有对应位置标识符对的组数
				for(int m=0; m<index1; m++)
				{
					int n;
					for(n=0; n<index2; n++)
					{
						if( lineIndex_temp1[m] == lineIndex_temp2[n] 
							&& tokenNum_temp1[m] == tokenNum_temp2[n] )
						{
							CString temp;
							temp += idName_temp1[m];			
							temp+= " ";
							temp += idName_temp2[n];
							int i;
							for(i=0; i<index_id1_id2; i++)
							{
								if(temp == id1_id2[i])
									break;
							}
							if(i == index_id1_id2)
							{
								id1_id2[index_id1_id2] = temp;
								index_id1_id2++;
							}
						}
					}
				} 
				int count_different_id1_id2 = 0;	//对应位置标识符不同的对的组数
				for(int m=0; m<index_id1_id2; m++)
				{
					int num_temp1 = id1_id2[m].Find(' ');
					if(id1_id2[m].Mid(0, num_temp1) != id1_id2[m].Right(id1_id2[m].GetLength()-num_temp1-1))
						count_different_id1_id2++;
				}

				if((double)count_different_id1_id2/index_id1_id2 >= 0.600000)	//值的设置需要更多考虑
				{
					if(id_number1 > id_number2)	//删除策略有问题
					{
						Ocp->CP_Seg_Array[ii].CP_Array.RemoveAt(j);
						j--;
						break;	//careful
					}
					else
					{
						Ocp->CP_Seg_Array[ii].CP_Array.RemoveAt(k);
						k--;
					}
				}
				else
				{
					CFile file("d:\\id1_id2.txt",  CFile::modeCreate | CFile::modeNoTruncate  | CFile::modeWrite );
					CString temp;
					temp.Format("%d  count_different_id1_id2: %d   index_id1_id2: %d   %6f", 
						ii+1, count_different_id1_id2, index_id1_id2, (double)count_different_id1_id2/index_id1_id2);
					temp += "\r\n";	//
					char *p = (char*)temp.GetBuffer(temp.GetLength());
					file.SeekToEnd();
					file.Write(p, temp.GetLength());
					file.Close();
				}

			}
		}
		if(Ocp->CP_Seg_Array[ii].CP_Array.GetSize() < 2)	//
		{
			Ocp->CP_Seg_Array.RemoveAt(ii);
			ii--;
		}
	}
	//end

	//Polaris-20141007
	//现在可以知道克隆代码组数、每组的片段数、每个片段的行数

	//计算克隆代码总行数
	int sum_clones = 0;
	for(int ii=0; ii<Ocp->CP_Seg_Array.GetSize(); ++ii)//对每一个克隆代码组
	{
         //sum_clones += Ocp.CP_Seg_Array[ii].len;

		for(int jjj= 0; jjj<Ocp->CP_Seg_Array[ii].CP_Array.GetSize();jjj++) //对每一个克隆代码段
		{
		    sum_clones += Ocp->CP_Seg_Array[ii].seq.GetLength()/10;	//除10的原因：源文件中的每一行代码被转化为9位数字+1位分隔符，所以seq的总长度除10就得到了代码行数。
			//sum_clones += Ocp.CP_Seg_Array[ii].seq.GetLength()/10;

			//Polaris-20141008
			string key;
			key = (char*)(LPCTSTR)(Ocp->CP_Seg_Array[ii].CP_Array[jjj].filename);

			if(cc_file_map.count(key) == 0)//集合中不存在当前文件目录
			{
				cc_file_map.insert(pair<string,int>(key ,1));
				cc_file_lines_map.insert(pair<string,int>(key ,Ocp->CP_Seg_Array[ii].seq.GetLength()/10));
			}
			else
			{
				int pre_count = cc_file_map[key];
					
				int new_count = pre_count + 1;
				cc_file_map[key] = new_count;

				cc_file_lines_map[key] += Ocp->CP_Seg_Array[ii].seq.GetLength()/10;
			}

			//Polaris-20150302
			if(cc_filename_groupnumber_map.count(key) == 0)//集合中不存在当前文件目录
			{
				vector<int> vec;
				vec.push_back(ii);
				cc_filename_groupnumber_map.insert(pair<string,vector<int>>(key, vec));
			}
			else
			{
				vector<int> *vec = &cc_filename_groupnumber_map[key];
				bool isExist = false;
				for(int i=0; i<vec->size(); i++)
				{
					if((*vec)[i] == ii)
					{
						isExist = true;
					}
				}
				if(!isExist)
				{
					vec->push_back(ii);
				}
			}

		}
		
	}
	Ocp->m_cpLineSum = sum_clones;

	//Polaris-20150302
	//获得克隆代码组关系图存储矩阵
	for(map<string, vector<int>>::iterator itr=cc_filename_groupnumber_map.begin(); itr!=cc_filename_groupnumber_map.end(); itr++)
	{
		vector<int> vec = itr->second;
		for(int i=0; i<vec.size(); i++)
		{
			for(int j=i+1; j<vec.size(); j++)
			{
				if(j<vec.size())
				{
					ccGroupGraph[vec[i]][vec[j]] = 1;
					ccGroupGraph[vec[j]][vec[i]] = 1;
				}
			}
		}
	}
	ccGroupNumber = Ocp->CP_Seg_Array.GetSize();

	/*测试
	CString temp11;
	for(int kk = 0; kk<Ocp->CP_Seg_Array.GetSize(); kk++)
	{
		for(int ll=0; ll<Ocp->CP_Seg_Array.GetSize(); ll++)
		{
			char num[256];
		    itoa(ccGroupGraph[kk][ll],num,10);
			temp11 += num;
			temp11 += "***";
		}
		temp11 += "\n";
	}
	AfxMessageBox(temp11);*/

	/*测试
	CString temp11;
	for (map<string, int>::iterator itr=cc_file_map.begin(); itr!=cc_file_map.end(); itr++)  
    {  
		temp11 += (itr->first).c_str();
		temp11 += "个数：";
		char num[256];
		itoa(itr->second,num,10);
		temp11 += num;
		temp11 += "---";
    }
	AfxMessageBox(temp11);*/
	//#####################################

	long endTime2=clock(); //计时  

	str_useTime.Format("%f",float(endTime2-beginTime)/CLOCKS_PER_SEC);
	str_useTime += "s";
	str_cpLineSum.Format("%d",Ocp->m_cpLineSum);

	outPath += "\\CPFinderInfor.txt";

	outText = "读入C程序文件数量：" + str_cfileSum + "\r\n" +
			  "代码总行数：" + str_lineSum + "\r\n" +
			  "文件处理时间：" + str_rfUseTime + "\r\n" +
			  "-----------------------------------\r\n"
			  "克隆代码总行数：" + str_cpLineSum + "\r\n" +
			  "克隆代码检测时间：" + str_useTime + "\r\n" +
			  "-----------------------------------\r\n\r\n\r\n" +
			  "此信息存于文件" + outPath + "中";

	//输出检测结果到输出目录的CPFinderInfor.txt文件中
	if(file.Open(outPath, CFile::modeCreate | CFile::modeWrite, &fileException))
	{
		file.WriteString(outText);
		file.Close();
	}
	else
	{
		TRACE( "Can't open file %s, error = %u\n", 
			 outPath, fileException.m_cause );
	}

	AfxMessageBox(outText);
	AfxMessageBox("克隆代码检测完毕!");

	//CString strr;  
	//strr.Format( "%d", Min_len);  
	//AfxMessageBox(strr);

	//左侧的文件树结构更新
	m_wndCCFileView->m_wndCCFileView.DeleteChildItems(m_wndCCFileView->m_hCTree);
	m_wndCCFileView->m_wndCCFileView.Min_len = Min_len;
	m_wndCCFileView->m_wndCCFileView.FillTree(Ocp,m_wndCCFileView->m_imgList,m_wndCCFileView->m_hCTree);

	ti->pMainFrame->m_bBeginFindCCThread = FALSE;

	//Polaris-20141009
	ti->pMainFrame->m_wndProgressBar.ShowWindow(SW_HIDE);

	return 0;
}

//Polaris-20140716
static THREAD_INFO g_ti;//定义的冗余检测线程结构体变量g_ti

/*
 * 函数功能：被点击菜单项“输出检测报告”的消息响应函数OnOutputReport()调用，获取文件行数
 */
int GetFileLineCount(LPCTSTR filename)
{
	ifstream f(filename,ios::binary);
	if (f==NULL) return 0;
	char ch;
	const char BR = char(10);
	int c=0;
	while (f.get(ch)){
		if (BR == ch) c++;
	}
	f.close();

	return c;
}

/*
 * 函数功能：点击菜单项“输出检测报告”的消息响应函数，命令ID：ID_OUTPUT_REPORT
 */
void CMainFrame::OnOutputReport()
{
	CString t;
	t = "<html>"
		"<head>"
		"<title>检测报告</title>"
		"<style>"
		"table.tb"
	"{"
		"font-family: Arial, Helvetica, sans-serif;"
		"margin-top: 10px;"
		"border-collapse: collapse;"
		"border: 1px solid #CDCDCD;"
		"width: 100%;"
	"}"

	"table.tb th"
	"{"
	"	vertical-align: baseline;"
"padding: 5px 15px 5px 5px;"
		"background-color: #E5EDF2;"
"border: 1px solid #C2D5E3;"
		"text-align: left;"
	"}"
	"table.tb tr"
	"{"
		"background-color: white;"
	"}"
	"table.tb td"
	"{"
		"vertical-align: text-top;"
"padding: 5px 15px 5px 5px;"
"border: 1px solid #C2D5E3;"
	"}"
	".c0 { background-color:#FFECEC;}"
	".c1 { background-color:#ECF5FF;}"
	".c2 { background-color:#D1E9E9;}"
	".c3 { background-color:#F3F3FA;}"
	".c4 { background-color:#FFF3EE;}"
	"</style>"
		"</head>"

		"<body>"
		"<table class=\"tb\">"
		"<caption><b>检测报告</b></caption>"
		"<tr>"
		"<th width=\"50%\">文件名</th>"
		"<th width=\"10%\">行数</th>"
		"<th width=\"10%\">行</th>"
		"<th width=\"30%\">描述</th>"
		"</tr>";

	CString last_file;
	TCHAR tmp[20];
	CString fmt;

	for (int i=0;i<m_buglist.size();i++)
	{
		BUG_LIST_ITEM* li = m_buglist.get(i);

		// line no
		itoa(li->line,tmp,10);

		if (last_file != li->filename) // 和上次不一样?
		{
			last_file = li->filename;

			TCHAR tmp2[20];
			itoa(GetFileLineCount(li->filename),tmp2,10);

			fmt.Format("<tr><td>%s</td><td>%d</td><td class=\"c%d\">%d</td><td class=\"c%d\">%s</td></tr>",
				li->filename,
				GetFileLineCount(li->filename),
				li->bug_type,
				li->line,
				li->bug_type,
				li->info);
			t += fmt;

			//Polaris-20141008
			vector<CString> temp_vector(4);

			temp_vector[0] = li->filename;
			temp_vector[1] = tmp2;
			TCHAR tmp3[20];
			itoa(li->line,tmp3,10);
			temp_vector[2] = tmp3;
			temp_vector[3] = li->info;

			rc_info_list.push_back(temp_vector);
		}
		else
		{
			fmt.Format("<tr><td></td><td></td><td class=\"c%d\">%d</td><td class=\"c%d\">%s</td></tr>",				
				li->bug_type,
                li->line,
				li->bug_type,
				li->info);
			t += fmt;

			//Polaris-20141008
			vector<CString> temp_vector(4);
			CString s = "";
			temp_vector[0] = li->filename;
			temp_vector[1] = s;
			TCHAR tmp3[20];
			itoa(li->line,tmp3,10);
			temp_vector[2] = tmp3;
			temp_vector[3] = li->info;

			rc_info_list.push_back(temp_vector);
		}
	}
	t += "</table>";

	//检测日期，用时，检测文件数
	itoa(m_buglist.elapsed_time,tmp,10);	//itoa是系统库函数，作用是把整型数转换为字符串

	COleDateTime   TD;	//	一个COleDateTime类对象代表一个绝对的日期和时间值 
	CString   strTD; 

	TD=TD.GetCurrentTime(); 
	strTD.Format( "%d-%d-%d %d:%d:%d ",TD.GetYear(),TD.GetMonth(),TD.GetDay(),TD.GetHour(),TD.GetMinute(),TD.GetSecond()); 

	t += "<p>耗时：";
	t +=tmp;
	t +="ms.</p><p>";
	t += strTD+" HIT</p>";
	t += "</body></html>";

	char szTempPath[MAX_PATH];
	GetTempPath(MAX_PATH, szTempPath);
	strcat_s(szTempPath,"REPORT.html");

	CFile of(szTempPath, CFile::modeWrite | CFile::modeCreate);
	of.Write(t,t.GetLength());
	of.Close();
	
	/*测试
	CString testStr = "";
	int elem_count = rc_info_list.size();
	for(int i=0;i<elem_count;i++)
	{
		for(int j=0;j<4;j++)
		{
			testStr += rc_info_list[i][j] + "-----";
		}
		testStr += "\n";
	}
	AfxMessageBox(testStr);*/

	//Polaris-20141008
	m_wndProperties.FillRCList(rc_info_list);

	//ShellExecute的功能是运行一个外部程序（或者是打开一个已注册的文件、打开一个目录、打印一个文件等等），并对外部程序有一定的控制。
	ShellExecute(AfxGetMainWnd()->m_hWnd,"open",szTempPath,NULL,NULL,SW_SHOWNORMAL);
}

/*
 * 函数功能：点击菜单项“设置预处理->设置预处理包含目录”的消息响应函数
 */
void CMainFrame::OnIncludeDir()
{
	CFileIncludeDlg cfd;
	cfd.DoModal();	//显示“预处理文件目录”对话框
}

/*
 * 函数功能：点击菜单项“设置预处理->自定义预处理参数”的消息响应函数
 */
void CMainFrame::OnDefParams()
{
	//Polaris-20140830
	//CString temp;
	//temp.Format("%d---%d---%d---%d---%d",count_ideoper,count_reduassign,count_deadcode,count_reducond,count_hideoper);
	//AfxMessageBox(temp);

	extern CString gcc_param;
	CCustomParamDlg cpg;
	cpg.m_strEdit = gcc_param;
	if (cpg.DoModal() == IDOK){//显示“自定义预处理参数”对话框
		
		//cpg.m_edit.GetWindowText(gcc_param);
		gcc_param = cpg.m_strEdit;//获取对话框中输入的文本
		//保存
		CWinApp * app = AfxGetApp();
		app->WriteProfileString("gcc","param",gcc_param);
	}
}

/*
 * 函数功能：点击菜单项“冗余代码检测->检测全部文件”的消息响应函数
 */
void CMainFrame::OnDetectAllRc()
{
	if (m_bBeginFindBugFolderThread) //取消？
	{
		m_bBeginFindBugFolderThread = FALSE;
	}
	else
	{
		g_ti.strFile = m_wndFileView.m_wndFileView.m_strRootDir;
		g_ti.pMainFrame = this;
		
		m_bBeginFindBugFolderThread = TRUE;		//布尔值m_bBeginFindBugFolderThread置为true，表示开始检测所有文件
		AfxBeginThread(FindBugFolderThread,&g_ti);	//启动检测进程
	}
}

/*
 * 函数功能：菜单项“冗余代码->检测全部文件”的（界面）消息响应函数，命令ID：ID_FINDBUG_ALL
 */
void CMainFrame::OnUpdateDetectAllRc(CCmdUI *pCmdUI)
{
	if (m_bBeginFindBugThread)//如果正在进行检测单个文件
	{
		pCmdUI->SetText("检测全部文件");
		pCmdUI->Enable(FALSE);
		return;
	}

	if (m_bBeginFindBugFolderThread)//如果正在进行检测所有文件
	{
		pCmdUI->SetText("取消检测全部文件");
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->SetText("检测全部文件");
		pCmdUI->Enable(m_wndFileView.m_wndFileView.GetCount()>0);
	}
}

/*
 * 函数功能：菜单项“克隆代码->检测全部文件”的（界面）消息响应函数
 */
void CMainFrame::OnUpdateDetectAllClone(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_wndFileView.m_wndFileView.GetCount()>0);
	/*if (m_bBeginFindBugThread)//如果正在进行检测单个文件
	{
		pCmdUI->SetText("检测全部文件");
		pCmdUI->Enable(FALSE);
		return;
	}

	if (m_bBeginFindBugFolderThread)//如果正在进行检测所有文件
	{
		pCmdUI->SetText("取消检测全部文件");
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->SetText("检测全部文件");
		pCmdUI->Enable(m_wndFileView.m_wndFileView.GetCount()>0);
	}*/
}

/*
 * 函数功能：菜单项“统计与分析->冗余代码类别统计（数量）”的消息响应函数
 */
void CMainFrame::OnRcType()
{
	//Polaris-20140830
	main_count_ideoper = count_ideoper;
    main_count_reduassign = count_reduassign;
    main_count_deadcode = count_deadcode;
    main_count_reducond = count_reducond;
    main_count_hideoper = count_hideoper;

	CRCTypeDiagramDlg dlg;

	dlg.Create(IDD_RC_TYPE_DIAGRAM ,NULL);

	dlg.ClearAllSeries();
	CSeries barSeries = (CSeries)dlg.m_RC_type_chart.Series(0);

	barSeries.AddXY((double)0,(double)main_count_ideoper,"幂等缺陷",RGB(255,255,0));
	barSeries.AddXY((double)1,(double)main_count_reduassign,"冗余赋值",RGB(0,255,255));
	barSeries.AddXY((double)2,(double)main_count_deadcode,"死代码",RGB(255,255,0));
	barSeries.AddXY((double)3,(double)main_count_reducond,"冗余条件表达式",RGB(0,255,255));
	barSeries.AddXY((double)4,(double)main_count_hideoper,"隐式幂等表达式",RGB(255,255,0));
	
	
	dlg.ShowWindow(SW_SHOW);

	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}

}

/*
 * 函数功能：菜单项“统计与分析->冗余代码分布统计（饼状图）”的消息响应函数
 */
void CMainFrame::OnRcFiles()
{
	//Polaris-20140830

	CRCFilesDiagramDlg dlg;

	dlg.Create(IDD_RC_FILES_DIAGRAM ,NULL);

	dlg.ClearAllSeries();
	CSeries pieSeries = (CSeries)dlg.m_rc_files_chart.Series(0);

	//barSeries.AddXY((double)0,(double)main_count_ideoper,"幂等缺陷",RGB(255,255,0));
	//barSeries.AddXY((double)1,(double)main_count_reduassign,"冗余赋值",RGB(0,255,255));
	//barSeries.AddXY((double)2,(double)main_count_deadcode,"死代码",RGB(255,255,0));
	//barSeries.AddXY((double)3,(double)main_count_reducond,"冗余条件表达式",RGB(0,255,255));
	//barSeries.AddXY((double)4,(double)main_count_hideoper,"隐式幂等表达式",RGB(255,255,0));

	int i = 0;
	srand(time(NULL));
	for (map<string, int>::iterator itr=rc_file_map.begin(); itr!=rc_file_map.end(); itr++)  
    {  
		//temp += (itr->first).c_str();
		//temp += "个数：";
		char num[256];
		itoa(itr->second,num,10);
		//temp += s;
		//temp += "---";

		vector<string> result=split((itr->first).c_str(),"\\");  
		string filename = result[result.size()-1];

		srand(rand()*i);
		pieSeries.AddXY((double)i,(double)itr->second,(filename + ":" + (char *)(LPCSTR)num + "处").c_str(),RGB(rand() % 256,(rand() + 300) % 256,(rand() + 600) % 256));
		

		i++;
    }  

	//CLegend legend = (CLegend)dlg.m_rc_files_chart.get_Legend();     
	//legend.put_Visible(FALSE);

	i = 0;
	dlg.ShowWindow(SW_SHOW);



	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：字符串分割函数
 */
std::vector<std::string> CMainFrame::split(std::string str,std::string pattern)
{
    std::string::size_type pos;
    std::vector<std::string> result;
    str += pattern;//扩展字符串以方便操作
    int size = str.size();

    for(int i=0; i<size; i++)
    {
        pos=str.find(pattern,i);
        if(pos<size)
        {
            std::string s=str.substr(i,pos-i);
            result.push_back(s);
            i=pos+pattern.size()-1;
        }
    }
    return result;
}

/*
 * 函数功能：菜单项“统计与分析->冗余代码类别统计（比例）”的消息响应函数
 */
void CMainFrame::OnRcTypeRatio()
{
	//Polaris-20140830
	main_count_ideoper = count_ideoper;
    main_count_reduassign = count_reduassign;
    main_count_deadcode = count_deadcode;
    main_count_reducond = count_reducond;
    main_count_hideoper = count_hideoper;

	CRCTypeRatioChartDlg dlg;

	dlg.Create(IDD_RC_TYPE_RATIO ,NULL);

	dlg.ClearAllSeries();
	CSeries barSeries = (CSeries)dlg.m_rc_type_ratio_chart.Series(0);

	int sum = 0;
	sum = main_count_ideoper + main_count_reduassign + main_count_deadcode + main_count_reducond + main_count_hideoper;

	string s = "";
	char percent[256];

	gcvt((double)main_count_ideoper * 100 / sum, 5, percent);
	barSeries.AddXY((double)0,(double)((double)main_count_ideoper * 100 / sum),(s + "幂等缺陷%"+ (char *)(LPCSTR)percent).c_str(),RGB(255,255,0));

	gcvt((double)main_count_reduassign * 100 / sum, 5, percent);
	barSeries.AddXY((double)1,(double)((double)main_count_reduassign * 100 / sum),(s + "冗余赋值%"+ (char *)(LPCSTR)percent).c_str(),RGB(0,255,255));

	gcvt((double)main_count_deadcode * 100 / sum, 5, percent);
	barSeries.AddXY((double)2,(double)((double)main_count_deadcode * 100 / sum),(s + "死代码%"+ (char *)(LPCSTR)percent).c_str(),RGB(255,255,0));

	gcvt((double)main_count_reducond * 100 / sum, 5, percent);
	barSeries.AddXY((double)3,(double)((double)main_count_reducond * 100 / sum),(s + "冗余条件表达式%"+ (char *)(LPCSTR)percent).c_str(),RGB(0,255,255));

	gcvt((double)main_count_hideoper * 100 / sum, 5, percent);
	barSeries.AddXY((double)4,(double)((double)main_count_hideoper * 100 / sum),(s + "隐式幂等表达式%"+ (char *)(LPCSTR)percent).c_str(),RGB(255,255,0));
	
	CLegend legend = (CLegend)dlg.m_rc_type_ratio_chart.get_Legend();     
	legend.put_Visible(FALSE);
	
	dlg.ShowWindow(SW_SHOW);

	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：菜单项“统计与分析->冗余代码分布统计（柱状图）”的消息响应函数
 */
void CMainFrame::OnRcFilesCol()
{
	//Polaris-20140830

	CRCFilesColumnDlg dlg;

	dlg.Create(IDD_RC_FILES_COLUMN ,NULL);

	dlg.ClearAllSeries();
	CSeries barSeries = (CSeries)dlg.m_rc_files_col_chart.Series(0);

	int i = 0;
	for (map<string, int>::iterator itr=rc_file_map.begin(); itr!=rc_file_map.end(); itr++)  
    {  
		//temp += (itr->first).c_str();
		//temp += "个数：";
		char num[256];
		itoa(itr->second,num,10);
		//temp += s;
		//temp += "---";

		vector<string> result=split((itr->first).c_str(),"\\");  
		string filename = result[result.size()-1];

		i % 2 == 0
			? barSeries.AddXY((double)i,(double)itr->second,filename.c_str(),RGB(0,128,255))
			: barSeries.AddXY((double)i,(double)itr->second,filename.c_str(),RGB(255,128,0));

		i++;
    }  

	//CLegend legend = (CLegend)dlg.m_rc_files_chart.get_Legend();     
	//legend.put_Visible(FALSE);

	i = 0;
	dlg.ShowWindow(SW_SHOW);



	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：菜单项“统计与分析->冗余代码类型-文件分布情况表”的消息响应函数
 */
void CMainFrame::OnRcFilesForm()
{
	CRCBugFormDlg dlg;
	dlg.Create(IDD_DIALOG3 ,NULL);

	DWORD dwStyle = dlg.m_rc_bug_list.GetExtendedStyle();   //添加列表框的网格线
    dwStyle |= LVS_EX_FULLROWSELECT;            
    dwStyle |= LVS_EX_GRIDLINES;                
    dlg.m_rc_bug_list.SetExtendedStyle(dwStyle);


    dlg.m_rc_bug_list.InsertColumn(0,"文件名",LVCFMT_LEFT,350);       //添加列标题，设置列的宽度，LVCFMT_LEFT用来设置对齐方式
    dlg.m_rc_bug_list.InsertColumn(1,"幂等缺陷",LVCFMT_LEFT,120);
    dlg.m_rc_bug_list.InsertColumn(2,"冗余赋值",LVCFMT_LEFT,120);
    dlg.m_rc_bug_list.InsertColumn(3,"死代码",LVCFMT_LEFT,120);
    dlg.m_rc_bug_list.InsertColumn(4,"冗余条件表达式",LVCFMT_LEFT,120);
	dlg.m_rc_bug_list.InsertColumn(5,"隐式幂等表达式",LVCFMT_LEFT,120);

	//dlg.m_rc_bug_list.InsertItem(0,_T("语言"));

	 map<string,map<int,int> >::iterator multitr;  // 以下是如何遍历本multiMap
     map<int,int>::iterator intertr;
     for(multitr=rc_type_file_map.begin();multitr!=rc_type_file_map.end();multitr++)
     {
		 int new_row = dlg.m_rc_bug_list.InsertItem(0,_T((multitr->first).c_str()));
		 int i = 1;
         for(intertr= multitr ->second.begin(); intertr != multitr ->second.end(); intertr ++)
		 {
			 char s[256];
			 itoa(intertr->second,s,10);
			 dlg.m_rc_bug_list.SetItemText(new_row, i, s);
			 i++;
		 }
     } 

	dlg.ShowWindow(SW_SHOW);

	MSG msg;
	BOOL bRet;
	while((bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：菜单项“克隆代码统计与分析->克隆代码段分布情况统计（数量）”的消息响应函数
 */
void CMainFrame::OnCcSegFileCol()
{
	CCCSegFileDlg dlg;
	dlg.Create(IDD_CC_SEG_FILES_DLG ,NULL);

	dlg.ClearAllSeries();
	CSeries barSeries = (CSeries)dlg.cc_seg_file_column_chart.Series(0);

	int i = 0;
	for (map<string, int>::iterator itr=cc_file_map.begin(); itr!=cc_file_map.end(); itr++)  
    {  
		char num[256];
		itoa(itr->second,num,10);

		vector<string> result=split((itr->first).c_str(),"\\");  
		string filename = result[result.size()-1];

		i % 2 == 0
			? barSeries.AddXY((double)i,(double)itr->second,filename.c_str(),RGB(0,128,255))
			: barSeries.AddXY((double)i,(double)itr->second,filename.c_str(),RGB(255,128,0));

		i++;
    }  

	i = 0;
	dlg.ShowWindow(SW_SHOW);

	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：菜单项“克隆代码统计与分析->克隆代码段分布情况统计（比例）”的消息响应函数
 */
void CMainFrame::OnCcSegFilePie()
{
	// TODO: 在此添加命令处理程序代码
	CCCSegRatioDlg dlg;

	dlg.Create(IDD_CC_SEG_RATIO_DLG,NULL);

	dlg.ClearAllSeries();
	CSeries pieSeries = (CSeries)dlg.m_cc_seg_file_ratio_dlg.Series(0);

	int i = 0;
	srand(time(NULL));
	for (map<string, int>::iterator itr=cc_file_map.begin(); itr!=cc_file_map.end(); itr++)  
    {  
		char num[256];
		itoa(itr->second,num,10);

		vector<string> result=split((itr->first).c_str(),"\\");  
		string filename = result[result.size()-1];

		srand(rand()*i);
		pieSeries.AddXY((double)i,(double)itr->second,(filename + ":" + (char *)(LPCSTR)num + "段").c_str(),RGB(rand() % 256,(rand() + 300) % 256,(rand() + 600) % 256));
		

		i++;
    }  

	i = 0;
	dlg.ShowWindow(SW_SHOW);



	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：菜单项“克隆代码统计与分析->克隆代码行数统计”的消息响应函数
 */
void CMainFrame::OnCcLinesFile()
{
	CCCLinesFileDlg dlg;
	dlg.Create(IDD_CC_LINES_DLG ,NULL);

	dlg.ClearAllSeries();
	CSeries barSeries = (CSeries)dlg.m_cc_lines_file_chart.Series(0);

	int i = 0;
	for (map<string, int>::iterator itr=cc_file_lines_map.begin(); itr!=cc_file_lines_map.end(); itr++)  
    {  
		char num[256];
		itoa(itr->second,num,10);

		vector<string> result=split((itr->first).c_str(),"\\");  
		string filename = result[result.size()-1];

		i % 2 == 0
			? barSeries.AddXY((double)i,(double)itr->second,filename.c_str(),RGB(0,128,255))
			: barSeries.AddXY((double)i,(double)itr->second,filename.c_str(),RGB(255,128,0));

		i++;
    }  

	i = 0;
	dlg.ShowWindow(SW_SHOW);

	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}

/*
 * 函数功能：自定义消息处理函数：处理克隆代码检测过程中更新输出窗口
 */
afx_msg LRESULT CMainFrame::OnUpdateOutput(WPARAM wParam, LPARAM lParam)
{
	CString outText((TCHAR*)lParam);
	m_wndOutput.m_wndOutputBuild.AddString(outText);
	m_wndOutput.m_wndOutputBuild.SendMessage(WM_VSCROLL,SB_BOTTOM); //自动滚动
	return 0;
}


/*
 * 函数功能：菜单项“克隆代码检测->关键克隆代码组识别”的消息响应函数
 */
void CMainFrame::OnShowCodeGraph()
{
	CCodeGraphDlg dlg;
	dlg.Create(IDD_CODE_GRAPH ,NULL);

	dlg.ShowWindow(SW_SHOW);

	CBetweenessCentralityDlg dlg2;
	dlg2.Create(IDD_BETWEENESS_CENTRALITY, NULL);

	dlg2.ShowWindow(SW_SHOW);

	MSG msg;
	BOOL bRet;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
	{ 
		if (bRet == -1)
		{
			// handle the error and possibly exit
	    }
		else
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
		}
	}
}
