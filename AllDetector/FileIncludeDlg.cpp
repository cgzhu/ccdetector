// FileIncludeDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "FileIncludeDlg.h"
#include "afxdialogex.h"
#include <vector>
using namespace std;
extern vector<CString> m_file_include;

// CFileIncludeDlg 对话框

IMPLEMENT_DYNAMIC(CFileIncludeDlg, CDialogEx)

CFileIncludeDlg::CFileIncludeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFileIncludeDlg::IDD, pParent)
{

}

CFileIncludeDlg::~CFileIncludeDlg()
{
}

void CFileIncludeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	//Polaris-20140716
	DDX_Control(pDX, IDC_EDIT1, m_tbDir);
	DDX_Control(pDX, IDC_LIST1, m_wndListBox);
	DDX_Control(pDX, IDC_BUTTON2, m_btnAdd);
	DDX_Control(pDX, IDC_BUTTON3, m_btnDelete);
}


BEGIN_MESSAGE_MAP(CFileIncludeDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CFileIncludeDlg::OnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CFileIncludeDlg::OnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CFileIncludeDlg::OnClickedButton3)
	ON_BN_CLICKED(IDOK, &CFileIncludeDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFileIncludeDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CFileIncludeDlg 消息处理程序

//Polaris-20140716
void CFileIncludeDlg::OnClickedButton1()
{
	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(BROWSEINFO));
	bi.hwndOwner = m_hWnd;
	bi.ulFlags   = BIF_RETURNONLYFSDIRS;
	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	BOOL bRet = FALSE;
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	if (pidl)
	{
		if (SHGetPathFromIDList(pidl, szFolder))  
			bRet = TRUE;
		IMalloc *pMalloc = NULL;
		if (SUCCEEDED(SHGetMalloc(&pMalloc)) && pMalloc)
		{ 
			pMalloc->Free(pidl); 
			pMalloc->Release();
		}

		m_tbDir.SetWindowText(szFolder);
		m_btnAdd.EnableWindow(TRUE);
	}
}

//Polaris-20140716
void CFileIncludeDlg::OnClickedButton2()
{
	if (m_tbDir.GetWindowTextLength()>0)
	{
		CString t;
		m_tbDir.GetWindowText(t);
		m_wndListBox.AddString(t);
		
		m_tbDir.SetWindowText("");
		m_btnAdd.EnableWindow(FALSE);
	}
}


void CFileIncludeDlg::OnClickedButton3()
{
	int i = m_wndListBox.GetCurSel();
	if (i>=0)
	{
		m_wndListBox.DeleteString(i);
	}
}


void CFileIncludeDlg::OnBnClickedOk()
{
	//CDialogEx::OnOK();
	m_file_include.clear();
	for (int i=0;i<m_wndListBox.GetCount();i++)
	{
		CString t;
		m_wndListBox.GetText(i,t);
		m_file_include.push_back(t);
	}

	//同步到注册表
	CWinApp * app = AfxGetApp();
	app->WriteProfileInt("dir","d",m_file_include.size());
	for (int i=0;i<m_file_include.size();i++)
	{
		TCHAR t[20];
		wsprintf(t,"d%d",i);
		app->WriteProfileString("dir",t,m_file_include[i]);
	}

	OnOK();
}


void CFileIncludeDlg::OnBnClickedCancel()
{
	OnCancel();
}


BOOL CFileIncludeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	for (vector<CString>::iterator ite = m_file_include.begin(); ite != m_file_include.end();ite++)
	{
		m_wndListBox.AddString(*ite);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
