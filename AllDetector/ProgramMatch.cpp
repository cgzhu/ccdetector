// ProgramMatch.cpp: implementation of the CProgramMatch class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ProgramMatch.h"
#include "AssignmentUnit.h"
#include "VarRemove.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProgramMatch::CProgramMatch()
{
	
}

CProgramMatch::~CProgramMatch()
{

}

//////////////////////////////////////////////////////////////////////
double ASWEIGHT[9]={0.05, 0.7, 0.2, 0.05,0.10,0.10,0.80,8.00,4.00};///wtt///5.10//
int CProgramMatch::operator()( int nType,int rArry[4],
/*student c-program's USDG  */ CSDGBase *pSHead,
/*template c-program's USDG */ CSDGBase *pMHead,
/*symbol table of student   */ CArray<SNODE,SNODE>   &SSArry,
/*symbol table of template  */ CArray<SNODE,SNODE>   &MSArry,
/*function list of student  */ CArray<FNODE,FNODE>   &SFArry,
/*function list of template */ CArray<FNODE,FNODE>   &MFArry,
/*error list                */ CArray<CERROR,CERROR> &EArry)
{
	/***********************************************************************
		FUNCTION:
		Match the student c-program with the the template c-program and 
		return its matching degree(1-1000).
		PARAMETER:
		as above.
	***********************************************************************/
	///////////////////////////////////////////////////////////////////////
//	extern double ASWEIGHT[];
	///////////////////////////////////////////////////////////////////////

	if(SSArry.GetSize()<1 ||MSArry.GetSize()<1 ||SFArry.GetSize()<1 ||MFArry.GetSize()<1 )
		return 0;
	m_nDegree=0;
	m_nType=nType;
	m_pError=&EArry;
	m_pSS=NULL;
	m_pST=NULL;
	
	
    //extern void PreOrder(int i,CSDGBase *p);
	//PreOrder(100,pSHead);
	//PreOrder(120,pMHead);

	double iCWeight=ASWEIGHT[2];
	double iBWeight=ASWEIGHT[3];


	int iSize=SizeMatch(pSHead,pMHead,SSArry,MSArry,SFArry,MFArry);//规模匹配

	int iStruct=StructMatch(pSHead,pMHead,SSArry,MSArry,SFArry,MFArry);//结构匹配

	extern void PreOrder(int i,CSDGBase *p);
	//PreOrder(100,m_pSS);
	//PreOrder(120,m_pST);
	
	int iComplete=0;
	if(iStruct>=1)
	{
		// completely match

		iComplete=CompletelyMatch(pSHead,pMHead,SSArry,MSArry);//深度匹配

	}

	int iNode=-1;
	if(iComplete<=500)
	{
	//	AfxMessageBox("iComplete<=500");//
	//	iBWeight=0.20;
	   // iCWeight=0.65;
		iBWeight=0.2;//wtt///5.10
	    iCWeight=0.05;//wtt//5.10
		iNode=BranchesMatch(pSHead,pMHead,SSArry,MSArry,SFArry,MFArry);//节点匹配	
	}
	else
	{//	AfxMessageBox("iComplete>500");//
	//	iNode=(int)(iSize*0.3+iStruct*0.2+iComplete*0.5);
	iNode=(int)(iSize*0.3+iStruct*0.5+iComplete*0.2);///wtt///5.10
	}

	extern void DeleteBranch(CSDGBase *pHead);
	DeleteBranch(m_pSS);
	DeleteBranch(m_pST);
	ResetSValue(pSHead);
    
	rArry[0]=iSize;
	rArry[1]=iStruct;
	rArry[2]=iComplete;
	rArry[3]=iNode;
	if(nType<=BOUND)
	{
		
		m_nDegree=(int)(iSize*ASWEIGHT[0]+iStruct*ASWEIGHT[1]+iComplete*iCWeight+iNode*iBWeight);
	}
	else
	{
		m_nDegree=iComplete;
	}
	return m_nDegree;
}


/////////////////////////////////////////////////////////////////////////////////////
//////////////@@@@@@@@@@@@@@@@Function group of Size-Match@@@@@@@@@@@@@//////////////
/////////////////////////////////////////////////////////////////////////////////////


int CProgramMatch::SizeMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the the student c-program's size is be close to template
		c-program and return its degree(1-1000).
		PARAMETER:
		as above.
	***********************************************************************/
	int nSSize=0,nMSize=0;
	nSSize+=SizeOfSDG(pSHead,SSArry,SFArry);
	nMSize+=SizeOfSDG(pMHead,MSArry,MFArry);
	
	nSSize=(int)(1000*(1-abs(nSSize-nMSize)/(nMSize+0.01)));
	if(nSSize<0)
		nSSize=0;
	
	return nSSize;
}

int CProgramMatch::SizeOfSDG(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{
	/***********************************************************************
		FUNCTION:
		Get the program's size.
		PARAMETER:
		...
	***********************************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	int value=0;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p&&p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SDGCONTINUE" || p->m_sType=="SELECTOR" ||
				   p->m_sType=="_ELSE"       || p->m_sType=="ITRSUB"   ||
				   p->m_sType=="ESPSUB"      || p->m_sType=="SDGBREAK" )
				{
					value+=1;
				}

				if(p->m_sType=="#DECLARE")
				{
					value+=1;
				}

				if(p->m_sType=="ASSIGNMENT")
				{
					if(p->m_pinfo && p->m_pinfo->T.key>=CN_CINT && p->m_pinfo->T.key<=CN_CSTRING)
					{
						value+=2;
					}
					else
					{
						value+=3;
					}
				}

				if(p->m_sType=="SELECTION")
				{
					value+=4;
				}

				if(p->m_sType=="ITERATION")
				{
					value+=5;
				}

				if(p->m_sType=="SDGENTRY")
				{
					value+=1;
				}
				
				S.RemoveTail();
			}
				   			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else if(p)
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return value;
}

int CProgramMatch::GetSDGDepth(CSDGBase *pHead)
{
	/***********************************************************************
		FUNCTION:
		get SDG's depth.
		PARAMETER:
		pHead: head pointer of SDG
	***********************************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	int depth=0;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);
			if(S.GetCount()>depth)
			{
				depth=S.GetCount(); 
			}
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return depth;
}

int CProgramMatch::FindIndex(CSDGBase *pson)
{
	/***********************************************
		FUNCTION:
		Find the index of pson in its father;
		PARAMETER:
		pson : ...
	***********************************************/
	
	CSDGBase *p=NULL;
	if(pson)
	{
		p=pson->GetFather();
	}
	else
	{
		return -1;
	}
	
	if(p)
	{
		int i=0;
		while(i<p->GetRelateNum())
		{
			if(p->GetNode(i)&&p->GetNode(i)->idno==pson->idno)
			{
				return i;
			}
			i++;
		}
	}
	return -1;
}


/////////////////////////////////////////////////////////////////////////////////////
//////////////@@@@@@@@@@@@@@@Function group of Struct-Match@@@@@@@@@@@@//////////////
/////////////////////////////////////////////////////////////////////////////////////


int CProgramMatch::StructMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the the student c-program has the same or approximate 
		program struct as template c-program and return its approximation
		(1-1000).
		PARAMETER:
		as above.
	***********************************************************************/

	int iSDepth,iMDepth,rvalue=0;
	int nSvalue,nMvalue;
	//CSDGBase *pssh,*pmsh; 

	iSDepth=GetSDGDepth(pSHead);
	iMDepth=GetSDGDepth(pMHead);

	m_pSS=GetSDGStruct(pSHead);
	//	AfxMessageBox("in struct match ok");////

	m_pST=GetSDGStruct(pMHead);

	//extern void PreOrder(int i,CSDGBase *p);
	//PreOrder(10,m_pSS);
	//PreOrder(20,m_pST);

	MatchStructSDG(m_pSS,m_pST);
	//PreOrder(30,m_pST);

	rvalue=GetStructMatch(m_pST);
	/////////////////////////
/*	CString s;
	s.Format("rvalue=%d",rvalue);
	AfxMessageBox(s);*/
	//////////////////////////////////
	
	nSvalue=GetStructValue(iSDepth,pSHead);
	nMvalue=GetStructValue(iMDepth,pMHead);


	//wtt 修改	nSvalue=(int)(1000*(1-abs(nSvalue-nMvalue)/(nMvalue+0.1)));把被除数改成nSvalue与nMvalue中较大者
	int value;
	if(nSvalue<nMvalue)
		value=nMvalue;
	else
		value=nSvalue;

//	nSvalue=(int)(1000*(1-abs(nSvalue-nMvalue)/(nMvalue+0.1)));//wtt//
	nSvalue=(int)(1000*(1-abs(nSvalue-nMvalue)/(value+0.1)));//wtt///4.12

	/////////////////////////
	
/*	s.Format("nSvalue=%d",nSvalue);
	AfxMessageBox(s);*/
	//////////////////////////////////


	rvalue=(int)(rvalue*0.7+nSvalue*0.3);

	return rvalue;
}

int  CProgramMatch::GetStructMatch(CSDGBase *pMSH)
{
	/***********************************************************************
		FUNCTION:
		Get the struct match result
		PARAMETER:
		pMSH : head pointer of tmplate c-program
	***********************************************************************/
	int rvalue=0;
	int itemp=0;
	int idepth=0;
	CSDGBase *p=NULL;
	CList<CSDGBase *,CSDGBase *> S;

	p=pMSH;
	idepth=GetSDGDepth(pMSH);
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
			// Add your Codes here
			if(p->m_sType=="SDGENTRY" || p->m_sType=="ITERATION" || p->m_sType=="SELECTION")
			{
				if(p->m_sType=="SDGENTRY")
				{
					itemp+=2*(S.GetCount()+1);
				//	AfxMessageBox("SDGENTRY appear");////////////////
					if(p->GetNodeVL()>=BOUND)
					{
					//	AfxMessageBox("SDGENTRY matched");//////////////
						rvalue+=2*(S.GetCount()+1);
					}
				}
				else if(p->m_sType=="ITERATION")
				{
					itemp+=5*(S.GetCount()+1);
					if(p->GetNodeVL()>=BOUND)
					{
						rvalue+=5*(S.GetCount()+1);
					}

				}
				else if(p->m_sType=="SELECTION")
				{
					itemp+=3*(S.GetCount()+1);
					if(p->GetNodeVL()>=BOUND)
					{
						rvalue+=3*(S.GetCount()+1);
					}

				}
			}
			// End of this part

	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
	
	if(itemp<=0)
	{
		return 0;
	}
	/////////////////////////////////
/*	CString s;
	s.Format("%d",(int)(1000*(rvalue/itemp))); 
	AfxMessageBox(s);*/
		////////////////////////////////
//	return (int)(1000*((rvalue)/itemp));//wtt///4.13//只能返回0或1000
	return (int)(1000*((rvalue+0.01)/itemp));//wtt///4.13//
}

int  CProgramMatch::MatchStructSDG(CSDGBase *pSSH,CSDGBase *pMSH)
{
	/***********************************************************************
		FUNCTION:
		Match the student program struct with template program struct
		PARAMETER:
		pSSH: head pointer of S-S-SDG
		pMSH: head pointer of M-S-SDG
	***********************************************************************/

	int value=0;
	CSDGBase *p=NULL;
	CList<CSDGBase *,CSDGBase *> S;

	if(!pSSH || !pMSH)
	{
		return 0;
	}


	p=pSSH;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here
				if(p->m_sType=="SDGENTRY" || p->m_sType=="ITERATION" || p->m_sType=="SELECTION")
				{
				//	if(p->m_sType=="SDGENTRY")
				//	AfxMessageBox("entry matchstructNode called");//////////////
					MatchStructNode(p,pSSH,pMSH);					
				}
				// End of this part

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }	

	return value;	
}

void  CProgramMatch::MatchStructNode(CSDGBase *pNode,CSDGBase *pSSH,CSDGBase *pMSH)
{
	/***********************************************************************
		FUNCTION:
		Find the corresponding S-S-SDG's node of pNode in template program 
		M-S-SDG 
		PARAMETER:
		pSSH: head pointer of S-S-SDG
		pMSH: head pointer of M-S-SDG
	***********************************************************************/
    if(pNode==NULL||pSSH==NULL||pMSH==NULL)
		return;
	
	CSDGBase *pTemp=NULL,*p=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	CArray<CSDGBase*,CSDGBase*> NTRACE,MTRACE;


	pTemp=pNode;
	NTRACE.Add(pTemp);
	while(pTemp && pTemp->m_sType!="SDGHEAD" )
	{
		pTemp=pTemp->GetFather();
		if(pTemp)
		{
			NTRACE.Add(pTemp);
		}		
	}

	p=pMSH;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p&&p->visit==p->GetRelateNum())
			{
				// Add your Codes here
			//	if(p->m_sType==pNode->m_sType)
				if(p->m_sType==pNode->m_sType&&p->GetNodeVL()!=BOUND)///wtt///4.13///必须保证节点尚未匹配过
				{
					pTemp=p;
					MTRACE.Add(pTemp);
					while(pTemp && pTemp->m_sType!="SDGHEAD" )
					{
						pTemp=pTemp->GetFather();
						if(pTemp)
						{
							MTRACE.Add(pTemp);
						}		
					}
					MatchNodeTrace(pNode,p,pSSH,pMSH,NTRACE,MTRACE);
				}

				// End of this part

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
}


void  CProgramMatch::MatchNodeTrace(CSDGBase *pS,CSDGBase *pT,CSDGBase *pSSH,CSDGBase *pMSH,CArray<CSDGBase*,CSDGBase*> &STRACE,CArray<CSDGBase*,CSDGBase*> &TTRACE)
{
	/***********************************************************************
		FUNCTION:
		Match the trace of two node pS and pT
		PARAMETER:
		pSSH   : head pointer of S-S-SDG
		pMSH   : head pointer of M-S-SDG
		STRACE : trace of pS
		TTRACE : trace of pT
	***********************************************************************/
    if(pS==NULL||pT==NULL||pSSH==NULL||pMSH==NULL)
		return;
	if(TTRACE.GetSize()<=0 || STRACE.GetSize()<=0 || TTRACE.GetSize()!=STRACE.GetSize() )
	{
		return;
	}
    int ix;
	for( ix=0;ix<STRACE.GetSize();ix++)
	{
		if(TTRACE[ix]==NULL||STRACE[ix]==NULL||STRACE[ix]&&TTRACE[ix]&&STRACE[ix]->m_sType!=TTRACE[ix]->m_sType)
		{
		//	AfxMessageBox("if(STRACE[ix]->m_sType!=TTRACE[ix]->m_sType)");/////
			return;
		}
	}

    CString sLab(""),tLab("");
	CSDGBase *pTemp=NULL;
	
	for(ix=0;ix<STRACE.GetSize();ix++)
	{
		sLab=_T("");
		tLab=_T("");
		if(STRACE[ix]->m_sType!="SDGHEAD" && STRACE[ix]->GetFather())
		{
			int itemp=FindIndex(STRACE[ix]);
			for(int i=0;i<itemp;i++)
			{
				if(STRACE[ix]->GetFather()&&STRACE[ix]->GetFather()->GetNode(i)&&(STRACE[ix]->GetFather()->GetNode(i)->m_sType=="SDGENTRY" ||
				   STRACE[ix]->GetFather()->GetNode(i)->m_sType=="ITERATOR" ||
				   STRACE[ix]->GetFather()->GetNode(i)->m_sType=="SELECTION"||
				   STRACE[ix]->GetFather()->GetNode(i)->m_sType=="SELECTOR" ||
				   STRACE[ix]->GetFather()->GetNode(i)->m_sType=="ITRSUB"))
				{
					sLab+=STRACE[ix]->GetFather()->GetNode(i)->m_sType;
				}
			}

			itemp=FindIndex(STRACE[ix]);
			int i;
			for(i=0;i<itemp;i++)
			{
				if(TTRACE[ix]->GetFather()&&TTRACE[ix]->GetFather()->GetNode(i)&&(i<TTRACE[ix]->GetFather()->GetRelateNum())&&
				   (TTRACE[ix]->GetFather()->GetNode(i)->m_sType=="SDGENTRY" ||
				   TTRACE[ix]->GetFather()->GetNode(i)->m_sType=="ITERATOR" ||
				   TTRACE[ix]->GetFather()->GetNode(i)->m_sType=="SELECTION"||
				   TTRACE[ix]->GetFather()->GetNode(i)->m_sType=="SELECTOR" ||
				   TTRACE[ix]->GetFather()->GetNode(i)->m_sType=="ITRSUB"))
				{
					tLab+=TTRACE[ix]->GetFather()->GetNode(i)->m_sType;
				}
			}

			if(sLab!=tLab)
			{
			//	AfxMessageBox("sLab!=tLab");/////////
				return;
			}
		}
	}

	pS->pCorrspM=pT->pCorrspS;
	pT->pCorrspM=pS->pCorrspS;
	pT->SetNodeVL(BOUND);	
//	AfxMessageBox("pT->SetNodeVL(BOUND)");//////////////
}


int CProgramMatch::GetStructValue(const int depth,CSDGBase *pHead)
{
	/***********************************************************************
		FUNCTION:
		...
		PARAMETER:
		pHead: head pointer of SDG
	***********************************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;	
	int value=0;
	int weigth=1;

	p=pHead;

    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);			
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your code here
				weigth=S.GetCount()+1;
				if(p->m_sType=="SDGCONTINUE" || p->m_sType=="SDGBREAK" )
				{
					weigth=(S.GetCount()+1);
				}
				
				if(p->m_sType=="#DECLARE")
				{
					weigth=(S.GetCount()+1)*1;
				}

				if(p->m_sType=="ASSIGNMENT" || p->m_sType=="SDGCALL")
				{
					if(p->m_sType=="SDGCALL"&&p->m_pinfo&&p->m_pinfo->T.name=="printf" )
					{//wtt///5.13/////printf的权值设小点，减小学生打的提示信息对程序评分的影响
						if(p->m_pinfo->info==1)
						{
							CSDGBase*pf=NULL;
							pf=p->GetFather();
							int index=0;
							index=FindIndex(p);
							if(pf&&pf->m_sType=="SDGENTRY")
							{//提示信息的printf语句不进行结构匹配//权值为0
								weigth=(S.GetCount()+1)*0;
							//	AfxMessageBox("printf 0"+p->m_pinfo->pinfo[0]->T.name  );
							}
							else if(pf)
							{
							//	AfxMessageBox("printf 2"+p->m_pinfo->pinfo[0]->T.name );
									weigth=(S.GetCount()+1)*2;
							}

						}
						else//其他printf语句权值为2
						{
							//	AfxMessageBox("printf 2"+p->m_pinfo->pinfo[0]->T.name );
									weigth=(S.GetCount()+1)*2;

						}
						//AfxMessageBox("printf");////
					}
					else
						weigth=(S.GetCount()+1)*2;
				/////////////////wtt//5.13	
				}

				if(p->m_sType=="SELECTOR" || p->m_sType=="_ELSE")
				{
					weigth=(S.GetCount()+1)*3;
				}

				if(p->m_sType=="ITRSUB")
				{
					weigth=(S.GetCount()+1)*2;
				}

				if(p->m_sType=="ESPSUB" && p->GetRelateNum()>0 )
				{
					weigth=(S.GetCount()+1)*2;
				}

				if(p->m_sType=="SELECTION")
				{
					weigth=(S.GetCount()+1)*3;
				}

				if(p->m_sType=="ITERATION")
				{
					weigth=(S.GetCount()+1)*5;				
				}

				if(p->m_sType=="SDGENTRY")
				{
					weigth=(S.GetCount()+1)*1;					
				}
				
				value+=weigth;

				// end of this part

				S.RemoveTail();
			}				   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return value;
}
CSDGBase*ExGetSDGStruct(CSDGBase *pHead)
{// 测试用用于显示pHead的结构图
	CProgramMatch programmatch;
	CSDGBase*pstruct=NULL;
	pstruct=programmatch.GetSDGStruct(pHead);
	return pstruct;

}
CSDGBase *CProgramMatch::GetSDGStruct(CSDGBase *pHead)
{
	/***********************************************************************
		FUNCTION:
		Get the SDG's struct, and return it;
		PARAMETER:
		as above.
	***********************************************************************/
	
	if(pHead==NULL)
		return NULL;
	CList<CSDGBase *,CSDGBase *> S,ST;
    CSDGBase *p=NULL,*pUnit=NULL;
	CSDGBase *pNHead=NULL;   
	extern int SDGNODEID;
	SDGNODEID=0;
	pUnit=new CSDGBase;
	pUnit->m_sType=pHead->m_sType;
	pNHead=pUnit;
	p=pHead;

	while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p);
			ST.AddTail(pUnit);
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);

				// copy  node or create new node to Unit Representation,and..
				pUnit=new CSDGBase;
				if(p->m_sType=="SELECTION" || p->m_sType=="ITERATION" || 
				   p->m_sType=="ITRSUB"    || p->m_sType=="ESPSUB" || 
				   p->m_sType=="SELECTOR"  || p->m_sType=="_ELSE"||
				   p->m_sType=="SDGENTRY")
				{
					pUnit->pCorrspS=p;
					pUnit->m_sType=p->m_sType;
				}

				if(p->m_sType=="SDGCALL" && p->m_pinfo && p->m_pinfo->T.name=="printf")
				{
					pUnit->m_sType="PRINTF";
				}
				
				ST.GetTail()->Add(CN_CONTROL,pUnit);
				pUnit->SetFather(ST.GetTail());
				// End
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);

				// copy  node or create new node to Unit Representation
				pUnit=new CSDGBase;

				if(p->m_sType=="SELECTION" || p->m_sType=="ITERATION" || 
				   p->m_sType=="ITRSUB"    || p->m_sType=="ESPSUB" || 
				   p->m_sType=="SELECTOR"  || p->m_sType=="_ELSE"||
				   p->m_sType=="SDGENTRY")
				{
					pUnit->pCorrspS=p;
					pUnit->m_sType=p->m_sType;
				}

				if(p->m_sType=="SDGCALL" && p->m_pinfo && p->m_pinfo->T.name=="printf")
				{
					pUnit->m_sType="PRINTF";
				}
				
				ST.GetTail()->Add(CN_CONTROL,pUnit);
				pUnit->SetFather(ST.GetTail());			
			}
			else
			{			
				p->visit=0;
				p=NULL;
				S.RemoveTail();
				ST.RemoveTail(); 
			}
		}
    }

	p=pNHead;
	S.RemoveAll(); 
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);			
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here

				for(int i=p->GetRelateNum()-1;i>=0;i--)
				{
					if(p->GetNode(i)&&p->GetNode(i)->m_sType=="PRINTF" )
					{
						if(! ( (p->m_sType=="ITRSUB" || p->m_sType=="ESPDUB" || 
								p->m_sType=="SELECTOR") && p->GetRelateNum()==1) )
						{
							if(p->GetNode(i))
								delete p->GetNode(i);
					
							p->Del(i); 
						}
						////////wtt///5/16///
						if( ( (p->m_sType=="ITRSUB" || p->m_sType=="ESPDUB" || 
								p->m_sType=="SELECTOR") && p->GetRelateNum()==1) )
						{
							if(p->GetNode(i))
							p->GetNode(i)->m_sType="SDGBASE";

						}
						/////////////////////
					}
				}

				if(p->GetRelateNum()>1)
				{
					for(int i=p->GetRelateNum()-1;i>0;i--)
					{
						if(i>0 && p->GetNode(i-1)&&p->GetNode(i)&&p->GetNode(i-1)->m_sType=="SDGBASE" &&p->GetNode(i)->m_sType=="SDGBASE")
						{
							if(p->GetNode(i) )
							delete p->GetNode(i);
							p->Del(i); 
						}
					}
				}
				int i;

				for(i=p->GetRelateNum()-1;i>=0;i--)
				{
					if(p->GetNode(i)&&(p->GetNode(i)->m_sType=="SELECTION" || p->GetNode(i)->m_sType=="ITERATION" || 
					    p->GetNode(i)->m_sType=="ITRSUB"    || p->GetNode(i)->m_sType=="ESPSUB" || 
				        p->GetNode(i)->m_sType=="SELECTOR"  || p->GetNode(i)->m_sType=="_ELSE"||
				        p->GetNode(i)->m_sType=="SDGENTRY") && p->GetNode(i)->GetRelateNum()<=0)
					{
						delete p->GetNode(i);
						p->Del(i);
					}
				}
				

				// End of this part
				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
	   
	return pNHead;

}

CSDGBase *CProgramMatch::SearchFirstCertainNode(CSDGBase *pSDG,CString strNode)
{
	/******************************************************

	    FUNCTION:
		Pre-order search node with the type specified by 
		strNode in pSDG.

		(此函数调用时必须确保各节点的visit值为零)
	    
		PARAMETER:
		....		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL,*pTemp=NULL;
	bool bFind=false;

	p=pSDG;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			if(!bFind)
			{
				if(p->m_sType==strNode)
				{
					pTemp=p;
					bFind=true;
				}
			}
			
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return pTemp;
}



/////////////////////////////////////////////////////////////////////////////////////
/////////////@@@@@@@@@@@@@@Function group of Completely-Match@@@@@@@@@@@/////////////
/////////////////////////////////////////////////////////////////////////////////////


int CProgramMatch::CompletelyMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the the student c-program could completely match the the 
		template c-program and return its matching degree(1-1000).
		PARAMETER:
		as above.
	***********************************************************************/
    CSDGBase *p=NULL,*pTemp=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	//AfxMessageBox("complete match called");////////////
	p=pMHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;

				// Add your Codes here
				if(p->m_sType=="SDGENTRY" || p->m_sType=="ITERATION" || p->m_sType=="SELECTION" )
				{
					pTemp=FindCorresSNode(p->idno,m_pST);
					if(pTemp)
					{
						////////////////////////////////////////
						/*if(p->m_sType=="ITERATION")
							AfxMessageBox("iteration !");*/
							////////////////////////////////////////
						CompleteMatchNode(pTemp->pCorrspM,p,pSHead,pMHead,SSArry,MSArry);
					}
				}
				// End of this part

				p=NULL;
			}
		}
    }

	return GetCompleteValue(pMHead);
}

int CProgramMatch::GetCompleteValue(CSDGBase *pMHead)
{
	/***********************************************************************
		FUNCTION:
		post order tranverse the template c-program's SDG and count it 
		the matching result.
		PARAMETER:
		pMHead :head pointer of template c-program's SDG.
	***********************************************************************/

	int nvalue=0,count=0,ndcount=0,ndvalue=0,ntemp=0;
	CSDGBase *p=NULL,*pTemp=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	p=pMHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;

				// Add your Codes here
				if(p->m_sType!="SDGHEAD" && p->m_sType!="ESPSUB")
				{
					ntemp=NodeWeight(p);
					//////////////////////////////////
				/*	CString stemp;
					stemp.Format("type=%s,weight=%d",p->m_sType,ntemp);
					AfxMessageBox(stemp);*/
					////////////////////////////////////

					if(p->GetNodeVL()>=0)
					{						
						bool btemp=false;
						CSDGBase *pTemp=p;
						while(pTemp!=NULL &&pTemp->m_sType!="SDGENTRY")
						{
							if(pTemp->m_sType=="ESPSUB")
							{
								btemp=true;
							}

							pTemp=pTemp->GetFather();							
							if(pTemp==NULL)
							{
								break;
							}							
						}

						if(m_nType<=BOUND)
						{
							if(!btemp)
							{
								nvalue+=p->GetNodeVL()*ntemp;
								//////////////wtt///////////5.14//////////////
								if(p->GetNodeVL()==0&&p->m_sType=="SDGCALL"&&p->m_pinfo&&p->m_pinfo->T.name=="printf"&&p->m_pinfo->info==1&&p->GetFather()&&p->GetFather()->m_sType== "SDGENTRY" )
								{//wtt///5.14/////printf的权值设小点，减小学生打的提示信息对程序评分的影响
							
											count+=0;
										/*	CString stemp;
					                         stemp.Format("  printf statement :p->GetNodeVL()=%d,ntemp=%d",p->GetNodeVL(),ntemp);
					                         AfxMessageBox(GetENodeStr(p->m_pinfo)+stemp);
										*/	//	AfxMessageBox("printf 0"+p->m_pinfo->pinfo[0]->T.name  );
								}
                    			else 
								{

									count+=ntemp;
								/*	  CString stemp;
					                   stemp.Format("  p->GetNodeVL()=%d,ntemp=%d",p->GetNodeVL(),ntemp);
					                    AfxMessageBox(p->m_sType+GetENodeStr(p->m_pinfo)+stemp);
                         
								*///	AfxMessageBox("count+ntemp");
								}

								////////////////////////////////////////
								//count+=ntemp;
								////////////////////////
									if(p->m_sType=="ASSIGNMENT")
								{
									//////////////////////////////////
		/*		CString stemp;
					stemp.Format(" Assignment statement :p->GetNodeVL()=%d,ntemp=%d",p->GetNodeVL(),ntemp);
					AfxMessageBox("="+GetENodeStr(p->m_pinfo)+stemp);*/
					////////////////////////////////////	
								}
									////////////////////
									//////////////////////////////////
				/*	CString stemp;
						stemp.Format(":p->GetNodeVL()=%d,ntemp=%d	,nvalue=%d,	count=%d",p->GetNodeVL(),ntemp,nvalue,count);
						AfxMessageBox(p->m_sType+GetENodeStr(p->m_pinfo)+stemp);
				*/	////////////////////////////////////
							
							}

							if(p->m_sType=="#DECLARE")
							{
								ndcount++;
								ndvalue+=p->GetNodeVL();
							}
						}
						else//if(m_nType<=BOUND)
						{
							if(p->important>=1)
							{
								if(!btemp)
								{
							//		AfxMessageBox("else1 ");////
									nvalue+=p->GetNodeVL()*ntemp;
									count+=ntemp;
								}
							}
						}
						
					}
					else//if(p->GetNodeVL()>=0)
					{
						if(m_nType<=BOUND)
						{
						//	AfxMessageBox("else2 "+p->m_sType );////
						//	AfxMessageBox(GetExpString(p->m_pinfo));//////////
							count+=ntemp;
						}
						else////if(m_nType<=BOUND)
						{
							if(p->important>=1)
							{
								AfxMessageBox("else3 ");////
								count+=ntemp;
							}
						}
					}
				}
				// End of this part

				p=NULL;
			}
		}
    }
	
    if(count-ndcount==0) count=BOUND;
	//int value=(int)((nvalue-ndvalue)/(count-ndcount);//wtt//4.13
///////////////////////////////////////////////////////////////////////////
	/*CString stemp;
	stemp.Format(" nvalue=%d,ndvalue=%d,count=%d,ndcount=%d",nvalue,ndvalue,count,ndcount);
	AfxMessageBox(stemp);*/
	//////////////////////////////////////
	int value=(int)((abs(nvalue-ndvalue)+0.01)/abs(count-ndcount));//wtt//4.13

	return value;

}

void CProgramMatch::CompleteMatchNode(CSDGBase *pSN,CSDGBase *pMN,
										   CSDGBase *pSHead,CSDGBase *pMHead,
										   CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		match the important node(ITERATION , SELECTION and ENTRY) and the 
		nodes close to it
		PARAMETER:
		pSN : important node in student c-program
		pMN : important node in template c-program (corresponding part of pSN)
		....
	***********************************************************************/
	if(pSN==NULL||pMN==NULL||pSHead==NULL||pMHead==NULL)
	 return;
	int ntemp=0,nvalue=0,index=0;

	// match its sub nodes
	if(pMN->m_sType=="ITERATION") 
	{
	//	pMN->SetNodeVL(-BOUND);
		if(pSN->m_sType=="ITERATION" &&pSN->GetNodeVL()==0 )
		{
			ntemp=BoolExpressionMatch(pSN->m_pinfo,pMN->m_pinfo,SSArry,MSArry);

			////////////////////////////////////////////
			/*CString stemp;
			stemp.Format("iteration boolexpression match=%d,Bound=%d",ntemp,BOUND); 
			AfxMessageBox(stemp);*/
			////////////////////////////////////////////////////

			if(ntemp>0)
			{
				pMN->SetNodeVL(ntemp);
				if(ntemp>500)
				{
					pSN->SetNodeVL(ntemp); 
				}
			}
			
		} 
	}

		

	if(pMN->m_sType=="SDGENTRY"  ||  pMN->m_sType=="SELECTOR" || pMN->m_sType=="ITERATION"|| pMN->m_sType=="ITRSUB")
	{

		if(pMN->m_sType=="SDGENTRY" )
		{
			pMN->SetNodeVL(BOUND);
		}
	

		for(int ix=0;ix<pMN->GetRelateNum();ix++)
		{
			CSDGBase *pmnode=pMN->GetNode(ix);
		
			for(int i=0;i<pSN->GetRelateNum();i++)
			{
				CSDGBase *psnode=pSN->GetNode(i);
				if(psnode->m_sType==pmnode->m_sType)
				{
					if(pmnode->m_sType=="SDGRETURN"&&psnode->m_sType=="SDGRETURN"&&pmnode->GetNodeVL()==0&&psnode->GetNodeVL()==0 )
					{
						if(pmnode->m_pinfo==NULL&&psnode->m_pinfo==NULL )
							pmnode->SetNodeVL(BOUND);

						break;
					}
					//////////wtt////2007.5.28////////////////
					if((pmnode->m_sType=="SDGCONTINUE" || pmnode->m_sType=="SDGBREAK" )&&pmnode->GetNodeVL()==0&&psnode->GetNodeVL()==0 )
					{
							pmnode->SetNodeVL(BOUND);
					}
					////////////////wtt////////////////////////

					if(pmnode->m_sType=="SELECTION"||pmnode->m_sType=="ITERATION")
					{
						if(pmnode->m_pinfo==NULL&&psnode->m_pinfo==NULL )
							pmnode->SetNodeVL(BOUND);
						else if(pmnode->m_pinfo!=NULL&&psnode->m_pinfo!=NULL)///////wtt 2007.11.12//有返回值得return的匹配
						{
							if(pmnode->m_pinfo->T.name==psnode->m_pinfo->T.name)
							{
							//	AfxMessageBox(pmnode->m_pinfo->T.name+"return pmnode->SetNodeVL(BOUND)");
								pmnode->SetNodeVL(BOUND);

							}
						}


						break;
					}

					if(pmnode->m_sType=="ASSIGNMENT")
					{
						CAssignmentUnit *pAsm=(CAssignmentUnit *)pmnode;
						CAssignmentUnit *pAss=(CAssignmentUnit *)psnode;

						if(psnode->GetNodeVL()==0&& pAsm->m_pleft && pAss->m_pleft && 
						   IsSameVariable(pAss->m_pleft->T,pAsm->m_pleft->T,SSArry,MSArry))
						{
						//	pmnode->SetNodeVL(-BOUND);
							///////////////wtt 2007.5.29//////////
					//		ntemp=ArithExpressionMatch(pAsm->m_pinfo,pAsm->m_pinfo,SSArry,MSArry);
							//////////wtt  2007.11.12/////////考虑赋值语句右侧为函数调用的情况//////
							if(pAss->m_pinfo&&pAsm->m_pinfo&&pAss->m_pinfo->T.key!=CN_DFUNCTION&&pAss->m_pinfo->T.key!=CN_BFUNCTION&&pAsm->m_pinfo->T.key!=CN_DFUNCTION&&pAsm->m_pinfo->T.key!=CN_BFUNCTION)
							{
								ntemp=ArithExpressionMatch(pAss->m_pinfo,pAsm->m_pinfo,SSArry,MSArry);
							}
							else if (pAss->m_pinfo&&pAsm->m_pinfo&&(pAss->m_pinfo->T.key==CN_DFUNCTION||pAss->m_pinfo->T.key==CN_BFUNCTION)&&(pAsm->m_pinfo->T.key==CN_DFUNCTION||pAsm->m_pinfo->T.key==CN_BFUNCTION))
							{
								ntemp=FunctionMatch(pAss->m_pinfo,pAsm->m_pinfo,SSArry,MSArry);
							}
							//////////wtt  2007.11.12///////////////

						
		/*	CString stemp;
			stemp.Format("arith expression match=%d,Bound=%d",ntemp,BOUND); 
			AfxMessageBox(pMN->m_sType);
			AfxMessageBox(GetExpString(pAss->m_pleft,SSArry)+"="+GetExpString(pAss->m_pinfo,SSArry));
		AfxMessageBox(GetExpString(pAsm->m_pleft,MSArry)+"="+GetExpString(pAsm->m_pinfo,MSArry));
		AfxMessageBox(stemp);*/
			////////////////////////////////////////////////////
						if(ntemp>0)
							{
								pmnode->SetNodeVL(ntemp);
								if(ntemp>500)
								{
									psnode->SetNodeVL(ntemp); 
								}
								

							}
							break;
						}

					}// if(pMN->GetNode(i)->m_sType=="ASSIGNMENT")

					if(pmnode->m_sType=="#DECLARE")
					{
						if(pmnode->TCnt.GetSize()>0&&pmnode->TCnt[0].name==psnode->TCnt[0].name)
						{
						//	AfxMessageBox("set delclare node matched");
							pmnode->SetNodeVL(BOUND);							
							break;
						}
					}// if(pMN->GetNode(i)->m_sType=="SDGCALL")
					
					if(pmnode->m_sType=="SDGCALL")
					{
				
						 if(psnode->GetNodeVL()==0&& pmnode->m_pinfo && psnode->m_pinfo && 
						   pmnode->m_pinfo->T.name==psnode->m_pinfo->T.name)
						{
							

							pmnode->SetNodeVL(-BOUND);
							
							ntemp=FunctionMatch(psnode->m_pinfo,pmnode->m_pinfo,SSArry,MSArry);
					////////////////////////////////////////////
			
								
			
				/*	AfxMessageBox(GetExpString(psnode->m_pinfo,SSArry));
					AfxMessageBox(GetExpString(pmnode->m_pinfo,MSArry));
					CString stemp;
						
					stemp.Format(  "  function match=%d,Bound=%d",ntemp,BOUND); 
					AfxMessageBox(pmnode->m_pinfo->T.name+stemp);*/
			////////////////////////////////////////////////////

					       if(ntemp>0)
							{
								pmnode->SetNodeVL(ntemp);
								if(ntemp>500)
								{
									psnode->SetNodeVL(ntemp); 
								}
							}
		
							break;
						}
						 	
					}// if(pMN->GetNode(i)->m_sType=="SDGCALL")


				}// if(pSN->GetNode(i)->m_sType==p...

				
			}// for(int i=0;i<pSN->Get...

			
		}// for(int ix=0;ix<pMN-...

	}// if(pMN->m_sType=="ITERATION" || pMN-....



	if(pMN->m_sType=="SELECTION" && pSN->m_sType=="SELECTION"&&pSN->GetNodeVL()==0 )
	{
		index=0;
		pMN->SetNodeVL(BOUND);
		pSN->SetNodeVL(BOUND);
	
		for(int ix=0;ix<pMN->GetRelateNum();ix++)
		{
		//	pMN->GetNode(ix)->SetNodeVL(-BOUND);

			if(index<pSN->GetRelateNum())
			{
					CSDGBase*psnode=pSN->GetNode(index)	;
					CSDGBase*pmnode=pMN->GetNode(ix)	;
				//////////////wtt//////4.11///////////////////////
				if(pSN->GetNode(index)&&pSN->GetNode(index)->m_sType=="ASSIGNMENT"&&pMN->GetNode(ix)->m_sType=="ASSIGNMENT" )
				{
				

					CAssignmentUnit *pAsm=(CAssignmentUnit *)pmnode;
						CAssignmentUnit *pAss=(CAssignmentUnit *)psnode;

					//	if(pAsm->m_pleft && pAss->m_pleft && 
					//	   IsSameVariable(pAss->m_pleft->T,pAsm->m_pleft->T,SSArry,MSArry))
												
						if(pAsm->m_pleft && pAss->m_pleft && GetExpString(pAsm->m_pleft,MSArry)==GetExpString(pAss->m_pleft,SSArry) )///////wtt/////2006.3.21///

						//		if(pAsm->m_pleft && pAss->m_pleft && GetExpString(pAsm->m_pleft)==GetExpString(pAss->m_pleft) )
						{
						//	pmnode->SetNodeVL(-BOUND);
							ntemp=ArithExpressionMatch(pAss->m_pinfo,pAsm->m_pinfo,SSArry,MSArry);
								////////////////////////////////////////////
		/*	CString stemp;
			stemp.Format("assignment under selection Arith expression match=%d,Bound=%d",ntemp,BOUND); 
			AfxMessageBox(stemp);*/
			////////////////////////////////////////////////////
							if(ntemp>0)
							{
								pmnode->SetNodeVL(ntemp);
								if(ntemp>500)
								{
									psnode->SetNodeVL(ntemp); 
								}
							}
						
						}
				}
				////////////////////////////wtt///////////////4.11/////////////////
			else if(pSN->GetNode(index)&&pSN->GetNode(index)->m_sType=="SELECTOR"&&pMN->GetNode(ix)->m_sType=="SELECTOR" )//wtt////
			{
			//	AfxMessageBox(GetExpString(pSN->GetNode(index)->m_pinfo));//////////
			//	AfxMessageBox(GetExpString(pMN->GetNode(ix)->m_pinfo));//////////

			ntemp=BoolExpressionMatch(pSN->GetNode(index)->m_pinfo,pMN->GetNode(ix)->m_pinfo,SSArry,MSArry);
							////////////////////////////////////////////
		/*	CString stemp;
			stemp.Format("index=%d,ix=%d,selector condition match=%d,Bound=%d",index,ix,ntemp,BOUND); 
			AfxMessageBox(stemp);*/
			////////////////////////////////////////////////////
				if(ntemp>0)
				{
					if(pMN->GetNode(ix))
					pMN->GetNode(ix)->SetNodeVL(ntemp);
					if(ntemp>500)
					{
						pSN->GetNode(index)->SetNodeVL(ntemp);	
				
					}
				}

				CompleteMatchNode(pSN->GetNode(index),pMN->GetNode(ix),pSHead,pMHead,SSArry,MSArry);
			}
			
			}
				index++;
		}
	}
       

}

bool CProgramMatch::IsSameVariable(TNODE &TS,TNODE &TM,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the Varibles TS and TM have the same name or same address.
		PARAMETER:
		TS : variable tnode1
		TM : variable tnode2
		SSArry : symbol table 1
		MSArry : symbol table 2
	***********************************************************************/

	if(TS.key!=CN_VARIABLE || TM.key!=CN_VARIABLE )
	{
		return false;
	}

	if(TS.addr>=0&&TS.addr<SSArry.GetSize()&&TS.name==TM.name && SSArry[TS.addr].kind!=CN_POINTER && MSArry[TM.addr].kind!=CN_POINTER)
	{
		return true;
	}

	CString strNames(""),strNamem("");
	
	if(TS.addr>=0&&TS.addr<SSArry.GetSize()&&(SSArry[TS.addr].kind==CN_VARIABLE || SSArry[TS.addr].kind==CN_ARRAY))
	{
		strNames=SSArry[TS.addr].name; 
	}
	else
	{
		if(TS.addr>=0&&TS.addr<SSArry.GetSize()&&SSArry[TS.addr].kind==CN_POINTER && TS.paddr>=0 && TS.paddr<SSArry.GetSize())
		{
			strNames=SSArry[TS.paddr].name;
		}
	}

	if(TM.addr>=0&&TM.addr<MSArry.GetSize()&&(MSArry[TM.addr].kind==CN_VARIABLE || MSArry[TM.addr].kind==CN_ARRAY))
	{
		strNamem=MSArry[TM.addr].name; 
	}
	else
	{
		if(TM.addr>=0&&TM.addr<MSArry.GetSize()&&MSArry[TM.addr].kind==CN_POINTER && TM.paddr>=0 && TM.paddr<MSArry.GetSize())
		{
			strNamem=MSArry[TM.paddr].name;
		}
	}

	return (strNamem==strNames);
}

int CProgramMatch::FunctionMatch(ENODE *pSF,ENODE *pTF,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		match the two functions
		PARAMETER:
		pSF : function exp of student c-program
		pTF : function exp of template c-program
	***********************************************************************/	
    if(pSF==NULL||pTF==NULL)
      return 0;
	if(pSF->T.name=="printf" && pTF->T.name=="printf")
	{
		///////////wtt//////////////2005/8.28///
		//return BOUND;
		if(pSF->info!=pTF->info  )
			return BOUND-500;
		
		if(pSF->info==2)
		{
		
			if(pSF->pinfo[0]&&pTF->pinfo[0])
			{
				//	AfxMessageBox(pSF->pinfo[0]->T.name);
                // AfxMessageBox(pTF->pinfo[0]->T.name);
				
			}
			if(pSF->pinfo[0]&&pTF->pinfo[0]&&pSF->pinfo[0]->T.name==pTF->pinfo[0]->T.name)
			{
				//	AfxMessageBox("equal");
                
				return BOUND;
			}
			else
			{   
			//	AfxMessageBox("not equal");
				return BOUND-500;
				
			}
		}
////////////////////////////////wtt/////////////////

    	return BOUND;
	}

	int nbeg=0,nvalue=0;
	

	if(pSF->T.name=="scanf")
	{


		/////////////wtt//////////////
			if(pSF->info!=pTF->info  )
			return BOUND-500;
		
		if(pSF->info==2)
		{
		
			if(pSF->pinfo[0]&&pTF->pinfo[0])
			{
				//	AfxMessageBox(pSF->pinfo[0]->T.name);
                // AfxMessageBox(pTF->pinfo[0]->T.name);
				
			}
			if(pSF->pinfo[0]&&pTF->pinfo[0]&&pSF->pinfo[0]->T.name==pTF->pinfo[0]->T.name)
			{
				//	AfxMessageBox("equal");
                
				return BOUND;
			}
			else
			{   
			//	AfxMessageBox("not equal");
				return BOUND-500;
				
			}
		}


		/////////////////////////
		nbeg++;
	}

	if(pSF->info!=pTF->info)
	{
		return 0;
	}

	if(pSF->T.key==CN_DFUNCTION && pSF->info==0) 
	{
		return BOUND;
	}
	int ix;
	for( ix=nbeg;ix<pSF->info&&pSF->info<10;ix++)
	{
		nvalue+=ArithExpressionMatch(pSF->pinfo[ix],pTF->pinfo[ix],SSArry,MSArry);		
	}
    
    if(ix-nbeg==0) ix=1000;
	return (int)(nvalue/(ix-nbeg));


}

CSDGBase *CProgramMatch::FindCorresSNode(const int ID,CSDGBase *pSH)
{
	/***********************************************************************
		FUNCTION:
		Find the corresponding part of node "ID" in SDG's struct(head pointer
		pSH), and return it.
		PARAMETER:
		ID  : the ID of one node in SDG
		pSH : the head pointer of template c-program SDG's struct
	***********************************************************************/
	if(pSH==NULL)
		return NULL;
	CSDGBase *pTemp=NULL,*p=NULL;
	CList<CSDGBase *,CSDGBase *> S;

	p=pSH;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here
				if(p->GetNodeVL()>=BOUND && p->pCorrspS)
				{
					if(p->pCorrspS->idno==ID)
					{
						pTemp=p;
					}
				}
				// End of this part

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }		
		
	return pTemp;
}


int CProgramMatch::BoolExpressionMatch(ENODE *pSE,ENODE *pTE,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Compare the bool expression pSE with expression pST and return its 
		result
		PARAMETER:
		pSE  : head pointer of expression in student program.
		pTE  : head pointer of expression in template program.
	***********************************************************************/
	if(pSE==NULL&&pTE==NULL)
		return BOUND;
	if(pSE==NULL||pTE==NULL)
		return 0;
	//extern CString GetExpString(ENODE *pEHead,bool bNIDX=true);
	int rvalue=0;
	int itemp=0,count=0;
	ENODE *p=NULL;
	TNODE T;
	CString sexps(""),sexpt("");
	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL;
	
	sexps=GetExpStringEx(pSE,SSArry);
	sexpt=GetExpStringEx(pTE,MSArry);
    //AfxMessageBox(sexps+":"+sexpt);/////

	if(sexps==sexpt)
	{
	//	AfxMessageBox("BOUND");
		return BOUND;
	}
	
	p=pTE;

	if(p==NULL)
	{
		return 0;
	}

	// post order tranverse the syntax tree
	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2);
				p=p->pright;
				iscontinue=false;
				break;
			case 2:
				// add action here				
				itemp=GetFatherType(p,pTE);
				T.key=itemp;

				if(OperatorKind(T)==0  && IsTerminal(p)>=1)
				{
					rvalue+=BoolPartMatch(itemp,pSE,p,SSArry,MSArry);
					count++;
				}

				if(OperatorKind(p->T)==1)
				{
					rvalue+=BoolPartMatch(itemp,pSE,p,SSArry,MSArry);
					count++;
				}
				
				//end of this part
				break;
			}

		}

	}while(STK.GetCount()>0);

	if(count<=0) count=10;
	
	return (int)(rvalue/count);
}


int CProgramMatch::ArithExpressionMatch(ENODE *pSE,ENODE *pTE,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Compare the arithmetic expression pSE with expression pST and return 
		its result
		PARAMETER:
		pSE  : head pointer of expression in student program.
		pTE  : head pointer of expression in template program.
	***********************************************************************/
    if(pSE==NULL&&pTE==NULL)
		return BOUND;
	if(pSE==NULL||pTE==NULL)
       return 0;
	//extern CString GetExpString(ENODE *pEHead,bool bNIDX=true);
	TNODE T;
	ENODE *p=NULL;
	int rvalue=0,itemp=0,count=0;
	CString sexps(""),sexpt("");
	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL;
	
	//sexps=GetExpStringEx(pSE,SSArry);//wtt///5.14
   // sexpt=GetExpStringEx(pTE,MSArry);//wtt///5.14
		sexps=GetExpString(pSE,SSArry);//wtt///5.14
	/////////wtt 2007.5.20/////强制类型转换/////
	/*	if(pSE&&pSE->info<-1&&0-pSE->info<132)
		{
			sexps=C_ALL_KEYWORD[0-pSE->info]+sexps;
			expbeg
			AfxMessageBox("强制类型转换");/////////
		}*/
		//////////////////////////////
	sexpt=GetExpString(pTE,MSArry);//wtt///5.14
//	AfxMessageBox("arithmatch called");
//	AfxMessageBox("Student"+sexps);///////////////
//	AfxMessageBox("model"+sexpt);///////////////////

	/*	if(pTE&&pTE->info<-1&&0-pTE->info<132)
		{
			sexpt=C_ALL_KEYWORD[0-pTE->info]+sexpt;
				AfxMessageBox("强制类型转换");//////////
		}*/

	if(sexps.GetLength()<=0 || sexpt.GetLength()<=0)
	{
		return 0;
	}

	if(sexps==sexpt)
	{
		return BOUND;
	}	
	
	p=pTE;

	if(p==NULL)
	{
		return 0;
	}

	// post order tranverse the syntax tree
	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);
			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2);

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here
				itemp=GetFatherType(p,pTE);
				T.key=itemp;

				if(OperatorKind(T)>=2  && p->pleft==NULL && p->pright==NULL )
				{
					rvalue+=ArithPartMatch(itemp,pSE,p,SSArry,MSArry);
					count++;
				}
							
				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);
	
	if(count<=0) count=10;
	return (int)(rvalue/count);	
	
}


int CProgramMatch::BoolPartMatch(const int ftype, ENODE *pSE,ENODE *pTN,
								 CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Compare the bool expression pTN with expression pSE and return 
		its result
		PARAMETER:
		pSE  : head pointer of expression in student program.
		pTN  : head pointer of expression in template program.
	***********************************************************************/

	//extern CString GetExpString(ENODE *pEHead,bool bNIDX=true);
     if(pSE==NULL||pTN==NULL)
		 return 0;
	TNODE T;
	ENODE *p=NULL;
	int rvalue=0,value=0,itemp;
	CString sexps(""),sexpt("");
	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL;
	
	sexps=GetExpStringEx(pSE,SSArry);
	sexpt=GetExpStringEx(pTN,MSArry);

	if(sexps==sexpt)
	{
		if(ftype<=0)
		{
			return BOUND;
		}
		else
		{
			rvalue=(int)(BOUND/2);
		}		
	}

	p=pSE;

	if(p==NULL)
	{
		return 0;
	}

	// post order tranverse the syntax tree
	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here
				itemp=GetFatherType(p,pSE);
				T.key=itemp;
				sexps=GetExpStringEx(p,SSArry);

				if( (OperatorKind(T)==0 || OperatorKind(p->T)==1) && IsTerminal(p)>=0)
				{
					TNODE TD;
					TD.key=ftype; 
                    
					value=0;
					// relation operators
					if(OperatorKind(T)==1 && OperatorKind(TD)==1 )
					{
						value=ArithExpressionMatch(p->pleft,pTN->pleft,SSArry,MSArry);
						value+=ArithExpressionMatch(p->pright,pTN->pright,SSArry,MSArry);
						value=(int)(value/2);
					}
					// logical operators
					else if(OperatorKind(T)==0 && OperatorKind(TD)==0 && IsTerminal(pTN)>=1)
					{
						value=ArithExpressionMatch(p,pTN,SSArry,MSArry);			
					}


					value=(int)(value/2);
					if(ftype==itemp)
					{
						value+=500;
					}

					// > or >=
					if(abs(ftype-itemp)==3)
					{
						value+=300;
					}
					
				}
				else
				{
					if(sexps.Find(sexpt)>=0 || sexpt.Find(sexps)>=0)
					{
						value=200;			
					}
				}

				if(value>rvalue)
				{
					rvalue=value;
				}
				// end of this part
				break;
			}

		}

	}while(STK.GetCount()>0);

	return rvalue;
}

int CProgramMatch::ArithPartMatch(const int ftype,ENODE *pSE,ENODE *pTN,
								  CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Compare the Arithmethic expression pTN with expression pSE and return 
		its result
		PARAMETER:
		pSE  : head pointer of expression in student program.
		pTN  : head pointer of expression in template program.
	***********************************************************************/

	//extern CString GetExpString(ENODE *pEHead,bool bNIDX=true);
	 if(pSE==NULL||pTN==NULL)
		 return 0;
	TNODE T;
	ENODE *p=NULL;
	int rvalue=0,value=0,itemp;
	CString sexps(""),sexpt("");
	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL;
	
	sexps=GetExpStringEx(pSE,SSArry);
	sexpt=GetExpStringEx(pTN,MSArry);

	if(sexps==sexpt)
	{
		return BOUND;
	}
	
	p=pSE;

	if(p==NULL)
	{
		return 0;
	}
    
    /////////////////////////////////////////////////////////////////// 
	// post order tranverse the syntax tree

	do{
		while(p!=NULL)
		{
			STK.AddTail(p);
			SBL.AddTail(1);
			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 
				p=p->pright;
				iscontinue=false;

				break;
			case 2:
                ///////////////////////////////////////////////////////
				// add action here

				value=0;
				itemp=GetFatherType(p,pSE);
				T.key=itemp;

				sexps=GetExpStringEx(p,SSArry,0);
				sexpt=GetExpStringEx(pTN,MSArry,0);

				// the e-node is terminal node in syntax tree
				if((OperatorKind(T)>=2  && p->pleft==NULL && p->pright==NULL))
				{
					// the two e-node can compare
					if( (p->T.key==CN_VARIABLE && pTN->T.key==CN_VARIABLE) ||
					    (p->T.key!=CN_VARIABLE && pTN->T.key!=CN_VARIABLE) )
					{
						// both are not variables
						if(p->T.key!=CN_VARIABLE && pTN->T.key!=CN_VARIABLE)
						{
						
							if(sexpt==sexps)
							{	
								if(ftype==itemp)
								{
									value=BOUND;
								}
								else
								{
									value=500;
								}
							
							}							
						}
						// both are variables
						else
						{
							// initiate

							if(sexpt==sexps)
							{	
								if(ftype==itemp)
								{
									value=BOUND;
								}
								else
								{
									value=500;
								}							
							}
                            
							int ntemp=0;
							// common variables
							if(p->T.addr>=0&&p->T.addr<SSArry.GetSize()&&pTN->T.addr>=0&&pTN->T.addr<MSArry.GetSize()&&SSArry[p->T.addr].kind==CN_VARIABLE && MSArry[pTN->T.addr].kind==CN_VARIABLE)
							{
								// value of initiating								
							}
							// both are array
							else if(p->T.addr>=0&&p->T.addr<SSArry.GetSize()&&pTN->T.addr>=0&&pTN->T.addr<MSArry.GetSize()&&SSArry[p->T.addr].kind==CN_ARRAY && MSArry[pTN->T.addr].kind==CN_ARRAY)
							{
								if(p->info==pTN->info && p->info>=1)
								{
									for(int i=0;i<10 && pTN->pinfo[i] &&  p->pinfo[i] ;i++)
									{
										ntemp+=ArithExpressionMatch(p->pinfo[i],pTN->pinfo[i],SSArry,MSArry);
									}
									value=(int)(value/2)+(int)(ntemp/(2*p->info));
								}
								else
								{
									value=(int)(value/2);
								}
							}
							// pointer and array
							else if(p->T.addr>=0&&p->T.addr<SSArry.GetSize()&&pTN->T.addr>=0&&pTN->T.addr<MSArry.GetSize()&&SSArry[p->T.addr].kind==CN_POINTER && MSArry[pTN->T.addr].kind==CN_POINTER)
							{
								if(p->info==pTN->info && p->info>=1)
								{
									for(int i=0;i<10 && pTN->pinfo[i] &&  p->pinfo[i] ;i++)
									{
										ntemp+=ArithExpressionMatch(p->pinfo[i],pTN->pinfo[i],SSArry,MSArry);
									}
									value=(int)(value/2)+(int)(ntemp/(2*p->info));
								}
								else if(p->info<pTN->info && p->info>=1)
								{
									for(int i=0;i<10  && pTN->pinfo[i];i++)
									{
										ntemp+=ArithExpressionMatch(p->pinfo[0],pTN->pinfo[i],SSArry,MSArry);
									}

									if(p->info>1)
									{
										int i;
										for(i=0;i<10 && pTN->pinfo[i];i++)
										{
											ntemp+=ArithExpressionMatch(p->pinfo[1],pTN->pinfo[i],SSArry,MSArry);
										}
									}

									value=(int)(value/2)+(int)(ntemp/(2*p->info));
								}
								else
								{
									value=(int)(value/2);
								}
							}
							// array and pointer
							else if(p->T.addr>=0&&p->T.addr<SSArry.GetSize()&&pTN->T.addr>=0&&pTN->T.addr<MSArry.GetSize()&&SSArry[p->T.addr].kind==CN_ARRAY && MSArry[pTN->T.addr].kind==CN_POINTER)
							{
								if(p->info==pTN->info && p->info>=1)
								{
									for(int i=0;i<10 && pTN->pinfo[i] &&  p->pinfo[i] ;i++)
									{
										ntemp+=ArithExpressionMatch(p->pinfo[i],pTN->pinfo[i],SSArry,MSArry);
									}
									value=(int)(value/2)+(int)(ntemp/(2*p->info));
								}
								else if(p->info>pTN->info && pTN->info>=1)
								{
									for(int i=0;i<10 && p->pinfo[i];i++)
									{
										ntemp+=ArithExpressionMatch(p->pinfo[i],pTN->pinfo[0],SSArry,MSArry);
									}

									if(pTN->info>1)
									{
										int i;
										for(i=0;i<10 && p->pinfo[i];i++)
										{
											ntemp+=ArithExpressionMatch(p->pinfo[i],pTN->pinfo[1],SSArry,MSArry);
										}
									}

									value=(int)(value/2)+(int)(ntemp/(2*p->info));
								}
								else
								{
									value=(int)(value/2);
								}
							}
							// both are pointer
							else if(p->T.addr>=0&&p->T.addr<SSArry.GetSize()&&pTN->T.addr>=0&&pTN->T.addr<MSArry.GetSize()&&SSArry[p->T.addr].kind==CN_POINTER && MSArry[pTN->T.addr].kind==CN_ARRAY)
							{
								if(p->info==pTN->info && p->info>=1)
								{
									for(int i=0;i<10 && pTN->pinfo[i] &&  p->pinfo[i] ;i++)
									{
										ntemp+=ArithExpressionMatch(p->pinfo[i],pTN->pinfo[i],SSArry,MSArry);
									}
									value=(int)(value/2)+(int)(ntemp/(2*p->info));
								}
								else
								{
									value=(int)(value/2);
								}
							}

						}

					}
				}
				// the e-node is not terminal node in syntax tree
				else
				{
					if(sexps.Find(sexpt)>=0 || sexpt.Find(sexps)>=0)
					{
						value=100;							
					}
				}

				if(value>rvalue)
				{
					rvalue=value;
					value=0;
				}				
				
				//end of this part
                ///////////////////////////////////////////////////////
				break;
			}

		}

	}while(STK.GetCount()>0);

	return rvalue;
}

int CProgramMatch::GetFatherType(ENODE *pEN,ENODE *pEH)
{
	if(pEN==NULL||pEH==NULL)
		return -1;
	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL;
	ENODE *p=NULL;
	int itemp=pEN->info;
	pEN->info=3*BOUND;

	if(pEH->info>=3*BOUND)
	{
		pEN->info=itemp;
		return -1;
	}

	p=pEH;

	do
	{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;
		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();
    		STK.RemoveTail();			
			SBL.RemoveTail(); 
			switch(lab)
			{
			case 1:
				STK.AddTail(p);
				SBL.AddTail(2); 
				p=p->pright;
				iscontinue=false;
				break;
			case 2:
				// add action here
				if( (p->pleft && p->pleft->info>=3*BOUND) || 
				    (p->pright && p->pright->info>=3*BOUND))
				{
					STK.RemoveAll(); 
					SBL.RemoveAll();
					pEN->info=itemp; 
					return p->T.key;
				}					
				//end of this part
			break;
			}

		}

	}while(STK.GetCount()>0);

	return -1;
	
}

int CProgramMatch::OperatorKind(TNODE T)
{
	int rvalue=-1;
	switch(T.key)
	{
	case CN_DAND:      // && 
	case CN_DOR:       // ||
	case CN_NOT:       // !
		rvalue=0;
		break;
	case CN_GREATT:    // >
	case CN_BANDE:     // >=
	case CN_NOTEQUAL:  // !=
	case CN_DEQUAL:    // == 
		rvalue=1;
		break;
	case CN_ADD:       // +
	case CN_SUB:       // -
		rvalue=2;
		break;
	case CN_MULTI:     // *
	case CN_DIV:       // / 
	case CN_FORMAT:    // %
		rvalue=3;
		break;
	default:
		rvalue=-1;
	}
	return rvalue;
}

int CProgramMatch::IsTerminal(ENODE *pENode)
{
	if(pENode->pleft==NULL && pENode->pright==NULL)
	{
		return 1;
	}

	if(OperatorKind(pENode->T)==1)
	{
		return 0;
	}

	if(OperatorKind(pENode->T)==0)
	{
		return -1;
	}

	return 1;
}

/////////////////////////////////////////////////////////////////////////////////////
//////////////@@@@@@@@@@@@@@@Function group of Branch-Match@@@@@@@@@@@@//////////////
/////////////////////////////////////////////////////////////////////////////////////

int CProgramMatch::BranchesMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry)
{
	/***********************************************************************
		FUNCTION:
		When size match, struct match and completely match are over, search
		and find if there exists the corresponding part of every important 
		student c-program's node in the template c-program, do some Statistics
		and return its degree(1-1000).
		PARAMETER:
		as above.
	***********************************************************************/
	if(pSHead==NULL||pMHead==NULL)
		return 0;
	int ncount=0;
	CSDGBase *p=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	
	p=pSHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here
				if(p->m_sType!="SDGHEAD" && p->m_sType!="SDGENTRY")
				{
					NodeMatch(ncount,p,pMHead,pSHead,SSArry,MSArry);															
				}
				// End of this part

				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);	
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return GetBranchValue(ncount,pSHead);
}

int CProgramMatch::GetBranchValue(int ncount,CSDGBase *pSHead)
{
	/***********************************************************************
		FUNCTION:
		post order tranverse the template c-program's SDG and count it 
		the matching result.
		PARAMETER:
		pMHead :head pointer of template c-program's SDG.
	***********************************************************************/
    if(pSHead==NULL)
		return 0;
	int nvalue=0,nNum=0;
	CSDGBase *p=NULL,*pTemp=NULL;
	CList<CSDGBase *,CSDGBase *> S;
////////////////////////////////////////////
		/*	CString stemp;
			stemp.Format("branch match ncount=%d,nNum=%d",ncount,nNum); 
			AfxMessageBox(stemp);*/
			////////////////////////////////////////////////////
	p=pSHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here
				if(p->m_sType!="SDGHEAD" && p->m_sType!="ESPSUB")
				{
					if(p->GetNodeVL()>=0)
					{
						nvalue+=p->GetNodeVL();						
					}			
				}
				nNum++;
				// End of this part
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

		////////////////////////////////////////////
		/*	CString stemp;
			stemp.Format("branch match ncount=%d,nNum=%d,nvalue=%d",ncount,nNum,nvalue); 
			AfxMessageBox(stemp);*/
			////////////////////////////////////////////////////
    if(ncount<nNum)
	{
		ncount=nNum*3+1;
		nvalue+=1000;
	}

	return (int)(nvalue/ncount);

}

int CProgramMatch::NodeMatch(int &ncount,CSDGBase *pNode,CSDGBase *pMHead,CSDGBase *pSHead,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry)
{
	/***********************************************************************
		FUNCTION:
		Analyze if there exists the corresponding part of one of the important 
		student c-program's node in the template c-program, do some Statistics
		and	return its degree(1-1000).
		PARAMETER:
		pNode : node to match;
		others as above.
	***********************************************************************/
	CSDGBase *p=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	
	ncount=0;
	p=pMHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your Codes here
				if(NodeInfoMatch(pNode,p) && p->m_sType!="SDGHEAD" )
				{
					CompleteMatchNode(p,pNode,pMHead,pSHead,MSArry,SSArry);	
				}
				ncount++;
				// End of this part
                
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return 0;
	
}

bool CProgramMatch::NodeInfoMatch(CSDGBase *pSN,CSDGBase *pTN)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the two nodes are in the same condition.
		PARAMETER:
		pSN : node in student  c program
		pTN : node in template c program
	***********************************************************************/

	if(!pSN || !pTN)
	{
		return false;
	}

	if((pSN->m_sType!=pTN->m_sType) || 
	   (pSN->m_sType=="_ELSE" && pTN->m_sType!="SELECTOR") ||
	   (pTN->m_sType=="_ELSE" && pSN->m_sType!="SELECTOR") )
	{
		return false;
	}

	if(pSN->m_sType=="SDGCALL" && pSN->m_pinfo && pTN->m_pinfo && 
	   pSN->m_pinfo->T.name!=pTN->m_pinfo->T.name)
	{
		return false;	
	}

	int index=0,ix=0;
	CSDGBase *pTemp=NULL,*pFth=NULL;
	CString strSLeft(""),strTLeft(""),strSTop(""),strTTop(""),strTemp("");
	CArray<CString,CString> strArry;
	
	// get father nodes information of s-c-program
	pTemp=pSN;
	while(pTemp && pTemp->m_sType!="SDGHEAD")
	{
		strSTop+=pTemp->m_sType;
		pTemp=pTemp->GetFather(); 
	}

	// get father nodes information of t-c-program
	pTemp=pTN;
	while(pTemp && pTemp->m_sType!="SDGHEAD")
	{
		strTTop+=pTemp->m_sType;
		pTemp=pTemp->GetFather();
	}

	// get left brother nodes information of s-c-program
	ix=0;
	index=FindIndex(pSN);
	pFth=pSN->GetFather();
	while(pFth && ix<index && index>0 && index<pFth->GetRelateNum())
	{
		strTemp="SDGBASE";
		if(pFth->GetNode(ix)&&(pFth->GetNode(ix)->m_sType=="SELECTION" || pFth->GetNode(ix)->m_sType=="ITERATION" || 
		   pFth->GetNode(ix)->m_sType=="ITRSUB"    || pFth->GetNode(ix)->m_sType=="ESPSUB" || 
		   pFth->GetNode(ix)->m_sType=="SELECTOR"  || pFth->GetNode(ix)->m_sType=="_ELSE"||
		   pFth->GetNode(ix)->m_sType=="SDGENTRY"))
		{
			
			strTemp=pFth->GetNode(ix)->m_sType;
		}

		if(pFth->GetNode(ix)&&pFth->GetNode(ix)->m_sType=="SELECTION" && 
		   pFth->GetNode(ix)->GetRelateNum()==1  &&pFth->GetNode(ix)->GetNode(0)&&
		   pFth->GetNode(ix)->GetNode(0)->GetRelateNum()==1 && pFth->GetNode(ix)->GetNode(0)->GetNode(0)&&
		   pFth->GetNode(ix)->GetNode(0)->GetNode(0)->m_sType=="SDGCALL" &&
		   pFth->GetNode(ix)->GetNode(0)->GetNode(0)->m_pinfo &&
		   pFth->GetNode(ix)->GetNode(0)->GetNode(0)->m_pinfo->T.name=="printf")
		{
			strTemp="SDGBASE";
		}
		
		strArry.Add(strTemp);
		
		ix++;
	}

	for(ix=strArry.GetSize()-1;ix>=0;ix--)
	{
		if(ix>0 && strArry[ix-1]=="SDGBASE" && strArry[ix]=="SDGBASE")
		{
			strArry.RemoveAt(ix); 
		}
	}

	for(ix=0;ix<strArry.GetSize();ix++)
	{
		strSLeft+=strArry[ix];
	}

	// get left brother nodes information of t-c-program
	ix=0;
	strArry.RemoveAll(); 
	index=FindIndex(pTN);
	pFth=pTN->GetFather();
	while(pFth && ix<index && index>0 && index<pFth->GetRelateNum())
	{
		strTemp="SDGBASE";
		if(pFth->GetNode(ix)&&(pFth->GetNode(ix)->m_sType=="SELECTION" || pFth->GetNode(ix)->m_sType=="ITERATION" || 
		   pFth->GetNode(ix)->m_sType=="ITRSUB"    || pFth->GetNode(ix)->m_sType=="ESPSUB" || 
		   pFth->GetNode(ix)->m_sType=="SELECTOR"  || pFth->GetNode(ix)->m_sType=="_ELSE"||
		   pFth->GetNode(ix)->m_sType=="SDGENTRY"))
		{
			
			strTemp=pFth->GetNode(ix)->m_sType;
		}

		if(pFth->GetNode(ix)&&pFth->GetNode(ix)->m_sType=="SELECTION" && 
		   pFth->GetNode(ix)->GetRelateNum()==1  &&pFth->GetNode(ix)->GetNode(0)&&
		   pFth->GetNode(ix)->GetNode(0)->GetRelateNum()==1 &&  pFth->GetNode(ix)->GetNode(0)->GetNode(0)&&
		   pFth->GetNode(ix)->GetNode(0)->GetNode(0)->m_sType=="SDGCALL" &&
		   pFth->GetNode(ix)->GetNode(0)->GetNode(0)->m_pinfo &&
		   pFth->GetNode(ix)->GetNode(0)->GetNode(0)->m_pinfo->T.name=="printf")
		{
			strTemp="SDGBASE";
		}
		
		strArry.Add(strTemp);
		
		ix++;
	}

	for(ix=strArry.GetSize()-1;ix>=0;ix--)
	{
		if(ix>0 && strArry[ix-1]=="SDGBASE" && strArry[ix]=="SDGBASE")
		{
			strArry.RemoveAt(ix); 
		}
	}

	for(ix=0;ix<strArry.GetSize();ix++)
	{
		strTLeft+=strArry[ix];
	}

	// end of get information


	if(strSLeft==strTLeft && strSTop==strTTop)
	{
		return true;
	}

	return false;
}


/////////////////////////////////////////////////////////////////////////////////////
//////////////@@@@@@@@@@@@@@@End Definitin of Class CProgramMatch@@@@@@@@@@@@//////////////
/////////////////////////////////////////////////////////////////////////////////////

CString CProgramMatch::GetExpStringEx(ENODE *pEHead,CArray<SNODE,SNODE> &SArry,bool bNIDX)
{
	/***********************************************************************
		FUNCTION:
		return the syntax tree's string
		PARAMETER:
		pEHead : head pointer of tree
		
	***********************************************************************/
	if(pEHead==NULL)
		return "";
	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 
	ENODE *p=pEHead;
	CString str("");
	
    /////////////////////////////
	
	p=pEHead;
   	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			// beg
			ENODE *pE=STK.GetTail();
			if(pE->T.key!=CN_VARIABLE || (pE->T.key==CN_VARIABLE&&pE->T.addr>=0&& pE->T.addr<SArry.GetSize()&& SArry[pE->T.addr].kind!=CN_POINTER ))
			{
				str+=GetENodeStr(STK.GetTail());			
			}
			else if(pE->T.key==CN_VARIABLE&&pE->T.addr>=0&& pE->T.addr<SArry.GetSize()&&SArry[pE->T.addr].kind==CN_POINTER)
			{
				if(pE->T.paddr>=0 && pE->T.paddr<SArry.GetSize())
				{
					str+=SArry[pE->T.paddr].name;
				}
				else
				{
					str+=pE->T.name;
				}
			}


			if(  bNIDX==true &&
			    (pE->T.key==CN_VARIABLE||pE->T.key==CN_BFUNCTION || pE->T.key==CN_DFUNCTION))
			{
				for(int i=0; i<10&&pE->pinfo[i] ;i++)
				{
					str+="["+GetExpStringEx(pE->pinfo[i],SArry,bNIDX)+"]";
				}
			}
			// end

			p=STK.GetTail()->pright;
			STK.RemoveTail();			
		}
	}

	return str;
}

int CProgramMatch::NodeWeight(CSDGBase *pNode)
{
	/***********************************************************************
		FUNCTION:
		Analyze the pNode's weight and return its value(>=1).
		PARAMETER:
		pNode : node pointer of SDG;
		
	***********************************************************************/
    if(pNode==NULL)
	 return 0;
	/////////////////////////////////////////////////////////
	extern bool IsDepended(CSDGBase *pNode1,CSDGBase *pNode2);
	//////////////////////////////////////////////////////////

	int nrvalue=1;
	CArray<CString,CString> strArry;

	CSDGBase *pTemp=pNode->GetFather();

	// redundant code
	while(pTemp && pTemp->m_sType!="SDGHEAD")
	{
		if(pTemp->m_sType=="ITERATION" || pTemp->m_sType=="SELECTION" )
		{
			strArry.InsertAt(0,pTemp->m_sType);
		}
		pTemp=pTemp->GetFather(); 
	}

	if(pNode->m_sType=="SELECTOR" || pNode->m_sType=="_ELSE" || pNode->m_sType=="ITERATION" )
	{
		nrvalue=2;
		for(int i=0;i<strArry.GetSize();i++)
		{
			if(strArry[i]=="ITERATION")
			{
				nrvalue++;			
			}
		}
	}
	else if(pNode->m_sType=="SDGCALL" && pNode->m_pinfo && pNode->m_pinfo->T.name=="printf") 
	{
		nrvalue=1;
	}
	else if(pNode->m_sType=="ITRSUB" || pNode->m_sType=="ESPSUB")
	{
		nrvalue=1;
	}
	else
	{
		for(int i=0;i<strArry.GetSize();i++)
		{
			if(strArry[i]=="ITERATION")
			{
				nrvalue++;
			}
		}
		
		CSDGBase *pfat=pNode->GetFather();
		int i;
		if(pfat && pfat->m_sType!="SDGENTRY" )
		{
			int ix=FindIndex(pNode);
			if(ix>=0 && ix<pfat->GetRelateNum())
			{
				i=ix+1;
				ix=0;

				for(;i<pfat->GetRelateNum();i++)
				{
					if(IsDepended(pNode,pfat->GetNode(i)))
					{
						nrvalue+=4;
					}
					nrvalue++;
				}
			}
		}
	}

	if( pNode->m_sType=="ASSIGNMENT" && pNode->m_pinfo && nrvalue>1&&
	    (pNode->m_pinfo->T.key==CN_CINT || pNode->m_pinfo->T.key==CN_CCHAR) )
	{
		nrvalue--;		
	}

	if(pNode->m_sType=="#DECLARE")
	{
		nrvalue=1;
	}

    //TRACE("\n%s=%d\n",pNode->m_sType,nrvalue);
	return nrvalue;
}

CString CProgramMatch::GetENodeStr(ENODE *pENode)
{
	/***********************************************************************
		FUNCTION:
		return the ENODE's string
		PARAMETER:
		pENode : pointer of pENode;
		
	***********************************************************************/
	
	CString str(""),s("");
	if(pENode==NULL)
	{
		return "";
	}

	switch(pENode->T.key)		
	{							 	
	case CN_CFLOAT:
		s.Format("%f",pENode->T.value);
		str+=s;
		break;
	case CN_CINT:
	case CN_CCHAR:
		s.Format("%d",pENode->T.addr);
		str+=s;
		break;
	case CN_ADD:
		str+="+ ";
		break;
	case CN_SUB:
		str+="-";
		break;
	case CN_MULTI:
		str+="*";
		break;
	case CN_DIV:
		str+="/";
		break;
	case CN_FORMAT:
		str+="%";
		break;
	case CN_GREATT:       // >
		str+=">";
		break;
	case CN_LESS:         // <
		str+="<";
		break;
	case CN_LANDE:        // <= 
		str+="<=";
		break;
	case CN_BANDE:        // >=
		str+=">=";
		break;
	case CN_NOTEQUAL:     // !=
		str+="!=";
		break;
	case CN_DEQUAL:       // == 
		str+="==";
		break;
	case CN_DAND:
		str+="&&";
		break;
	case CN_DOR:
		str+="||";
		break;
	case CN_NOT:
		str+="!";
		break;
	case CN_DFUNCTION:
	case CN_BFUNCTION:
	case CN_CSTRING:
		s.Format("%s",pENode->T.name); 
		str+=s;
		break;
	case CN_VARIABLE:
        s.Format("%s",pENode->T.name); 	
		str+=s;
	default:
		str+=' ';
	}	

	return str;
}

void CProgramMatch::ResetSValue(CSDGBase *pSHead)
{
	/***********************************************************************
		FUNCTION:
		reset the student c-program SDG node's value to "0". 
		PARAMETER:
		pSHead : student c-program SDG head
	***********************************************************************/
    if(pSHead==NULL)
		return;
	CSDGBase *p=NULL;
	CList<CSDGBase *,CSDGBase *> S;
	
	p=pSHead;
	while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);		
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;

				// Add your Codes here
				if(p->GetNodeVL()!=0)
				{
					p->SetNodeVL(0); 
				}
				// End of this part

				p=NULL;
			}
		}
    }
}

////////////////////////////////////////////////////////////////////
/////////////////////////////////2004.04.20.13.09///////////////////
////////////////////////////////////////////////////////////////////
