// ScintillaWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ScintillaWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScintillaWnd

CScintillaWnd::CScintillaWnd()
{
}

CScintillaWnd::~CScintillaWnd()
{
}


BEGIN_MESSAGE_MAP(CScintillaWnd, CWnd)
	//{{AFX_MSG_MAP(CScintillaWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CScintillaWnd message handlers
BOOL CScintillaWnd::Create(DWORD dwExStyle, DWORD dwStyle,const RECT& rect, CWnd* pParentWnd, UINT nID)
{   
    // TODO: 在此添加专用代码和/或调用基类
    return CWnd::CreateEx(dwExStyle,_T("Scintilla"),_T(""),dwStyle,rect,pParentWnd,nID);
}

/////////////////////////////////////
// @mfunc Reset the Scintiall control and add new Text
// @rvalue void | not used
//
void CScintillaWnd::SetText (
                             LPCSTR szText) //@parm pointer to new text
{
   LRESULT lResult = 0;
   if (szText != NULL)
	   lResult = SendMessage(SCI_SETTEXT,0,(LPARAM)szText);
   GotoPosition(0);
   SetFocus();
}
/////////////////////////////////////
// @mfunc Get the text from the control
// @rvalue void | not used
//
void CScintillaWnd::GetText (
                             CString &strText) //@parm handle to receive text
{
   LPSTR szText = GetText();
   if (szText != NULL)
   {
      strText = szText;
      delete [] szText;
   }
}
/////////////////////////////////////
// @mfunc Get the text from the control
// @rvalue LPSTR | a character string with text from the control - NULL on error - the caller has to free pointer
//
LPSTR CScintillaWnd::GetText ()
{
   long lLen = SendMessage(SCI_GETLENGTH, 0, 0) + 1;
   if (lLen > 0)
   {
      char *pReturn = new char[lLen];
      if (pReturn != NULL)
      {
         *pReturn = '\0';
         SendMessage(SCI_GETTEXT, lLen, (long)pReturn);
         return pReturn;
      }
   }
   return NULL;
}

/////////////////////////////////////
// @mfunc Update UI and do brace matching
// @rvalue void | not used
//
void CScintillaWnd::UpdateUI()
{
// do brace matching
   long lStart = SendMessage(SCI_GETCURRENTPOS, 0, 0);
   long lEnd = SendMessage(SCI_BRACEMATCH, lStart-1, 0);
// if there is a matching brace highlight it
   if (lEnd >= 0)
      SendMessage(SCI_BRACEHIGHLIGHT, lStart-1, lEnd);
// if there is NO matching brace erase old highlight
   else
      SendMessage(SCI_BRACEHIGHLIGHT, -1, -1);
}

/////////////////////////////////////
// @mfunc Goto givven character position
// @rvalue void | not used
//
void CScintillaWnd::GotoPosition(
                                 long lPos) //@parm new character position
{
	SendMessage(SCI_GOTOPOS, lPos, 0);
}
/////////////////////////////////////
// @mfunc Goto givven line position
// @rvalue void | not used
//
void CScintillaWnd::GotoLine(
                             long lLine) //@parm new line - lines start at 1
{
	SendMessage(SCI_GOTOLINE, lLine-1, 0);
}

// 保存文件
BOOL CScintillaWnd::Save(LPCTSTR lpszPathName)
{
	CString txt;
	GetText(txt);
	
	CFile of(lpszPathName,CFile::modeWrite | CFile::modeCreate);
	of.Write(txt.GetBuffer(txt.GetLength()),txt.GetLength());
	of.Close();
	
	return TRUE;
}

//加载文件
BOOL CScintillaWnd::Load(LPCTSTR lpszPathName)
{
	CFile fi(lpszPathName,CFile::modeRead);
	if (fi.GetLength()==0) return TRUE;
	TCHAR *txt = new TCHAR[fi.GetLength()+1];
	txt[fi.GetLength()]=0;
	fi.Read(txt,fi.GetLength());
	fi.Close();
	SetText(txt);
	delete [] txt;
	return TRUE;
}

int CScintillaWnd::GetLength()
{
	return SendMessage(SCI_GETLENGTH, 0, 0);
}