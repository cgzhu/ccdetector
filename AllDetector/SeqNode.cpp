// SeqNode.cpp: implementation of the SeqNode class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SeqNode.h"
#include "ConstData.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Seq_NODE::Seq_NODE()
{
	B_index = -1;
	beg = -1;			
	end = -1;			
	cfilepath = _T("");	
	qsize = -1;			
	Sequence = _T("");	
	Line_Pos = _T(""); 
	relaLPOS = _T("");
//	str_tokenIndex = _T(""); 
	str_srcLine = _T("");
	funcRang.srcBegLine = -1;
	funcRang.srcEndLine = -1;

	funcBgnRealLine = -1;
	funcEndRealLine = -1;
	tokenString = _T("");
}

Seq_NODE::~Seq_NODE()
{
	B_index = -1;
	beg = -1;			
	end = -1;			
	cfilepath = _T("");	
	qsize = -1;			
	Sequence = _T("");	
	Line_Pos = _T(""); 
	relaLPOS = _T("");
//	str_tokenIndex = _T(""); 
	str_srcLine = _T("");
	funcRang.srcBegLine = -1;
	funcRang.srcEndLine = -1;

	funcBgnRealLine = -1;
	funcEndRealLine = -1;
	tokenString = _T("");
}

void Seq_NODE::AddFuncRang(const FUNC_RANG &v_funcRang)
{
	funcRang.srcBegLine = v_funcRang.srcBegLine;
	funcRang.srcEndLine = v_funcRang.srcEndLine;
}

