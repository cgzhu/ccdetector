// CP_Segment.h: interface for the CP_Segment class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CP_SEGMENT_H__FC9FA558_2687_4377_B3D1_60F62628DE53__INCLUDED_)
#define AFX_CP_SEGMENT_H__FC9FA558_2687_4377_B3D1_60F62628DE53__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"
#include "Latt_Node.h"
#include "Seq_Mining.h"
#include "HeaderFileElem.h"

class CP_Segment  
{//记录重复代码信息的类
	friend class IdentifierInconsistence;
public:
	CString seq;//对应的闭合频繁子序列
	int len;//闭合频繁子序列的项数，即重复代码的行数
	int sup;//闭合频繁子序列的支持度，及重复代码出现的次数
	bool Is_Expanded;//标志位，记录重复代码是否可以扩展
//	int expandedTimes;
	CArray <class CP_SEG, class CP_SEG> CP_Array;//重复代码出现的位置
public:
	CP_Segment();
	CP_Segment(class CP_Segment &R_CPS);
	CP_Segment& operator=(CP_Segment &R_CPS);
	virtual ~CP_Segment();
	bool Init_CPNode(class Seq_Mining &Rsm, int i, int j);
	void UpdateIdsLocArrayOfCpSegs(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry);//wq-20090223

	//取出类Rsm中候选集元素LS[i][j]指向的节点信息，存入类CP_Segment里//若该节点所存序列的支持度大于等于2，则返回true，否则返回false

//	void FormIdsPlaceSet(CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G);//wq-20081122-生成代码片段的所有标识符的位置信息集合表
//	void CpSegIdsMappingSet(CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G);//wq-20081123-根据标识符的位置信息集合表生成标识符映射表集合
private:
//	void FormIdsMappingSet( int srcCpSegIndex, int mapCpSegIndex, int mapCpSegNum,
//						   CArray<Seq_NODE,Seq_NODE> &Seq_Arry_G); //wq-20081123
//	void IniIdMappingSet(int memNum, CArray<IdMapping,IdMapping>& IdMapSet);//wq-20081123
//	void FindMappedIdName(const IDENTF_LOCATION& idLoc, 
//						   const CArray<IdsPlaceSetMem,IdsPlaceSetMem>& dstIdsPlaceSet,
//						  CString& mappedIdName);//wq-20081123
public:	
//	void OutputIdMappingSet(int cpArrayNum);//wq-20081123
//	void OutputIdsPlaceSet(int cpArrayNum);//wq-20081123

private:
	bool Is_CPSeg(CString Seq, CString DB_Seq, CString Line_Pos, CString relaLPOS, CP_SEG &CN);//wq20080926///添加了参数CString strTknIndex, CString strTknIndex
	//通过对比子序列Seq和原数据库序列DB_Seq，判断子序列代表的程序片段是否满足连续性的要求。若满足，则将其信息存于CN中，并返回true
	void Init_CP_Array(CArray <Seq_NODE, Seq_NODE> &Seq_Arry_G, CArray<Ds_NODE,Ds_NODE> &Ds_Arry ,CString Seq);
	//遍历数组Ds_Arry，若某程序碎片的连续性满足要求，则将其信息存入类CP_Segment的CP_Array中
};

#endif // !defined(AFX_CP_SEGMENT_H__FC9FA558_2687_4377_B3D1_60F62628DE53__INCLUDED_)
