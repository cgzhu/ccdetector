// CloneTreeCtrl.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "CloneTreeCtrl.h"
#include "MainFrm.h"
#include "CloneCodeView.h"

// CCloneTreeCtrl

IMPLEMENT_DYNAMIC(CCloneTreeCtrl, CTreeCtrl)

CCloneTreeCtrl::CCloneTreeCtrl()
{

}

CCloneTreeCtrl::~CCloneTreeCtrl()
{
}


BEGIN_MESSAGE_MAP(CCloneTreeCtrl, CTreeCtrl)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CCloneTreeCtrl::OnNMDblclk)
END_MESSAGE_MAP()



// CCloneTreeCtrl 消息处理程序

/*
 * FillTree函数功能：构建文件树
 */
void CCloneTreeCtrl::FillTree(Out_CpPos *Ocp, CImageList &imgList, HTREEITEM &m_hCTree)
{
	HTREEITEM subhti;
    int i;
    CString str_i;

	CArray <CP_Segment, CP_Segment> &CP_Seg_Array = Ocp->CP_Seg_Array;
	int cpSize = CP_Seg_Array.GetSize();	//cpSize的值表示有多少段重复代码

	for( i = 0; i < cpSize; i++ )	//对于每一段重复代码
	{
		if(CP_Seg_Array[i].len >= Min_len)//重复代码的行数大于最小检测行数阈值
		{
			str_i.Format("%d",i+1);
			subhti=InsertItem(TREE_ITEMS[1] + str_i ,0,1,m_hCTree);

			int j;
			CString str_j;
			int fileNum = CP_Seg_Array[i].CP_Array.GetSize();
			for( j = 0; j < fileNum; j++ )
			{
				str_j.Format("%d",j+1);
				InsertItem(TREE_ITEMS[2] + str_j ,2,2,subhti);
			}
		}
	}
}

/*
 * 函数功能：删除子节点
 */
void CCloneTreeCtrl::DeleteChildItems(HTREEITEM &m_hCTree)
{
	//MessageBox("tt");
	HTREEITEM subhti,child_subhti;
	while(ItemHasChildren(m_hCTree))
	{
		//MessageBox("tt");
		subhti = GetChildItem(m_hCTree);
		while(ItemHasChildren(subhti))
		{
			child_subhti = GetChildItem(subhti);
			DeleteItem(child_subhti);
		}
		DeleteItem(subhti);

	}

}

/*
 * 函数功能：返回（加了标记后的）克隆代码段所在文件的源代码
 */
CString CCloneTreeCtrl::GetSourceCode(CString filePath, CString linePos)
{
	CStdioFile codeFile;
	if( !codeFile.Open(filePath, CFile::modeRead) )		//打开源文件失败，则直接返回
	{
		return _T("");
	}
	if( linePos.GetLength() <= 0 )		//无克隆代码段，则直接返回
	{
		return _T("");
	}

	int lineNum = 1;
	CString srcLine(""),viewLine(""),viewText("");	//srcLine是一行源代码的文本，viewLine是加了标记之后的一行的文本
	CString str_LPOS = linePos;//.Mid(0,linePos.GetLength()-8);

	while(codeFile.ReadString(srcLine))//逐行读取源文件
	{
		int cpLine = -1;
		if(Find_Code_Line(lineNum,cpLine,str_LPOS))//如果该行是克隆行，此时cpLine已经被修改成该行在这个文件的所有克隆行中的顺序
		{
			viewLine = FormViewLine(srcLine,lineNum,cpLine);//以源代码行作基础，生成带标记的显示行
		}
		else		//如果该行是普通行，传入-1
		{
			viewLine = FormViewLine(srcLine,lineNum,-1);//以源代码行作基础，生成带标记的显示行
		}
		viewText += viewLine;	//将新生成的显示行添加到viewText末尾
		lineNum++;				//继续处理下一行
	}
	return viewText;			//返回最终的viewText
}

/*
 * 函数功能：判断一行代码是否是克隆行
 */
bool CCloneTreeCtrl::Find_Code_Line(int curpos, int &cpline, CString &linePos)
{
	CString astrPos = _T("");
	int pos;
	int k,m;
    for( k = 0 , m = 1; k < linePos.GetLength(); k += 4 , m++)
	{
		astrPos = linePos.Mid(k,4);	//从左边第k个字符开始, 获取连续的4个字符, 存入astrPos，此时astrPos保存的是4位的克隆行号字符串
		pos = atoi(astrPos);		//把astrPos转成整数，存入pos
		if(curpos == pos)			//如果当前处理的行就是一个克隆行
		{
			cpline = m;				//cpline表示当前行是克隆行的第几行
			return true;			//返回true，表示这行代码是克隆行
		}
	}
	return false;		//否则，返回false，表示这行不是一个克隆行

}

/*
 * 函数功能：在显示出的每一行的前面添加行号和其它标记，并返回
 */
CString CCloneTreeCtrl::FormViewLine(CString strLine, int lineNum, int cpLine)//strLine是一行源代码的文本，lineNum是这行代码在源文件中的行号，cpline是克隆行序数或-1
{
	CString viewLine("");
	CString str_lineNum,str_cpLine;
	str_lineNum.Format("%d",lineNum);	//str_lineNum是行号字符串

	while( str_lineNum.GetLength() < 5 )	//如果行号字符串长度小于5，则用空格补齐
	{
		str_lineNum += " ";
	}
	viewLine+=str_lineNum;			//每个显示行的最前面都是这5位行号


	if( cpLine > 0 )//如果是克隆行
	{
		str_cpLine.Format( "%d",cpLine );	//str_cpLine是克隆序数字符串
		while( str_cpLine.GetLength() < 5 )	//如果克隆序数字符串长度小于5，则用空格补齐
		{
			str_cpLine += " ";
		}
		str_cpLine = "-CP" + str_cpLine + "- ";		//在之前的5位行号后面添加  -CP克隆序数-
		viewLine += str_cpLine;
	}
	else	//如果是普通行 
	{
		while( viewLine.GetLength() < 5+5+7 )	//在普通行的5位行号后面补空格，使对齐
		{
			viewLine += " ";
		}
	}
	viewLine += strLine + "\r\n";		//在行尾添加换行符
	return viewLine;					//返回生成的显示行
}

/*
 * 函数功能：克隆代码树的子节点的双击事件响应函数
 */
void CCloneTreeCtrl::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	//MessageBox("aaaaa");

	CString str_itemText(""),str_itemParentText("");
	str_itemText = GetItemText(GetSelectedItem());
	str_itemParentText = GetItemText(GetParentItem(GetSelectedItem()));

	int cpGroup = -1;
	if( str_itemParentText.GetLength() > TREE_ITEMS[1].GetLength() )
	{
		cpGroup	= atoi(str_itemParentText.Mid(TREE_ITEMS[1].GetLength(),str_itemParentText.GetLength())) - 1;
	}

	int cpSeg = -1;
	if( str_itemText.GetLength() > TREE_ITEMS[2].GetLength() )
	{
		cpSeg = atoi(str_itemText.Mid(TREE_ITEMS[2].GetLength(),str_itemText.GetLength())) - 1;
	}

	//CMainFrame *pFrame = (CMainFrame *)GetParent()->GetParent();
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();

	//MessageBox("aaaaa");
	//CString str;  
	//str.Format( "%d", pFrame->testInt);  
	//AfxMessageBox(str);
	if( cpGroup > -1 && cpSeg > -1 
		&& cpGroup < pFrame->Ocp.CP_Seg_Array.GetSize()
		&& cpSeg <pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array.GetSize()
		&& pFrame->Ocp.CP_Seg_Array[cpGroup].len >= Min_len)
	{
		//MessageBox("aaaa");
		CString filePath = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].filename;		//当前克隆片段所在的文件目录
		CString linePos = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].LPOS;			//当前克隆片段的绝对位置，比如第一个文件的第29~31行是克隆片段，linePos的值就是002900300031
		CString relaPos = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].relaLPOS;		//当前克隆片段的相对位置

		int funcBgn = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].funcBegLine;		//记录函数范围的起始行号
		int funcEnd = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].funcEndLine;		//记录函数范围的终止行号

		CString viewText(""),funcViewText("");

		viewText += "filePath:" + filePath + "\r\n" + "LPOS:" + linePos + "\r\n\r\n";
		funcViewText = viewText;
		viewText += GetSourceCode(filePath,linePos);
		funcViewText += pFrame->filesToTokens.GetFuncSourceCode(filePath,relaPos,linePos,funcBgn,funcEnd,pFrame->headerFilesArry);
		
		CDocument* pDoc=theApp.pEditTemplate->OpenDocumentFile(NULL);
		pDoc->SetTitle(str_itemText + "源文件");
		CCloneCodeView* pTestView; 
		POSITION pos = pDoc->GetFirstViewPosition();
		pTestView = (CCloneCodeView*)pDoc->GetNextView(pos);
		pTestView->GetEditCtrl().SetWindowText(viewText);		//源文件窗口的初始文本设为viewText

		CDocument* pDoc1=theApp.pEditTemplate->OpenDocumentFile(NULL);
		pDoc1->SetTitle(str_itemText + "函数范围");
		CCloneCodeView* pTestView1; 
		POSITION pos1 = pDoc1->GetFirstViewPosition();
		pTestView1 = (CCloneCodeView*)pDoc1->GetNextView(pos1);
		pTestView1->GetEditCtrl().SetWindowText(funcViewText);	//函数范围窗口的初始文本设为funcViewText
	}


	*pResult = 0;
}
