// SDGSwitch.h: interface for the CSDGSwitch class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGSWITCH_H__BAFE0B0B_D180_4B56_AF03_C63E6F84F8BF__INCLUDED_)
#define AFX_SDGSWITCH_H__BAFE0B0B_D180_4B56_AF03_C63E6F84F8BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CSDGSwitch : public CSDGBase  
{
public:
	CSDGSwitch();
	virtual ~CSDGSwitch();
public:
	int expbeg;
	int expend;
	int SPOS;
    bool HVDefault;
	int DPOS;
	CArray<int,int> CASEPOS;
	CArray<int,int> CBEGPOS;
	CArray<int,int> CASEVL;

};

#endif // !defined(AFX_SDGSWITCH_H__BAFE0B0B_D180_4B56_AF03_C63E6F84F8BF__INCLUDED_)
