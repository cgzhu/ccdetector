#pragma once

/*
 * 这个类是一个显示冗余代码信息的列表停靠窗口
 */
// CRCListWnd
#include "RCListCtrl.h"
#include <vector>
using namespace std;


class CRCListWnd : public CDockablePane
{
	DECLARE_DYNAMIC(CRCListWnd)

public:
	CRCListWnd();
	virtual ~CRCListWnd();

	//
	CRCListCtrl RCList;

	void FillRCList(vector<vector<CString>>);
	void AdjustLayout();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


