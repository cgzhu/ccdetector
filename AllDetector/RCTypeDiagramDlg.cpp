// RCTypeDiagramDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "RCTypeDiagramDlg.h"
#include "afxdialogex.h"
#include"CSeries.h"

// CRCTypeDiagramDlg 对话框

IMPLEMENT_DYNAMIC(CRCTypeDiagramDlg, CDialogEx)

CRCTypeDiagramDlg::CRCTypeDiagramDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRCTypeDiagramDlg::IDD, pParent)
	, m_RC_type_chart()
{

}

CRCTypeDiagramDlg::~CRCTypeDiagramDlg()
{
}

void CRCTypeDiagramDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RC_TYPE_TCHART, m_RC_type_chart);
}

//清除所有图线
void CRCTypeDiagramDlg::ClearAllSeries() 
 {
     for(long i = 0;i<m_RC_type_chart.get_SeriesCount();i++)
     {
         ((CSeries)m_RC_type_chart.Series(i)).Clear();
     }
 }


BEGIN_MESSAGE_MAP(CRCTypeDiagramDlg, CDialogEx)
END_MESSAGE_MAP()


// CRCTypeDiagramDlg 消息处理程序
