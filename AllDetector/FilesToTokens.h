// FilesToTokens.h: interface for the FilesToTokens class.
// 作者：王倩
// 时间：2008-12-22
// 功能：将所有输入的C源文件转换成token流存储到tokenFileArray中
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILESTOTOKENS_H__E9F50701_E710_45E7_97F9_D56545CA8A20__INCLUDED_)
#define AFX_FILESTOTOKENS_H__E9F50701_E710_45E7_97F9_D56545CA8A20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TokenedFile.h"
#include "HeaderFileElem.h"
//wq-20090305-与标识符映射结合的函数源码定位结构
struct FUNCCODE_IDMAP
{
	CString codeSrcLine;//某标识符所在的克隆代码行的源码
	int cpLine;//记录重复代码在显示时的行数位置，是重复代码行则>0,非重复代码行则=0
	int rline;//wq-20090512-记录重复代码行在源代码中的相对位置,非重复代码行=-1

};
/**************************************************/

class FilesToTokens  
{
	friend class CP_SEG; 
public:
	FilesToTokens();
	virtual ~FilesToTokens();
	FilesToTokens(const FilesToTokens& FTT);
	FilesToTokens& operator=(const FilesToTokens& FTT);
	void operator()(CString filePath);
	int FindFileName(const long& hashedFileName, const CString& fileName);
	void ChangTArryToTokenLines( CArray<TNODE,TNODE>& TArry,
								 CArray<SNODE,SNODE>& SArry,
								 CArray<FNODE,FNODE>& FArry,
								 CString cFileName);
	CString GetFuncSourceCode(CString fname,CString relaLPOS,CString LPOS, int funcBgn, int funcEnd,
							  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry//wq-20090522
							  );
	void GetFuncCodeLines(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
						  CArray<FUNCCODE_IDMAP,FUNCCODE_IDMAP> &funcCodeLinesArry,
						  CString fname, CString relaLPOS, CString LPOS, 
						  int funcBgn, int funcEnd);

private:
	void ReadAllFiles(CString filePath);
	void ChangeFileToToken(const CString cFileName);
	int ReadCfile(const CString cFileName,CString &program);
	long HashedFileName(const CString filename);
	void TArryToTokenList( CArray<TNODE,TNODE>& TArry,CArray<TokenLine,TokenLine>& tokenList);
	int FindEqualHashedValue(int begin, int end,const long& hashedFileName);


//private:
public:
	bool ReadCFile(const CString cfilename, CString &program);
	void Clean();
	void TokensOutput();

	//输入的所有C源文件的token行存储表示的集合
	CArray<TokenedFile,TokenedFile> tokenFileArray;
	//输入源文件的路径
	CString Obj_file_path;//wq-20090414
	//结果输出路径
	CString out_file_path;//wq-20090518
};

#endif // !defined(AFX_FILESTOTOKENS_H__E9F50701_E710_45E7_97F9_D56545CA8A20__INCLUDED_)
