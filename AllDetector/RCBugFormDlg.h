#pragma once
#include "afxcmn.h"


// CRCBugFormDlg 对话框

class CRCBugFormDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CRCBugFormDlg)

public:
	CRCBugFormDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRCBugFormDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_rc_bug_list;
};
