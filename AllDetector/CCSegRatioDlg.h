#pragma once
#include "rc_type_tchart.h"


// CCCSegRatioDlg 对话框

class CCCSegRatioDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCCSegRatioDlg)

public:
	CCCSegRatioDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCCSegRatioDlg();

// 对话框数据
	enum { IDD = IDD_CC_SEG_RATIO_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart m_cc_seg_file_ratio_dlg;
	void ClearAllSeries();
};
