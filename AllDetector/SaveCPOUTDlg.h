#if !defined(AFX_SAVECPOUTDLG_H__E49713E0_EB06_4BD5_AEC0_77A315D98AFE__INCLUDED_)
#define AFX_SAVECPOUTDLG_H__E49713E0_EB06_4BD5_AEC0_77A315D98AFE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveCPOUTDlg.h : header file
//
#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// SaveCPOUTDlg dialog

class SaveCPOUTDlg : public CDialog
{
// Construction
public:
	SaveCPOUTDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SaveCPOUTDlg)
	enum { IDD = IDD_DIALOG_SAVECPOUT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SaveCPOUTDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SaveCPOUTDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVECPOUTDLG_H__E49713E0_EB06_4BD5_AEC0_77A315D98AFE__INCLUDED_)
