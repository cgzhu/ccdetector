// FileOutRootDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ALLDetector.h"
#include "FileOutRootDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileOutRootDlg dialog


CFileOutRootDlg::CFileOutRootDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileOutRootDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileOutRootDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFileOutRootDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileOutRootDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFileOutRootDlg, CDialog)
	//{{AFX_MSG_MAP(CFileOutRootDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileOutRootDlg message handlers

void CFileOutRootDlg::OnOK() 
{
	// TODO: Add extra validation here
	CMainFrame *pFrame = (CMainFrame *)GetParent();//->GetParent();
	pFrame->fileOutRootChange = TRUE;
	CDialog::OnOK();
//	CDialog::DestroyWindow();
}

void CFileOutRootDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CMainFrame *pFrame = (CMainFrame *)GetParent();//->GetParent();
	pFrame->fileOutRootChange = FALSE;
	
	CDialog::OnCancel();
}
