// CCLinesFileDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "CCLinesFileDlg.h"
#include "afxdialogex.h"
#include "CSeries.h"

// CCCLinesFileDlg 对话框

IMPLEMENT_DYNAMIC(CCCLinesFileDlg, CDialogEx)

CCCLinesFileDlg::CCCLinesFileDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCCLinesFileDlg::IDD, pParent)
	, m_cc_lines_file_chart()
{

}

CCCLinesFileDlg::~CCCLinesFileDlg()
{
}

void CCCLinesFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TCHART1, m_cc_lines_file_chart);
}

//清除所有图线
void CCCLinesFileDlg::ClearAllSeries()
 {
     for(long i = 0;i<m_cc_lines_file_chart.get_SeriesCount();i++)
     {
         ((CSeries)m_cc_lines_file_chart.Series(i)).Clear();
     }
 }

BEGIN_MESSAGE_MAP(CCCLinesFileDlg, CDialogEx)
END_MESSAGE_MAP()


// CCCLinesFileDlg 消息处理程序
