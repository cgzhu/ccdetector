
// AllDetector.h : main header file for the AllDetector application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CAllDetectorApp:
// See AllDetector.cpp for the implementation of this class
//

class CAllDetectorApp : public CWinAppEx
{
public:
	CAllDetectorApp();
	//Polaris-20140716
	CMultiDocTemplate* pEditTemplate;
	CMultiDocTemplate* pSciTemplate;

private:
	HMODULE m_hDll;//ʹ��SciLexer.dll

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();

	//Polaris-20140714
	afx_msg  void OnFileNew();

	DECLARE_MESSAGE_MAP()
};

extern CAllDetectorApp theApp;
