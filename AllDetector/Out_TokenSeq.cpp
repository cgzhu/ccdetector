// Out_TokenSeq.cpp: implementation of the Out_TokenSeq class.
//
//////////////////////////////////////////////////////////////////////
/*
 * 这个类记录了Token序列以及数字序列信息
 */

#include "stdafx.h"
#include "Lexical.h"

#include "direct.h" 
#include "io.h"
#include "Out_TokenSeq.h"
#include "FormatTokenLines.h"
#include "FuncBndBuild.h"
#include <iostream>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Out_TokenSeq::Out_TokenSeq()
{
	Line_Sum=0;
	Cfile_Sum=0;//初始化
	Obj_file_path = "";//wq-20090319
	out_file_path = "";//wq-20090402
}

Out_TokenSeq::~Out_TokenSeq()
{

}

void Out_TokenSeq::CFILE_TO_TFILE(FilesToTokens &filesToTokens,//wq-20090319
								  const CString readpath, CArray <Seq_NODE,Seq_NODE> &Seq_Arry_G,
								  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
								  CArray<CH_FILE_INFO,CH_FILE_INFO> &filesReadInfo//wq-20090610
								  )
//读取目录readpath下的所有C文件，通过调用OUTPUT_SEQ函数输出对应的token文件
{
//	OutputMemoryRecord("Out_TokenSeq","CFILE_TO_FILE",out_file_path,1,"");
	CFileFind finder;
	CString curpath=readpath;
	_chdir(readpath);//当前工作目录置成readpath
	
	int cfile_sum=0;
	BOOL bWorking=finder.FindFile("*.*");
	while (bWorking)
	{
		bWorking=finder.FindNextFile();
		CString curname=finder.GetFileName();
		int curname_size=curname.GetLength();
		if(finder.IsDirectory())
		{
			if( curname.GetAt(0)!='.')
			{
				curpath=readpath+"\\"+curname;
				CFILE_TO_TFILE(filesToTokens,////wq-20090319
					curpath, Seq_Arry_G,headerFilesArry,filesReadInfo);//递归，读取目录curpath下的所有C文件
			}
			curpath=readpath;
		}
		else if(curname.GetAt(curname_size-2)=='.' && 
			    ( curname.GetAt(curname_size-1)=='c'|| curname.GetAt(curname_size-1)=='C') )//wq-20090213-加入.C后缀的识别
		{//找到后缀名为.c或.C的文件
			CString strFilePath = finder.GetFilePath();
			cout<<"READ: \""<<LPCTSTR(strFilePath)<<"\"..."<<endl;
			Cfile_Sum++;
			OUTPUT_SEQ(filesToTokens,//wq-20090319
				finder.GetFilePath(), Seq_Arry_G,headerFilesArry,filesReadInfo);
		}
	}
//	OutSeqToken(Seq_Arry_G);//wq-20081114-输出序列项和对应的每行源程序（由token中得到），测试存储的token信息是否正确
	finder.Close();
}

BOOL Out_TokenSeq::READ_CHFILE(const CString cfilename,CString &program)
{
//	OutputMemoryRecord("Out_TokenSeq","READ_CHFILE",out_file_path,1,cfilename);
	//  将名为"cfilename"的程序读入program
	CStdioFile file;
	CString str=_T("");
	
	if( file.Open(cfilename,CFile::modeRead|CFile::typeText) == 0 )
	{
	   //str="读文件"+cfilename+"失败!\n\n"+"请检查是否已创建该文件！";
	   //AfxMessageBox(str);
	   return 0;
	}
	else
	{  
		str=_T("");
	    program=_T("");
		while(file.ReadString(str))
		{
			program=program+str+"\n";
			str=_T("");
		}
		file.Close();			//AfxMessageBox(program);//ww right
		return 1;
	}
}


/////////////////////////////////实现分块功能的所有函数////////////////////////////////////
void Out_TokenSeq::Div_Block(CArray <TNODE,TNODE> &TArry,CArray <FL_NODE,FL_NODE> &FL_Arry)
{
	Mark_Flower(0,TArry.GetSize()-1,1,TArry,FL_Arry);//在FL_Arry中记录花括号的位置,作为以后分块的参考
	for(int i=0; i<FL_Arry.GetSize(); i++)
	{
		Add_BlockHead(i,FL_Arry,TArry);//加入块头条件行
		Add_Semicolon(i,FL_Arry,TArry);//加入块尾分号所在行
		                               //王倩注释-20080725-"};"结构体定义或数组定义时赋值的情况
	}
	Reset_Block(FL_Arry,TArry);	//将各模块间的同层散语句合并成块
	Deal_Flower(FL_Arry,TArry);		//合并同层细小模块	
	Combin_Block(FL_Arry,TArry);
	//for(i=0; i<FL_Arry.GetSize(); i++)
	//{
	//	cout<<FL_Arry[i].beg<<" "<<FL_Arry[i].end<<" "<<FL_Arry[i].layer<<" "<<FL_Arry[i].Blength<<endl;
	//}
}

void Out_TokenSeq::Mark_Flower(int Bbeg,int Bend,const int Flower_layer,CArray <TNODE,TNODE> &TArry,CArray <FL_NODE,FL_NODE> &FL_Arry)
{
	int F_layer=Flower_layer;
	int Blength=0;
	FL_NODE FLN;
	int initflower_flag=0;//用于初始化数组的'{'的个数减去'}'的个数
	int arrayFlag = 0;
	for(int i=Bbeg; i<=Bend; i++)
	{
		if(TArry[i].key==CN_LFLOWER) //发现'{'
		{
			if( ( i>0 && TArry[i-1].key==CN_EQUAL) || initflower_flag!=0)  //发现初始化数组时使用的'{'
			{//wq-20081224-添加条件判断:i>0
				initflower_flag++;
			}
			else
			{
				F_layer++;
				if(F_layer==Flower_layer+1)
				{
					FLN.beg=i;
					Blength=0;
				}
			}
		}
		if(TArry[i].key==CN_RFLOWER)  //发现'}'
		{
			if(initflower_flag!=0)              //发现初始化数组时使用的'}'
			{
				initflower_flag--;
			}
			else
			{
				F_layer--;
				if(F_layer==Flower_layer)
				{
					FLN.end=i;
					FLN.layer=F_layer;
					if(i+1<TArry.GetSize() && TArry[i].line!=TArry[i+1].line)
					{
						Blength++;
					}
					FLN.Blength=Blength;
					FL_Arry.Add(FLN);
					if(Blength>Block_ThrA)
					{
						Mark_Flower(FLN.beg+1,FLN.end-1,F_layer+1,TArry,FL_Arry);
					}
				}
			}
		}
		if(i+1<=Bend && TArry[i].line!=TArry[i+1].line)
		{
			Blength++;
		}
	}
}

bool Out_TokenSeq::Add_BlockHead(const int &i,CArray <FL_NODE,FL_NODE> &FL_Arry,const CArray <TNODE,TNODE> &TArry)
{
	bool Find_flag=0;
	int  index=FL_Arry[i].beg;
	if(Find_LRCIRCLE(index, TArry)==0)//王倩注释-20080725-如果{前的()不配对，则返回0，操作失败
		return 0;

    //王倩注释-20080725-当没有()时,index==块首位置
	//王倩注释-20080725-当存在()时,index=="("的前一个位置
	while( index>=0 && //王倩注释-20080725-"index--;"
		   TArry[index].key!=CN_LINE && 
		   TArry[index].key!=CN_LFLOWER && 
		   TArry[index].key!=CN_RFLOWER)//王倩注释-20080725-控制向前读的范围位于同一行		   
	{
		switch(TArry[index].key)
		{
			case CN_ELSE:   //王倩注释-20080725-else{}
			case CN_ENUM:
			case CN_STRUCT:
			case CN_UNION:
				Find_flag=1;
				break;
		    case CN_IF:		//王倩注释-20080725-if或else if	
			case CN_FOR:
			case CN_WHILE:
			case CN_SWITCH:
			case CN_DFUNCTION://王倩注释-20080725-只包括函数定义的头部，不包括函数声明语句
				if(index+1<TArry.GetSize() && TArry[index+1].key==CN_LCIRCLE)
				{
					Find_flag=1;
				}
				break;
			case CN_DO:
				if(Deal_DoWhile(i,FL_Arry,TArry))
				{
					Find_flag=1;
				}
				break;
			default:
				break;
		}
		if(Find_flag == 1)//王倩注释-20080725-{}前有头部内容
		{
			while(index-1>=0 && TArry[index].line==TArry[index-1].line)
				index--;//王倩注释-20080725-将index移到头部内容的最前端
			FL_Arry[i].beg=index;//王倩注释-20080725-修改基本块的起始位置
			FL_Arry[i].Blength++;//王倩注释-20080725-基本块的行数增加1
			                     //王倩疑问-20080725-是否需要，有待考虑
			break;
		}
		index--;//王倩注释-20080725-前移一个token字
	}
	return Find_flag;//王倩注释-20080725-加入头部内容返回1，否则返回0
}


bool Out_TokenSeq::Find_LRCIRCLE(int &index, const CArray <TNODE,TNODE> &TArry)
{
	index--;

	/**wq-20081231-add***/
	if( index <0 )
	{
		return 0;
	}/****************************/

	int nRec=0;
	if(TArry[index].key==CN_RCIRCLE)
	{
		nRec++;
	}
	while(nRec!=0 && index-1>=0)//王倩注释-20080725-()配对或越界则跳出
	{
		index--;
		if(TArry[index].key==CN_LCIRCLE)
		{
			nRec--;
		}
		if(TArry[index].key==CN_RCIRCLE)
		{
			nRec++;
		}
	}

	if(nRec==0)
		return 1;//王倩注释-20080725-返回时index为(的前一个位置
	else 
		return 0;
}


bool Out_TokenSeq::Add_Semicolon(const int &i,CArray <FL_NODE,FL_NODE> &FL_Arry,const CArray <TNODE,TNODE> &TArry)
{
	int index=FL_Arry[i].end;
	if( index+1<TArry.GetSize() && TArry[index].key==CN_RFLOWER && TArry[index+1].key==CN_LINE)
	{
		FL_Arry[i].end=index+1;
		if(TArry[index].line==TArry[index+1].line)
			FL_Arry[i].Blength++;
		return 1;
	}
	return 0;
}


int Out_TokenSeq::Count_Line(const int &beg, const int &end, const CArray <TNODE,TNODE> &TArry)
{
	int count=0;
	for(int i=beg; i<=end; i++)
	{
		if(i+1<TArry.GetSize() && TArry[i].line!=TArry[i+1].line)
			count++;
	}
	if(end == TArry.GetSize()-1)
	{
		count++;
	}
	return count;
}

FL_NODE	Out_TokenSeq::INIT_FL_NODE(const int beg, const int end, const int layer, const int Blength)
{
	FL_NODE FLN;
	FLN.beg=beg;
	FLN.end=end;
	FLN.layer=layer;
	FLN.Blength=Blength;
	return FLN;
}

void Out_TokenSeq::Reset_Block(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry)
{
	FL_NODE FLN;
	CArray <FL_NODE,FL_NODE> FL_Temp_Arry;
	int TD_Num=TArry.GetSize()-1;
	int Whole_Length=Count_Line(0,TD_Num,TArry);//王倩注释-20080725-计算0和TD_Num范围内token串源代码的实际行数
	FLN=INIT_FL_NODE(0,TD_Num,0,Whole_Length);
	/*王倩注释-20080725-begin-初始化分块信息节点变量FLN
    FLN.beg=0;
	FLN.end=TD_Num;
	FLN.layer=0;
	FLN.Blength=Whole_Length;
	王倩注释-20080725-end*/

	FL_Temp_Arry.Add(FLN);
	int Max_layer=0;
	int i=0,j=0,k=0;
	for(i=0; i<FL_Arry.GetSize(); i++)
	{
		if(Max_layer<FL_Arry[i].layer)
			Max_layer=FL_Arry[i].layer;//王倩注释-20080724-找到最深层，记录于Max_layer
	}
	for(i=0; i<Max_layer; i++)//i记录合并操作的层数
	{//王倩注释-20080725-由外向内，将第i层分块块内（即第i+1层分块间）的散语句合并成为一个新的块

		for(j=0; j<FL_Temp_Arry.GetSize(); j++)//j记录数组FL_Temp_Arry的下标
		{//王倩注释-20080725-初始状态FL_Temp_Arry.GetSize()==1
			//王倩注释-20080725-找到与层数为i的分块FL_Temp_Arry[j]
			//，将块FL_Temp_Arry[j]内的散语句合并成新块
			if(FL_Temp_Arry[j].layer==i)//王倩注释-20080725-初始状态FL_Temp_Arry[0].layer==0
			{
				int Temp_beg=FL_Temp_Arry[j].beg;//王倩注释-20080725-初值为0
				int Temp_end=FL_Temp_Arry[j].end;//王倩注释-20080725-初值为TArry.GetSize()-1
				int Temp_Blength=0;
				for(k=0; k<FL_Arry.GetSize(); k++)//k记录数组FL_Arry的下标
				{//王倩注释-20080725-顺序遍历FL_Arry记录的每个基本块
				 //王倩注释-20080725-按照FL_Arry的生成规则，块的嵌套层次局部递增
				 //从前向后，每次循环合并一个最前位置的散语句块
					if(FL_Arry[k].layer==i+1 && Temp_beg<=FL_Arry[k].beg && Temp_end>=FL_Arry[k].end)
					{	//若层数为i+1，且基本块在Temp_beg至Temp_end的范围内

						Temp_beg=FL_Temp_Arry[j].beg;
						Temp_end=FL_Temp_Arry[j].end;
						FL_Temp_Arry.RemoveAt(j);
						/*王倩注释-20080725-begin
						考虑到在循环内，FL_Temp_Arry被修改
						Temp_beg,Temp_end被重新赋值，记录FL_Temp_Arry[j]的起始、终止
						然后将被记录后的FL_Temp_Arry对应节点删除
						王倩注释-20080725-end*/						
						
						if(Temp_beg<FL_Arry[k].beg)
						{//王倩注释-20080725-如果Tmep_beg到块k前之间有散语句
						 //，则将这些散语句合并成一块，加入FL_Temp_Arry[j]
							Temp_Blength=Count_Line(Temp_beg,FL_Arry[k].beg-1,TArry);
							//王倩注释-20080725-计算Tmep_beg到块k之前的实际代码行数
							FLN=INIT_FL_NODE(Temp_beg,FL_Arry[k].beg-1,i,Temp_Blength);
							/*王倩注释-20080725-begin-设置分块信息节点变量FLN
							FLN.beg=Temp_beg;
							FLN.end=FL_Arry[k].beg-1;
							FLN.layer=i;
							FLN.Blength=Temp_Blength;
							王倩注释-20080725-end*/
							FL_Temp_Arry.InsertAt(j,FLN);
							//王倩注释-20080725-将FLN插入FL_Temp_Arry[j]
							j++;
						}
						FL_Temp_Arry.InsertAt(j,FL_Arry[k]);
						//将块k也依次加入FL_Temp_Arry
						if(Temp_end>FL_Arry[k].end)
						{//王倩注释-20080725-如果块k后到Tmep_end之间有散语句
						 //，则将这些散语句合并成一块，依次加入FL_Temp_Arry
						 /*王倩注释-20080725-此块的终止一直等于TArry.GetSize()-1,
						 起始随着循环不断向终止靠近
						 因此，每次循环开始此处的beg和end被赋值给Temp_big和Temp_end，
						 然后被清除于FL_Temp_Arry，再在此处更新；
						 FL_Temp_Arry的最后一个元素一直处于被更新状态，
						 之前其余的元素为被重新分块好的结果*/
							j++;
							Temp_Blength=Count_Line(FL_Arry[k].end+1,Temp_end,TArry);
							FLN=INIT_FL_NODE(FL_Arry[k].end+1,Temp_end,i,Temp_Blength);
							FL_Temp_Arry.InsertAt(j,FLN);
						}
					}
				}
			}
		}
	}
	FL_Arry.RemoveAll();
	FL_Arry.Copy(FL_Temp_Arry);
	FL_Temp_Arry.RemoveAll();
}

void Out_TokenSeq::Deal_Flower(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry)
//重新为Blength赋值（单行的'{'或'}'不计入段长度）; 将'{'合并到下一段，将'}'合并到上一段，
{
	int i;
	for (i=0; i<FL_Arry.GetSize(); i++)
	{
		int beg=FL_Arry[i].beg;
		int end=FL_Arry[i].end;
		int F_count=0, F_sum=0;
		for(int j=beg; j<=end; j++)
		{
			if(j<0 || j>TArry.GetSize())
			{
				break;
			}//越界！！！
			if(TArry[j].key==CN_LFLOWER || TArry[j].key==CN_RFLOWER)
			{
				if(TArry[j].key==CN_LFLOWER)
				{
					F_count++;
				}
				if(TArry[j].key==CN_RFLOWER)
				{
					F_count--;
				}
				if(j-1>0 && TArry[j-1].line==TArry[j].line)
				{
					continue;
				}//如果前面有一个token字与TArry[j]在同一行
				if(j+1<TArry.GetSize() && TArry[j+1].line==TArry[j].line)
				{
					continue;
				}//如果后面有一个token字与TArry[j]在同一行
				F_sum++;//找到'{'或'}'单独作为一行
			}
		}
		FL_Arry[i].F_count=F_count;
		FL_Arry[i].Blength=FL_Arry[i].Blength-F_sum;
	}//重新为Blength赋值（单行的'{'或'}'不计入段长度）
	
	for (i=0; i<FL_Arry.GetSize(); i++)
	{
		if(FL_Arry[i].Blength<=2 && FL_Arry[i].F_count!=0)
		{
			if( FL_Arry[i].F_count>0 && (i+1)<FL_Arry.GetSize() && 
				FL_Arry[i].Blength+FL_Arry[i+1].Blength <= Block_ThrC)
			{
				FL_Arry[i].layer=FL_Arry[i+1].layer;
				Combin_NextBlock(i,FL_Arry);
			}//'{'多的程序快与后面的程序块合并
			if( FL_Arry[i].F_count<0 &&  i>0 && 
				FL_Arry[i-1].Blength+FL_Arry[i].Blength <= Block_ThrC)
			{
				Combin_NextBlock(i-1,FL_Arry);
				i--;
			}//'}'多的程序快与前面的程序块合并
		}
	}
}

void Out_TokenSeq::Combin_Block(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry)//合并同层的小模块
{
	for (int i=0; i+1<FL_Arry.GetSize(); i++)
	{
		if( //FL_Arry[i].layer==FL_Arry[i+1].layer && 
			FL_Arry[i].F_count>=0 && FL_Arry[i+1].F_count<=0 &&
			FL_Arry[i].Blength+FL_Arry[i+1].Blength <= Block_ThrC)
		{
			Combin_NextBlock(i,FL_Arry);
		}//同层合并
	}
}

void Out_TokenSeq::Combin_NextBlock(int pos, CArray <FL_NODE,FL_NODE> &FL_Arry)
{
	FL_Arry[pos].end=FL_Arry[pos+1].end;
	FL_Arry[pos].F_count+=FL_Arry[pos+1].F_count;
	FL_Arry[pos].Blength+=FL_Arry[pos+1].Blength;
	FL_Arry.RemoveAt(pos+1);
}

bool Out_TokenSeq::Deal_DoWhile(const int &i,CArray <FL_NODE,FL_NODE> &FL_Arry,const CArray <TNODE,TNODE> &TArry)
{
	bool Deal_flag=0;
	int index=FL_Arry[i].end+1;
	int nRec=0;
	if(TArry[index].key==CN_WHILE && index+2<TArry.GetSize() && TArry[index+1].key==CN_LCIRCLE)
	{
		index++;
		nRec++;
	}
	while(nRec!=0 && index+1<TArry.GetSize())
	{
		index++;
		if(TArry[index].key==CN_LCIRCLE)
		{
			nRec++;
		}
		if(TArry[index].key==CN_RCIRCLE)
		{
			nRec--;
		}
	}
	if(nRec==0 && index+1<TArry.GetSize() && TArry[index+1].key==CN_LINE)
	{
		FL_Arry[i].end=index+1;
		//FL_Arry[i].Blength++;
		Deal_flag=1;
	}
	return Deal_flag;
}

void Out_TokenSeq::Div_SwitchBlock(CArray <FL_NODE,FL_NODE> &FL_Arry, const CArray <TNODE,TNODE> &TArry)
{
	//
}
/////////////////////////////////////////////////////////////////////////////////////////////
CString Out_TokenSeq::GENERATE_TOKEN(CArray<TNODE,TNODE> &TArry,const CArray<SNODE,SNODE> &SArry,const CArray<FNODE,FNODE> &FArry)
{	
	//返回一个CString类型变量存贮的Token流
	CString str_token=_T("");
	int key,i;
	CString str=_T(""),Line=_T("");
	//cout<<TArry.GetSize()<<endl;
	for(i=0;i<TArry.GetSize();i++)
	{
		key=TArry[i].key;

		/***王倩修改-20080817-去掉序列中的{和}对应的数字项**
		if( key == CN_LFLOWER || key == CN_RFLOWER )
		{
			continue;
		}*/
	
		if( (key>0 && key<=82) || (key>=94 && key<=105) || 
		    (key>=109 && key<=139) || (key==108) )//关键字、运算符、SDG符、头文件字符和"NULL"
		{
			str=CLX_ALL_KEYWORD[key];
			//cout<<str<<endl;
		}		
		else if(key>=83 && key<=86)	//变量名、数组名、指针名和指针数组名
		{
			if(key==83)
			{
				str="B_";
				if(TArry[i].addr>=0 && TArry[i].addr<SArry.GetSize())
				{
					//wq-20090520-注释掉
//					VARIABLE_TYPE(str, SArry[TArry[i].addr].type, SArry[TArry[i].addr].kind);
				}
			}
			else
			{
				str=CLX_ALL_KEYWORD[key];
			}			
		}
		else if(key>=87 && key<=93) // 各类常数
		{
			switch(key)
			{
				case 87: //string(#define宏定义)
					str=CLX_ALL_KEYWORD[key];
					break;
				case 88: //CINT
					str="C_fig";
					break;
				case 89: //CCHAR
					str="C_har";
					break;
				case 90: //CLONG
				case 91: //CFLOAT
				case 92: //CDOUBLE
					str="C_fig";
					break;
				case 93:
					str="C_str";
					break;
			}
		}
		else if(key==106)
		{
			if(TArry[i].name == "main")
				str="main";
			else
			{
				str="H_";//函数名前缀  void类型？
				if(TArry[i].addr>=0 && TArry[i].addr<FArry.GetSize())
				{
					//wq-20090520-注释掉
//					VARIABLE_TYPE(str, FArry[TArry[i].addr].rtype, FArry[TArry[i].addr].rkind);
				}
			}
		}
		else if(key==107)
		{
			str=C_FUNCTION[TArry[i].addr];
		}
		TArry[i].name=str;
		
		str_token=str_token+" "+str;
		
		if( (i+1)<TArry.GetSize() && TArry[i].line!=TArry[i+1].line )
		{
			str_token=str_token+"\r\n";			
			Line.Format("%d",TArry[i].line);
			str_token=str_token+Line+"\r\n";	
		}else if((i+1)==TArry.GetSize())
		{		
			Line.Format("%d",TArry[i].line);
			str_token=str_token+"\r\n"+Line;
		}
		
	}
	return str_token;
}


void Out_TokenSeq::VARIABLE_TYPE( CString &str, const int &type,const int &kind)
{
	//根据变量的type和kind形成新的与变量名对应的token
	switch(type) //变量的type
	{
		case 4: //char
			str+="c";//字符类型变量
			break;
		case  9: //double
		case 13: //float
		case 17: //int
		case 18: //long
			str+="g";//数字类型变量
			break;
		case 30: //unsigned
			str+="u"; //无符号类型
			break;
		case 132: //struct
			str+="s"; //结构体类型
			break;
		case 139: //file
			str+="f"; //文件类型
			break;
		default:      //其他
			str+="_";
		}
					
	switch(kind) //变量的kind
	{
		case 83: //普通变量
			str+="v";
			break;
		case 84: //数组
			str+="r";
			break;
		case 85: //指针
			str+="p";
			break;
		case 86: //指针数组
			str+="a";
			break;
		default: //其他
			str+="_";
	}
	
}


CString Out_TokenSeq::GENERATE_SEQ(const CArray<TNODE,TNODE> &TArry, 
								   const CArray <FL_NODE,FL_NODE> &FL_Arry, 
								   CArray <Seq_NODE,Seq_NODE> &Seq_Arry, 
								   const CString c_filename)
{
	int i,j;
	Seq_NODE SEQN;
	int seqArrySize = 0;//wq-20081130-始终记录序列数据库每次添加新元素后的大小
	SEQN.cfilepath=c_filename;
	CString str=_T(""), Pos=_T("");
	CString str_Index = _T("");
	int layer = 0;
	for(i=0; i<FL_Arry.GetSize(); i++)
	{
	
		int seqsize = Seq_Arry.GetSize();
		if(TArry[FL_Arry[i].beg].key==CN_SWITCH && FL_Arry[i].Blength >= 30)
		{
			continue;
		}
		if(FL_Arry[i].Blength <= Block_ThrB || TArry[FL_Arry[i].beg].key==CN_SWITCH)
		{
			if(FL_Arry[i].beg>=TArry.GetSize() || FL_Arry[i].end>=TArry.GetSize())
			{
				return "";
			}
			SEQN.funcRang.srcBegLine = TArry[FL_Arry[i].beg].funcBegLine; //wq-20081221
			SEQN.funcRang.srcEndLine = TArry[FL_Arry[i].beg].funcEndLine; //wq-20081221
			SEQN.funcBgnRealLine = TArry[FL_Arry[i].beg].funcBgnRealLine; //wq-20090202
			SEQN.funcEndRealLine = TArry[FL_Arry[i].beg].funcEndRealLine; //wq-20090202
			SEQN.beg=TArry[FL_Arry[i].beg].line;
			SEQN.end=TArry[FL_Arry[i].end].line;
			SEQN.Sequence=_T("");
			SEQN.Line_Pos=_T("");
			SEQN.relaLPOS = _T("");
			SEQN.str_srcLine = _T("");//wq20080924
            int token_Begin = 0;//wq20080924
			int token_End = 0;//wq20080924
			int itemNum = 0;//王倩-20081113-记录项在序列中的位置
			int begin = FL_Arry[i].beg;
			int end = FL_Arry[i].end;
			int iniFlag = 0;
			for(j=FL_Arry[i].beg; j<=FL_Arry[i].end; j++)
			{
				/***wq-20090309-删除#if,#endif等***********/
				if( TArry[j].key == CN_WELL )
				{
					j++;
					for( ; j < TArry.GetSize() && TArry[j].line == TArry[j-1].line; j++ )
					{
					}
					continue;
				}/*******************************************/
				
				/***王倩修改-20080717-去掉序列中的{和}对应的数字项***/
				if(TArry[j].key == CN_LFLOWER)
				{
					if( j>0 && TArry[j-1].key == CN_EQUAL || iniFlag != 0 )
					{
						iniFlag++;
					}
					else
					{
						layer++;
						continue;
					}
				}
				else if( TArry[j].key == CN_RFLOWER )
				{
					if( iniFlag != 0 )
					{
						iniFlag--;
					}
					else
					{
						layer--;
						if( j+1 < TArry.GetSize() && TArry[j+1].key == CN_LINE)//wq-20081115，token链表中记录"};"，itemNum保持与之前的相等
						{
							j++;
						}
						while(j+1 < TArry.GetSize() && TArry[j+1].key == CN_RFLOWER && layer > 0)
						{
							j++;
							if( j > FL_Arry[i].end )
							{
								i++;
							}
							layer--;
						}
						
						continue;
					}
				}

				//wq20080824
				if( j == FL_Arry[i].beg
					|| ( (j > 0) && (j < TArry.GetSize())
					   && (TArry[j].line != TArry[j-1].line) ) )				
				{
					token_Begin = j;//wq20080824//记录该行TArry的起始位置
					str_Index.Format(_T("%d"), token_Begin);
				}////////	
				SEQN.str_srcLine += TArry[j].srcWord + " ";////与生成序列对应的token串链表同时进行//wq-20081113
				str=str+TArry[j].name+" ";
//				AfxMessageBox(str);
				//王倩添加-20080801
				if((j+1<TArry.GetSize() && TArry[j].line!=TArry[j+1].line) || j==FL_Arry[i].end)
//wq//			if((j+1<TArry.GetSize() && TArry[j].srcLine!=TArry[j+1].srcLine) || j==FL_Arry[i].end)
				{
					itemNum++;//王倩-20081113-记录项在序列中的位置
					SEQN.Sequence+=Token_to_Seq(str);
					str=_T("");
					Pos.Format("%d",(TArry[j].srcLine+10000));
					SEQN.Line_Pos+=Pos.Right(4);
					Pos.Format("%d",(TArry[j].line + 10000) );
					SEQN.relaLPOS += Pos.Right(4);

					//wq//20080824//记录记录该行在TArry的起始位置
					token_End = j;					
					str_Index.Format(_T("%d"),token_End);
					SEQN.str_srcLine += "\r\n";
					//////////////////////////

				}//生成每行Token串对应的数字序列，并联在一起
			}
			if(SEQN.Sequence != "")//wq-20081114
			{
				SEQN.qsize=SEQN.Sequence.GetLength()/10;
				Seq_Arry.Add(SEQN);
				seqArrySize++;//wq-20081130
			}
			itemNum = 0;//wq-20081114
		}
		else //王倩注释-20080801-行数超过阈值Block_ThrB，则每一行作为一个块
		{			
			SEQN.qsize=1;
			SEQN.Sequence=_T("");
            int token_Begin = 0;//wq20080924
			int token_End = 0;//wq20080924
			int itemNum = 0;//王倩-20081113-记录项在序列中的位置
			int iniFlag = 0;
			for(j=FL_Arry[i].beg; j<=FL_Arry[i].end; j++)
			{
				/***wq-20090309-删除#if,#endif等***********/
				if( TArry[j].key == CN_WELL )
				{
					j++;
					for( ; j < TArry.GetSize() && TArry[j].line == TArry[j-1].line; j++ )
					{
					}
					continue;
				}/*******************************************/

				/***王倩修改-20080717-去掉序列中的{和}对应的数字项***/
				/*********/
				if(TArry[j].key == CN_LFLOWER)
				{
					if( j>0 && TArry[j-1].key == CN_EQUAL || iniFlag != 0 )
					{
						iniFlag++;
					}
					else
					{
						layer++;
						continue;
					}
				}
				else if(TArry[j].key == CN_RFLOWER)
				{
					if( iniFlag != 0 )
					{
						iniFlag--;
					}
					else
					{
						layer--;
						if( j+1< TArry.GetSize() && TArry[j+1].key == CN_LINE)//wq20081114-去掉"};"
						{
							j++;
						}
						while(j+1 < TArry.GetSize() && TArry[j+1].key == CN_RFLOWER && layer > 0)
						{
							j++;
							if( j > FL_Arry[i].end )
							{
								i++;
							}
							layer--;
						}
						continue;
					}
				}/****/

				//wq20080824
				if( j == FL_Arry[i].beg
					|| ( (j > 0) && (j < TArry.GetSize())
					   && (TArry[j].line != TArry[j-1].line) ) )				
				{
					token_Begin = j;//wq20080824//记录该行TArry的起始位置
					str_Index.Format(_T("%d"), token_Begin);
					SEQN.str_srcLine = _T("");
				}////////	
				
				
				SEQN.str_srcLine += TArry[j].srcWord + " ";////与生成序列对应的token串链表同时进行//wq-20081113
				str=str+TArry[j].name+" ";

				//王倩添加-20080801
				if((j+1<TArry.GetSize() && TArry[j].line!=TArry[j+1].line) || j==FL_Arry[i].end)
//wq//			if((j+1<TArry.GetSize() && TArry[j].srcLine!=TArry[j+1].srcLine) || j==FL_Arry[i].end)
				{
					itemNum++;//王倩-20081113-记录项在序列中的位置
					SEQN.funcRang.srcBegLine = TArry[j].funcBegLine; //wq-20081221
					SEQN.funcRang.srcEndLine = TArry[j].funcEndLine; //wq-20081221
					SEQN.funcBgnRealLine = TArry[j].funcBgnRealLine; //wq-20090202
					SEQN.funcEndRealLine = TArry[j].funcEndRealLine; //wq-20090202
					SEQN.beg=TArry[j].srcLine;
					SEQN.end=TArry[j].srcLine;
					SEQN.Sequence=Token_to_Seq(str);
					Pos.Format("%d",(TArry[j].srcLine+10000));
					SEQN.Line_Pos=Pos.Right(4);
					Pos.Format("%d",(TArry[j].line + 10000) );
					SEQN.relaLPOS = Pos.Right(4);

					//wq//20080824//记录记录该行在TArry的起始位置
					token_End = j;
					
					str_Index.Format(_T("%d"),token_End);
					//////////////////////////
					
					if(SEQN.Sequence != "")//wq-20081114
					{
						Seq_Arry.Add(SEQN);
						seqArrySize++;//wq-20081130
					}
//					SEQN.head = SEQN.tail = NULL;//wq-20081114
					itemNum = 0;//wq-20081114
					str=_T("");
				}
			}
		}
	}
	CString seq_str=_T("");
	CString str_TKIndex = _T("");
	for(i=0; i<Seq_Arry.GetSize(); i++)
	{
		Seq_Arry[i].B_index=i;//王倩注释-20080717-记录重复代码的相对位置(已经去除空行、{、}、注释行等)
		seq_str+=Seq_Arry[i].Sequence+"\r\n";
//		str_TKIndex += Seq_Arry[i].str_tokenIndex + "\r\n";
	}
	seq_str += str_TKIndex;
	return seq_str;
}

CString  Out_TokenSeq::Token_to_Seq(const CString &token)
{
	long int Hsize=1000000000;
	unsigned long h=0, g;
	CString seq=_T("");
	int i;
    for(i=0; i<token.GetLength(); i++)
	{
        h = (h << 4)+ token.GetAt(i);
        if(g = h & 0xf0000000)
		{
            h ^= g>>24;
            h ^= g;
        }
    }
	h = h % Hsize;
	seq.Format("%d",h);
	int Slength=seq.GetLength();
	for(i=0; i<10-Slength; i++)//输出长度固定为10
	{
		seq=" "+seq;
	}
	return seq;
}

void Out_TokenSeq::PUTOUT_TOKENALL(const CArray<TNODE,TNODE> &TArry,const CArray<SNODE,SNODE> &SArry,const CArray<FNODE,FNODE> &FArry)
{
	//此函数为测试函数，在程序中不起作用！
	int i=0;
	cout <<"--------"<<"SArry.GetSize():"<<SArry.GetSize()<<endl;
	for(i=0;i<SArry.GetSize();i++)
	{
		cout<<"type:"<<SArry[i].type<<" ";
		cout<<"kind:"<<SArry[i].kind<<" ";
		cout<<"value:"<<SArry[i].value<<" ";
		cout<<"name:"<<SArry[i].name<<endl;
	}
	cout <<"--------"<<"FArry.GetSize():"<<FArry.GetSize()<<endl;
	for(i=0;i<FArry.GetSize();i++)
	{
		cout<<"id:"<<FArry[i].fid<<" ";
		cout<<"rtype:"<<FArry[i].rtype<<" ";
		cout<<"rkind:"<<FArry[i].rkind<<" ";
		cout<<"pnum:"<<FArry[i].pnum<<" ";
		cout<<"name:"<<FArry[i].name<<endl;
	}

	/*for(i=0;i<TArry.GetSize();i++)
	{
		if(TArry[i].key>0 && TArry[i].key<140)
		{
			cout<<CLX_ALL_KEYWORD[TArry[i].key]<<" ";  //KEY
			//cout<<"("<<TArry[i].addr<<") ";           //ADDR
			//cout<<TArry[i].value<<" ";               //VALUE
			if(TArry[i].name==_T(""))
				cout<<"X-X"<<endl;
			else
				cout<<TArry[i].name <<" ";           //NAME
			if((i+1)<TArry.GetSize() && TArry[i].line!=TArry[i+1].line)
				cout<<"\r\n"<<endl;
			cout<<" ";
		}
	}*/
}

void Out_TokenSeq::WRITE_SEQFILE(const CString str_token,const CString cFilePath)
//将Token流写入对应名称的txt文件中，存在D:\Token_Out\目录下
//命名规则：将与token对应的c文件的路径名作为其文件名，由于文件名中无法包含':'和'\'，故将其替换为'^'和'$'；
{
	CString tFileName=cFilePath;
	tFileName.Replace(':','^');
	tFileName.Replace('\\','$');
	tFileName.Delete((tFileName.GetLength()-2),2);//去掉.c后缀名
//	tFileName="D:\\WangQian\\Seq_Out\\"+tFileName+".txt";
	tFileName=out_file_path+"\\Seq_Out\\"+tFileName+".txt";

	CStdioFile Out_Token;
	Out_Token.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	Out_Token.WriteString(str_token);
	Out_Token.Close();
}

void Out_TokenSeq::WRITE_TOKENFILE(const CString str_token,const CString cFilePath)
//将Token流写入对应名称的txt文件中，存在D:\Token_Out\目录下
//命名规则：将与token对应的c文件的路径名作为其文件名，由于文件名中无法包含':'和'\'，故将其替换为'^'和'$'；
{
	CString tFileName=cFilePath;
	tFileName.Replace(':','^');
	tFileName.Replace('\\','$');
	tFileName.Delete((tFileName.GetLength()-2),2);//去掉.c后缀名
//	tFileName="D:\\WangQian\\Token_Out\\"+tFileName+".txt";
	tFileName=out_file_path+"\\Token_Out\\"+tFileName+".txt";
	
	CStdioFile Out_Token;
	Out_Token.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	Out_Token.WriteString(str_token);
	Out_Token.Close();
}

void Out_TokenSeq::OUTPUT_SEQ(FilesToTokens &filesToTokens,//wq-20090319
							  const CString c_filename, 
							  CArray <Seq_NODE,Seq_NODE> &Seq_Arry_G,
						  	  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
							  CArray<CH_FILE_INFO,CH_FILE_INFO> &filesReadInfo//wq-20090610
							  )
{
	CString str_token,seq_str,program=_T("");
	if(READ_CHFILE(c_filename,program)==1)
	{
		
		//   生成token串，并输出之
		CArray<TNODE,TNODE> TArry;
		CArray<SNODE,SNODE> SArry;
		CArray<FNODE,FNODE> FArry;
		CArray<CERROR,CERROR>   EArry;
		CArray<VIPCS,VIPCS>     VArry;	
		CLexical clc;

		clc.Obj_file_path = Obj_file_path;//wq-20090319
		clc.out_file_path = out_file_path;//wq-20090515
		OutputMemoryRecord("","",out_file_path,0,"");
		OutputMemoryRecord("CLexical","operater()",out_file_path,1,"Reading:"+c_filename);
		//AfxMessageBox(program);//ww
		Line_Sum =Line_Sum + (long)clc(0,program,VArry,EArry,TArry,SArry,FArry,headerFilesArry,c_filename,filesReadInfo);//词法分析：将程序program中的信息存入TArry,SArry,FArry中
//		clc.OutputSrcFileAfterTokened(TArry);/////wq//输出正常	
		//AfxMessageBox(program);//ww right
		if(TArry.GetSize() <= Min_Token_Num)
		{
			return;//所含token字太少，不与分析！
		}
		FormatTokenLines ftls;
		ftls(TArry,clc.srcCode);
//		OutputMemoryRecord("FormatTokenLines","operater()",out_file_path,1,"");
		FuncBndBuild fbb;
		fbb(TArry);
//		OutputMemoryRecord("FuncBndBuild","operater()",out_file_path,1,"");

//		filesToTokens.ChangTArryToTokenLines(TArry,SArry,FArry,c_filename);//wq-20090521-delete
//		OutputMemoryRecord("FilesToTokens","ChangTArryToTokenLines",out_file_path,1,"");

//		filesToTokens.TokensOutput();
//		OutputMemoryRecord("FilesToTokens","TokensOutput",out_file_path,1,"");
//		clc.OutputSrcFileAfterTokened(TArry);
		//for(int i=0; i<TArry.GetSize(); i++)
		//{
		//	cout<<i<<":"<<CLX_ALL_KEYWORD[TArry[i].key]<<endl;
		//}

		//PUTOUT_TOKENALL(TArry,SArry,FArry);

		CArray <FL_NODE,FL_NODE> FL_Arry;
		CArray <Seq_NODE,Seq_NODE> Seq_Arry;
		Div_Block(TArry,FL_Arry);					//将源程序分块
//		OutputMemoryRecord("Out_TokenSeq","Div_Block",out_file_path,1,"");

//		clc.OutputSrcFileAfterTokened(TArry);
		str_token=GENERATE_TOKEN(TArry,SArry,FArry);//生成token流
//		OutputMemoryRecord("Out_TokenSeq","GENERATE_TOKEN",out_file_path,1,"");
//		clc.OutputSrcFileAfterTokened(TArry);
		seq_str=GENERATE_SEQ(TArry,FL_Arry,Seq_Arry,c_filename);//生成数字序列
//		OutputMemoryRecord("Out_TokenSeq","GENERATE_SEQ",out_file_path,1,"");
		Seq_Arry_G.Append(Seq_Arry);//将此c文件生成的数字序列加入到数组Seq_Arry_G中

		/*######################################*/
		WRITE_TOKENFILE(str_token,c_filename);      //将token流写入对应的txt文件
		WRITE_SEQFILE(seq_str,c_filename);        //将数字序列写入对应的txt文件
		/*#####################################*/

		TArry.RemoveAll();
		SArry.RemoveAll();
		FArry.RemoveAll();
//		OutputMemoryRecord("Out_TokenSeq","OUTPUT_SEQ",out_file_path,1,"");

	} 
}
/*
//wq-20081114-输出序列项和对应的每行源程序（由token中得到），测试存储的token信息是否正确
void Out_TokenSeq::OutSeqToken(const CArray <Seq_NODE,Seq_NODE> &Seq_Arry_G)
{
	CString str_seqToken = _T("");
	int i;
	CString str_i;
	for( i = 0; i < Seq_Arry_G.GetSize(); i++ )
	{
		str_i.Format(_T("%5d  "),i);
		str_seqToken += str_i;
		str_seqToken += Seq_Arry_G[i].cfilepath + "\r\n";
		int j;
		TKN_LINK_NODE *temp = Seq_Arry_G[i].head;
		TKN_LINK_NODE *tail = Seq_Arry_G[i].tail;
		for( j = 0; j*10 < Seq_Arry_G[i].Sequence.GetLength(); j++ )
		{
			str_seqToken += Seq_Arry_G[i].Sequence.Mid( j*10, 10 ) + "     ";
			while( temp != Seq_Arry_G[i].tail->next 
				   && temp->m_itemNum == j )
			{
				str_seqToken += temp->tkNode.srcWord + " ";
				temp = temp->next;
			}
			str_seqToken += "\r\n";
		}
		str_seqToken += "               ";
		while( temp != Seq_Arry_G[i].tail->next 
				   && temp->m_itemNum == j )
		{
			str_seqToken += temp->tkNode.srcWord + " ";
			temp = temp->next;
		}
		str_seqToken += "\r\n";
		
	}

	CString tFileName = "D:\\SeqToken_Out.txt";
	
	CStdioFile Out_SeqToken;
	Out_SeqToken.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	Out_SeqToken.WriteString(str_seqToken);
	Out_SeqToken.Close();
}
*/


