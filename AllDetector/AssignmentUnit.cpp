// AssignmentUnit.cpp: implementation of the CAssignmentUnit class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "AssignmentUnit.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAssignmentUnit::CAssignmentUnit()
{
	m_sType="ASSIGNMENT";
	m_iAdvl=0;
	m_pleft=NULL;
	b_formal=false;
	visit=0;
}

CAssignmentUnit::~CAssignmentUnit()
{

}

