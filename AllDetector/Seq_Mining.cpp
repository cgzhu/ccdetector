// Seq_Mining.cpp: implementation of the Seq_Mining class.
//
//////////////////////////////////////////////////////////////////////
/*
 * 这个类记录了在数字序列上的序列挖掘的结果信息
 */


#include "stdafx.h"
#include "Lexical.h"

#include "direct.h" 
#include "io.h"
#include "Seq_Mining.h"
#include "Latt_Node.h"
#include <iostream>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Seq_Mining::Seq_Mining()
{
	out_file_path = "";//wq-20090402

}

Seq_Mining::Seq_Mining(CArray <Seq_NODE,Seq_NODE> &Seq_Arry_Glob)
{
	Seq_Arry_G.Copy(Seq_Arry_Glob);
	Seq_Arry_Glob.RemoveAll();
}

Seq_Mining::~Seq_Mining()
{
	int i;
	for( i=0; i<Latt_Root.Son.GetSize(); i++)
	{
		Del_Tree(Latt_Root.Son[i],0);
	}
	Seq_Arry_G.RemoveAll();//wq-20090204

	for( i = 0; i < 10000; i++ )
	{
		LS[i].RemoveAll();
		Ds_HASH[i].RemoveAll();
		FItem_Arry[i].RemoveAll();
		FItem_HASH[i].RemoveAll();
	}
//	int j;
//	for( j = 0; j < Seq_Arry_G.GetSize(); j++ )
//	{
//		TKN_LINK_NODE *temp, *head, *tail;
//		head = Seq_Arry_G[j].head;
//		tail = Seq_Arry_G[j].tail;
//		temp = head;
//		while( head != NULL && tail != NULL 
//			   && head != tail->next )
//		{
//			head = head->next;
//			delete temp;
//			temp = head;
//		}
//	}
}

void Seq_Mining::ClosedMining()//闭合序列挖掘算法总函数
{
	OutputMemoryRecord("Seq_Mining","Begin--ClosedMining",out_file_path,1,"");
	Find_Freqitems();
	Sort_Items();

	CloSpanNum = 0;
	for(int i=0; i<10000; i++)//对每个存在HASH数组FItem_HASH[10000]中的频繁项应用算法CloSpan
	{	
		//cout<<i<<endl;
		int bucket_size=FItem_HASH[i].GetSize();
		for(int j=0; j<bucket_size; j++)
		{			
			//cout<<FItem_HASH[i][j]->Get_Sup()<<":"<<FItem_HASH[i][j]->Get_Item()<<endl;
			CloSpan(FItem_HASH[i][j],&Latt_Root);
		}
	}
	Eliminate_NCS();
	CString stext("");
	stext.Format("%d",CloSpanNum);
	OutputMemoryRecord("Seq_Mining","End--ClosedMining",out_file_path,1,"CloSpan()调用次数:"+stext);
	//Test_Result(Ds_HASH);
	//Test_Result(LS);

	/*for(i=0; i<10000; i++)
	{
		for(int j=0; j<LS[i].GetSize(); j++)
		{
			cout<<LNode_To_Seq(LS[i][j].LNode)<<":"<<LS[i][j].LNode->Get_Sup()<<endl;
		}	
	}*/
}

void Seq_Mining::Find_Freqitems()
//找出数据库Seq_Arry_G中所有出现过的项（items），并存入FItem_HASH[10000]
{

	CArray<Temp_Seq_Node,Temp_Seq_Node> Temp_SeqArry;
	Temp_Seq_Node TS_Node;
	CString str=_T(""),Item=_T("");
	int index=0;//项（item）的后四位
	int i=0,j=0,k=0;

	for(i=0; i<Seq_Arry_G.GetSize(); i++)//开始遍历数据库
	{
		str=Seq_Arry_G[i].Sequence;
		for(j=0; j<str.GetLength()/10; j++)
		{
			Item=str.Mid(j*10,10);
			index=atoi(Item.Right(4));			
			bool Is_Exist=0;//标识该Item是否已加入数组Temp_SeqArry
			for(k=0; k<Temp_SeqArry.GetSize();k++)
			{
				if(Temp_SeqArry[k].Item==Item)
				{
					Is_Exist=1;//该项（items）已存在
					break;
				}
			}
			if(Is_Exist==0)//该项（items）未加入
			{
				TS_Node=Init_TNode(index,Item,i,j+1);
				Temp_SeqArry.Add(TS_Node);
			}
		}//将str中出现的项（items）记录在Temp_SeqArry中

		for(k=0; k<Temp_SeqArry.GetSize(); k++)
		{
			index=Temp_SeqArry[k].index;
			int Seq_ID=Temp_SeqArry[k].Seq_ID;//数据库中包含item序列的ID
			int len=Seq_Arry_G[Seq_ID].qsize;//数据库中包含该项的原序列的长度
			int Subseq_Addr=Temp_SeqArry[k].Subseq_Addr;//子序列的偏移量
			
			int i=Is_Have_Items(index,Temp_SeqArry[k].Item);
			if(i==-1)//项Temp_SeqArry[k].Item还未存入到数组F_Items_Arry[]中
			{
				Latt_Node *Temp_Node_P=new Latt_Node(Temp_SeqArry[k].Item,0);
				Temp_Node_P->Add_DsNode(Seq_ID, Subseq_Addr, len);//将节点DN加入到LND所指节点的数组Ds_Arry中，该数组中存有seq对应的所有后缀序列的信息
				FItem_HASH[index].Add(Temp_Node_P);
			}
			else if(FItem_HASH[index][i]->Get_Item()==Temp_SeqArry[k].Item)//项Temp_SeqArry[k].seq已存入数组F_Items_Arry[]
			{
				FItem_HASH[index][i]->Add_DsNode(Seq_ID, Subseq_Addr, len);
			}
		}
		Temp_SeqArry.RemoveAll();
	}
}

void Seq_Mining::Sort_Items()
//删除FItem_HASH[10000]中出现次数小于2的项，并将频繁项（出现次数大于等于2）存入数组FItem_Arry对应的位置
{
	for(int i=0; i<10000; i++)//遍历FItem_HASH[10000]
	{
		for(int j=0; j<FItem_HASH[i].GetSize(); j++)
		{
			if(FItem_HASH[i][j]->Get_Sup() < 2)
			{
				delete FItem_HASH[i][j];
				FItem_HASH[i].RemoveAt(j);//删掉支持度小于2的节点
				j--;//因为Carray是动态数组，删除一项后，其后面的所有项前移，序号减一
			}
			else
			{
				FItem_Arry[i].Add(FItem_HASH[i][j]->Get_Item());
			}
		}
	}
}

void Seq_Mining::CloSpan(class Latt_Node *Son_Node,class Latt_Node *Father_Node)//CloSpan挖掘算法
{
	CloSpanNum++;
//	OutputMemoryRecord("Seq_Mining","CloSpan",out_file_path,1,"");
	CString Seq = LNode_To_Seq(Father_Node) + Son_Node->Get_Item();	
	int Contained_Mark=Check_ProDBSize(Seq,Son_Node);

	if(Contained_Mark!=-1)//如果不存在backward sub-pattern的情况
	{
		Add_DsHash(Son_Node);//将Son_Node加入到Ds_HASH表中
		Son_Node->Father=Father_Node;
		Father_Node->Son.Add(Son_Node);//连接父子节点Son_Node与Father_Node
	}
	if(Contained_Mark!=0)//如果存在backward super-pattern或backward sub-pattern的情况
	{
		return;
	}
	
	CArray<Latt_Node*,Latt_Node*> Son_Node_Arry;
	Is_Expanded(Son_Node->Ds_Arry,Son_Node_Arry);//查看节点Son_Node是否可以扩展；如可扩展，将可扩展的节点指针存于Son_Node_Arry

	for(int i=0; i<Son_Node_Arry.GetSize(); i++)
	{
		CloSpan(Son_Node_Arry[i],Son_Node);
	}
}

int Seq_Mining::Check_ProDBSize(CString seq, class Latt_Node *S_Node)
//Algorithm 4：查看候选集中是否存在影射数据库大小与S_Node相等，且与序列seq有包含关系的节点
{
	CArray <class Latt_Node *,class Latt_Node *> Lsup;//backward super-pattern
	CArray <class Latt_Node *,class Latt_Node *> Lsub;//backward sub-pattern
	int Is_exist=0;
	int support=S_Node->Get_Sup();
	int Dsize=S_Node->Get_Dsize();
	float IDSum=Get_IDSum(S_Node);
	int index = (Dsize+(int)IDSum)%10000;//Hash表键值为Dsize后四位

	for(int i=0; i<Ds_HASH[index].GetSize(); i++)
	//遍历Hash表中键值为index的桶，找到指向的的序列与seq存在包含关系的节点指针
	{
		class Latt_Node *Pt_L=Ds_HASH[index][i].LNode;//Pt_L指向对应的偏序图节点
		if(Dsize=Ds_HASH[index][i].Ds_Size && support==(Pt_L->Get_Sup()) && IDSum==Get_IDSum(Pt_L))
		//如果投影数据库规模大小相等，序列支持度相等，而且数据库中对应的ID也和相等
		{
			CString seq_t=LNode_To_Seq(Pt_L);//Pt_L所指节点对应的序列
			int Is_Contained = Seq_Is_Contained(seq,seq_t);
			if(Is_Contained==1)//seq包含seq_t
			{
				Lsup.Add(Pt_L);
			}
			else if(Is_Contained==-1)//seq_t包含seq
			{
				Lsub.Add(Pt_L);
			}
		}
	}

	if(Lsup.GetSize()!=0)
	{
		Is_exist=1;
		class Latt_Node *LN_From = Lsup[0];
		Trans_SubTrees(LN_From, S_Node);//将Lsup中第一个节点的子孙移植给S_Node

		for(int j=1; j<Lsup.GetSize(); j++)
		{
			Del_Tree(Lsup[j],1);//删除Lsup在偏序图中对应的子树
		}
	}
	if(Lsub.GetSize()!=0)
	{
		Is_exist=-1;
		Del_Tree(S_Node,0);//删除当前节点及其子孙
		return Is_exist;
	}
	return Is_exist;
}

void Seq_Mining::Eliminate_NCS()//从Ds_HASH中去除非闭合的序列（non-closed sequences）
{
	int i=0, j=0;
	for(i=0; i<10000; i++)
	{
		int bucket = Ds_HASH[i].GetSize();
		for(j=0; j<bucket; j++)
		{
			//CString seqx=LNode_To_Seq(Ds_HASH[i][j].LNode);
			float &SupID_Sum=Ds_HASH[i][j].SupID_Sum;
			if(SupID_Sum > 100000)
			{
				SupID_Sum = SupID_Sum/100000;
			}
			int index=(int)(SupID_Sum*10)%10000;
			LS[index].Add(Ds_HASH[i][j]);
		}
	}//将Ds_HASH中的数据转入到LS中，新HASH表的键值为SupID_Sum
	for(i=0; i<10000; i++)
	{
		for(j=0; j<LS[i].GetSize(); j++)
		{
			float IDSum1=LS[i][j].SupID_Sum;
			int Sup1=LS[i][j].LNode->Get_Sup();
			CString seq1=LNode_To_Seq(LS[i][j].LNode);
			for(int k=j+1; k<LS[i].GetSize(); k++)
			{
				float IDSum2=LS[i][k].SupID_Sum;
				int Sup2=LS[i][k].LNode->Get_Sup();
				if(IDSum1==IDSum2 && Sup1==Sup2)//如果数据库中支持该子序列的序列ID之和相等，而且支持度相等
				{
					CString seq2=LNode_To_Seq(LS[i][k].LNode);
					int Is_contained=Seq_Is_Contained(seq1,seq2);
					if(Is_contained==-1)//seq2包含seq1
					{
						LS[i].SetAt(j,LS[i][k]);
						seq1=seq2;
					}
					if(Is_contained!=0)//seq1与seq2之间存在包含关系
					{
						LS[i].RemoveAt(k);
						k--;
					}
				}
			}
		}
	}
}

int Seq_Mining::Is_Have_Items(int index,CString Item)
//查找HASH表中以index为键值的桶否包含项Item,如找到则返回对应的索引;如没找到则返回-1
{
	for(int i=0; i<FItem_HASH[index].GetSize(); i++)
	{
		if(FItem_HASH[index][i]->Get_Item()==Item)
		{
			return i;
		}
	}
	return -1;
}

Temp_Seq_Node Seq_Mining::Init_TNode(int index, CString Item, int Seq_ID, int Subseq_Addr)
//给节点TS_Node赋值
{
	Temp_Seq_Node TS_Node;
	TS_Node.index=index;
	TS_Node.Item=Item;
	TS_Node.Seq_ID=Seq_ID;				
	TS_Node.Subseq_Addr=Subseq_Addr;
	return TS_Node;
}

void Seq_Mining::Add_DsHash(class Latt_Node *LN)//将指针LN存入Hash表中
{
	int IDSum=(int)Get_IDSum(LN);
	int index=(LN->Get_Dsize()+IDSum)%10000;//投影数据库Ds的大小的后四位对应Hash表的键值
	struct Ds_Hash_Node DHN;
	DHN.Ds_Size=LN->Get_Dsize();
	DHN.SupID_Sum=Get_IDSum(LN);
	DHN.LNode=LN;	
	Ds_HASH[index].Add(DHN);//将存储LN信息的节点DHN加入到Hash表中
}

int Seq_Mining::Is_Expanded(const CArray<Ds_NODE,Ds_NODE> &Ds_Arry, CArray<Latt_Node*,Latt_Node*> &Exp_Node_Arry)
//检查投影数据库Ds_Arry，查看是否可以继续扩展；如可以，将可扩展的项（item）信息存入Exp_Node_Arry
{
	int i=0,j=0,k=0;
	CString str1=_T(""),str2=_T(""),Item=_T("");
	
	for(i=0; i<Ds_Arry.GetSize(); i++)//扫描整个投影数据库
	{
		str1=Seq_Arry_G[Ds_Arry[i].Seq_ID].Sequence;//数据库中对应的序列
		int len1=str1.GetLength()/10;//记录数据库中对应序列的长度
		for(j=Ds_Arry[i].Subseq_Addr; j<len1; j++)//扫描投影数据库中序列的每一项
		{
			Item=str1.Mid(j*10,10);
			bool Item_Is_Add=0;
			for(k=0; k<Exp_Node_Arry.GetSize(); k++)//察看该项是否已被记录
			{
				if(Item==Exp_Node_Arry[k]->Get_Item())
				{
					Item_Is_Add=1;
					break;
				}
			}
			if(Is_FreqItem(Item) && Item_Is_Add==0)//如果Item是频繁项，且未加入到数组Exp_Node_Arry中
			{
				class Latt_Node *Temp_Node_P=new Latt_Node(Item,0);
				Temp_Node_P->Add_DsNode(Ds_Arry[i].Seq_ID, j+1, len1);

				int x=0,y=0;
				for(x=i+1; x<Ds_Arry.GetSize();x++)
				{		
					str2=Seq_Arry_G[Ds_Arry[x].Seq_ID].Sequence;//数据库中对应的序列
					int len2=str2.GetLength()/10;
					for(y=Ds_Arry[x].Subseq_Addr; y<len2; y++)
					{
						if(Item==str2.Mid(y*10,10))
						{
							Temp_Node_P->Add_DsNode(Ds_Arry[x].Seq_ID, y+1, len2);
							break;
						}
					}
				}
				if((Temp_Node_P->Get_Sup()) >= 2)//如果Temp_Node中的item在投影数据库中出现的次数大于等于2
				{
					Exp_Node_Arry.Add(Temp_Node_P);
				}
				else
				{
					delete Temp_Node_P;
				}
			}
		}
	}
	return Exp_Node_Arry.GetSize();
}

bool Seq_Mining::Is_FreqItem(const CString Item)
//通过查询数组FItem_Arry，判断Item是否为频繁项（Frequent Item）
{
	int index=atoi(Item.Right(4));
	for(int i=0; i<FItem_Arry[index].GetSize(); i++)
	{
		if(Item == FItem_Arry[index][i])
			return 1;
	}
	return 0;
}

CString Seq_Mining::LNode_To_Seq(class Latt_Node *LN)
//根据偏序前缀序列图中的某个节点返回对应的序列
{
	CString Seq=_T("");
	class Latt_Node *Pt=LN;
	while (Pt!=NULL) 
	{
		Seq=Pt->Get_Item()+Seq;
		Pt=Pt->Father;
	}
	return Seq;
}

int Seq_Mining::Seq_Is_Contained(const CString seq1, const CString seq2)
//判断数字序列seq1与seq2之间的包含关系
{
	int L1=seq1.GetLength(), L2=seq2.GetLength();
	if(seq1==seq2 || (L1%10)!=0 || (L2%10)!=0 )
	{
		AfxMessageBox("序列相同或序列长度不正常!");
		return 0;
	}
	
	if(L1==L2)
	{
		return 0;//序列长度长度相等！
	}

	CString s1=seq1,s2=seq2;
	CString item=_T("");
	L1=L1/10;
	L2=L2/10;//记录序列包含项的个数
	if(L1>L2)//序列seq1长度大于seq2
	{
		s1=seq2;
		s2=seq1;
		int L_temp=L1;
		L1=L2;
		L2=L_temp;
	}//交换s1和s2（还有L1和L2）的位置，保证s1的长度小于s2，L1小于L2

	int i=0, j=0, k=0;
	bool Is_Contained=1;
	for(i=0; i<L1; i++)
	{
		item=s1.Mid(i*10,10);
		for(j=k; j<L2; j++)//在s2中查找与s1中的item对应的项
		{
			if(item == s2.Mid(j*10,10))
			{
				k=j+1;//记录下此遍历s2的起始位置
				break;
			}
		}
		if(j>=L2 || (L2-j)<(L1-i))//遍历s2结束或没必要继续遍历，不包含s1
		{
			Is_Contained=0;
			break;
		}
	}
	if(Is_Contained == 1)//存在包含关系
	{
		if(seq1.GetLength() > seq2.GetLength())
			return 1;//seq1包含seq2
		else 
			return -1;//seq2包含seq1
	}
	else
	{
		return 0;//没有包含关系
	}
}

void Seq_Mining::Trans_SubTrees(class Latt_Node *LN_From, class Latt_Node *LN_To)
//将节点*LN_From的子孙移植给节点*LN_To，然后删除节点*LN_From
{
	if(LN_From == NULL)
	{
		AfxMessageBox("用于移植的根节点为空!");
		return;
	}
	for(int i=0; i<LN_From->Son.GetSize(); i++)
	{
		LN_From->Son[i]->Father=LN_To;//让LN_From的子节点指向LN_To
	}
	LN_To->Son.Copy(LN_From->Son);//将存储LN_From子节点指针的数组拷贝Son给LN_To
	Del_Node(LN_From,1);
}

void Seq_Mining::Remove_Hash(int index, CArray <int,int> &LS)
//删除那些在DS_HASH表键值为index的桶中，且序号存于LS的元素
{
	int i=LS.GetSize()-1;
	while(i>=0)
	{
		if(LS[i]<Ds_HASH[index].GetSize())
		{
			Ds_HASH[index].RemoveAt(LS[i]);
			i--;//动态数组，为防止序号变化，从后往前删除
		}
		else
		{
			AfxMessageBox("序号无效，越界访问!");
			break;
		}
	}
	LS.RemoveAll();
}

void Seq_Mining::Del_Tree(class Latt_Node *LN, bool Is_delHash)
//释放以LN为根节点的树所申请的内存，Is_delHash表示是否需要删除LN子孙节点在Ds_Hash中对应的节点
{
	if(LN == NULL)
	{
		AfxMessageBox("欲删除的那棵树根节点为空!");
		return;
	}
	CArray <Latt_Node*,Latt_Node*> Queue;//用排队Queue存储需释放内存的节点指针
	Queue.Copy(LN->Son);
	
	while(Queue.GetSize()!=0)
	{
		Del_HashNode(Queue[0]);		
		if(Queue[0] != NULL)//如果Queue第一个元素的子树非空
		{
			if(Queue[0]->Son.GetSize()!=0)
			{
				Queue.Append(Queue[0]->Son);
			}
			delete Queue[0];
			Queue[0]=NULL;
		}
		Queue.RemoveAt(0);
	}
	Del_Node(LN,Is_delHash);
}

bool Seq_Mining::Del_HashNode(class Latt_Node *LN)
//删除Ds_HASH中指向*LN的节点
{
	if(LN == NULL)
	{
		AfxMessageBox("HASH表指针为空，无法删除！");
		return 0;
	}
	bool is_exist=0;
	int IDSum=(int)Get_IDSum(LN);
	int index  = (LN->Get_Dsize()+IDSum)%10000;
	for (int i=0; i<Ds_HASH[index].GetSize(); i++)
	{
		if(Ds_HASH[index][i].LNode == LN)
		{
			is_exist=1;
			Ds_HASH[index].RemoveAt(i);
			break;
		}
	}
	if(is_exist==0)
	{
		AfxMessageBox("Ds_HASH中没有这个指针，无法删除！");
	}
	return is_exist;
}

void Seq_Mining::Del_Node(class Latt_Node *LN, bool Is_delHash)//释放指针LN指向的节点内存
{
	if(LN->Father != NULL)
	{
		class Latt_Node *Father=LN->Father;
		bool Is_find=0;
		for(int i=0; i<Father->Son.GetSize(); i++)
		{
			if(Father->Son[i] == LN)
			{
				Is_find=1;
				Father->Son.RemoveAt(i);
				break;
			}
		}//删除LN_From在其父节点中（指针数组Son里）保存的信息
		if(Is_find==0)
		{
			AfxMessageBox("父节点中没有子节点的信息!");
		}
	}
	if(Is_delHash==1)
	{
		Del_HashNode(LN);
	}
	delete LN;
	LN=NULL;
}

float Seq_Mining::Get_IDSum(class Latt_Node *LN)
//用数据库中支持该子序列的序列ID生成一个浮点数
{
	float IDSum=0;
	float DataBase_Size = (float)Seq_Arry_G.GetSize();
	for(int i=0; i<LN->Ds_Arry.GetSize(); i++)
	{
		float Seq_ID = (float)LN->Ds_Arry[i].Seq_ID;
		IDSum = IDSum + (Seq_ID*Seq_ID/DataBase_Size);
	}
	return IDSum;
}

CString Seq_Mining::Get_SubSeq(int Addr, int ID)//由序列ID和子序列偏移量Addr返回子序列
{
	int Len=Seq_Arry_G[ID].Sequence.GetLength();
	CString str=_T("");
	if(Len/10 > Addr)
	{
		str=Seq_Arry_G[ID].Sequence.Right(Len-Addr*10);
	}
	return str;
}

void Seq_Mining::Test_Result(const CArray <Ds_Hash_Node, Ds_Hash_Node> Test_HASH[])//测试函数
{
	int Sup[500];
	int Len[500];
	int i=0, j=0;
	for(i=0; i<500; i++)
	{
		Sup[i]=0;
		Len[i]=0;
	}
	int count1=0, count2=0;
	for(i=0; i<10000; i++)
	{
		for(j=0; j<Test_HASH[i].GetSize(); j++)
		{
			class Latt_Node *Pt=Test_HASH[i][j].LNode;
			int sup=Pt->Get_Sup();
			Sup[sup]++;
			int len=LNode_To_Seq(Pt).GetLength()/10;
			Len[len]++;
			count1++;
		}	
		if(Test_HASH[i].GetSize()>0)
		{
			count2++;
		}	
	}
	for(i=0; i<10; i++)
	{
		if(Sup[i]!=0)
		{
			cout<<"Sup"<<i<<":"<<Sup[i]<<endl;
		}
	}
	for(i=0; i<10; i++)
	{
		if(Len[i]!=0)
		{
			cout<<"Len"<<i<<":"<<Len[i]<<endl;
		}
	}
	cout<<"Sum of the seq is"<<count1<<endl;
	cout<<"Sum of the key is"<<count2<<endl;
}

void Seq_Mining::OutputSeqToken()
{
	CString str_seqToken = _T("");
	int i;
	CString str_i,str_funcBgn,str_funcEnd;
	for( i = 0; i < Seq_Arry_G.GetSize(); i++ )
	{
		str_i.Format(_T("%5d  "),i);
		str_funcBgn.Format(_T("%d"),Seq_Arry_G[i].funcRang.srcBegLine);
		str_funcBgn += "-";
		str_funcEnd.Format(_T("%d"),Seq_Arry_G[i].funcRang.srcEndLine);
		str_seqToken += str_i + "     ";
		str_seqToken += str_funcBgn + str_funcEnd;
		str_seqToken += Seq_Arry_G[i].cfilepath + "\r\n\r\n";
		str_seqToken +=  Seq_Arry_G[i].Sequence + "\r\n";
		str_seqToken += Seq_Arry_G[i].str_srcLine + "\r\n\r\n";
	}

	CString tFileName = "D:\\SeqTokenFuncRang_Out.txt";
	
	CStdioFile Out_SeqToken;
	Out_SeqToken.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	Out_SeqToken.WriteString(str_seqToken);
	Out_SeqToken.Close();
}
void Seq_Mining::DltSingleLnSeq()
{
	int i;
	for( i = 0; i < Seq_Arry_G.GetSize(); i++ )
	{
/*wq-20081224-delete-函数外的语句全部除去
		if( Seq_Arry_G[i].Sequence.GetLength() == 10
			&& Seq_Arry_G[i].funcRang.srcBegLine == Seq_Arry_G[i].funcRang.srcEndLine )
/************************************************************************************/
		if( Seq_Arry_G[i].funcRang.srcBegLine == 0 && Seq_Arry_G[i].funcRang.srcEndLine == 0 ) 
		{
			Seq_Arry_G.RemoveAt(i);
			i--;
			continue;
		}
	}
/**************/
	CString str_seqToken = _T("");
	CString str_i,str_funcBgn,str_funcEnd;
	for( i = 0; i < Seq_Arry_G.GetSize(); i++ )
	{
		str_i.Format(_T("%5d  "),i);
//		str_funcBgn.Format(_T("%d"),Seq_Arry_G[i].funcRang.srcBegLine);
		str_funcBgn.Format(_T("%d"),Seq_Arry_G[i].funcBgnRealLine);
		str_funcBgn += "-";
//		str_funcEnd.Format(_T("%d"),Seq_Arry_G[i].funcRang.srcEndLine);
		str_funcEnd.Format(_T("%d"),Seq_Arry_G[i].funcEndRealLine);
		str_seqToken += str_i + "     ";
		str_seqToken += str_funcBgn + str_funcEnd;
		str_seqToken += Seq_Arry_G[i].cfilepath + "\r\n\r\n";
		str_seqToken +=  Seq_Arry_G[i].Sequence + "\r\n";
		str_seqToken += Seq_Arry_G[i].str_srcLine + "\r\n\r\n";
	}

	CString tFileName = "D:\\SeqTokenFuncRang_Out2.txt";
	
	CStdioFile Out_SeqToken;
	Out_SeqToken.Open(tFileName, CFile::modeCreate | CFile::modeWrite);
	Out_SeqToken.WriteString(str_seqToken);
	Out_SeqToken.Close();
	/**********************/
}
