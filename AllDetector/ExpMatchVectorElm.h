// ExpMatchVectorElm.h: interface for the ExpMatchVectorElm class.
// 王倩-20090612
// 用于表达式匹配的度量值向量结构定义
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXPMATCHVECTORELM_H__AF80A31B_E181_4561_AF1F_E2443CEB5553__INCLUDED_)
#define AFX_EXPMATCHVECTORELM_H__AF80A31B_E181_4561_AF1F_E2443CEB5553__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ExpMatchVectorElm  
{
public:
	ExpMatchVectorElm();
	virtual ~ExpMatchVectorElm();
public:
	double var;//变量，=所变量权值之和--------------0
	double cstf;//数值常量的个数--------------------1
	double cstc;//字符常量的个数--------------------2
	double csts;//字符串常量的个数------------------3
	double rlt;//关系运算符，=权值之和-----------4
	double lgc;//逻辑运算符，=权值之和-----------5
	double mth;//算术运算符，=权值之和-----------6
	double bfnc;//C库函数个数-----------------------7
	double dfnc;//自定义函数函数个数----------------8
	double bfncPara;//C库函数所有参数总数-----------9
	double dfncPara;//自定义函数所有参数总数--------10
};

#endif // !defined(AFX_EXPMATCHVECTORELM_H__AF80A31B_E181_4561_AF1F_E2443CEB5553__INCLUDED_)
