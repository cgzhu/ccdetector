// SNODE.h: interface for the SNODE class.
// 王倩-20090505
// 符号表节点结构
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SNODE_H__A584DB6F_4AE6_4CEB_8825_A09E86F8D176__INCLUDED_)
#define AFX_SNODE_H__A584DB6F_4AE6_4CEB_8825_A09E86F8D176__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class SNODE  
{
public:
	SNODE();
	virtual ~SNODE();
	SNODE(const SNODE& SN);
	SNODE operator=(const SNODE& SN);
public:
	int     type;       // such as "int" "float" "long"...
	int     kind;       // variable kind(common variable, pointer or array) 
	int     arrdim;     // the dimension number of array
	int     arrsize[4]; // the dimension of array is not beyond 4;
	int     addr;       // address of the variable (no use in this system)
	int	    layer;      // layer number of the variable(line out which function it's in)
	double  value;      // value of float,double variable
	CString name;	// variable name
	int     initaddr;	//用于在函数内联变量重命名时记录原来的addr
	int     taddr;   //用于表示SNODE结点在Token中的位置
	int     sv;	    //wn// point to the id of Struct type,-1 if initiate
	int 	mv;		//wn//point to the id of struct type if member variable is struct type,-2 if initiate,-1 if common member var(not struct type)

};

#endif // !defined(AFX_SNODE_H__A584DB6F_4AE6_4CEB_8825_A09E86F8D176__INCLUDED_)
