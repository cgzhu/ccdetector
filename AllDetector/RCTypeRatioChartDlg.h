#pragma once
#include "rc_type_tchart.h"


// CRCTypeRatioChartDlg 对话框

class CRCTypeRatioChartDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CRCTypeRatioChartDlg)

public:
	CRCTypeRatioChartDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRCTypeRatioChartDlg();

// 对话框数据
	enum { IDD = IDD_RC_TYPE_RATIO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart m_rc_type_ratio_chart;

	void ClearAllSeries();
};
