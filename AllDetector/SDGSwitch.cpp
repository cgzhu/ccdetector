// SDGSwitch.cpp: implementation of the CSDGSwitch class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SDGSwitch.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSDGSwitch::CSDGSwitch()
{
	m_sType="SDGSWITCH";
	HVDefault=false; 
	visit=0;

}

CSDGSwitch::~CSDGSwitch()
{

}
