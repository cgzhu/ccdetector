// CCSegRatioDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "CCSegRatioDlg.h"
#include "afxdialogex.h"
#include "CSeries.h"

// CCCSegRatioDlg 对话框

IMPLEMENT_DYNAMIC(CCCSegRatioDlg, CDialogEx)

CCCSegRatioDlg::CCCSegRatioDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCCSegRatioDlg::IDD, pParent)
	, m_cc_seg_file_ratio_dlg()
{

}

CCCSegRatioDlg::~CCCSegRatioDlg()
{
}

void CCCSegRatioDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TCHART1, m_cc_seg_file_ratio_dlg);
}

//清除所有图线
void CCCSegRatioDlg::ClearAllSeries()
 {
     for(long i = 0;i<m_cc_seg_file_ratio_dlg.get_SeriesCount();i++)
     {
         ((CSeries)m_cc_seg_file_ratio_dlg.Series(i)).Clear();
     }
 }

BEGIN_MESSAGE_MAP(CCCSegRatioDlg, CDialogEx)
END_MESSAGE_MAP()


// CCCSegRatioDlg 消息处理程序
