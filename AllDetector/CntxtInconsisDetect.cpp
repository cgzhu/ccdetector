// CntxtInconsisDetect.cpp: implementation of the CntxtInconsisDetect class.
//
//////////////////////////////////////////////////////////////////////
/*
 * 这个类检测上下文不一致性缺陷
 */

#include "stdafx.h"
#include "AllDetector.h"
#include "CntxtInconsisDetect.h"
#include "Expression.h"

#include "SDG.h"
#include "SDGAssignment.h"
#include "SDGEntry.h"
#include "SDGDeclare.h"
#include "SDGDoWhile.h"
#include "SDGFor.h"
#include "SDGIf.h"
#include "SDGSwitch.h"
#include "SDGWhile.h"
#include "SDGIncDec.h"
#include "SDGBreak.h"
#include "SDGContinue.h"
#include "SDGReturn.h"
#include "SDGCall.h"

#include "ProgramMatch.h"

#include "dataStruct.h"
#include "Lexical.h"
#include "FormatTokenLines.h"
#include "FuncBndBuild.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CntxtInconsisDetect::CntxtInconsisDetect()
{
	cntxtIncnsisType1.RemoveAll();
	cntxtIncnsisType2.RemoveAll();
	Min_len = 1;
	mchValThrhld = 0.0;
}

CntxtInconsisDetect::~CntxtInconsisDetect()
{
	cntxtIncnsisType1.RemoveAll();
	cntxtIncnsisType2.RemoveAll();
	Min_len = 1;

}

/*上下文不一致性缺陷检测入口函数
	filesToTokens:	源文件token行转化表示类
	CP_Seg_Array:	所有克隆代码集合的集合
	headerFilesArry:头文件信息集合
	*/
void CntxtInconsisDetect::operator()(FilesToTokens &filesToTokens,
									 CArray<CP_Segment,CP_Segment> &CP_Seg_Array,
									 CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry)
{	/*
	class CP_Segment:
	CString seq;//对应的闭合频繁子序列
	int len;//闭合频繁子序列的项数，即重复代码的行数
	int sup;//闭合频繁子序列的支持度，及重复代码出现的次数
	bool Is_Expanded;//标志位，记录重复代码是否可以扩展
	CArray <class CP_SEG, class CP_SEG> CP_Array;//重复代码出现的位置

  	class CP_SEG:
	bool ReadCFile(const CString cfilename,CString &program);
	CString filename;	//代码片段所在的文件名
	int funcBegLine;	 //wq-20081220-所在函数的源程序起始(相对)行数
	int funcEndLine;	 //wq-20081220-所在函数的源程序终止(相对)行数

	int	funcBgnRealLine;//wq-20090202-函数起始源程序绝对行数
	int funcEndRealLine;//wq-20090202-函数终止源程序绝对行数

	long FN_Hvalue;//文件名转化的散列值
	CString LPOS;//将每行代码在程序中的绝对位置（四位整数，不够用零补齐）组成字符串
	CString relaLPOS;//相对位置(函数)

	int Beg_index;  //起始模块的序号
	int End_index;  //结束模块的序号
	int B_gap;//子序列的头与原序列的头之间的距离
	int E_gap;//子序列的尾与原序列的尾之间的距离
	bool Is_combine;//是否曾经合并的标志
	int combineTimes;
	CArray<IdLocSetMem,IdLocSetMem> idsLocArray;//wq-20090223-add-记录每个代码片段的所有标识符位置信息
	
	class FilesToTokens:
	//输入的所有C源文件的token行存储表示的集合
	CArray<TokenedFile,TokenedFile> tokenFileArray;
	//输入源文件的路径
	CString Obj_file_path;//wq-20090414
	//结果输出路径
	CString out_file_path;//wq-20090518

	class TokenedFile:
	private:
	CString fileName;//C源文件的完整路径文件名称
	long hashedFileName;//fileName经散列算法hashpjw后对应的数字
	CArray<TokenLine,TokenLine> tokenList;//C源文件token流的按行存储
	CArray<SNODE,SNODE> SArry;//wq-20090505-符号表
	CArray<FNODE,FNODE> FArry;//wq-20090505-函数列表
	*/
/*	CString str;
	for(int i=0; i<filesToTokens.tokenFileArray.GetSize(); ++i)
	{
		for(int j=0; j<filesToTokens.tokenFileArray[i].tokenList.GetSize(); ++j)
		{
			for(int k=0; k<filesToTokens.tokenFileArray[i].tokenList[j].tokenLine.GetSize(); ++k)
			{
				str += filesToTokens.tokenFileArray[i].tokenList[j].tokenLine[k].name;
			}
		}
	}
	AfxMessageBox(str);
*/
/*	
	CString str;
	str.Format("%d",CP_Seg_Array.GetSize());  //CP_OUT中片段个数
	AfxMessageBox(str);
*/
	int i;

	for( i = 0; i < CP_Seg_Array.GetSize(); i++ )
	{
		if( CP_Seg_Array[i].seq.GetLength()/10 < Min_len )	////克隆代码最小行数阈值Min_len
		{
			continue;
		}
		int j;

		for( j = 0; j < CP_Seg_Array[i].CP_Array.GetSize(); j++ )
		{
			CP_SEG &cpSeg1 = CP_Seg_Array[i].CP_Array[j];
			int k;
			for( k = j+1 ; k < CP_Seg_Array[i].CP_Array.GetSize(); k++ )
			{
				CP_SEG &cpSeg2 = CP_Seg_Array[i].CP_Array[k];
				CPPairCntxtIncnsisDtct(filesToTokens,cpSeg1,cpSeg2,CP_Seg_Array[i].seq,i,j,k,headerFilesArry);
			}
		}
	}
}

/*对克隆代码对<SEG1,SEG2>进行上下文不一致性缺陷的检测
	filesToToken:	源文件token行转化表示类
	cpSeg1:			克隆代码片段SEG1
	cpSeg2:			克隆代码片段SEG2
	seq:			频繁子序列
	cpArryIdx:		克隆代码对<SEG1,SEG2>所在的克隆代码集合的序号
	cpSegIdx1:		SEG1在克隆代码集合中的序号
	cpSegIdx2:		SEG2在克隆代码集合中的序号
	headerFilesArry:头文件信息集合
*/
void CntxtInconsisDetect::CPPairCntxtIncnsisDtct(FilesToTokens &filesToTokens,
												 CP_SEG &cpSeg1, 
												 CP_SEG &cpSeg2, 
												 CString seq,
												 int cpArryIdx, 
												 int cpSegIdx1, 
												 int cpSegIdx2,
												 CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry)
{
	TokenedFile tknFile1,tknFile2;//wq-20090505


	/*
	GetTknListForCPSeg:
	返回一个克隆代码片段的token行表示
	filesToTokens:	源文件token行转化表示类
	cpSeg:			克隆代码片段
	headerFilesArry:头文件信息集合
	*/
//	CArray<TokenLine,TokenLine> tknList1,tknList2;
//	if( !GetTknListForCPSeg(tknList1,filesToTokens,cpSeg1)
//		|| !GetTknListForCPSeg(tknList2,filesToTokens,cpSeg2) )
	if( !GetTknListForCPSeg(tknFile1,filesToTokens,cpSeg1,headerFilesArry)
		|| !GetTknListForCPSeg(tknFile2,filesToTokens,cpSeg2,headerFilesArry) )//wq-20090505
	{
		return;
	}

	int i;
	CNTXT_INCNSIS cntxtIncnsis;
	IniCntxtIncnsis(cntxtIncnsis);	/*初始化一个上下文不一致性缺陷记录
									cntxtIncnsis:	一个上下文不一致性缺陷记录
									*/

	cntxtIncnsis.cpArryIdx = cpArryIdx;
	cntxtIncnsis.cpSeg1.cpSegIdx = cpSegIdx1;
	cntxtIncnsis.cpSeg2.cpSegIdx = cpSegIdx2;
	for( i = 0; i < seq.GetLength()/10; i++ )
	{
		/*
		BuildCPSegCntxtInfo:
		获取某一克隆代码行的上下文信息
		cntxtInfo:	一个克隆代码行的上下文信息
		cpSeg:		克隆代码行所在的克隆代码片段
		lnIdx:		克隆代码行数
		tknList:	克隆代码行对应的token行信息
		*/
		BuildCPSegCntxtInfo(cntxtIncnsis.cpSeg1,cpSeg1,i,tknFile1.tokenList);
		BuildCPSegCntxtInfo(cntxtIncnsis.cpSeg2,cpSeg2,i,tknFile2.tokenList);
		

		/*
		CntxtIncnsisJudge:
		判别上下文不一致性缺陷
		cntxtIncnsis:	一个上下文不一致性缺陷记录
		tknFile1:		SEG1对应的token行存储
		tknFile2:		SEG2对应的token行存储
		cpSeg1:			克隆代码片段SEG1
		cpSeg2:			克隆代码片段SEG2
		*/
		CntxtIncnsisJudge(cntxtIncnsis,tknFile1,tknFile2,cpSeg1,cpSeg2);

	}
	

}

//bool CntxtInconsisDetect::GetTknListForCPSeg(CArray<TokenLine,TokenLine> &tknList,FilesToTokens &filesToTokens, CP_SEG &cpSeg)
/*wq-20090520-delete**************
bool CntxtInconsisDetect::GetTknListForCPSeg(TokenedFile &tknFile,FilesToTokens &filesToTokens, CP_SEG &cpSeg)//wq-20090505
{
	tknFile.tokenList.RemoveAll();
	int fileIdx;
	fileIdx = filesToTokens.FindFileName(cpSeg.FN_Hvalue,cpSeg.filename);
	if( fileIdx >= 0 && fileIdx < filesToTokens.tokenFileArray.GetSize())
	{
//		TokenedFile &p_tknFile = filesToTokens.tokenFileArray[fileIdx];
//		tknList.Copy(tknFile.GetTokenList());
		tknFile = filesToTokens.tokenFileArray[fileIdx];//wq-20090505
	}
	if( tknFile.tokenList.GetSize() > 0 )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
/******************************************************/
	/*
	GetTknListForCPSeg:
	返回一个克隆代码片段的token行表示
	filesToTokens:	源文件token行转化表示类
	cpSeg:			克隆代码片段
	headerFilesArry:头文件信息集合
	*/
/*wq-20090520-change**************/
bool CntxtInconsisDetect::GetTknListForCPSeg(TokenedFile &tknFile,
											 FilesToTokens &filesToTokens, 
											 CP_SEG &cpSeg,
											 CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry)//wq-20090505
{
	tknFile.tokenList.RemoveAll();

	CString program;
	ReadCFile(cpSeg.filename,program);		//源程序存入program

	CArray<TNODE,TNODE> TArry;
	CArray<SNODE,SNODE> SArry;
	CArray<FNODE,FNODE> FArry;
	CArray<CERROR,CERROR>   EArry;
	CArray<VIPCS,VIPCS>     VArry;	
	CArray<CH_FILE_INFO,CH_FILE_INFO> filesReadInfo;
	CLexical clc;
	clc(0,program,VArry,EArry,TArry,SArry,FArry,headerFilesArry,cpSeg.filename,filesReadInfo);	

	FormatTokenLines ftls;
	ftls(TArry,clc.srcCode);//语句换行标准化
	FuncBndBuild fbb;
	fbb(TArry);//根据TArry，为每个token字添加所属函数的源程序范围
	filesToTokens.Clean();
	filesToTokens.ChangTArryToTokenLines(TArry,SArry,FArry,cpSeg.filename);
	tknFile = filesToTokens.tokenFileArray[0];

	//############################################################ ww
//	经测试，tknFile中保存的是源程序（去掉了注释，用s代替了字符串，并换行标准化及添加{}）
/*	static int flag=0;
	//if(flag == 0)
	{
		CString str;
		str += cpSeg.filename;
		str += "\n";
		for(int i=0; i<tknFile.tokenList.GetSize(); ++i)
		{
			for(int j=0; j<tknFile.tokenList[i].tokenLine.GetSize(); ++j)
			{
				str += tknFile.tokenList[i].tokenLine[j].srcWord;
			}
			str += "\n";
		}
		CString file;
		file.Format("%d",flag);
		CStdioFile outfile;
		if( outfile.Open("D:\\test\\test"+file+".txt",CFile::modeCreate | CFile::modeWrite))
		{
			outfile.WriteString(str);
		}
		outfile.Close();
	}
	flag++;
*/

	if( tknFile.tokenList.GetSize() > 0 )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
/******************************************************/
		/*
		BuildCPSegCntxtInfo:
		获取某一克隆代码行的上下文信息
		cntxtInfo:	一个克隆代码行的上下文信息
		cpSeg:		克隆代码行所在的克隆代码片段
		lnIdx:		克隆代码行数
		tknList:	克隆代码行对应的token行信息
		*/
void CntxtInconsisDetect::BuildCPSegCntxtInfo( CNTXT_INFO &cntxtInfo, 
											   CP_SEG &cpSeg, 
											   int lnIdx,
											   CArray<TokenLine,TokenLine> &tknList)
{
	int line;
	line = atoi(cpSeg.relaLPOS.Mid(lnIdx*4,4));
	if( line <= 0 )
	{
		return;
	}

	CArray<TNODE,TNODE> &tknLine = tknList[line-1].GetTokenLine();
	if( tknLine.GetSize() <= 0 )
	{
		return;
	}

	cntxtInfo.cpLine = line;//
	int ctxtLine, dowhileLine = -2;
	ctxtLine = tknLine[0].cntxtLine;

	if( tknLine[0].key == CN_IF && ctxtLine > 0 && ctxtLine <= tknList.GetSize())
	{
		CArray<TNODE,TNODE> &ctxtTknLine = tknList[ctxtLine-1].GetTokenLine();
		if( ctxtTknLine.GetSize() > 0 && ctxtTknLine[0].key == CN_ELSE )
		{
			ctxtLine = ctxtTknLine[0].cntxtLine;
		}
	}
	/**wq-20090512**/
	if( ctxtLine>0 && ctxtLine<=tknList.GetSize() )
	{
		CArray<TNODE,TNODE> &ctxtTknLine = tknList[ctxtLine-1].GetTokenLine();
		if( ctxtTknLine.GetSize()>0 && ctxtTknLine[0].key==CN_DO )
		{
			dowhileLine = ctxtTknLine[0].dowhileLine;
		}
	}
	cntxtInfo.doWhileLine = dowhileLine;/************************/

	cntxtInfo.ctxtLine = ctxtLine;//
}

void CntxtInconsisDetect::IniCntxtInfo( CNTXT_INFO &cntxtInfo )
{
	cntxtInfo.cpLine = -1;
	cntxtInfo.cpSegIdx = -1;
	cntxtInfo.ctxtLine = -1;
}

void CntxtInconsisDetect::IniCntxtIncnsis( CNTXT_INCNSIS &cntxtIncnsis )
{
	cntxtIncnsis.cpArryIdx = -1;
	IniCntxtInfo(cntxtIncnsis.cpSeg1);
	IniCntxtInfo(cntxtIncnsis.cpSeg2);
}
/*
void CntxtInconsisDetect::CntxtIncnsisJudge( CNTXT_INCNSIS &cntxtIncnsis,
										     CArray<TokenLine,TokenLine> &tknList1,
											 CArray<TokenLine,TokenLine> &tknList2,
											 CP_SEG &cpSeg1, 
											 CP_SEG &cpSeg2)*/
		/*
		CntxtIncnsisJudge:
		判别上下文不一致性缺陷
		cntxtIncnsis:	一个上下文不一致性缺陷记录
		tknFile1:		SEG1对应的token行存储
		tknFile2:		SEG2对应的token行存储
		cpSeg1:			克隆代码片段SEG1
		cpSeg2:			克隆代码片段SEG2
		*/
void CntxtInconsisDetect::CntxtIncnsisJudge( CNTXT_INCNSIS &cntxtIncnsis,
										     TokenedFile &tknFile1,
											 TokenedFile &tknFile2,
											 CP_SEG &cpSeg1, 
											 CP_SEG &cpSeg2)//wq-20090505
{
	/*wq-20090505*//////////
	CArray<TokenLine,TokenLine> &tknList1 = tknFile1.tokenList;
	CArray<TokenLine,TokenLine> &tknList2 = tknFile2.tokenList;/**/////////////////

	int ctxtLine1 = cntxtIncnsis.cpSeg1.ctxtLine;
	int ctxtLine2 = cntxtIncnsis.cpSeg2.ctxtLine;
	int ctxtType1 = CntxtTypeJudge( ctxtLine1,tknList1 );
	int ctxtType2 = CntxtTypeJudge( ctxtLine2,tknList2 );
	int typeAdd = 0;
	float value;
	
//	if((ctxtType1 == TP_FUNDEF) && (ctxtType2 == TP_FUNDEF))	//ww
//		AfxMessageBox("nihao");

	if( (ctxtType1 == TP_IF|| ctxtType1 == TP_FOR || ctxtType1 == TP_WHILE|| ctxtType1 == TP_DOWHILE)
		&& (ctxtType2 == TP_FUNDEF || ctxtType2 == TP_PROG)
		|| (ctxtType2 == TP_IF || ctxtType2 == TP_FOR || ctxtType2 == TP_WHILE|| ctxtType2 == TP_DOWHILE)
		&& (ctxtType1 == TP_FUNDEF || ctxtType1 == TP_PROG) )
	{//Type1-subtype6
		typeAdd = 1;
	}
	else if( ctxtType1 == TP_IF && (ctxtType2 == TP_FOR || ctxtType2 == TP_WHILE || ctxtType2 == TP_DOWHILE ) )
	{//Type1-subtype3--the context of if is loop
//		if(LoopCntxtJudge(ctxtLine1,tknList1))////wq-20090420
		{
			typeAdd = 1;
		}			
	}
	else if( ctxtType2 == TP_IF && (ctxtType1 == TP_FOR || ctxtType1 == TP_WHILE || ctxtType1 == TP_DOWHILE ) )
	{//Type1-subtype3--the context of if is loop
//		if(LoopCntxtJudge(ctxtLine2,tknList2))////wq-20090420
		{
			typeAdd = 1;
		}			
	}
	else if( (ctxtType1 == TP_FOR || ctxtType1 == TP_WHILE)
		&& ((ctxtType2 == TP_FOR || ctxtType2 == TP_WHILE)))
	{//TYPE2 judge-subtype4_1
		typeAdd = 2;
	}
	else if( ctxtType1 == TP_DOWHILE && ctxtType2 == TP_DOWHILE )
	{//TYPE2 judge-subtype4_2
		typeAdd = 2;
	}
	else if( ctxtType2 == TP_IF && ctxtType2 == TP_IF )
	{//TYPE2 judge-subtype5
		typeAdd = 2;
	}

	if( typeAdd == 1 )
	{
		if(!CntxtIncnsisExisted(cntxtIncnsis,cntxtIncnsisType1))
		{
			cntxtIncnsisType1.Add(cntxtIncnsis);		//##############################
			return;
		}		
	}
	else if( typeAdd == 2 )
	{
		///////////判断这对上下文是否属于克隆代码/////////////
		CString sCtxtLine1,sCtxtLine2;
		if(ctxtType1 == TP_DOWHILE && ctxtType2 == TP_DOWHILE )
		{
			sCtxtLine1.Format("%d",cntxtIncnsis.cpSeg1.doWhileLine);
			sCtxtLine2.Format("%d",cntxtIncnsis.cpSeg2.doWhileLine);
		}
		else
		{
			sCtxtLine1.Format("%d",cntxtIncnsis.cpSeg1.ctxtLine);
			sCtxtLine2.Format("%d",cntxtIncnsis.cpSeg2.ctxtLine);
		}
		int len;
		for( len = sCtxtLine1.GetLength(); len < 4; len++ )
		{
			sCtxtLine1 = "0" + sCtxtLine1;
		}
		for( len = sCtxtLine2.GetLength(); len < 4; len++ )
		{
			sCtxtLine2 = "0" + sCtxtLine2;
		}

		int pos1 = 0, pos2 = 0;
		pos1 = cpSeg1.relaLPOS.Find(sCtxtLine1,pos1);


str = cpSeg1.filename;//ww		
//############################################################ ww
//############################################################
/*	static int flag=0;
	//if(flag == 0)
	{
		CString str;
		str += cpSeg1.filename;
		str += "\n";
		for(int i=0; i<tknFile1.tokenList.GetSize(); ++i)
		{
			for(int j=0; j<tknFile1.tokenList[i].tokenLine.GetSize(); ++j)
			{
				str += tknFile1.tokenList[i].tokenLine[j].srcWord;
			}
			str += "\n";
		}
		str += "\n";
		str += cpSeg1.relaLPOS;
		str += "\n";
		str += sCtxtLine1;
		CString temp;
		temp.Format("%d", pos1);
		str += "\n";
		str += temp;

		CString file;
		file.Format("%d",flag);
		CStdioFile outfile;
		if( outfile.Open("D:\\test\\test"+file+".txt",CFile::modeCreate | CFile::modeWrite))
		{
			outfile.WriteString(str);
		}
		outfile.Close();
	}
	flag++;

*/
		while( pos1 != -1 && pos1%4 == 0 )
		{
			pos2 = cpSeg2.relaLPOS.Find(sCtxtLine2,pos2);
			while( pos2 != -1 && pos2%4 == 0 )
			{
				if( pos1 == pos2 )
				{
					return;
				}
				//pos2 = cpSeg2.relaLPOS.Find(sCtxtLine2,pos2); //ww20090810 error
				pos2 = cpSeg2.relaLPOS.Find(sCtxtLine2, pos2+1);
			}
			//pos1 = cpSeg1.relaLPOS.Find(sCtxtLine1,pos1);	//ww20090810 error
			pos1 = cpSeg1.relaLPOS.Find(sCtxtLine1, pos1+1);
		}
		/**************************************************/
		
		//不属于重复代码，则进行表达式匹配
		if(!CntxtIncnsisExisted(cntxtIncnsis,cntxtIncnsisType2))
		{//如果该缺陷没有被加入缺陷列表中
			value = ExpressionMatch(ctxtLine1,ctxtLine2,tknFile1,tknFile2,ctxtType1,ctxtType2);//wq-20090511
//			if( value >= mchValThrhld && value < 1 )
			cntxtIncnsis.expMatchValue = value;
			if( value >= mchValThrhld && value < 1 )
//			if( value >0 && value <= mchValThrhld   )
			{
				cntxtIncnsisType2.Add(cntxtIncnsis);
			}
			return;
		}
		
	}

}

int CntxtInconsisDetect::CntxtTypeJudge( int ctxtLine, CArray<TokenLine,TokenLine> &tknList )
{
	if( ctxtLine == -1 )
	{
		return TP_PROG;
	}
	else if( ctxtLine > 0 && ctxtLine <= tknList.GetSize() )
	{
		CArray<TNODE,TNODE> &tknLine = tknList[ctxtLine-1].GetTokenLine();
		if( tknLine.GetSize() <= 0 )
		{
			return TP_ERROR;
		}
		int key = tknLine[0].key;
		switch(key)
		{
			case CN_ELSE://wq-20090507
				if( tknLine.GetSize() > 1
					&& tknLine[1].key != CN_IF )
				{
					return TP_ERROR;
				}
			case CN_IF:
				return TP_IF;
			case CN_FOR:
				return TP_FOR;
			case CN_WHILE:
				return TP_WHILE;
			case CN_DO:
				return TP_DOWHILE;
			case CN_SWITCH:
				return TP_SWITCH;
			default:
				if( FindKey(tknLine,CN_DFUNCTION ) )
				{
					return TP_FUNDEF;
				}
				else
				{
					return TP_ERROR;
				}
		}
	}
	else 
	{
		return TP_ERROR;
	}
}
bool CntxtInconsisDetect::FindKey(CArray<TNODE,TNODE> &tknLine, int key)
{
	int i;
	for( i = 0; i < tknLine.GetSize(); i++ )
	{
		if( tknLine[i].key == key )
		{
			return TRUE;
		}
	}
	return FALSE;
}

bool CntxtInconsisDetect::LoopCntxtJudge( int line, CArray<TokenLine,TokenLine> &tknList )
{
	if( line > 0 && line <= tknList.GetSize() )
	{
		CArray<TNODE,TNODE> &tknLine = tknList[line-1].GetTokenLine();
		if( tknLine.GetSize() > 0 )
		{
			int ctxtLine = tknLine[0].cntxtLine;
			if( ctxtLine > 0 && ctxtLine <= tknList.GetSize() )
			{
				CArray<TNODE,TNODE> &ctxtTknLine = tknList[ctxtLine-1].GetTokenLine();
				if( ctxtTknLine.GetSize() > 0 )
				{
					int key = ctxtTknLine[0].key;
					if( key == CN_FOR || key == CN_DO || key == CN_WHILE )
					{
						return TRUE;
					}
				}
			}
		}
	}
	return FALSE;
}
bool CntxtInconsisDetect::CntxtIncnsisExisted(CNTXT_INCNSIS &cntxtIncnsis,CArray<CNTXT_INCNSIS,CNTXT_INCNSIS> &cntxtIncnsisArry)
{
	int i;
	for( i = 0; i < cntxtIncnsisArry.GetSize(); i++ )
	{
		if( CompareCntxtIncnsis(cntxtIncnsis,cntxtIncnsisArry[i]) )
		{
			return TRUE;
		}
	}
	return FALSE;
}
bool CntxtInconsisDetect::CompareCntxtIncnsis( CNTXT_INCNSIS &cntxtIncnsis1, CNTXT_INCNSIS &cntxtIncnsis2)
{
	if( cntxtIncnsis1.cpArryIdx == cntxtIncnsis2.cpArryIdx )
	{
		if( cntxtIncnsis1.cpSeg1.cpSegIdx == cntxtIncnsis2.cpSeg1.cpSegIdx 
			&& cntxtIncnsis1.cpSeg2.cpSegIdx == cntxtIncnsis2.cpSeg2.cpSegIdx )
		{
			if( cntxtIncnsis1.cpSeg1.ctxtLine == cntxtIncnsis2.cpSeg1.ctxtLine 
				&& cntxtIncnsis1.cpSeg2.ctxtLine == cntxtIncnsis2.cpSeg2.ctxtLine)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*
	克隆代码对的两个上下文条件谓词匹配相似度的计算；double返回上下文条件谓词匹配相似度
	ctxtLine1:	上下文1在克隆代码SEG1的token行表示中的行数
	ctxtLine2:	上下文2在克隆代码SEG2的token行表示中的行数
	tknFile1:	克隆代码SEG1的token行表示
	tknFile2:	克隆代码SEG2的token行表示
	ctxtType1:	上下文1的上下文结构类型
	ctxtType2:	上下文2的上下文结构类型
		*利用“机考系统”的系统依赖图节点表达式匹配
			利用机考系统的CSDGBase等与系统依赖图有关的类；
			为C1和C2分别生成系统依赖图节点SDG（C1）和SDG（C2）；
			利用CExpression类为SDG（C1）和SDG（C2）建立表达式语法树并表达式的标准化；
			利用CProgramMatch:;BoolExpressionMatch()函数进行表达式语法树的匹配。
*///wq-20090505
double CntxtInconsisDetect::ExpressionMatch( int ctxtLine1, 
										 	 int ctxtLine2,
										 	 TokenedFile &tknFile1, 
										 	 TokenedFile &tknFile2,
											 int ctxtType1,
											 int ctxtType2 )
{
	double rtnVal = 0.0;
	int value;

	CArray<CERROR,CERROR> EArry;
	CExpression exp;
	CProgramMatch pgMtch;

	if( ctxtLine1 > tknFile1.tokenList.GetSize()
		|| ctxtLine2 > tknFile2.tokenList.GetSize() )
	{
		return double(0);
	}
	CArray<TNODE,TNODE> &tknLine1 = tknFile1.tokenList[ctxtLine1-1].GetTokenLine();
	CArray<TNODE,TNODE> &tknLine2 = tknFile2.tokenList[ctxtLine2-1].GetTokenLine();

//###############################################//ww
//###############################################//
	CString temp;
	temp.Format("ctxtLine1: %d    ctxtLine2: %d     ctxtType1: %d      ctxtType2: %d", ctxtLine1, ctxtLine2, ctxtType1, ctxtType2);
	static int flag=0;
	//if(flag == 0)
	{
		//CString str;
		//str += cpSeg1.filename;
		str += "\n";
		for(int i=0; i<tknFile1.tokenList.GetSize(); ++i)
		{
			for(int j=0; j<tknFile1.tokenList[i].tokenLine.GetSize(); ++j)
			{
				str += tknFile1.tokenList[i].tokenLine[j].srcWord;
				str += " ";
			}
			str += "\n";
		}

		str += "\n";
		for(int ii=0; ii<tknLine1.GetSize(); ++ii)
		{
			str += tknLine1[ii].srcWord;
			str += " ";
		}
		str += "\n";
		str += temp;
	
		CString file;
		file.Format("%d",flag);
		CStdioFile outfile;
		if( outfile.Open("D:\\test\\test"+file+".txt",CFile::modeCreate | CFile::modeWrite))
		{
			outfile.WriteString(str);
		}
		outfile.Close();
	}
	flag++;


	if( ctxtType1 == TP_IF && ctxtType2 == TP_IF )
	{
		CSDGIf *pIf1=new CSDGIf;
		CSDGIf *pIf2=new CSDGIf;
		if( IfStatementSDG(pIf1,tknFile1,ctxtLine1)		
			&& IfStatementSDG(pIf2,tknFile2,ctxtLine2) )//为if语句构造SDG结点和表达式语法树
		{//AfxMessageBox("1");//ww ww
			exp(pIf1,tknFile1.SArry,EArry);//translate the expression in list to syntax tree
			exp(pIf2,tknFile2.SArry,EArry);
			value = pgMtch.BoolExpressionMatch(pIf1->m_pinfo,pIf2->m_pinfo,tknFile1.SArry,tknFile2.SArry);
//			ExpMatchVectorElm expMV1,expMV2;
//			GenerateExpVector(pIf1->TCnt,expMV1);
//			GenerateExpVector(pIf2->TCnt,expMV2);
//			rtnVal = ComputeVectorsSimilarity(expMV1,expMV2);

		}
		delete pIf1;//ww
		delete pIf2;//ww
	}
	else if( ctxtType1 == TP_DOWHILE && ctxtType2 == TP_DOWHILE )
	{
		CSDGDoWhile *pDo1=new CSDGDoWhile;
		CSDGDoWhile *pDo2=new CSDGDoWhile;
		if( DoWhileStatementSDG(pDo1,tknFile1,ctxtLine1) 
			&& DoWhileStatementSDG(pDo2,tknFile2,ctxtLine2) )
		{//AfxMessageBox("2");//ww ww
			exp(pDo1,tknFile1.SArry,EArry);
			exp(pDo2,tknFile2.SArry,EArry);
			value = pgMtch.BoolExpressionMatch(pDo1->m_pinfo,pDo2->m_pinfo,tknFile1.SArry,tknFile2.SArry);
//			ExpMatchVectorElm expMV1,expMV2;
//			GenerateExpVector(pDo1->TCnt,expMV1);
//			GenerateExpVector(pDo2->TCnt,expMV2);
//			rtnVal = ComputeVectorsSimilarity(expMV1,expMV2);
		}
		delete pDo1;//ww
		delete pDo2;//ww
	}
	else if( ctxtType1 == TP_FOR )
	{
		CSDGFor *pFor1=new CSDGFor;
		if( ForStatementSDG(pFor1,tknFile1,ctxtLine1) )
		{
			if( ctxtType2 == TP_FOR )
			{//AfxMessageBox("3");//ww ww
				CSDGFor *pFor2=new CSDGFor;
				if( ForStatementSDG(pFor2,tknFile2,ctxtLine2) )
				{
					exp(pFor1,tknFile1.SArry,EArry);
					exp(pFor2,tknFile2.SArry,EArry);
					value = pgMtch.BoolExpressionMatch(pFor1->m_pinfo,pFor2->m_pinfo,tknFile1.SArry,tknFile2.SArry);
//					ExpMatchVectorElm expMV1,expMV2;
//					GenerateExpVector(pFor1->TCnt,expMV1);
//					GenerateExpVector(pFor2->TCnt,expMV2);
//					rtnVal = ComputeVectorsSimilarity(expMV1,expMV2);
				}
				delete pFor2;//ww
			}
			else if( ctxtType2 == TP_WHILE )
			{//AfxMessageBox("4");//ww
				CSDGWhile *pWhile2=new CSDGWhile;
				if( WhileStatementSDG(pWhile2,tknFile2,ctxtLine2) )
				{
					exp(pFor1,tknFile1.SArry,EArry);
					exp(pWhile2,tknFile2.SArry,EArry);
					value = pgMtch.BoolExpressionMatch(pFor1->m_pinfo,pWhile2->m_pinfo,tknFile1.SArry,tknFile2.SArry);
//					ExpMatchVectorElm expMV1,expMV2;
//					GenerateExpVector(pFor1->TCnt,expMV1);
//					GenerateExpVector(pWhile2->TCnt,expMV2);
//					rtnVal = ComputeVectorsSimilarity(expMV1,expMV2);
				}
				delete pWhile2;//ww
			}
		}
		delete pFor1;//ww
	}
	else if( ctxtType1 == TP_WHILE )
	{
		CSDGWhile *pWhile1=new CSDGWhile;
		if( WhileStatementSDG(pWhile1,tknFile1,ctxtLine1) )
		{
			if( ctxtType2 == TP_FOR )
			{//AfxMessageBox("5");//ww
				CSDGFor *pFor2=new CSDGFor;
				if( ForStatementSDG(pFor2,tknFile2,ctxtLine2) )
				{
					exp(pWhile1,tknFile1.SArry,EArry);
					exp(pFor2,tknFile2.SArry,EArry);
					value = pgMtch.BoolExpressionMatch(pWhile1->m_pinfo,pFor2->m_pinfo,tknFile1.SArry,tknFile2.SArry);
//					ExpMatchVectorElm expMV1,expMV2;
//					GenerateExpVector(pWhile1->TCnt,expMV1);
//					GenerateExpVector(pFor2->TCnt,expMV2);
//					rtnVal = ComputeVectorsSimilarity(expMV1,expMV2);
				}
				delete pFor2;//ww
			}
			else if( ctxtType2 == TP_WHILE )
			{//AfxMessageBox("6");//ww ww
				CSDGWhile *pWhile2=new CSDGWhile;
				if( WhileStatementSDG(pWhile2,tknFile2,ctxtLine2) )
				{
					exp(pWhile1,tknFile1.SArry,EArry);
					exp(pWhile2,tknFile2.SArry,EArry);
					value = pgMtch.BoolExpressionMatch(pWhile1->m_pinfo,pWhile2->m_pinfo,tknFile1.SArry,tknFile2.SArry);
//					ExpMatchVectorElm expMV1,expMV2;
//					GenerateExpVector(pWhile1->TCnt,expMV1);
//					GenerateExpVector(pWhile2->TCnt,expMV2);
//					rtnVal = ComputeVectorsSimilarity(expMV1,expMV2);
				}
				delete pWhile2;//ww
			}
		}
		delete pWhile1;//ww
	}

	rtnVal = double(value)/1000;
	return rtnVal;

}


//wq-20090507-为if语句构造SDG结点和表达式语法树
bool CntxtInconsisDetect::IfStatementSDG(CSDGIf *pIf, TokenedFile &tknFile, int ctxtLine)
{
	if( ctxtLine > tknFile.tokenList.GetSize() )
	{
		return false;
	}
	CArray<TNODE,TNODE> &tknLine = tknFile.tokenList[ctxtLine-1].GetTokenLine();
	pIf->SetNodeID(CN_IFYJ);//set node type, labeling the statement type.
	pIf->IFPOS=ctxtLine;//if语句所在行
	if( FindIfExpBody(pIf,tknLine) )
	{
		int i;
		for( i = pIf->expbeg; i<tknLine.GetSize() && i<pIf->expend; i++ )
		{
			pIf->TCnt.Add(tknLine[i]);	//CArray<TNODE,TNODE> TCnt;
		}
		return true;
	}

	return false;
}

bool CntxtInconsisDetect::FindIfExpBody(CSDGIf *pIf,CArray<TNODE,TNODE> &tknLine)
{
	int crclFlag = 0;
	int crclMatch = 0;
	int i;
	for( i = 0; i < tknLine.GetSize(); i++ )
	{
		int key = tknLine[i].key;
		if( key == CN_LCIRCLE)		// (
		{
			if(crclFlag == 0 )
			{
				pIf->expbeg = i+1;
				crclFlag = 1;
			}
			crclMatch++;
		}
		if( key == CN_RCIRCLE )		// )
		{
			crclMatch--;
			if( crclFlag == 1 && crclMatch == 0 )
			{
				pIf->expend = i;	//AfxMessageBox("true");//ww
				return true;	
			}
		}
	}	//AfxMessageBox("false");//ww
	return false;
}

bool CntxtInconsisDetect::DoWhileStatementSDG(CSDGDoWhile *pDo, TokenedFile &tknFile, int ctxtLine)
{
	if( ctxtLine > tknFile.tokenList.GetSize() )
	{
		return false;
	}
	CArray<TNODE,TNODE> &tknLine = tknFile.tokenList[ctxtLine-1].GetTokenLine();
    pDo->SetNodeID(CN_DOYJ);
	pDo->DPOS = ctxtLine;

	if(tknLine.GetSize()<=0)return false;
	int wLine = tknLine[0].dowhileLine;
	if( wLine <= tknFile.tokenList.GetSize() )
	{
		if( wLine > tknFile.tokenList.GetSize() )
		{
			return false;
		}

		CArray<TNODE,TNODE> &whTknLine = tknFile.tokenList[wLine-1].GetTokenLine();
		if( FindDoWhileExpBody(pDo,whTknLine) )
		{
			int i;
			for( i = pDo->expbeg; i<whTknLine.GetSize() && i<pDo->expend; i++ )
			{
				pDo->TCnt.Add(whTknLine[i]);
			}
			return true;
		}
	}
	
	return false;
}

bool CntxtInconsisDetect::FindDoWhileExpBody(CSDGDoWhile *pDo,CArray<TNODE,TNODE> &tknLine)
{
	int crclFlag = 0;
	int crclMatch = 0;
	int i;
	for( i = 0; i < tknLine.GetSize(); i++ )
	{
		int key = tknLine[i].key;
		if( key == CN_LCIRCLE)
		{
			if(crclFlag == 0 )
			{
				pDo->expbeg = i+1;
				crclFlag = 1;
			}
			crclMatch++;
		}
		if( key == CN_RCIRCLE )
		{
			crclMatch--;
			if( crclFlag == 1 && crclMatch == 0 )
			{
				pDo->expend = i;
				return true;
			}
		}
	}

	return false;
}

bool CntxtInconsisDetect::WhileStatementSDG(CSDGWhile *pWhile, TokenedFile &tknFile, int ctxtLine)
{
	if( ctxtLine > tknFile.tokenList.GetSize() )
	{
		return false;
	}

	CArray<TNODE,TNODE> &tknLine = tknFile.tokenList[ctxtLine-1].GetTokenLine();
	pWhile->SetNodeID(CN_IFYJ);
	pWhile->WPOS=ctxtLine;//while语句所在行
	if( FindWhileExpBody(pWhile,tknLine) )
	{
		int i;
		for( i = pWhile->expbeg; i<tknLine.GetSize() && i<pWhile->expend; i++ )
		{
			pWhile->TCnt.Add(tknLine[i]);
		}
		return true;
	}

	return false;
}

bool CntxtInconsisDetect::FindWhileExpBody(CSDGWhile *pWhile,CArray<TNODE,TNODE> &tknLine)
{
	int crclFlag = 0;
	int crclMatch = 0;
	int i;
	for( i = 0; i < tknLine.GetSize(); i++ )
	{
		int key = tknLine[i].key;
		if( key == CN_LCIRCLE)
		{
			if(crclFlag == 0 )
			{
				pWhile->expbeg = i+1;
				crclFlag = 1;
			}
			crclMatch++;
		}
		if( key == CN_RCIRCLE )
		{
			crclMatch--;
			if( crclFlag == 1 && crclMatch == 0 )
			{
				pWhile->expend = i;
				return true;
			}
		}
	}

	return false;

}

bool CntxtInconsisDetect::ForStatementSDG(CSDGFor *pFor, TokenedFile &tknFile, int ctxtLine)
{
	if( ctxtLine > tknFile.tokenList.GetSize() )
	{
		return false;
	}
	CArray<TNODE,TNODE> &tknLine = tknFile.tokenList[ctxtLine-1].GetTokenLine();
	pFor->SetNodeID(CN_FORYJ);
	pFor->FORPOS = ctxtLine;
	if( FindForExpBody(pFor,tknLine) )
	{
		int i;
		for( i=pFor->expbeg+1; i<tknLine.GetSize() && i<pFor->DOUPOS1; i++ )
		{
			pFor->TCnt1.Add(tknLine[i]);
		}
		for( i=pFor->DOUPOS1+1; i<tknLine.GetSize() && i<pFor->DOUPOS2; i++ )
		{
			pFor->TCnt2.Add(tknLine[i]);
		}
		for( i=pFor->DOUPOS2+1; i<tknLine.GetSize() && i<pFor->expend; i++ )
		{
			pFor->TCnt3.Add(tknLine[i]);
		}
		pFor->TCnt.Copy(pFor->TCnt2);

		return true;
	}

	return false;
}

bool CntxtInconsisDetect::FindForExpBody(CSDGFor *pFor,CArray<TNODE,TNODE> &tknLine)
{
	int crclFlag = 0;
	int crclMatch = 0;
	int lineCnt = 0;
	int i;
	for( i = 0; i < tknLine.GetSize(); i++ )
	{
		int key = tknLine[i].key;
		if( key == CN_LCIRCLE)
		{
			if(crclFlag == 0 )
			{
				pFor->expbeg = i;
				crclFlag = 1;
			}
			crclMatch++;
		}
		else if( key == CN_RCIRCLE )
		{
			crclMatch--;
			if( crclFlag == 1 && crclMatch == 0 )
			{
				pFor->expend = i;
				return true;
			}
		}
		else if( key == CN_LINE)
		{
			if(lineCnt == 0 && crclMatch != 0 )
			{
				pFor->DOUPOS1 = i;
				lineCnt++;
			}
			else if( lineCnt == 1 && crclMatch != 0 )
			{
				pFor->DOUPOS2 = i;
				lineCnt++;
			}
		}
	}

	return false;
}


bool CntxtInconsisDetect::ReadCFile(const CString cfilename, CString &program)
{
	CStdioFile file;
	CString str=_T("");
	
	if( file.Open(cfilename,CFile::modeRead|CFile::typeText) == 0 )
	{
	   //str="读文件"+cfilename+"失败!\n\n"+"请检查是否已创建该文件！";
	   //AfxMessageBox(str);
	   return 0;
	}
	else
	{  
		str=_T("");
	    program=_T("");
		while(file.ReadString(str))
		{
			program=program+str+"\n";
			str=_T("");
		}
		file.Close();
		return 1;
	}
}

double CntxtInconsisDetect::GetMatchVectorElmType(int key,int &vecPos)
{
	vecPos = -1;
	double rtnVal = 0;
	switch(key)
	{
	case CN_VARIABLE: 
		rtnVal = 1.5;
		vecPos = 0;//variable
		return rtnVal;
	case CN_ARRAY:    
		rtnVal = 1.3;
		vecPos = 0;//variable
		return rtnVal;
	case CN_POINTER:  
		rtnVal = 1.1;
		vecPos = 0;//variable
		return rtnVal;
	case CN_ARRPTR:  
		rtnVal = 0.9;
		vecPos = 0;//variable
		return rtnVal;
	case CN_STRING:   
		rtnVal = 0.7;
		vecPos = 0;//variable
		return rtnVal;

	case CN_CINT:
	case CN_CLONG:
	case CN_CFLOAT:
	case CN_CDOUBLE:
		rtnVal = 1;
		vecPos = 1;//const figure
		return rtnVal;

	case CN_CCHAR:
		rtnVal = 1;
		vecPos = 2;//const char
		return rtnVal;
	case CN_CSTRING:
		rtnVal = 1;
		vecPos = 3;//const string
		return rtnVal;

	case CN_GREATT:       // >
	case CN_LESS:         // <
		rtnVal = 0.7;
		vecPos = 4;//relation operator
		return rtnVal;
	case CN_LANDE:        // <= 
	case CN_BANDE:        // >=
		rtnVal = 0.9;
		vecPos = 4;//relation operator
		return rtnVal;
	case CN_NOTEQUAL:     // !=
	case CN_DEQUAL:       // == 
		rtnVal = 1.1;
		vecPos = 4;//relation operator
		return rtnVal;

	case CN_DAND:         // &&
		rtnVal = 0.7;
		vecPos = 5;//logical operator
		return rtnVal;
	case CN_DOR:          // ||
		rtnVal = 0.9;
		vecPos = 5;//logical operator
		return rtnVal;
	case CN_NOT:          // !
		rtnVal = 1.1;
		vecPos = 5;//logical operator
		return rtnVal;

	case CN_ADD:          // +
		rtnVal = 0.7;
		vecPos = 6;//math operator
		return rtnVal;
	case CN_SUB:          // -
		rtnVal = 0.9;
		vecPos = 6;//math operator
		return rtnVal;
	case CN_MULTI:        // *
		rtnVal = 1.1;
		vecPos = 6;//math operator
		return rtnVal;
	case CN_DIV:          // / 
		rtnVal = 1.3;
		vecPos = 6;//math operator
		return rtnVal;
	case CN_FORMAT:       // %
		rtnVal = 1.5;
		vecPos = 6;//math operator
		return rtnVal;

	case CN_BFUNCTION: 
		rtnVal = 1;
		vecPos = 7;//C库函数调用

	case CN_DFUNCTION:
		rtnVal = 1;
		vecPos = 8;//自定义函数调用

	default:
		rtnVal = 0;
		vecPos = -1;
		return rtnVal;

	}


}

bool CntxtInconsisDetect::GenerateExpVector(CArray<TNODE,TNODE> &expTArry,ExpMatchVectorElm &expMV)
{

	int vecPos = -1;
	double vecElmVal = 0;
	int i;
	for(i=0;i<expTArry.GetSize();i++)
	{
		int key = expTArry[i].key;
		vecElmVal = GetMatchVectorElmType(key,vecPos);
		switch(vecPos)
		{
		case 0:
			expMV.var+=vecElmVal;
			break;
		case 1:
			expMV.cstf+=vecElmVal;
			break;
		case 2:
			expMV.cstc+=vecElmVal;
			break;
		case 3:
			expMV.csts+=vecElmVal;
			break;
		case 4:
			expMV.rlt+=vecElmVal;
			break;
		case 5:
			expMV.lgc+=vecElmVal;
			break;
		case 6:
			expMV.mth+=vecElmVal;
			break;
		case 7:
			expMV.bfnc+=vecElmVal;
			expMV.bfncPara += ComputeFuncParaNum(i,expTArry);
			break;
		case 8:
			expMV.dfnc+=vecElmVal;
			expMV.dfncPara += ComputeFuncParaNum(i,expTArry);
			break;
		default:
			break;
		}
	}
	return false;
}

int CntxtInconsisDetect::ComputeFuncParaNum(int &idx,CArray<TNODE,TNODE> &expTArry)
{
	int paraNum = 0;
	int key = expTArry[idx].key;
	if( key == CN_BFUNCTION || key == CN_DFUNCTION )
	{
		idx++;
		int lcrl = 0,rcrl = 0;
		while( idx < expTArry.GetSize() )
		{
			key = expTArry[idx].key;
			if( key == CN_LCIRCLE )
			{
				lcrl++;
			}
			else if( key == CN_RCIRCLE )
			{
				rcrl++;
				if( lcrl==rcrl && lcrl!=0 )
				{
					return paraNum;
				}
				
			}
			else if( key != CN_DOU )
			{
				paraNum++;
			}
			idx++;
		}
	}
	else
	{//error
		paraNum = -1;
	}
	return paraNum;

}

double CntxtInconsisDetect::ComputeVectorsSimilarity(ExpMatchVectorElm &expMV1, ExpMatchVectorElm &expMV2)
{
	double value = 0;
	double avgDifQu[11];
	int i;
	for( i=0; i<11; i++ )
	{
		avgDifQu[i] = 0;
	}
//	if(expMV1.var!=double(0) || expMV2.var!=double(0))
//		avgDifQu[0]= pow(double(expMV1.var-expMV2.var)/( (expMV1.var>expMV2.var)?expMV1.var:expMV2.var ),2);
		avgDifQu[0]= fabs(double(expMV1.var-expMV2.var));
//	if(expMV1.cstf!=double(0) || expMV2.cstf!=double(0))
//		avgDifQu[1]= pow(double(expMV1.cstf-expMV2.cstf)/( (expMV1.cstf>expMV2.cstf)?expMV1.cstf:expMV2.cstf ),2);
		avgDifQu[1]= fabs(double(expMV1.cstf-expMV2.cstf));
//	if(expMV1.cstc!=double(0) || expMV2.cstc!=double(0))
//		avgDifQu[2]= pow(double(expMV1.cstc-expMV2.cstc)/( (expMV1.cstc>expMV2.cstc)?expMV1.cstc:expMV2.cstc ),2);
		avgDifQu[2]= fabs(double(expMV1.cstc-expMV2.cstc));
//	if(expMV1.csts!=double(0) || expMV2.csts!=double(0))
//		avgDifQu[3]= pow(double(expMV1.csts-expMV2.csts)/( (expMV1.csts>expMV2.csts)?expMV1.csts:expMV2.csts ),2);
		avgDifQu[3]= fabs(double(expMV1.csts-expMV2.csts));
//	if(expMV1.rlt!=double(0) || expMV2.rlt!=double(0))
//		avgDifQu[4]= pow(double(expMV1.rlt-expMV2.rlt)/( (expMV1.rlt>expMV2.rlt)?expMV1.rlt:expMV2.rlt ),2);
		avgDifQu[4]= fabs(double(expMV1.rlt-expMV2.rlt));
//	if(expMV1.lgc!=double(0) || expMV2.lgc!=double(0))
//		avgDifQu[5]= pow(double(expMV1.lgc-expMV2.lgc)/( (expMV1.lgc>expMV2.lgc)?expMV1.lgc:expMV2.lgc ),2);
		avgDifQu[5]= fabs(double(expMV1.lgc-expMV2.lgc));
//	if(expMV1.mth!=double(0) || expMV2.mth!=double(0))
//		avgDifQu[6]= pow(double(expMV1.mth-expMV2.mth)/( (expMV1.mth>expMV2.mth)?expMV1.mth:expMV2.mth ),2);
		avgDifQu[6]= fabs(double(expMV1.mth-expMV2.mth));
//	if(expMV1.bfnc!=double(0) || expMV2.bfnc!=double(0))
//		avgDifQu[7]= pow(double(expMV1.bfnc-expMV2.bfnc)/( (expMV1.bfnc>expMV2.bfnc)?expMV1.bfnc:expMV2.bfnc ),2);
		avgDifQu[7]= fabs(double(expMV1.bfnc-expMV2.bfnc));
//	if(expMV1.dfnc!=double(0) || expMV2.dfnc!=double(0))
//		avgDifQu[8]= pow(double(expMV1.dfnc-expMV2.dfnc)/( (expMV1.dfnc>expMV2.dfnc)?expMV1.dfnc:expMV2.dfnc ),2);
		avgDifQu[8]= fabs(double(expMV1.dfnc-expMV2.dfnc));
//	if(expMV1.bfncPara!=double(0) || expMV2.bfncPara!=double(0))
//		avgDifQu[9]= pow(double(expMV1.bfncPara-expMV2.bfncPara)/( (expMV1.bfncPara>expMV2.bfncPara)?expMV1.bfncPara:expMV2.bfncPara ),2);
		avgDifQu[9]= fabs(double(expMV1.bfncPara-expMV2.bfncPara));
//	if(expMV1.dfncPara!=double(0) || expMV2.dfncPara!=double(0))
//		avgDifQu[10]=pow(double(expMV1.dfncPara-expMV2.dfncPara)/( (expMV1.dfncPara>expMV2.dfncPara)?expMV1.dfncPara:expMV2.dfncPara ),2) ;
		avgDifQu[10]=fabs(double(expMV1.dfncPara-expMV2.dfncPara));
	for( i =0; i<11; i++ )
	{
		value += avgDifQu[i];
	}
	value = double(value)/11;
	return value;
}
