// dataStruct.h: interface for the CdataStruct class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DATASTRUCT_H
#define DATASTRUCT_H
#include "CP_SEG.h"
#include "TNODE.h"
//#include <windows.h>
//#include <stdio.h>
#include "psapi.h"
/*************************************************
// data struct of token chain node
typedef struct
{
	int     key;       //  code which defined in "constdata.h"  
    int     addr;      //  address in symbol table if it is variable of function
	int     paddr;     //  pointer address
	int     deref;//wtt//2006.3.21//指针脱引用*级别

	int     line;      //  text line the token word in    
	double  value;     //  value of const data 
	CString name;      //  name of token word if it is string
	int     srcLine;  //  王倩添加-20080729-记录语句换行标准化后的语句相对行数
	CString srcWord;   //  王倩添加-20080729-记录源程序中token字对应的单词

	int funcBegLine;   //wq-20081220-所在函数的源程序起始行数
	int funcEndLine;   //wq-20081220-所在函数的源程序终止行数

}TNODE;//token节点
/*王倩20081113*/
void TNODECopy(const TNODE &srcTNODE, TNODE &dstTNODE);
/**************************************************************/	

// 单项选择题结构
typedef struct{
	bool  banswered;
	int   answer;
	double weight;
}S_NODE;

// 多项选择题结构
typedef struct{
	bool    banswered;
	CString answer;
	double   weight;
}M_NODE;

// 判断题 
typedef struct{
	bool    banswered;
	int     ianswer;
	CString stranswer;	
	double   weight;
}D_NODE;

// 填空题结构
typedef struct{
	bool    banswered;
	int     vacnum;
    CString arryanswer[5];
	double  weight;
}F_NODE;

// 简答题结构
typedef struct{
	bool    banswered;
	CString answer;
	double   weight;
}A_NODE;

// 编程题结构
typedef struct{
	bool    banswered;
	CString arryanswer[10];
	CString answer;
	int     nlexerror;
	double  weight;
}P_NODE;
// if the program type is "filling vacancys" this struct node labels the
// position of VIP words 

typedef struct
{
	int beg;
	int end;
}VIPCS;
/////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	int index;
	CString Item;
	int Subseq_Addr;
	int Seq_ID;
}Temp_Seq_Node;//Seq_Mining类Find_Freqitems函数中用到的暂存项（Items）信息的节点

struct Ds_NODE{
	int Subseq_Addr;  //该序列的后缀序列在原数据库序列里的偏移量
	int Seq_ID;     //该序列在序列数据库D中的索引
};//用于存储Ds中序列的结点（Ds：s的后继序列数据库，据此可生成该序列的后缀序列）

struct Ds_Hash_Node{
	int Ds_Size;
	float SupID_Sum;
	class Latt_Node *LNode;
};//基于Ds数据库大小创建的hash表的结点

typedef struct
{
	int beg;			//起始位置在TArry中的下标
	int end;			//终止位置在TArry中的下标
	int F_count;        //记录'}'的个数减去'{'的个数
	int layer;			//模块所在的嵌套层
	int Blength;		//模块所占的行数
}FL_NODE;//LX：用于记录源程序每个模块的分块信息
/**王倩20081113-del**
typedef struct 
{
	int B_index;
	int beg;			//对应源程序的起始行数
	int end;			//对应源程序的终止行数
	CString cfilepath;	//对应源程序的路径
	int qsize;			//包含子序列的个数
	CString Sequence;	//一行数字序列
	CString Line_Pos;   //源程序每一行的位置是一个整数，将每个整数存于一个4位字符串中，然后穿起来
	CString str_tokenIndex; //wq-20080924//每行程序在TArry中对应的起止位置，格式为(beg1,end1)(beg2,end2)....
	CString str_srcLine;//wq-序列对应的源程序
}Seq_NODE;//LX：用于存放生成的每块数字序列
/****/




typedef struct 
{
	CString filename;//代码片段所在的文件名
	bool Line_Mark[5000];//如果第i行为重复代码，则Line_Mark[i]行置1
}CP_POS;//记录每行代码是否为重复代码的节点

typedef struct
{
	CP_SEG *S1;
	CP_SEG *S2;
}Con_SEG;
///////////////////////////////////////////////////////////////////////////////////////////


//王倩添加-20080729-记录源程序中每行代码，代码实际行数，和换行标准化后的相对行数
typedef struct
{
	CString str_srcLine;  //  srcline行对应的源代码
	int     srcline;      //  代码实际行数
	int     codeline;     //  换行标准化后的相对行数
}SRCLINE;
/*
// data struct of symbol table element
typedef struct
{
	int     type;       // such as "int" "float" "long"...
	int     kind;       // variable kind(common variable, pointer or array) 
	int     arrdim;     // the dimension number of array
	int     arrsize[4]; // the dimension of array is not beyond 4;
	int     addr;       // address of the variable (no use in this system)
	int	    layer;      // layer number of the variable(line out which function it's in)
	double  value;      // value of float,double variable
	CString name;	// variable name
	int     initaddr;	//用于在函数内联变量重命名时记录原来的addr
	int     taddr;   //用于表示SNODE结点在Token中的位置
	int     sv;	    //wn// point to the id of Struct type,-1 if initiate
	int 	mv;		//wn//point to the id of struct type if member variable is struct type,-2 if initiate,-1 if common member var(not struct type)
}SNODE;//符号表中元素


// data struct of function list node
typedef struct
{
	int     fid;        // id of this function
	int     rtype;      // return type  
	int     rkind;      // return kind 
	int     pnum;       // parameter number
	int     pbeg;       // begnning position(as a token word in token list) of parameter 
	int     pend;       // end....  
	int     plist[20];  // parameter list(stores the parameter symbol table address)
	int     bbeg;       // function body beginning position
	int     bend;       // ....          end   
    int     calleelist[20];// the fid of its callees
	int     calleenum;// the number of its callees//wtt/////
	int     callednum;// the number of called by all its caller///wtt///
	CString name;       // name of this function
	int expanded;
}FNODE;//函数列表节点
*/
// data struct of syntax tree node
struct ENODE{
	int   info;
//	int deref;//wtt//2006.3.21//指针脱引用*级别
	TNODE T;
	struct ENODE *pleft;
	struct ENODE *pright;
	struct ENODE *pinfo[10];
};
typedef struct ENODE ENODE;

// data struct of syntax stack element
typedef struct{
	int   info;
	TNODE T;
	ENODE *pEN;
}SSTACK;

// data struct of error
typedef struct{
	int     ID;        // error id
	int     line;
	float   weight;
	CString sdetail;  // error detail
}CERROR;

// variable frequency
typedef struct
{
	int   addr;
	int   type;
	int   vcount;
	float vfrequency;
	int sv;		//the same to sv of TNODE
	int mv;		//the same to mv of TNODE
	CString oldname;
	CString newname;
}FREQ;

struct FCNODE 
{//函数调用树中的节点
	int id;//节点编号
	FCNODE*children[20];//子节点
	FCNODE*father;//父节点
	int addr;//在函数列表中的下标
};
struct FlowNODE
{//建立流依赖时使用的二元组结构
	int varaddr;//变量在符号表中的下标
	int SDGID;//SDG节点的编号

};

struct AddrNODE
{//用于变量重命名
	int initaddr;//变量在重名命前在符号表中的地址
	int curaddr;//变量在重名命后在符号表中的地址
};

/******************wq-20081115-函数范围信息*******************/
struct FUNC_RANG{
	int srcBegLine;//wq-20081220-所在函数的源程序起始行数
	int srcEndLine;//wq-20081220-所在函数的源程序终止行数
};
/*********************************************************************/
struct TKN_LINK_NODE;
struct FLW_INFO{
	TKN_LINK_NODE *flwTknLink;//指向花括号的token节点
	int seqIndex;//所在的序列号
};
/***************************王倩20081113-add***************************/
struct TKN_LINK_NODE{
	TNODE tkNode;
	int m_itemNum;//记录该token字所在程序行对应的项在序列中的位置（从0开始）
	TKN_LINK_NODE *next;
	FLW_INFO lFlower;//直接外层"{"的信息
	FLW_INFO rFlower;//直接外层"}"的信息
	int layer;//所在层次，最外层为0;
};
/**********************************************************************/
void IniFlwInfoNode(FLW_INFO &flwInfo);
void AssgnFlwInfoNode(FLW_INFO &flwInfo,TKN_LINK_NODE *flwTknLink,int seqIndex);
void CopyFlwInfoNode(FLW_INFO &dst, const FLW_INFO &src);

/*****************************************/
//wq-20090227-标识符不一致性缺陷定位
struct BUG_POSITION
{
	int CPPairIndex;//重复代码对号
	int idMapDirct;//标识符映射方向(1to2或2to1)
	int idIndex;//标识符位置；存在缺陷的标识符是所在克隆代码片段中第idIndex个出现的不重复的标识符
};
/****************************************/

struct CH_FILE_INFO
{//wq-20090610-C文件和包含的头文件数量，处理时间等信息
	int hFileSum;//包含的互补重复的头文件的总数
	int prcss_hFileSum;//处理的头文件的总数
	float cfilePrcssTime;//处理一个C文件的总时间(ms)
};
struct HFILE_NAME_INCLUDECNT
{//记录处理过的头文件名称和其所包含的不重复头文件的数量
	CString fileName;
	int hFileSum;
};

struct ALIAS//wq-20090719-添加wtt的指针修改
{//指针指向信息三元组
	ENODE* psrc;//源变量（由于可能是数组或指针要记下标因此用了ENODE而不是TNODE）
	ENODE* ptar;//目标变量
	int isdefinit;//是否是必然指向，是1，否0
};

//wq-20090402-获取当前进程占用的内存
CString GetMemoryInfo();//DWORD processID )
void OutputMemoryRecord(CString className, CString funcName, CString outPath, int timeClean, CString funcInfo);

#endif // !defined(AFX_DATASTRUCT_H__4573E9A5_EFE9_4C3A_9890_3EF3A7533C54__INCLUDED_)
