#pragma once
#include "rc_type_tchart.h"


// CCCLinesFileDlg 对话框

class CCCLinesFileDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCCLinesFileDlg)

public:
	CCCLinesFileDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCCLinesFileDlg();

// 对话框数据
	enum { IDD = IDD_CC_LINES_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart m_cc_lines_file_chart;
	void ClearAllSeries();
};
