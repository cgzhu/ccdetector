
// MainFrm.h : interface of the CMainFrame class
//

#pragma once
#include "FileView.h"
#include "ClassView.h"
#include "OutputWnd.h"
#include "PropertiesWnd.h"
#include "Out_CpPos.h"
#include "SaveCPOUTDlg.h"
#include "FileOutRootDlg.h"
#include "Out_TokenSeq.h"
#include "direct.h"

//Polaris-20140711
#include "CFileView.h"
#include "resource.h"
#include "MySciEditView.h"
#include "ansi.h"
#include "bug_list.h"
using namespace std;

//克隆代码检测结果输出目录
#define outDir "D:\\Detector_Out"

extern int main_count_ideoper;
extern int main_count_reduassign;
extern int main_count_deadcode;
extern int main_count_reducond;
extern int main_count_hideoper;

//Polaris-20150302
extern int ccGroupGraph[100][100];	
extern int ccGroupNumber;

class CMainFrame : public CMDIFrameWndEx
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:
	CString outFilePath;
	int Min_len;//wq-20090310
	Out_CpPos Ocp;		//用在“打开输入文件”中，用到Out_CpPos类
	CArray<CH_FILE_INFO,CH_FILE_INFO> filesReadInfo;	//CH_FILE_INFO：文件和包含的头文件数量，处理时间等信息。		用在“打开输入文件”中
	CArray<HeaderFileElem,HeaderFileElem> headerFilesArry;//wq-20090522-存储头文件处理信息。		用在“打开输入文件”中
	FilesToTokens filesToTokens;		//用在“打开输入文件”中
	bool fileOutRootChange;//用在“打开输入文件”中
	CString outText;

	//Polaris-20140712
	CString cc_Folder;//克隆代码检测的根目录

	int testInt;

	//Polaris-20140716冗余代码
	vector<CMySciEditView *> m_edit_view;
	BOOL	m_bBeginFindBugThread;
	BOOL	m_bBeginFindBugFolderThread;

	//Polaris-20140930
	BOOL   m_bBeginFindCCThread;

// Operations
public:
	void LoadAST(treeNode* root);

	std::vector<std::string> split(std::string str,std::string pattern);

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CMFCMenuBar       m_wndMenuBar;
	CMFCToolBar       m_wndToolBar;
	CMFCStatusBar     m_wndStatusBar;
	CMFCToolBarImages m_UserImages;

public:
	//Polaris-20140711
	CCFileView         m_wndCCFileView;	//显示克隆代码的文件树

	//Polaris-20141008
	//CRCListWnd        m_RCListWnd;	//显示冗余代码信息的列表窗口

public:
	CFileView         m_wndFileView;
	CClassView        m_wndClassView;
	COutputWnd        m_wndOutput;
	CPropertiesWnd    m_wndProperties;
	BugList			  m_buglist;
	CProgressCtrl	  m_wndProgressBar;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWindowManager();
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);
	afx_msg void OnApplicationLook(UINT id);
	afx_msg void OnUpdateApplicationLook(CCmdUI* pCmdUI);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	DECLARE_MESSAGE_MAP()

	BOOL CreateDockingWindows();
	void SetDockingWindowIcons(BOOL bHiColorIcons);
public:
	afx_msg void OnOpenFolder();
	afx_msg void OnSetMinline();
	afx_msg void OnDetectAllClone();
	afx_msg void OnOutputReport();
	afx_msg void OnIncludeDir();
	afx_msg void OnDefParams();
	afx_msg void OnDetectAllRc();
	afx_msg void OnUpdateDetectAllRc(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDetectAllClone(CCmdUI *pCmdUI);
	afx_msg void OnRcType();
	afx_msg void OnRcFiles();
	afx_msg void OnRcTypeRatio();
	afx_msg void OnRcFilesCol();
	afx_msg void OnRcFilesForm();
	afx_msg void OnCcSegFileCol();
	afx_msg void OnCcSegFilePie();
	afx_msg void OnCcLinesFile();
protected:
	afx_msg LRESULT OnUpdateOutput(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnShowCodeGraph();
};