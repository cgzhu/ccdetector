#pragma once
#include "rc_type_tchart.h"


// CRCFilesColumnDlg 对话框

class CRCFilesColumnDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CRCFilesColumnDlg)

public:
	CRCFilesColumnDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRCFilesColumnDlg();

// 对话框数据
	enum { IDD = IDD_RC_FILES_COLUMN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart m_rc_files_col_chart;

	void ClearAllSeries();
};
