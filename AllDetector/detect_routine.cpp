/********************************************************************
	created:	2011/02/11
	created:	11:2:2011   10:42
	filename: 	D:\BugFinder\BugFinder\detect_routine.cpp
	file path:	D:\BugFinder\BugFinder
	file base:	detect_routine
	file ext:	cpp
	author:		qj
	
	purpose:	检测函数
*********************************************************************/
#include "stdafx.h"
#include "ansi.h"
#include "detect_routine.h"
#ifndef _FILE_EXTERN
#define _FILE_EXTERN
#endif
#include "filename.h"
//queue<treeNode*> temp_queue;

//创建语法树叶子节点
treeNode *new_treeNode(nodeType type, char *s, int lineno_to)
{
	treeNode *p = new(treeNode);
	if(!p)
	{
		//// cout <<"error in new_treeNode" << endl;
		exit(0);
	}
	p->lineno = lineno_to;
	p->type = type;
	p->str = s;
	p->parentNode = NULL;
	return p;
}


//释放语法树空间
void freeNode(treeNode *p)
{
	if(!p)
		return;
queue<treeNode*> free_queue;	
	/*while(!free_queue.empty())
		free_queue.pop();
		*/
	free_queue.push(p);
	while(!free_queue.empty())
	{
		treeNode *temp = free_queue.front();
		free_queue.pop();
		
		if(temp->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter;
			for(iter=temp->subNode.begin(); iter!=temp->subNode.end(); iter++)
			{
				free_queue.push(*iter);
			}
		}
		
		free(temp);
		temp = NULL;
	}


/*	if(!p)
		return;
	if(p->subNode.size()==0)
	{
		free(p);
		return;
	}
	vector<node*>::iterator iter = p->subNode.begin();
	while(iter != p->subNode.end())
	{
		vector<node*>::iterator temp_iter = iter;
		iter ++;
		freeNode(*temp_iter);
	}
	p->subNode.clear();
	//free(p);
*/
}


//stack<treeNode*> temp_stack1;
//stack<treeNode*> temp_stack2;

int num_layer[300];				//记录语法树每层的节点数
int index;

//将子树合并为一个节点
void mergeSubNode(treeNode *p)
{
	if(!p)
		return;
	if(p->subNode.size()==0)
		return;
	vector<node*>::iterator iter = p->subNode.begin();
	p->str = "";
	for(; iter!=p->subNode.end(); iter++)
	{
		mergeSubNode(*iter);
		p->str += (*iter)->str;
		p->str += " ";
		free(*iter);
	}
	p->subNode.clear();
}


//只合并子树叶子内容到根节点，不改变树结构，返回该内容串
string mergeSubNodeContent(treeNode *p)
{
	if(!p)
		return "";
	if(p->subNode.size()==0)
		return p->str;
	vector<node*>::iterator iter = p->subNode.begin();
	string temp_str = "";
	for(; iter!=p->subNode.end(); iter++)
	{
		temp_str += mergeSubNodeContent(*iter);
		temp_str += " ";
	}
	return temp_str;
}

typedef struct
{
	string str;
	int lineno;
} NameElem;
vector<NameElem> structName;
vector<NameElem> typeName;

//将文件structName内容读入容器structName中
void readStructName()
{
	ifstream infile(STRUCT_NAME_FILE);
	string str;
	string lineno;

	if(!infile)
	{
		//// cout <<"failed to open preprocessed_struct_name" << endl;
		exit(0);
	}
	while(!infile.eof())
	{
		infile >> str;
		infile >> lineno;
		NameElem elem;
		elem.str = str;
		elem.lineno = atoi(lineno.c_str());
		structName.push_back(elem);
	}
	infile.close();
}

//将文件typeName内容读入到容器typeName中
void readTypeName()
{
	ifstream infile(TYPE_NAME_FILE);
	string str;
	string lineno;

	if(!infile)
	{
		//// cout <<"failed to open preprocessed_type_name" << endl;
		exit(0);
	}
	while(!infile.eof())
	{
		infile >> str;
		infile >> lineno;
		NameElem elem;
		elem.str = str;
		elem.lineno = atoi(lineno.c_str());
		typeName.push_back(elem);
	}
	infile.close();
}

//提取子树中的最左边的标识符（深度遍历）

string firstIdExtract(treeNode *p)
{
    stack<treeNode*> stack_fie;
	/*while(!stack_fie.empty())
		stack_fie.pop();*/
		
	if(!p)
		return "";
		
	stack_fie.push(p);
	while(!stack_fie.empty())
	{
		treeNode *node = stack_fie.top();
		stack_fie.pop();
		
		if(node->type == typeId)
		{
		/*	bool flag1 = true;		//0619
			bool flag2 = true;
			vector<NameElem>::iterator iter_name;
			for(iter_name=structName.begin(); iter_name!=structName.end(); iter_name++)
			{
				if(node->str == (*iter_name).str)
				{
					flag1 = false;
					break;
				}
			}
			for(iter_name=typeName.begin(); iter_name!=typeName.end(); iter_name++)
			{	
				if(node->str == (*iter_name).str)
				{	
					flag2 = false;
					break;
				}
			}
			if(flag1 && flag2)
			{
				return node->str;
			}
		*/
			return node->str;	
		
		}
		
		
		if(node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = node->subNode.end()-1;
			while(iter != node->subNode.begin())
			{
				stack_fie.push(*iter);
				iter--;
			}
			stack_fie.push(*iter);
		}
		
	}	
	
	//test
/*	vector<NameElem>::iterator iter;
	//// cout <<"***********structName************" << endl;
	for(iter=structName.begin(); iter!=structName.end(); iter++)
	{
		//// cout <<(*iter).str << " ";
	}
	//cout <<endl << "***********end*******************" << endl;
*/	
	
}

//幂等缺陷检测
void ideOperDect(treeNode *p)
{
	// cout <<endl << "--------------idempotent operations detection------------------------" << endl;
	
	if(!p)
		return;
	
	ofstream file(IDEOPERDECT_FILE, ios::out | ios::app );	//将缺陷写入文件ideOperDect
	
    queue<treeNode*> temp_queue;
	/*while(!temp_queue.empty())
		temp_queue.pop();*/
	
	temp_queue.push(p);
	while(!temp_queue.empty())
	{
		treeNode *temp_node = temp_queue.front();
		temp_queue.pop();
		
		string str_left, str_oper, str_right;
		switch(temp_node->type)
		{
			case typeAssignExpr:
				str_oper = temp_node->subNode[1]->str;
				if(str_oper == "=")
				{
					str_left = mergeSubNodeContent(temp_node->subNode[0]);
					str_right = mergeSubNodeContent(temp_node->subNode[2]);					
						 
					if(str_left == str_right)
					{
						//发现一个幂等操作缺陷
						// cout <<" a name = name bug " << endl;
						
						file << temp_node->subNode[1]->lineno << " "
							<< str_left << " "
							<< str_oper << " "
							<< str_right << endl;	
					}
										
					//var = var++;
					if(temp_node->subNode[2]->type == typePostExpr)
					{
						treeNode *post_expr = temp_node->subNode[2];
						if(post_expr->subNode.size() == 2 
							&& (post_expr->subNode[1]->str == "++" || post_expr->subNode[1]->str == "--"))
						{
							str_right = mergeSubNodeContent(post_expr->subNode[0]);
							if(str_left == str_right)
							{
								//发现一个幂等操作缺陷
								// cout <<" a name = name++; or name = name--; bug " << endl;
								
								file << temp_node->subNode[1]->lineno << " "
									<< str_left << " "
									<< str_oper << " "
									<< str_right << endl;
							}
						}
					}					
					
				}				
				break;
			case typeMulExpr:
				str_oper = temp_node->subNode[1]->str;
				if(str_oper == "/")
				{
					str_left = mergeSubNodeContent(temp_node->subNode[0]);
					str_right = mergeSubNodeContent(temp_node->subNode[2]);
											 
					if(str_left == str_right)
					{
						//发现一个幂等操作缺陷
						// cout <<" a name / name bug " << endl;
						
						file << temp_node->subNode[1]->lineno << " "
							<< str_left << " "
							<< str_oper << " "
							<< str_right << endl;		
					}
				}
				break;
			case typeAndExpr:
				str_oper = temp_node->subNode[1]->str;
				if(str_oper == "&")
				{
					str_left = mergeSubNodeContent(temp_node->subNode[0]);
					str_right = mergeSubNodeContent(temp_node->subNode[2]);
											 
					if(str_left == str_right)
					{
						//发现一个幂等操作缺陷
						// cout <<" a name & name bug " << endl;
						
						file << temp_node->subNode[1]->lineno << " "
							<< str_left << " "
							<< str_oper << " "
							<< str_right << endl;
					}
				}
				break;
			case typeInOrExpr:
				str_oper = temp_node->subNode[1]->str;
				if(str_oper == "|")
				{
					str_left = mergeSubNodeContent(temp_node->subNode[0]);
					str_right = mergeSubNodeContent(temp_node->subNode[2]);
						 		
					if(str_left == str_right)
					{
						//发现一个幂等操作缺陷
						// cout <<" a name | name bug " << endl;
						
						file << temp_node->subNode[1]->lineno << " "
							<< str_left << " "
							<< str_oper << " "
							<< str_right << endl;
					}
				}
				break;
				
			default:
				;
		}
		
		if(temp_node->subNode.size() > 0)		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queue.push(*iter);
				iter++;
			}
		}	
	}
	
	file.close(); 
	
	// cout <<"---------------------------------end---------------------------------" << endl;
}

//###########################################################################################################################################
//###########################################################################################################################################

//int count_selec_node = 0;	//子树p的总节点数
//遍历子树查找是否存在str，存在返回true
bool exSelecStm(treeNode *p, string str)
{
	if(!p)
		return false;
	queue<treeNode*> temp_queue;
	//while(!temp_queue.empty())		//0224
	//	temp_queue.pop();
	
	temp_queue.push(p);
	while(!temp_queue.empty())
	{
		treeNode *temp_node = temp_queue.front();
		temp_queue.pop();	
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{	
			string temp1;
			if(temp_node->subNode[0]->subNode.size() > 0)
			{
				temp1 = mergeSubNodeContent(temp_node->subNode[0]);
				if(temp1 == str)
					return true;
			}
			
		}
	 	if(temp_node->type == typePostExpr)
		{
			string temp1;
			if(temp_node->subNode[0]->subNode.size() > 0)
			{
				temp1 = mergeSubNodeContent(temp_node->subNode[0]);
				if(temp1 == str)
					return true;
			}
		}
		if(temp_node->type == typeId)
		{
			if(str == temp_node->str)
				return true;
		}
		
		if(temp_node->type == typeJumpStm)				//0224
		{
			if(mergeSubNodeContent(temp_node) == str)
				return true;
		}
		
		if(temp_node->subNode.size() > 0)		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queue.push(*iter);
				iter++;
			}
		}
	}
	return false;
}

//返回子树的节点个数
int exCountNode(treeNode *p)
{
    queue<treeNode*> temp_queue;
	/*while(!temp_queue.empty())
		temp_queue.pop();*/
		
	if(!p) 
		return 0;
	if(p->subNode.size()==0)
		return 1;
	int count = 0;
	temp_queue.push(p);
	while(!temp_queue.empty())
	{
		treeNode *temp_node = temp_queue.front();
		temp_queue.pop();
		count++;
		if(temp_node->subNode.size() > 0)	//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queue.push(*iter);
				iter++;
			}
		}
	}
	return count;
}


//提取函数形参存入容器（按层遍历）
		//保存函数参数
vector<string> str_param;
void paramExtract(treeNode *pp)
{    
	str_param.clear();

     queue<treeNode*> q_param;
    stack<treeNode*> s_param;
	/*
    while(!q_param.empty())
		q_param.pop();
	while(!s_param.empty())
		s_param.pop(); */
	
	treeNode *p = pp;
	if(!p)
		return;
	q_param.push(p);
	while(!q_param.empty())
	{
		vector<node*>::iterator iter;
		treeNode *temp_node = q_param.front();
		q_param.pop();
		
		if(temp_node->type == typeDreDec && temp_node->subNode[2]->type == typeId)	//是不是有问题啊？要是连续的声明怎么办？
		//会不会是先检测到函数声明语句，然后调用该函数？
		{
			str_param.push_back(temp_node->subNode[2]->str);
			break;
		}
		
		if(temp_node->type == typeIdList)	//type fun(var...) type_var var; {...} 
		{
			while(!s_param.empty())
				s_param.pop();
			s_param.push(temp_node);
			while(!s_param.empty())
			{
				treeNode *node = s_param.top();
				s_param.pop();
				if(node->type == typeId)	//对标识符判断
				{
					bool flag = true;
					vector<NameElem>::iterator iter_name;
					for(iter_name=structName.begin(); iter_name!=structName.end(); iter_name++)	//标识符是否为structName
					{
						if(node->str == (*iter_name).str)
						{
							flag = false;
							break;
						}
					}
					for(iter_name=typeName.begin(); iter_name!=typeName.end(); iter_name++)	//标识符是否为typeName
					{	
						if(node->str == (*iter_name).str)
						{
							flag = false;
							break;
						}
					}
					if(flag == true)
					{
						str_param.push_back(node->str);	//保存参数变量	
					}
				}
				//iter = node->subNode.begin();
				//while(iter != node->subNode.end())
				//{
				//	s_param.push(*iter);
				//	iter++;
				//}
				if(node->subNode.size() > 0)
				{
					iter = node->subNode.end()-1;
					while(iter != node->subNode.begin())
					{
						s_param.push(*iter);
						iter--;
					}
					s_param.push(*iter);
				}
			}
			break; 
		}
		
		 
	 	if(temp_node->type == typeParaDeclr)		//type fun(var_type var, var_type2 var2, ...) {...}
		{
			while(!s_param.empty())
				s_param.pop();
			s_param.push(temp_node);
			while(!s_param.empty())
			{
				treeNode *node = s_param.top();
				s_param.pop();
				if(node->type == typeId)	//对标识符判断
				{
					bool flag = true;
					vector<NameElem>::iterator iter_name;
					for(iter_name=structName.begin(); iter_name!=structName.end(); iter_name++)	//标识符是否为structName
					{
						if(node->str == (*iter_name).str)
						{
							flag = false;
							break;
						}
					}
					for(iter_name=typeName.begin(); iter_name!=typeName.end(); iter_name++)	//标识符是否为typeName
					{	
						if(node->str == (*iter_name).str)
						{
							flag = false;
							break;
						}
					}
					if(flag == true)
					{
						str_param.push_back(node->str);	//保存参数变量
						break;	
					}
				}
				//iter = node->subNode.begin();
				//while(iter != node->subNode.end())
				//{
				//	s_param.push(*iter);
				//	iter++;
				//}
				if(node->subNode.size() > 0)
				{
					for(iter=node->subNode.begin(); iter!=node->subNode.end(); iter++)
					{
						s_param.push(*iter);
					}
				}
		
			}
		}
	 	for(iter=temp_node->subNode.begin(); iter!=temp_node->subNode.end(); iter++)
		{
			q_param.push(*iter);
		}
	}
}

//判断节点p是否是switch体中的节点，是为true
bool inSwitch(treeNode *p)		//0223
{
	if(!p)
		return false;
	
	treeNode *temp = p;
	while(temp->parentNode!=NULL 
			&& temp->parentNode->subNode[0]->str!="switch"
			&& temp->parentNode->subNode[0]->str!="if")
	{
		temp = temp->parentNode;
	}
	
	if(temp->parentNode)
	{
		return true;
	}
	
	return false;
}

extern stack<treeNode*> stack_cds;
void jump_node(int n);

vector<string> global_varible;	//保存全局变量名

void exGlobalVarible(treeNode *p)
{
	global_varible.clear();
    
    //stack<treeNode*> stack_cds;

	while(!stack_cds.empty())
		stack_cds.pop();
		
	if(!p)
		return;
		
	stack_cds.push(p);
	while(!stack_cds.empty())
	{
		treeNode *node = stack_cds.top();
		stack_cds.pop();
		
		if(node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = node->subNode.end()-1;
			while(iter != node->subNode.begin())
			{
				stack_cds.push(*iter);
				iter--;
			}
			stack_cds.push(*iter);
		}
		
		if(node->type==typeFuncDef || node->type==typeStructOrUnionSpe 
			|| node->type==typeEnumSpe || node->type==typeDreDec) //node->type==typeDreDec is added at 0412
		{
			jump_node(exCountNode(node) - 1);
		}

		if(node->type == typeId)	//对标识符判断
		{
			bool flag = true;
			vector<NameElem>::iterator iter_name;
			for(iter_name=structName.begin(); iter_name!=structName.end(); iter_name++)	//标识符是否为structName
			{
				if(node->str == (*iter_name).str)
				{
					flag = false;
					break;
				}
			}
			for(iter_name=typeName.begin(); iter_name!=typeName.end(); iter_name++)	//标识符是否为typeName
			{	
				if(node->str == (*iter_name).str)
				{
					flag = false;
					break;
				}
			}
			if(flag == true)
			{
				global_varible.push_back(node->str);	//保存参数变量
			}
		}
	}
}

bool compare_node(treeNode *p1, treeNode *p2); //比较两节点是否为同一节点，同一节点返回true
//bool compare_node2(treeNode *p1, treeNode *p2); //比较两节点是否为同一节点，同一节点返回true
bool exSelecStm(treeNode *p, string str); //遍历子树查找是否存在str，存在返回true


//遍历子树查找是否存在str，存在返回true

bool exSelecStr(treeNode *p, string str)
{
    if(!p)
		return false;
	
    queue<treeNode*> temp_queue0;
	//while(!temp_queue0.empty())		//0224
	//	temp_queue0.pop();
//	ex(p);
	temp_queue0.push(p);
	while(!temp_queue0.empty())
	{
		treeNode *temp_node = temp_queue0.front();
		temp_queue0.pop();	
		
		if(temp_node->type == typeId)
		{
			if(str == temp_node->str)
				return true;
		}
		
		
		if(temp_node->subNode.size() > 0)		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queue0.push(*iter);
				iter++;
			}
		}
	}
	return false;
}

//gdd  
bool exSelecStrn(treeNode *p, string str)
{
    if(!p)
		return false;
	queue<treeNode*> temp_queue0;
	//while(!temp_queue0.empty())		//0224
	//	temp_queue0.pop();
//	ex(p);
	temp_queue0.push(p);
	while(!temp_queue0.empty())
	{
		treeNode *temp_node = temp_queue0.front();
		temp_queue0.pop();	
		
		if(temp_node->type == typeId)
		{
			if(str == temp_node->str)
				return true;
		}
		if(temp_node->type == typeDeclrSpec)
		{
			if(temp_node->subNode[0]->str == "extern")
				return false;
		}
		
		if(temp_node->subNode.size() > 0 &&temp_node->type!=typeInitDec)		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queue0.push(*iter);
				iter++;
			}
		}
		if(temp_node->subNode.size() > 0 &&temp_node->type==typeInitDec)		//0519
		{
			temp_queue0.push(temp_node->subNode[0]);
			
		}
		
	}
	return false;
}
//gdd 过滤全局变量

bool global_value(treeNode *p,string str)
{
	if(!p)
		return false;
	queue<treeNode*> temp_queue;
	//while(!temp_queue.empty())		//0224
	//	temp_queue.pop();
	
	temp_queue.push(p);
	
	while(!temp_queue.empty())
	{
		treeNode *temp_node = temp_queue.front();
		temp_queue.pop();	
		
		if(temp_node->type==typeDeclr)
		{
		  
		  if(exSelecStrn(temp_node, str))
		     return true;
		}			
		
	 	
		if(temp_node->subNode.size() > 0)		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queue.push(*iter);
				iter++;
			}
		}
	}
	return false;
}

//gdd判断语法树P中，结点PP前，是否使用过str（深度遍历）

bool front_use(treeNode *p, treeNode *pp,string str)
{
    if(!p)
		return false;
    stack<treeNode*> stack_use;	
	
    /*while(!stack_use.empty())
		stack_use.pop();*/
	
	stack_use.push(p);
	//ex(p);
	
	while(!stack_use.empty())
	{
	    treeNode *temp_node = stack_use.top();;
		stack_use.pop();
		
		////cout<<temp_node->str<<endl;
		
		if(compare_node(temp_node, pp))
		     return false;
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{	
		    if(exSelecStr(temp_node->subNode[0], str))
		        return false;
            if(exSelecStr(temp_node->subNode[2], str))
                return true;		
		}
	 	if(temp_node->type == typePostExpr)
		{
		     //cout<<"--------typePostExpr-----" << endl;
			if(exSelecStr(temp_node, str))
			    return true;
		}
		if(temp_node->type == typeId)
		{
			if(str == temp_node->str)
				return true;
		}
		
		if(temp_node->type == typeJumpStm)				//0224
		{
			if(mergeSubNodeContent(temp_node) == str)
				return true;
		}
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp_node->subNode.end()-1;
			while(iter != temp_node->subNode.begin())
			{
				stack_use.push(*iter);
				iter--;
			}
			stack_use.push(*iter);
		}
	}
}

//gdd 树p中，结点pp后，是否存在跳转语句，且跳转语句标识符为str

bool later_lable(treeNode *p, treeNode *pp,treeNode *endp,string str)
{

    if(!p)
		return false;
	stack<treeNode*> stack_later_lable;	
	//while(!stack_later_lable.empty())
	//	stack_later_lable.pop();
	
	stack_later_lable.push(p);
	
	treeNode *temp = stack_later_lable.top();
	
	////cout<<pp->str<<endl;
	while(1) //跳过pp前的结点
	{
		temp = stack_later_lable.top();
		stack_later_lable.pop();
		
		if(compare_node(temp, pp))
		   break;
		if(temp->subNode.size()>0)
		{
			vector<node*>::iterator iter = temp->subNode.end()-1;
			while(iter != temp->subNode.begin())
			{
				stack_later_lable.push(*iter);
				iter--;
			}
			stack_later_lable.push(*iter);
		}
	}
	
	////cout<< temp->str <<endl;
	
	while(!stack_later_lable.empty())
	{
	    treeNode *temp_node = stack_later_lable.top();
		stack_later_lable.pop();
		
	//	//cout<<temp_node->str<<endl;
		
		if(compare_node(temp_node, endp))
		     return false;
		if(temp_node->type==typeJumpStm && temp_node->subNode[1]->str==str)
		{	
		     return true;
		}
	 	
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iterr = temp_node->subNode.end()-1;
			while(iterr != temp_node->subNode.begin())
			{
				stack_later_lable.push(*iterr);
				iterr--;
			}
			stack_later_lable.push(*iterr);
		}
	} 
	
}

//gdd
bool extr_use(treeNode *p,string str)
{
    if(!p)
		return false;
//	ex(p);	
    stack<treeNode*> stack_extr;
	/*while(!stack_extr.empty())
		stack_extr.pop();*/
	
	stack_extr.push(p);
	
	while(!stack_extr.empty())
	{
	    treeNode *temp_node = stack_extr.top();;
		stack_extr.pop();
		
		
		
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{	
		    if(temp_node->subNode[0]->str==str)
		        return false;
					
		}
	 	if(temp_node->type == typePostExpr)
		{
		     //cout<<"--------typePostExpr-----" << endl;
			if(exSelecStr(temp_node, str))
			    return true;
		}
		if(temp_node->type == typeId)
		{
			if(str == temp_node->str)
				return true;
		}
		
		if(temp_node->type == typeJumpStm)				//0224
		{
			if(mergeSubNodeContent(temp_node) == str)
				return true;
		}
	
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iterr = temp_node->subNode.end()-1;
			while(iterr != temp_node->subNode.begin())
			{
				stack_extr.push(*iterr);
				iterr--;
			}
			stack_extr.push(*iterr);
		}
	}
	return false;
}

//gdd 在树p中，结点pp后，结点endp前，是否在重新赋值之前使用过str
bool lable_use_str(treeNode *p,treeNode *pp,treeNode *endp,string str)
{
    if(!p)
		return false;
	stack<treeNode*> stack_lable_use;
//	//cout<<"lable_use_str"<<endl;
		
	if(extr_use(pp,str))
	    return true;
		
	/*while(!stack_lable_use.empty())
		stack_lable_use.pop();*/
	
	stack_lable_use.push(p);
	
	treeNode *temp = stack_lable_use.top();
	
//	//cout<< pp->str<<endl;
//	//cout<< endp->str<<endl;
	
	while(1) //跳过pp前的结点
	{
		temp = stack_lable_use.top();
		stack_lable_use.pop();
		
		if(compare_node(temp, pp))
		   break;
		
		if(temp->subNode.size()>0)
		{
			vector<node*>::iterator iter = temp->subNode.end()-1;
			while(iter != temp->subNode.begin())
			{
				stack_lable_use.push(*iter);
				iter--;
			}
			stack_lable_use.push(*iter);
		}
		
	}
	
//	//cout<< temp->str<<endl;
	
		while(!stack_lable_use.empty())
	{
	    treeNode *temp_node = stack_lable_use.top();;
		stack_lable_use.pop();
		
		//cout<<temp_node->str<<endl;
		
		if(compare_node(temp_node, endp))
		     return false;
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{	
		    if(exSelecStr(temp_node->subNode[0], str))
		        return false;
			front_use(temp_node->subNode[2],endp,str);			
		}
	 	if(temp_node->type == typePostExpr)
		{
		     //cout<<"--------typePostExpr-----" << endl;
			if(exSelecStr(temp_node, str))
			    return true;
		}
		if(temp_node->type == typeId)
		{
			if(str == temp_node->str)
				return true;
		}
		
		if(temp_node->type == typeJumpStm)				//0224
		{
			if(mergeSubNodeContent(temp_node) == str)
				return true;
		}
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iterr = temp_node->subNode.end()-1;
			while(iterr != temp_node->subNode.begin())
			{
				stack_lable_use.push(*iterr);
				iterr--;
			}
			stack_lable_use.push(*iterr);
		}
	}

}

//gdd 结点pp在树p中，是否处于lable中，如果处于lable中，前面是否使用过str

bool lable_use(treeNode *p, treeNode *pp,treeNode *endp,string str)
{
    if(!p)
		return false;
	stack<treeNode*> stack_lable;
	/*while(!stack_lable.empty())
		stack_lable.pop();*/
	
	stack_lable.push(p);
	//ex(p);
	
//	//cout<<pp->str<<endl;
	
	while(!stack_lable.empty())
	{
	    treeNode *temp_node = stack_lable.top();
		stack_lable.pop();
		
		////cout<<temp_node->str<<endl;
		
		if(compare_node(temp_node, pp))
		     return false;
		if(temp_node->type==typeLabelStm)
		{	
		   //cout<<"later lable"<<endl;
		   
		   if(later_lable(p,pp,endp,temp_node->subNode[0]->str))
		    {
		        //cout<<"later have jump"<<endl;
		         if(lable_use_str(p,temp_node,pp,str))
		         {
		             //cout<<"use use use"<<endl;
		              return true;
		         }
		     }		   
		}
	 	
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp_node->subNode.end()-1;
			while(iter != temp_node->subNode.begin())
			{
				stack_lable.push(*iter);
				iter--;
			}
			stack_lable.push(*iter);
		}
	
	
	}

}

//冗余赋值缺陷检测
void reduAssignDect(treeNode *p)
{
	vector<string>::iterator iter_param;
    
    stack<treeNode*> stack1;
    stack<treeNode*> stack2;
    stack<treeNode*> stack3;
    queue<treeNode*> queue_assign;

    /*while(!stack1.empty())
    stack1.pop();
    while(!stack2.empty())
    stack2.pop();
    while(!stack3.empty())
    stack3.pop();*/
		
	readStructName();	//将文件structName内容读入容器structName中
	readTypeName();		//将文件typeName内容读入容器typeName中
	
	// cout <<endl << "----------------redundant assignments detection----------------------" << endl;
	
	if(!p)
		return;
		
//	ex(p);
		
	exGlobalVarible(p);		//提取全局变量
	vector<string>::iterator iter_global;

	
	ofstream file(REDUASSIGNDECT_FILE, ios::out | ios::app);	//将缺陷存入文件reduAssignDect
	
    /*while(!queue_assign.empty())
    queue_assign.pop();*/
	
	queue_assign.push(p);
	while(!queue_assign.empty())
	{
		// 取队头节点
		treeNode *queue_front = queue_assign.front();
		queue_assign.pop();
		
		// 队头节点的孩子节点入队
		if(queue_front->subNode.size() > 0)	
		{
			vector<treeNode*>::iterator iter;
			for(iter=queue_front->subNode.begin(); iter!=queue_front->subNode.end(); iter++)
			{
				queue_assign.push(*iter);
			}
		}
		
		// 对函数子树进行处理
		if(queue_front->type == typeFuncDef)
		{
			// 提取函数参数保存于str_param中
			if(queue_front->subNode[1]->type == typeDreDec
				|| queue_front->subNode[1]->type == typeDec)	
			{
				paramExtract(queue_front->subNode[1]);
			
				/*vector<string>::iterator iter;
				// cout <<endl << "********** function parameter ***********" << endl;
				for(iter=str_param.begin(); iter!=str_param.end(); iter++)
				{
					// cout <<(*iter) << " ";
				}
				// cout <<endl << "****************** end ******************" << endl;
				*/
			}
									
			//遍历整个函数子树，提取*var中的标识符var，用来简单消除指针别名的误检	//0619
			treeNode *root_func_subtree = queue_front;
			int num_node_func = exCountNode(root_func_subtree);
			stack<treeNode*> stack_func;
			stack_func.push(root_func_subtree);
			int count = 0;
			vector<string> pointer_var;
			while(!stack_func.empty() && count < num_node_func)
			{
				treeNode *temp_node = stack_func.top();
				stack_func.pop();
				count++;
				
				if(temp_node->subNode.size() > 0)
				{
					vector<node*>::iterator iter = temp_node->subNode.end()-1;
					while(iter != temp_node->subNode.begin())
					{
						stack_func.push(*iter);
						iter--;
					}
					stack_func.push(*iter);
				}
				
				switch(temp_node->type)
				{
					case typeDec:
						if(temp_node->subNode.size()==2 && temp_node->subNode[0]->str=="*")
						{
							pointer_var.push_back(temp_node->subNode[1]->str);
							break;
						}
						break;
					
					//case
					
					/*default:
						;*/
				}
			}		
			
			//找到还函数子树的最后一个节点		//0619
			while(!stack_func.empty())
				stack_func.pop();
			count = 0;
			stack_func.push(root_func_subtree);
			while(!stack_func.empty() && count < num_node_func - 1)
			{
				treeNode *temp_node = stack_func.top();
				stack_func.pop();
				count++;
				
				if(temp_node->subNode.size() > 0)
				{
					vector<node*>::iterator iter = temp_node->subNode.end()-1;
					while(iter != temp_node->subNode.begin())
					{
						stack_func.push(*iter);
						iter--;
					}
					stack_func.push(*iter);
				}
			}
			treeNode *end_func_subtree = stack_func.top();
			
			//提取static变量  //0621
			count = 0;
			vector<string> static_var;
			stack<treeNode*> stack_static_var;
			stack_static_var.push(queue_front);
			while(!stack_static_var.empty() && count < num_node_func)
			{
				treeNode *temp_node = stack_static_var.top();
				stack_static_var.pop();
				count++;
				
				if(temp_node->subNode.size() > 0)
				{
					vector<node*>::iterator iter = temp_node->subNode.end() - 1;
					while(iter != temp_node->subNode.begin())
					{
						stack_static_var.push(*iter);
						iter--;
					}
					stack_static_var.push(*iter);
				}
				
				if(temp_node->type == typeKeyWord && temp_node->str == "static")
				{
					while(temp_node->parentNode && temp_node->parentNode->type != typeDeclr)
						temp_node = temp_node->parentNode;
					if(temp_node->parentNode)
					{
						static_var.push_back(firstIdExtract(temp_node->parentNode->subNode[1]));
					}
				}
			}
			//{	//test
			//	// cout <<"-------------static_var--------------" << endl;
			//	for(int i=0; i<static_var.size(); i++)
			//	{
			//		// cout <<static_var[i] << endl;
			//	}
			//	// cout <<"-------------end---------------------" << endl;
			//}			
			
			// 缺陷检测开始 (从左到右先根遍历语法树)
			stack1.push(queue_front);
			treeNode *temp_node;
			
			while(!stack1.empty())
			{
				temp_node = stack1.top();
				stack1.pop();
				if(temp_node->subNode.size() > 0)
				{
					vector<node*>::iterator iter = temp_node->subNode.end()-1;
					while(iter != temp_node->subNode.begin())
					{
						stack1.push(*iter);
						iter--;
					}
					stack1.push(*iter);
				}
				
				if(compare_node(end_func_subtree, temp_node))	//0619 
					break;
				
				// 带有赋值操作的节点 assignment_expression init_declarator
				if(temp_node->type==typeAssignExpr || temp_node->type==typeInitDec)
				{
				   // //cout<<temp_node->str<<endl;
					string temp_str;			//需要检测的标识符保存在temp_str中
					string temp_str_head = "";	//存放复合标识符的最左标识符名，如var->name中的var
					int lineno;					//需要检测的标识符所在的行号
					
					treeNode *node_detect = stack1.top();	//需要检测的标识符的根节点
					treeNode *temp_node_detect = node_detect;
					treeNode *node_detect_in_for_expr = node_detect;
					
					//提取并保存要检测的标识符
					if(stack1.top()->subNode.size() > 0)
					{
						temp_str = mergeSubNodeContent(stack1.top());
						temp_str_head = firstIdExtract(stack1.top());
						if(stack1.top()->lineno!=-1)
						{						
						    lineno = stack1.top()->subNode[0]->lineno;
						}
						else
						{
						    treeNode *temp_detect = stack1.top();
						    while(temp_detect->subNode[0]->lineno==-1)
						       temp_detect=temp_detect->subNode[0];
						    lineno = temp_detect->subNode[0]->lineno;
						}
					}
					else
					{				
						temp_str = stack1.top()->str;
						temp_str_head = temp_str;
							
						lineno = stack1.top()->lineno;
					}	
					// cout <<lineno << "  " << temp_str << endl;		//test
					
					//跳过assignment_expression和init_declarator节点下的子节点
					int count = exCountNode(temp_node)-1; //temp_node子树的个数-1
					treeNode *temp;
					int temp_count = 0;
					while(temp_count < count)
					{
						temp = stack1.top();
						stack1.pop();
						temp_count++;
						if(temp->subNode.size()>0)
						{
							vector<node*>::iterator iter = temp->subNode.end()-1;
							while(iter != temp->subNode.begin())
							{
								stack1.push(*iter);
								iter--;
							}
							stack1.push(*iter);
						}
					}
					
					//stack2保存断点，以便下次从此处开始下一轮的缺陷检测  //这里直接保存位置指针就可以了
					while(!stack1.empty())
					{
						temp = stack1.top();
						stack1.pop();
						stack2.push(temp);
						stack3.push(temp);
					}
					while(!stack3.empty())
					{
						temp = stack3.top();
						stack3.pop();
						stack1.push(temp);
					}
					
					//标识符为指针别名时，不进行冗余检测	//0619
					{
						for(int i=0; i<pointer_var.size(); i++)
						{
							if(temp_str_head == pointer_var[i])
								goto label1;
						}
					}
					
					//标识符为static变量时，不进行冗余检测	//0621
					{
						for(int i=0; i<static_var.size(); i++)
						{
							if(temp_str_head == static_var[i])
								goto label1;
						}
					}
					
					//标识符为数组时，不进行冗余检测 
					if(temp_str.find('[') != -1)
					{
						goto label1;
					}
					
					//标识符为函数参数时，不进行冗余检测
					
					for(iter_param=str_param.begin(); iter_param!=str_param.end(); iter_param++)
					{ 
						if(*iter_param == temp_str_head)
							goto label1;
					}
					//标识符为全局变量时，不进行冗余检测 gdd
					if(!global_value(root_func_subtree,temp_str_head))
					{
					   // cout << " kkkkkkkkkkkkk "  << endl;	
					   goto label1;
					   
					}
					
					//在for循环条件表达式中的赋值不进行缺陷检测
					//treeNode *node_detect_in_for_expr = node_detect;
					while(node_detect_in_for_expr->parentNode
							&& node_detect_in_for_expr->parentNode->type != typeIterStm)
					{
						node_detect_in_for_expr = node_detect_in_for_expr->parentNode;
					}
					if(node_detect_in_for_expr->parentNode
						&&	node_detect_in_for_expr->parentNode->type == typeIterStm)
					{
						treeNode *iter_node = node_detect_in_for_expr->parentNode;
						
						if(iter_node->subNode[0]->str == "for")
						{
							int lineno_for = iter_node->subNode[0]->lineno;
							int lineno_youkuohao = lineno_for;
							
							for(int i=0; i<iter_node->subNode.size(); i++)
							{
								if(iter_node->subNode[i]->str == ")")
								{
									lineno_youkuohao = iter_node->subNode[i]->lineno;
								}
							}
							
							if(lineno >= lineno_for && lineno <= lineno_youkuohao)
							{
								goto label1;
							}
						}
					}
					
					//标识符在循环语句体中被赋值，但该标识符包含在循环语句的条件表达式中时，不进行冗余检测
					//gdd修改，考虑了循环嵌套情况，也解决了循环中使用的情况
					//treeNode *temp_node_detect = node_detect;                    					
                    while(temp_node_detect->parentNode 
							&& (!compare_node(temp_node_detect->parentNode,root_func_subtree)))
					{
						temp_node_detect = temp_node_detect->parentNode;
					
					    if(temp_node_detect->parentNode
						    && temp_node_detect->parentNode->type == typeIterStm)
					    {
						    treeNode *iter_node = temp_node_detect->parentNode;
						
						    if(iter_node->subNode[0]->str == "do")
						    {
							    if(compare_node(iter_node->subNode[1], temp_node_detect))
							    {
								    if(exSelecStm(iter_node->subNode[4], temp_str_head))	// 遍历子树查找是否存在str，存在返回true
								    {
									    goto label1;
								    }
                                
								    if(front_use(temp_node_detect,temp_node,temp_str_head))
								    {
									    goto label1;
								    }
							    }
						    }
						    else
						    {
							    int last_index = iter_node->subNode.size() - 1;
							    if(compare_node(iter_node->subNode[last_index], temp_node_detect))
							    {
							    
								    int index;
								    for(index=0; index<last_index; index++)
								    {
									    if(exSelecStm(iter_node->subNode[index], temp_str_head))
									    {
										    goto label1;
									    }
								    }
								
								    if(front_use(temp_node_detect,temp_node,temp_str_head))
								    {                                    
									    goto label1;
								    }                                
							    }
						    }
						    temp_node_detect=iter_node;
					     }
					}
				    					

					//检测跳转语句
					if(lable_use(root_func_subtree,temp_node,end_func_subtree,temp_str_head))
					{
					           goto label1;
					}
					
					// 判断要检测的标识符在后面是否使用
					while(!stack1.empty())
					{							
						temp = stack1.top();
						stack1.pop();
						if(temp->subNode.size()>0)
						{
							vector<node*>::iterator iter = temp->subNode.end()-1;
							while(iter != temp->subNode.begin())
							{
								stack1.push(*iter);
								iter--;
							}
							stack1.push(*iter);
						}
						
						if(compare_node(end_func_subtree, temp))	//0619
						{
							//标识符为全局变量时，则不判为缺陷	//
							vector<string>::iterator iter_global;
							for(iter_global=global_varible.begin(); iter_global!=global_varible.end(); iter_global++)
							{
								if(*iter_global== temp_str_head)
									goto label1;
							}
							////发现一个冗余赋值缺陷
							// cout <<"  3 Find a redundancy assignment bug " << endl;
						
							//加入缺陷
							file << lineno << " " << temp_str << endl;
                            //OutputDebugString("2");
							break;
						}
						
						switch(temp->type)
						{
							case typeSelecStm:		// 选择语句，只要有一个分支中出现要找的标识符，则表示已使用，不存在缺陷
								if(exSelecStm(temp, temp_str) || exSelecStm(temp, temp_str_head))
									goto label1;
								break;				
								
							//case 其他待考虑的情况
							
							case typeAssignExpr:	//未使用又重新赋值
								if(temp->subNode[1]->str == "=")		//0227
								{
									string str_assign;
									if(stack1.top()->subNode.size() > 0)
									{
										str_assign = mergeSubNodeContent(stack1.top());
									}
									else
									{
										str_assign = stack1.top()->str;
									}
									if(temp_str == str_assign)
									{	
										//0412 去除保守编程赋值的情况
                                            treeNode *temp_node_detect = node_detect;
                                        while(temp_node_detect->parentNode
                                            && temp_node_detect->type != typeInitDec)
                                        {
                                            temp_node_detect = temp_node_detect->parentNode;
                                        }
                                        if(temp_node_detect->type == typeInitDec)
                                        {
                                            goto label1;
                                        }
									
										//sum = 0;	0309
										//sum = sum + x;
										if(exSelecStm(temp->subNode[2], str_assign))
											break;								
																	
										if(!inSwitch(node_detect) || !inSwitch(temp))	//如果不是switch中的赋值语句则为缺陷
										{
											//发现一个冗余赋值缺陷
											// cout <<"  2 Find a redundancy assignment bug " << endl;
										
											//加入缺陷列表
											file << lineno << " " << temp_str << endl;																			
											goto label1;
										}
																	
										//跳过与检测目标比较过的赋值语句 0223
										int count = 0;
										while(!stack1.empty() && count<(exCountNode(temp)-1))
										{
											treeNode *temp_jump;
											temp_jump = stack1.top();
											stack1.pop();
											count++;
											if(temp_jump->subNode.size()>0)
											{
												vector<node*>::iterator iter = temp_jump->subNode.end()-1;
												while(iter != temp_jump->subNode.begin())
												{
													stack1.push(*iter);
													iter--;
												}
												stack1.push(*iter);
											}
											
										}
									}
									//break;
								}
								break;
							case typePostExpr:
								{
									string str_post;
									string str_post_head;					//0126
									str_post = mergeSubNodeContent(temp);
									str_post_head = firstIdExtract(temp);	//0126
									//// cout <<"temp_str: " << temp_str << endl;
									//// cout <<"str_post: " << str_post << endl;
									if(str_post == temp_str || str_post_head == temp_str_head)	//0126 redundant
									{
										goto label1;
									}
									break;
								}
							case typeId:			//一般标识符节点，比对内容
								if(temp_str == temp->str || temp_str_head == temp->str)	//0131
									goto label1;
								break;
							
							default:
								;
						}
					}// end_while
					
				/*	if(stack1.empty())
					{
						//发现一个冗余赋值缺陷
						// cout <<"  3 Find a redundancy assignment bug " << endl;
						
						//加入缺陷
						file << lineno << " " << temp_str << endl;				
					}
				*/			
				label1:
					//恢复断点
					while(!stack1.empty())
					{
						stack1.pop();
					}
					while(!stack2.empty())
					{
						temp_node = stack2.top();
						stack2.pop();
						stack1.push(temp_node);
					}
				}// end_if
			}// end_while
		}
	}
	
	file.close();
	
	// cout <<endl << "------------------------------end------------------------------------" << endl;
}


//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
void reduParameter(treeNode *p)
{
    // cout <<endl << "----------------redundant parameter detection----------------------" << endl;
    if(!p)
		return;
    ofstream file(REDUPARAMETER_FILE, ios::out | ios::app);

    stack<treeNode*> stackp1;
    queue<treeNode*> queue_para;

    /*while(!stackp1.empty())
		stackp1.pop();*/

    /*while(!queue_para.empty())
		queue_para.pop();*/
	
	queue_para.push(p);
	
	while(!queue_para.empty())
	{
		// 取队头节点
		treeNode *queue_front = queue_para.front();
		queue_para.pop();
		
		// 队头节点的孩子节点入队
		if(queue_front->subNode.size() > 0)	
		{
			vector<treeNode*>::iterator iter;
			for(iter=queue_front->subNode.begin(); iter!=queue_front->subNode.end(); iter++)
			{
				queue_para.push(*iter);
			}
		}
		
		// 对函数子树进行处理
		int lineno=0;
		if(queue_front->type == typeFuncDef)
		{
		    if(queue_front->lineno!=-1)
			{						
				lineno =queue_front->subNode[0]->lineno;
			}
			else
			{
				treeNode *temp_detecta = queue_front;
				while(temp_detecta->subNode[0]->lineno==-1)
					temp_detecta=temp_detecta->subNode[0];
				lineno = temp_detecta->subNode[0]->lineno;
			}	    
		
		    // 提取函数参数保存于str_param中
			if(queue_front->subNode[1]->type == typeDreDec
				|| queue_front->subNode[1]->type == typeDec)	
			{
				paramExtract(queue_front->subNode[1]);
				
			}
			//找到还函数子树的最后一个节点		//0619
			treeNode *root_func_subtree = queue_front;
			int num_node_func = exCountNode(root_func_subtree);
			stack<treeNode*> stack_func;
			stack_func.push(root_func_subtree);

			while(!stack_func.empty())
				stack_func.pop();
			int count = 0;
			stack_func.push(root_func_subtree);
			while(!stack_func.empty() && count < num_node_func - 1)
			{
				treeNode *temp_node = stack_func.top();
				stack_func.pop();
				count++;
				
				if(temp_node->subNode.size() > 0)
				{
					vector<node*>::iterator iter = temp_node->subNode.end()-1;
					while(iter != temp_node->subNode.begin())
					{
						stack_func.push(*iter);
						iter--;
					}
					stack_func.push(*iter);
				}
			}
			treeNode *end_func_subtree = stack_func.top();

			
			vector<string>::iterator iter_param;
			string temp_str_head = "";
			for(iter_param=str_param.begin(); iter_param!=str_param.end(); iter_param++)
			{ 
				 temp_str_head=*iter_param;
				 
			//	 // cout <<temp_str_head <<"aaaaaaaaaaaaaaa" <<endl;
				  // 判断要检测的标识符在后面是否使用
				  int numqueue=queue_front->subNode.size()-1;
				  stackp1.push(queue_front->subNode[numqueue]);
					treeNode *temp;
					while(!stackp1.empty())
					{							
						temp = stackp1.top();
						stackp1.pop();
					//	// cout <<temp->str << endl;
						if(temp->subNode.size()>0)
						{
							vector<node*>::iterator iter = temp->subNode.end()-1;
							while(iter != temp->subNode.begin())
							{
								stackp1.push(*iter);
								iter--;
							}
							stackp1.push(*iter);
						}
						
						if(compare_node(end_func_subtree, temp))	//0619
						{
						/*	//标识符为全局变量时，则不判为缺陷	//
							vector<string>::iterator iter_global;
							for(iter_global=global_varible.begin(); iter_global!=global_varible.end(); iter_global++)
							{
								if(*iter_global== temp_str_head)
									goto label1;
							}
							*/
							////发现一个冗余赋值缺陷
							// cout <<"  3 Find a redundancy parameter bug " << endl;
						
							//加入缺陷
							file << lineno<<  " " << temp_str_head << endl;	
							
							break;
						}
						
						switch(temp->type)
						{
							case typeSelecStm:		// 选择语句，只要有一个分支中出现要找的标识符，则表示已使用，不存在缺陷
								if(exSelecStm(temp, temp_str_head))
									goto label1;
								break;				
								
							//case 其他待考虑的情况
							
							case typeAssignExpr:	//未使用又重新赋值
								if(temp->subNode[1]->str == "=")		//0227
								{
									string str_assign;
									if(stackp1.top()->subNode.size() > 0)
									{
										str_assign = mergeSubNodeContent(stackp1.top());
									}
									else
									{
										str_assign = stackp1.top()->str;
									}
									if(temp_str_head == str_assign)
									{	
									
									        if(exSelecStm(temp->subNode[2], str_assign))
											           break;	
											//发现一个冗余赋值缺陷
											// cout <<"  2 Find a redundancy parameter bug " << endl;
										
											//加入缺陷列表
											file << lineno<<  " " << temp_str_head << endl;								
											
											goto label1;
									
																	
										//跳过与检测目标比较过的赋值语句 0223
										int count = 0;
										while(!stackp1.empty() && count<(exCountNode(temp)-1))
										{
											treeNode *temp_jump;
											temp_jump = stackp1.top();
											stackp1.pop();
											count++;
											if(temp_jump->subNode.size()>0)
											{
												vector<node*>::iterator iter = temp_jump->subNode.end()-1;
												while(iter != temp_jump->subNode.begin())
												{
													stackp1.push(*iter);
													iter--;
												}
												stackp1.push(*iter);
											}
											
										}
									}
									break;
								}
								break;
							case typePostExpr:
								{
									string str_post;
									string str_post_head;					//0126
									str_post = mergeSubNodeContent(temp);
									str_post_head = firstIdExtract(temp);	//0126
									//// cout <<"temp_str: " << temp_str << endl;
									//// cout <<"str_post: " << str_post << endl;
									if(str_post == temp_str_head || str_post_head == temp_str_head)	//0126 redundant
									{
										goto label1;
									}
									break;
								}
							case typeId:			//一般标识符节点，比对内容
								if( temp_str_head == temp->str)	//0131
									goto label1;
								break;
							
							default:
								;
						}
					}// end_while
					     
					     
			     
			label1:
			  ;
				 
			}//end for
		
		
		
		} 
	} 
		
	file.close();
	
	// cout <<endl << "------------------------------end------------------------------------" << endl;
 
}


//###########################################################################################################################################
//###########################################################################################################################################
stack<treeNode*> stack_cds;
//返回p节点在源代码中的行号
int line_of_node(treeNode *p)
{
    
    queue<treeNode*> queue_cds;

	/*while(!queue_cds.empty())
		queue_cds.pop();*/
		
	if(!p)
		return -1;
	queue_cds.push(p);			
	while(!queue_cds.empty())
	{
		treeNode *node = queue_cds.front();
		queue_cds.pop();
		
		if(node->lineno != -1)
		{						
			return node->lineno;
		}
		
		vector<treeNode*>::iterator iter;
		for(iter=node->subNode.begin(); iter!=node->subNode.end(); iter++)
		{
			queue_cds.push(*iter);
		}
	}
	
	return -1;
}

//跳过栈stack_cds中的n个节点
void jump_node(int n)
{
	int count = 0;
	while(!stack_cds.empty() && count<n)
	{
		treeNode* node = stack_cds.top();
		stack_cds.pop();
		count++;
		
		if(node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = node->subNode.end() - 1;
			while(iter != node->subNode.begin())
			{
				stack_cds.push(*iter);
				iter--;
			}
			stack_cds.push(*iter);		
		}
	}
}

//比较两节点是否为同一节点，同一节点返回true
bool compare_node(treeNode *p1, treeNode *p2)
{
	if(!p1 || !p2)
		return false;
		
	if(p1->type == p2->type)
	{
		if(p1->str == p2->str)
		{
			if(p1->lineno == p2->lineno)
			{
				if(mergeSubNodeContent(p1) ==  mergeSubNodeContent(p2))
				{
					return true;
				}
			}
		}
	}
	
	return false;
}

//声明
void iter_node(treeNode *iterNode, treeNode *p);
void selec_node(treeNode *selecNode, treeNode *p);

//对statement节点处理
	//root_node：cds中statement子树的根节点
	//stack_cds栈顶元素指向原语法树中statement子树的第一个节点
void statement_node(treeNode *root_node)
{
	if(!root_node)
		return;
	
	int count_node = exCountNode(stack_cds.top());	//原语法树中statement子树的节点个数
	int count = 0;
	treeNode *p;
	
	//处理statement部分
	while(!stack_cds.empty() && count<count_node)
	{
		p = stack_cds.top();
		stack_cds.pop();
		count++;
		
		if(p->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = p->subNode.end() - 1;
			while(iter != p->subNode.begin())
			{
				stack_cds.push(*iter);
				iter--;
			}
			stack_cds.push(*iter);
		}
		
		treeNode *general_node;	
		switch(p->type)
		{
			case typeLabelStm:
				general_node = new_treeNode(typeLabelStm, "", -1);
				general_node->str = mergeSubNodeContent(p);
				general_node->lineno = line_of_node(p);
				general_node->parentNode = root_node;
				
				jump_node(exCountNode(p)-1);
				count += exCountNode(p) - 1;
				root_node->subNode.push_back(general_node);
				break;
			case typeOperator:		// ;
				if(p->str == ";")
				{
					general_node = new_treeNode(typeOperator, "", -1);
					general_node->str = p->str;
					general_node->lineno = p->lineno;
					general_node->parentNode = root_node;
				
					root_node->subNode.push_back(general_node);
				}
				break;
			case typeExprStm:
				general_node =new_treeNode(typeExprStm, "", -1);
				general_node->str = mergeSubNodeContent(p);
				general_node->lineno = line_of_node(p);
				general_node->parentNode = root_node;
				
				jump_node(exCountNode(p)-1);
				count += exCountNode(p) - 1;	//careful
				root_node->subNode.push_back(general_node);
				break;
			case typeSelecStm:
				general_node = new_treeNode(typeSelecStm, "", -1);
				general_node->str = p->subNode[0]->str;
				general_node->lineno = p->subNode[0]->lineno;
				general_node->parentNode = root_node;
				
				root_node->subNode.push_back(general_node);
				
				selec_node(general_node, p);	//
				count += exCountNode(p) - 1;
				break;
			case typeIterStm:
				general_node = new_treeNode(typeIterStm, "", -1);
				general_node->str = p->subNode[0]->str;
				general_node->lineno = p->subNode[0]->lineno;
				general_node->parentNode = root_node;
				
				root_node->subNode.push_back(general_node);
				
				iter_node(general_node, p);	//
				count += exCountNode(p) - 1;
				break;
			case typeJumpStm:
				general_node = new_treeNode(typeJumpStm, "", -1);
				general_node->str = p->subNode[0]->str;
				general_node->lineno = p->subNode[0]->lineno;
				general_node->parentNode = root_node;
				
				jump_node(exCountNode(p)-1);
				count += exCountNode(p) - 1;
				root_node->subNode.push_back(general_node);
				break;
			case typeCompStm:	//只单独处理空的情况
				if(p->subNode.size() == 2)
				{
					general_node = new_treeNode(typeOperator, "", -1);
					general_node->str = ";";
					general_node->lineno = p->subNode[0]->lineno;
					general_node->parentNode = root_node;
					
					jump_node(2);
					count += 2;
					root_node->subNode.push_back(general_node);
				}
				break;
			default:
				;
		}	
	}
	
}

//对条件语句处理
	//selecNode为新创建的语法树中的选择语句的根
	//p为原语法树的selec_stm节点
		//if语句均为两个分支，一个true, 一个false
		//switch语句为多个分支，每个标号语句为一个分支
void selec_node(treeNode *selecNode, treeNode *p)
{
	if(!selecNode || !p)
		return;
	
	treeNode *temp = stack_cds.top();
	stack_cds.pop();
	while(!compare_node(temp, p->subNode[4]))	//使栈顶元素为statement节点
	{
		if(temp->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp->subNode.end() - 1;
			while(iter != temp->subNode.begin())
			{
				stack_cds.push(*iter);
				iter--;
			}
			stack_cds.push(*iter);
		}
		
		temp = stack_cds.top();
		stack_cds.pop();
	}
	stack_cds.push(temp);
	//// cout <<endl << stack_cds.top()->str << endl;	//test
	
	if(selecNode->str=="if" && p->subNode.size()==5)	//IF '(' expression ')' statement
	{
		//if表达式的true分支
		treeNode *if_true = new_treeNode(typeOther, "TRUE", -1);
		if_true->lineno = line_of_node(selecNode);
		if_true->parentNode = selecNode;
		selecNode->subNode.push_back(if_true);
	
		statement_node(if_true);

		//if表达式的false分支
		treeNode *if_false = new_treeNode(typeOther, "FALSE", -1);
		if_false->parentNode = selecNode;
		selecNode->subNode.push_back(if_false);
	}
	else if(selecNode->str == "if")	//IF '(' expression ')' statement ELSE statement
	{
		//if表达式的true分支
		treeNode *if_true = new_treeNode(typeOther, "TRUE", -1);
		if_true->lineno = line_of_node(selecNode);
		if_true->parentNode = selecNode;
		selecNode->subNode.push_back(if_true);
	
		statement_node(if_true);
		
		stack_cds.pop();	//pop ELSE
		
		//if表达式的false分支
		treeNode *if_false = new_treeNode(typeOther, "FALSE", -1);
		if_false->lineno = line_of_node(stack_cds.top());
		if_false->parentNode = selecNode;
		
		selecNode->subNode.push_back(if_false);
		
		statement_node(if_false);
	}
	else	//SWITCH '(' expression ')' statement
	{
		treeNode *node_stm = stack_cds.top();
		int count_node = exCountNode(node_stm);
		
		treeNode *p;
		treeNode *general_node;
		treeNode *current_node = selecNode;
		int count = 0;
		while(!stack_cds.empty() && count<count_node)
		{
			p = stack_cds.top();
			stack_cds.pop();
			count++;
			if(p->subNode.size() > 0)
			{
				vector<treeNode*>::iterator iter = p->subNode.end() - 1;
				while(iter != p->subNode.begin())
				{
					stack_cds.push(*iter);
					iter--;
				}	
				stack_cds.push(*iter);
			}

			switch(p->type)
			{
				case typeLabelStm:
					if(p->subNode.size()>0 && 
						(p->subNode[0]->str=="case" || p->subNode[0]->str=="default"))
					{
						general_node = new_treeNode(typeLabelStm, "", -1);
				
						if(p->subNode[0]->str == "case")
							general_node->str = mergeSubNodeContent(p->subNode[1]);
						else
							general_node->str = "default";
				
						general_node->lineno = p->subNode[0]->lineno;
						general_node->parentNode = selecNode;
						
						jump_node(exCountNode(p->subNode[1]) + 1);
						count += exCountNode(p->subNode[1]) + 1;
						
						selecNode->subNode.push_back(general_node);
						
						current_node = general_node;	//
					}
					else
					{
						general_node = new_treeNode(typeLabelStm, "", -1);
						general_node->str = mergeSubNodeContent(p);
						general_node->lineno = line_of_node(p);
						general_node->parentNode = current_node;
					
						jump_node(exCountNode(p)-1);
						count += exCountNode(p) - 1;
						
						current_node->subNode.push_back(general_node);
					}
					break;
				case typeOperator:		// ;
					if(p->str == ";")
					{
						general_node = new_treeNode(typeOperator, "", -1);
						general_node->str = p->str;
						general_node->lineno = p->lineno;
						general_node->parentNode = current_node;
					
						current_node->subNode.push_back(general_node);
					}
					break;
				case typeExprStm:
					general_node = new_treeNode(typeExprStm, "", -1);
					general_node->str = mergeSubNodeContent(p);
					general_node->lineno = line_of_node(p);
					general_node->parentNode = current_node;
					
					jump_node(exCountNode(p)-1);
					count += exCountNode(p) - 1;
					
					current_node->subNode.push_back(general_node);
					break;
				case typeSelecStm:
					general_node = new_treeNode(typeSelecStm, "", -1);
					general_node->str = p->subNode[0]->str;
					general_node->lineno = p->subNode[0]->lineno;
					general_node->parentNode = current_node;
					
					current_node->subNode.push_back(general_node);
					
					selec_node(general_node, p);	//
					count += exCountNode(p) - 1;
					break;
				case typeIterStm:
					general_node = new_treeNode(typeIterStm, "", -1);
					general_node->str = p->subNode[0]->str;
					general_node->lineno = p->subNode[0]->lineno;
					general_node->parentNode = current_node;
					
					current_node->subNode.push_back(general_node);
					
					iter_node(general_node, p);	//
					count += exCountNode(p) - 1;
					break;
				case typeJumpStm:
					general_node = new_treeNode(typeJumpStm, "", -1);
					general_node->str = p->subNode[0]->str;
					general_node->lineno = p->subNode[0]->lineno;
					general_node->parentNode = current_node;
					
					jump_node(exCountNode(p)-1);
					count += exCountNode(p) - 1;
					
					current_node->subNode.push_back(general_node);
					break;
				case typeCompStm:	//只单独处理空的情况
					if(p->subNode.size() == 2)
					{
						general_node = new_treeNode(typeOperator, "", -1);
						general_node->str = ";";
						general_node->lineno = p->subNode[0]->lineno;
						general_node->parentNode = selecNode;
						
						jump_node(2);
						count += 2;
						selecNode->subNode.push_back(general_node);
					}
					break;
				default:
					;
			}	
		}
	}			
}

//对for循环语句进行处理
	//iterNode为新创建的语法树中的循环语句子树的根
	//p为原语法树的iter_stm节点
void iter_node(treeNode *iterNode, treeNode *p)
{
	if(!iterNode || !p)
		return;
	
	if(iterNode->str == "for" || iterNode->str == "while")
	{
		treeNode *temp = stack_cds.top();
		stack_cds.pop();
		while(!compare_node(temp, p->subNode[p->subNode.size()-1]))
		{
			if(temp->subNode.size() > 0)
			{
				vector<treeNode*>::iterator iter = temp->subNode.end() - 1;
				while(iter != temp->subNode.begin())
				{
					stack_cds.push(*iter);
					iter--;
				}
				stack_cds.push(*iter);
			}
					
			temp = stack_cds.top();
			stack_cds.pop();
		}	
		stack_cds.push(temp);					
	}
	
	int flag_do = false;	//标识do循环
	if(iterNode->str == "do")
	{
		flag_do = true;
		stack_cds.pop();
	}

	statement_node(iterNode);	//
	
	if(flag_do == true)	//对do循环特殊处理，去掉while '(' expression ')' ';'部分
	{
		int count = exCountNode(p->subNode[3]) +
					exCountNode(p->subNode[4]) +
					exCountNode(p->subNode[5]) +
					exCountNode(p->subNode[6]);

		int count_node = 0;
		while(count_node<count && !stack_cds.empty())
		{
			treeNode *node = stack_cds.top();
			stack_cds.pop();
			count_node++;
			
			if(node->subNode.size() > 0)
			{
				vector<treeNode*>::iterator iter = node->subNode.end() - 1;
				while(iter != node->subNode.begin())
				{
					stack_cds.push(*iter);
					iter--;
				}
				stack_cds.push(*iter);
			}
		}
	}
}

//对函数子树p创建控制依赖子图
//若节点有多个孩子，保存在subNode中
//跳转语句中subNode[0]保存跳转到的节点（创建完树后统一处理）
treeNode *createCDS(treeNode *p)
{
	if(!p)
		return NULL;
	
	while(!stack_cds.empty())
		stack_cds.pop();
			
	stack_cds.push(p);
	
	treeNode *entry_node;
	while(!stack_cds.empty())
	{
		treeNode *node = stack_cds.top();
		stack_cds.pop();
		
		if(node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = node->subNode.end() - 1;
			while(iter != node->subNode.begin())
			{
				stack_cds.push(*iter);
				iter--;
			}
			stack_cds.push(*iter);
		}
		
		treeNode *general_node;
		switch(node->type)
		{
			case typeFuncDef:
				entry_node = new_treeNode(typeEntry, "", -1);
				
				if(node->subNode.size() == 3)
				{
					entry_node->str = mergeSubNodeContent(node->subNode[0])+" "+mergeSubNodeContent(node->subNode[1]);
					entry_node->lineno = line_of_node(node);	//
					entry_node->parentNode = NULL;
			
					jump_node(exCountNode(node->subNode[0])+exCountNode(node->subNode[1]));	//
				}
				else
				{
					entry_node->str = mergeSubNodeContent(node->subNode[0])+" "+mergeSubNodeContent(node->subNode[1])+" "+mergeSubNodeContent(node->subNode[2]);
					entry_node->lineno = line_of_node(node);	//
					entry_node->parentNode = NULL;
			
					jump_node(exCountNode(node->subNode[0])+exCountNode(node->subNode[1])+exCountNode(node->subNode[2]));	//
				}
				//// cout <<endl << stack_cds.top()->str << endl;	//test
				break;
			case typeDeclr:				
				general_node = new_treeNode(typeDeclr, "", -1);
				general_node->str = mergeSubNodeContent(node); 
				general_node->lineno = line_of_node(node);	
				general_node->parentNode = entry_node;
				
				jump_node(exCountNode(node)-1);
				entry_node->subNode.push_back(general_node);
				break;
			case typeIterStm:
				general_node = new_treeNode(typeIterStm, "", -1);
				general_node->str = node->subNode[0]->str;
				general_node->lineno = node->subNode[0]->lineno;
				general_node->parentNode = entry_node;
				
				entry_node->subNode.push_back(general_node);
				
				iter_node(general_node, node);//
				
				break;
			case typeSelecStm:
				general_node = new_treeNode(typeSelecStm, "", -1);
				general_node->str = node->subNode[0]->str;
				general_node->lineno = node->subNode[0]->lineno;
				general_node->parentNode = entry_node;
				
				entry_node->subNode.push_back(general_node);
				
				selec_node(general_node, node);//				
				break;
			case typeExprStm:
				general_node = new_treeNode(typeExprStm, "", -1);
				general_node->str = mergeSubNodeContent(node);
				general_node->lineno = line_of_node(node);
				general_node->parentNode = entry_node;
				
				jump_node(exCountNode(node)-1);
				entry_node->subNode.push_back(general_node);
				break;
			case typeJumpStm:
				general_node = new_treeNode(typeJumpStm, "", -1);
				general_node->str = node->subNode[0]->str;
				general_node->lineno = node->subNode[0]->lineno;
				general_node->parentNode = entry_node;
				
				jump_node(exCountNode(node)-1);
				entry_node->subNode.push_back(general_node);
				break;
			case typeLabelStm:
				general_node = new_treeNode(typeLabelStm, "", -1);
				general_node->str = mergeSubNodeContent(node);
				general_node->lineno = line_of_node(node);
				general_node->parentNode = entry_node;
				
				jump_node(exCountNode(node)-1);	
				entry_node->subNode.push_back(general_node);
				break;
			case typeCompStm:	//只单独处理空的情况
				if(node->subNode.size() == 2)
				{
					general_node = new_treeNode(typeOperator, "", -1);
					general_node->str = ";";
					general_node->lineno = p->subNode[0]->lineno;
					general_node->parentNode = entry_node;
					
					jump_node(2);
					entry_node->subNode.push_back(general_node);
				}
				break;
			default:
				;		
		}
	}
	
	return entry_node;
}

//if结构的所有分支中是否均有break或return语句
bool selecStm_one(treeNode *temp)
{
	if(!temp)
		return false;
		
	vector<treeNode*>::iterator iter;
	treeNode *lchild = temp->subNode[0];
	treeNode *rchild = temp->subNode[1];
	bool flag_l = false;
	bool flag_r = false;
	
	for(iter=lchild->subNode.begin(); iter!=lchild->subNode.end(); iter++)
	{
		if((*iter)->str=="break" || (*iter)->str=="return" )
		{
			flag_l = true;
			break;
		}
	}	
	for(iter=rchild->subNode.begin(); iter!=rchild->subNode.end(); iter++)
	{
		if((*iter)->str=="break" || (*iter)->str=="return" )
		{
			flag_r = true;
			break;
		}
	}
	if(flag_l && flag_r)
		return true;
	
	if(!flag_l)
	{
		for(iter=lchild->subNode.begin(); iter!=lchild->subNode.end(); iter++)
		{
			if((*iter)->str == "if")
			{
				flag_l = selecStm_one(*iter);
				if(flag_l)
					break;
			}
		}
	}
	
	if(!flag_r)
	{
		for(iter=rchild->subNode.begin(); iter!=rchild->subNode.end(); iter++)
		{
			if((*iter)->str == "if")
			{
				flag_r = selecStm_one(*iter);
				if(flag_r)
					break;
			}
		}
	}
	
	if(flag_l && flag_r)
		return true;
		
	return false;
}

//if选择语句的所有分支中都有跳转语句则为true 
bool selecStm_break(treeNode *temp)
{
	if(!temp)
		return false;
		
	vector<treeNode*>::iterator iter;
	treeNode *lchild = temp->subNode[0];
	treeNode *rchild = temp->subNode[1];
	bool flag_l = false;
	bool flag_r = false;
	
	for(iter=lchild->subNode.begin(); iter!=lchild->subNode.end(); iter++)
	{
		if((*iter)->str=="break" || (*iter)->str=="return" || (*iter)->str=="continue")
		{
			flag_l = true;
			break;
		}
	}	
	for(iter=rchild->subNode.begin(); iter!=rchild->subNode.end(); iter++)
	{
		if((*iter)->str=="break" || (*iter)->str=="return" || (*iter)->str=="continue")
		{
			flag_r = true;
			break;
		}
	}
	if(flag_l && flag_r)
		return true;
	
	if(!flag_l)
	{
		for(iter=lchild->subNode.begin(); iter!=lchild->subNode.end(); iter++)
		{
			if((*iter)->str == "if")
			{
				flag_l = selecStm_break(*iter);
				if(flag_l)
					break;
			}
		}
	}
	
	if(!flag_r)
	{
		for(iter=rchild->subNode.begin(); iter!=rchild->subNode.end(); iter++)
		{
			if((*iter)->str == "if")
			{
				flag_r = selecStm_break(*iter);
				if(flag_r)
					break;
			}
		}
	}
	
	if(flag_l && flag_r)
		return true;
		
	return false;	
}

//死代码缺陷检测

void deadCodeDect(treeNode *p)
{
    queue<treeNode*> queue1;
    

	/*while(!queue1.empty())
		queue1.pop();*/
	
	// cout <<endl << "-------------------dead code detection-------------------" << endl;
	
	if(!p)
		return;
	
	ofstream file(DEADCODEDECT_FILE, ios::out | ios::app);	//	
	 
	queue1.push(p);
	while(!queue1.empty())
	{
		//取队头节点
		treeNode *node = queue1.front();
		queue1.pop();
		
		//队头节点的孩子节点入队
		if(node->subNode.size() > 0)	//0519
		{
			vector<treeNode*>::iterator iter;
			for(iter=node->subNode.begin(); iter!=node->subNode.end(); iter++)
			{
				queue1.push(*iter);
			}
		}
		
		//对函数子树进行处理（转化为语句为节点的树并进行死代码缺陷的判断）
		if(node->type == typeFuncDef)
		{
			treeNode *copy_node = node;
			treeNode *entry = createCDS(copy_node);
		//	ex(entry);
			
			//对每个函数的CDS处理（思路--按层遍历该树，从结构上判断冗余）
			//-----------------------------------------------
			
            queue<treeNode*> queue2;
			/*while(!queue2.empty())
				queue2.pop();*/
			
			// cout <<endl << "-------------dead code--------------" << endl;
			
			queue2.push(entry);
			while(!queue2.empty())
			{
				treeNode *temp = queue2.front();
				queue2.pop();
				
				if(temp->subNode.size() > 0)		//0519
				{
					vector<treeNode*>::iterator iter;
					for(iter=temp->subNode.begin(); iter!=temp->subNode.end(); iter++)
					{
						queue2.push(*iter);
					}
				}
				
				switch(temp->type)
				{
					case typeJumpStm:			//若跳转语句节点右边有兄弟节点，则其兄弟节点为冗余节点，	
						if(temp->str != "goto")	//goto 未处理
						{
							if(temp->parentNode)
							{
								treeNode *temp_parent = temp->parentNode;
								if((temp_parent->subNode[temp_parent->subNode.size()-1]->str != temp->str)
									|| (temp_parent->subNode[temp_parent->subNode.size()-1]->lineno != temp->lineno))
								{
									int index = temp_parent->subNode.size() - 1;
									while((temp_parent->subNode[index]->str != temp->str)
										|| (temp_parent->subNode[index]->lineno != temp->lineno))
									{
											
										index--;
									}
									if(temp_parent->subNode[index+1]->type != typeLabelStm)//兄弟节点为label语句除外 0606
										file << temp_parent->subNode[index+1]->lineno << " "
											<< temp_parent->subNode[index+1]->str << endl;
								}
							}
						}
						break;
						
					case typeSelecStm:
						if(temp->str == "if")	
						{	
							if(selecStm_break(temp))	//0301
							{
								treeNode *temp_parent = temp->parentNode;
								if((temp_parent->subNode[temp_parent->subNode.size()-1]->str != "if")
									|| (temp_parent->subNode[temp_parent->subNode.size()-1]->lineno != temp->lineno))
								{
									int index = temp_parent->subNode.size() - 1;
									while((temp_parent->subNode[index]->str != "if")
										|| (temp_parent->subNode[index]->lineno != temp->lineno))
									{
										index--;
									}
									file << temp_parent->subNode[index+1]->lineno << " "
											<< temp_parent->subNode[index+1]->str << endl;
									
								}
							}
						}
						break;
						
					case typeIterStm:	//0524 如果循环体中有单独的return语句则为缺陷：只执行一次循环
						{
							vector<treeNode*>::iterator iter = temp->subNode.begin();
							while(iter != temp->subNode.end())
							{
								//if((*iter)->type == typeJumpStm && ((*iter)->str=="goto" || (*iter)->str=="return"))
								if((*iter)->type == typeJumpStm && (*iter)->str=="return")
								{
									
									file << (*iter)->lineno << " "
										<< (*iter)->str << endl;
									
									break;
								}
								
								if((*iter)->type == typeSelecStm && (*iter)->str == "if")	//0602
								{															//不能含continue
									if(selecStm_one(*iter))
									{
										
										file << (*iter)->lineno << " "
											<< (*iter)->str << endl;
											
										break;
									}
								}
								
								iter++;
							}
						}
						break;
						
					default:
						;
				}	
			}			
			// cout <<"----------------end-----------------" << endl;
			
			freeNode(entry);
			//end---------------------------------------------		
		}
	}	

	file.close();
	// cout <<endl << "-------------------end-------------------" << endl;	
}

//#################################################################################################################################################
//#################################################################################################################################################

stack<treeNode*> stack_cond;
 
//跳过栈stack_cond中的n个节点
void jump_node_stack_cond(int n)
{
	int count = 0;
	while(!stack_cond.empty() && count<n)
	{
		treeNode* node = stack_cond.top();
		stack_cond.pop();
		count++;
		
		if(node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = node->subNode.end() - 1;
			while(iter != node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);		
		}
	}
}

//存储和计算表达式用
typedef struct
{
	string name;
	nodeType type;
	string value;
}expr_or_const_elem;


typedef struct
{
	string var_name;
	vector<expr_or_const_elem> var_value;
	string flag;	//constant expression con_predicate
	int line;
}elem;

vector<elem> array_var;	//存储变量-常量

//从根节点为p的子树中提取表达式存入array_value
void exExpr(treeNode *p, vector<expr_or_const_elem> &var_value)	//
{
    stack<treeNode*> s_exExpr;

	//while(!s_exExpr.empty())
	//{
	//	s_exExpr.pop();
	//}
	
	if(p->parentNode->subNode[1]->str == "*="||p->parentNode->subNode[1]->str == "/="||
	   p->parentNode->subNode[1]->str == "%="||p->parentNode->subNode[1]->str == "+="||
	   p->parentNode->subNode[1]->str == "-="||p->parentNode->subNode[1]->str == "<<="||
	   p->parentNode->subNode[1]->str == ">>="||p->parentNode->subNode[1]->str == "&="||
	   p->parentNode->subNode[1]->str == "^="||p->parentNode->subNode[1]->str == "|=")
	{expr_or_const_elem var_left_elem;
	var_left_elem.name = p->parentNode->subNode[0]->str;
	var_left_elem.type = p->parentNode->subNode[0]->type;
	var_left_elem.value = "unknown";
	
	expr_or_const_elem add_elem;
	add_elem.type = typeOperator;
	add_elem.value = "unknown";
	switch(p->parentNode->subNode[1]->str[0])
	{
		case '*':
			add_elem.name = "*";
			break;
		case '/':
			add_elem.name = "/";
			break;
		case '%':
			add_elem.name = "%";
			break;
		case '+':
			add_elem.name = "+";
			break;
		case '-':
			add_elem.name = "-";
			break;
		case '<':
			add_elem.name = "<<";
			break;
		case '>':
			add_elem.name = ">>";
			break;
		case '&':
			add_elem.name = "&";
			break;
		case '^':
			add_elem.name = "^";
			break;
		case '|':
			add_elem.name = "|";
			break;
		default:
			return;
	}
	var_value.push_back(var_left_elem);
	var_value.push_back(add_elem);	
	}
	
	if(p->parentNode->subNode[1]->str != "=")
	{
		expr_or_const_elem left_kuohao;
		left_kuohao.name = "(";
		left_kuohao.type = typeOperator;
		left_kuohao.value = "unknown";
		var_value.push_back(left_kuohao);
	}
	
	s_exExpr.push(p);
	while(!s_exExpr.empty())
	{
		treeNode *temp_node = s_exExpr.top();
		s_exExpr.pop();
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				s_exExpr.push(*iter);
				iter--;
			}
			s_exExpr.push(*iter);
		}
		
		if(temp_node->subNode.size() == 0)
		{
			expr_or_const_elem temp_elem;
			if(temp_node->type == typeConstant)
			{
				temp_elem.value = temp_node->str;
			}
			else
			{
				temp_elem.name = temp_node->str;				
				if((temp_node->str == "++" || temp_node->str == "--")	//确定++，--在标识符之前还是之后
					&& temp_node->parentNode->type == typeUnaryExpr)
				{
					temp_elem.value = "forward";
				}
				else if(temp_node->parentNode->type == typePostExpr)
				{
					temp_elem.value = "backward";
				}
				else
				{
					temp_elem.value = "unknown";
				}
			}
			temp_elem.type = temp_node->type;
			var_value.push_back(temp_elem);
		}
	}
	
	if(p->parentNode->subNode[1]->str != "=")
	{
		expr_or_const_elem right_kuohao;
		right_kuohao.name = ")";
		right_kuohao.type = typeOperator;
		right_kuohao.value = "unknown";
		var_value.push_back(right_kuohao);
	}
		


}

//运算符的算符优先表
const char operTable[20][20] = {
/*  +  */		'>', '>', '<', '<', '<', '>', ' ', '<', '<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*  -  */		'>', '>', '<', '<', '<', '>', ' ', '<',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*  *  */		'>', '>', '>', '>', '<', '>', ' ', '>',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*  /  */		'>', '>', '>', '>', '<', '>', ' ', '>',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*  (  */		'<', '<', '<', '<', '<', '=', '<', '<',	'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', ' ',
/*  )  */		'>', '>', '>', '>', ' ', '>', ' ', '>',	'>', '>', '>', '>', '>', ' ', '>', '>', ' ', '>', '>', '>',
/*  M  6 */		'>', '>', '>', '>', '<', '>', ' ', '>',	'>', '>', '>', '>', '>', ' ', '>', '>', ' ', '>', '>', '>',
/*  %  */		'>', '>', '>', '>', '<', '>', ' ', '>',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*++,--*/		'>', '>', '>', '>', '<', '>', ' ', '>',	'<', '>', '>', '>', '>', ' ', '>', '>', ' ', '>', '>', '>',
/*<,>,<=,>=*/	'<', '<', '<', '<', '<', '>', '<', '<',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '<', '>',
/*==,!=*/		'<', '<', '<', '<', '<', '>', '<', '<',	'<', '<', '>', '>', '>', '<', '>', '>', '<', '>', '<', '>',
/*	&&	11*/	'<', '<', '<', '<', '<', '>', '<', '<',	'<', '<', '<', '>', '>', '<', '<', '<', '<', '<', '<', '>',
/*	||	*/		'<', '<', '<', '<', '<', '>', '<', '<',	'<', '<', '<', '<', '>', '<', '<', '<', '<', '<', '<', '>',
/*	!	*/		'>', '>', '>', '>', '<', '>', '<', '>',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*	&	*/		'<', '<', '<', '<', '<', '>', '<', '<',	'<', '<', '<', '>', '>', '<', '>', '>', '<', '>', '<', '>',
/*	|	*/		'<', '<', '<', '<', '<', '>', '<', '<',	'<', '<', '<', '>', '>', '<', '<', '>', '<', '>', '<', '>',
/*	~	*/		'>', '>', '>', '>', '<', '>', '<', '>',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*	^	*/		'<', '<', '<', '<', '<', '>', '<', '<',	'<', '<', '<', '>', '>', '<', '<', '>', '<', '>', '<', '>',
/*<<,>>*/		'<', '<', '<', '<', '<', '>', '<', '<',	'<', '>', '>', '>', '>', '<', '>', '>', '<', '>', '>', '>',
/*  #  */		'<', '<', '<', '<', '<', ' ', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '=',
//				 +    -    *    /    (    )   Minus %    ++   <    ==   &&   ||   !    &    |    ~    ^    <<	#
};

//判断运算符栈的栈顶运算符与读入的运算符之间的优先关系
char Precede(expr_or_const_elem a, expr_or_const_elem b)
{
	int x;
	int y;
	
	if(a.value == "minus")	//负号
		x = 6;
	if(b.value == "minus")
		y = 6;
	
	if(a.name == "++" || a.name == "--")
		x = 8;
	if(b.name == "++" || b.name == "--")
		y = 8;
		
	if(a.name == "<" || a.name == ">" || a.name == "<=" || a.name == ">=")
		x = 9;
	if(b.name == "<" || b.name == ">" || b.name == "<=" || b.name == ">=")
		y = 9;
	
	if(a.name == "==" || a.name == "!=")
		x = 10;
	if(b.name == "==" || b.name == "!=")
		y = 10;
	
	if(a.name == "&&")
		x = 11;
	if(b.name == "&&")
		y = 11;
	
	if(a.name == "||")
		x = 12;
	if(b.name == "||")
		y = 12;
	
	if(a.name == "<<" || a.name == ">>")
		x = 18;
	if(b.name == "<<" || b.name == ">>")
		y = 18;
	
	switch(*(a.name.c_str()))
	{
		case '+':
			x = 0;	break;
		case '-':
			x = 1;	break;
		case '*':
			x = 2;	break;
		case '/':
			x = 3;	break;
		case '(':
			x = 4;	break;
		case ')':
			x = 5;	break;
		case '%':
			x = 7;	break;
		case '!':
			x = 13; break;
		case '&':
			x = 14; break;
		case '|':
			x = 15; break;
		case '~':
			x = 16; break;
		case '^':
			x = 17; break;
		case '#':
			x = 19;  break;
		default:
			;
	}
	switch(*(b.name.c_str()))
	{
		case '+':
			y = 0;	break;
		case '-':
			y = 1;	break;
		case '*':
			y = 2;	break;
		case '/':
			y = 3;	break;
		case '(':
			y = 4;	break;
		case ')':
			y = 5;	break;
		case '%':
			y = 7;	break;
		case '!':
			y = 13; break;
		case '&':
			y = 14; break;
		case '|':
			y = 15; break;
		case '~':
			y = 16; break;
		case '^':
			y = 17; break;
		case '#':
			y = 19;  break;
		default:
			;
	}

	return operTable[x][y];
}

//二元运算
string Operator(expr_or_const_elem a_elem,
				expr_or_const_elem theta_elem,
				expr_or_const_elem b_elem)
{
	int result;
	
	int a_num = atoi(a_elem.value.c_str());
	int b_num = atoi(b_elem.value.c_str());
	
	char buf[10];
	
	if(theta_elem.name == "<=")
	{
		result = a_num <= b_num;
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == ">=")
	{
		result = a_num >= b_num;
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == "==")
	{
		//result = a_num == b_num;	
		result = a_elem.value == b_elem.value;	//防止非二进制数赋值
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == "!=")
	{
		//result = a_num != b_num;
		result = a_elem.value  != b_elem.value;
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == "&&")
	{
		result = a_num && b_num;
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == "||")
	{
		result = a_num || b_num;
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == "<<")
	{
		result = a_num << b_num;
		return itoa(result, buf, 10);
	}
	if(theta_elem.name == ">>")
	{
		result = a_num >> b_num;
		return itoa(result, buf, 10);
	}

	switch(*(theta_elem.name.c_str()))
	{
		case '+':
			result = atoi(a_elem.value.c_str()) + atoi(b_elem.value.c_str());
			break;
		case '-':
			result = atoi(a_elem.value.c_str()) - atoi(b_elem.value.c_str());
			break;
		case '*':
			result = atoi(a_elem.value.c_str()) * atoi(b_elem.value.c_str());
			break;
		case '/':
			result = atoi(a_elem.value.c_str()) / atoi(b_elem.value.c_str());
			break;
		case '%':
			result = atoi(a_elem.value.c_str()) % atoi(b_elem.value.c_str());
			break;
		case '<':
			result = a_num < b_num;
			break;
		case '>':
			result = a_num > b_num;
			break;
		case '&':
			result = a_num & b_num;
			break;
		case '|':
			result = a_num | b_num;
			break;
		case '^':
			result = a_num ^ b_num;
			break;
		default:
			;
	}
	
	return itoa(result, buf, 10);
}

//如果表达式可以计算，则计算表达式的值；否则返回"cannot_calculate"
string calculate_expr(vector<elem> &array_var)
{
    if(array_var.size() < 2)
	{
		return "cannot_calculate";
	}	
	
//	//cout<<array_var.size()<<endl;
	
	
	elem expr_elem = array_var[array_var.size()-1];	//copy
	vector<expr_or_const_elem> expr_right = expr_elem.var_value;
	 
	//0428
	for(int j=0; j<expr_right.size(); j++)
	{
		if(expr_right[j].name == "NULL" || expr_right[j].name == "false")
		{
			expr_right[j].type = typeConstant;
			expr_right[j].value = "0";
		}
		if(expr_right[j].name == "true")
		{
			expr_right[j].type = typeConstant;
			expr_right[j].value = "1";
		}
	}
	
	//如果现存的条件表达式和已存的某个条件表达式相同，则为缺陷	0517
	if(array_var.size() > 1 && expr_elem.flag == "con_predicate")
	{
		for(int m=0; m<array_var.size()-1; m++)
		{
			if(array_var[m].flag == "con_predicate")
			{
				if(array_var[m].var_value.size() == expr_right.size())
				{
					int i;
					for(i=0; i<expr_right.size(); i++)
					{
						if(expr_right[i].type == typeConstant)
						{
							if(expr_right[i].value != array_var[m].var_value[i].value)
							{
								break;
							}
						}
						else if(expr_right[i].name != array_var[m].var_value[i].name)
						{
							break;
						}
					}
					if(i == expr_right.size())
					{
						return "1";
					}
				}
			}
		}
	}

	//判断是否可以计算
	int i;
	for(i=0; i<expr_right.size(); i++)
	{
		if(expr_right[i].type == typeId)
		{
			int j;
			for(j=0; j<array_var.size()-1; j++)
			{
				if(array_var[j].var_name == expr_right[i].name
					&& array_var[j].flag == "constant")
				{
					break;
				}
			}
			
			if(j == array_var.size()-1)
			{
				return "cannot_calculate";
			}
		}
	}
		 
	//计算表达式的值
	
	//确定变量的值；并找出负号，将value值设为"minus"
	for(i=0; i<expr_right.size(); i++)
	{
		if(expr_right[i].type == typeId)
		{
			for(int j=0; j<array_var.size()-1; j++)
			{
				if(array_var[j].var_name == expr_right[i].name
					&& array_var[j].flag == "constant")
				{
					expr_right[i].value = array_var[j].var_value[0].value;
					break;
				}
			}
			
		}
		
		if(expr_right[i].name == "-"
			&& (i==0 || expr_right[i-1].type==typeOperator))
		{
			expr_right[i].value = "minus";
		}
	}

	//去掉正号'+'
	vector<expr_or_const_elem>::iterator iter = expr_right.begin();
	while(iter != expr_right.end())
	{
		if((*iter).name == "+"
			&& (iter == expr_right.begin() || (*(iter-1)).type == typeOperator))
		{
			iter = expr_right.erase(iter);
		}
		iter++;
	}

	if(expr_right.size() == 1)
	{
		return expr_right[0].value;
	}
	
	//为表达式的末尾加‘#’
	expr_or_const_elem _elem;
	_elem.name = "#";
	_elem.type = typeOperator;
	_elem.value = "unknown";
	
	expr_right.push_back(_elem);
    stack<expr_or_const_elem> s_opnd;	//计算表达式所用的操作数栈
    stack<expr_or_const_elem> s_optr;	//计算表达式所用的操作符栈
	/*while(!s_optr.empty())
		s_optr.pop();
	while(!s_opnd.empty())
		s_opnd.pop();*/
	
	//计算表达式的值
	s_optr.push(_elem);
	int index = 0;
	expr_or_const_elem c_elem = expr_right[index];
	index++;
//	//cout<<"222222222222222"<<endl;
	while(c_elem.name!="#" || s_optr.top().name!="#")
	{
		if(c_elem.type != typeOperator)
		{
			s_opnd.push(c_elem);
			c_elem = expr_right[index];
			index++;
		}
		else
		{
			switch(Precede(s_optr.top(), c_elem))	//####
			{
				case '<':
					s_optr.push(c_elem);
					c_elem = expr_right[index];
					index++;
					break;
				case '=':
					s_optr.pop();
					c_elem = expr_right[index];
					index++;
					break;
				case '>':
					expr_or_const_elem result_elem;
					result_elem.type = typeConstant;
					
					expr_or_const_elem theta_elem = s_optr.top();
					s_optr.pop();
					char buf[10];
					if(theta_elem.value == "minus")	// - 负号
					{
						expr_or_const_elem temp_elem = s_opnd.top();
						s_opnd.pop();
						int result = 0 - atoi(temp_elem.value.c_str());
						result_elem.value = itoa(result, buf, 10);
					}
					else if(theta_elem.value == "forward")
					{
						if(theta_elem.name == "++")	// ++var;
						{
							expr_or_const_elem temp_elem = s_opnd.top();
							s_opnd.pop();
							int result = atoi(temp_elem.value.c_str()) + 1;
							result_elem.value = itoa(result, buf, 10);
						}
						else	// --var;
						{
							expr_or_const_elem temp_elem = s_opnd.top();
							s_opnd.pop();
							int result = atoi(temp_elem.value.c_str()) - 1;
							result_elem.value = itoa(result, buf, 10);
						}
					}
					else if(theta_elem.value == "backward")	//var++; var--;
					{
						expr_or_const_elem temp_elem = s_opnd.top();
						s_opnd.pop();
						result_elem.value = temp_elem.value;
					}
					else if(theta_elem.name == "!")	// !var;
					{
						expr_or_const_elem temp_elem = s_opnd.top();
						s_opnd.pop();
						int result = !(atoi(temp_elem.value.c_str()));
						result_elem.value = itoa(result, buf, 10); 
					}
					else if(theta_elem.name == "~")	//~var;
					{
						expr_or_const_elem temp_elem = s_opnd.top();
						s_opnd.pop();
						int result = ~(atoi(temp_elem.value.c_str()));
						result_elem.value = itoa(result, buf, 10);
					}
					else
					{
						expr_or_const_elem b_elem = s_opnd.top();
						s_opnd.pop();
						expr_or_const_elem a_elem = s_opnd.top();
						s_opnd.pop();
						
						result_elem.value = Operator(a_elem, theta_elem, b_elem);	//####
					}
					s_opnd.push(result_elem);
					break;
			}
		}
	}

	expr_or_const_elem result_elem = s_opnd.top();
	s_opnd.pop();
	return result_elem.value;	 
}

//调整容器array_var
//如果刚存入的赋值的右边为常量，则删除之前的与左边变量同名的变量的记录；
//如果刚存入的赋值的右边为表达式，则尝试用之前已知的值计算该表达式，并删除之前的同名被赋值变量的记录。
//删除之前已存的含该赋值语句左边变量的表达式
void adjust_array_var(vector<elem> &array_var)
{
	int current_index = array_var.size() - 1;
	elem current_elem = array_var[current_index];
	
	if(array_var.size() < 2)
		return;
	
	if(current_elem.flag == "constant")
	{
		if(array_var.size() > 1)	//0513
		{
			vector<elem>::iterator iter;
			for(iter=array_var.begin(); iter!=array_var.end()-1; iter++)
			{
				if((*iter).var_name == array_var[current_index].var_name)
				{
					array_var.erase(iter);
					current_index--;
					break;
				}
			}
		}
	}
	else if(current_elem.flag == "expression")
	{
		//0503 var1 = var2 = constant; 暂时简单处理
		int i;
		for(i=0; i<current_elem.var_value.size(); i++)
		{
			if(current_elem.var_value[i].name == "=")
			{
				break;
			}
		}
		if(i != current_elem.var_value.size())
		{
			array_var.erase(array_var.end() - 1);
			return;
		}
		
		
		string result = calculate_expr(array_var); 
		
		if(result != "cannot_calculate")
		{						
			elem temp_elem;
			temp_elem.var_name = current_elem.var_name;
			temp_elem.line = current_elem.line;	//
			expr_or_const_elem temp;
			temp.value = result;
			temp_elem.var_value.push_back(temp);
			temp_elem.flag = "constant";
			array_var.push_back(temp_elem);
			
			current_elem.var_value.clear();
			array_var.erase(array_var.end() - 2);	//
		}
		
		if(array_var.size() > 1)	//0513
		{
			vector<elem>::iterator iter;
			for(iter=array_var.begin(); iter!=array_var.end()-1; iter++)
			{
				if((*iter).var_name == array_var[array_var.size()-1].var_name)
				{
					array_var.erase(iter); 
					break;
				}
			}
		}
	}
	
	//删除之前已存的含该赋值语句左边变量的表达式	//0506
	if(array_var.size() > 1)	//0513
	{
		string var_left = array_var[array_var.size()-1].var_name;
		vector<elem>::iterator iter = array_var.begin();
		while(iter != array_var.end() - 1)
		{
			if((*iter).flag == "con_predicate")		//0517
			{
				iter++;
				continue;
			}
			
			int i;
			for(i=0; i<(*iter).var_value.size(); i++)
			{
				if(((*iter).var_value)[i].type == typeId
					&& var_left == ((*iter).var_value)[i].name)
				{	
					break;
				}
			}
			if(i != (*iter).var_value.size())
			{
				iter = array_var.erase(iter);
			}
			else
				iter++;
		}
	}
}

void process_unary_expr(treeNode *p,vector<elem> &array_var)
{
	if(!p)
		return;
    stack<node*> s_unary_post;		
	/*while(!s_unary_post.empty())
		s_unary_post.pop();*/
	
	s_unary_post.push(p);
	while(!s_unary_post.empty())
	{
		treeNode *temp_node = s_unary_post.top();
		s_unary_post.pop();
		
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				s_unary_post.push(*iter);
				iter--;
			}
			s_unary_post.push(*iter);
		}
		
		if(temp_node->type == typeId)
		{
			//如果++var; --var; 是在循环体中，则删除该变量在array_var中的记录	//0428
					
			for(int i=0; i<array_var.size(); i++)
			{
				if(array_var[i].var_name == temp_node->str)
				{
					if(array_var[i].flag == "constant")
					{
						if(p->subNode[0]->str == "++")
						{
							int num = atoi(array_var[i].var_value[0].value.c_str()) + 1;
							char buf[10];
							array_var[i].var_value[0].value = itoa(num, buf, 10);
						}
						else
						{
							int num = atoi(array_var[i].var_value[0].value.c_str()) - 1;
							char buf[10];
							array_var[i].var_value[0].value = itoa(num, buf, 10);
						}
					}
					if(array_var[i].flag == "expression")
					{
						expr_or_const_elem temp_elem;
						temp_elem.name = "(";
						temp_elem.type = typeOperator;
						temp_elem.value = "unknown";
						array_var[i].var_value.insert(array_var[i].var_value.begin(), temp_elem);
						
						temp_elem.name = ")";
						temp_elem.type = typeOperator;
						temp_elem.value = "unknown";
						array_var[i].var_value.push_back(temp_elem);
						
						if(p->subNode[0]->str == "++")
						{
							temp_elem.name = "+";
							temp_elem.type = typeOperator;
							temp_elem.value = "unknown";
							array_var[i].var_value.push_back(temp_elem);
						}
						else
						{
							temp_elem.name = "-";
							temp_elem.type = typeOperator;
							temp_elem.value = "unknown";
							array_var[i].var_value.push_back(temp_elem);
						}
						
						temp_elem.name = "";
						temp_elem.type = typeConstant;
						temp_elem.value = "1";
						array_var[i].var_value.push_back(temp_elem);
					}
				}
			}
			return;
		}
		
	}
}

void process_post_expr(treeNode *p,vector<elem> &array_var)
{
	if(!p)
		return;
	stack<node*> s_unary_post;
	/*while(!s_unary_post.empty())
		s_unary_post.pop();*/
	
	s_unary_post.push(p);
	while(!s_unary_post.empty())
	{
		treeNode *temp_node = s_unary_post.top();
		s_unary_post.pop();
		
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				s_unary_post.push(*iter);
				iter--;
			}
			s_unary_post.push(*iter);
		}
		
		if(temp_node->type == typeId)
		{
			//如果var++; var++; 是在循环体中，则删除该变量在array_var中的记录	//0428
			
			for(int i=0; i<array_var.size(); i++)
			{
				if(array_var[i].var_name == temp_node->str)
				{
					if(array_var[i].flag == "constant")
					{
						if(p->subNode[1]->str == "++")
						{
							int num = atoi(array_var[i].var_value[0].value.c_str()) + 1;
							char buf[10];
							array_var[i].var_value[0].value = itoa(num, buf, 10);
						}
						else
						{
							int num = atoi(array_var[i].var_value[0].value.c_str()) - 1;
							char buf[10];
							array_var[i].var_value[0].value = itoa(num, buf, 10);
						}
					}
					if(array_var[i].flag == "expression")
					{
						expr_or_const_elem temp_elem;
						temp_elem.name = "(";
						temp_elem.type = typeOperator;
						temp_elem.value = "unknown";
						array_var[i].var_value.insert(array_var[i].var_value.begin(), temp_elem);
						
						temp_elem.name = ")";
						temp_elem.type = typeOperator;
						temp_elem.value = "unknown";
						array_var[i].var_value.push_back(temp_elem);
						
						if(p->subNode[1]->str == "++")
						{
							temp_elem.name = "+";
							temp_elem.type = typeOperator;
							temp_elem.value = "unknown";
							array_var[i].var_value.push_back(temp_elem);
						}
						else
						{
							temp_elem.name = "-";
							temp_elem.type = typeOperator;
							temp_elem.value = "unknown";
							array_var[i].var_value.push_back(temp_elem);
						}
						
						temp_elem.name = "";
						temp_elem.type = typeConstant;
						temp_elem.value = "1";
						array_var[i].var_value.push_back(temp_elem);
					}
				}
			}
			return;
		}
		
	}
}

//对条件表达式分析，得出其中确定的变量-常量绑定
//删除之前已存的同名的变量记录
void analyze_cond_expr(vector<elem> &array_var)
{
	bool flag = true;

	int last_index = array_var.size() - 1;
	vector<expr_or_const_elem> &array_expr = array_var[last_index].var_value;
	
	//只有一个标识符 (var)
	if(array_expr.size() == 3
		&& array_expr[1].type == typeId)
	{	
		elem new_elem;
		new_elem.var_name = array_expr[1].name;
		new_elem.flag = "constant";
		
		expr_or_const_elem temp_elem;
		temp_elem.value = "1";
		
		new_elem.var_value.push_back(temp_elem);
		
		array_var.erase(array_var.end() - 1);
		array_var.push_back(new_elem);

		flag = false;
		//return;
	}
	
	//	(!var)
	if(array_expr.size() == 4
		&& array_expr[1].name == "!" && array_expr[2].type == typeId)
	{
		elem new_elem;
		new_elem.var_name = array_expr[2].name;
		new_elem.flag = "constant";
		
		expr_or_const_elem temp_elem;
		temp_elem.value = "0";
		
		new_elem.var_value.push_back(temp_elem);
		
		array_var.erase(array_var.end() - 1);
		array_var.push_back(new_elem);
		
		flag = false;
		//return;		
	}
	
	//	(var == constant)	(var != constant)
	if(array_expr.size() == 5
		&& array_expr[1].type == typeId
		&& (array_expr[2].name == "==" )//  gdd|| array_expr[2].name == "!=")
		&& array_expr[3].type == typeConstant)
	{
		elem new_elem;
		new_elem.var_name = array_expr[1].name;
		new_elem.flag = "constant";
		
		expr_or_const_elem temp_elem;
		temp_elem.value = array_expr[3].value;
		
		new_elem.var_value.push_back(temp_elem);
		
		array_var.erase(array_var.end() - 1);
		array_var.push_back(new_elem);
		
		flag = false;
		//return;	
	}	
	
	//添加关系运算分析	
	
	//	(var1 == var2)	(var1 != var2)

	//删除之前已存的同名的变量记录	//0508
	if(!flag && array_var.size() > 1)
	{
		string str_left = array_var[array_var.size() - 1].var_name;
		if(array_var.size() > 1)		//0519
		{
			vector<elem>::iterator iter;	
			for(iter=array_var.begin(); iter!=array_var.end()-1; iter++)
			{	
				if(str_left == (*iter).var_name)
				{	
					array_var.erase(iter); 
					break;
				}
			}
		}
	}

}

//冗余条件表达式缺陷检测
void reduCondDect(treeNode *p)
{
	if(!p)
		return;

	while(!stack_cond.empty())
		stack_cond.pop();
		
	stack_cond.push(p);
	treeNode *temp_node = NULL;
	
	ofstream file(REDUCONDDECT_FILE, ios::out | ios::app);
	
	// cout <<endl << "-------------------redu cond detection-------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	
	//缺陷检测开始
	while(!stack_cond.empty())
	{
		temp_node = stack_cond.top();
		stack_cond.pop();
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		if(temp_node->type == typeFuncDef)
		{
			// cout <<"	---------print------------" << endl;
			//print_array_var(array_var);
			// cout <<"--------------------------------------------------------" << endl;
			array_var.clear();
		}
		
		//跟踪赋值语句，获得变量-常量绑定
		//带有赋值操作的节点assignment_expression init_declarator
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{
			//保存整型变量和值
			if(temp_node->subNode[0]->type == typeId)	//varname...
			{
				//如果变量的定义在循环体中的话，则删除该变量在array_var中的记录 //0428
				treeNode *node = temp_node;
				while(node->parentNode && node->parentNode->type != typeIterStm)
				{
					node = node->parentNode;
				}
				if(node->parentNode)
				{
					string var_name = temp_node->subNode[0]->str;
					if(array_var.size() > 0)
					{
						vector<elem>::iterator iter = array_var.begin();
						while(iter != array_var.end())
						{
							if((*iter).var_name == var_name)
							{
								array_var.erase(iter);
								break;
							}
							iter++;
						}
					}
					continue;
				}			
			
				//先存入容器，然后利用以前已知的计算该表达式值，然后再删除之前同名的变量记录
				//解决 x = x + 1;
				
				elem new_elem;
				new_elem.var_name = temp_node->subNode[0]->str;
				new_elem.line = temp_node->subNode[0]->lineno;	//
				
				if(temp_node->subNode[2]->type == typeConstant
					&& temp_node->subNode[1]->str == "=")
				{
					expr_or_const_elem temp_elem;
					temp_elem.value = temp_node->subNode[2]->str;					
					new_elem.var_value.push_back(temp_elem);					
					new_elem.flag = "constant";
				}
				else
				{
					//从根节点为temp_node的子树中提取表达式存入array_value
					exExpr(temp_node->subNode[2], new_elem.var_value);					
					new_elem.flag = "expression";
				
				}
			
				array_var.push_back(new_elem);
				
				//调整容器array_var
				//如果刚存入的赋值的右边为常量，则删除之前的与左边变量同名的变量的记录；
				//如果刚存入的赋值的右边为表达式，则尝试用之前已知的值计算该表达式，并删除之前的同名被赋值变量的记录。
				//删除之前已存的含该赋值语句左边变量的表达式
				adjust_array_var(array_var);
				
				//跳过处理过的该子树的节点
				//jump_node_stack_cond(exCountNode(temp_node) - 1);
				//jump_node_stack_cond(2);	//
			}
		
		
			//保存指针变量和值 //0428
			if(temp_node->subNode[0]->subNode.size() == 2
				&& temp_node->subNode[0]->subNode[0]->str == "*")	//*varname...
			{
				//如果变量的定义在循环体中的话，则删除该变量在array_var中的记录 //0428
				treeNode *node = temp_node;
				while(node->parentNode && node->parentNode->type != typeIterStm)
				{
					node = node->parentNode;
				}
				if(node->parentNode)
				{
					string var_name = temp_node->subNode[0]->subNode[1]->str;	//
					if(array_var.size() > 0)		//0519
					{
						vector<elem>::iterator iter = array_var.begin();
						while(iter != array_var.end())
						{
							if((*iter).var_name == var_name)
							{
								array_var.erase(iter);
								break;
							}
							iter++;
						}
					}
					continue;
				}			
			
			
				//先存入容器，然后利用以前已知的计算该表达式值，然后再删除之前同名的变量记录
				//解决 x = x + 1;
				
				elem new_elem;
				new_elem.var_name = temp_node->subNode[0]->subNode[1]->str;
				new_elem.line = temp_node->subNode[0]->subNode[1]->lineno;	//
				
				if(temp_node->subNode[2]->type == typeConstant
					&& temp_node->subNode[1]->str == "=")
				{
					expr_or_const_elem temp_elem;
					temp_elem.value = temp_node->subNode[2]->str;					
					new_elem.var_value.push_back(temp_elem);					
					new_elem.flag = "constant";
				}
				else
				{
					//从根节点为temp_node的子树中提取表达式存入array_value
					exExpr(temp_node->subNode[2], new_elem.var_value);					
					new_elem.flag = "expression";
				}
				
				array_var.push_back(new_elem);
				
				//调整容器array_var
				//如果刚存入的赋值的右边为常量，则删除之前的与左边变量同名的变量的记录；
				//如果刚存入的赋值的右边为表达式，则尝试用之前已知的值计算该表达式，并删除之前的同名被赋值变量的记录。
				//删除之前已存的含该赋值语句左边变量的表达式
				adjust_array_var(array_var);
				
				//跳过处理过的该子树的节点
				//jump_node_stack_cond(exCountNode(temp_node) - 1);
			}
		}
		
		//++variable; --variable;
		if(temp_node->type == typeUnaryExpr
			&& (temp_node->subNode[0]->str=="++" || temp_node->subNode[0]->str=="--"))
		{
			process_unary_expr(temp_node,array_var);	
		}
		//variable++; variable--;
		if(temp_node->type == typePostExpr
			&& (temp_node->subNode[1]->str=="++" || temp_node->subNode[1]->str=="--"))
		{
			process_post_expr(temp_node,array_var);
		}		
		
		
		//跟踪条件表达式，计算表达式的值判断缺陷，或获得变量-常量绑定 "!=" "==" "!"
		//收集条件谓词
		//条件谓词中跟踪变量的常界限
		if(temp_node->type == typeSelecStm)	//subNode[2] == expression
		{
			//IF '(' expression ')' statement %prec IFX		
			//IF '(' expression ')' statement ELSE statement
			//SWITCH '(' expression ')' statement
			
			elem new_elem;
			new_elem.var_name = "conditional_predicate";
			new_elem.flag = "con_predicate";
			new_elem.line = temp_node->subNode[0]->lineno;	//
			exExpr(temp_node->subNode[2], new_elem.var_value);
			
			array_var.push_back(new_elem);
			
			//判断条件表达式是否为永真或永假
			if(calculate_expr(array_var) != "cannot_calculate")
			{
				string result = calculate_expr(array_var);
				int num = atoi(result.c_str());	//0505
				if(!num && temp_node->parentNode
					&& temp_node->parentNode->type == typeSelecStm)	//0504
				{
					;
				}
				else
				{
					// cout <<"		*** reduCondBug: " <<  new_elem.line << endl;
					
					//保存到缺陷文件中
					string str;
					for(int i=0; i<new_elem.var_value.size(); i++)
					{
						if(new_elem.var_value[i].type == typeConstant)
						{
							str += new_elem.var_value[i].value;
						}
						else
						{
							str += new_elem.var_value[i].name;
						}
					}
					file << new_elem.line << " " << str << endl;
				}
				
				array_var.erase(array_var.end() - 1);
			}
			else
			{
				//对条件表达式分析，得出其中确定的变量-常量绑定
				analyze_cond_expr(array_var);
			}
					
			//// cout <<"@@@selec   " << calculate_expr() << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@
		}
		if(temp_node->type == typeIterStm)
		{
			//暂不处理do_while
		/*	if(temp_node->subNode[0]->str=="do")	//DO statement WHILE '(' expression ')' ';'
			{
				elem new_elem;
				new_elem.var_name = "conditional_predicate";
				new_elem.flag = "con_predicate";
				exExpr(temp_node->subNode[4], new_elem.var_value);
				array_var.push_back(new_elem);				
			}
		*/	
			if(temp_node->subNode[0]->str == "while")	//WHILE '(' expression ')' statement
			{
				//while(1)	0506
				if(temp_node->subNode[2]->type == typeConstant)
				{
					int num = atoi((temp_node->subNode[2]->str).c_str());
					if(num != 0)
					{
						continue;
					}
				}
			
				elem new_elem;
				new_elem.var_name = "conditional_predicate";
				new_elem.flag = "con_predicate";
				new_elem.line = temp_node->subNode[0]->lineno;	//
				exExpr(temp_node->subNode[2], new_elem.var_value);	//
				
				array_var.push_back(new_elem);
				
				//判断条件表达式是否为永真或永假
				if(calculate_expr(array_var) != "cannot_calculate")
				{
					string result = calculate_expr(array_var); 
					int num = atoi(result.c_str());
					if(!num)	//0510
					{
				
						// cout <<"		*** reduCondBug: " <<  new_elem.line << endl;
						
						//保存到缺陷文件中
						string str;
						for(int i=0; i<new_elem.var_value.size(); i++)
						{
							if(new_elem.var_value[i].type == typeConstant)
							{
								str += new_elem.var_value[i].value;
							}
							else
							{
								str += new_elem.var_value[i].name;
							}
						}
						file << new_elem.line << " " << str << endl;
						
						array_var.erase(array_var.end() - 1);
					}
				}
				else
				{
					//对条件表达式分析，得出其中确定的变量-常量绑定
					analyze_cond_expr(array_var);
				}
			
				//// cout <<"@@@iter   " << calculate_expr() << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@
			}
		}
		
		
	}
	
//	// cout <<"	---------print------------" << endl;
//	//print_array_var(array_var);	//test
	
	file.close();
	
	// cout <<"------------------------------------------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	// cout <<endl << "-------------------------end-----------------------------" << endl;
	
}

void process_if_stm(treeNode *temp_noden, vector<elem> &parent_array_var,vector<elem> &array_var);
void process_iter_stm(treeNode *temp_node, vector<elem> &parent_array_var,vector<elem> &array_var);
void process_else_stm(treeNode *temp_noden, vector<elem> &parent_array_var,vector<elem> &array_var); //gdd
void delete_elem(vector<elem> &parent_array_var,vector<elem> &array_var);//gdd
bool find_delete_elem(vector<elem> &array_var,elem &temp_var);

bool find_delete_elem(vector<elem> &array_var,elem &temp_var)
{
    for(int i=0; i<array_var.size(); i++)
    {
        if(array_var[i].var_name==temp_var.var_name 
           &&array_var[i].flag==temp_var.flag
           &&array_var[i].line==temp_var.line
           &&array_var[i].var_value.size()==temp_var.var_value.size())
           {
               if(temp_var.var_value.size()==0)
                   return true;
               
               for(int j=0;j<temp_var.var_value.size();j++)
               {
                   if(temp_var.var_value[j].type == typeConstant)
					{
						if(temp_var.var_value[j].value != array_var[i].var_value[j].value)
						{
							break;
						}
					}
					else if(temp_var.var_value[j].name != array_var[i].var_value[j].name)
					{
						break;
					}               
               }
               if(i == temp_var.var_value.size())
					{
						return true;
					}
           }
    }
return false;
}
void delete_elem(vector<elem> &parent_array_var,vector<elem> &array_var)
{
    for(int i=0; i<parent_array_var.size(); i++)
    {
       if(!find_delete_elem(array_var,parent_array_var[i]))
           parent_array_var.erase(parent_array_var.begin()+i) ;         
    }

}
//提取条件表达式，如果可以计算则为缺陷。删除记录。
//对switch语句体简单处理，只寻找其中的赋值，初始化，前缀和后缀节点，删除涉及到的之前保存的变量记录
void process_switch_stm(treeNode *temp_node, vector<elem> &parent_array_var,vector<elem> &array_var)
{
	ofstream file(REDUCONDDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_node);
	int count = 0;
	stack<treeNode*> s_unary_post;
	vector<elem> array_var_copy = parent_array_var;
	//提取表达式并存入array_var
	elem new_elem;
	new_elem.var_name = "conditional_predicate";
	new_elem.flag = "con_predicate";
	new_elem.line = temp_node->subNode[0]->lineno;
	exExpr(temp_node->subNode[2], new_elem.var_value);
	array_var_copy.push_back(new_elem);
	
	//判断表达式是否可以计算
	string result = calculate_expr(array_var_copy);
	if(result != "cannot_calculate")	//可以计算
	{
		// cout <<"		*** reduCondBug: " <<  new_elem.line << endl;
							
		//保存到缺陷文件中
		string str;
		for(int i=0; i<new_elem.var_value.size(); i++)
		{
			if(new_elem.var_value[i].type == typeConstant)
			{
				str += new_elem.var_value[i].value;
			}
			else
			{
				str += new_elem.var_value[i].name;
			}
		}
		file << new_elem.line << " " << str << endl;	
	}
	
	array_var_copy.erase(array_var_copy.end() - 1);	//
	
	//跳过条件表达式节点
	int count_expr = exCountNode(temp_node->subNode[2]);
	int count_jump_node = count_expr + 3;
	count += count_jump_node;
	jump_node_stack_cond(count_jump_node);
	
	//对switch语句体简单处理，只寻找其中的赋值，初始化，前缀和后缀节点，删除涉及到的之前保存的变量记录
	while(!stack_cond.empty() && count < count_node-1)
	{
		treeNode *temp_node = stack_cond.top();
		stack_cond.pop();
		count++;
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		switch(temp_node->type)
		{
			case typeAssignExpr:
			case typeInitDec:
				if(temp_node->subNode[0]->type == typeId)
				{
					string var_name = temp_node->subNode[0]->str;
					if(array_var.size() > 0)
					{
						vector<elem>::iterator iter = array_var.begin();
						while(iter != array_var.end())
						{
							if((*iter).var_name == var_name)
							{
								array_var.erase(iter);
								break;
							}
							iter++;
						}
					}
					
					if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end())
						{
							if((*iter).var_name == var_name)
							{
								array_var_copy.erase(iter);
								break;
							}
							iter++;
						}
					}
					
					break;
				}
			
				break;
			
			case typeUnaryExpr:	// ++var; --var;
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					while(!s_unary_post.empty())
						s_unary_post.pop();
					s_unary_post.push(temp_node);
					while(!s_unary_post.empty())
					{
						treeNode *temp = s_unary_post.top();
						s_unary_post.pop();
						if(temp->subNode.size() > 0)
						{
							vector<node*>::iterator iter = temp->subNode.end() - 1;
							while(iter != temp->subNode.begin())
							{
								s_unary_post.push(*iter);
								iter--;
							}
							s_unary_post.push(*iter);
						}
						if(temp->type == typeId)
						{
							string var_name = temp->str;
							if(array_var.size() > 0)
							{
								vector<elem>::iterator iter = array_var.begin();
								while(iter != array_var.end())
								{
									if((*iter).var_name == var_name)
									{
										array_var.erase(iter);
										break;
									}
									iter++;
								}
							}
							if(array_var_copy.size() > 0)
							{
								vector<elem>::iterator iter = array_var_copy.begin();
								while(iter != array_var_copy.end())
								{
									if((*iter).var_name == var_name)
									{
										array_var_copy.erase(iter);
										break;
									}
									iter++;
								}
							}
						}
						
					}
				}	
				break;
			case typePostExpr:	// var++; var--;
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					while(!s_unary_post.empty())
						s_unary_post.pop();
					s_unary_post.push(temp_node);
					while(!s_unary_post.empty())
					{
						treeNode *temp = s_unary_post.top();
						s_unary_post.pop();
						if(temp->subNode.size() > 0)
						{
							vector<node*>::iterator iter = temp->subNode.end() - 1;
							while(iter != temp->subNode.begin())
							{
								s_unary_post.push(*iter);
								iter--;
							}
							s_unary_post.push(*iter);
						}
						if(temp->type == typeId)
						{
							string var_name = temp->str;
							if(array_var.size() > 0)		//0519
							{
								vector<elem>::iterator iter = array_var.begin();
								while(iter != array_var.end())
								{
									if((*iter).var_name == var_name)
									{
										array_var.erase(iter);
										break;
									}
									iter++;
								}
							}
							if(array_var_copy.size() > 0)
							{
								vector<elem>::iterator iter = array_var_copy.begin();
								while(iter != array_var_copy.end())
								{
									if((*iter).var_name == var_name)
									{
										array_var_copy.erase(iter);
										break;
									}
									iter++;
								}
							}
						}
						
					}
				}
				break;
			case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
					{
						process_if_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
								
					}
					else
					{
						process_switch_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
					}
					break;
				case typeIterStm:
					process_iter_stm(temp_node, array_var_copy,array_var);	//
					
					count += exCountNode(temp_node);
					
					break;	
			case typeLabelStm: //case default
		        if(temp_node->subNode[0]->str == "case" || temp_node->subNode[0]->str == "default")
				{
				//删除parent_array_var中array_var_copy中没有的元素
	               delete_elem(parent_array_var,array_var_copy);
	
	             //还原array_var
	             array_var_copy.clear();
	               array_var_copy = parent_array_var;	
				}	
					
			default:
				;		
		}
	}
}

//提取循环条件表达式，判断是否为缺陷，是则保存缺陷。否则保存条件表达式。
//在循环体内按照同样的规则找冗余条件缺陷
//对于循环体的外部来说，只是删除了在循环体中出现的赋值，初始化，前缀和后缀节点的相关变量的记录
void process_iter_stm(treeNode *temp_node, vector<elem> &parent_array_var, vector<elem> &array_var)
{
	//备份array_var
	vector<elem> array_var_copy = parent_array_var;
	stack<treeNode*> s_unary_post;
	ofstream file(REDUCONDDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_node);
	int count = 0;
	
	int flag_while = false;	//0604
	elem while_elem;
	

	if(temp_node->subNode[0]->str == "while")
	{
           
		//提取循环条件表达式，判断是否为缺陷，是则保存缺陷，否则不记录
		elem new_elem;
		new_elem.var_name = "conditional_predicate";
		new_elem.flag = "con_predicate";
		new_elem.line = temp_node->subNode[0]->lineno;
		exExpr(temp_node->subNode[2], new_elem.var_value);
		array_var_copy.push_back(new_elem);
		
		while_elem = new_elem;	//0604
		flag_while = true;
		
//// cout <<"		process_iter_stmprocess_iter_stmprocess_iter_stm " << endl;
//				//print_array_var(array_var_copy);
//				// cout <<"       ************************** " << endl << endl;		
		
		//判断表达式是否可以计算
		string result = calculate_expr(array_var_copy);
	//	//cout<<"OKOKOKOKOKOKOK"<<endl;
		if(result != "cannot_calculate")	//可以计算
		{
	     //	//cout<<"!!!!!!!!!!!"<<endl;
			//0603 while中条件表达式为假，才为缺陷
			int num = atoi(result.c_str());
		//	//cout<<num<<endl;
			if(!num)
			{		
		
				// cout <<"		*** reduCondBug: " <<  new_elem.line << endl;
								
				//保存到缺陷文件中
				string str;
				for(int i=0; i<new_elem.var_value.size(); i++)
				{
					if(new_elem.var_value[i].type == typeConstant)
					{
						str += new_elem.var_value[i].value;
					}
					else
					{
						str += new_elem.var_value[i].name;
					}
				}
				file << new_elem.line << " " << str << endl;
			}
			
			array_var_copy.erase(array_var_copy.end() - 1);	//
		}
		else
		{
			analyze_cond_expr(array_var_copy);	//
			
			//备份array_var
		//	array_var_copy.clear();
		//	array_var_copy = array_var;
		}
		
		//在循环体内按照同样的规则找冗余条件缺陷
		//对于循环体的外部来说，只是删除了在循环体中出现的赋值，初始化，前缀和后缀节点的相关变量的记录
		
		//跳过条件表达式
		int count_jump_node = exCountNode(temp_node->subNode[2]) + 3;
		count += count_jump_node;
		jump_node_stack_cond(count_jump_node);
		
		while(!stack_cond.empty() && count < count_node-1)
		{
			treeNode *temp_node = stack_cond.top();
			stack_cond.pop();
			count++;
			if(temp_node->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp_node->subNode.end() - 1;
				while(iter != temp_node->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			switch(temp_node->type)
			{
			  
				case typeAssignExpr:
				case typeInitDec:
			         
					if(temp_node->subNode[0]->type == typeId)
					{
						elem new_elem;
						new_elem.var_name = temp_node->subNode[0]->str;
						new_elem.line = temp_node->subNode[0]->lineno;							
						
						if(temp_node->subNode[2]->type == typeConstant
							&& temp_node->subNode[1]->str == "=")
						{
							expr_or_const_elem temp_elem;
							temp_elem.value = temp_node->subNode[2]->str;					
							new_elem.var_value.push_back(temp_elem);					
							new_elem.flag = "constant";
						}
						else
						{
							//从根节点为temp_node的子树中提取表达式存入array_value
							exExpr(temp_node->subNode[2], new_elem.var_value);					
							new_elem.flag = "expression";
						}
						array_var_copy.push_back(new_elem);
						
						adjust_array_var(array_var_copy);
						
						
						//删除已有的记录
						string var_name = temp_node->subNode[0]->str;
						
						if(array_var.size() > 0)
						{
							vector<elem>::iterator iter = array_var.begin();
							while(iter != array_var.end())
							{
								if((*iter).var_name == var_name)
								{
									array_var.erase(iter);
									break;
								}
								iter++;
							}
						}
						if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
					}
							
					break;
				case typeUnaryExpr:
					if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
					{
						process_unary_expr(temp_node,array_var_copy);
						
						//删除在array_var中记录
						while(!s_unary_post.empty())
							s_unary_post.pop();
						s_unary_post.push(temp_node);
						while(!s_unary_post.empty())
						{
							treeNode *temp = s_unary_post.top();
							s_unary_post.pop();
							if(temp->subNode.size() > 0)
							{
								vector<node*>::iterator iter = temp->subNode.end() - 1;
								while(iter != temp->subNode.begin())
								{
									s_unary_post.push(*iter);
									iter--;
								}
								s_unary_post.push(*iter);
							}
							if(temp->type == typeId)
							{
								string var_name = temp->str;
								
								if(array_var.size() > 0)
								{
									vector<elem>::iterator iter = array_var.begin();
									while(iter != array_var.end())
									{
										if((*iter).var_name == var_name)
										{
											array_var.erase(iter);
											break;
										}
										iter++;
									}
								}
								if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
							}
						}
					}
					break;	
				case typePostExpr:
					if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
					{
						process_post_expr(temp_node,array_var_copy);
						
						//删除在array_var_copy中记录
						while(!s_unary_post.empty())
							s_unary_post.pop();
						s_unary_post.push(temp_node);
						while(!s_unary_post.empty())
						{
							treeNode *temp = s_unary_post.top();
							s_unary_post.pop();
							if(temp->subNode.size() > 0)
							{
								vector<node*>::iterator iter = temp->subNode.end() - 1;
								while(iter != temp->subNode.begin())
								{
									s_unary_post.push(*iter);
									iter--;
								}
								s_unary_post.push(*iter);
							}
							if(temp->type == typeId)
							{
								string var_name = temp->str;
								
								if(array_var.size() > 0)		//0519
								{
									vector<elem>::iterator iter = array_var.begin();
									while(iter != array_var.end())
									{
										if((*iter).var_name == var_name)
										{
											array_var.erase(iter);
											break;
										}
										iter++;
									}
								}
								if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
							}
						}
					}	
					break;
				case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
					{
						process_if_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
								
					}
					else
					{
						process_switch_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
					}
					break;
				case typeIterStm:
					process_iter_stm(temp_node, array_var_copy,array_var);	//
					
					count += exCountNode(temp_node);
					
					break;		
								
				default:
					;
			}
		}		
	}
	else if(temp_node->subNode[0]->str == "do")
	{
		//对do-while的条件不进行提取和判断，对其循环体内部按之前的规则找缺陷
		
		//跳过条件表达式
		int count_jump_node = 1;
		count += count_jump_node;
		jump_node_stack_cond(count_jump_node);
		
		int count_spe = exCountNode(temp_node->subNode[4]) + 4;	//
				
		while(!stack_cond.empty() && count < count_node-1-count_spe)	//
		{
			treeNode *temp_node = stack_cond.top();
			stack_cond.pop();
			count++;
			if(temp_node->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp_node->subNode.end() - 1;
				while(iter != temp_node->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			switch(temp_node->type)
			{
				case typeAssignExpr:
				case typeInitDec:
					if(temp_node->subNode[0]->type == typeId)
					{
						elem new_elem;
						new_elem.var_name = temp_node->subNode[0]->str;
						new_elem.line = temp_node->subNode[0]->lineno;
						
						if(temp_node->subNode[2]->type == typeConstant
							&& temp_node->subNode[1]->str == "=")
						{
							expr_or_const_elem temp_elem;
							temp_elem.value = temp_node->subNode[2]->str;					
							new_elem.var_value.push_back(temp_elem);					
							new_elem.flag = "constant";
						}
						else
						{
							//从根节点为temp_node的子树中提取表达式存入array_value
							exExpr(temp_node->subNode[2], new_elem.var_value);					
							new_elem.flag = "expression";
						}
						array_var_copy.push_back(new_elem);
						adjust_array_var(array_var_copy);
						
						//删除已有的记录
						string var_name = temp_node->subNode[0]->str;
						
						if(array_var.size() > 0)	//
						{
							vector<elem>::iterator iter = array_var.begin();
							while(iter != array_var.end())
							{
								if((*iter).var_name == var_name)
								{
									array_var.erase(iter);
									break;
								}
								iter++;
							}
						}
						if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
					}
					break;
				case typeUnaryExpr:
					if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
					{
						process_unary_expr(temp_node,array_var_copy);
						
						//删除在array_var_copy中记录
						while(!s_unary_post.empty())
							s_unary_post.pop();
						s_unary_post.push(temp_node);
						while(!s_unary_post.empty())
						{
							treeNode *temp = s_unary_post.top();
							s_unary_post.pop();
							if(temp->subNode.size() > 0)
							{
								vector<node*>::iterator iter = temp->subNode.end() - 1;
								while(iter != temp->subNode.begin())
								{
									s_unary_post.push(*iter);
									iter--;
								}
								s_unary_post.push(*iter);
							}
							if(temp->type == typeId)
							{
								string var_name = temp->str;
								
								if(array_var.size() > 0)
								{
									vector<elem>::iterator iter = array_var.begin();
									while(iter != array_var.end())
									{
										if((*iter).var_name == var_name)
										{
											array_var.erase(iter);
											break;
										}
										iter++;
									}
								}
								if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
							}
						}
					}
					break;	
				case typePostExpr:
					if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
					{
						process_post_expr(temp_node,array_var_copy);
						
						//删除在array_var_copy中记录
						while(!s_unary_post.empty())
							s_unary_post.pop();
						s_unary_post.push(temp_node);
						while(!s_unary_post.empty())
						{
							treeNode *temp = s_unary_post.top();
							s_unary_post.pop();
							if(temp->subNode.size() > 0)
							{
								vector<node*>::iterator iter = temp->subNode.end() - 1;
								while(iter != temp->subNode.begin())
								{
									s_unary_post.push(*iter);
									iter--;
								}
								s_unary_post.push(*iter);
							}
							if(temp->type == typeId)
							{
								string var_name = temp->str;
								
								if(array_var.size() > 0)		//0519
								{
									vector<elem>::iterator iter = array_var.begin();
									while(iter != array_var.end())
									{
										if((*iter).var_name == var_name)
										{
											array_var.erase(iter);
											break;
										}
										iter++;
									}
								}
								if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
							}
						}
					}	
					break;
				case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
					{
						process_if_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
						
					}
					else
					{
						process_switch_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
					}
					break;
				case typeIterStm:
					process_iter_stm(temp_node, array_var_copy,array_var);	//
					
					count += exCountNode(temp_node);
					
					break;		
								
				default:
					;
			}
		}
		jump_node_stack_cond(count_spe);	//		
	}
	else if (!stack_cond.empty())//for
	{
		treeNode *temp = stack_cond.top();
		stack_cond.pop();
		count++;
		while(!stack_cond.empty() 
			&& !compare_node(temp, temp_node->subNode[temp_node->subNode.size() - 1]))
		{
			if(temp->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp->subNode.end() - 1;
				while(iter != temp->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			temp = stack_cond.top();
			stack_cond.pop();
			count++;
		}
		if(temp->subNode.size() > 0)	//
		{
			vector<node*>::iterator iter = temp->subNode.end() - 1;
			while(iter != temp->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
													//	// cout <<"	" << count_node - 1 <<  endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@	
		while(!stack_cond.empty() && count < count_node-1)
		{	
			treeNode *temp_node = stack_cond.top();		//// cout <<temp_node->lineno << endl;//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			stack_cond.pop();
			count++;
			if(temp_node->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp_node->subNode.end() - 1;
				while(iter != temp_node->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			switch(temp_node->type)
			{
				case typeAssignExpr:
				case typeInitDec:
					if(temp_node->subNode[0]->type == typeId)
					{
						elem new_elem;
						new_elem.var_name = temp_node->subNode[0]->str;
						new_elem.line = temp_node->subNode[0]->lineno;
						
						if(temp_node->subNode[2]->type == typeConstant
							&& temp_node->subNode[1]->str == "=")
						{
							expr_or_const_elem temp_elem;
							temp_elem.value = temp_node->subNode[2]->str;					
							new_elem.var_value.push_back(temp_elem);					
							new_elem.flag = "constant";
						}
						else
						{
							//从根节点为temp_node的子树中提取表达式存入array_value
							exExpr(temp_node->subNode[2], new_elem.var_value);					
							new_elem.flag = "expression";
						}
						array_var_copy.push_back(new_elem);
						adjust_array_var(array_var_copy);
						
						//删除已有的记录
						string var_name = temp_node->subNode[0]->str;
						if(array_var.size() > 0)
						{
							vector<elem>::iterator iter = array_var.begin();
							while(iter != array_var.end())
							{
								if((*iter).var_name == var_name)
								{
									array_var.erase(iter);
									break;
								}
								iter++;
							}
						}
						if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
					}
					break;
				case typeUnaryExpr:
					if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
					{
						process_unary_expr(temp_node,array_var_copy);
						
						//删除在array_var_copy中记录
						while(!s_unary_post.empty())
							s_unary_post.pop();
						s_unary_post.push(temp_node);
						while(!s_unary_post.empty())
						{
							treeNode *temp = s_unary_post.top();
							s_unary_post.pop();
							if(temp->subNode.size() > 0)
							{
								vector<node*>::iterator iter = temp->subNode.end() - 1;
								while(iter != temp->subNode.begin())
								{
									s_unary_post.push(*iter);
									iter--;
								}
								s_unary_post.push(*iter);
							}
							if(temp->type == typeId)
							{
								string var_name = temp->str;
								if(array_var.size() > 0)
								{
									vector<elem>::iterator iter = array_var.begin();
									while(iter != array_var.end())
									{
										if((*iter).var_name == var_name)
										{
											array_var.erase(iter);
											break;
										}
										iter++;
									}
								}
								if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							       //   //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
							}
						}
					}
					break;	
				case typePostExpr:
														
					if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
					{
						process_post_expr(temp_node,array_var_copy);	
						
						//删除在array_var_copy中记录
						while(!s_unary_post.empty())
							s_unary_post.pop();
						s_unary_post.push(temp_node);
						while(!s_unary_post.empty())
						{
							treeNode *temp = s_unary_post.top();
							s_unary_post.pop();
							if(temp->subNode.size() > 0)
							{
								vector<node*>::iterator iter = temp->subNode.end() - 1;
								while(iter != temp->subNode.begin())
								{
									s_unary_post.push(*iter);
									iter--;
								}
								s_unary_post.push(*iter);
							}
							if(temp->type == typeId)
							{
								string var_name = temp->str;	
								if(array_var.size() > 0)		//0519
								{
									vector<elem>::iterator iter = array_var.begin();
									while(iter != array_var.end())
									{
										if((*iter).var_name == var_name)
										{
											array_var.erase(iter);
											break;
										}
										iter++;
									}
								}
								if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
							}
						}
					}	
					break;
				case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
					{//	// cout <<"### 1 " << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					
					/*	// cout <<"--------- before if -----------------" << endl;
						//print_array_var(array_var);
						// cout <<endl;
						//print_array_var(array_var_copy);
						// cout <<"-------------------------------------" << endl;
					*/	
						process_if_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
						
					/*	// cout <<"--------- after if -----------------" << endl;
						//print_array_var(array_var);
						// cout <<endl;
						//print_array_var(array_var_copy);
						// cout <<"-------------------------------------" << endl;						
					*/
					}
					else
					{
						process_switch_stm(temp_node, array_var_copy,array_var);
						
						count += exCountNode(temp_node);
					}
					break;
				case typeIterStm:
					process_iter_stm(temp_node, array_var_copy,array_var);	//
					
					count += exCountNode(temp_node);
					
					break;		
								
				default:
					;
			}
		}		
	}
	
	file.close();

//删除parent_array_var中array_var_copy中没有的元素
	delete_elem(parent_array_var,array_var_copy);
	
	//还原array_var
	array_var_copy.clear();
	array_var_copy = parent_array_var;	
	
/*	if(flag_while)	//0604
	{
		array_var.push_back(while_elem);
		analyze_cond_expr();
	}
*/

           //     // cout <<"		process_iter_stmprocess_iter_stmprocess_iter_stm " << endl;
			//	//print_array_var(array_var_copy);
			//	//print_array_var(array_var);
			//	// cout <<"       ************************** " << endl << endl;		
}
//gdd
bool exist(treeNode *p, treeNode *pp)
{
    if(!p)
		return false;
	queue<treeNode*> temp_queuew;
	//while(!temp_queuew.empty())		//0224
	//	temp_queuew.pop();
		
	temp_queuew.push(p);
	
	
	while(!temp_queuew.empty())
	{
		treeNode *temp_nodei = temp_queuew.front();
		temp_queuew.pop();	
		
		if(temp_nodei->type == typeId)
		{
		 
			if(exSelecStr(pp,temp_nodei->str))
				return true;
		}
				
		if(temp_nodei->subNode.size() > 0 )		//0519
		{
			vector<node*>::iterator iterw = temp_nodei->subNode.begin();
			while(iterw != temp_nodei->subNode.end())
			{
				temp_queuew.push(*iterw);
				iterw++;
			}
		}
		
	}
	return false;
}
//gdd
bool re_assin(treeNode *p, treeNode *pp,int flag)
{
    if(!p)
		return false;
	queue<treeNode*> temp_queuen;
	//while(!temp_queuen.empty())		//0224
	//	temp_queuen.pop();
//	ex(p);
	temp_queuen.push(p);
	while(!temp_queuen.empty())
	{
		treeNode *temp_node = temp_queuen.front();
		temp_queuen.pop();	
		
		if(temp_node->type == typeAssignExpr)
		{
		   // //cout<<"in typeAssignExpr typeAssignExpr typeAssignExpr"<<endl;
			if(exist(temp_node->subNode[0],pp->subNode[2]))
				return true;
		}
		if(temp_node->type ==typePostExpr && (temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--"))
		{
		    if(exist(temp_node->subNode[0],pp->subNode[2]))
				return true;
		}
		if(temp_node->type ==typeUnaryExpr && (temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--"))
		{
		    if(exist(temp_node->subNode[1],pp->subNode[2]))
				return true;
		}
		
		
		if(temp_node->subNode.size() > 0 &&(!compare_node(temp_node,pp)))		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queuen.push(*iter);
				iter++;
			}
		}
		if(temp_node->subNode.size() > 0 &&(compare_node(temp_node,pp)) && flag==1)
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queuen.push(*iter);
				iter++;
			}
		}
		if(temp_node->subNode.size() > 0 &&(compare_node(temp_node,pp)) && flag==0)		//0519
		{
		    if(temp_node->subNode.size()>5)
			   temp_queuen.push(temp_node->subNode[6]);
			
		}
		
	}
	return false;
}
//gdd
bool chang_value(treeNode *p)//gdd
{
    if(!p)
		return false;
	stack<treeNode*> stack_chang;
	/*while(!stack_chang.empty())
		stack_chang.pop();*/
	
	stack_chang.push(p);
	//ex(p);
	
	while(!stack_chang.empty())
	{
	    treeNode *temp_node = stack_chang.top();
		stack_chang.pop();
		
		////cout<<temp_node->str<<endl;
		

		if(temp_node->type==typeUnaryExpr)
		{	//cout<<"typeUnaryExprtypeUnaryExprtypeUnaryExpr"<<endl;
		    if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
		        return true;			
		}
	 	if(temp_node->type == typePostExpr)
		{    //cout<<"typePostExprtypePostExprtypePostExpr"<<endl;
		     if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
		        return true;
		}
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp_node->subNode.end()-1;
			while(iter != temp_node->subNode.begin())
			{
				stack_chang.push(*iter);
				iter--;
			}
			stack_chang.push(*iter);
		}
	}
	return false;

}


//gdd 判断结点temp_node是否在循环结构中，如果在，判断选择结构外，循环结构中是否对结点temp_node中的变量重新赋值
//stack<treeNode*> stack_funcn;
bool in_iter_stm(treeNode *p,int flag)
{
    if(!p)
		return false;
	//找到改结点的函数根结点
	treeNode *temp_nodef = p;
	while(temp_nodef->parentNode	&& temp_nodef->parentNode->type != typeFuncDef)
	{
		temp_nodef = temp_nodef->parentNode;
	}
	treeNode *root_func_subtree = temp_nodef->parentNode;
//	//cout<<root_func_subtree->str<<endl;

	treeNode *temp_node_detect = p;
	while(temp_node_detect->parentNode 	&& (!compare_node(temp_node_detect->parentNode,root_func_subtree)))
	{
		
					
	    if(temp_node_detect->parentNode	&& temp_node_detect->parentNode->type == typeIterStm)
		{
		    //cout<<"in typeIterStm typeIterStm typeIterStm typeIterStm typeIterStm"<<endl;
		    if(chang_value(p->subNode[2]))
			   return true;
			if(re_assin(temp_node_detect,p,flag))	
			   return true;	
					
		}
		temp_node_detect = temp_node_detect->parentNode;
	}
	
	
    return false;
}

//gdd
void process_else_stm(treeNode *temp_noden, vector<elem> &parent_array_var,vector<elem> &array_var)	//temp_node为typeSelecStm
{


	//备份array_var
	vector<elem> array_var_copy = parent_array_var;	//
	stack<treeNode*> s_unary_post;

																	
	ofstream file(REDUCONDDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_noden);
	
	int count = 0;
	

				
			//	// cout <<"		************************** " << endl;
			//	//print_array_var(array_var_copy);
			//	// cout <<"       ************************** " << endl << endl;
				
	//判断条件表达式是否可以计算

	//跳过条件表达式节点

	jump_node_stack_cond(1);
		
	while(!stack_cond.empty() && count < count_node-1)
	{
		treeNode *temp_node = stack_cond.top();	
		stack_cond.pop();
		count++;
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		switch(temp_node->type)
		{
			case typeAssignExpr:	//跟踪赋值语句，获得变量-常量绑定
			case typeInitDec:
				
				//保存整型变量和值
				if(temp_node->subNode[0]->type == typeId)	//varname...
				{											
					//先存入容器，然后利用以前已知的计算该表达式值，然后再删除之前同名的变量记录
					//解决 x = x + 1;
					////cout<<temp_node->subNode[0]->str<<endl;
					elem new_elem;
					new_elem.var_name = temp_node->subNode[0]->str;
					new_elem.line = temp_node->subNode[0]->lineno;	//
					
					if(temp_node->subNode[2]->type == typeConstant
						&& temp_node->subNode[1]->str == "=")
					{
						expr_or_const_elem temp_elem;
						temp_elem.value = temp_node->subNode[2]->str;					
						new_elem.var_value.push_back(temp_elem);					
						new_elem.flag = "constant";
					}
					else
					{
						//从根节点为temp_node的子树中提取表达式存入array_value
						exExpr(temp_node->subNode[2], new_elem.var_value);					
						new_elem.flag = "expression";
					}
				
					array_var_copy.push_back(new_elem);
					
					//调整容器array_var
					adjust_array_var(array_var_copy);
					
					//删除已有的记录
					string var_name = temp_node->subNode[0]->str;		//// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					
					if(array_var.size() > 0)
					{
						vector<elem>::iterator iter = array_var.begin();
						while(iter != array_var.end())
						{
							if((*iter).var_name == var_name)
							{
								array_var.erase(iter);
								break;
							}
							iter++;
						}
					}
		//		// cout <<"	111100000000000000000000000000000 " << endl;
		//		//print_array_var(array_var_copy);
		//		// cout <<"   111100000000000000000000000000000 " << endl << endl;	
					if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {										//	// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
				}
		//		// cout <<"	111100000000000000000000000000000 " << endl;
		//		//print_array_var(array_var_copy);
		//		// cout <<"   111100000000000000000000000000000 " << endl << endl;					
			
				break;
			case typeUnaryExpr:	// ++var; --var;
			
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					process_unary_expr(temp_node,array_var_copy);
					
					//删除在array_var_copy中记录
					while(!s_unary_post.empty())
						s_unary_post.pop();
					s_unary_post.push(temp_node);
					while(!s_unary_post.empty())
					{
						treeNode *temp = s_unary_post.top();
						s_unary_post.pop();
						if(temp->subNode.size() > 0)
						{
							vector<node*>::iterator iter = temp->subNode.end() - 1;
							while(iter != temp->subNode.begin())
							{
								s_unary_post.push(*iter);
								iter--;
							}
							s_unary_post.push(*iter);
						}
						if(temp->type == typeId)
						{
							string var_name = temp->str;
							
							if(array_var.size() > 0)
							{
								vector<elem>::iterator iter = array_var.begin();
								while(iter != array_var.end())
								{
									if((*iter).var_name == var_name)
									{
										array_var.erase(iter);
										break;
									}
									iter++;
								}
							}
							if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							          //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
						}
					}
				}
				
				break;
			case typePostExpr:	// var++; var--;
			
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{		
					process_post_expr(temp_node,array_var_copy);
					
					//删除在array_var_copy中记录
					while(!s_unary_post.empty())
						s_unary_post.pop();
					s_unary_post.push(temp_node);
					while(!s_unary_post.empty())
					{
						treeNode *temp = s_unary_post.top();
						s_unary_post.pop();
						if(temp->subNode.size() > 0)
						{
							vector<node*>::iterator iter = temp->subNode.end() - 1;
							while(iter != temp->subNode.begin())
							{
								s_unary_post.push(*iter);
								iter--;
							}
							s_unary_post.push(*iter);
						}
						if(temp->type == typeId)
						{
							string var_name = temp->str; 
							
							if(array_var.size() > 0)		//0519
							{
								vector<elem>::iterator iter = array_var.begin();
								while(iter != array_var.end())
								{
									if((*iter).var_name == var_name)
									{	
										array_var.erase(iter);
										break;
									}
									iter++;
								}
							}
							if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							          //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
						}
					}
				}
				
				break;
				
			case typeSelecStm:
				//IF '(' expression ')' statement %prec IFX		
				//IF '(' expression ')' statement ELSE statement
				//SWITCH '(' expression ')' statement
				
				//递归处理
				if(temp_node->subNode[0]->str == "if")
				{
				//	// cout <<"### 2 " << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					process_if_stm(temp_node, array_var_copy,array_var);	//										
				//	// cout <<"### end 2" << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_stm(temp_node, array_var_copy,array_var);
					
					count += exCountNode(temp_node);
				}
				break;
			case typeIterStm:
			   // //cout<<"OOOOOOOOOOOOOOOOOOOOOOO"<<endl;
			
				process_iter_stm(temp_node, array_var_copy,array_var);
				
				count += exCountNode(temp_node);
				
			break;
			
			//...
				
			default:
				;
		}
	}	
		
	file.close();
	
//删除parent_array_var中array_var_copy中没有的元素
	delete_elem(parent_array_var,array_var_copy);
	//还原array_var
	array_var_copy.clear();
	array_var_copy = parent_array_var;	
  	
}
//gdd

bool later_in_lable(treeNode *p,string str)
{
    if(!p)
		return false;
	queue<treeNode*> temp_queueb;
	//while(!temp_queueb.empty())		//0224
	//	temp_queueb.pop();
//	ex(p);
	temp_queueb.push(p);
	while(!temp_queueb.empty())
	{
		treeNode *temp_node = temp_queueb.front();
		temp_queueb.pop();	
		
		if(temp_node->type==typeJumpStm && temp_node->subNode[1]->str==str)
		{	
		     return true;
		}
		
		
		if(temp_node->subNode.size() > 0)		//0519
		{
			vector<node*>::iterator iter = temp_node->subNode.begin();
			while(iter != temp_node->subNode.end())
			{
				temp_queueb.push(*iter);
				iter++;
			}
		}
	}
	return false;
}

//gdd

bool add_in_lable(treeNode *p)
{
    if(!p)
		return false;
	//找到改结点的函数根结点
	treeNode *temp_nodef = p;
	while(temp_nodef->parentNode	&& temp_nodef->parentNode->type != typeFuncDef)
	{
		temp_nodef = temp_nodef->parentNode;
	}
	treeNode *root_func_subtree = temp_nodef->parentNode;
	
	//找到还函数子树的最后一个节点		//0619
	stack<treeNode*> stack_fun;
	/*while(!stack_fun.empty())
		stack_fun.pop();*/
	int count = 0;
	stack_fun.push(root_func_subtree);
	int num_node_func = exCountNode(root_func_subtree);
	while(!(stack_fun.empty()) && (count < num_node_func - 1))
	{
		treeNode *temp_node = stack_fun.top();
		stack_fun.pop();
		count++;
				
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end()-1;
			while(iter != temp_node->subNode.begin())
			{
				stack_fun.push(*iter);
				iter--;
			}
			stack_fun.push(*iter);
		}
	}
	treeNode *end_func_subtree = stack_fun.top();
	
	if(chang_value(p->subNode[2]))
	{
        stack<treeNode*> stack_lableb;
	    /*while(!stack_lableb.empty())
		  stack_lableb.pop();*/
	
	     stack_lableb.push(root_func_subtree);
		
	    while(!stack_lableb.empty())
	    {
	        treeNode *temp_node = stack_lableb.top();
		    stack_lableb.pop();
		
		    if(compare_node(temp_node, p))
		         return false;
		    if(temp_node->type==typeLabelStm)
		    {	
		       if(later_lable(root_func_subtree,p,end_func_subtree,temp_node->subNode[0]->str))
			        return true;
			    if(later_in_lable(p,temp_node->subNode[0]->str))
			        return true;		   
		    }		
		    if(temp_node->subNode.size() > 0)
		    {
			    vector<treeNode*>::iterator iter = temp_node->subNode.end()-1;
			    while(iter != temp_node->subNode.begin())
			    {
				    stack_lableb.push(*iter);
				    iter--;
			    }
			    stack_lableb.push(*iter);
		    }
	
	
	    }
		   
	}
	return false;
}

void process_if_stm(treeNode *temp_noden, vector<elem> &parent_array_var,vector<elem> &array_var)	//temp_node为typeSelecStm
{
	vector<elem> array_var_cp = array_var;	//备份以留备else分支使用
	vector<elem> array_var_total_if_else = array_var;	//
    stack<treeNode*> s_unary_post;
	//备份array_var
	vector<elem> array_var_copy = parent_array_var;	//
	
	vector<string> delete_var_local;
																	
	ofstream file(REDUCONDDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_noden->subNode[0])+exCountNode(temp_noden->subNode[1])
	                 +exCountNode(temp_noden->subNode[2])+exCountNode(temp_noden->subNode[3])
	                 +exCountNode(temp_noden->subNode[4]);
	
	int count = 0;
	
	//提取选择语句条件表达式并存入array_var
	elem new_elem;
	new_elem.var_name = "conditional_predicate";
	new_elem.flag = "con_predicate";
	new_elem.line = temp_noden->subNode[0]->lineno;
	exExpr(temp_noden->subNode[2], new_elem.var_value);
	array_var_copy.push_back(new_elem);
				
				// cout <<"选择选择选择选择选择选择选择选择 " << endl;
				//print_array_var(array_var_copy);
				// cout <<" 选择选择选择选择选择选择选择选择 " << endl << endl;
				
	//判断条件表达式是否可以计算
	string result = calculate_expr(array_var_copy);	
	//cout<<result<<endl;
	if(result != "cannot_calculate")	//可以计算  //gdd修改版
	{   int flag;
	    int num = atoi(result.c_str());
		if(!num)
		   flag=0;
		else
		   flag=1;
	    if(in_iter_stm(temp_noden,flag))    //gdd修改
	    {	       
	        analyze_cond_expr(array_var_copy);	//待添加关系运算分析 
	        //cout<<"qqqqqqq"<<endl;	        
	    }
	    else if(add_in_lable(temp_noden))
	    {
	        analyze_cond_expr(array_var_copy);
	    }  
	    else
	    {                                                   
		// cout <<"		*** reduCondBug: " <<  new_elem.line << endl;
							
		//保存到缺陷文件中
		string str;
		for(int i=0; i<new_elem.var_value.size(); i++)
		{
			if(new_elem.var_value[i].type == typeConstant)
			{
				str += new_elem.var_value[i].value;
			}
			else
			{
				str += new_elem.var_value[i].name;
			}
		}
		file << new_elem.line << " " << str << endl;
		}
		
	}
	else	//不能计算
	{
		//IF '(' expression ')' statement %prec IFX		
		//IF '(' expression ')' statement ELSE statement
		//SWITCH '(' expression ')' statement
		
		//对各种语句节点分析
		analyze_cond_expr(array_var_copy);	//待添加关系运算分析	####
	}	
       //         // cout <<"选择选择选择选择选择选择选择选择 " << endl;
	//			//print_array_var(array_var_copy);
	//			// cout <<" 选择选择选择选择选择选择选择选择 " << endl << endl;	
	//跳过条件表达式节点
	int count_expr = exCountNode(temp_noden->subNode[2]);
	int count_jump_node = count_expr + 3;
	count += count_jump_node;
	jump_node_stack_cond(count_jump_node);
	

	while(!stack_cond.empty() && count < count_node)
	{
		treeNode *temp_node = stack_cond.top();	
		stack_cond.pop();
		count++;
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		switch(temp_node->type)
		{
			case typeAssignExpr:	//跟踪赋值语句，获得变量-常量绑定
			case typeInitDec:
				
				//保存整型变量和值
				if(temp_node->subNode[0]->type == typeId)	//varname...
				{											
					//先存入容器，然后利用以前已知的计算该表达式值，然后再删除之前同名的变量记录
					//解决 x = x + 1;
					////cout<<temp_node->subNode[0]->str<<endl;
					elem new_elem;
					new_elem.var_name = temp_node->subNode[0]->str;
					new_elem.line = temp_node->subNode[0]->lineno;	//
					
					if(temp_node->subNode[2]->type == typeConstant
						&& temp_node->subNode[1]->str == "=")
					{
						expr_or_const_elem temp_elem;
						temp_elem.value = temp_node->subNode[2]->str;					
						new_elem.var_value.push_back(temp_elem);					
						new_elem.flag = "constant";
					}
					else
					{
						//从根节点为temp_node的子树中提取表达式存入array_value
						exExpr(temp_node->subNode[2], new_elem.var_value);					
						new_elem.flag = "expression";
					}
				
					array_var_copy.push_back(new_elem);
					
					//调整容器array_var
					adjust_array_var(array_var_copy);
					
					//删除已有的记录
					string var_name = temp_node->subNode[0]->str;		//// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					
					if(array_var.size() > 0)
					{
						vector<elem>::iterator iter = array_var.begin();
						while(iter != array_var.end())
						{
							if((*iter).var_name == var_name)
							{
								array_var.erase(iter);
								break;
							}
							iter++;
						}
					}
		/*		// cout <<"		00000000000000000000000000000 " << endl;
				//print_array_var(array_var_copy);
				// cout <<"       00000000000000000000000000000 " << endl << endl;	*/
				    
					if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							        //  //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
				}
				// cout <<"		00000000000000000000000000000 " << endl;
				//print_array_var(array_var_copy);
				// cout <<"       00000000000000000000000000000 " << endl << endl;
				//print_array_var(parent_array_var);	// cout <<"       666666666666666666660000000000000 " << endl << endl;			
			
				break;
			case typeUnaryExpr:	// ++var; --var;
			
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					process_unary_expr(temp_node,array_var_copy);
					
					//删除在array_var_copy中记录
					while(!s_unary_post.empty())
						s_unary_post.pop();
					s_unary_post.push(temp_node);
					while(!s_unary_post.empty())
					{
						treeNode *temp = s_unary_post.top();
						s_unary_post.pop();
						if(temp->subNode.size() > 0)
						{
							vector<node*>::iterator iter = temp->subNode.end() - 1;
							while(iter != temp->subNode.begin())
							{
								s_unary_post.push(*iter);
								iter--;
							}
							s_unary_post.push(*iter);
						}
						if(temp->type == typeId)
						{
							string var_name = temp->str;
							
							if(array_var.size() > 0)
							{
								vector<elem>::iterator iter = array_var.begin();
								while(iter != array_var.end())
								{
									if((*iter).var_name == var_name)
									{
										array_var.erase(iter);
										break;
									}
									iter++;
								}
							}
							if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							         // //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
						}
					}
				}
				
				break;
			case typePostExpr:	// var++; var--;
			
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{		
					process_post_expr(temp_node,array_var_copy);
					
					//删除在array_var_copy中记录
					while(!s_unary_post.empty())
						s_unary_post.pop();
					s_unary_post.push(temp_node);
					while(!s_unary_post.empty())
					{
						treeNode *temp = s_unary_post.top();
						s_unary_post.pop();
						if(temp->subNode.size() > 0)
						{
							vector<node*>::iterator iter = temp->subNode.end() - 1;
							while(iter != temp->subNode.begin())
							{
								s_unary_post.push(*iter);
								iter--;
							}
							s_unary_post.push(*iter);
						}
						if(temp->type == typeId)
						{
							string var_name = temp->str; 
							
							if(array_var.size() > 0)		//0519
							{
								vector<elem>::iterator iter = array_var.begin();
								while(iter != array_var.end())
								{
									if((*iter).var_name == var_name)
									{	
										array_var.erase(iter);
										break;
									}
									iter++;
								}
							}
							if(array_var_copy.size() > 0)
					{
						vector<elem>::iterator iter = array_var_copy.begin();
						while(iter != array_var_copy.end()-1)    //gdd修改
						{
						    if((*iter).flag == "constant")
						    {
							    if((*iter).var_name == var_name)
							    {											// cout <<"~~~~~~~~~~~" << var_name << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								    array_var_copy.erase(iter);
								        break;
							    }
							}
							else
							{
							    int flag=0;
							    for(int j=0; j<(*iter).var_value.size(); j++)
							    {
							       if((*iter).var_value[j].name==var_name)
							       {
							        //  //cout<<(*iter).var_value[j].name<<endl;
							           array_var_copy.erase(iter);
								            flag=1;
								            break;
							       }
							    }
							    if(flag==1)
							    break;
							}
							iter++;
						}
					}
						}
					}
				}
				
				break;
				
			case typeSelecStm:
				//IF '(' expression ')' statement %prec IFX		
				//IF '(' expression ')' statement ELSE statement
				//SWITCH '(' expression ')' statement
				
				//递归处理
				if(temp_node->subNode[0]->str == "if")
				{
				//	// cout <<"### 2 " << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					process_if_stm(temp_node, array_var_copy,array_var);	//										
				//	// cout <<"### end 2" << endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_stm(temp_node, array_var_copy,array_var);
					
					count += exCountNode(temp_node);
				}
				break;
			case typeIterStm:
			   // //cout<<"OOOOOOOOOOOOOOOOOOOOOOO"<<endl;
			
				process_iter_stm(temp_node, array_var_copy,array_var);
				
				count += exCountNode(temp_node);
				
			break;
			
			//...
				
			default:
				;
		}
	}	
		
	file.close();
	
	//删除parent_array_var中array_var_copy中没有的元素
	delete_elem(parent_array_var,array_var_copy);
	
	//还原array_var
	array_var_copy.clear();
	array_var_copy = parent_array_var;	


	if(temp_noden->subNode.size()>5)
	  process_else_stm(temp_noden->subNode[6],parent_array_var ,array_var);
	  	
}



//冗余的条件分支检测
void reduCondDect_2(treeNode *p)
{
	// cout <<endl << "-------------------redu cond detection-------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;

	if(!p)
		return;
	//stack<treeNode*> stack_cond;
	while(!stack_cond.empty())
		stack_cond.pop();
		

	ofstream file(REDUCONDDECT_FILE, ios::out | ios::app);		// 将缺陷保存到文件中	

	//缺陷检测开始
	stack_cond.push(p);
	treeNode *temp_node = NULL;
	
	while(!stack_cond.empty())
	{
		temp_node = stack_cond.top();
		stack_cond.pop();
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
    
    			
		treeNode *node = NULL;
		switch(temp_node->type)
		{
			case typeFuncDef:
				// cout <<"	---------print 1 ------------" << endl;
				//print_array_var(array_var);	//test
				// cout <<"--------------------------------------------------------" << endl;
				array_var.clear();
				break;
			case typeAssignExpr:	//跟踪赋值语句，获得变量-常量绑定
			case typeInitDec:
				
				//保存整型变量和值
				if(temp_node->subNode[0]->type == typeId)	//varname...
				{
					//先存入容器，然后利用以前已知的计算该表达式值，然后再删除之前同名的变量记录
					//解决 x = x + 1;
					
					elem new_elem;
					new_elem.var_name = temp_node->subNode[0]->str;
					new_elem.line = temp_node->subNode[0]->lineno;	//
					
					if(temp_node->subNode[2]->type == typeConstant
						&& temp_node->subNode[1]->str == "=")
					{
						expr_or_const_elem temp_elem;
						temp_elem.value = temp_node->subNode[2]->str;					
						new_elem.var_value.push_back(temp_elem);					
						new_elem.flag = "constant";
					}
					else
					{
						//从根节点为temp_node的子树中提取表达式存入array_value
						exExpr(temp_node->subNode[2], new_elem.var_value);					
						new_elem.flag = "expression";
					}
				
					array_var.push_back(new_elem);
					
					//调整容器array_var
					adjust_array_var(array_var);
				}
					   
				break;
			case typeUnaryExpr:	// ++var; --var;
			
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					process_unary_expr(temp_node,array_var);
				}
				
				break;
			case typePostExpr:	// var++; var--;
			
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					process_post_expr(temp_node,array_var);
				}
				
				break;
				
			case typeSelecStm:		//IF '(' expression ')' statement %prec IFX		
									//IF '(' expression ')' statement ELSE statement
									//SWITCH '(' expression ')' statement
				{
					vector<elem> array_var_copy=array_var;	
				//	// cout <<"over over over over mmm " << endl;
			    //	//print_array_var(array_var_copy);										
					if(temp_node->subNode[0]->str == "if")	// 处理if语句
					{
						process_if_stm(temp_node, array_var_copy,array_var);
					}
					else	//switch
					{
						//提取条件表达式，如果可以计算则为缺陷
						//对switch语句体简单处理，只寻找其中的赋值，初始化，前缀和后缀节点，删除涉及到的之前保存的变量记录
						process_switch_stm(temp_node, array_var_copy,array_var);
					}											
				}
							
				break;
			case typeIterStm:
				{	
					vector<elem> array_var_copy=array_var;
					process_iter_stm(temp_node, array_var_copy,array_var);	//
				}
							
				break;
				
				//...
				
			default:
				;
		}
		
	}
	
	// cout <<"	---------print 2 ------------" << endl;
	//print_array_var(array_var);	//test
	// cout <<"   -----------------------------" << endl;
	
	file.close();
	
	// cout <<"------------------------------------------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	// cout <<endl << "-------------------------end-----------------------------" << endl;
}
//gdd以下程序为隐式幂等检查器

typedef struct
{
	string name;
	
	int lineno;
}hnode;

typedef struct 	//语法树节点类型
{
	vector<hnode> every_hnode_var;
	
} everyhideNode;

//vector<everyhideNode> line_hvar;

typedef struct 	//语法树节点类型
{
	vector<everyhideNode> hnode_var;
	
} hideNode;

vector<hideNode> array_hvar;

//gdd隐式幂等相关函数
void delete_hvar(vector<hideNode> &array,everyhideNode &every_var);
void deal_hvar(vector<hideNode> &array,everyhideNode &every_node,everyhideNode &every_node2,int line,treeNode *p);

void process_if_hide(treeNode *temp_noden, vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar);
void process_iter_hide(treeNode *temp_node, vector<hideNode> &parent_array_hvar, vector<hideNode> &array_hvar);
void process_switch_hide(treeNode *temp_node, vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar);
void process_else_hide(treeNode *temp_noden, vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar);
bool iter_reassin(treeNode *p);
bool iter_reassinment(treeNode *iter_node,treeNode *p);
void exvar(treeNode *p, everyhideNode &every_var);
int find_hide_var(vector<hideNode> &array,everyhideNode &every_node);
void delete_hide_elem(vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar_copy);
bool find_hide_elem(everyhideNode &every_var,vector<hideNode> &array_hvar_copy);
bool lable_reassin(treeNode *p);
bool later_lable_hide(treeNode *p, treeNode *pp,treeNode *endp,string str,treeNode &lable_node);
bool lable_hide_reassinment(treeNode *root_func_subtree,treeNode *p,treeNode &lable_node);


bool find_hide_elem(everyhideNode &every_var,vector<hideNode> &array)
{
    for(int i=0; i<array.size(); i++)
	{
		for(int j=0; j<array[i].hnode_var.size(); j++)
		{	
		    if(array[i].hnode_var[j].every_hnode_var.size()>=every_var.every_hnode_var.size())	
		    {	
		         if(every_var.every_hnode_var.size()==1)
		         {
		             if(array[i].hnode_var[j].every_hnode_var[0].name==every_var.every_hnode_var[0].name)
		             {
		                if(array[i].hnode_var[j].every_hnode_var[0].lineno==every_var.every_hnode_var[0].lineno)
		                {
		                    return true;
		                }
		             }
		         }
		         else
		         {
					 int ii;
		         for(ii=0;ii<every_var.every_hnode_var.size();ii++)
			        {
			           if(array[i].hnode_var[j].every_hnode_var[ii].name!=every_var.every_hnode_var[ii].name)
			              break;
			        }
			        if(ii=every_var.every_hnode_var.size())
			        {
			          if(array[i].hnode_var[j].every_hnode_var[0].lineno==every_var.every_hnode_var[0].lineno)
			                       return true;
			        }  
			    }
			}
		}	
	}
	
	return false;
}

void delete_hide_elem(vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar_copy)
{

    for(int i=0; i<parent_array_hvar.size(); i++)
	{
		for(int j=0; j<parent_array_hvar[i].hnode_var.size(); j++)
		{	
		    if(!find_hide_elem(parent_array_hvar[i].hnode_var[j],array_hvar_copy))	
		    {	
		       parent_array_hvar[i].hnode_var.erase(parent_array_hvar[i].hnode_var.begin()+j);  
			}
		}	
	}

}

bool iter_reassinment(treeNode *iter_node,treeNode *p)
{
    if(!iter_node)
		return false;
		
    stack<treeNode*> stack_iter_reassin;
	/*while(!stack_iter_reassin.empty())
		stack_iter_reassin.pop();*/
	
	stack_iter_reassin.push(iter_node);
	
	treeNode *temp = stack_iter_reassin.top();
	

	while(1) //跳过pp前的结点
	{
		temp = stack_iter_reassin.top();
		stack_iter_reassin.pop();
		
		if(compare_node(temp, p))
		   break;
		
		if(temp->subNode.size()>0)
		{
			vector<node*>::iterator iter = temp->subNode.end()-1;
			while(iter != temp->subNode.begin())
			{
				stack_iter_reassin.push(*iter);
				iter--;
			}
			stack_iter_reassin.push(*iter);
		}
		
	}
	
	while(!stack_iter_reassin.empty())
	{
	    treeNode *temp_node = stack_iter_reassin.top();
		stack_iter_reassin.pop();
		
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{	
		  if(temp_node->subNode[1]->str == "=")
		    if(temp_node->subNode[0]->type==typeId&&p->subNode[0]->str==temp_node->subNode[0]->str)
		    {	       
		       if(!compare_node(temp_node,p))
		          return true;
		    }
		    if(exSelecStr(p->subNode[2], temp_node->subNode[0]->str))
		          return true;		
		}
	 			
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iterr = temp_node->subNode.end()-1;
			while(iterr != temp_node->subNode.begin())
			{
				stack_iter_reassin.push(*iterr);
				iterr--;
			}
			stack_iter_reassin.push(*iterr);
		}
	}
	
	return false;
}

bool lable_hide_reassinment(treeNode *root_func_subtree,treeNode *p,treeNode &lable_node)
{
    if(!root_func_subtree)
		return false;
		
	stack<treeNode*> stack_iter_reassin;
	/*while(!stack_iter_reassin.empty())
		stack_iter_reassin.pop();*/
	
	stack_iter_reassin.push(root_func_subtree);
	
	treeNode *temp = stack_iter_reassin.top();
	

	while(1) //跳过pp前的结点
	{
		temp = stack_iter_reassin.top();
		stack_iter_reassin.pop();
		
		if(compare_node(temp, p))
		{
		     treeNode *temp_nodea =p;
		     treeNode *temp_nodeb =temp;
		     int linea;
		     int lineb;
		     while(temp_nodea->subNode[0]->lineno==-1)
		          temp_nodea=temp_nodea->subNode[0];
		     linea=temp_nodea->subNode[0]->lineno;
		     while(temp_nodeb->subNode[0]->lineno==-1)
		          temp_nodeb=temp_nodeb->subNode[0];
		     lineb=temp_nodeb->subNode[0]->lineno;
		 //    //cout<<linea<<"  "<<lineb<<endl;
		     if(linea==lineb)
		     	   break;
		}
		
		if(temp->subNode.size()>0)
		{
			vector<node*>::iterator iter = temp->subNode.end()-1;
			while(iter != temp->subNode.begin())
			{
				stack_iter_reassin.push(*iter);
				iter--;
			}
			stack_iter_reassin.push(*iter);
		}
		
	}

	while(!stack_iter_reassin.empty())
	{
	    treeNode *temp_node = stack_iter_reassin.top();
		stack_iter_reassin.pop();
	//	//cout<<temp_node->str<<endl;
		treeNode *lable_noden=&lable_node;
		if(compare_node(temp_node, lable_noden))
		   return false;
		
		if(temp_node->type==typeAssignExpr
			|| temp_node->type==typeInitDec)
		{	
		  if(temp_node->subNode[1]->str == "=")
		    if(temp_node->subNode[0]->type==typeId&&p->subNode[0]->str==temp_node->subNode[0]->str)
		    {	       
		       if(!compare_node(temp_node,p))
		          return true;
		    }
		    if(exSelecStr(p->subNode[2], temp_node->subNode[0]->str))
		          return true;		
		}
	 			
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iterr = temp_node->subNode.end()-1;
			while(iterr != temp_node->subNode.begin())
			{
				stack_iter_reassin.push(*iterr);
				iterr--;
			}
			stack_iter_reassin.push(*iterr);
		}
	}
	
	return false;
}

bool later_lable_hide(treeNode *p, treeNode *pp,treeNode *endp,string str,treeNode &lable_node)
{

    if(!p)
		return false;
    
    stack<treeNode*> stack_hiden_lable;		
	/*while(!stack_hiden_lable.empty())
		stack_hiden_lable.pop();*/
	
	stack_hiden_lable.push(p);
	
	treeNode *temp = stack_hiden_lable.top();
	
	////cout<<pp->str<<endl;
	while(1) //跳过pp前的结点
	{
		temp = stack_hiden_lable.top();
		stack_hiden_lable.pop();
		
		if(compare_node(temp, pp))
		{
		     treeNode *temp_nodea =pp;
		     treeNode *temp_nodeb =temp;
		     int linea;
		     int lineb;
		     while(temp_nodea->subNode[0]->lineno==-1)
		          temp_nodea=temp_nodea->subNode[0];
		     linea=temp_nodea->subNode[0]->lineno;
		     while(temp_nodeb->subNode[0]->lineno==-1)
		          temp_nodeb=temp_nodeb->subNode[0];
		     lineb=temp_nodeb->subNode[0]->lineno;
		  //   //cout<<linea<<"  "<<lineb<<endl;
		     if(linea==lineb)
		          break;
		}
		if(temp->subNode.size()>0)
		{
			vector<node*>::iterator iter = temp->subNode.end()-1;
			while(iter != temp->subNode.begin())
			{
				stack_hiden_lable.push(*iter);
				iter--;
			}
			stack_hiden_lable.push(*iter);
		}
	}
	
	////cout<< temp->str <<endl;
	int find=0;
	while(!stack_hiden_lable.empty())
	{
	    treeNode *temp_node = stack_hiden_lable.top();
		stack_hiden_lable.pop();
		
	//	//cout<<temp_node->str<<endl;
		
		if(temp_node->type==typeJumpStm && temp_node->subNode[1]->str==str)
		{	
		     lable_node=*temp_node;
		    
		     find=1;
		}
	 	
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iterr = temp_node->subNode.end()-1;
			while(iterr != temp_node->subNode.begin())
			{
				stack_hiden_lable.push(*iterr);
				iterr--;
			}
			stack_hiden_lable.push(*iterr);
		}
	}
	
	if(find==1)
	 return true;
	
	return false;
	
}

bool lable_reassin(treeNode *p)
{
   if(!p)
		return false;
	//找到改结点的函数根结点
	treeNode *temp_nodef = p;
	while(temp_nodef->parentNode	&& temp_nodef->parentNode->type != typeFuncDef)
	{
		temp_nodef = temp_nodef->parentNode;
	}
	treeNode *root_func_subtree = temp_nodef->parentNode;
	
	int num_node_func = exCountNode(root_func_subtree);
	
	//找到还函数子树的最后一个节点
    stack<treeNode*> stack_func_hide;
	/*while(!stack_func_hide.empty())
		stack_func_hide.pop();*/
	int count = 0;
	stack_func_hide.push(root_func_subtree);
	while(!stack_func_hide.empty() && count < num_node_func - 1)
	{
		treeNode *temp_node = stack_func_hide.top();
		stack_func_hide.pop();
		count++;
				
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end()-1;
			while(iter != temp_node->subNode.begin())
			{
				stack_func_hide.push(*iter);
				iter--;
			}
			stack_func_hide.push(*iter);
		}
	}
	treeNode *end_func_subtree = stack_func_hide.top();
	
    if(!root_func_subtree)
		return false;
	
	while(!stack_func_hide.empty())
		stack_func_hide.pop();
	
	stack_func_hide.push(root_func_subtree);
	//ex(p);
	
//	//cout<<p->str<<endl;
////cout<<"lllllllllll "<<endl;	//cout<<p->subNode[0]->lineno<<endl;
	while(!stack_func_hide.empty())
	{
	    treeNode *temp_node = stack_func_hide.top();
		stack_func_hide.pop();
		
//		//cout<<temp_node->str<<" "<<endl;
		
		if(compare_node(temp_node, p))
		{
		     treeNode *temp_nodea =p;
		     treeNode *temp_nodeb =temp_node;
		     int linea;
		     int lineb;
		     while(temp_nodea->subNode[0]->lineno==-1)
		          temp_nodea=temp_nodea->subNode[0];
		     linea=temp_nodea->subNode[0]->lineno;
		     while(temp_nodeb->subNode[0]->lineno==-1)
		          temp_nodeb=temp_nodeb->subNode[0];
		     lineb=temp_nodeb->subNode[0]->lineno;
		    
		     if(linea==lineb)
		     return false;
		}
		if(temp_node->type==typeLabelStm)
		{	
		  // //cout<<"later lable"<<endl;
		   treeNode lable_node;
		   if(later_lable_hide(root_func_subtree,p,end_func_subtree,temp_node->subNode[0]->str,lable_node))
		    {
		        if(lable_hide_reassinment(root_func_subtree,p,lable_node))
		         {
		              return true;
		         }
		     }		   
		}
	 	
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp_node->subNode.end()-1;
			while(iter != temp_node->subNode.begin())
			{
				stack_func_hide.push(*iter);
				iter--;
			}
			stack_func_hide.push(*iter);
		}
	
	
	}
	
	
}


bool iter_reassin(treeNode *p)
{
   if(!p)
		return false;
	//找到改结点的函数根结点
	treeNode *temp_nodef = p;
	while(temp_nodef->parentNode	&& temp_nodef->parentNode->type != typeFuncDef)
	{
		temp_nodef = temp_nodef->parentNode;
	}
	treeNode *root_func_subtree = temp_nodef->parentNode;
	
	treeNode *temp_node_detect =p;
	while(temp_node_detect->parentNode 	&& (!compare_node(temp_node_detect->parentNode,root_func_subtree)))
	{
		temp_node_detect = temp_node_detect->parentNode;
					
		if(temp_node_detect->parentNode	&& temp_node_detect->parentNode->type == typeIterStm)
		{
			if(iter_reassinment(temp_node_detect,p))
			  return true;
		}
	}
	
	return false;
}
int find_hide_var(vector<hideNode> &array,everyhideNode &every_node)
{
    for(int i=0; i<array.size(); i++)
	{
		for(int j=0; j<array[i].hnode_var.size(); j++)
		{		    	
		        if(array[i].hnode_var[j].every_hnode_var.size()==every_node.every_hnode_var.size())
			    {
					int ii;
			        for(ii=0;ii<every_node.every_hnode_var.size();ii++)
			        {
			           if(array[i].hnode_var[j].every_hnode_var[ii].name!=every_node.every_hnode_var[ii].name)
			                   break;
			        }
			        if(ii==every_node.every_hnode_var.size())
			          return i;
			    }
			
		}	
	}
	return -1;
}

void deal_hvar(vector<hideNode> &array,everyhideNode &every_node,everyhideNode &every_node2,int line,treeNode *p)
{
   if(array.size() < 1) 
   {
     
      hideNode temp_hvar;
      temp_hvar.hnode_var.push_back(every_node);  
      temp_hvar.hnode_var.push_back(every_node2);
      array.push_back(temp_hvar); 
 //     //print_hvar(array);
    
      return;
    }
      
    ofstream file(HIDEOPERDECT_FILE, ios::out | ios::app);		// 将缺陷保存到文件中
   

    int line1=-1;
    int line2=-1;
    line1=find_hide_var(array,every_node);
    line2=find_hide_var(array,every_node2);
    
 //   //cout<<line1<<endl;
 //   //cout<<line2<<endl;
 //   //cout<<"%%%%%%%%%%%%%%%%%%"<<endl;
    
    if(line1!=-1 && line2!=-1 && line1==line2)
    {
        if(iter_reassin(p))
		{
		 return;
		}

		if(lable_reassin(p))
		{
		  return;
		}
        file << line << " "<< endl; //cout<<line<< " "<<"error!"<<endl;//print_hvar(array);//cout<<line<< "888888888888888 "<<"error!"<<endl;
        return;
    }
    if(line2!=-1 && line1==-1)  //
    {
       array[line2].hnode_var.push_back(every_node);////cout<<"************1"<<endl;//print_hvar(array);//cout<<"************1"<<endl;  
     //  //print_hvar(array);
      return;
    }
    if(line1==-1 && line2==-1)
    {
      hideNode temp_hvar;
      temp_hvar.hnode_var.push_back(every_node);  
      temp_hvar.hnode_var.push_back(every_node2);
      array.push_back(temp_hvar);////cout<<"************2"<<endl;//print_hvar(array);//cout<<"************2"<<endl;
 
      return;      
    }
    if(line1!=-1 &&line2==-1)
    {
      delete_hvar(array,every_node);////cout<<"************a"<<endl;//print_hvar(array);//cout<<"************a"<<endl;
      hideNode temp_hvar;
      temp_hvar.hnode_var.push_back(every_node);  
      temp_hvar.hnode_var.push_back(every_node2);
      array.push_back(temp_hvar);
     // //cout<<"************3"<<endl;//print_hvar(array);//cout<<"************3"<<endl;
      return;
    }
    if(line1!=-1 && line2!=-1)
    {
      delete_hvar(array,every_node);
      delete_hvar(array,every_node2);
      hideNode temp_hvar;
      temp_hvar.hnode_var.push_back(every_node);  
      temp_hvar.hnode_var.push_back(every_node2);
      array.push_back(temp_hvar);////cout<<"************4"<<endl;//print_hvar(array);//cout<<"************4"<<endl;
      return;
    }
        
 }

void delete_hvar(vector<hideNode> &array,everyhideNode &every_var)//gdd 删除array中，与str同名的变量
{
    for(int i=0; i<array.size(); i++)
	{
		for(int j=0; j<array[i].hnode_var.size(); j++)
		{	
		    if(array[i].hnode_var[j].every_hnode_var.size()>=every_var.every_hnode_var.size())	
		    {
		         for(int ii=0;ii<every_var.every_hnode_var.size();ii++)
			        {
			           for(int k=0;k<array[i].hnode_var[j].every_hnode_var.size();k++)
			           
			             if(array[i].hnode_var[j].every_hnode_var[k].name==every_var.every_hnode_var[ii].name)
			                  {
                                   array[i].hnode_var.erase(array[i].hnode_var.begin()+j);
                                   goto _end;
						 }
			        }
			}
            _end:;
		}
	}
}

void process_iter_hide(treeNode *temp_node, vector<hideNode> &parent_array_hvar, vector<hideNode> &array_hvar)
{
	//备份array_var
	vector<hideNode> array_hvar_copy = parent_array_hvar;
	
	ofstream file(HIDEOPERDECT_FILE, ios::out | ios::app);
	//备份array_var

	int count_node = exCountNode(temp_node);
	int count = 0;
	
	int flag_while = false;	//0604
//	elem while_elem;
	

	if(temp_node->subNode[0]->str == "while")
	{
           
		flag_while = true;
		
		
		
		while(!stack_cond.empty() && count < count_node-1)
		{
			treeNode *temp_node = stack_cond.top();
			stack_cond.pop();
			count++;
			if(temp_node->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp_node->subNode.end() - 1;
				while(iter != temp_node->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			switch(temp_node->type)
			{
			  
				case typeAssignExpr:
				case typeInitDec:
			         
					if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
							
					break;
				case typeUnaryExpr:
					if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
					break;	
				case typePostExpr:
					if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
					break;
				case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
				{
					process_if_hide(temp_node, array_hvar_copy,array_hvar);	//										
			
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					
					count += exCountNode(temp_node);
				}
					break;
				case typeIterStm:
					process_iter_hide(temp_node, array_hvar_copy,array_hvar);
				
				    count += exCountNode(temp_node);
					
					break;		
								
				default:
					;
			}
		}		
	}
	else if(temp_node->subNode[0]->str == "do")
	{
		//对do-while的条件不进行提取和判断，对其循环体内部按之前的规则找缺陷
		
		//跳过条件表达式
		int count_jump_node = 1;
		count += count_jump_node;
		jump_node_stack_cond(count_jump_node);
		
		int count_spe = exCountNode(temp_node->subNode[4]) + 4;	//
				
		while(!stack_cond.empty() && count < count_node-1-count_spe)	//
		{
			treeNode *temp_node = stack_cond.top();
			stack_cond.pop();
			count++;
			if(temp_node->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp_node->subNode.end() - 1;
				while(iter != temp_node->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			switch(temp_node->type)
			{
				case typeAssignExpr:
				case typeInitDec:
					if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
					break;
				case typeUnaryExpr:
					if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
					break;	
				case typePostExpr:
					if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
					break;
				case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
				{
					process_if_hide(temp_node, array_hvar_copy,array_hvar);	//										
			
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					
					count += exCountNode(temp_node);
				}
					break;
				case typeIterStm:
					process_iter_hide(temp_node, array_hvar_copy,array_hvar);
				
				count += exCountNode(temp_node);
					
					break;		
								
				default:
					;
			}
		}
		jump_node_stack_cond(count_spe);	//		
	}
	else	//for
	{		
						//	// cout <<"	" << count_node - 1 <<  endl;	//@@@@@@@@@@@@@@@@@@@@@@@@@@@	
		while(!stack_cond.empty() && count < count_node-1)
		{	
			treeNode *temp_node = stack_cond.top();		//// cout <<temp_node->lineno << endl;//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			stack_cond.pop();
			count++;
			if(temp_node->subNode.size() > 0)
			{
				vector<node*>::iterator iter = temp_node->subNode.end() - 1;
				while(iter != temp_node->subNode.begin())
				{
					stack_cond.push(*iter);
					iter--;
				}
				stack_cond.push(*iter);
			}
			
			switch(temp_node->type)
			{
				case typeAssignExpr:
				case typeInitDec:
					if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			
					break;
				case typeUnaryExpr:
					if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
					break;	
				case typePostExpr:
														
					if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
					break;
				case typeSelecStm:
					if(temp_node->subNode[0]->str == "if")
				{
					process_if_hide(temp_node, array_hvar_copy,array_hvar);	//										
			
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					
					count += exCountNode(temp_node);
				}
					break;
				case typeIterStm:
					process_iter_hide(temp_node, array_hvar_copy,array_hvar);
				
				count += exCountNode(temp_node);
					
					break;		
								
				default:
					;
			}
		}		
	}

	
	file.close();
//删除parent_array_hvar中array_hvar_copy中没有的元素	
delete_hide_elem(parent_array_hvar,array_hvar_copy);		
	//还原array_var
	array_hvar_copy.clear();
	array_hvar_copy = parent_array_hvar;	
}

void process_switch_hide(treeNode *temp_node, vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar)
{
	ofstream file(HIDEOPERDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_node);
	int count = 0;
	
	vector<hideNode> array_hvar_copy = parent_array_hvar;


	
	//对switch语句体简单处理，只寻找其中的赋值，初始化，前缀和后缀节点，删除涉及到的之前保存的变量记录
	while(!stack_cond.empty() && count < count_node-1)
	{
		treeNode *temp_node = stack_cond.top();
		stack_cond.pop();
		count++;
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		switch(temp_node->type)
		{
			case typeAssignExpr:
			case typeInitDec:
				if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			
				break;
			
			case typeUnaryExpr:	// ++var; --var;
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
				break;
			case typePostExpr:	// var++; var--;
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
				break;
			case typeSelecStm:
				//IF '(' expression ')' statement %prec IFX		
				//IF '(' expression ')' statement ELSE statement
				//SWITCH '(' expression ')' statement
				
				//递归处理
				if(temp_node->subNode[0]->str == "if")
				{
					process_if_hide(temp_node, array_hvar_copy,array_hvar);	//										
			
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					
					count += exCountNode(temp_node);
				}
				break;
			case typeIterStm:
			   // //cout<<"OOOOOOOOOOOOOOOOOOOOOOO"<<endl;
			process_iter_hide(temp_node, array_hvar_copy,array_hvar);
				
				count += exCountNode(temp_node);
		    case typeLabelStm: //case default
		        if(temp_node->subNode[0]->str == "case" || temp_node->subNode[0]->str == "default")
				{
				//删除parent_array_hvar中array_hvar_copy中没有的元素	
               delete_hide_elem(parent_array_hvar,array_hvar_copy);		
            	//还原array_var
            	array_hvar_copy.clear();
	              array_hvar_copy = parent_array_hvar;	
				 //  array_hvar_copy.clear();
	             //  array_hvar_copy = array_hvar;
				}	
			default:
				;		
		}
	}
}
	

void process_else_hide(treeNode *temp_noden, vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar)	//temp_node为typeSelecStm
{


	//备份array_var
	vector<hideNode> array_hvar_copy = parent_array_hvar;	//
	

																	
	ofstream file(HIDEOPERDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_noden);
	
	int count = 0;
	
	jump_node_stack_cond(1);
		
	while(!stack_cond.empty() && count < count_node-1)
	{
		treeNode *temp_node = stack_cond.top();	
		stack_cond.pop();
		count++;
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		switch(temp_node->type)
		{
			case typeAssignExpr:	//跟踪赋值语句，获得变量-常量绑定
			case typeInitDec:
				
				if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }		
			
				break;
			case typeUnaryExpr:	// ++var; --var;
			
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
				
				break;
			case typePostExpr:	// var++; var--;
			
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
				break;
				
			case typeSelecStm:
				//IF '(' expression ')' statement %prec IFX		
				//IF '(' expression ')' statement ELSE statement
				//SWITCH '(' expression ')' statement
				
				//递归处理
				if(temp_node->subNode[0]->str == "if")
				{
					process_if_hide(temp_node, array_hvar_copy,array_hvar);	//										
			
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					
					count += exCountNode(temp_node);
				}
				break;
			case typeIterStm:
			   // //cout<<"OOOOOOOOOOOOOOOOOOOOOOO"<<endl;
			process_iter_hide(temp_node, array_hvar_copy,array_hvar);
				
				count += exCountNode(temp_node);
				
			break;
			
			//...
				
			default:
				;
		}
	}	
		
	file.close();
//删除parent_array_hvar中array_hvar_copy中没有的元素	
delete_hide_elem(parent_array_hvar,array_hvar_copy);		
	//还原array_var
 	array_hvar_copy.clear();
	array_hvar_copy = parent_array_hvar;	

  	
}


void process_if_hide(treeNode *temp_noden, vector<hideNode> &parent_array_hvar,vector<hideNode> &array_hvar)	//temp_node为typeSelecStm
{
	//备份array_var
	vector<hideNode> array_hvar_copy = parent_array_hvar;	//
	
	
	ofstream file(HIDEOPERDECT_FILE, ios::out | ios::app);
	int count_node = exCountNode(temp_noden->subNode[0])+exCountNode(temp_noden->subNode[1])
	                 +exCountNode(temp_noden->subNode[2])+exCountNode(temp_noden->subNode[3])
	                 +exCountNode(temp_noden->subNode[4]);
	
	int count = 0;
//cout<<"aaaaaaaaaaaaaaa"<<endl;
  //print_hvar(array_hvar_copy);
//cout<<"bbbbbbbbbbbbbb"<<endl;
//print_hvar(array_hvar);	
//cout<<"cccccccccccccc"<<endl;
	while(!stack_cond.empty() && count < count_node)
	{
		treeNode *temp_node = stack_cond.top();	
		stack_cond.pop();
		count++;
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
		
		switch(temp_node->type)
		{
			case typeAssignExpr:	//跟踪赋值语句，获得变量-常量绑定
			case typeInitDec:
				
				if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          exvar(temp_node->subNode[0],every_node);
	                  
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  delete_hvar(array_hvar_copy,every_node);
	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                      delete_hvar(array_hvar_copy,every_node);
	                      
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                          delete_hvar(array_hvar_copy,every_node); 
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar_copy,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			
				break;
			case typeUnaryExpr:	// ++var; --var;
			
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
				break;
			case typePostExpr:	// var++; var--;
			
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
	                delete_hvar(array_hvar_copy,every_node);
				}
				break;
				
			case typeSelecStm:
			
				//递归处理
				if(temp_node->subNode[0]->str == "if")
				{
					process_if_hide(temp_node, array_hvar_copy,array_hvar);	//										
			
					count += exCountNode(temp_node);
					
				}
				else
				{
					process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					
					count += exCountNode(temp_node);
				}
				break;
			case typeIterStm:
			  
				process_iter_hide(temp_node, array_hvar_copy,array_hvar);
				
				count += exCountNode(temp_node);
				
			break;
			
			//...
				
			default:
				;
		}
	}	
		
	file.close();
//删除parent_array_hvar中array_hvar_copy中没有的元素	
delete_hide_elem(parent_array_hvar,array_hvar_copy);	
	//还原array_var
	array_hvar_copy.clear();
	array_hvar_copy = parent_array_hvar;	
//	parent_array_hvar.clear();
//	parent_array_hvar = array_hvar;	
//cout<<"dddddddddddddddd"<<endl;	
//print_hvar(array_hvar_copy);
//cout<<"eeeeeeeeeee"<<endl;
//print_hvar(array_hvar);	
//cout<<"fffffffffffff"<<endl;

	if(temp_noden->subNode.size()>5)
	  process_else_hide(temp_noden->subNode[6],parent_array_hvar ,array_hvar);
	  	
}

void exvar(treeNode *p, everyhideNode &every_var)
{
    stack<treeNode*> s_exvar;
	//while(!s_exvar.empty())
	//{
	//	s_exvar.pop();
	//}

    s_exvar.push(p);
    
	while(!s_exvar.empty())
	{
		treeNode *temp_node = s_exvar.top();
		s_exvar.pop();
		
		if(temp_node->subNode.size() > 0)
		{
			vector<treeNode*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				s_exvar.push(*iter);
				iter--;
			}
			s_exvar.push(*iter);
		}
		
		if(temp_node->subNode.size() == 0)
		{
			hnode temp_elem;
			temp_elem.name = temp_node->str;				
				
			temp_elem.lineno = temp_node->lineno;
			every_var.every_hnode_var.push_back(temp_elem);
		}
	}
}

//gdd 隐式幂等检查器
void hideOperDect(treeNode *p)
{
	// cout <<endl << "-------------------hideOperDect  detection-------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;

	if(!p)
		return;
	
	while(!stack_cond.empty())
		stack_cond.pop();
		
	ofstream file(HIDEOPERDECT_FILE, ios::out | ios::app);		// 将缺陷保存到文件中	
	
	//缺陷检测开始
	stack_cond.push(p);
	treeNode *temp_node = NULL;
	
	while(!stack_cond.empty())
	{
		temp_node = stack_cond.top();
		stack_cond.pop();
		if(temp_node->subNode.size() > 0)
		{
			vector<node*>::iterator iter = temp_node->subNode.end() - 1;
			while(iter != temp_node->subNode.begin())
			{
				stack_cond.push(*iter);
				iter--;
			}
			stack_cond.push(*iter);
		}
    
    			
		treeNode *node = NULL;
		switch(temp_node->type)
		{
			case typeFuncDef:
				//file << temp_node->lineno << " "<< endl;
				// cout <<"	---------print 1 hide------------" << endl;
				array_hvar.clear();
				break;
				
			case typeAssignExpr:	//跟踪赋值语句
			case typeInitDec:
			    if(temp_node->subNode[0]->type == typeId)
			    {
			       
			       if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
	                  delete_hvar(array_hvar,every_node);
	               
	               }
	               if(temp_node->subNode[1]->str == "=")
				   {
				      
				      everyhideNode every_node;
	                  exvar(temp_node->subNode[0],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node);  
				          }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    if(temp_node->subNode[0]->type==typeUnaryExpr &&temp_node->subNode[0]->subNode[0]->str=="*")
			    {
			        if(temp_node->subNode[1]->str == "*="||temp_node->subNode[1]->str == "/="||
	                  temp_node->subNode[1]->str == "%="||temp_node->subNode[1]->str == "+="||
	                  temp_node->subNode[1]->str == "-="||temp_node->subNode[1]->str == "<<="||
	                  temp_node->subNode[1]->str == ">>="||temp_node->subNode[1]->str == "&="||
	                  temp_node->subNode[1]->str == "^="||temp_node->subNode[1]->str == "|=")
	               {
	                  
	                  everyhideNode every_node;
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
	                  delete_hvar(array_hvar,every_node);
	                  	               
	               }
	               
	               if(temp_node->subNode[1]->str == "=")
				   {
				      everyhideNode every_node;
				          
	                  exvar(temp_node->subNode[0]->subNode[1],every_node);
				      
				      if(temp_node->subNode[2]->type == typeConstant)
				      {				          
	                      delete_hvar(array_hvar,every_node);
	                     
				      }
				      else
				      { 
				          if(exSelecStr(temp_node->subNode[2],temp_node->subNode[0]->subNode[1]->str))
				          {
				             
	                          delete_hvar(array_hvar,every_node); 
	                      }
				          else
				          {	
				              everyhideNode every_node2;
	                          exvar(temp_node->subNode[2],every_node2);			              
				              deal_hvar(array_hvar,every_node,every_node2,temp_node->subNode[0]->lineno,temp_node);				             		          				          
				          }
				      }					  
				   }
			    }
			    
			    break;
			case typeUnaryExpr:	// ++var; --var;
			
				if(temp_node->subNode[0]->str == "++" || temp_node->subNode[0]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[1],every_node);
	                delete_hvar(array_hvar,every_node);
				}
				
				break;
			case typePostExpr:	// var++; var--;
			
				if(temp_node->subNode[1]->str == "++" || temp_node->subNode[1]->str == "--")
				{
					everyhideNode every_node;
	                exvar(temp_node->subNode[0],every_node);
	                delete_hvar(array_hvar,every_node);
				}
				
				break;
			case typeSelecStm:		//IF '(' expression ')' statement %prec IFX		
									//IF '(' expression ')' statement ELSE statement
									//SWITCH '(' expression ')' statement
				{
					vector<hideNode> array_hvar_copy=array_hvar;	
				//	// cout <<"over over over over mmm " << endl;
			    //	//print_array_var(array_var_copy);										
					if(temp_node->subNode[0]->str == "if")	// 处理if语句
					{
						process_if_hide(temp_node, array_hvar_copy,array_hvar);
					}
					else	//switch
					{
						//提取条件表达式，如果可以计算则为缺陷
						//对switch语句体简单处理，只寻找其中的赋值，初始化，前缀和后缀节点，删除涉及到的之前保存的变量记录
						process_switch_hide(temp_node, array_hvar_copy,array_hvar);
					}											
				}
							
				break;
			case typeIterStm:
				{	
					vector<hideNode> array_hvar_copy=array_hvar;
					process_iter_hide(temp_node, array_hvar_copy,array_hvar);	//
				}
							
				break;
		
			default:
				;
		}
		
	}
	

	
	file.close();
	
	// cout <<"------------------------------------------------------" << endl;
	// cout <<"------------------------------------------------------" << endl;
	// cout <<endl << "-------------------------end-----------------------------" << endl;	


}


