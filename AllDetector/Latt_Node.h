// Latt_Node.h: interface for the Latt_Node class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LATT_NODE_H__95F3ECDC_6104_4894_BE11_61E40CF4BE8F__INCLUDED_)
#define AFX_LATT_NODE_H__95F3ECDC_6104_4894_BE11_61E40CF4BE8F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"

class Latt_Node  
//偏序前缀序列图的结点
{
public:
	class Latt_Node *Father;//指向父节点（序列中的前一项）的指针
	CArray <Latt_Node*,Latt_Node*> Son;//指向子节点（序列中的后一项）的指针数组
	CArray<Ds_NODE,Ds_NODE> Ds_Arry;//存储投影数据库Ds中所有序列信息的数组
private:
	CString Item; //该结点存储的项（items），即由空格和数字组成的长度为10的字符串
	int Support;  //该节点表示的序列的支持度
	int Ds_Size;  //投影数据库Ds的大小
	
public:
	Latt_Node();
	Latt_Node(CString N_Item, int N_Support);
	Latt_Node(class Latt_Node &LN);
	Latt_Node& operator=(Latt_Node &LN);
	virtual ~Latt_Node();

	CString Get_Item(); //返回该节点的项值（Item）
	int Get_Sup();      //返回该节点对应序列的支持度（Support）
	int Get_Dsize();    //返回该节点对应序列的影射数据库大小（Ds_Size）	
	void Add_DsNode(int ID, int Addr, int len);//将节点DN加入到数组Ds_Arry中
	void Add_Sup();     //该节点中的序列支持度加一;
	
private:
	//
};

#endif // !defined(AFX_LATT_NODE_H__95F3ECDC_6104_4894_BE11_61E40CF4BE8F__INCLUDED_)