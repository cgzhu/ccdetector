#pragma once
#include "afxcmn.h"
#include "BetweenessCentralityAlgorithm.h"

// CBetweenessCentralityDlg 对话框

class CBetweenessCentralityDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBetweenessCentralityDlg)

public:
	CBetweenessCentralityDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CBetweenessCentralityDlg();

// 对话框数据
	enum { IDD = IDD_BETWEENESS_CENTRALITY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl betweenessCentralityList;
};
