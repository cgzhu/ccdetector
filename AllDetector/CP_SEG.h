// CP_SEG.h: interface for the CP_SEG class.
// wq-20081120
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CP_SEG_H__0FEEC0D0_2400_446A_89E9_A52267FAADDE__INCLUDED_)
#define AFX_CP_SEG_H__0FEEC0D0_2400_446A_89E9_A52267FAADDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "IdLocSetMem.h"
#include "FilesToTokens.h"

class CP_SEG  
{
public:
	CP_SEG();
	CP_SEG(const CP_SEG& cp_seg);
	virtual ~CP_SEG();
	
	CP_SEG& operator=(const CP_SEG& cp_seg);
	void UpdateIdsLocArray(CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry);

public:
	bool ReadCFile(const CString cfilename,CString &program);
	CString filename;//代码片段所在的文件名
	int funcBegLine;   //wq-20081220-所在函数的源程序起始(相对)行数
	int funcEndLine;   //wq-20081220-所在函数的源程序终止(相对)行数

	int	funcBgnRealLine;//wq-20090202-函数起始源程序绝对行数
	int funcEndRealLine;//wq-20090202-函数终止源程序绝对行数

	long FN_Hvalue;//文件名转化的散列值
	CString LPOS;//将每行代码在程序中的绝对位置（四位整数，不够用零补齐）组成字符串
	CString relaLPOS;//相对位置(函数)	该行在源程序token化后的相对行数


	int Beg_index;  //起始模块的序号
	int End_index;  //结束模块的序号
	int B_gap;//子序列的头与原序列的头之间的距离
	int E_gap;//子序列的尾与原序列的尾之间的距离
	bool Is_combine;//是否曾经合并的标志
	int combineTimes;

	CArray<IdLocSetMem,IdLocSetMem> idsLocArray;//wq-20090223-add-记录每个代码片段的所有标识符位置信息

};

#endif // !defined(AFX_CP_SEG_H__0FEEC0D0_2400_446A_89E9_A52267FAADDE__INCLUDED_)
