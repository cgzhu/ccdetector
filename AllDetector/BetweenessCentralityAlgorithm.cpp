#include "StdAfx.h"
#include "BetweenessCentralityAlgorithm.h"
#include <iostream>
using namespace std;

#define MAXLEN 1000
#define MAXINT 8

int matrix[1000][1000];

int dist[1000][1000];
int S[1000];
int prev_node[1000][1000];

float ratio[1000];
int fenmus[1000] = {0};//

BetweenessCentralityAlgorithm::BetweenessCentralityAlgorithm(void)
{
}


BetweenessCentralityAlgorithm::~BetweenessCentralityAlgorithm(void)
{
}

void calculate_ratio(int id,int start_id,int fenmu)
{
	if(id == start_id)
	{
		return ;
	}
	else
	{
		int prev_num = prev_node[id][0];
		ratio[id] += (float)fenmus[id]/(float)fenmu;//
		for(int i=0;i<prev_num;i++)
		{
			calculate_ratio(prev_node[id][i+1],start_id,fenmu);
		}
	}

}

void BetweenessCentralityAlgorithm::start(void)
{
	for(int i=0;i<1000;i++)
	{
		for(int j=0;j<1000;j++)
		{
			matrix[i][j] = MAXINT;
		}
	}

	for(int i=0;i<ccGroupNumber;i++)
	{
		for(int j=0;j<ccGroupNumber;j++)
		{
			if(ccGroupGraph[i][j]!=0 || (ccGroupGraph[i][j]==0 && i==j))
			{
				matrix[i+1][j+1] = ccGroupGraph[i][j];
			}
		}
	}
	
	for(int i=1;i<=ccGroupNumber;i++)
	{
		memset(prev_node,0,sizeof(prev_node));//
		memset(fenmus,0,sizeof(fenmus));//
		//Dijkstra
		for(int j=1;j<=ccGroupNumber;j++)
		{
			dist[i][j] = matrix[i][j];
			S[j] = false;
			if(dist[i][j] == MAXINT)
			{
				prev_node[j][0] = 1;
				prev_node[j][1] = -1;
			}
			else
			{
				prev_node[j][0] = 1;
				prev_node[j][1] = i;
			}
		}
		dist[i][i] = 0;
		S[i] = true;
		for(int j=2;j<=ccGroupNumber;j++)
		{
			int mindist = MAXINT;
			int next_node = i;

			//找到加入的点
			for(int k=1;k<=ccGroupNumber;k++)
			{
				if((!S[k]) && dist[i][k]<mindist)
				{
					next_node = k;
					mindist = dist[i][k];
				}
			}
			S[next_node] = true;

			//更新分母
			if(dist[i][next_node] == 1)//
			{
				fenmus[next_node] = 1;//
			}
			
			if(matrix[i][next_node] != 1 && i !=next_node)
			{
				for(int m=0;m<=ccGroupNumber;m++)
				{
					if(matrix[next_node][m]==1)//找到与next_node相邻的所有节点
					{
						int prev_num = prev_node[next_node][0];
						int flag = false;
						for(int c=0;c<prev_num;c++)
						{
							if(prev_node[next_node][c+1] == m)
							{
								flag = true;
							}
						}
						if(dist[i][m] < dist[i][next_node] && flag == false)//如果该点与起始点的距离不是正无穷
						{
							//cout<<"m="<<m<<endl;
							fenmus[next_node] += fenmus[m];//
							//将该点加入到next_node的前缀中
							int current_prev_num = prev_node[next_node][0];
							prev_node[next_node][current_prev_num+1] = m;
							prev_node[next_node][0] += 1;
						}
					}
				}

				//对所有前缀节点，递归加分子
				int prev_num = prev_node[next_node][0];
				for(int p=0;p<prev_num;p++)
				{
					calculate_ratio(prev_node[next_node][p+1],i,fenmus[next_node]);//
				}

			}

			//更新相连的权值
			for(int k=1;k<=ccGroupNumber;k++)
			{
				if((!S[k]) && matrix[next_node][k]<MAXINT)
				{
					//cout<<"next node  "<<next_node<<endl;
					if(dist[i][next_node] + matrix[next_node][k] < dist[i][k])
					{
						dist[i][k] = dist[i][next_node] + matrix[next_node][k];
						prev_node[k][1] = next_node;
						//更新相连的分母
						fenmus[k] = fenmus[next_node];//
					}
				}
			}
			

		}
	}

	/*测试
	CString temp11;
	for(int kk = 1; kk<=17; kk++)
	{
		char num[256];
		itoa((int)ratio[kk],num,10);
		temp11 += num;
		temp11 += "\n";
	}
	AfxMessageBox(temp11);*/

	//getchar();
	return ;
}