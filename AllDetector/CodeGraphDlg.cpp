// CodeGraphDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AllDetector.h"
#include "CodeGraphDlg.h"
#include "afxdialogex.h"
#include "MainFrm.h"

// CCodeGraphDlg 对话框

IMPLEMENT_DYNAMIC(CCodeGraphDlg, CDialogEx)

CCodeGraphDlg::CCodeGraphDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCodeGraphDlg::IDD, pParent)
{
	isFirstShow = true;
}

CCodeGraphDlg::~CCodeGraphDlg()
{
}

void CCodeGraphDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCodeGraphDlg, CDialogEx)
	ON_WM_PAINT()
	ON_MESSAGE(WM_VERTEX_REPAINT, OnVertexRepaint) //重绘消息映射
END_MESSAGE_MAP()


BOOL CCodeGraphDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	/*p_MyBut = new CMovableButton();
	p_MyBut->Create( _T("动态按钮"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(20,10,80,40), this, 501);
	p_MyBut->previous_x = 30;
	p_MyBut->previous_y = 25;

	p_MyBut2 = new CMovableButton();
	p_MyBut2->Create( _T("态按钮"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(100,60,160,90), this, 502);

	p_MyBut3 = new CMovableButton();
	p_MyBut3->Create( _T("按钮"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(180,110,240,140), this, 503);*/

	srand(time(NULL));

	for(int i=0; i<ccGroupNumber; i++)
	{
		p_MovableButtons[i] = new CMovableButton();

		char text[256];
		itoa(i+1,text,10);

		srand(rand()*i);

		int x = rand()%500;
		int y = rand()%400;

		p_MovableButtons[i]->Create(_T(text), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(x,y,x+30,y+30), this, 500+i);
		p_MovableButtons[i]->now_x = x +15;
		p_MovableButtons[i]->now_y = y +15;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CCodeGraphDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialogEx::OnPaint()
	
	if(isFirstShow)
	{
		CPen pen(PS_SOLID,1,RGB(255,0,0)); 

		CClientDC dc1(this);
		CPen *pOldPen=dc1.SelectObject(&pen); //将其选入设备表

		for(int i=0; i<ccGroupNumber; i++)
		{
			for(int j=0; j<ccGroupNumber; j++)
			{
				if(ccGroupGraph[i][j] == 1 && i<j)
				{
					dc1.MoveTo(CPoint(p_MovableButtons[i]->now_x,p_MovableButtons[i]->now_y));
					dc1.LineTo(CPoint(p_MovableButtons[j]->now_x,p_MovableButtons[j]->now_y));
				}
			}
		}

		dc1.SelectObject(pOldPen); 
		isFirstShow = false;
	}
}

/*
 * 函数功能：重绘消息的处理函数
 */
LRESULT CCodeGraphDlg::OnVertexRepaint(WPARAM wParam, LPARAM lParam)
{
	Invalidate();
	UpdateWindow(); //清除线条


	CPen pen(PS_SOLID,1,RGB(255,0,0)); 

	CClientDC dc1(this);
	CPen *pOldPen=dc1.SelectObject(&pen); //将其选入设备表

	for(int i=0; i<ccGroupNumber; i++)
	{
		for(int j=0; j<ccGroupNumber; j++)
		{
			if(ccGroupGraph[i][j] == 1 && i<j)
			{
				dc1.MoveTo(CPoint(p_MovableButtons[i]->now_x,p_MovableButtons[i]->now_y));
				dc1.LineTo(CPoint(p_MovableButtons[j]->now_x,p_MovableButtons[j]->now_y));
			}
		}
	}

	/*dc1.MoveTo(CPoint(p_MyBut->now_x,p_MyBut->now_y));
	dc1.LineTo(CPoint(130,75));
	p_MyBut->link_nodes[2] = 1;
	p_MyBut->link_nodes[3] = 1;

	dc1.MoveTo(CPoint(p_MyBut->now_x,p_MyBut->now_y));
	dc1.LineTo(CPoint(210,125));*/

	dc1.SelectObject(pOldPen);  

   return 1;
}
