// CPFilesListView.cpp : implementation file
//
/*
 * 这个类是一个文件树类，基类是CTreeView
 */

#include "stdafx.h"
#include "AllDetector.h"
#include "CPFilesListView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCPFilesListView

IMPLEMENT_DYNCREATE(CCPFilesListView, CTreeView)

CCPFilesListView::CCPFilesListView()
{

}

CCPFilesListView::~CCPFilesListView()
{
}


BEGIN_MESSAGE_MAP(CCPFilesListView, CTreeView)
	//{{AFX_MSG_MAP(CCPFilesListView)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCPFilesListView drawing

void CCPFilesListView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CCPFilesListView diagnostics

#ifdef _DEBUG
void CCPFilesListView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CCPFilesListView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCPFilesListView message handlers

/*
 * FillTree函数功能：构建文件树
 */
void CCPFilesListView::FillTree(Out_CpPos *Ocp, CImageList &imgList, HTREEITEM &m_hCTree)
{
	CMainFrame *pFrame = (CMainFrame *)GetParent()->GetParent();
//	pFrame->m_pSourceCodeView1->ReDrawSourceCodeView("");
//	pFrame->m_pSourceCodeView2->ReDrawSourceCodeView("");
//	pFrame->m_pSourceCodeView1->selected = 1;
//	pFrame->m_pSourceCodeView2->selected = -1;
//	pFrame->m_pTabFormView1->selected = 1;
//	pFrame->m_pTabFormView2->selected = 0;

	HTREEITEM subhti;
    int i;
    CString str_i;

	CArray <CP_Segment, CP_Segment> &CP_Seg_Array = Ocp->CP_Seg_Array;
	int cpSize = CP_Seg_Array.GetSize();

	for( i = 0; i < cpSize; i++ )
	{
		if(CP_Seg_Array[i].len >= Min_len)//大于最小检测行数
		{
			str_i.Format("%d",i+1);
			subhti=GetTreeCtrl().InsertItem(TREE_ITEMS[1] + str_i ,0,1,m_hCTree);

			int j;
			CString str_j;
			int fileNum = CP_Seg_Array[i].CP_Array.GetSize();
			for( j = 0; j < fileNum; j++ )
			{
				str_j.Format("%d",j+1);
				GetTreeCtrl().InsertItem(TREE_ITEMS[2] + str_j ,2,2,subhti);
			}
		}
	}
}

void CCPFilesListView::OnInitialUpdate() 
{
	CTreeView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	//wq-20090201-树形文件展开图标
	GetTreeCtrl().ModifyStyle(NULL,
							  //wq-20090201-树控件的样式
							  TVS_HASBUTTONS    //在每一父项的左边添加一个展开/折叠按钮
							  | TVS_HASLINES    //在子项与其相应的父项之间画上一条连线
							  | TVS_LINESATROOT //与TVS_HASLINES组合，链接层次结构根位置的项
							  );

}

/**/
void CCPFilesListView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	CTreeCtrl &cTree=GetTreeCtrl();

	CString str_itemText(""),str_itemParentText("");
	str_itemText = cTree.GetItemText(cTree.GetSelectedItem());
	str_itemParentText = cTree.GetItemText(cTree.GetParentItem(cTree.GetSelectedItem()));

	int cpGroup = -1;
	if( str_itemParentText.GetLength() > TREE_ITEMS[1].GetLength() )
	{
		cpGroup	= atoi(str_itemParentText.Mid(TREE_ITEMS[1].GetLength(),str_itemParentText.GetLength())) - 1;
	}

	int cpSeg = -1;
	if( str_itemText.GetLength() > TREE_ITEMS[2].GetLength() )
	{
		cpSeg = atoi(str_itemText.Mid(TREE_ITEMS[2].GetLength(),str_itemText.GetLength())) - 1;
	}

	CMainFrame *pFrame = (CMainFrame *)GetParent()->GetParent();

	if( cpGroup > -1 && cpSeg > -1 
		&& cpGroup < pFrame->Ocp.CP_Seg_Array.GetSize()
		&& cpSeg <pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array.GetSize()
		&& pFrame->Ocp.CP_Seg_Array[cpGroup].len >= Min_len)
	{

		CString filePath = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].filename;
		CString linePos = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].LPOS;//绝对位置
		CString relaPos = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].relaLPOS;
		int funcBgn = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].funcBegLine;
		int funcEnd = pFrame->Ocp.CP_Seg_Array[cpGroup].CP_Array[cpSeg].funcEndLine;
		CString viewText(""),funcViewText("");
		viewText += "filePath:" + filePath + "\r\n" + "LPOS:" + linePos + "\r\n\r\n";
		funcViewText = viewText;
		viewText += GetSourceCode(filePath,linePos);
		funcViewText += pFrame->filesToTokens.GetFuncSourceCode(filePath,relaPos,linePos,funcBgn,funcEnd,pFrame->headerFilesArry);
		/*if(pFrame->m_pTabFormView1->selected == 1)
		{
			pFrame->m_pTabFormView1->m_tabPage2.m_pageEdit.SetWindowText(viewText);
			pFrame->m_pTabFormView1->m_tabPage1.m_pageEdit.SetWindowText(funcViewText);
		}
		else if(pFrame->m_pTabFormView2->selected == 1)
		{
			pFrame->m_pTabFormView2->m_tabPage2.m_pageEdit.SetWindowText(viewText);
			pFrame->m_pTabFormView2->m_tabPage1.m_pageEdit.SetWindowText(funcViewText);
		}*/
	}
	
	
	CTreeView::OnLButtonDblClk(nFlags, point);
}

void CCPFilesListView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CTreeCtrl &cTree=GetTreeCtrl();
	CTreeView::OnLButtonDown(nFlags, point);
}

/**/
CString CCPFilesListView::GetSourceCode(CString filePath, CString linePos)
{
	CStdioFile codeFile;
	if( !codeFile.Open(filePath, CFile::modeRead) )
	{
		return _T("");
	}
	if( linePos.GetLength() <= 0 )
	{
		return _T("");
	}
	int lineNum = 1;
	CString srcLine(""),viewLine(""),viewText("");
	CString str_LPOS = linePos;//.Mid(0,linePos.GetLength()-8);
	while(codeFile.ReadString(srcLine))
	{
		int cpLine = -1;
		if(Find_Code_Line(lineNum,cpLine,str_LPOS))
		{
			viewLine = FormViewLine(srcLine,lineNum,cpLine);
		}
		else
		{
			viewLine = FormViewLine(srcLine,lineNum,-1);
		}
		viewText += viewLine;
		lineNum++;
	}
	return viewText;

}

/**/
bool CCPFilesListView::Find_Code_Line(int curpos, int &cpline, CString &linePos)
{
	CString astrPos = _T("");
	int pos;
	int k,m;
    for( k = 0 , m = 1; k < linePos.GetLength(); k += 4 , m++)
	{
		astrPos = linePos.Mid(k,4);
		pos = atoi(astrPos);
		if(curpos == pos)
		{
			cpline = m;
			return true;
		}
	}
	return false;

}

CString CCPFilesListView::FormViewLine(CString strLine, int lineNum, int cpLine)
{
	CString viewLine("");
	CString str_lineNum,str_cpLine;
	str_lineNum.Format("%d",lineNum);

	while( str_lineNum.GetLength() < 5 )
	{
		str_lineNum += " ";
	}
	viewLine+=str_lineNum;


	if( cpLine > 0 )
	{
		str_cpLine.Format( "%d",cpLine );
		while( str_cpLine.GetLength() < 5 )
		{
			str_cpLine += " ";
		}
		str_cpLine = "-CP" + str_cpLine + "- ";
		viewLine += str_cpLine;
	}
	else 
	{
		while( viewLine.GetLength() < 5+5+7 )
		{
			viewLine += " ";
		}
	}
	viewLine += strLine + "\r\n";
	return viewLine;
}

/*
 * 函数功能：删除子节点
 */
void CCPFilesListView::DeleteChildItems(HTREEITEM &m_hCTree)
{
	HTREEITEM subhti,child_subhti;
	while(GetTreeCtrl().ItemHasChildren(m_hCTree))
	{
		subhti = GetTreeCtrl().GetChildItem(m_hCTree);
		while( GetTreeCtrl().ItemHasChildren(subhti))
		{
			child_subhti = GetTreeCtrl().GetChildItem(subhti);
			GetTreeCtrl().DeleteItem(child_subhti);
		}
		GetTreeCtrl().DeleteItem(subhti);

	}

}

CString CCPFilesListView::GetFuncSourceCode(int cpGroup, int cpSeg)
{
	CString funcViewText("");

	return funcViewText;
}
