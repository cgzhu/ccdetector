// Unit.cpp: implementation of the CUnit class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Unit.h"

#include "SDGSwitch.h"
#include "SDGReturn.h"
#include "Expression.h"
#include "SDGAssignment.h" 
#include "SDGDeclare.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUnit::CUnit()
{


}

CUnit::~CUnit()
{

}

void CUnit::operator()(CSDGBase *pSDGHead,CArray<SNODE,SNODE> &SArry)
{
		if(SArry.GetSize()<=0)
		return ;
	m_pSDGHead=pSDGHead;
//		AfxMessageBox("ok");////////////

	TransSDGToUnit(pSDGHead,SArry);//后根序遍历SDG，根据SDG节点的类型创建对应的USDG节点，并加入控制依赖关系
//	AfxMessageBox("ok");////////////
	SetElseSelector(SArry);//后根序遍历USDG，若SelectionUnit的最后一个字节点不是_ELSE类型，则新建一个_ELSE类型节点
	SetItrBranch(SArry);//后根序遍历USDG，向IterationUnit的控制依赖关系序列中加入ITRSUB
	
	CaseBreak(pSDGHead);//后根序遍历USDG，查找由switch语句转换而成的selectionUnit,并将其中的break语句删除
	SetBoolExpToElseExp(pSDGHead,SArry);//后根序遍历USDG，设置_ELSE节点的条件表达式

}

void CUnit::TransSDGToUnit(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/************************************************************************
	  FUNCTION:
	  Create Unit representation(USDG) through post-order traversing SDG,at 
	  the same time,delete the old SDG
	  PARAMETER:
	  m_pSDGHead: class member,point to the head of SDG
	************************************************************************/
	
	CList<CSDGBase *,CSDGBase *> S,SU;
    CSDGBase *p=NULL,*pUnit=NULL;
	CSDGBase *pNewHead=NULL;
	
	// Copy SDG to Unit representation;
    if(pHead==NULL)
		return;
    p=pHead;
	pUnit=CopyNode(p,pHead,SArry);

	pNewHead=pUnit;
//  AfxMessageBox("copy head ok");
	while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p);

			SU.AddTail(pUnit);
			

	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);

				// copy  node or create new node to Unit Representation,and..
				pUnit=CopyNode(p,S.GetTail(),SArry);
				// Add new Unit to USDG
				if(SU.GetTail())
				{
				SU.GetTail()->Add(CN_CONTROL,pUnit);
				pUnit->SetFather(SU.GetTail());
				}
				// End
			}
			else
			{
				p=NULL; 
			}	

		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			   			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);

				// copy  node or create new node to Unit Representation
				pUnit=CopyNode(p,S.GetTail(),SArry);			
				// Add new Unit to USDG
				if(SU.GetTail())
				{
				SU.GetTail()->Add(CN_CONTROL,pUnit);
				pUnit->SetFather(SU.GetTail());
				}
			}
			else if(p)
			{			
				p->visit=0;
				p=NULL;

				S.RemoveTail();
				SU.RemoveTail(); 
			}
		}
    }
    //f.Close(); 
	// end
    
//  AfxMessageBox("copy SDG ok");///
	
	
	// Delete SDG but keep the syntax tree down;
	for(int i=0;i<m_pSDGHead->GetRelateNum();i++)
	{
	p=pHead->GetNode(i);
	S.RemoveAll();
	
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p&&p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			// end
			   			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else if(p)
			{
				p->visit=0;
				if(p)
				delete p;
				p=NULL;
			}
		}
    }
	}
    //end;


	// Create new USDG
	pHead->RemoveAll();
	for(int i=0;i<pNewHead->GetRelateNum();i++)
	{
		if(pNewHead->GetNode(i))
		{
		pHead->Add(CN_CONTROL,pNewHead->GetNode(i));
		pNewHead->GetNode(i)->SetFather(pHead); 
		}
	}
	pNewHead->RemoveAll();
	if(pNewHead)
	delete pNewHead;
	pNewHead=NULL;
	
}

void CUnit::SetSelector(CSDGBase* pSelector,CSDGBase* pSelection,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Part of the function that translate the if ,if-else,
		switch statement to the Selection Unit in USDG
			    
		PARAMETER:
		pSelector : pointer which is one of the branch of 
		            selection unit.
		pSelection: the encounter part as pSelector's father
		            in SDG(switch or if-else node in SDG)  
	******************************************************/

	extern void BoolExpToSTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry);

	if(pSelection==NULL||pSelector==NULL)
		return;
	if(pSelection->m_sType=="SDGIF")
	{
		if(pSelector->m_sType=="IFLINK")
		{
			pSelector->m_pinfo=pSelection->m_pinfo;
			pSelection->m_pinfo=NULL;
			pSelector->m_sType=_T("SELECTOR");


		}
		else if(pSelector->m_sType=="ELSELINK")
		{
			pSelector->m_sType=_T("_ELSE"); 
		}
	}
	else if(pSelection->m_sType=="SDGSWITCH")
	{
	
		if(pSelector->m_sType=="SELECTORLINK")
		{
			// combine the "switch" value and the "case" value to a bool exp.
//		AfxMessageBox("deal with selectorlink");///////////
			TNODE T;
			
			T.paddr=-1; 
		
			T.line=0;
			T.name=_T("");
			T.value=0; 
			CArray<TNODE,TNODE> aTemp;
			
			aTemp.RemoveAll();
			aTemp.Copy(pSelection->TCnt);
		//	AfxMessageBox("deal with selectorlink:aTemp.Copy(pSelection->TCnt) ok ");////////
		//	pSelector->TCnt.Copy(pSelection->TCnt);
			T.key=CN_CINT;
			T.addr=pSelector->GetNodeVL()   ;
			T.deref=0;//wtt 2008 3.19
			pSelector->TCnt.Add(T);
		//	AfxMessageBox("deal with selectorlink:pSelector->TCnt.Add(T) ");////////

			T.key=CN_DEQUAL;
			T.addr=-1;
			T.deref=0;//wtt 2008 3.19
			pSelector->TCnt.Add(T);
		//	AfxMessageBox("deal with selectorlink:pSelector->TCnt.Add(T) =");////////

			////////wtt//4.16//去掉强制类型转换/////
			int k=0;
			 if(aTemp.GetSize()>3&&aTemp[0].key==CN_LCIRCLE&&aTemp[1].key==CN_INT&&aTemp[2].key==CN_RCIRCLE)
			 {
				 k=3;

			 }
			 ///////////////////////////////////////////
			for(;k<aTemp.GetSize();k++)
			{
				pSelector->TCnt.Add(aTemp.GetAt(k) );
//				AfxMessageBox(C_ALL_KEYWORD[aTemp.GetAt(k).key] );

			}
		//	AfxMessageBox("deal with selectorlink:pSelector->TCnt.Add(aTemp.GetAt(k))");////////
	
			// end

			// translate the bool exp to a syntax tree
			BoolExpToSTree(pSelector->TCnt,pSelector,SArry);
//			AfxMessageBox("deal with selectorlink:bool exp to tree ok");////////
			pSelector->TCnt.RemoveAll(); 
			//end
			
			pSelection->m_pinfo=NULL;
			pSelector->m_sType=_T("SELECTOR");

		}
		else if(pSelector->m_sType=="SDGDEFAULT")
		{
			pSelector->m_sType=_T("_ELSE");
		}
	}
}

CSDGBase *CUnit::CopyNode(CSDGBase *pNode,CSDGBase *pFather,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
	    Copy the SDG Node to USDG(without copy the syntax tree,
		move it to the unit of USDG directly)
			    
		PARAMETER:
		pNode : pointer of SDG Node to copy		
		
	******************************************************/

	if(!pNode)
	{
		return NULL;
	}

	CSDGBase *pNew=NULL;
   
	if(pNode->m_sType=="SDGASSIGNMENT")
	{
		// copy Assignment node
		
		pNew=new CAssignmentUnit;
		CAssignmentUnit *pAss=(CAssignmentUnit *)pNew;
		CSDGAssignment *pas=(CSDGAssignment *)pNode;

		//**pAss->TCnt.Copy(pNode->TCnt);

		pAss->m_pleft=pas->m_pleft;  
		pAss->m_pinfo=pNode->m_pinfo;
        pas->m_pleft=NULL;
		pNode->m_pinfo=NULL; 
//		AfxMessageBox("copy assignment");/////////////////

	}
	else if(pNode->m_sType=="SDGDOWHILE" || pNode->m_sType=="SDGWHILE" ||
			pNode->m_sType=="SDGFOR"  )
	{
		// copy Iteration node
		pNew=new CIterationUnit;
		CIterationUnit *pItr=(CIterationUnit *)pNew;
		pItr->m_sAsi=pNode->m_sType;
		pItr->m_pinfo=pNode->m_pinfo;
		pNew=pItr;
//		AfxMessageBox("copy iteration");/////////////////

	}
	else if(pNode->m_sType=="SDGIF" || pNode->m_sType=="SDGSWITCH")
	{
		// copy Selection node
		pNew=new CSelectionUnit;
		CSelectionUnit *pSel=(CSelectionUnit *)pNew;
		CSDGSwitch *psw=(CSDGSwitch *)pNode;		
		if(pNode->m_sType=="SDGSWITCH" && psw->HVDefault && pNode->GetRelateNum()>0&&pNode->GetNode(pNode->GetRelateNum()-1))
		{
			pNode->GetNode(pNode->GetRelateNum()-1)->m_sType=_T("SDGDEFAULT");  
		}
		pSel->m_sAsi=pNode->m_sType;
//				AfxMessageBox("copy if switch");/////////////////


	}
	else if(pNode->m_sType=="IFLINK" || pNode->m_sType=="ELSELINK"||
		    pNode->m_sType=="SELECTORLINK" || pNode->m_sType=="SDGDEFAULT")
	{
		// copy selector node
//		AfxMessageBox(" before copy if link");/////////////////
    	pNew=new CSDGBase;
		pNew->SetNodeVL(pNode->GetNodeVL()); 
		pNew->m_sType=pNode->m_sType;
		if(pFather && (pFather->m_sType=="SDGSWITCH" || pFather->m_sType=="SDGIF"))
		{
//	    	AfxMessageBox(" pFather->m_sType== switch or if");////
			SetSelector(pNew,pFather,SArry);
		}
//				AfxMessageBox("copy if link");/////////////////

	}
	else if(pNode->m_sType=="SDGCALL")
	{
		pNew=new CSDGBase;
		pNew->m_sType=pNode->m_sType;
		pNew->m_pinfo=pNode->m_pinfo;
		///////////////////////////////////////////////
	/*	if(pNew->m_pinfo)/////////////////////////5.13
		{
		AfxMessageBox("copy SDGCall:"+pNew->m_pinfo->T.name);
		

		}*/
		/////////////////////////////////////////////////////////////
	}
	else if(pNode->m_sType=="SDGRETURN")
	{
		pNew=new CSDGBase;
		pNew->m_sType=pNode->m_sType;
		pNew->m_pinfo=pNode->m_pinfo;
		CSDGReturn *ptemp=(CSDGReturn *)pNode;
		TNODE T;
		T.addr=ptemp->m_iValue;
		
		pNew->TCnt.Add(T);  
	}
	else
	{
		// copy other node;
		pNew=new CSDGBase;
		pNew->m_sType=pNode->m_sType;
		pNew->TCnt.Copy(pNode->TCnt);
//		AfxMessageBox("copy "+pNode->m_sType);/////////////////

		
	}
    
	if(pNew)
	{
		pNew->important=pNode->important;  
	}

	return pNew;	

}

CSDGBase *CUnit::CopyNodeEx(CSDGBase *pNode,CSDGBase *pFather)
{

	/******************************************************

	    FUNCTION:
		Copy the Unit pNode completely,and return it;

		PARAMETER:
		pNode : pointer of Unit Node to copy;		
		
	******************************************************/
	
	#ifdef DEBUG
    static int num;
	num++;
	CString s;
	#endif

	if(!pNode)
	{
		return NULL;
	}

	CSDGBase *pNew=NULL;
	extern ENODE *CopyExpTreeEx(ENODE *pnew,ENODE *pint);
	
	if(pNode->m_sType=="ASSIGNMENT")
	{
		// copy Assignment node
		
		pNew=new CAssignmentUnit;
		CAssignmentUnit *pAss=(CAssignmentUnit *)pNew;
		CAssignmentUnit *pas=(CAssignmentUnit*)pNode;
        //**pAss->TCnt.Copy(pNode->TCnt);
		
		pAss->m_pinfo=CopyExpTreeEx(pAss->m_pinfo,pNode->m_pinfo);
	//	AfxMessageBox(C_ALL_KEYWORD[pNode->m_pinfo->T.key]);////
        pAss->b_formal=pas->b_formal;  
		#ifdef DEBUG
//		extern CString PreOrderPrint(ENODE *pHead,CFile &F);
//		s.Format("data\\tree%d.dat",num); 
//		CFile f(s,CFile::modeWrite|CFile::modeCreate);
//	    PreOrderPrint(pas->m_pleft,f);
//		f.Close(); 
		#endif
        
		pAss->m_pleft=CopyExpTreeEx(pAss->m_pleft,pas->m_pleft); 
	}
	else if(pNode->m_sType=="ITERATION")
	{
		// copy Iteration node
		pNew=new CIterationUnit;
		pNew->b_formal=pNode->b_formal;  
		CIterationUnit *pItr=(CIterationUnit *)pNew;
		CIterationUnit *pItrTemp=(CIterationUnit *)pNode;
		pItr->m_sAsi=pItrTemp->m_sAsi; 
		pItr->m_sType=pNode->m_sType;		
		pItr->m_pinfo=CopyExpTreeEx(pItr->m_pinfo,pNode->m_pinfo);

	}
	else if(pNode->m_sType=="SELECTION")
	{
		// copy Selection node
		pNew=new CSelectionUnit;
		pNew->b_formal=pNode->b_formal;  
		CSelectionUnit *pSel=(CSelectionUnit *)pNew;
		CSelectionUnit *pSelTemp=(CSelectionUnit *)pNode;
		pSel->m_sAsi=pSelTemp->m_sAsi;
		pSel->m_sType=pNode->m_sType;
	}
	else if(pNode->m_sType=="_ELSE")
	{
		pNew=new CSDGBase;
		pNew->b_formal=pNode->b_formal;  
		pNew->m_sType=_T("_ELSE");

	}
	else if(pNode->m_sType=="SELECTOR")
	{
		// copy selector node
		pNew=new CSDGBase;
		pNew->b_formal=pNode->b_formal;  
		pNew->m_sType=pNode->m_sType;
		pNew->m_pinfo=CopyExpTreeEx(pNew->m_pinfo,pNode->m_pinfo);
	}
	else if(pNode->m_sType=="SDGCALL")
	{
		pNew=new CSDGBase;
		pNew->b_formal=pNode->b_formal;  
		pNew->m_sType=pNode->m_sType;
		pNew->m_pinfo=CopyExpTreeEx(pNew->m_pinfo,pNode->m_pinfo);
	}
	else if(pNode->m_sType=="SDGRETURN")
	{
		pNew=new CSDGBase;
		pNew->b_formal=pNode->b_formal;  
		pNew->m_sType=pNode->m_sType;
		pNew->m_pinfo=CopyExpTreeEx(pNew->m_pinfo,pNode->m_pinfo);
	}
	//////////////wtt////////////3.24////////////////
	else if(pNode->m_sType=="#DECLARE")
	{
		pNew=new CSDGDeclare;
		pNew->b_formal=pNode->b_formal;  
		CSDGDeclare*p1=(CSDGDeclare*)pNew;
		CSDGDeclare*p2=(CSDGDeclare*)pNode;
		p1->m_sType=pNode->m_sType;
		p1->no=p2->no;
		pNew->TCnt.Copy(pNode->TCnt); 
	}
	/////////wtt///////////////////////////////////////
	else
	{	// copy other node;
		pNew=new CSDGBase;
		pNew->b_formal=pNode->b_formal;  
		pNew->m_sType=pNode->m_sType;
		pNew->TCnt.Copy(pNode->TCnt); 
	}


	if(pNew)
	{
		pNew->important=pNode->important;  
	}
 	return pNew;
}


void CUnit::SetElseSelector(CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG and add the else sub-
		program to the selection unit
			    
		PARAMETER:
		SArry :...		
		
	******************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;

	p=m_pSDGHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION" && p->GetRelateNum()>=1 &&p->GetNode(p->GetRelateNum()-1)&& 
				   p->GetNode(p->GetRelateNum()-1)->m_sType!="_ELSE")
				{
					CSDGBase *pElse=new CSDGBase;
					pElse->important=p->important;  
					pElse->m_sType=_T("_ELSE"); 
					pElse->SetFather(p); 
					p->Add(CN_CONTROL,pElse);
					p->visit=p->GetRelateNum();
				}
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
	
}

void CUnit::SetItrBranch(CArray<SNODE,SNODE> &SArry)
{
	
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG and add the escape and
		iteration sub-program to the iteration unit "pItr"
			    
		PARAMETER:
		SArry: ...
		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	
	p=m_pSDGHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="ITERATION")
				{
					AddItrSub(p);
					AddEspSub(p);
					p->visit=p->GetRelateNum();
				}
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;

}

void CUnit::AddItrSub(CSDGBase *pItr)
{
	/******************************************************

	    FUNCTION:
		add the iteration sub-program to the iteration unit
		"pItr"
			    
		PARAMETER:
		pItr : pointer of iteration unit		
		
	******************************************************/

	if(!pItr || pItr->GetRelateNum()<1)
	{
		return;
	}
	
	CSDGBase *pNew=new CSDGBase;
	pNew->important=pItr->important;  
	pNew->m_sType=_T("ITRSUB"); 

	for(int i=0;i<pItr->GetRelateNum();i++ )
	{
		pNew->Add(CN_CONTROL,pItr->GetNode(i));
		pItr->GetNode(i)->SetFather(pNew);  
	}

	pNew->SetFather(pItr);

	pItr->RemoveAll();
	pItr->Add(CN_CONTROL,pNew);	
}
	
void CUnit::AddEspSub(CSDGBase *pItr)
{
	/******************************************************

	    FUNCTION:
		add the escape sub-program to the iteration unit "pItr"
			    
		PARAMETER:
		pItr : pointer of iteration unit		
		
	******************************************************/


	if(!pItr || pItr->GetRelateNum()<1)
	{
		return;
	}

	CIterationUnit *pItrUt=(CIterationUnit *)pItr;

	CSDGBase *pNew=NULL;

	if(pItrUt->m_sAsi=="SDGDOWHILE")
	{  
		pNew=CopyBranch(pItr->GetNode(0));
		pNew->SetFather(pItr);
		pItr->Add(CN_CONTROL,pNew); 
		pNew->m_sType=_T("ESPSUB");

	}
	else
	{
		pNew=new CSDGBase;
		pNew->important=pItr->important;  
		pNew->m_sType=_T("ESPSUB");
		pNew->SetFather(pItr);
		pItr->Add(CN_CONTROL,pNew); 
	}

}

CSDGBase *CUnit::CopyBranch(CSDGBase *pInt)
{
	/******************************************************

	    FUNCTION:
		pre-order traverse the SDG or USDG branch and copy it
			    
		PARAMETER:
		pInt : head pointer of SDG or USDG branch to copy 		
		(including pInt)

	******************************************************/

 	CList<CSDGBase *,CSDGBase *> S,ST;
    CSDGBase *p=NULL,*pUnit=NULL;
	CSDGBase *pHead=NULL;
	
	
	if(!pInt)
	{
		return (new CSDGBase);
	}

	// Copy SDG to Unit representation;

   
	pUnit=CopyNodeEx(pInt,NULL);

	pHead=pUnit;
	p=pInt;

	while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p);

			ST.AddTail(pUnit);
			

	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);

				// copy  node or create new node to Unit Representation,and..
				pUnit=CopyNodeEx(p,NULL);
				// Add new Unit to USDG
				ST.GetTail()->Add(CN_CONTROL,pUnit);
				pUnit->SetFather(ST.GetTail());
				// End
			}
			else
			{
				p=NULL; 
			}	

		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);

				// copy  node or create new node to Unit Representation
				pUnit=CopyNodeEx(p,NULL);			
				ST.GetTail()->Add(CN_CONTROL,pUnit);
				pUnit->SetFather(ST.GetTail());
			
			}
			else
			{			
				p->visit=0;
				p=NULL;

				S.RemoveTail();
				ST.RemoveTail(); 
			}
		}
    }

    // end	
	return pHead;

}

/*void CUnit::SetBoolExpToElseExp(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;
	extern ENODE *CopyExpTreeEx(ENODE *pnew,ENODE *pint);
	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION")
				{
					int i=0;
					ENODE *pTemp;
					ENODE *pInt=p->GetNode(i)->m_pinfo;
					ENODE *pNew=NULL;
					pNew=CopyExpTreeEx(pNew,pInt);
					pTemp=pNew;
					
					i++;
					while(i<p->GetRelateNum()-1)
					{
						pInt=p->GetNode(i)->m_pinfo;
					    pNew=CopyExpTreeEx(pNew,pInt);
						pInt=pTemp;

						pTemp=new ENODE;
						
						// Initial node
						pTemp->info=0;
						for(int j=0;j<10;j++)
						{
							pTemp->pinfo[i]=NULL; 
						}
						pTemp->pleft=pNew;
						pTemp->pright=pInt;
						pTemp->T.addr=-1;
						pTemp->T.paddr=-1; 
						pTemp->T.key=CN_DOR;
						pTemp->T.value=0;
						
						i++;												
					}

					p->GetNode(p->GetRelateNum()-1)->m_pinfo=pTemp; 

					CSDGBase *ptr=p->GetNode(p->GetRelateNum()-1);
					if(ptr && ptr->m_pinfo->T.key==CN_NOT)
					{
						pNew=new ENODE;
						pInt=new ENODE;
						pNew->info=pInt->info=0;
						pInt->pleft=NULL;
						pInt->pright=NULL; 
						pInt->T.key=CN_CINT;
						pNew->pleft=ptr->m_pinfo;
						pNew->pright=pInt;
						ptr->m_pinfo=pNew; 
						NotAndReverse(ptr->m_pinfo);
						pNew=ptr->m_pinfo;
						ptr->m_pinfo=ptr->m_pinfo->pleft;
						delete pNew->pright;
						delete pNew;
					}
					else
					{
						NotAndReverse(ptr->m_pinfo);
					}

					BExpStandard(ptr->m_ pinfo,SArry);	
									
				}

				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
}
*/
//////////////////wtt/////////3.5//////////////////////////////////
void CUnit::SetBoolExpToElseExp(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	extern ENODE *CopyExpTreeEx(ENODE *pnew,ENODE *pint);
	extern CString GetExpString(ENODE*pHead, CArray<SNODE,SNODE>&SArry);//////
	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION"&&p->GetNode(p->GetRelateNum()-1)&&p->GetNode(p->GetRelateNum()-1)->m_sType=="_ELSE" &&p->GetNode(p->GetRelateNum()-1)->GetRelateNum()>0 )
				{
					int i=0;
					ENODE *pTemp=NULL;
					ENODE *pInt=p->GetNode(i)->m_pinfo;
					ENODE *pNew=NULL;
					pNew=CopyExpTreeEx(pNew,pInt);
					pTemp=pNew;
					
					//i++;
					bool flag=false;
					while(i<p->GetRelateNum()-1)
					{
						
						
						if(p->GetNode(i)==NULL||p->GetNode(i)&&p->GetNode(i)->GetRelateNum()==0|| p->GetNode(i)->m_pinfo==NULL)
						{
							i++;
							continue;
						}
						pInt=p->GetNode(i)->m_pinfo;
					//	AfxMessageBox(GetExpString(pInt));/////
					    pNew=CopyExpTreeEx(pNew,pInt);
						
								
						if(flag==false)
						{
							i++;
							flag=true;
							pTemp=pNew;
							continue;

						}

                    	pInt=pTemp;
						pTemp=new ENODE;
						
						// Initial node
						pTemp->info=0;
//						pTemp->T.deref=0;
						for(int j=0;j<10;j++)
						{
							pTemp->pinfo[i]=NULL; 
						}
						pTemp->pleft=pNew;
						pTemp->pright=pInt;
						pTemp->T.addr=-1;
						pTemp->T.deref=0;//wtt 2008 3.19
						pTemp->T.paddr=-1; 
						pTemp->T.key=CN_DOR;//6.7
						pTemp->T.value=0;
						
						
				//	AfxMessageBox("&&");
						i++;												
					}
						pInt=new ENODE;
						
						// Initial node
						pInt->info=0;
//						pInt->T.deref=0;
						for(int j=0;j<10;j++)
						{
							pInt->pinfo[i]=NULL; 
						}
							pInt->pleft=NULL;
							pInt->pright=NULL;
							pInt->T.addr=-1;
							pInt->T.deref=0;//wtt 2008 3.19
							pInt->T.paddr=-1; 
							pInt->T.key=CN_NOT;
							pInt->T.value=0;
							pInt->pleft=pTemp; 
						//	AfxMessageBox("add else exp"+GetExpString(pInt));///

						

                     
						 // AfxMessageBox(p->GetNode(p->GetRelateNum()-1)->m_sType );//
						BExpStandard(pInt,SArry);
						  p->GetNode(p->GetRelateNum()-1)->m_pinfo=pInt; 
					  

					
									
				}

				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
}
//////////////////////wtt///////////3.5/////////////////////////////////
/*void CUnit::GetNotOfBoolExp(ENODE *ptr)
{
	switch(ptr->T.key)
	{
	case CN_GREATT:       // >
		ptr->T.key=CN_LANDE;  
		break;
	case CN_LESS:         // <
		ptr->T.key=CN_BANDE; 
		break;
	case CN_LANDE:        // <= 
		ptr->T.key=CN_GREATT;
		break;
	case CN_BANDE:        // >=
		ptr->T.key=CN_LESS;
		break;
	case CN_NOTEQUAL:     // !=
		ptr->T.key=CN_DEQUAL;
		break;
	case CN_DEQUAL:       // == 
		ptr->T.key=CN_NOTEQUAL;
		break;
	case CN_DOR:       // || 
		ptr->T.key=CN_DAND;
		break;
	case CN_DAND:       // && 
		ptr->T.key=CN_DOR;
		break;
	}
		
}

void CUnit::NotAndReverse(ENODE *ptr)
{
	if(!ptr)
	{
		return;
	}
    
    GetNotOfBoolExp(ptr);
    
	if(ptr->pleft && ptr->pleft->T.key==CN_NOT)
	{
		ENODE *pTemp=ptr->pleft;
		ptr->pleft=ptr->pleft->pleft;
		delete pTemp;
	}
	else if(ptr->pleft && (ptr->pleft->T.key==CN_DAND || ptr->pleft->T.key==CN_DOR))
	{
		NotAndReverse(ptr->pleft);
	}
	else
	{
		if(ptr->pleft && (ptr->T.key==CN_DAND || ptr->T.key==CN_DOR) && 
		   (ptr->pleft->T.key<58 || ptr->pleft->T.key>63))
		{
			ENODE *pTemp=new ENODE;
			InitialENODE(pTemp);
			pTemp->T.line=ptr->T.line;    
			pTemp->pright=new ENODE;
			InitialENODE(pTemp->pright);
			pTemp->pright->T.line=ptr->T.line; 
			pTemp->info=0;
			pTemp->T.key=CN_DEQUAL;
			pTemp->pright->info=0;
			pTemp->pright->T.key=CN_CINT;
			pTemp->pright->T.addr=0;
			pTemp->pleft=ptr->pleft;
			ptr->pleft=pTemp;	
		}
	}
	
     
	if(ptr->pright && ptr->pright->T.key==CN_NOT)
	{
		ENODE *pTemp=ptr->pright;
		ptr->pright=ptr->pright->pleft;
		delete pTemp;
	}
	else if(ptr->pright && (ptr->pright->T.key==CN_DAND || ptr->pright->T.key==CN_DOR))
	{
		NotAndReverse(ptr->pright);
	}
    else
    {
		if(ptr->pright && (ptr->T.key==CN_DAND || ptr->T.key==CN_DOR) &&
		   (ptr->pright->T.key<58 || ptr->pright->T.key>63 ))
		{
			ENODE *pTemp=new ENODE;
			InitialENODE(pTemp);
			pTemp->T.line=ptr->T.line;
			pTemp->pright=new ENODE;
			InitialENODE(pTemp->pright);
			pTemp->pright->T.line=ptr->T.line;
			pTemp->info=0;
			pTemp->T.key=CN_DEQUAL;
			pTemp->pright->info=0;
			pTemp->pright->T.key=CN_CINT;
			pTemp->pright->T.addr=0;
			pTemp->pleft=ptr->pright;
			ptr->pright=pTemp;			
		}
	}

}*/

/**************################################****************/

void CUnit::CaseBreak(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG and find the Selection
		Unit transformed by switch, and remove the BREAK
		Unit in it.
		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	if(pHead==NULL)
		return;
	p=pHead;

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION")
				{
					CSelectionUnit *pSel=(CSelectionUnit *)p;
					if(pSel->m_sAsi=="SDGSWITCH")
					{
						RemoveCaseBreak(p);
					}
					p->visit=p->GetRelateNum();
				}
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;

}


void CUnit::RemoveCaseBreak(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		remove the BREAK Unit in Selection whitch transformed 
		by switch.
		
	******************************************************/
	if(pHead==NULL)
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		
		CSDGBase *p=pHead->GetNode(i); 
		int len=0;
		if(p)
		{
			len=p->GetRelateNum();
		}
	
		if(len>0 &&p->GetNode(len-1)&& p->GetNode(len-1)->m_sType=="SDGBREAK")
		{
			if(p->GetNode(len-1) )
			delete p->GetNode(len-1);
			p->Del(len-1 );
		}
		else if(len==0&&i<pHead->GetRelateNum()-1)
		{/////////////wtt/////////////5.15///////////case 9: case 8: s1 break 的情况转换成case i==9||i==8: s1//
			///wtt///6.2//修改了此部分//
            CSDGBase *prail=pHead->GetNode(i+1);
			if(p&&p->GetRelateNum()==0)
				{
				//	AfxMessageBox("case 9: case 8:");////////
					

				//		CSDGBase*psbl=NULL;
					//	psbl=pHead->GetNode(ix+1);
						if(prail)
						{
							ENODE*pen=NULL;
							pen=new ENODE;
							if(pen)
							{
								pen->info=0;
								for(int k=0;k<10;k++)
								{
									pen->pinfo[k]=NULL;
								
								}
//								pen->T.deref=0;
								pen->pleft=NULL;
								pen->pright=NULL;
								pen->T.addr=0;
								pen->T.deref=0;//wtt 2008 3.19
								pen->T.key =CN_DOR;
								pen->T.line=0;
								pen->T.name ="";
								pen->T.paddr=0;
								pen->T.value =0; 
								pen->pleft=p->m_pinfo;
								p->m_pinfo=NULL;
								pen->pright=prail->m_pinfo;
								prail->m_pinfo=pen;	
								pen=NULL;							

						}
					}

				}

		}
		else if( (i<pHead->GetRelateNum()-1&& len>0 && p->GetNode(len-1)&&p->GetNode(len-1)->m_sType!="SDGBREAK"))
		{
		//	AfxMessageBox("combine case");/////////////
			bool flag=true;
		
			int ix=i+1;
			CSDGBase*prail=NULL;

			do
			{
			//	if(ix>pHead->GetRelateNum()-1 )
				//	break;
			

				prail=pHead->GetNode(ix) ;
				
				

				///////////wtt////////case 9: s1 ;case 8: s2 break 的情况转换成selector exp= 8 s1; s2; selector exp=9 s2;///////////////

				for(int j=0;prail && j<prail->GetRelateNum() ;j++)
				{
					if(prail->GetNode(j)&&prail->GetNode(j)->m_sType!="SDGBREAK" )
					{
					//	AfxMessageBox("add nodes");//////////////
						CSDGBase *pNew=CopyBranch(prail->GetNode(j));
						if(pNew)
						{

							p->Add(CN_CONTROL,pNew); 
							pNew->SetFather(p); 
						}
					}
					else if(prail->GetNode(j)&&prail->GetNode(j)->m_sType=="SDGBREAK")
					{	flag=false;
					    break;
					}

				}

				ix++;				

			}while(ix<pHead->GetRelateNum() &&flag);
			
		}
		
	}

}

/**************################################****************/

void CUnit::SetDataFlow()
{

}

void CUnit::ReferencedDataFlow()
{

}

void CUnit::DefineDataFlow()
{
	
}

void CUnit::InitialENODE(ENODE *pENODE)
{
	pENODE->pleft=NULL;
	pENODE->pright=NULL;
	pENODE->info=0; 
	for(int i=0;i<10;i++)
	{
		pENODE->pinfo[i]=NULL;
	}
	pENODE->T.addr=-1;
	pENODE->T.paddr=-1;
	pENODE->T.key=0;
	pENODE->T.deref=0;//wtt 2008 3.19
	pENODE->T.line=0;
	pENODE->T.value=0;
	pENODE->T.name="";

}

void CUnit::PreOrderTraverse(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		pre-order traverse the SDG or USDG 
			    
		PARAMETER:
		pHead : head pointer of SDG or USDG		
		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;
	CFile f("data\\PreVisit.dat",CFile::modeWrite|CFile::modeCreate );
	CString s,st;


	// Copy SDG to Unit representation;
    p=pHead;
	while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p);

			// visit node
			
			s.Format("%2d:%15s relatenum=%3d",p->idno,p->m_sType,p->GetRelateNum());
			if(p->GetFather())
			{
				st.Format(" father=%2d ",p->GetFather()->idno);
			}
			s+=st;
				f.Write(s,s.GetLength());
			f.Write("\r\n",1);

			// end

	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}	

		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{			
				p->visit=0;
				p=NULL;

				S.RemoveTail();
			}
		}
    }
    f.Close(); 
	// end
}

CSDGBase *CopyBranchEx(CSDGBase *pint)
{
	CUnit unit;
	return unit.CopyBranch(pint); 
}
