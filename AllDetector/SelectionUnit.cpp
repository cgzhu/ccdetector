// SelectionUnit.cpp: implementation of the CSelectionUnit class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SelectionUnit.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSelectionUnit::CSelectionUnit()
{
	m_sType="SELECTION";
	m_sAsi=_T("");
	visit=0;

}

CSelectionUnit::~CSelectionUnit()
{

}
