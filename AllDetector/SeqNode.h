// SeqNode.h: interface for the SeqNode class.
//作者：王倩
//日期：2008-11-13
//功能：将原结构体Seq_NODE改为类
//      并添加记录每个序列对应的token串的功能
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SEQNODE_H__B0DF94F7_3CF1_4B56_873D_884FD66B548C__INCLUDED_)
#define AFX_SEQNODE_H__B0DF94F7_3CF1_4B56_873D_884FD66B548C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "dataStruct.h"



class Seq_NODE  
{
public:
	Seq_NODE();
	virtual ~Seq_NODE();
	void AddFuncRang(const FUNC_RANG &v_funcRang);

public:
	int B_index;
	int beg;			//对应源程序的起始行数
	int end;			//对应源程序的终止行数
	CString cfilepath;	//对应源程序的路径
	int qsize;			//包含子序列的个数
	CString Sequence;	//一行数字序列
	CString Line_Pos;   //源程序每一行的位置是一个整数，将每个整数存于一个4位字符串中，然后穿起来
	CString relaLPOS;//wq-20081222-相对行数
//	CString str_tokenIndex; //wq-20080924//每行程序在TArry中对应的起止位置，格式为(beg1,end1)(beg2,end2)....
	CString str_srcLine;//wq-序列对应的源程序
	FUNC_RANG funcRang; //wq-20081115-记录序列的上下文信息(函数起止相对行数)
	int funcBgnRealLine;//wq-20090202-函数起始源程序绝对行数
	int funcEndRealLine;//wq-20090202-函数终止源程序绝对行数
	CString tokenString;/*wq-20090714-记录序列对应的token串中key值和源码单词内容
						**序列形式：(key$word,...,key$word),...,(key$word,...key$word)**
						**其中每个(key$word,...,key$word)是一个序列项，代表一行源码**/
	
};

#endif // !defined(AFX_SEQNODE_H__B0DF94F7_3CF1_4B56_873D_884FD66B548C__INCLUDED_)
