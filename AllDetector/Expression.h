// Expression.h: interface for the CExpression class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXPRESSION_H__979EF0EE_F483_46AB_B640_E4D28D6D8D3F__INCLUDED_)
#define AFX_EXPRESSION_H__979EF0EE_F483_46AB_B640_E4D28D6D8D3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CExpression  
{
	friend  ENODE *ExCopyENODE(ENODE *pnew,ENODE *pint);

	friend ENODE *CopyTreeEx(ENODE *pnew,ENODE *pint);
	friend void BoolExpToSTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry);
	friend void BExpStandard(ENODE *pNode,CArray<SNODE,SNODE>&SArry);
	friend void AExpStandard(ENODE *pNode,CArray<SNODE,SNODE>&SArry);
public:
	CExpression();
	virtual ~CExpression();
public:
	void operator()(CSDGBase *pSDGHead,CArray<SNODE,SNODE> &SArry,CArray<CERROR,CERROR> &EArry);
	CArray<CERROR,CERROR> *m_pError;
	CERROR ER;
	ENODE *CopyTree(ENODE *pnew,ENODE *pint);

protected:
    void CopyTNODE(TNODE *pNew,TNODE *pInt);
	void CopyTNODE(TNODE &TNew,TNODE &TInt);
	ENODE *CopyENODE(ENODE *pnew,ENODE *pint);

protected:
	void ExpressionToTree(CArray<TNODE,TNODE> &TEXP,ENODE *phead,CArray<SNODE,SNODE> &SArry);
	void InitENode(ENODE *pENode);
	

	// function parameter analyze
	void CallParameter(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry);

	// bool expression to syntax tree
	void BExpToTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry);
	void ReplaceAExp(int &icount,CArray<TNODE,TNODE> &TEXP,CArray<TNODE,TNODE> TList[20]);
//	void ReplaceOneAEXP(int beg,int end,CArray<TNODE,TNODE> &TEXP);
//	void FindLeftExp(int &ix,CList<int,int> &Bound,CArray<TNODE,TNODE> &TEMP,CArray<TNODE,TNODE> &TEXP);
//	void FindRightExp(int &ix,CArray<TNODE,TNODE> &TEMP,CArray<TNODE,TNODE> &TEXP);
	int  BOperator(TNODE T);
	
	//bool IsAExpression(int beg,int end,CArray<TNODE,TNODE> &TEXP);
    
	void BoolExpAnalyze(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry,CArray<ENODE*,ENODE*> &EList);
    void BSTopInduce(int &ix,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry);
	void BPushToStack(int &ix,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry,CArray<ENODE*,ENODE*> &EList); 
	int  BGetPrior(const int n1,const int n2);


	// arithematic expression syntax tree(sub tree of bool..)
    void AExpToTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry);
	int  GetAExpInfo(CArray<TNODE,TNODE> &TEXP);
	void STopInduce(int &ix,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry);
	void PushToStack(int &ix,CArray<TNODE,TNODE> &TEXP,CArray<SSTACK,SSTACK> &S,CArray<SNODE,SNODE> &SArry);
	void ArrayToTree(int &index,SSTACK &st,CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry);
	void FunctionToTree(int &index,SSTACK &st,CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry);
	int  AOperator(TNODE T);
	int  GetPrior(const int n1,const int n2);
   // void CountDeref(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry);//wtt   2008.3.18  预处理指针脱引用
CString TraverseAST(ENODE*pHead,CArray<SNODE,SNODE> &SArry);//测试用


	// standard of expression:
	ENODE* BoolExpStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
//	void   BExpStandardRule01(ENODE *pHead);
//	void   BExpStandardRule02(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
	void   BExpStandardRule(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
     int InSet(int i,int j);//wtt//5.13//

	ENODE* ArithExpStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
	void  AExpStandardRule(ENODE*pHead,CArray<SNODE,SNODE>&SArry);
//	void   ReMoveParenth(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
//	void   AdvancedAEStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
	void   AEOrderStandard(ENODE *pHead,CArray<SNODE,SNODE> &SArry);
	  
//	ENODE* LeftChildStandard (ENODE *pfather,ENODE *pnode);
//	ENODE* RightChildStandard(ENODE *pfather,ENODE *pnode);
//	ENODE* MoveBranch(ENODE *pfather,ENODE *pnode);
//	ENODE* MoveRightBranch(ENODE *pfather,ENODE *pnode);
//	ENODE* SelectFather(ENODE *pnode,ENODE * ptemp1,ENODE *ptemp2);
//	ENODE* FindFather(ENODE *pson,ENODE *pfather,ENODE *pHead);
//	ENODE* FindFirstAddSub(ENODE *pnode,ENODE *ptempf);
	int    AOpClassify(ENODE *p);


	/***************************array pointer************************/
	void ArrayPointerInExp(ENODE *pEHead,CArray<SNODE,SNODE> &SArry);
	bool LegalAddrOperation(ENODE *p,ENODE *pHead);
	void SetAPIndex(ENODE *pEHead,CArray<SNODE,SNODE> &SArry);
	void DeleteExpTree(CSDGBase *ptr);
	bool PointerRefer(CArray<SSTACK,SSTACK> &S);
	void ErrorInExp(int line,CArray<SSTACK,SSTACK> &S);
	/*************************end of this part***********************/


		
protected:
	void SearchTree(ENODE *pHead);
private:
 bool b_modified;//算术表达式是否在标准化过程中改变
 bool b_boolmodified;//布尔表达式是否在标准化过程中改变
 
};

void BoolExpToSTree(CArray<TNODE,TNODE> &TEXP,CSDGBase *ptr,CArray<SNODE,SNODE> &SArry);
ENODE *CopyExpTreeEx(ENODE *pnew,ENODE *pint);
void BExpStandard(ENODE *pNode,CArray<SNODE,SNODE>&SArry);
bool CompareTrees(ENODE*pHead1,ENODE*pHead2,CArray<SNODE,SNODE> &SArry);
ENODE *ExCopyENODE(ENODE *pnew,ENODE *pint);



#endif // !defined(AFX_EXPRESSION_H__979EF0EE_F483_46AB_B640_E4D28D6D8D3F__INCLUDED_)
