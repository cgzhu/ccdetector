// VarRemove.cpp: implementation of the CVarRemove class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "VarRemove.h"
#define   DELLAB -9999


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Expression.h"
#include "IterationUnit.h"
#include "SDGDeclare.h"
#include "SDGBase.h"
#include "SDGCall.h"
#include "RemoveInvocation.h"
//////////////////////////////////// //////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVarRemove::CVarRemove()
{

}

CVarRemove::~CVarRemove()
{

}

void CVarRemove::operator()(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)
{//wtt将此整个函数改写了
	// variations removing
	if(SArry.GetSize()<=0||FArry.GetSize()<=0)
		return ;
	m_pUSDGHead=pUSDGHead;

	if(!m_pUSDGHead)
	{
		return;
	}
	

	do//////控制结构标准化
	{
		///////////////////////选择分支结构的标准化///////////////////////////
		modified=false;
		ChangeElseToSelector(pUSDGHead);
		RemoveEmptyBranch(pUSDGHead);
		NestedSelection(pUSDGHead,SArry); 
		TrueInSelection(pUSDGHead);////////////wtt//3.9///  

		SubstractCommenBranch(pUSDGHead,SArry);////wtt///3.10///
		RemoveEmptyBranch(pUSDGHead);////wtt///3.10///

		CombineSelection(pUSDGHead,SArry);////wtt///3.10///
		MergeSelection(pUSDGHead,SArry);////wtt//3.11//
		
   ////////////////////////////////////////////////////////

   /////////////////消除控制转移语句///////////////////////////
   
    	 RemoveContinue(pUSDGHead,SArry);////wtt//3.10//
	//	RemoveEmptyBranch(pUSDGHead); 

		RemoveBreak(pUSDGHead,pUSDGHead,SArry);
	//	RemoveEmptyBranch(pUSDGHead);

		RemoveReturn(pUSDGHead,SArry);///wtt//3.11//
   /////////////////////////////////////////////////////////////
   
   ///////////////循环结构的标准化/////////////////////////////
	 	ChangeIteration(pUSDGHead);////wtt///3.10///
		FalseIteration(pUSDGHead);//////wtt////3.10////
		RemoveInvariableInIteration(pUSDGHead,pUSDGHead,SArry);//wtt//3.11//		RemoveInvariableInIteration(pUSDGHead,pUSDGHead,SArry);//wtt//3.11//
////////////wtt///05.10.20/////
        StandLoopCondition(pUSDGHead);
  ////////////////////////////////////////////
	}while(modified==true);
  

	///////////////////拆分输入输出语句//////////////////////////////// 	
  
   ScanfPrintfDivide(pUSDGHead,SArry);

/////////////////////////////////////////////////////
  // RemoveInvocation removeinvocation;/////////wtt////3.17
 //  removeinvocation(pUSDGHead,SArry,FArry);/////////wtt////3.17

  ////////问题：运行时比较耗时/////wtt/3.14//////函数内联的预处理部分:将函数调用语句、return中的表达式、实参中的表达式单独提出用临时变量替换 
/*  do
   {
	modified=false;
	RemoveExpInReturn(pUSDGHead,SArry,FArry);
   	RemoveParaExp(pUSDGHead,SArry,FArry);
	RemoveCallInExp(pUSDGHead,SArry,FArry);
   }while(modified==true);*/

   //////////////////////////////////////////////////////

   		
 
///////////////////删除没有被引用的变量定义语句和赋值语句/////拿到消除函数调用后////////////////////////////////////////////
  // 
	  // modified=false;
//	DeleteUnrefered(pUSDGHead,SArry);////3.17

/*	DeleteIdentity(pUSDGHead,SArry);/////////wtt////3.12////
 	RemoveSimpleStatement(pUSDGHead,pUSDGHead,SArry);////////////wtt///3.12//

	RemoveNotReferedAssignment(pUSDGHead,pUSDGHead,SArry);///wtt////3.9////
	SetInOutDataFlow(pUSDGHead,SArry);
	RemoveNotReferedVar(pUSDGHead); 
	RemoveEmptyBranch(pUSDGHead);*/
  
	///////////////////////////////////////////


	///////////变量重命名和重排语句的顺序///////////////////////////
//	RenameAndReorder(pUSDGHead,SArry);//wtt//3.17////
	/*VariablesRename(pUSDGHead,SArry);

	SetSDGNodeName(pUSDGHead);
	SetInOutDataFlow(pUSDGHead,SArry);
	ResetStatementOrder(pUSDGHead,SArry);*/
/////////////////wtt/////////////////////////////////////
       
}

void CVarRemove::ChangeElseToSelector(CSDGBase *pHead)
{//wtt编写：_ELSE节点的类型改为SELECTOR，使其与SELRECTOR具有同等的地位
	if(pHead==NULL)
		return;

	if(pHead->m_sType=="_ELSE")
	{	
		pHead->m_sType="SELECTOR";		
		modified=true;
	}
    int	i=0;
	while(i<pHead->GetRelateNum())
	{
	ChangeElseToSelector(pHead->GetNode(i));
	i++;
	}	


}
void CVarRemove::InitialENODE(ENODE *pENODE)
{
	pENODE->pleft=NULL;
	pENODE->pright=NULL;
	pENODE->info=0; 
	for(int i=0;i<10;i++)
	{
		pENODE->pinfo[i]=NULL;
	}
	pENODE->T.addr=-1;
	pENODE->T.deref=0;//wtt 2008 3.19
	pENODE->T.paddr=-1;
	pENODE->T.key=0;
	pENODE->T.line=-1;
	pENODE->T.value=0;
	pENODE->T.name="";

}

void CVarRemove::SetInOutDataFlow(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG ,  set nodes' in data 
		flow and out data flow
		
		PARAMETER:
		pHead : USDG head pointer		
		
	******************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;

	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				//  important codes
				
				if(p->m_sType!="SDGBREAK" && p->m_sType!="SDGCONTINUE")
				{
					
					p->m_aDefine.RemoveAll();
					p->m_aRefer.RemoveAll();
					SearchChildrenDataFlow(p,SArry);
				}
				
				//   end of this area 

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
}
/////////////////////////////////////////
void ExSetInOutDataFlow(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	CVarRemove varremove;
	varremove.SetInOutDataFlow(pHead,SArry);


}
/////////////////////////////////////
void CVarRemove::SearchChildrenDataFlow(CSDGBase *pFnd,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Search the node and find the variables it referenced
		and defined.
		PARAMETER:
		pFnd  : USDG node pointer;
	******************************************************/
	if(!pFnd)
	{
		return;
	}

	pFnd->m_aDefine.RemoveAll();
	pFnd->m_aRefer.RemoveAll();

	// Add Children's Data to it;
	if(pFnd->m_sType!="ASSIGNMENT" && pFnd->m_sType!="SDGRETURN" &&pFnd->m_sType!="SDGCALL")
	{
		GetSonsDataInfo(pFnd);
	}

	if(pFnd->m_sType=="ASSIGNMENT")
	{
		// Add referenced data in right expression of assignment statement
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
		CAssignmentUnit *pUt=(CAssignmentUnit *)pFnd;
        
		// Add Defined data flow 
		if(pUt->m_pleft && pUt->m_pleft->T.key==CN_VARIABLE)
		{
			if(!FindTNODEInDArry(pUt->m_pleft->T,pFnd->m_aDefine))
			{
				pFnd->m_aDefine.Add(pUt->m_pleft->T);
			}
		}

		// Add referenced data in left expression of assignment statement
		// if the left variable kind is array
		if(pUt->m_pleft && pUt->m_pleft->T.key==CN_VARIABLE &&pUt->m_pleft->T.addr>=0&&pUt->m_pleft->T.addr<SArry.GetSize()&&
			SArry[pUt->m_pleft->T.addr].kind==CN_ARRAY)//wtt 2008.4.1
		{
			for(int i=0;i<10 &&pUt->m_pleft->pinfo[i];i++)
			{
				SearchExpReferData(pFnd,pUt->m_pleft->pinfo[i],pFnd->m_aRefer,SArry);
			}

		}

	}
	else if(pFnd->m_sType=="ITERATION")
	{
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
	}
	else if(pFnd->m_sType=="SDGRETURN")
	{
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
	}
	else if(pFnd->m_sType=="SDGCALL")
	{
		// Add referenced data in parameter expressions of call node
		
		if(pFnd->m_pinfo && (pFnd->m_pinfo->T.key==CN_DFUNCTION||
		   pFnd->m_pinfo->T.key==CN_BFUNCTION))
		{
		    
			for(int i=0;i<10 &&pFnd->m_pinfo->pinfo[i];i++)
			{
				SearchExpReferData(pFnd,pFnd->m_pinfo->pinfo[i],pFnd->m_aRefer,SArry);
			}
			if(pFnd->m_pinfo->T.key==CN_BFUNCTION&&(pFnd->m_pinfo->T.name=="scanf"||pFnd->m_pinfo->T.name=="gets"))/////////wtt//3.7///)
			{
				pFnd->m_aRefer.RemoveAll();  
			}

			SetFunctionDefinedData(pFnd,pFnd->m_pinfo,SArry);

		}

	}
	else if(pFnd->m_sType=="SELECTOR")
	{
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);

	}	
	else if(pFnd->m_sType=="ITRSUB")
	{
	
		SearchExpReferData(pFnd,pFnd->m_pinfo,pFnd->m_aRefer,SArry);
	}
	else if(pFnd->m_sType=="#DECLARE" && pFnd->TCnt.GetSize()>0)
	{
		pFnd->m_aDefine.Add(pFnd->TCnt[0]); 
	}	
}

void CVarRemove::SetFunctionDefinedData(CSDGBase *pFnd,ENODE *pHead,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Search the call expression and find the variables it 
		defined.
		PARAMETER:
		pFnd  : USDG node pointer;
		pHead : call expression tree;
	******************************************************/

	int ix=0;
	if(pHead && pHead->T.key==CN_BFUNCTION)
	{
		switch(pHead->T.addr)
		{
		case 58://wtt//get//5.16//
		case 68: //scanf
			if(pHead->T.addr==58)//wtt//
				ix=0;
			else
				ix=1;
			for(;ix<pHead->info && ix<10;ix++)
			{
				if(pHead->pinfo[ix] && pHead->pinfo[ix]->T.key==CN_VARIABLE )
				{
					if(!FindTNODEInDArry(pHead->pinfo[ix]->T,pFnd->m_aDefine))
					{
						pFnd->m_aDefine.Add(pHead->pinfo[ix]->T);
					}
						////////////////////读入的数据是数组元素,则其下标存在引用关系/wtt /3.29///
					if( pHead->pinfo[ix]->T.addr<SArry.GetSize()&&SArry[pHead->pinfo[ix]->T.addr].kind==CN_ARRAY)
					{
						for(int i=0;i<10;i++)
						{
							if(pHead->pinfo[ix]->pinfo[i]&&pHead->pinfo[ix]->pinfo[i]->T.key==CN_VARIABLE)
							{
								if(!FindTNODEInDArry(pHead->pinfo[ix]->pinfo[i]->T,pFnd->m_aRefer))
								{
									pFnd->m_aRefer.Add(pHead->pinfo[ix]->pinfo[i]->T);
								}

							}
						}
					}



					//////////////////////////////
				}
			}
			
			break;
		}
	}


}

void CVarRemove::GetSonsDataInfo(CSDGBase *pNode)
{
	/******************************************************

	    FUNCTION:
		Search the node's child node and add their data inf-
		ormation to it;		
		PARAMETER:				
	******************************************************/

	if(!pNode)
	{
		return;
	}

    CSDGBase *p=NULL;
	for(int i=0;i<pNode->GetRelateNum();i++)
	{
		p=pNode->GetNode(i); 
		for(int j=0;j<p->m_aDefine.GetSize();j++)
		{
			if(!FindTNODEInDArry(p->m_aDefine[j],pNode->m_aDefine))
			{
				pNode->m_aDefine.Add(p->m_aDefine[j]);
			}
		}

		for(int j=0;j<p->m_aRefer.GetSize();j++)
		{
			if(!FindTNODEInDArry(p->m_aRefer[j],pNode->m_aRefer))
			{
				pNode->m_aRefer.Add(p->m_aRefer[j]);
			}
		}

	}//for(int i=0

}

void CVarRemove::SearchExpReferData(CSDGBase *pFnd,ENODE *pHead,CArray<TNODE,TNODE> &DArry,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		Search the syntax tree of the nodes' expression and 
		find the variables it referenced,then put them into 
		array "DArry"
		
		PARAMETER:				
		pHead : syntax tree head pointer
		DArry : array which stores the data syntax tree ref-
		        erenced
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	ENODE *p=NULL,*ptemp=NULL;
	
	if(pHead==NULL)
	{
		return;
	}

	p=pHead;
	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
		
			// If this E-node is Variable Insert it to refered Data
			if(p->T.key==CN_VARIABLE)
			{
				if(!FindTNODEInDArry(p->T,DArry))
				{
					DArry.Add(p->T);
				}

				if(p->info>0 && p->info<10)
				{
					for(int i=0;i<10&&p->pinfo[i];i++)
					{
						SearchExpReferData(pFnd,p->pinfo[i],DArry,SArry);
					}
				}
			}
			else if(p->T.key==CN_BFUNCTION || p->T.key==CN_DFUNCTION )
			{
				SetFunctionDefinedData(pFnd,p,SArry);
			/////////////wtt///////////3.9/////////////////函数实参/////
				for(int i=0;i<10&&p->pinfo[i];i++)
				{
						SearchExpReferData(pFnd,p->pinfo[i],DArry,SArry);
				}
				//////////////wtt///////////////////////////////////////
			}
			// end of this part
						
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			p=STK.GetTail()->pright;
			ptemp=STK.GetTail(); 
			STK.RemoveTail();			
		}
	}
}


bool CVarRemove::FindTNODEInDArry(TNODE &TD,CArray<TNODE,TNODE> &DArry)
{
	/******************************************************

	    FUNCTION:
		If the Node TD is in array DArry?	in- return true;
	******************************************************/

	for(int i=0;i<DArry.GetSize();i++)
	{
		if(TD.key==CN_VARIABLE && DArry[i].key==CN_VARIABLE&&
		   TD.addr==DArry[i].addr)// && TD.name==DArry[i].name)
		{
			return true;
		}
	}
	return false;
}


int CVarRemove::DefOrRefVariable(TNODE &TD,CSDGBase *pNode)
{
	/******************************************************

	    FUNCTION:
		Is the variable TD defined or refered by branch pNode?	
		if defined return 1.
		if refered return 2.
		if both return 3;
		if neither return 0;
	******************************************************/

	int nDR=0;
	for(int i=0;i<pNode->m_aDefine.GetSize();i++)
	{
		if(TD.key==pNode->m_aDefine[i].key && TD.key==CN_VARIABLE &&
		   TD.addr==pNode->m_aDefine[i].addr)
		{
			nDR=1;
			break;
		}
	}

	for(int i=0;i<pNode->m_aRefer.GetSize();i++)
	{
		if(TD.key==pNode->m_aRefer[i].key && TD.key==CN_VARIABLE &&
		   TD.addr==pNode->m_aRefer[i].addr)
		{
			nDR=nDR==1?3:2;
			break;
		}
	}
	return nDR;
}

 int CVarRemove::DefOrRefVariableEx(TNODE &TD,CSDGBase *pNode)
{
	/******************************************************

	    FUNCTION:
		Is the variable TD defined or refered by pNode's exp?	
		if defined return 1.
		if refered return 2.
		if both return 3;
		if neither return 0;
	******************************************************/
	if(!pNode)
	{
		return 0;
	}

	int nDR=0;
	CArray<TNODE,TNODE> DArry;
	CArray<SNODE,SNODE> SArry;
    CAssignmentUnit *pas=NULL;
    
	int i;
	if(pNode->m_sType=="ASSIGNMENT")
	{
		pas=(CAssignmentUnit *)pNode;
		for(i=0;pas->m_pleft&&i<10;i++)
		{
			if(pas->m_pleft->pinfo[i])
			{
				SearchExpReferData(pNode,pas->m_pleft->pinfo[i],DArry,SArry);
			}
		}

	}

    SearchExpReferData(pNode,pNode->m_pinfo,DArry,SArry);

	
	for(i=0;i<DArry.GetSize();i++)
	{
		if(TD.key==DArry[i].key && TD.key==CN_VARIABLE &&
		   TD.addr==DArry[i].addr)
		{
			nDR=1;
			break;
		}
	}
	
	if(pNode->m_sType=="ASSIGNMENT")
	{
		pas=(CAssignmentUnit *)pNode;
		if(pas && pas->m_pleft && pas->m_pleft->T.key==TD.key &&
		   TD.key==CN_VARIABLE && pas->m_pleft->T.addr==TD.addr)
		{
			nDR=nDR==1?3:2;
		}
		
	}
	/////////////wtt////////3.8////scanf有定值的变量////////
	if(pNode->m_sType=="SDGCALL"&&pNode->m_pinfo&&(pNode->m_pinfo->T.name=="scanf" ||pNode->m_pinfo->T.name=="gets")   )
			
	{
		SetFunctionDefinedData(pNode,pNode->m_pinfo,SArry );
		for(int i=0;i<pNode->m_aDefine.GetSize();i++)
		{
			if(i<DArry.GetSize()&&TD.key==DArry[i].key && TD.key==CN_VARIABLE &&  TD.addr==DArry[i].addr)
			{
				nDR=1;
				break;
			}
		}

	}
////////////////////////////////////////wtt////////////////////////////////
	return nDR;


}

/*void CVarRemove::DefAndRefVariable(CArray<TNODE,TNODE> &DArry,CArray<TNODE,TNODE> &RArry,CSDGBase *pNode)
{
	/******************************************************

	    FUNCTION:
		
		Find the defined and refered variables in pNode'exp?	
		PARAMETERS:
		RArry : refered variables;
		DArry : defined variables;
		
	******************************************************/

/*	if(!pNode)
	{
		return;
	}

	int nDR=0;	    
	int i;

    CArray<SNODE,SNODE> SArry;
    CAssignmentUnit *pas=NULL;

	if(pNode->m_sType=="ASSIGNMENT")
	{
		pas=(CAssignmentUnit *)pNode;
		for(i=0;i<pas->m_pleft->info && pas->m_pleft->info<10;i++)
		{
			if(pas->m_pleft->pinfo[i])
			{
				SearchExpReferData(pNode,pas->m_pleft->pinfo[i],RArry,SArry);
			}
		}

	}

    SearchExpReferData(pNode,pNode->m_pinfo,RArry,SArry);
	
	if(pNode->m_sType=="ASSIGNMENT")
	{
		pas=(CAssignmentUnit *)pNode;
		DArry.Add(pas->m_pleft->T);		
	}

}*/

/************$$$$$$$$$$$$######Not Refered Unit#####$$$$$$$$$$$$**************/

/*void CVarRemove::NoReferedUnit(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		A unit is not refered by any otherunits should be 
		removed from USDG.
		PARAMETER:				
		
	******************************************************/
/*	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;

	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="#DECLARE" || p->m_sType=="SDGASSIGNMENT")
				{
					RemoveNotReferedOfDA(p);				
				}				

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;

}*/


/*void CVarRemove::RemoveNotReferedOfDA(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		Find and remove not refered "#DECLARE" or "ASSIGNMENT"
		unit.
		PARAMETER:				
		
	******************************************************/
    
/*	if(!(pHead->m_sType!="#DECLARE" && pHead->m_sType!="SDGASSIGNMENT"))
	{
		return;
	}

	CSDGBase *pFnd=pHead->GetFather(); 

	if(!pFnd)
	{
		return;
	}
}
*/
/************$$$$$$$$$$$$######Refer than one#####$$$$$$$$$$$$**************/

/*void CVarRemove::RefThanOne()
{
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG ,  search for the 
		Selection or Iteration Unit that has different 
		irrelevant out-data flows,then separate them into 
		several units
		
		PARAMETER:				
		
	******************************************************/

/*	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;

	p=m_pUSDGHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION")
				{

				}
				else if(p->m_sType=="ITERATION")
				{

				}

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;

}*/

/*void CVarRemove::MoreControl()
{
	
}

void CVarRemove::RefBySelection()
{

}

void CVarRemove::RefByIteration()
{

}


void CVarRemove::DefineBySelection()
{

}
*/
void CVarRemove::NestedSelection(CSDGBase *pHead,CArray<SNODE,SNODE>&SArry)
{
	/******************************************************
     将分支的深层次嵌套结构转换成多分支结构
	    FUNCTION:
		post-order traverse the USDG ,  search for nested 
		Selection and transform it to USDG without nested 
		Selection.
		PARAMETER:
		pHead : head pointer of USDG.
	******************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
   	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION")
				{
					NestedSelectionNode(p,SArry);
				}

				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
    
}

void CVarRemove::NestedSelectionNode(CSDGBase *pHead,CArray<SNODE,SNODE>&SArry)
{
	/******************************************************

	    FUNCTION:
		Find a node with nested selection and transform it;
		
		PARAMETER:
		pHead : head pointer of SUB-USDG.		
	******************************************************/

	if(pHead->m_sType!="SELECTION")
	{
		return;
	}	
	
	CSDGBase *pTemp=NULL,*pSub=NULL;
	int idx=0;
	int itemp=0;
	while(idx<pHead->GetRelateNum())
	{
		CSDGBase *pTemp=pHead->GetNode(idx);

		// Nested type 1:
	////////////////wtt//////////////////3.7/////////////////
		if(pTemp && pTemp->GetRelateNum()==1 &&pTemp->GetNode(0)&&
		   pTemp->GetNode(0)->m_sType=="SELECTION" )
		{
			itemp=pTemp->GetNode(0)->GetRelateNum(); 
			DirectNestedSel(idx,pHead,pTemp->GetNode(0),SArry);
			//idx+=itemp;
			idx+=itemp-1;//wtt///3.7
			
		}

		idx++;
	}

	///////////////////////wtt////////////////////////////
}

void CVarRemove::DirectNestedSel(const int idx,CSDGBase *pFat,CSDGBase *pNest,CArray<SNODE,SNODE>&SArry)
{
	/******************************************************

	    FUNCTION:
		Nested selection Type1: Only one Selection in a 
		Selection's selector;
		
		PARAMETER:
		idx  : selector index in external Selection.
		pFat : pointer of external Selection
		pSub : pointer of internal Selection
	******************************************************/	

	/////////////////////////////////////////////
	extern ENODE *CopyExpTreeEx(ENODE *,ENODE *);
	/////////////////////////////////////////////
	
   if(pFat==NULL)
	   return;
	if(idx<0||idx>=pFat->GetRelateNum())
	{
		return;
	}

	CSDGBase *pSeltor=pFat->GetNode(idx); 

	if(!(pFat && pNest && pSeltor))
	{
		return;
	}
	int ix=0;
	int index=idx;
	ENODE *pENew=NULL;
	ENODE *pETemp=NULL;	

	// Add all internal selector to external  
	while(ix<pNest->GetRelateNum()-1)
	{

		pENew=CopyExpTreeEx(pENew,pSeltor->m_pinfo);
		pETemp=new ENODE;
		InitialENODE(pETemp);
		pETemp->T.line=pSeltor->m_pinfo?pSeltor->m_pinfo->T.line:0;   
		pETemp->pleft=pENew;
		if(pNest->GetNode(ix))
		pETemp->pright=pNest->GetNode(ix)->m_pinfo;
		else
         pETemp->pright=NULL;
		pETemp->T.key=CN_DAND;
		

		pETemp->T.addr=-CN_DAND; // signal it is add bool exapression  
		
		BExpStandard(pETemp,SArry);///wtt//3.7///布尔表达式的标准化
	
		
		if(pNest->GetNode(ix))
		{ pNest->GetNode(ix)->m_pinfo=pETemp;
		  pFat->InsertNode(index,pNest->GetNode(ix));
		  pNest->GetNode(ix)->SetFather(pFat); 
		}
		index++;
		ix++;
	}


	// Add internal else to external 
	pETemp=new ENODE;
	InitialENODE(pETemp);
	pETemp->T.line=pSeltor->m_pinfo?pSeltor->m_pinfo->T.line:0;    
	pETemp->pleft=pSeltor->m_pinfo;
	
	pETemp->pright=pNest->GetNode(ix)->m_pinfo;



	if(pNest->GetNode(ix))
	pNest->GetNode(ix)->m_pinfo=pETemp;
	
	pETemp->T.key=CN_DAND;
	pETemp->T.addr=-CN_DAND; // signal it is add bool exapression  
	BExpStandard(pETemp,SArry);///wtt//3.7///布尔表达式的标准化

	index=FindIndex(pSeltor);

    if(pNest->GetNode(ix)&& pFat->GetNode(index))
	pNest->GetNode(ix)->m_sType= pFat->GetNode(index)->m_sType;  

	if(pFat->GetNode(index))
	delete pFat->GetNode(index);

    if(pNest->GetNode(ix))
	{
	pFat->InsertNode(index,pNest->GetNode(ix));
	pNest->GetNode(ix)->SetFather(pFat);
	pFat->Del(index+1);
	}
	if(pNest)
	delete pNest;
	modified=true;
	
}

/*CSDGBase * CVarRemove::IsNestedSelection(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the SUB-USDG ,  search for nested 
		Selection and return it;
		
		PARAMETER:
		pHead : head pointer of SUB-USDG.		
	******************************************************/
 /*   CString str;
    CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p,*ptr=NULL;

	p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				if(p->m_sType=="SELECTION")
				{
					ptr=p;
				}
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;

	
	return ptr;
}
*/
/*void CVarRemove::TransformNestedSelection(CSDGBase *pHead,CSDGBase *pnest)
{
	/******************************************************
	    FUNCTION:
		Transform the nested selection  to the SUB-USDG 
		without nested selection;
		
		PARAMETER:
		pHead : head pointer of SUB-USDG.
		pnest : head pointer of pHead's child branch.
	******************************************************/
/*	if(!(pHead && pnest))
	{
		return;
	}

	CSDGBase *pfather=pnest->GetFather();

	if(!pfather)
	{
		return;
	}

	// Add or copy pnest's every brother after its position to all of 
	// its selectors
    int n=FindIndex(pnest);
	if(n<0 || n>=pfather->GetRelateNum())
	{
		return;
	}

	CSDGBase *psub=NULL;
	CSDGBase *pselector;
    if(n<pfather->GetRelateNum()-1)
	{
		for(int ix=0;ix<pnest->GetRelateNum()-1;ix++)
		{
			pselector=pnest->GetNode(ix);
			for(int i=n+1;i<pfather->GetRelateNum();i++)
			{
				psub=CopyBranchEx(pfather->GetNode(i));
				if(psub)
				{
					pselector->Add(CN_CONTROL,psub);
					psub->SetFather(pselector); 
				}
			}
		}
        
		pselector=pnest->GetNode(pnest->GetRelateNum()-1);

		for(int i=n+1;i<pfather->GetRelateNum() && pselector->m_sType=="_ELSE";i++)
		{
			psub=pfather->GetNode(i);
			if(psub)
			{
				pselector->Add(CN_CONTROL,psub); 
				psub->SetFather(pselector); 
			}
		}
        
		for(i=pfather->GetRelateNum()-1;i>n;i--)
		{
			pfather->Del(i); 
		} i++;
	}
	
	// part 2:
}
*/
//int CVarRemove::FindIndex(CSDGBase *pson)
int FindIndex(CSDGBase *pson)
{
	/***********************************************
		FUNCTION:
		Find the index of pson in its father;
		PARAMETER:
		pson : ...
	***********************************************/
	//AfxMessageBox("find index");///////
	CSDGBase *p=NULL;
	if(pson)
	{
		
		p=pson->GetFather();
	}
	else
	{
	//	AfxMessageBox("pson==NULL");////
		return -1;
	}
	
//	AfxMessageBox("find index");///////
	if(p)
	{
		int i=0;
		while(i<p->GetRelateNum())
		{
			if(p->GetNode(i)->idno==pson->idno)
			{
				return i;
			}
			i++;
		}
	}
	return -1;
}

/************$$$$$$$$$$$$######Simplify USDG#####$$$$$$$$$$$$**************/

                       // rule:simplify USDG
/*void CVarRemove::FindIterationSelection(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************
		FUNCTION:
		Find Iteration or Selection unit to simplify;
		PARAMETER:
		pHead : head pointer of USDG
	***********************************************/

/*	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;
   	p=pHead;

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
			//	BoolRedundantRemove(p,SArry);///wtt//3.7//由于已经调用了布尔表达式标准化，所以此部分删除

				if(p->m_sType=="SELECTION")
				{
					//SimplifySelection(p);
				}
				else if(p->m_sType=="ITERATION")
				{
					//SimplifyIteration(p);
				}				
				
				p->visit=p->GetRelateNum();
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    //end;
}*/

/*bool CVarRemove::SelItrCanSimplify(CSDGBase *pNode)
{
	if(!pNode)
	{
		return false;
	}

	if(pNode->GetRelateNum()<=1 )
	{
		return false;
	}

	bool bt=true;
	CArray<bool,bool> BArry;


	//  is the set Define a sub-set of Refer?
	for(int i=0;i<pNode->m_aDefine.GetSize();i++) 
	{
		bt=true;
		for(int j=0;j<pNode->m_aRefer.GetSize();j++)
		{
			if(pNode->m_aDefine[i].key==pNode->m_aRefer[j].key &&
			   pNode->m_aDefine[i].addr==pNode->m_aRefer[j].addr)
			{
				bt=false;
				break;
			}
		}
		BArry.Add(bt);
	}

	if(BArry.GetSize()<=1)
	{
		return false;
	}

	bt=true;
	for(i=0;i<BArry.GetSize();i++) 
	{
		if(!BArry[i])
		{
			bt=false;
		}
	}

	return !bt;
}*/
	// rule: simplify complex iteratio
/*void CVarRemove::SimplifyIteration(CSDGBase *pItr)
{

	

	//DefOrRefVariableEx(TNODE &TD,CSDGBase *pNode);
    

}*/

/*void CVarRemove::SimplifySelection(CSDGBase *pItr)
{
	
}*/

/*********$$$$$$$$$$$$######Remove Redundant Part in Bool Expression#####$$$$$$$$$$$$**********/

/*void CVarRemove::BoolRedundantRemove(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************
		FUNCTION:
		Remove redundant part(s) in bool expression;
		PARAMETER:
		pNode : pointer of USDG node
	***********************************************/
	
/*	if(pNode==NULL)
	{
		return;
	}

	if(pNode->m_sType!="ITERATION" && pNode->m_sType!="SELECTOR" && pNode->m_sType!="_ELSE")
	{
		return;
	}
    
	CList<ENODE *,ENODE *> STK;
	CArray<ENODE *,ENODE*> NAND;
	CArray<ENODE *,ENODE*> FAND;
	ENODE *p=pNode->m_pinfo;
	
   	while(STK.GetCount()>0 || (p && p->T.key==CN_DAND))
	{
		if(p && p->T.key==CN_DAND )
		{		
			// add action here
			if(p->pleft && p->pleft->T.key>=CN_GREATT&&p->pleft->T.key<=CN_DEQUAL)
			{
				FAND.Add(p); 
				NAND.Add(p->pleft); 
			}

			if(p->pright && p->pright->T.key>=CN_GREATT&&p->pright->T.key<=CN_DEQUAL)
			{
				FAND.Add(p); 
				NAND.Add(p->pright); 
			}

			// end
			STK.AddTail(p);
			 p=STK.GetTail()->pleft;
		}
		else
		{
			p=STK.GetTail()->pright;
			STK.RemoveTail();
		}
	}

	int ix=0;
	int index=0;
	while(ix<NAND.GetSize())
	{
		index=ix+1;
		while(index<NAND.GetSize())
		{
			if(NAND[ix] && NAND[index] &&
			   NAND[ix]->pleft && NAND[index]->pleft &&			   
			   NAND[ix]->pright && NAND[index]->pright )
			{
				if(FindBoolRedundant(NAND[ix],NAND[index],FAND[ix],FAND[index],SArry))
				{
					NAND.RemoveAt(index);
					FAND.RemoveAt(index);
					index--;
				}				
			}
			index++;
		}
		ix++;
	}
	
}*/

/*bool CVarRemove::FindBoolRedundant(ENODE *ptr1,ENODE *ptr2,ENODE *pftr1,ENODE *pftr2,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************
		FUNCTION:
		Remove redundant part(s) in bool expression;
		example:
		A>3 && A>10--->A>10;
		A>3 && A==3--->A>=3;
		...

		PARAMETER:
		pNode : pointer of USDG node
	***********************************************/

/*	double dtemp1=0,dtemp2=0;
	
    if(ptr1->pleft->T.key==CN_VARIABLE && ptr2->pleft->T.key==CN_VARIABLE &&
	   ptr1->pleft->T.addr==ptr2->pleft->T.addr && SArry[ptr1->pleft->T.addr].kind==CN_VARIABLE &&
	   isConstOfE(ptr1->pright) && isConstOfE(ptr2->pright) )
	{
		// get the value;
		if(ptr1->pright->T.key==CN_CINT || ptr1->pright->T.key==CN_CCHAR)
		{
			dtemp1=ptr1->pright->T.addr; 
		}
		else if(ptr1->pright->T.key==CN_CFLOAT)
		{
			dtemp1=ptr1->pright->T.value; 
		}

		if(ptr2->pright->T.key==CN_CINT || ptr2->pright->T.key==CN_CCHAR)
		{
			dtemp2=ptr2->pright->T.addr; 
		}
		else if(ptr2->pright->T.key==CN_CFLOAT)
		{
			dtemp2=ptr2->pright->T.value; 
		}

		// to find redundant part and remove
		if(ptr1->T.key==CN_DEQUAL) //==
		{
			if(ptr2->T.key==CN_GREATT && dtemp1>dtemp2)
			{
				DeleteBoolPart(pftr2,ptr2);			
				return true;
			}

			if(ptr2->T.key==CN_NOTEQUAL && dtemp1==dtemp2)
			{
				// error
				// 将此布尔表达式置成0;
				return false;
			}

			if(ptr2->T.key==CN_BANDE && dtemp1>=dtemp2)
			{
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}
		}
		else if(ptr1->T.key==CN_BANDE) //>=
		{
			if(ptr2->T.key==CN_GREATT)
			{
				ptr1->T.key=dtemp1>dtemp2?CN_BANDE:CN_GREATT;
				if(ptr1->pright->T.key==CN_CFLOAT || ptr2->pright->T.key==CN_CFLOAT)
				{
					ptr1->pright->T.value=dtemp1>dtemp2?dtemp1:dtemp2;
				}
				else
				{
					ptr1->pright->T.addr=(int)(dtemp1>dtemp2?dtemp1:dtemp2);
				}	
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

			if(ptr2->T.key==CN_DEQUAL && dtemp1<=dtemp2)
			{
				ptr1->T.key=CN_DEQUAL;
			    CopyTNODE(ptr1->pright->T,ptr2->pright->T);
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

			if(ptr2->T.key==CN_BANDE)
			{
				if(ptr1->pright->T.key==CN_CFLOAT || ptr2->pright->T.key==CN_CFLOAT)
				{
					ptr1->pright->T.value=dtemp1>dtemp2?dtemp1:dtemp2;
				}
				else
				{
					ptr1->pright->T.addr=(int)(dtemp1>dtemp2?dtemp1:dtemp2);
				}
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

		}
		else if(ptr1->T.key==CN_GREATT) // >
		{
			if(ptr2->T.key==CN_GREATT)
			{
				if(ptr1->pright->T.key==CN_CFLOAT || ptr2->pright->T.key==CN_CFLOAT)
				{
					ptr1->pright->T.value=dtemp1>dtemp2?dtemp1:dtemp2;
				}
				else
				{
					ptr1->pright->T.addr=(int)(dtemp1>dtemp2?dtemp1:dtemp2);
				}
				DeleteBoolPart(pftr2,ptr2);				   
				return true;
			}

			if(ptr2->T.key==CN_DEQUAL && dtemp1<dtemp2)
			{
				ptr1->T.key=CN_DEQUAL;
				CopyTNODE(ptr1->pright->T,ptr2->pright->T);
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

			if(ptr2->T.key==CN_BANDE)
			{
				ptr1->T.key=dtemp1>=dtemp2?CN_GREATT:CN_BANDE;
				if(ptr1->pright->T.key==CN_CFLOAT || ptr2->pright->T.key==CN_CFLOAT)
				{
					ptr1->pright->T.value=dtemp1>=dtemp2?dtemp1:dtemp2;
				}
				else
				{
					ptr1->pright->T.addr=(int)(dtemp1>=dtemp2?dtemp1:dtemp2);
				}
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}			
		}

	}


	if(ptr1->pright->T.key==CN_VARIABLE && ptr2->pright->T.key==CN_VARIABLE &&
	   ptr1->pright->T.addr==ptr2->pright->T.addr && SArry[ptr1->pright->T.addr].kind==CN_VARIABLE &&
	   isConstOfE(ptr1->pleft) && isConstOfE(ptr2->pleft) )
	{
		// get the value;
		if(ptr1->pleft->T.key==CN_CINT || ptr1->pleft->T.key==CN_CCHAR)
		{
			dtemp1=ptr1->pleft->T.addr; 
		}
		else if(ptr1->pleft->T.key==CN_CFLOAT)
		{
			dtemp1=ptr1->pleft->T.value; 
		}

		if(ptr2->pleft->T.key==CN_CINT || ptr2->pleft->T.key==CN_CCHAR)
		{
			dtemp2=ptr2->pleft->T.addr; 
		}
		else if(ptr2->pleft->T.key==CN_CFLOAT)
		{
			dtemp2=ptr2->pleft->T.value; 
		}

		// to find redundant part and remove
		if(ptr1->T.key==CN_DEQUAL) //==
		{
			if(ptr2->T.key==CN_GREATT && dtemp1<dtemp2)
			{
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}
			else
			{
				// error
				// 将此布尔表达式置成0;
			}

			if(ptr2->T.key==CN_NOTEQUAL && dtemp1==dtemp2)
			{
				// error
				// 将此布尔表达式置成0;
				return false;
			}

			if(ptr2->T.key==CN_BANDE && dtemp1<=dtemp2)
			{
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

		}
		else if(ptr1->T.key==CN_BANDE) //>=
		{
			if(ptr2->T.key==CN_GREATT)
			{
				ptr1->T.key=dtemp1<dtemp2?CN_BANDE:CN_GREATT;
				if(ptr1->pleft->T.key==CN_CFLOAT || ptr2->pleft->T.key==CN_CFLOAT)
				{
					ptr1->pleft->T.value=dtemp1<dtemp2?dtemp1:dtemp2;
				}
				else
				{   
					ptr1->pleft->T.addr=(int)(dtemp1<dtemp2?dtemp1:dtemp2);
				}

				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

			if(ptr2->T.key==CN_DEQUAL && dtemp1>=dtemp2)
			{				
				ptr1->T.key=CN_DEQUAL;
				CopyTNODE(ptr1->pleft->T,ptr2->pleft->T);
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

			if(ptr2->T.key==CN_BANDE)
			{
				if(ptr1->pleft->T.key==CN_CFLOAT || ptr2->pleft->T.key==CN_CFLOAT)
				{
					ptr1->pleft->T.value=dtemp1>dtemp2?dtemp2:dtemp1;
				}
				else
				{
					ptr1->pleft->T.addr=(int)(dtemp1>dtemp2?dtemp2:dtemp1);
				}
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}

		}
		else if(ptr1->T.key==CN_GREATT) // >
		{
			if(ptr2->T.key==CN_GREATT)
			{
				
				if(ptr1->pleft->T.key==CN_CFLOAT || ptr2->pleft->T.key==CN_CFLOAT)
				{
					ptr1->pleft->T.value=dtemp1>dtemp2?dtemp2:dtemp1;
				}
				else
				{
					ptr1->pleft->T.addr=(int)(dtemp1>dtemp2?dtemp2:dtemp1);
				}
				DeleteBoolPart(pftr2,ptr2);				   
				return true;
			}

			if(ptr2->T.key==CN_DEQUAL && dtemp1>dtemp2)
			{
				ptr1->T.key=CN_DEQUAL;
				CopyTNODE(ptr1->pleft->T,ptr2->pleft->T);
				DeleteBoolPart(pftr2,ptr2);	
				return true;
			}

			if(ptr2->T.key==CN_BANDE)
			{
				ptr1->T.key=dtemp1>dtemp2?CN_BANDE:CN_GREATT;

				if(ptr1->pleft->T.key==CN_CFLOAT || ptr2->pleft->T.key==CN_CFLOAT)
				{
					ptr1->pleft->T.value=dtemp1>=dtemp2?dtemp2:dtemp1;
				}
				else
				{
					ptr1->pleft->T.addr=(int)(dtemp1>=dtemp2?dtemp2:dtemp1);
				}
				DeleteBoolPart(pftr2,ptr2);
				return true;
			}			
		}			
	}

	return FindBoolRedundantC(ptr1,ptr2,pftr1,pftr2,SArry);
}*/

/*bool CVarRemove::FindBoolRedundantC(ENODE *ptr1,ENODE *ptr2,ENODE *pftr1,ENODE *pftr2,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************
		FUNCTION:
		Remove redundant part(s) in bool expression
		(complementarity of FindBoolRedundant);
		example:
		A==3 && 10<10--->A==10;
		...

		PARAMETER:
		pNode : pointer of USDG node
	***********************************************/
 /*   ENODE *pel,*ptr;
	ENODE *pfel,*pftr;
    if(ptr1->T.key!=CN_DEQUAL && ptr2->T.key!=CN_DEQUAL) 
	{
		return false;
	}

	if(ptr1->T.key==CN_DEQUAL)
	{
		pel=ptr1;
		ptr=ptr2;
		pfel=pftr1;
		pftr=pftr2;
	}
	else
	{
		pel=ptr2;
		ptr=ptr1;
		pfel=pftr2;
		pftr=pftr1;
	}

    if(!(pel->pleft && pel->pright && ptr->pleft && ptr->pright))
	{
		return false;
	}

	bool bel=isConstOfE(pel->pright);
	bool btr=isConstOfE(ptr->pright);
   
	if((isConstOfE(pel->pleft) || bel ) &&
	   (isConstOfE(ptr->pleft) || btr ) )
	{
		TNODE *pt1,*pt2;
		TNODE *p1,*p2;

		pt1=bel?&(pel->pleft->T):&(pel->pright->T);
		pt2=btr?&(ptr->pleft->T):&(ptr->pright->T);

		p1=bel?&(pel->pright->T):&(pel->pleft->T);
		p2=btr?&(ptr->pright->T):&(ptr->pleft->T);
        
		double dtemp1,dtemp2;
		
		if(p1->key==CN_CFLOAT)
		{
			dtemp1=p1->value; 
		}
		else
		{
			dtemp1=p1->addr; 
		}

		if(p2->key==CN_CFLOAT )
		{
			dtemp2=p2->value; 
		}
		else
		{
			dtemp2=p2->addr; 
		}
		
		if(pt1->key==CN_VARIABLE && SArry[pt1->addr].kind==CN_VARIABLE &&
		   pt2->key==CN_VARIABLE && SArry[pt2->addr].kind==CN_VARIABLE &&
		   pt1->addr==pt2->addr )
		{
			if(btr)
			{
				if(ptr->T.key==CN_DEQUAL && dtemp1==dtemp2)
				{
					DeleteBoolPart(pftr,ptr);
					return true;
				}

				if(ptr->T.key==CN_GREATT && dtemp1>dtemp2)
				{
					DeleteBoolPart(pftr,ptr);
					return true;
				}

				if(ptr->T.key==CN_LANDE && dtemp1>=dtemp2)
				{
					DeleteBoolPart(pftr,ptr);
					return true;
				}			

			}
			else 
			{
				if(ptr->T.key==CN_DEQUAL && dtemp1==dtemp2)
				{
					DeleteBoolPart(pftr,ptr);
					return true;
				}

				if(ptr->T.key==CN_GREATT && dtemp1<dtemp2)
				{
					DeleteBoolPart(pftr,ptr);
					return true;
				}

				if(ptr->T.key==CN_LANDE && dtemp1<=dtemp2)
				{
					DeleteBoolPart(pftr,ptr);
					return true;
				}

			}
		}
		
	}

    return false;
        

}*/

/*void CVarRemove::DeleteBoolPart(ENODE *pftr,ENODE * ptr)
{
	int itemp=ptr->info ;
	ptr->info=DELLAB;
	ENODE *ptemp=NULL;
	if(pftr->pleft && pftr->pleft->info==DELLAB)
	{
		ptemp=pftr->pleft;
		pftr->pleft=NULL;
	}
	else if(pftr->pright && pftr->pright->info==DELLAB)
	{
		ptemp=pftr->pright;
		pftr->pright=NULL;
	}

	if(ptemp)
	{
		if(ptemp->pleft)
		{
			delete ptemp->pleft; 
		}
		if(ptemp->pright)
		{
			delete ptemp->pright; 
		}

		delete ptemp;
	}
}*/

////////////////////###########Rename Variables############///////////////////////
/*void CVarRemove::RenameAndReorder(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry)
{//变量重命名和重新排列语句顺序
	VariablesRename(pUSDGHead,SArry);

	SetSDGNodeName(pUSDGHead);
	SetInOutDataFlow(pUSDGHead,SArry);
	ResetStatementOrder(pUSDGHead,SArry);

}*/
void CVarRemove::VariablesRename(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************
		FUNCTION:
        Rename Variables;
		PARAMETER:
		pHead : head pointer of USDG
		SArry : ...
	***********************************************/

	FREQ FNode;
	CArray<FREQ,FREQ> vFreq;
 
	for(int i=0;i<SArry.GetSize();i++)
	{
		if(SArry[i].kind!=CN_DFUNCTION && SArry[i].kind!=CN_BFUNCTION)
		{
			FNode.addr=i;
			FNode.oldname=SArry[i].name;
			FNode.type=0;
			FNode.vcount=0;
			FNode.vfrequency=0;
			FNode.newname=_T("");
			vFreq.Add(FNode);
		}
	}
  
	GetFrequency(pHead,SArry,vFreq);
	AssignNewName(SArry,vFreq);
	ResetVarName(pHead,SArry,vFreq);
	vFreq.RemoveAll();
}


void CVarRemove::GetFrequency(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		search USDG and get Statistic result;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/
   
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
    p=pHead;
   

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p->visit==p->GetRelateNum())
			{
				// Add action here
                
			/*	if(p->m_sType=="ASSIGNMENT" || p->m_sType=="ITERATION" ||
				   p->m_sType=="SELECTOR"   || p->m_sType=="_ELSE"     ||
				   p->m_sType=="SDGRETURN"  || p->m_sType=="SDGCALL" )*/
				if(p->m_sType=="ASSIGNMENT" || p->m_sType=="ITERATION" ||
				   p->m_sType=="SELECTOR"   ||
				   p->m_sType=="SDGRETURN"  || p->m_sType=="SDGCALL" )
				{
					SetFrequencyInfo(p,SArry,vFreq);					
				}
				
				// end of this part
				S.RemoveTail();
			}
					   			
			if(p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

}

void CVarRemove::SetFrequencyInfo(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		Analyze the node and add variable Statistic 
		information to vFreq;
			    
		PARAMETER:
		pNode : pointer of USDG
		vFreq : ...
		
	******************************************************/
	int itype=0;

	if(pNode==NULL)
		return;
	
	if(pNode->m_sType=="ASSIGNMENT" )
	{
		CAssignmentUnit *pas=(CAssignmentUnit *)pNode;
		FrequencyOfExp(pas->m_pinfo,SArry,vFreq);
		FrequencyOfExp(pas->m_pleft,SArry,vFreq);
		
		if(isIncOrDec(pNode,SArry))
		{
			AfxMessageBox("inc or dec statement");/////
			TNODE T;

			if(pas->m_pinfo&&pas->m_pinfo->pleft)
			CopyTNODE(T,pas->m_pinfo->pleft->T);
         
            CSDGBase *ptemp=pNode;
			while(ptemp && ptemp->m_sType!="SDGENTRY" && ptemp->m_sType!="SDGHEAD" )
			{
				if(ptemp->m_sType=="ITERATION")
				{
					if(DefOrRefVariableEx(T,ptemp)>0)
					{
						itype=2; 				
						break;
					}
				}
				ptemp=ptemp->GetFather(); 

			}

			if(itype>0)
			{
				for(int i=0;i<vFreq.GetSize();i++)
				{
					if(vFreq[i].addr==T.addr)
					{
						vFreq[i].type+=itype;
					}
				}
			}
			
		}
	}
	else 
	{
		FrequencyOfExp(pNode->m_pinfo,SArry,vFreq);

	}		
    
}

void CVarRemove::FrequencyOfExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		search syntax tree and add variable Statistic 
		information to vFreq;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=pENode;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;
				if(p&&p->T.key==CN_VARIABLE)
				{
					for(int i=0;i<vFreq.GetSize(); i++)
					{
						if(p->T.addr==vFreq[i].addr)
						{
							vFreq[i].vcount++; 
						}
					}

					for(int i=0;p&&i<10&& p->pinfo[i] ; i++)
					{
						FrequencyOfExp(p->pinfo[i],SArry,vFreq);						
					}
				}
			///////////////////////wtt//////////3.8/////////////////函数参数中的变量///
				if(p&&(p->T.key==CN_BFUNCTION||p->T.key==CN_DFUNCTION))
				{
						
					for(int i=0;i<10&& p->pinfo[i] ; i++)
					{
						FrequencyOfExp(p->pinfo[i],SArry,vFreq);						
					}

				}
				////////////////////////////////////

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);
}

void CVarRemove::AssignNewName(CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		Assign new name to variable based on Statistic result;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/
	/*
	TRACE("\n");
    for(int j=0;j<vFreq.GetSize();j++)
	{
		TRACE("%s= addr=%d vcount=%d type=%d\n",vFreq[j].oldname,vFreq[j].addr,vFreq[j].vcount,vFreq[j].type );
	}
	*/

	CArray<bool,bool> aLab;
	for(int i=0;i<vFreq.GetSize();i++)
	{
		if(vFreq[i].vcount<=0)
		{
			aLab.Add(true);
		}
		else
		{
			aLab.Add(false); 
		}
	}

	bool bEnd=false;
	int index=0;
	int value=0;
	CString str,s;
	int inum=0,cnum=0,dnum=0,fnum=0,itr=0;
	int i;
	while(!bEnd)
	{
		i=0;
		while(i<vFreq.GetSize() && aLab[i])
		{
			i++;
		}
		if(i>=vFreq.GetSize())
		{
			break;
		}

		index=i;
		value=vFreq[i].vcount;

		while(i<vFreq.GetSize())
		{
			if(aLab[i]==false && vFreq[i].vcount>value)
			{
				index=i;
				value=vFreq[i].vcount;
			}
			i++;
		}

		if(index<vFreq.GetSize() && aLab[index]==false)
		{
			str="";
			s="";
			if(vFreq[index].addr>=0&&vFreq[index].addr<SArry.GetSize())
			switch(SArry[vFreq[index].addr].type)
			{
			case CN_INT:
				if(vFreq[index].type>=2)
				{
					itr++;
					if(itr<10)
					{
						s.Format("_0%d",itr); 
					}
					else
					{
						s.Format("_%d",itr); 
					}
					str="x";
				}
				else
				{
					inum++;
				    if(inum<10)
					{
						s.Format("_0%d",inum); 
					}
					else
					{
						s.Format("_%d",inum); 
					}
					str="i";
				}

				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			case CN_CHAR:
				cnum++;
				if(cnum<10)
				{
					s.Format("_0%d",cnum); 
				}
				else
				{
					s.Format("_%d",cnum); 
				}
				str="c";
				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			case CN_DOUBLE:
				dnum++;
				if(dnum<10)
				{
					s.Format("_0%d",dnum); 
				}
				else
				{
					s.Format("_%d",dnum); 
				}
				str="d";
				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			case CN_FLOAT:
				fnum++;
				if(fnum<10)
				{
					s.Format("_0%d",fnum); 
				}
				else
				{
					s.Format("_%d",fnum); 
				}
				str="f";
				if(SArry[vFreq[index].addr].kind==CN_ARRAY)
				{
					str+="A";
				}

				if(SArry[vFreq[index].addr].kind==CN_POINTER)
				{
					str+="P";
				}
				break;
			}

			vFreq[index].newname=str+s;
			aLab[index]=true;

		}		
		
		// is every variable assigned a new name?
		bEnd=true;
		for(i=0;i<aLab.GetSize();i++)
		{
			if(!aLab[i])
			{
				bEnd=false;
			}
		}
		// end of this part
	}
    /*
	for(i=0;i<vFreq.GetSize();i++)
	{
		TRACE("%s addr=%d vcount=%d type=%d newname=%s\n",vFreq[i].oldname,vFreq[i].addr,vFreq[i].vcount,vFreq[i].type,vFreq[i].newname);
	}
	*/
}

void CVarRemove::ResetVarName(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		Reset Variable  name based on vFreq's elements;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
    p=pHead;
    
	for(int i=0;i<vFreq.GetSize();i++)
	{
		if(vFreq[i].addr>=0&&vFreq[i].addr<SArry.GetSize())
		SArry[vFreq[i].addr].name=vFreq[i].newname;  
	}

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p&&p->visit==p->GetRelateNum())
			{
				// Add action here
                
				if(p->m_sType=="#DECLARE" && p->TCnt.GetSize()>=1 && 
				   p->TCnt[0].key==CN_VARIABLE&&p->TCnt[0].addr>=0&&p->TCnt[0].addr<SArry.GetSize())
				{
					p->TCnt[0].name=SArry[p->TCnt[0].addr].name; 
				}

				RenameVarInExp(p->m_pinfo,SArry,vFreq);

				if(p->m_sType=="ASSIGNMENT")
				{
					CAssignmentUnit *pas=(CAssignmentUnit *)p;
					RenameVarInExp(pas->m_pleft,SArry,vFreq);
				}

				// end of this part
				S.RemoveTail();
			}
					   			
			if(p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
    
}

void CVarRemove::RenameVarInExp(ENODE *pENode,CArray<SNODE,SNODE> &SArry,CArray<FREQ,FREQ> &vFreq)
{
	/******************************************************

	    FUNCTION:
		search syntax tree and add variable Statistic 
		information to vFreq;
			    
		PARAMETER:
		pHead : Head pointer of USDG
		vFreq : ...
		
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	ENODE *p=pENode;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;
				if(p->T.key==CN_VARIABLE || p->T.key==CN_DFUNCTION ||p->T.key==CN_BFUNCTION)
				{
					for(int i=0;i<vFreq.GetSize(); i++)
					{
						if(p->T.addr==vFreq[i].addr && p->T.key==CN_VARIABLE)
						{
							p->T.name=vFreq[i].newname; 
						}
					}

					for(int i=0;i<10  && p->pinfo[i] ; i++)
					{
						RenameVarInExp(p->pinfo[i],SArry,vFreq);						
					}
				}

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);
}



///////////////////////////////////////////////////////////////////////////////////
void RemoveEmptyBranchEx(CSDGBase *pHead)
{
	CVarRemove varremove;
	varremove.RemoveEmptyBranch(pHead);



}
void CVarRemove::RemoveEmptyBranch(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		Traverse the USDG and delete the declare node that 
		the variable it defined is never used in the current
		function
			    
		PARAMETER:
		pHead : Head pointer of USDG
		
	******************************************************/

	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=pHead;
    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());

			if(p->visit==p->GetRelateNum())
			{
				//  add your action here
				/*
				if(p->m_sType=="SELECTION")
				{
					int i=p->GetRelateNum()-1;
					for(;i>=0;i--)
					{
						CSDGBase *psel;
						psel=p->GetNode(i);

						if(psel->GetRelateNum()==0)
						{
							DeleteExpTree(psel);
							delete psel;
							p->Del(i); 
						}
					}						
				}
				
				if(p->m_sType=="ITERATION")
				{
					if(p->GetRelateNum()>=2 && p->GetNode(1)->m_sType=="ESPSUB" && p->GetNode(1)->GetRelateNum()==0)
					{
						delete p->GetNode(1);
						p->Del(1); 
					}
				}
				*/

				if(p->GetRelateNum()>0)
				{
					for(int i=p->GetRelateNum()-1;i>=0;i--)
					{
						if((p->GetNode(i)&&p->GetNode(i)->m_sType=="SELECTOR"  && p->GetNode(i)->GetRelateNum()<=0) ||
						   (p->GetNode(i)&&p->GetNode(i)->m_sType=="ITERATION" && p->GetNode(i)->GetRelateNum()<=0) 
						   )
						{
						//	AfxMessageBox("delete empty branch"+p->GetNode(i)->m_sType);
						//	DeleteExpTree(p->GetNode(i));//wtt///5.10
							if(p->GetNode(i) )
							delete p->GetNode(i);
							p->Del(i); 
							modified=true;
						}
						else if(p->GetNode(i)&& (p->GetNode(i)->m_sType=="ESPSUB"    && p->GetNode(i)->GetRelateNum()<=0) ||
							(p->GetNode(i)->m_sType=="ITRSUB"    && p->GetNode(i)->GetRelateNum()<=0) ||							
						    (p->GetNode(i)->m_sType=="SELECTION" && p->GetNode(i)->GetRelateNum()<=0))
						{
							if(p->GetNode(i))
							delete p->GetNode(i);
							p->Del(i); 
							modified=true;
						}
					}
				}

				p->visit=p->GetRelateNum(); 
				//   end of this part
				S.RemoveTail();
			}
			if(p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

}

///////////////////////////////////////////////////////////////////////////////////

/*void CVarRemove::RemoveNotReferedVar(CSDGBase *pHead)
{
	/******************************************************

	    FUNCTION:
		post-order traverse the USDG and delete the Empty
		selector node of else node
			    
		PARAMETER:
		pHead : Head pointer of USDG
		
	******************************************************/
/*	int i=pHead->GetRelateNum()-1,j;
	bool bused=false;
	CSDGBase *p,*ptr;


	// remove global not used variables; 
	while(i>=0)
	{
		p=pHead->GetNode(i);

		if(p->m_sType=="#DECLARE")
		{
			j=i+1;
			bused=false;
			int k=0;
			while(j<pHead->GetRelateNum())
			{
				ptr=pHead->GetNode(j);
						
				if(ptr->m_sType!="#DECLARE"&&p->TCnt.GetSize()>=1 && (FindTNODEInDArry(p->TCnt[0],ptr->m_aDefine) ||
				 FindTNODEInDArry(p->TCnt[0],ptr->m_aRefer)) )
				{
					bused=true;
				}
				j++;
			}

			if(!bused)
			{
		
				delete p;
				pHead->Del(i);
		

			}
		}
		i--;
	}

	// remove local not used variables
    int k=0;
	while(k<pHead->GetRelateNum())
	{
		CSDGBase* phead1=pHead->GetNode(k);
		if(phead1 && phead1->m_sType=="SDGENTRY")
		{
			i=phead1->GetRelateNum()-1; 
			while(i>=0)
			{
				p=phead1->GetNode(i);
                
				if(p->m_sType=="#DECLARE")
				{
					j=i+1;
					bused=false;
					while(j<phead1->GetRelateNum())
					{
						ptr=phead1->GetNode(j); 
						if(p->TCnt.GetSize()>=1 && (FindTNODEInDArry(p->TCnt[0],ptr->m_aDefine) ||
						   FindTNODEInDArry(p->TCnt[0],ptr->m_aRefer)) )
						{
							bused=true;
						}
						j++;
					}

					if(!bused)
					{
						delete p;
						phead1->Del(i);						
					}
				}
				i--;
			}
		} // if(phead && ph
		k++;
	} // while(k<pHead->

}*/
//////////////////////wtt/////////////3.9///////////////////////

//void CVarRemove::RemoveNotReferedAssignment(CSDGBase *p,CSDGBase *pHead,CArray<SNODE,SNODE>&SArry)
//{
	/******************************************************

	    FUNCTION:
	     wtt编写删除没有被引用的赋值语句表达式
			    
		PARAMETER:
		pHead : Head pointer of USDG

		
	******************************************************/
/*	if(p==NULL)
		return;
	CSDGBase *pf;
	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	int m=p->GetRelateNum();
	for(int n=0;n<m;n++)
	{
	for(int i=0;i<p->GetRelateNum();i++)
	{
		RemoveNotReferedAssignment(p->GetNode(i),pHead,SArry);
	}
	}
	if(p->m_sType!="ASSIGNMENT")
		return;
	TNODE T;

	if(pas->m_pleft&&pas->m_pleft->T.key==CN_VARIABLE)
	{
		CopyTNODE(T,pas->m_pleft->T);

	}
	else 
		return;
	int index,index1;
	int	i=index1=index=FindIndex(p);
	pf=p->GetFather();
	if(pf==NULL)
		return;
	CSDGBase*pf1=pf;
/*	if(pf->m_sType=="SELECTOR" )
	{
	//	AfxMessageBox("SELCTOR");
		
		pf1=pf;
		pf=pf->GetFather();//"SELECTION"由于已经消除了嵌套的SELECTION所以只找一层SELECTION即可
		
		if(pf==NULL)
			return;
		index=FindIndex(pf);
		pf=pf->GetFather();
		if(pf==NULL)
			return;
	}
	if(pf->m_sType=="ITERATION" )
	{

	}
    
	SetInOutDataFlow(pHead,SArry);
	//i++;
	i=index+1;
	bool flag=false;
	while(i<pf->GetRelateNum())
	{
		if(FindTNODEInDArry(T,pf->GetNode(i)->m_aDefine))
		{
			flag=true;
			break;
		}
		i++;
	}

		
	
	for(int j=index+1;flag&&j<=i||j<i&&!flag;j++)
	{
		if(FindTNODEInDArry(T,pf->GetNode(j)->m_aRefer))
			break;
	}

	if(j<i&&!flag||j<=i&&flag)
		return;
*/
/////////////////////////////////
/*	SetInOutDataFlow(pHead,SArry);
	CArray<CSDGBase*,CSDGBase*>prList;
	prList.RemoveAll(); 
	FindRightSDGNode(p,prList);

	
	i=0;
	bool flag=false;
	while(i<prList.GetSize())
	{
		if(FindTNODEInDArry(T,prList[i]->m_aDefine))
		{
			flag=true;
			break;
		}
		i++;
	}		
	
	for(int j=0;flag&&j<=i||j<i&&!flag;j++)
	{
		if(FindTNODEInDArry(T,prList[j]->m_aRefer))
			break;
	}

	if(j<i&&!flag||j<=i&&flag)
		return;




	////////////////////////////////////

	DeleteExpTree(pas->m_pleft);
	DeleteExpTree(pas->m_pinfo);
	pas->m_pleft=pas->m_pinfo=NULL;
	delete pas;
	pf1->Del(index1);    

}*/

//////////////////////////////////////wtt//////////////////////////////
 /*void CVarRemove:: DeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry)////3.17
 {//删除未被引用的赋值语句和变量声明节点
	DeleteIdentity(pUSDGHead,SArry);/////////wtt////3.12////
 	RemoveSimpleStatement(pUSDGHead,pUSDGHead,SArry);////////////wtt///3.12//

	RemoveNotReferedAssignment(pUSDGHead,pUSDGHead,SArry);///wtt////3.9////
	SetInOutDataFlow(pUSDGHead,SArry);
	RemoveNotReferedVar(pUSDGHead); 
	RemoveEmptyBranch(pUSDGHead);
 }*/
extern bool CompareTrees(ENODE*pHead1,ENODE*pHead2,CArray<SNODE,SNODE> &SArry); 
/*void CVarRemove::DeleteIdentity(CSDGBase *p,CArray<SNODE,SNODE>&SArry)
{//wtt编写//3.12//删除恒等的赋值语句例如：n=n;
	if(p==NULL)
		return;
	CSDGBase *pf;
	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		DeleteIdentity(p->GetNode(i),SArry);
	}
	if(p->m_sType!="ASSIGNMENT")
		return;
	if(!CompareTrees(pas->m_pleft,pas->m_pinfo,SArry))
		return;
	pf=p->GetFather();
	int index=FindIndex(p);
	if(pf==NULL)
		return;
	pf->Del(index);
	DeleteBranch(p);
	p=NULL;


}

*/

//////////////////////////////////////Other Functions//////////////////////////////

void CVarRemove::CopyTNODE(TNODE &T,TNODE &TINT)
{
	T.addr=TINT.addr;
	T.key=TINT.key;
	T.line=TINT.line;
	T.name=TINT.name;
	T.value=TINT.value; 
	T.deref=TINT.deref;
	
}

void DeleteBranch(CSDGBase *pHead)
{
	CVarRemove varremove;
	varremove.DeleteBranch(pHead);
}
/*void CVarRemove::DeleteBranch1(CSDGBase *pHead)
{
	if(pHead==NULL)
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		CSDGBase*p=pHead->GetNode(i);
		DeleteBranch1(p);
		pHead->SetNode(i,NULL);
		
	}
	if(pHead->m_pinfo)
	DeleteExpTree1(pHead->m_pinfo);
	if(pHead->m_sType=="ASSIGNMENT" )
	{
		CAssignmentUnit *pas=(CAssignmentUnit *)pHead;
		DeleteExpTree1(pas->m_pleft);
	}
 
//	if (pHead)//有问题为什么加上后再处理就出错？
//	{
//		pHead->Clear(); 
//		delete pHead;
//	}
//	pHead=NULL;

}*/

void CVarRemove::DeleteBranch(CSDGBase *phead)
{
	// do not test
	/******************************************************

	    FUNCTION:
		delete the branch with the head poiter "phead" 
			    
		PARAMETER:
		phead : head pointer of SDG or USDG branch to delete,
		(including phead)
		
	******************************************************/

	if(phead==NULL)
		return;
	CList<CSDGBase *,CSDGBase *> S;
	CSDGBase *p=phead;

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
			//	DeleteExpTree(p);///wtt//5.10
				p->m_pinfo=NULL;///wtt//5.10 
				if(p->m_sType=="ASSIGNMENT" )
				{
					CAssignmentUnit *pas=(CAssignmentUnit *)p;
					pas->m_pinfo=pas->m_pleft;
				//	DeleteExpTree(pas);///wtt//5.10
					pas->m_pinfo=pas->m_pleft=NULL;///wtt//5.10
				}
				if(p)
				delete p;
				p=NULL;
			}
		}
    }
    //end;

}
/*void CVarRemove::DeleteExpTree1(ENODE*p)
{
	if(p==NULL)
		return;
	
	if(p==NULL)
	{
		return;
	}
	DeleteExpTree1(p->pleft );
	p->pleft=NULL;
	DeleteExpTree1(p->pright);
	p->pright=NULL;

	if(p->info>0 && p->info<10&& p->T.key==CN_VARIABLE)
	{
		
		for(int i=0;i<10 && p->pinfo[i] ; i++)
		{
				
			DeleteExpTree1(p->pinfo[i]);
			p->pinfo[i]=NULL;
		}
	}
	if(p) delete p;
	p=NULL;






}*/


void CVarRemove::DeleteExpTree(CSDGBase *ptr)
{   
	/******************************************************

	    FUNCTION:
		post-order traverse the syntax tree and delete it 
			    
		PARAMETER:
		ptr : Unit node which inludes the syntax tree to 
		      delete .
		
	******************************************************/

	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 

	// -1:NULL,0:not,1:left,2:all

	
	if(ptr==NULL)
		return;

	ENODE *p=NULL;

	p=ptr->m_pinfo;

	if(p==NULL)
	{
		return;
	}

	do{
		while(p!=NULL)
		{

			STK.AddTail(p);
			SBL.AddTail(1);

			p=p->pleft; 
		}

		bool iscontinue=true;

		while(iscontinue && STK.GetCount()>0 )
		{
			p=STK.GetTail();
			int lab=SBL.GetTail();

    		STK.RemoveTail();			
			SBL.RemoveTail(); 

			switch(lab)
			{
			case 1:

				STK.AddTail(p);
				SBL.AddTail(2); 

				p=p->pright;

				iscontinue=false;
				break;
			case 2:
				// add action here;

				if(p&&p->info>0 && p->info<10&& p->T.key==CN_VARIABLE)
				{
					CSDGBase SDGTemp;
					for(int i=0;i<10 && p->pinfo[i] ; i++)
					{
						SDGTemp.m_pinfo=p->pinfo[i];
						DeleteExpTree(&SDGTemp);
						p->pinfo[i]=NULL;
					}
				}
	
				if(p) delete p;
				p=NULL;

				//end of this part

				break;
			}

		}

	}while(STK.GetCount()>0);

	ptr->m_pinfo=NULL;
}
/*void CVarRemove::DeleteExpTree(ENODE *ptr)
{
	if(ptr==NULL)
		return;
	
	if(ptr->pleft)
	DeleteExpTree(ptr->pleft);
	ptr->pleft=NULL;
	if(ptr->pright)
	DeleteExpTree(ptr->pright);
	ptr->pright=NULL;
	for(int i=0;i<10;i++)
	{
		if(ptr->pinfo[i])
		{
			DeleteExpTree(ptr->pinfo[i]);
			ptr->pinfo[i]=NULL;
		}
	}
	delete ptr;

}*/

bool CVarRemove::isConstOfE(ENODE *p)
{
	/******************************************************

	    FUNCTION:
		the node is CN_CINT,CN_CCHAR or CN_CFLOAT(including 
		-/+ const);
			    
		PARAMETER:
		p   : ENODE pointer
		
	******************************************************/

	if(!p)
	{
		return false;
	}

	if(p->T.key==CN_CINT || p->T.key==CN_CCHAR || p->T.key==CN_CFLOAT)
	{
		return true;
	}

	if((p->T.key==CN_ADD || p->T.key==CN_SUB ) && p->pleft && p->pright==NULL)  
	{
		if(p->pleft->T.key==CN_CINT || p->pleft->T.key==CN_CCHAR || p->pleft->T.key==CN_CFLOAT)
		{
			if(p->T.key==CN_SUB)
			{
				CopyTNODE(p->T,p->pleft->T);
				if(p->pleft->T.key==CN_CFLOAT)
				{
					p->T.value=-p->T.value;  
				}
				else
				{
					p->T.addr=-p->T.addr;
				}
			}
			else
			{
				CopyTNODE(p->T,p->pleft->T);
			}
			if(p->pleft)
			delete p->pleft;
			p->pleft=NULL;
			return true;
		}
		
	}	

	return false;
}

bool CVarRemove::isIncOrDec(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
{
	/******************************************************

	    FUNCTION:
		the assignment node is one of the kinds i=i+1 ,i=i-1; 
			    
		PARAMETER:
		pNode   : ...
		
	******************************************************/

	if(pNode==NULL)
		return false;
	if(pNode->m_sType!="ASSIGNMENT")
	{
		return false;
	}

	CAssignmentUnit *pas=(CAssignmentUnit *)pNode;

	int addr=-1;
	int itype=-1;

	if(pas->m_pleft && pas->m_pleft->T.key==CN_VARIABLE)
	{
		addr=pas->m_pleft->T.addr;
		if( addr>=0&&addr<SArry.GetSize()&&SArry[addr].kind==CN_VARIABLE &&
			SArry[addr].type==CN_INT)
		{
			itype=1;
		}

		if(addr>=0&&addr<SArry.GetSize()&&SArry[addr].kind==CN_POINTER)
		{
			itype=2;
		}

		if(itype<1 || itype>2)
		{
			return false;
		}
        
		if(pas->m_pinfo && pas->m_pinfo->pleft && pas->m_pinfo->pright &&
		   (pas->m_pinfo->T.key==CN_ADD || pas->m_pinfo->T.key==CN_SUB) )
		{
			ENODE *pleft,*pright;
			pleft=pas->m_pinfo->pleft;
			pright=pas->m_pinfo->pright;

			if(pleft->T.key==CN_VARIABLE && pleft->T.addr==addr &&
			   pleft->info==pas->m_pleft->info &&   
			   pleft->pleft==NULL && pleft->pright==NULL &&
			   isConstOfE(pright) )
			{
				return true;
			}
		}
	}

	return false;
}


//void CVarRemove::SetSDGNodeName(CSDGBase *pHead)
//{
	/***********************************************************************
		FUNCTION:
		set the string of every node  in SDGthe
		PARAMETER:
		pHead : head pointer of SDG
		
	***********************************************************************/

/*	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;
	int depth=0;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);
			
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				// Add your code here
				p->m_sName="";//wtt///3.17

				p->m_sName+=p->m_sType+":";

				if(p->m_sType=="ASSIGNMENT")
				{
					CAssignmentUnit *pas=(CAssignmentUnit *)p;
					p->m_sName+=GetExpString(pas->m_pleft);
			
					p->m_sName+="=";
					p->m_sName+=GetExpString(p->m_pinfo);
				

				}			
				else if(p->m_sType=="SELECTOR")
				{
					p->m_sName+=GetExpString(p->m_pinfo);
				
				}
				else if(p->m_sType=="_ELSE")
				{
					p->m_sName+=GetExpString(p->m_pinfo);	
				
				}
				else if(p->m_sType=="ITERATION")
				{
					p->m_sName+=GetExpString(p->m_pinfo);
					
				}
				else if(p->m_sType=="#DECLARE")
				{
					p->m_sName+=p->TCnt.GetSize()?p->TCnt[0].name:"";
				}
				else if(p->m_sType=="SDGCALL")
				{
					p->m_sName+=GetExpString(p->m_pinfo);
				
				}
				else if(p->m_sType=="SDGRETURN")
				{
					p->m_sName+=GetExpString(p->m_pinfo);	
					
				}
				else if(p->m_sType=="SDGENTRY")
				{
					p->m_sName+=p->TCnt.GetSize()?p->TCnt[0].name:"";					
				}


				//TRACE("ID=%d:%s\n",p->idno,p->m_sName);

				// End of this part
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }
	
}*/

///////////////////////////////////////////////////////////////////

//////////////wtt////////////3.9//////////
//CString CVarRemove::GetExpString(ENODE*pHead)
CString GetExpString(ENODE*pHead, CArray<SNODE,SNODE> &SArry )
{


//wtt//3.9///中根序遍历抽象语法树,输出表达式字符串

CString s="",stemp="";
ENODE*p=pHead;

if(p==NULL)
return stemp;

	for(int m=0;m<p->T.deref;m++)//wtt 2008.3.19 指针脱引用
	{
		//	AfxMessageBox("ok");//////////
		stemp+="*(";
	}

	if(p->pright==NULL)
	{
		stemp+=GetENodeStr(p,SArry);
	
		if(p->pleft)
		{
		//	AfxMessageBox("p->pleft");
			stemp+=GetExpString(p->pleft,SArry);
		
		}
				///////////////////////////////////////////////////////
		if(p->info<0)//指针自赋值算术////////wtt  2008.3.22
		{
			//stemp+="+";
			for(int n=0;n<-p->info;n++)
				stemp+=GetExpString(p->pinfo[n],SArry);

		}
	/////////////////////////////////////////
		for(int m=0;m<p->T.deref;m++)//wtt 2008.3.19 指针脱引用
		{
			//	AfxMessageBox("ok");//////////
				stemp=stemp+=")";
		}
	


		return stemp;


	}

	
	if(p->pleft)
	{
	//	AfxMessageBox("p->pleft");
		stemp+=GetExpString(p->pleft,SArry);
		
	}
	stemp+=GetENodeStr(p,SArry);

	if(p->pright)
	{ //   AfxMessageBox("p->pright");
		stemp+=GetExpString(p->pright,SArry);
	}

	for(int m=0;m<p->T.deref;m++)//wtt 2008.3.19 指针脱引用
	{
		//	AfxMessageBox("ok");//////////
			stemp=stemp+=")";
	}
	///////////////////////////////////////////////////////
	if(p->info<0)//指针自赋值算术////////wtt  2008.3.22
	{
			//stemp+="+";
		for(int n=0;n<-p->info;n++)
			stemp+=GetExpString(p->pinfo[n],SArry);

	}
	/////////////////////////////////////////

	return stemp;




}
//CString CVarRemove::GetENodeStr(ENODE*p)
CString GetENodeStr(ENODE*p,CArray<SNODE,SNODE> &SArry)///wtt//3.9
{///wtt/3.9//返回一个语法树节点的字符串
	CString stemp="",s;
	if(p==NULL)
		return stemp;




	if((p->T).key>0&&(p->T).key<133)
		{
			switch((p->T).key)
			{
		
			case CN_CINT:
				s.Format("%d",(p->T).addr); 
				//AfxMessageBox(s);
				stemp+=s;
				break;
			case CN_CCHAR:
				switch((p->T).addr)
				{
					case '\0':
						s="\'\\0\'";
						break;
					case '\n':
						s="\'\\n\'";
						break;
					case '\t':
						s="\'\\t\'";
					case '\v':
						s="\'\\v\'";
						break;	
					case '\b':
						s="\'\\b\'";
						break;	
					case '\r':
						s="\'\\r\'";
						break;	
					case '\f':
						s="\'\\f\'";
						break;	
					case '\\':
						s="\'\\\\'";
						break;	
					case ' ':
						s="\' \'";
						break;
					default:
						s.Format("%c",(p->T).addr); 
				}
				//AfxMessageBox(s);
				stemp+=s;
				break;

			case CN_CFLOAT:
				s.Format("%f",(p->T).value);
				//	AfxMessageBox(s);
				stemp+=s;
			case CN_VARIABLE:			
			case CN_DFUNCTION:
			case CN_BFUNCTION:
				
				stemp+=(p->T).name;
				//////////wtt 2006.3.21////////////////*指针///
			/*	if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_POINTER)
				{
				
			
					for(int m=0;m<p->T.deref;m++)
					{
						stemp="*"+stemp;
					}

				}*/
				if(p->T.key==CN_VARIABLE&&p->T.deref==-1)
			
				{
					stemp="&"+stemp;

				}
				///////////////////wtt 2006.3.21//////////////////////////
				//	AfxMessageBox((p->T).name);
				break;
			case CN_CSTRING:
				stemp+="\""+(p->T).name+"\"";
				break;
			default:
	
				int key=(p->T).key;
				//AfxMessageBox(C_ALL_KEYWORD[key]);
			 stemp+=C_ALL_KEYWORD[key];
			}
		}
		else
			stemp+="$";
	/////////////////////wtt////3.2
//	if(p->T.key==CN_VARIABLE&&p->T.addr>=0&&p->T.addr<SArry.GetSize()&&SArry[p->T.addr].kind==CN_ARRAY||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)
//	if(p->T.key==CN_VARIABLE&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_POINTER)||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)// WTT 2008 0318
	//	if(p->T.key==CN_VARIABLE&&(p->T.addr>=0&&p->T.addr<SArry.GetSize()&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_ARRPTR||SArry[p->T.addr].kind==CN_POINTER))||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)// WTT 2008 0318
		if(p->T.key==CN_VARIABLE&&p->info>0&&(p->T.addr>=0&&p->T.addr<SArry.GetSize()&&(SArry[p->T.addr].kind==CN_ARRAY||SArry[p->T.addr].kind==CN_ARRPTR||SArry[p->T.addr].kind==CN_POINTER))||p->T.key==CN_DFUNCTION||p->T.key==CN_BFUNCTION)// WTT 2008 0322

	{
			int i=0;
			CString str;
				if(p->T.key==CN_VARIABLE)
				str="";
			else
				str="(";
		while(i<10&&p->pinfo[i]!=NULL)
		{
			if(p->T.key==CN_VARIABLE)
				str+="[";
			
			str+=GetExpString(p->pinfo[i],SArry);

				if(p->T.key==CN_VARIABLE)
				str+="]";
			else if(i<p->info-1) 
				str+=",";

			i++;

		}

		if(p->T.key==CN_VARIABLE)
				str+="";
		else
		{
		//	str.SetAt(str.GetLength()-1,' ');
			str+=")";
		}
		stemp+=str;
		}
	
			
	return stemp;

}

////////////wtt////////////////
///////////////////////////////////////////////////////////////////

//void CVarRemove::ResetStatementOrder(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
//{
	/***********************************************************************
		FUNCTION:
		Rearrange the statements order in SDG under the condition that the 
		program semantic is not changed.
		PARAMETER:
		as above.
	***********************************************************************/

/*	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				// Add your code here
				if(p->GetRelateNum()>=2)
				{
					ResetChildrenOrder(p,SArry);
				}				
				// end of this part

				p=NULL;
			}
		}
    }
	
}*/

//void CVarRemove::ResetChildrenOrder(CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
//{
	/***********************************************************************
		FUNCTION:
		Rearrange the sub-statements order of pNode under the condition that
		the program semantic is not changed.
		PARAMETER:
		as above.
	***********************************************************************/
/*
	int ix=0;
	int beg=0,end=0;
	while(ix<pNode->GetRelateNum())
	{
		beg=ix;
		end=ix;
		while(ix<pNode->GetRelateNum() &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGBREAK")==NULL &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGRETURN")==NULL &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGENTRY")==NULL &&
			SearchFirstCertainNode(pNode->GetNode(ix),"SDGCONTINUE")==NULL)
		{
			ix++;
		}
		
		if(ix>=end+2)
		{
			end=ix-1;
			ResetRangeOrder(beg,end,pNode,SArry);
		}
		
		ix++;
	}

}*/

//void CVarRemove::ResetRangeOrder(const int bpos,const int epos,CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
//{
	/***********************************************************************
		FUNCTION:
		Rearrange the sub-statements(the index form bpos to epos) order of 
		pNode under the condition that the program semantic is not changed.
		PARAMETER:
		as above.
	***********************************************************************/	

/*	if(bpos>=epos || pNode->GetRelateNum()<=epos)
	{
		return;
	}

	int nbound=bpos;
	CSDGBase *pTemp=NULL;
    
	//TRACE("Pre Change\n");
	//for(int n=bpos;n<=epos;n++)
	//{		
	//	TRACE(pNode->GetNode(n)->m_sName+"\n");
	//}
	
    
	for(int ix=bpos+1;ix<=epos;ix++)
	{
		for(int i=ix-1;i>=bpos;i--)
		{
			nbound=i;
			if(IsDepended(pNode->GetNode(i),pNode->GetNode(ix)))
			{
				nbound=i+1; 
				break;
			}
			
		}

		if(nbound<ix)
		{
			for(i=nbound;i<ix;i++)
			{
				if(pNode->GetNode(ix)->m_sName<pNode->GetNode(i)->m_sName)
				{   
					pTemp=pNode->GetNode(ix);
					pNode->InsertNode(i,pTemp);
					pNode->Del(ix+1);
					break;
				}
			}
		}
	}

     
	/*
	for(int ix=bpos;ix<epos;ix++)
	{
		MaxNoDependentSet(bound,ix,epos,pNode,SArry,FArry);
		
		if(bound>ix)
		{
			for(int i=bound;i>ix;i--)
			{
				if(pNode->GetNode(i)->m_sName<pNode->GetNode(i-1)->m_sName)
				{   
					pTemp=pNode->GetNode(i);
					pNode->InsertNode(i-1,pTemp);
					pTemp=pNode->GetNode(i);
					pNode->Del(i);
					pNode->InsertNode(i,pTemp);
					pNode->Del(i+1);

					
				}
			}
		}
	}
    */

	//TRACE("After Change\n");
	//for(n=bpos;n<=epos;n++)
	//{		
	//	TRACE(pNode->GetNode(n)->m_sName+"\n");
	//}

//}

/*void CVarRemove::MaxNoDependentSet(int &ibound,const int bpos,const int epos,CSDGBase *pNode,CArray<SNODE,SNODE> &SArry)
{
	/***********************************************************************
		FUNCTION:
		Find the maximized indenpendent statements set.
		PARAMETER:
		as above.
	***********************************************************************/	

/*	if(bpos>=epos || pNode->GetRelateNum()<=epos)
	{
		ibound=bpos;
		return;
	}

	ibound=bpos;

	
	for(int ix=bpos+1;ix<=epos;ix++)
	{
		if(IsDepended(pNode->GetNode(bpos),pNode->GetNode(ix)))
		{
			ibound=ix-1;
			break;
		}	
	}

	if(ibound<=bpos)
	{
		ibound=bpos;
		return;
	}

	int  tpos=ibound;
	bool bdep=false;
	while(tpos>bpos)
	{
		bdep=false;
		for(ix=tpos-1;ix>=bpos;ix--)
		{
			if(IsDepended(pNode->GetNode(ix),pNode->GetNode(ibound)))
			{
				bdep=true;
				ibound=ix-1;
				break;
			}
		}

		if(bdep)
		{
			tpos=ibound;
		}
		else
		{
			tpos--;
		}
	}

	if(ibound<=bpos)
	{
		ibound=bpos;		
	}	

}*/

bool IsDepended(CSDGBase *pNode1,CSDGBase *pNode2)
{
	CVarRemove Var;
	return Var.IsDepended(pNode1,pNode2);
}

bool CVarRemove::IsDepended(CSDGBase *pNode1,CSDGBase *pNode2)
{
	/***********************************************************************
		FUNCTION:
		Analyze if the two node have data dependent relation.
		PARAMETER:
		as above.
	***********************************************************************/

	bool rvalue=false;
	if(pNode1==NULL||pNode2==NULL)
		return false;
	for(int i=0;i<pNode1->m_aDefine.GetSize();i++)
	{
		if(pNode1->m_aDefine[i].key==CN_VARIABLE)
		{
			for(int j=0;j<pNode2->m_aRefer.GetSize();j++)
			{
				if(pNode2->m_aRefer[j].key==CN_VARIABLE && 
				   (pNode1->m_aDefine[i].name==pNode2->m_aRefer[j].name || 
				    pNode1->m_aDefine[i].addr==pNode2->m_aRefer[j].addr))
				{
					return true;
				}
			}
		}
	}

	for(int i=0;i<pNode2->m_aDefine.GetSize();i++)
	{
		if(pNode2->m_aDefine[i].key==CN_VARIABLE)
		{
			for(int j=0;j<pNode1->m_aRefer.GetSize();j++)
			{
				if(pNode1->m_aRefer[j].key==CN_VARIABLE && 
				   (pNode2->m_aDefine[i].name==pNode1->m_aRefer[j].name || 
				    pNode2->m_aDefine[i].addr==pNode1->m_aRefer[j].addr))
				{
					return true;
				}
			}
		}
	}

	return rvalue;
}

//CSDGBase *CVarRemove::SearchFirstCertainNode(CSDGBase *pSDG,CString strNode)
//{
	/******************************************************

	    FUNCTION:
		Pre-order search node with the type specified by 
		strNode in pSDG.

		(此函数调用时必须确保各节点的visit值为零)
	    
		PARAMETER:
		....		
	******************************************************/
/*	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p,*pTemp=NULL;
	bool bFind=false;

	p=pSDG;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			if(!bFind)
			{
				if(p->m_sType==strNode)
				{
					pTemp=p;
					bFind=true;
				}
			}
			
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			if(p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	return pTemp;
}
*/

void CVarRemove::ScanfPrintfDivide(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry)
{
	/**********************************************************
	    FUNCTION:
		post-order tranverse the SDG ,find the node scanf and
		divide its parameters if necessary.
		example:

		scanf("%d,%d",&i,&j);
		=
		scanf("%d",&i);
		scanf("%d",&j);

		PARAMETER:
		....	
		wtt增加了printf的划分同scanf
			printf("%d,%d%d",i,j,k);
		=
		printf("%d",i);
		printf("%d",j);
		printf("%d",k);

	**********************************************************/
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p=NULL;

	p=pHead;
    while(p || S.GetCount()>0)
    {
		if(p)
		{
			S.AddTail(p);
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());			
			if(p&&p->visit==p->GetRelateNum())
			{
				S.RemoveTail();
			}
			
			if(p&&p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);			
			}
			else if(p)
			{
				p->visit=0;

				//////////////////////////////////////////////
				// Add your code here/////////////////////////
				for(int ix=p->GetRelateNum()-1;ix>=0;ix--)
				{
					//	if(p->GetNode(ix)->m_sType=="SDGCALL" && p->GetNode(ix)->m_pinfo &&  
				    //   p->GetNode(ix)->m_pinfo->T.name=="scanf" && p->GetNode(ix)->m_pinfo->info>=2)
					if(p->GetNode(ix)&&p->GetNode(ix)->m_sType=="SDGCALL" && p->GetNode(ix)->m_pinfo &&  
				      ( p->GetNode(ix)->m_pinfo->T.name=="scanf"||p->GetNode(ix)->m_pinfo->T.name=="printf") && p->GetNode(ix)->m_pinfo&&p->GetNode(ix)->m_pinfo->info>=2)//wtt//3.9///////
					{
						int ntemp=p->GetNode(ix)->m_pinfo->info;
						CString stemp="";
						if(p->GetNode(ix)->m_pinfo->pinfo[0])
						stemp=p->GetNode(ix)->m_pinfo->pinfo[0]->T.name;
					
						for(int i=ntemp-1;i>1&&i<10;i--)
						{
							CSDGBase *pNew=new CSDGBase;
							pNew->important=p->GetNode(ix)->important; 
							pNew->m_sType="SDGCALL";
							pNew->m_pinfo=new ENODE;
							InitialENODE(pNew->m_pinfo);
							pNew->m_pinfo->info=2;
							
							pNew->m_pinfo->T.addr=p->GetNode(ix)->m_pinfo->T.addr;
							pNew->m_pinfo->T.key=CN_BFUNCTION;
							pNew->m_pinfo->T.line=p->GetNode(ix)->m_pinfo->T.line;
							pNew->m_pinfo->T.name=p->GetNode(ix)->m_pinfo->T.name;
							pNew->m_pinfo->T.paddr=p->GetNode(ix)->m_pinfo->T.paddr;
							pNew->m_pinfo->T.deref=p->GetNode(ix)->m_pinfo->T.deref;

							pNew->m_pinfo->pinfo[0]=new ENODE;
							InitialENODE(pNew->m_pinfo->pinfo[0]);
							pNew->m_pinfo->pinfo[0]->T.key=CN_CSTRING;
							
							// begin: 04051210 modify(ADD) 
							int j=stemp.GetLength()-1;
							while(j>=0 && stemp.GetAt(j)!='%') j--;
							if(j>=0)
							{
							
								if( p->GetNode(ix)->m_pinfo->T.name=="scanf")//忽略输入修饰符%*c的识别
								{
								//	AfxMessageBox("scanf");
									int nposnext=j-1;

									while(nposnext>=0 && stemp.GetAt(nposnext)!='%')
									{nposnext--;
								//	AfxMessageBox("nposnext--");
									}

								//	CString sint;
								//	sint.Format("nposnext=%d",nposnext);
								//	AfxMessageBox(sint);

									if(nposnext>=0&&nposnext+1<j&&stemp.GetAt(nposnext+1)=='*')
									{//AfxMessageBox("ok");
										j=nposnext;
									}
								}

							
						     
								 CString str;
								 str=stemp.Mid(j) ;
								 int j=str.GetLength()-1;/////wtt///2005//9.1//去掉提示字符串
								 while(j>0)
								 {
									 if(!(str[j]>='a'&&str[j]<='z'||str[j]>='A'&&str[j]<='Z'||str[j]>='1'&&str[j]<='9'||str[j]==':'||str[j]=='='||str[j]==' '))
										 break;
									 else
										 str.Delete(j);
									 j--;
								 }
							
								// AfxMessageBox(stemp);
							//	 AfxMessageBox(str);

								 int npos=stemp.GetLength()-1;
								while(npos>=j) {stemp.Delete(npos);npos--;}
								pNew->m_pinfo->pinfo[0]->T.name=str;
							//	AfxMessageBox(str);////////////////
							}
							else
							{
								pNew->m_pinfo->pinfo[0]->T.name="%d";
							}
							// end: 04051210 modify(ADD) 

							pNew->m_pinfo->pinfo[1]=p->GetNode(ix)->m_pinfo->pinfo[i];
							p->GetNode(ix)->m_pinfo->pinfo[i]=NULL;
							pNew->SetFather(p); 
							if(ix+1>=p->GetRelateNum())
							{
								p->Add(CN_CONTROL,pNew); 
							}
							else
							{
								p->InsertNode(ix+1,pNew);
							}
						}

						p->GetNode(ix)->m_pinfo->info=2;

						// begin: 04051210 modify(ADD)
						int npos=stemp.GetLength()-1;
						while(npos>=0 && stemp.GetAt(npos)!='%') npos--;
						CString str("%d");



					
					
						if(npos>=0)
						{
								
							//	str=stemp.Mid(npos) ;//wtt//2005//8.28
							int j=0;
							while(j<npos)//wtt//2005//9.1//
							{
								if(!(stemp[j]>=65&&stemp[j]<=100||stemp[j]>=97&&stemp[j]<=122||stemp[j]>='1'&&stemp[j]<='9'||stemp[j]==' '||stemp[j]=='='||stemp[j]==':'))
								{
									break;
								}
								j++;
							}
							if(j!=npos)//去掉字符串提示信息
							str=stemp;//wtt//2005//8.28//第一个%前的字符写入%前面//
						    else 
								str=stemp.Mid(npos) ;
						//	int npos=stemp.GetLength()-1;							
						}  
						// end: 04051210 modify(ADD)

						p->GetNode(ix)->m_pinfo->pinfo[0]->T.name=str;
					//	AfxMessageBox(str);////////////////
						
					}
				}
				// end of this part///////////////////////////
				//////////////////////////////////////////////

				p=NULL;
			}
		}
    }
}


/////////////////////////////////////////////////////////////////////////
////////////////////////////other functions//////////////////////////////
/////////////////////////////////////////////////////////////////////////

/*CString GetExpString(ENODE *pEHead,bool bNIDX)
{
	CVarRemove varremove;
	return varremove.GetExpString(pEHead,bNIDX); 
}*/

/*CString CVarRemove::GetExpString(ENODE *pEHead,bool bNIDX)
{
	/***********************************************************************
		FUNCTION:
		return the syntax tree's string
		PARAMETER:
		pEHead : head pointer of tree
		
	***********************************************************************/
	
/*	CList<ENODE *,ENODE *> STK;
	CList<int,int>         SBL; 
	ENODE *p=pEHead;
	CString str("");
	
    /////////////////////////////
	
	p=pEHead;
   	while(STK.GetCount()>0 || p)
	{
		if(p)
		{
			STK.AddTail(p);
			p=STK.GetTail()->pleft;  
		}
		else
		{
			// beg
			str+=GetENodeStr(STK.GetTail());			
			if( STK.GetTail()->info>=1 && bNIDX==true &&
			    (STK.GetTail()->T.key==CN_VARIABLE||STK.GetTail()->T.key==CN_BFUNCTION || STK.GetTail()->T.key==CN_DFUNCTION))
			{
				if(STK.GetTail()->T.key==CN_VARIABLE)////wtt////3.9//
					str+="[";
				else
					str+="(";
				for(int i=0;STK.GetTail()->pinfo[i] && i<STK.GetTail()->info && i<10;i++)
				{
					str+=GetExpString(STK.GetTail()->pinfo[i])+",";
				}
				str.Delete(str.GetLength()-1);
				if(STK.GetTail()->T.key==CN_VARIABLE)///wtt//3.9//
					str+="]";
				else
					str+=")";
			}
			// end

			p=STK.GetTail()->pright;
			STK.RemoveTail();			
		}
	}

	return str;
}*/

/*CString CVarRemove::GetENodeStr(ENODE *pENode)
{
	/***********************************************************************
		FUNCTION:
		return the ENODE's string
		PARAMETER:
		pENode : pointer of pENode;
		
	***********************************************************************/
	
/*	CString str(""),s("");
	if(pENode==NULL)
	{
		return "";
	}

	switch(pENode->T.key)		
	{							 	
	case CN_CFLOAT:
		s.Format("%f",pENode->T.value);
		str+=s;
		break;
	case CN_CINT:
	case CN_CCHAR:
		s.Format("%d",pENode->T.addr);
		str+=s;
		break;
	case CN_ADD:
		str+="+ ";
		break;
	case CN_SUB:
		str+="-";
		break;
	case CN_MULTI:
		str+="*";
		break;
	case CN_DIV:
		str+="/";
		break;
	case CN_FORMAT:
		str+="%";
		break;
	case CN_GREATT:       // >
		str+=">";
		break;
	case CN_LESS:         // <
		str+="<";
		break;
	case CN_LANDE:        // <= 
		str+="<=";
		break;
	case CN_BANDE:        // >=
		str+=">=";
		break;
	case CN_NOTEQUAL:     // !=
		str+="!=";
		break;
	case CN_DEQUAL:       // == 
		str+="==";
		break;
	case CN_DAND:
		str+="&&";
		break;
	case CN_DOR:
		str+="||";
		break;
	case CN_NOT:
		str+="!";
		break;
	case CN_DFUNCTION:
	case CN_BFUNCTION:
	case CN_CSTRING:
		s.Format("%s",pENode->T.name); 
		str+=s;
		break;
	case CN_VARIABLE:
        s.Format("%s",pENode->T.name); 	
		str+=s;
	default:
		str+=' ';
	}	

	return str;
}
*/

/*void CVarRemove::PreOrder(CSDGBase *pHead)
{
	// F:先根搜索SDG图

	CFile f("data\\SDGtemp.dat",CFile::modeWrite|CFile::modeCreate);
	CString s,st;
    
	CList<CSDGBase *,CSDGBase *> S;
    CSDGBase *p;
    p=pHead;
    

    while(p!=NULL || S.GetCount()>0)
    {
		if(p!=NULL)
		{
						
			S.AddTail(p); 
	        if(p->visit<p->GetRelateNum())
			{
				p->visit++;
				p=p->GetNode(p->visit-1);
			}
			else
			{
				p=NULL; 
			}
		}
		else
		{
			p=S.GetAt(S.GetTailPosition());
			
			// visit node
			if(p->visit==p->GetRelateNum())
			{
				
				s.Format("%2d: %15s v=%3d rnum=%3d (%3d,%3d)",p->idno,p->m_sType,p->GetNodeVL(),p->GetRelateNum(),p->bodybeg,p->bodyend);
				st=_T("");

				
				if(p->GetFather())
				{
					st.Format(" f=%2d ",p->GetFather()->idno);
				}
				s+=st;			
				
                s+=" Dfd:";
				for(int i=0;i<p->m_aDefine.GetSize();i++)
				{
					s+="["+p->m_aDefine[i].name+"]";
				}
				s+=" Rfd:";
				for(i=0;i<p->m_aRefer.GetSize();i++)
				{
					s+="["+p->m_aRefer[i].name+"]";
				}              
          
				f.Write(s,s.GetLength());
				f.Write("\r\n",1);
				
				S.RemoveTail();
			}
			// end
			   			
			if(p->visit<p->GetRelateNum())
			{		
				p->visit++;
				p=p->GetNode(p->visit-1);				
			}
			else
			{
				p->visit=0;
				p=NULL;
			}
		}
    }

	f.Close();
}*/

//////////////////////////////////////////////////////////////////////////
//////////////////////////2004.04.20.20.48////////////////////////////////
//////////////////////////////////////////////////////////////////////////
void CVarRemove::TrueInSelection(CSDGBase*p)
{///////////wtt///3.9////将含有永真分支的选择语句进行化简:如If (1) A else B ---〉A或If (!0) A else B ---〉A

	if(p==NULL)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		TrueInSelection(p->GetNode(i));
	}

	if(p->m_sType!="SELECTION")
		return;
	int i;
	for( i=0;i<p->GetRelateNum();i++)
	{
		CSDGBase*pb=p->GetNode(i);
		if(pb&&pb->m_pinfo&&pb->m_pinfo->T.key==CN_CINT&&pb->m_pinfo->T.addr>0)
		{
			
			break;
		}
		if(pb&&pb->m_pinfo&&pb->m_pinfo->T.key==CN_NOT&&pb->m_pinfo->pleft&&pb->m_pinfo->pright==NULL&&pb->m_pinfo->pleft->T.key==CN_CINT&&pb->m_pinfo->pleft->T.addr==0)
		{
		
			break;
		}
	}
	if(i==p->GetRelateNum())
	return;
	CSDGBase*pf=NULL;
	pf=p->GetFather();
    int index;
	index=FindIndex(p);
	pf->Del(index); 
	for(int j=0;p->GetNode(i)&&j<p->GetNode(i)->GetRelateNum();j++)
	{
		if(p->GetNode(i)->GetNode(j))
		{p->GetNode(i)->GetNode(j)->SetFather(pf);
		pf->InsertNode(index+j,p->GetNode(i)->GetNode(j));
		}

	}
	if(p->GetNode(i))
	p->GetNode(i)->RemoveAll();
	DeleteBranch(p);
	p=NULL;
	modified=true;
}

//////////////////////////////////
/*void CVarRemove::FalseInSelection(CSDGBase*p)
{///////////wtt///3.12////将选择语句的永假分支删除:如If (0) A else B ---〉B或If (!1) A else B ---〉B
//不要了因为TrueInSelection已经实现了此功能
  if(p==NULL)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		TrueInSelection(p->GetNode(i));
	}

	if(p->m_sType!="SELECTION")
		return;
	for(i=0;i<p->GetRelateNum();i++)
	{
		CSDGBase*pb=p->GetNode(i);
		if(pb->m_pinfo&&pb->m_pinfo->T.key==CN_CINT&&pb->m_pinfo->T.addr==0)
		{
			
			break;
		}
		if(pb->m_pinfo&&pb->m_pinfo->T.key==CN_NOT&&pb->m_pinfo->pleft&&pb->m_pinfo->pright==NULL&&pb->m_pinfo->pleft->T.key==CN_CINT&&pb->m_pinfo->pleft->T.addr>0)
		{
		
			break;
		}
	}
	if(i==p->GetRelateNum())
	return;
	p->Del(i);	
	DeleteBranch(p->GetNode(i));
	modified=true;
}*/


/////////////////////////////////
void CVarRemove::SubstractCommenBranch(CSDGBase*p,CArray<SNODE,SNODE> &SArry)
{//wtt编写 ：提取选择各分支语句中的公共的分支,如If(p){TC;A;}else{FC;A；}--> If(p)TC;else FC; A;

	if(p==NULL)
	{
		return ;
	}
	for(int i=0;i<p->GetRelateNum();i++)
	{
		SubstractCommenBranch(p->GetNode(i),SArry);
		
	}
	if(p->m_sType!="SELECTION")
		return;
	if(p->GetRelateNum()==1)
		return;
	//AfxMessageBox("SELECTION");////
	int min=32767,minNo=32767;
	int i;
	for( i=0;i<p->GetRelateNum();i++)
	{
		if(p->GetNode(i)&&p->GetNode(i)->GetRelateNum()<min)
		{
			min=p->GetNode(i)->GetRelateNum();
			minNo=i;
			////////////////////////
		/*	CString str;
			str.Format("min=%d,minNo=%d",min,minNo);
			AfxMessageBox(str);*/
			/////////////////////////////
		}

	}
	if(minNo==32767)
		return;
	CSDGBase*pmin=p->GetNode(minNo);
	int sameNum=0;
	i=min-1;
	int k=1;
	bool different=false;
	while(i>=0&&!different)
	{   
		different=false;
		for(int j=0;j<p->GetRelateNum();j++)
		{
			CSDGBase*pCur=p->GetNode(j);
			if(pmin&&pmin->GetNode(i)==NULL&&pCur&&pCur->GetNode(i)==NULL)
				return;
				///////////wt///////6.2/////////////////printf语句暂不提出///////////
			if(pmin->GetNode(i)->m_sType=="SDGCALL"&&pmin->GetNode(i)->m_pinfo&&pmin->GetNode(i)->m_pinfo->T.name=="printf" )
			{
				different=true;
		
				break;
			}
	/////////////////wtt///////////////////
			if(!CompareBranches(pmin->GetNode(i),pCur->GetNode(pCur->GetRelateNum()-k),SArry))
			{
			//	AfxMessageBox("!CompareBranches");///////////////////////
				different=true;
				break;
			}
		}
		if(!different)
		sameNum++;
	/*	CString str;
			str.Format("sameNum=%d",sameNum);
			AfxMessageBox(str);*/
		
		k++;

		i--;
	}
	if(sameNum==0)
		return;
	/////////////////////
/*	CString str;
	str.Format("%d",sameNum);
	AfxMessageBox(str);*/
	////////////////////////
	
	CSDGBase*pf=p->GetFather();
	if(!pf)
	{
	//	AfxMessageBox("pf==NULL");
		return;
	}
//	AfxMessageBox("pf!=NULL start change");

	int index=FindIndex(p);

	for(i=1;i<=sameNum;i++)
	{
		if(pmin->GetNode(min-i))
		{
		pmin->GetNode(min-i)->SetFather(pf);
		pf->InsertNode(index+1,pmin->GetNode(min-i)) ;
		if(min-i>=0&&min-i<pmin->GetRelateNum())//5.10//
		pmin->Del(min-i);
		}
		

	}
	for(i=0;i<p->GetRelateNum();i++ )
	{
		if(i!=minNo)
		{
			for(int j=1;j<=sameNum;j++)///error
			{
				int k;
				k=p->GetNode(i)->GetRelateNum();
				if(p->GetNode(i))
				{
					
			
			//	AfxMessageBox("delete branch"+p->GetNode(i)->GetNode(k-j)->m_sType);///
				DeleteBranch(p->GetNode(i)->GetNode(k-j));
				
				if(k-j>=0&&k-j<	p->GetNode(i)->GetRelateNum())//5.10//
					p->GetNode(i)->Del(k-j);
				}

			}
		}
	}
//	AfxMessageBox("substract common branch");
	modified=true;
}
bool CVarRemove::CompareBranches(CSDGBase*p1,CSDGBase*p2,CArray<SNODE,SNODE> &SArry)
{//wtt编写 ：先根序遍历USDG比较p1和p2，若相同返回true，否则返回false
	if(p1==NULL&&p2!=NULL||p1!=NULL&&p2==NULL)
	{
	//	AfxMessageBox("p1==NULL&&p2!=NULL||p1!=NULL&&p2==NULL");/////
		return false;
	}
	if(p1==NULL&&p2==NULL)
		return true;
	if(p1->m_sType!=p2->m_sType)
	{
	//	AfxMessageBox("p1->m_sType!=p2->m_sType"+p1->m_sType+"&&"+p2->m_sType);////
		return false;
	}
	if(p1->GetRelateNum()!=p2->GetRelateNum())
	{
	//	AfxMessageBox("p1->GetRelateNum()!=p2->GetRelateNum()");////

		return false;
	}
	
	if(!CompareTrees(p1->m_pinfo,p2->m_pinfo,SArry))
	{
	//	AfxMessageBox("!CompareTrees(p1->m_pinfo,p2->m_pinfo,SArry)");////
		
		return false;
	}

	for(int i=0;i<p1->GetRelateNum();i++)
	{
		if(!CompareBranches(p1->GetNode(i),p2->GetNode(i),SArry))
		{
		//	AfxMessageBox("!CompareBranches(p1->GetNode(i),p2->GetNode(i),SArry)");////

			return false;
		}
	}
	//////////////////wtt  2008.3.28//////////比较函数调用的实参/////////
	if(p1->m_sType=="SDGCALL")
	{
		if(p1->m_pinfo&&p2->m_pinfo&&p1->m_pinfo->info!=p2->m_pinfo->info)
			return false;
		for(int k=0;k<p1->m_pinfo->info;k++)
		{
			if(!CompareTrees(p1->m_pinfo->pinfo[k],p2->m_pinfo->pinfo[k],SArry))
				return false;
		}
	}
	/////////////////wtt  2008.3.28////////////////////
	return true;
	

}
/*bool CVarRemove::CompareTrees(ENODE*pHead1,ENODE*pHead2,CArray<SNODE,SNODE> &SArry)
{//wtt编写 ：先根序遍历语法树比较pHead1和pHead2，若相同返回true，否则返回false
	if(!pHead1&&!pHead2)
		return true;
	else if(!pHead1||!pHead2)
	{
	//	AfxMessageBox("!pHead1||!pHead2");
		return false;
	}
	if(pHead1->T.key<0||pHead1->T.key>=132 )
	{
	//	AfxMessageBox("pHead1->T.key<0||pHead1->T.key>=132");
		return false;
	}
	if(pHead2->T.key<0||pHead2->T.key>=132 )
	{
	//	AfxMessageBox("pHead2->T.key<0||pHead2->T.key>=132");
		return false;
	}
	if(pHead1->T.key !=pHead2->T.key)
	{
	//	AfxMessageBox("pHead1->T.key !=pHead2->T.key");
		return false;
	}
	switch(pHead1->T.key )
	{
		case CN_CINT:
		case CN_CCHAR:
			if(pHead1->T.addr!=pHead2->T.addr )
			{
			//	AfxMessageBox("pHead1->T.addr!=pHead2->T.addr");
				return false;
			}
				break;
		case CN_CFLOAT:
		case CN_DOUBLE:
		case CN_LONG:
			if(pHead1->T.value!=pHead2->T.value )
				return false;
				break;
		case CN_VARIABLE:
		case CN_CSTRING:
		case CN_DFUNCTION:
		case CN_BFUNCTION:
			if(pHead1->T.name!=pHead2->T.name )
			{
			//	AfxMessageBox("pHead1->T.name!=pHead2->T.name"+pHead1->T.name+"&&"+pHead2->T.name );

			return false;
			}
			break;
	}
	if(pHead1->T.key==CN_VARIABLE||pHead1->T.key==CN_DFUNCTION)
	{
		if(pHead1->T.addr!=pHead2->T.addr)
		{
			//	AfxMessageBox("pHead1->T.addr!=pHead2->T.addr");

			return false;
		}
		if(SArry[pHead1->T.addr].kind==CN_ARRAY||SArry[pHead1->T.addr].kind==CN_DFUNCTION)
		{
			int i=0;
			while(i<10&&pHead1->pinfo[i]!=NULL)
			{
				if(!CompareTrees(pHead1->pinfo[i],pHead2->pinfo[i],SArry))
					return false;
				i++;

				}
		}
	}
	if(pHead1->T.key==CN_BFUNCTION)
	{

	
		int i=0;
		while(i<10&&pHead1->pinfo[i]!=NULL)
		{
			if(!CompareTrees(pHead1->pinfo[i],pHead2->pinfo[i],SArry))
				return false;
				i++;

		}

	}
	if(!CompareTrees(pHead1->pleft ,pHead2->pleft ,SArry))
	{
	//	AfxMessageBox("!CompareTrees(pHead1->pleft ,pHead2->pleft ,SArry)");
		return false;
	}
	if(!CompareTrees(pHead1->pright ,pHead2->pright ,SArry))
	{
	//	AfxMessageBox("!CompareTrees(pHead1->pright ,pHead2->pright ,SArry)");

		return false;
	}
	
	return true;
}*/

/*void CVarRemove::RearrangeSelector(CSDGBase*p)
{//wtt3.10编的，不要了没有必要重排各selector的顺序

	if(p)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		RearrangeSelector(p->GetNode(i));
	}

	if(p->m_sType!="SELECTION")
		return;
	CArray<CSDGBase*,CSDGBase*>PointerList;
	CArray<CString,CString>ExpList;
	CArray<int,int>numList;
	PointerList.RemoveAll() ;
	ExpList.RemoveAll() ;
	for( i=0;i<p->GetRelateNum();i++ )
	{
		ExpList.Add(GetExpString(p->GetNode(i)->m_pinfo));
		numList.Add(i);
	}
	int minNum,ntemp;
	CString minString,Stemp;
	for(i=0;i<ExpList.GetSize();i++ )
	{
		minNum=i;
		minString=ExpList[i];
		for(int j=i+1;j<ExpList.GetSize();j++)
		{
			if(ExpList[j]<ExpList[minNum])
			{
				minNum=j;
				minString=ExpList[j];
			}

		}
		if(minNum!=i)
		{
			Stemp=ExpList[minNum];
			ExpList[minNum]=ExpList[i];
			ExpList[i]=Stemp;

			ntemp=numList[minNum];
			numList[minNum]=numList[i];
			numList[i]=ntemp;

		}
		PointerList.Add(p->GetNode(numList[minNum])) ;
	}
	p->RemoveAll();
	for(i=0;i<PointerList.GetSize();i++ )
	{
		p->InsertNode(i,PointerList[i]);
		PointerList[i]->SetFather(p); 
	}
}*/
void CVarRemove::CombineSelection(CSDGBase*p,CArray<SNODE,SNODE> &SArry)
{//wtt编写 3.10 后根序遍历USDG,合并相邻的selection:如果有两个if –else语句或switch语句并且一个紧跟在另一个后面，并且它们的谓词的值相等，那么第二个语句的分支的代码可以移到另一个语句的适当的位置上。从而消除第二个测试
	if(p==NULL)
	{
		return ;
	}
	for(int j=0;j<p->GetRelateNum();j++)
	{
		CombineSelection(p->GetNode(j),SArry);
		
	}
	if(p->m_sType!="SELECTION")
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int i=FindIndex(p);
	int index=i;
	if(i<0||i>=pf->GetRelateNum()-1)
	{
		return;
	}

  int 	j=i+1;
	int no=0;
	CSDGBase * psbl=NULL;
	int flag=1;
	while(flag&&j<pf->GetRelateNum())
	{
		psbl=pf->GetNode(j);
		if(!psbl||psbl->m_sType!="SELECTION")
		break;	
		if(p->GetRelateNum()!=psbl->GetRelateNum())
			break;
		for(i=0;i<p->GetRelateNum();i++)
		{
			if(!CompareTrees(p->GetNode(i)->m_pinfo,psbl->GetNode(i)->m_pinfo,SArry))
			{
				flag=0;
				break;
			}
		}
		if(flag)
			no++;
		j++;

	}
	if(no==0)
		return;
	int k;
	for( k=1;k<=no;k++)
	{
		psbl=pf->GetNode(index+k);
		for(i=0;i<p->GetRelateNum();i++)
		{
			CSDGBase*pch=p->GetNode(i);
			CSDGBase*psblch=psbl->GetNode(i);
			for(j=0;pch&&psblch&&j<psblch->GetRelateNum();j++)
			{
				int num=pch->GetRelateNum();
				pch->InsertNode(num+j,psblch->GetNode(j));
				psblch->GetNode(j)->SetFather(pch);
			}
			psblch->RemoveAll();
		}
	}
	for(k=no;k>=1;k--)
	{
	
		DeleteBranch(pf->GetNode(index+k));
	
		pf->Del(index+k);
	}
	modified=true;

}


void CVarRemove::MergeSelection(CSDGBase*p,CArray<SNODE,SNODE> &SArry)
{//wtt编写:////3.11//将相邻的多个控制变量相同的if语句进行合并
	if(p==NULL)
	{
		return ;
	}
	for(int j=0;j<p->GetRelateNum();j++)
	{
		MergeSelection(p->GetNode(j),SArry);
		
	}
	if(p->m_sType!="SELECTION")
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int i=FindIndex(p);
	int index=i;
	if(i<0||i>=pf->GetRelateNum()-1)
	{
		return;
	}
	int j=i+1;
	int num=0;
	while(j<pf->GetRelateNum())
	{
		CSDGBase * psbl=pf->GetNode(j);	
		if(!psbl||psbl->m_sType!="SELECTION")
			break;
		if(p->GetRelateNum()!=1||psbl->GetRelateNum()!=1)
			break;
		ENODE*p1=NULL,*p2=NULL;
		if(p->GetNode(0))
		p1=p->GetNode(0)->m_pinfo;
		if(psbl->GetNode(0))
		p2=psbl->GetNode(0)->m_pinfo;
		if(p1==NULL||p2==NULL)
		break;
	
		if(!(CompareTrees(p1->pleft,p2->pleft,SArry)||CompareTrees(p1->pleft,p2->pright,SArry)||CompareTrees(p1->pright,p2->pleft,SArry)||CompareTrees(p1->pright,p2->pright,SArry)))
		{
			break;
		}
	  
		num++;
		j++;
	}
	//////////////////////////
/*	CString stemp;
	stemp.Format("num=%d",num);
	AfxMessageBox(stemp);*/
	//////////////////////
	if(num==0)
		return;
	for(i=num;i>=1;i--)
	{
		CSDGBase*psel=pf->GetNode(index+i);
		//AfxMessageBox(pf->GetNode(index+i)->m_sType+GetExpString(pf->GetNode(index+i)->m_pinfo,SArry));
		if(psel)
		{
			p->InsertNode(1,psel->GetNode(0));
		//	AfxMessageBox(p->GetNode(0)->m_sType+GetExpString(p->GetNode(0)->m_pinfo,SArry));

		//	AfxMessageBox(p->GetNode(1)->m_sType+GetExpString(p->GetNode(1)->m_pinfo,SArry));

			if(psel->GetNode(0))
			{
				psel->GetNode(0)->SetFather(p);
				pf->Del(index+i);
				psel->Del(0);
			//	  DeleteBranch(psel);
				psel=NULL;
		}
		}

	}
	modified=true;
}

////////////////////////////////////wtt5.15//////////////////
/*void CVarRemove::SameConditionVar(ENODE*p1,ENODE*p2)
{

}*/
///////////////////////////////////////////////
///////////////////////////////////////////
void CVarRemove::ChangeIteration(CSDGBase*p)
{//wtt编写 //3.10后根序遍历USDG,将Iteration进行转换去掉ITRSUB和ESPSUB.ITRSUB的语句直接作为ITERATION的子节点。do while 语句将其ESPSUB中的语句置于ITERATION的左兄弟，
	if(p==NULL)
	{
		return ;
	}
	for(int j=0;j<p->GetRelateNum();j++)
	{
		ChangeIteration(p->GetNode(j));
		
	}
	if(p->m_sType!="ITERATION")
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int index=FindIndex(p);
	CSDGBase *pESP=NULL,*pITR=NULL;
	CIterationUnit* pUnit=(CIterationUnit*)p;
	
	if(p->GetRelateNum()==2&&pUnit->GetNode(0)&&pUnit->GetNode(0)->m_sType=="ITRSUB"&&pUnit->GetNode(1)&&pUnit->GetNode(1)->m_sType=="ESPSUB" )
	{///
		//AfxMessageBox("pUnit->m_sAsi==SDGDoWhile");
		pESP=p->GetNode(1);
		
		for(int i=pESP->GetRelateNum()-1;i>=0;i--)
		{
			if(pESP->GetNode(i))
			{
			pf->InsertNode(index,pESP->GetNode(i));
			pESP->GetNode(i)->SetFather(pf);
			}
		}
		pESP->RemoveAll();
		DeleteBranch(pESP);
		pESP=NULL;
		p->Del(1);
	
	}
	if(p->GetRelateNum()<1)
	{
	//	AfxMessageBox("p->GetRelateNum()<1");/////////////////
		return;
	}
	//////////////////////////////////
	/*	CString stemp;
		stemp.Format("after delete ESPSUB p->GetRelateNum()=%d",p->GetRelateNum());
		AfxMessageBox(stemp);*/
		///////////////////////////////
	
	pITR=p->GetNode(0);
	if(pITR&&pITR->m_sType=="ITRSUB" )
	{
	//AfxMessageBox("pITR name:"+p->GetNode(0)->m_sType);
		p->RemoveAll();
		for(int i=0;i<pITR->GetRelateNum();i++)
		{
	//	AfxMessageBox("insert node to p");///////////	
			if(pITR->GetNode(i))
			{
			p->InsertNode(i,pITR->GetNode(i));
			pITR->GetNode(i)->SetFather(p);
			}
		}
		pITR->RemoveAll();
		DeleteBranch(pITR);
		pITR=NULL;
		modified=true;
	}

		//delete pITR;
		/////////////////////////////
	/*	CString stemp;
		stemp.Format("p->GetRelateNum()=%d",p->GetRelateNum());
		AfxMessageBox(stemp);*/
		//////////////////////////////


}
void CVarRemove::FalseIteration(CSDGBase*p)
{///////////wtt///3.10////后根序遍历USDG,将控制条件永假的循环语句删除
	if(p==NULL)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		FalseIteration(p->GetNode(i));
	}

	if(p->m_sType!="ITERATION")
		return;
	
	if(p->m_pinfo&&p->m_pinfo->T.key==CN_CINT&&p->m_pinfo->T.addr==0||p->m_pinfo&&p->m_pinfo->T.key==CN_NOT&&p->m_pinfo->pleft&&p->m_pinfo->pright==NULL&&p->m_pinfo->pleft->T.key==CN_CINT&&p->m_pinfo->pleft->T.addr>0)	
	{
		CSDGBase*pf=NULL;
		pf=p->GetFather();
		int index;
		index=FindIndex(p);
		pf->Del(index); 
		DeleteBranch(p);
		p=NULL;
		modified=true;

	}	
	
	
}


///wtt//05.10.20/////// 循环控制条件标准化////////
void   CVarRemove::StandLoopCondition(CSDGBase*p)
{//for(n=100;n<=999;n++)-->for(n=100;n<1000;n++)

	if(p==NULL)
		return;
	for(int i=0;i<p->GetRelateNum();i++)
	{
		StandLoopCondition(p->GetNode(i));
	}

	if(p->m_sType!="ITERATION")
		return;
		if(p->m_pinfo&&p->m_pinfo->T.key==CN_BANDE&&p->m_pinfo->pleft&& p->m_pinfo->pright&&p->m_pinfo->pleft->T.key==CN_CINT  )	
		{//for(n=100;n<=999;n++)-->for(n=100;n<1000;n++)
			  p->m_pinfo->T.key=CN_GREATT;
			  p->m_pinfo->pleft->T.addr+=1; 

		}
		else if(p->m_pinfo&&p->m_pinfo->T.key==CN_BANDE&&p->m_pinfo->pleft&& p->m_pinfo->pright&&p->m_pinfo->pleft->T.key==CN_SUB&&p->m_pinfo->pleft->pleft&&p->m_pinfo->pleft->pright
			&&p->m_pinfo->pleft->pleft->T.key==CN_VARIABLE
			&&p->m_pinfo->pleft->pright&& p->m_pinfo->pleft->pright->T.key==CN_CINT&& p->m_pinfo->pleft->pright->T.addr==1      )
		{//for (i=0; i<=n-1; i++) --->for (i=0; i<n; i++)   
			
			TNODE TD;
            CopyTNODE(TD,p->m_pinfo->pleft->pleft->T);
			CopyTNODE(p->m_pinfo->pleft->T,TD);
			p->m_pinfo->pleft->pright=NULL; 
			p->m_pinfo->pleft->pleft=NULL; 
			p->m_pinfo->T.key=CN_GREATT ; 
		
		}
			else if(p->m_pinfo&&p->m_pinfo->T.key==CN_GREATT&&p->m_pinfo->pleft&& p->m_pinfo->pright&&p->m_pinfo->pleft->T.key==CN_ADD&&p->m_pinfo->pleft->pleft&&p->m_pinfo->pleft->pright
			&&p->m_pinfo->pleft->pleft->T.key==CN_VARIABLE
			&&p->m_pinfo->pleft->pright&& p->m_pinfo->pleft->pright->T.key==CN_CINT&& p->m_pinfo->pleft->pright->T.addr==1      )
		{// //for (i=0; i<n+1; i++) --->for (i=0; i<=n; i++)  
			
			TNODE TD;
            CopyTNODE(TD,p->m_pinfo->pleft->pleft->T);
			CopyTNODE(p->m_pinfo->pleft->T,TD);
			p->m_pinfo->pleft->pright=NULL; 
			p->m_pinfo->pleft->pleft=NULL; 
			p->m_pinfo->T.key=CN_BANDE ; 
		
		}

}
////////////////////

void CVarRemove::RemoveContinue(CSDGBase*p,CArray<SNODE,SNODE>&SArry)
{//wtt编写//3.10//后根序遍历USDG,用if-else结构取代if-continue结构
	if(p==NULL)
	{
		return ;
	}
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveContinue(p->GetNode(j),SArry);
		
	}
	if(p->m_sType!="ITERATION")
		return;
	if(p->GetRelateNum()<1)
		return;
	int i=0;
	int no;
	while(p->GetNode(0)&&i<p->GetNode(0)->GetRelateNum()&&NeedTransformContinue(p->GetNode(0)->GetNode(i))<0)
	{
/*	CString stemp;
	stemp.Format("i=%d,return no=%d",i,NeedTransformContinue(p->GetNode(0)->GetNode(i)));
	AfxMessageBox(stemp);*/
	i++;
	}
	if(p->GetNode(0)&&i==p->GetNode(0)->GetRelateNum())
		return;
	if(p->GetNode(0)==NULL)
		return;
	//////////////////////////////////
/*	CString stemp;
	stemp.Format("return no %d",NeedTransformContinue(p->GetNode(0)->GetNode(i)));
	AfxMessageBox(stemp);*/
	///////////////////////////////////////
	int flag1,flag2;
	int selpos=i;
	CSDGBase*psel=p->GetNode(0)->GetNode(i);
	CSDGBase*psel1=NULL;
	no=NeedTransformContinue(p->GetNode(0)->GetNode(i));
	if(p->GetRelateNum()==2&&p->GetNode(1)&&p->GetNode(1)->m_sType=="ESPSUB")
	{
		psel1=p->GetNode(1)->GetNode(i);
		
	}
	
	if(i==p->GetNode(0)->GetRelateNum()-1)
	{
		flag2=0;
	}
	else
	{
		flag2=1;
	}
	if(NeedTransformContinue(p->GetNode(0)->GetNode(i))==0)
	{
		flag1=0;
	}
	else
	{
		flag1=1;
	}
		
	if(flag1==0&&flag2==0)
	{
		p->GetNode(0)->Del(selpos);
		DeleteBranch(psel);
		psel=NULL;
		if(p->GetRelateNum()==2&&p->GetNode(1)&&p->GetNode(1)->m_sType=="ESPSUB")
		{
			p->GetNode(1)->Del(selpos);
			DeleteBranch(p->GetNode(1)->GetNode(selpos));
		
		}

	}
	
	if(flag1==0&&flag2!=0)
	{
	//	AfxMessageBox("flag1==0&&flag2!=0");
		CSDGBase*pselector=psel->GetNode(0);
		ENODE*pExp;
		pExp=new  ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_NOT;
		pExp->pleft=pselector->m_pinfo;
		pExp->pright=NULL;
		BExpStandard(pExp,SArry);////
		pselector->m_pinfo=pExp;
		DeleteBranch(pselector->GetNode(0));
	
		pselector->RemoveAll();
		
	//	AfxMessageBox("ok");
		int num;
		i=p->GetNode(0)->GetRelateNum()-1;
	     num=i;
		while(i>selpos)
		{
			if(pselector)
			{
			pselector->InsertNode(0,p->GetNode(0)->GetNode(i));
			p->GetNode(0)->GetNode(i)->SetFather(pselector);
			p->GetNode(0)->Del(i);
			}
			i--;
		}	
		//	AfxMessageBox("ok");////////////
		if(p->GetRelateNum()==2&&p->GetNode(1)->m_sType=="ESPSUB")
		{
		CSDGBase*pselector1=NULL;
		if(psel1)
			pselector1=psel1->GetNode(0);
		ENODE*pExp1=NULL;
		pExp1=new  ENODE;
		InitialENODE(pExp1);
		pExp1->T.key=CN_NOT;
		if(pselector1)
		pExp1->pleft=pselector1->m_pinfo;
		else
			pExp1->pleft=NULL;

		pExp1->pright=NULL;
		BExpStandard(pExp1,SArry);////
		if(pselector1)
		{
		pselector1->m_pinfo=pExp1;
		DeleteBranch(pselector1->GetNode(0));	
		pselector1->RemoveAll();
		}
	//	AfxMessageBox("ok");
		
		i=num;
		while(i>selpos&&pselector1&&p->GetNode(1)&&p->GetNode(1)->GetNode(i))
		{
			pselector1->InsertNode(0,p->GetNode(1)->GetNode(i));
			p->GetNode(1)->GetNode(i)->SetFather(pselector1);
			p->GetNode(1)->Del(i);
			i--;
		}
		}

	}
	if(flag1!=0&&flag2==0)
	{
		CSDGBase*pselector=psel->GetNode(0);
		if(pselector)
		{
		DeleteBranch(pselector->GetNode(no));
		pselector->Del(no);
		}
		if(p->GetRelateNum()==2&&psel1&&p->GetNode(1)&&p->GetNode(1)->m_sType=="ESPSUB")
		{
			CSDGBase*pselector1=psel1->GetNode(0);
			if(pselector1)
			{	DeleteBranch(pselector1->GetNode(no));
				pselector1->Del(no);
			}

		}		
	}
	if(flag1!=0&&flag2!=0)
	{
		CSDGBase*pselectorl=psel->GetNode(0);
		if(pselectorl)
		{DeleteBranch(pselectorl->GetNode(no));
	     pselectorl->Del(no);
		}

		CSDGBase*pselectorr=new CSDGBase;
		pselectorr->m_sType="SELECTOR";
	//	pselectorr->m_sName="SELECTOR";
		pselectorr->RemoveAll();		
		
		pselectorr->m_pinfo=CopyExpTreeEx(pselectorr->m_pinfo,pselectorl->m_pinfo);
		ENODE*pExp=NULL;
		pExp=new  ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_NOT;
		
		pExp->pleft=pselectorr->m_pinfo;
		pExp->pright=NULL;
		BExpStandard(pExp,SArry);////
		pselectorr->m_pinfo=pExp;

		
		
		
	//	AfxMessageBox("ok");
		int num=0;
		if(p->GetNode(0))
		num=i=p->GetNode(0)->GetRelateNum()-1;
	
		while(i>selpos)
		{
			if(p->GetNode(0))
			{
			pselectorr->InsertNode(0,p->GetNode(0)->GetNode(i));
			if(p->GetNode(0)->GetNode(i))
			{
			p->GetNode(0)->GetNode(i)->SetFather(pselectorr);
			p->GetNode(0)->Del(i);
			}
			}
			i--;
		}	
		pselectorr->SetFather(psel);
		psel->InsertNode(1,pselectorr);

		if(p->GetRelateNum()==2&&p->GetNode(1)&&p->GetNode(1)->m_sType=="ESPSUB")
		{
			psel1=p->GetNode(1)->GetNode(selpos);
			CSDGBase*pselectorl1=psel1->GetNode(0);
			if(pselectorl1)
			{
			DeleteBranch(pselectorl1->GetNode(no));
		
			pselectorl1->Del(no);
			}
			CSDGBase*pselectorr1=new CSDGBase;
			pselectorr1->m_sType="SELECTOR";
			pselectorr1->RemoveAll();
			
			if(pselectorl1)
			pselectorr1->m_pinfo=CopyExpTreeEx(pselectorr1->m_pinfo,pselectorl1->m_pinfo);
			
			ENODE*pExp1;
			pExp1=new  ENODE;
			InitialENODE(pExp1);
			pExp1->T.key=CN_NOT;
		
			pExp1->pleft=pselectorr1->m_pinfo;
			pExp1->pright=NULL;
			BExpStandard(pExp1,SArry);////
			pselectorr1->m_pinfo=pExp1;

			
	//	AfxMessageBox("ok");
		
			i=num;
	
			while(i>selpos)
			{
				if(pselectorr1&&p->GetNode(1)&&p->GetNode(1)->GetNode(i))
				{
				pselectorr1->InsertNode(0,p->GetNode(1)->GetNode(i));
				p->GetNode(1)->GetNode(i)->SetFather(pselectorr1);
				p->GetNode(1)->Del(i);
				}
				i--;
			}	
			if(pselectorr1&&psel1)
			{
			pselectorr1->SetFather(psel1);
			psel1->InsertNode(1,pselectorr1);
			}
		}		

	}
	modified=true;


}

int CVarRemove::NeedTransformContinue(CSDGBase*p)
{//wtt///3.10//判定是有if-continue结构，如果没有返回-1，否则返回break在其父节点控制队列中的编号
	if(p==NULL)
		return -1;
	if(p->m_sType!="SELECTION")
	{
	//	AfxMessageBox("p->m_sType!=SELECTION");
		return -1;
	}
	if(p->GetRelateNum()!=1)
	{
	//	AfxMessageBox("p->GetRelateNum()!=1");
		return -1;
	}
	CSDGBase*pselector=p->GetNode(0);
	if(pselector==NULL)
		return -1;
	if(pselector->m_sType!="SELECTOR")
	{
	//	AfxMessageBox("pselector->m_sType!=SELECTOR");
		return -1;
	}	
	if(pselector->GetRelateNum()<1)
	{
	//	AfxMessageBox("pselector->GetRelateNum()<1");
		return -1;
	}
	if(pselector->GetNode(0)==NULL)
		return -1;
	if(pselector->GetNode(0)->m_sType=="SDGCONTINUE")
		return 0;
	int i;
	for(i=1;i<pselector->GetRelateNum();i++)
	{
		if(pselector->GetNode(i)&&pselector->GetNode(i)->m_sType=="SDGCONTINUE")
			break;
	}
	if(i==pselector->GetRelateNum())
	{
	//	AfxMessageBox("i==pselector->GetRelateNum()");
		return -1;
	}
	else
		return i;
}
void CVarRemove::RemoveReturn(CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{///wtt//3.11编写：用if-else结构，消除USDG中的if-return 结构，但仅限于if的父节点是entry节点
	if(pHead==NULL)
		return;
	if(pHead->m_sType!="SDGHEAD")
		return;
	for(int i=0;i<pHead->GetRelateNum();i++)
	{
		if(pHead->GetNode(i)&&pHead->GetNode(i)->m_sType=="SDGENTRY")
		{
			RemoveReturnInFuction(pHead->GetNode(i),SArry);
		}
	}

}
void CVarRemove::RemoveReturnInFuction(CSDGBase*pEntry,CArray<SNODE,SNODE>&SArry)
{/////wtt//3.11编写：消除一个函数中的if-return 结构，但仅限于if的父节点是entry节点
	if(!pEntry)
		return;
	for(int i=pEntry->GetRelateNum()-1;i>=0;i--)
	{
		int no=NeedTransformReturn(pEntry->GetNode(i));
	//////////////////////////////////////
	/*	CString stemp;
		stemp.Format("no=%d",no);
		AfxMessageBox(stemp);*/
		///////////////////////////////
		if(no>=0)
		{
			int selpos=i;
			int flag1,flag2;
			CSDGBase*psel=pEntry->GetNode(i);
			CSDGBase*pselector=NULL;
			if(psel&&psel->GetNode(0))
			pselector=psel->GetNode(0);	
			if(selpos==pEntry->GetRelateNum()-1&&pselector->GetNode(no)->m_pinfo==NULL)
			{
				flag2=0;
			}
			else
			{
				flag2=1;
			}
			if(no==0&&pselector->GetNode(no)->m_pinfo==NULL)
			{
				flag1=0;
			}
			else
			{
				flag1=1;
			}
			if(flag1==0&&flag2==0)//    {....... if()return;}
			{
				
				pEntry->Del(selpos);
				DeleteBranch(psel);
				psel=NULL;
				
			}
			if(flag1==0&&flag2!=0&&pselector)//{.... if()return;.....}
			{
				
				ENODE*pExp;
				pExp=new  ENODE;
				InitialENODE(pExp);
				pExp->T.key=CN_NOT;				
				pExp->pleft=pselector->m_pinfo;
				pExp->pright=NULL;
				BExpStandard(pExp,SArry);////
				pselector->m_pinfo=pExp;
				DeleteBranch(pselector->GetNode(0));			
				pselector->RemoveAll();	
			
				i=pEntry->GetRelateNum()-1;
				 
				while(i>selpos)
				{
					pselector->InsertNode(0,pEntry->GetNode(i));
					if(pEntry->GetNode(i))
					{
					pEntry->GetNode(i)->SetFather(pselector);
					pEntry->Del(i);
					}
					i--;
				}	

			}
			if(flag1!=0&&flag2==0&&pselector)
			{
				if(pselector&&pselector->GetNode(no)&&pselector->GetNode(no)->m_pinfo==NULL)
				{//{......if(){....;return;}}//删return； 
					DeleteBranch(pselector->GetNode(no));
				
					pselector->Del(no);
				}
				//{......if(){....;return exp;}}//不删return exp


			}
			if(flag1!=0&&flag2!=0&&pselector)
			{
				CSDGBase*pselectorl=psel->GetNode(0);
				if(pselectorl)
				{
					if(pselectorl->GetNode(no)&&pselectorl->GetNode(no)->m_pinfo==NULL)
					{//{......if(){....;return;}...}删return；
					DeleteBranch(pselectorl->GetNode(no));
					pselectorl->Del(no);
					}
					//{......if(){....;return exp;}...}//不删return exp
				}

				CSDGBase*pselectorr=new CSDGBase;
				pselectorr->m_sType="SELECTOR";
				pselectorr->RemoveAll();		
		
				pselectorr->m_pinfo=CopyExpTreeEx(pselectorr->m_pinfo,pselectorl->m_pinfo);
				ENODE*pExp=NULL;
				pExp=new  ENODE;
				InitialENODE(pExp);
				pExp->T.key=CN_NOT;
		
				pExp->pleft=pselectorr->m_pinfo;
				pExp->pright=NULL;
				BExpStandard(pExp,SArry);////
				pselectorr->m_pinfo=pExp;		
				i=pEntry->GetRelateNum()-1;	
				while(i>selpos)
				{
					if(pEntry->GetNode(i))
					{
					pselectorr->InsertNode(0,pEntry->GetNode(i));
					pEntry->GetNode(i)->SetFather(pselectorr);
					pEntry->Del(i);
					}
					i--;
				}	
				if(pselectorr&&psel)
				{
				pselectorr->SetFather(psel);
				psel->InsertNode(1,pselectorr);
				}
			}
			modified=true;
		}
	
	}

}
int CVarRemove::NeedTransformReturn(CSDGBase*p)
{/////wtt//3.11编写：判定是否满足消除if-return 结构的条件，若不满足返回-1。否则返回return在其父节点的控制队列中的序号
	
	if(p==NULL)
		return -1;
	if(p->m_sType!="SELECTION")
	{
	//	AfxMessageBox("p->m_sType!=SELECTION");
		return -1;
	}

//	AfxMessageBox("p->m_sType==SELECTION");
	if(p->GetRelateNum()!=1)
	{
	//	AfxMessageBox("p->GetRelateNum()!=1");
		return -1;
	}
	CSDGBase*pselector=p->GetNode(0);
	if(pselector==NULL)
		return -1;
	if(pselector->m_sType!="SELECTOR")
	{
	//	AfxMessageBox("pselector->m_sType!=SELECTOR");
		return -1;
	}	
	if(pselector->GetRelateNum()<1)
	{
	//	AfxMessageBox("pselector->GetRelateNum()<1");
		return -1;
	}
	if(pselector->GetNode(0)==NULL)
		return -1;
//	if(pselector->GetNode(0)->m_sType=="SDGRETURN"&&pselector->GetNode(0)->m_pinfo!=NULL )
//		return -1;
//	if(pselector->GetNode(0)->m_sType=="SDGRETURN")
//		return 0;
	if(pselector->GetNode(0)->m_sType=="SDGRETURN"&&pselector->GetNode(0)->m_pinfo==NULL)
		return 0;
	else if (pselector->GetNode(0)->m_sType=="SDGRETURN"&&pselector->GetNode(0)->m_pinfo!=NULL)
		return -1;//////////wtt//2007.11.7

	int i;
	for(i=1;i<pselector->GetRelateNum();i++)
	{
		//if(pselector->GetNode(i)&&pselector->GetNode(i)->m_sType=="SDGRETURN")
		if(pselector->GetNode(i)&&pselector->GetNode(i)->m_sType=="SDGRETURN"&&pselector->GetNode(i)->m_pinfo==NULL)
			break;
	}
	if(i==pselector->GetRelateNum())
	{
	//	AfxMessageBox("i==pselector->GetRelateNum()");
		return -1;
	}
	else
		return i;
}

void CVarRemove::RemoveBreak(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//wtt编写//3.11//后根序遍历USDG,通过增加控制循环条件消除if-break结构
	if(p==NULL)
	{
		return ;
	}
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveBreak(p->GetNode(j),pHead,SArry);
		
	}
	if(p->m_sType!="ITERATION")
		return;
	if(p->GetRelateNum()<1)
		return;
	int i=0;
	int no;
	while(p->GetNode(0)&&i<p->GetNode(0)->GetRelateNum()&&NeedTransformBreak(p->GetNode(0)->GetNode(i))<0)
	{
/*	CString stemp;
	stemp.Format("i=%d,return no=%d",i,NeedTransformContinue(p->GetNode(0)->GetNode(i)));
	AfxMessageBox(stemp);*/
	i++;
	}
	if(i==p->GetNode(0)->GetRelateNum())
		return;
	//////////////////////////////////
/*	CString stemp;
	stemp.Format("return no %d",NeedTransformBreak(p->GetNode(0)->GetNode(i)));
	AfxMessageBox(stemp);
*/	///////////////////////////////////////
	
	int selpos=i;
	CSDGBase*psel=p->GetNode(0)->GetNode(i);
	CSDGBase*psel1=NULL;
	no=NeedTransformBreak(p->GetNode(0)->GetNode(i));
	if(no!=0)
		return;
	if(p->GetRelateNum()==2&&p->GetNode(1)&&p->GetNode(1)->m_sType=="ESPSUB")
	{
		psel1=p->GetNode(1)->GetNode(i);
		
	}
		
	
//	AfxMessageBox("need transformation");//////////
	if(!IsDependent(psel,selpos,pHead,SArry))
	{//ITRSUB的处理
	//	AfxMessageBox("ok");//////////////
		ENODE*pExp=NULL,*pExp2=NULL;
		pExp=new  ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_DAND;

		pExp2=new ENODE;
		InitialENODE(pExp2);
		pExp2->T.key=CN_NOT;
		pExp2->pleft=psel->GetNode(0)->m_pinfo;
		psel->GetNode(0)->m_pinfo=NULL;
		pExp2->pright=NULL;

		pExp->pleft=p->m_pinfo;
		pExp->pright=pExp2;				
		BExpStandard(pExp,SArry);
		p->m_pinfo=pExp;

		p->GetNode(0)->Del(selpos);
		DeleteBranch(psel);
		psel=NULL;

	
	}

/////////////////add///code///here///////////////////
	
	else
	{//ITRSUB的处理
		SNODE SN;
		SN.type=CN_INT;
		SN.kind=CN_VARIABLE;
		SN.arrdim=0;
		for(int i=0;i<4;i++)
		SN.arrsize[i]=0 ;
		SN.addr=-1;
		SN.value=0;
		CSDGBase*pf=NULL;
		pf=psel;
		while(pf&&pf->m_sType!="SDGENTRY" )
		{
			pf=pf->GetFather(); 
		}
		if(pf==NULL)
			return;
		if(pf->TCnt.GetSize()<=0 )
			return;
		SN.layer=pf->TCnt[0].addr+1;
		SN.name=="";
		for( i=0;i<SArry.GetSize();i++ )
		{
			if(SArry[i].layer==SN.layer)
				SN.name+=SArry[i].name.Left(3);
		}

		SArry.Add(SN);
		CSDGDeclare*pdec=new CSDGDeclare;
		pdec->m_sType="#DECLARE";		

		TNODE T;
		T.key=CN_VARIABLE;
		T.addr=SArry.GetSize()-1;
		T.name=SN.name;
		pdec->TCnt.Add(T); 
		pf->InsertNode(0,pdec);
		pdec->SetFather(pf); 





		CAssignmentUnit*pas=new CAssignmentUnit;
		pas->m_sType="ASSIGNMENT";
		ENODE*pExpl=new ENODE;
		InitialENODE(pExpl);
		pExpl->T.key=CN_VARIABLE;
		pExpl->T.addr=SArry.GetSize()-1;
		pExpl->T.name=SN.name;


		
		
		ENODE*pExpr=new ENODE;
		InitialENODE(pExpr);
		pExpr->T.key=CN_CINT;
		pExpr->T.addr=0;
		
		

		pas->m_pleft=pExpl; 
		pas->m_pinfo=pExpr;


		////////////
		CAssignmentUnit*pas1=new CAssignmentUnit;
		pas1->m_sType="ASSIGNMENT";

		pExpl=new ENODE;
		InitialENODE(pExpl);
		pExpl->T.key=CN_VARIABLE;
		pExpl->T.addr=SArry.GetSize()-1;
		pExpl->T.name=SN.name;
		
		pas1->m_pleft=pExpl; 
		pas1->m_pinfo=psel->GetNode(0)->m_pinfo;		
		
		
		psel->GetNode(0)->m_pinfo=NULL;
		
	


		pf=p->GetFather();
		if(pf==NULL)
			return;
		int index=FindIndex(p);
		pf->InsertNode(index,pas);
		pas->SetFather(pf); 
		DeleteBranch(psel);
		psel=NULL;
		p->GetNode(0)->Del(selpos);
		p->GetNode(0)->InsertNode(selpos,pas1);
		pas1->SetFather(p->GetNode(0) ); 


		ENODE*	pExp=new  ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_DAND;

		ENODE*pExp2=new ENODE;
		InitialENODE(pExp2);
		pExp2->T.key=CN_NOT;

		pExpl=new ENODE;
		InitialENODE(pExpl);
		pExpl->T.key=CN_VARIABLE;
		pExpl->T.addr=SArry.GetSize()-1;
		pExpl->T.name=SN.name;



		pExp2->pleft=pExpl;
		pExp2->pright=NULL;

		pExp->pleft=p->m_pinfo;
		pExp->pright=pExp2;				
		BExpStandard(pExp,SArry);
		p->m_pinfo=pExp;



	}	
	if(p->GetRelateNum()==2&&p->GetNode(1)&&p->GetNode(1)->m_sType=="ESPSUB")
		{	//do while语句的ESPSUB的处理
			if(selpos==p->GetNode(1)->GetRelateNum()-1)
			{
			//	AfxMessageBox("selpos==p->GetNode(1)->GetRelateNum()-1");////
				p->GetNode(1)->Del(selpos);
				DeleteBranch(psel1);
				psel1=NULL;
			}
			else
			{
				psel1=p->GetNode(1)->GetNode(selpos);
			//	AfxMessageBox("selpos!=p->GetNode(1)->GetRelateNum()-1");/////
				CSDGBase*pselector=psel1->GetNode(0); 

				ENODE*pExp=new ENODE;
				InitialENODE(pExp);
				pExp->T.key=CN_NOT;
				if(pselector)
				pExp->pleft=pselector->m_pinfo;
				else
					pExp->pleft=NULL;

				pExp->pright=NULL;
				BExpStandard(pExp,SArry);

				if(pselector)
				{pselector->m_pinfo=pExp;

				DeleteBranch(pselector->GetNode(0));
			
				pselector->RemoveAll();
				}
				CSDGBase*pf=psel1->GetFather(); 
				int i=pf->GetRelateNum()-1;
				while(i>selpos)
				{
					if(pselector&&pf->GetNode(i))
					{
					pselector->InsertNode(0,pf->GetNode(i));
					pf->GetNode(i)->SetFather(pselector);  
					pf->Del(i);
					}
					i--;
				}

			}

			
		}	
	modified=true;


}
int CVarRemove::NeedTransformBreak(CSDGBase*p)
{/////wtt//3.11编写：判定是否满足消除if-break 结构的条件，若不满足返回-1。否则返回0
     if(p==NULL)
		 return -1;
	if(p->m_sType!="SELECTION")
	{
	//	AfxMessageBox("p->m_sType!=SELECTION");
		return -1;
	}
	if(p->GetRelateNum()!=1)
	{
	//	AfxMessageBox("p->GetRelateNum()!=1");
		return -1;
	}
	CSDGBase*pselector=p->GetNode(0);
	if(pselector==NULL)
		return -1;
	if(pselector->m_sType!="SELECTOR")
	{
	//	AfxMessageBox("pselector->m_sType!=SELECTOR");
		return -1;
	}	
	if(pselector->GetRelateNum()!=1)
	{
	//	AfxMessageBox("pselector->GetRelateNum()<1");
		return -1;
	}
	if(pselector->GetNode(0)==NULL)
		return -1;
	if(pselector->GetNode(0)->m_sType=="SDGBREAK")
		return 0;	
	else
		return -1;

}
bool CVarRemove::IsDependent(CSDGBase*psel,int selpos,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//判定if语句的条件表达式是否与它左部和右部的语句块存在数据相关，是返回true，否则返回false
	if(psel==NULL||pHead==NULL)
		return false;
	CSDGBase*pf=psel->GetFather();
	SetInOutDataFlow(pHead,SArry);
	bool dependent=false;
	for(int i=0;dependent==false&&psel->GetNode(0)&&i<psel->GetNode(0)->m_aRefer.GetSize();i++)
	{
		TNODE T=psel->GetNode(0)->m_aRefer[i];
		for(int j=0;dependent==false&&j<pf->GetRelateNum();j++)
		{
			if(i!=selpos)
			{
				if(pf->GetNode(j)&&FindTNODEInDArry(T,pf->GetNode(j)->m_aDefine))
				{
					dependent=true;
				}
			}
		}
	}
	return dependent;
}
void CVarRemove::RemoveInvariableInIteration(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//wtt编：//3.11///将循环中不发生改变且不于循环中其它语句和控制条件存在数据依赖的语句提取到循环外
	if(p==NULL)
	{
		return;
	}
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveInvariableInIteration(p->GetNode(j),pHead,SArry);
		
	}
	if(p->m_sType!="ITERATION")
		return;
	if(p->GetRelateNum()<1)
		return;
	SetInOutDataFlow(pHead,SArry);
	int num=p->GetRelateNum();
	int i=0;
	CArray<int ,int>removeList;
	removeList.RemoveAll(); 
	while(i<num)
	{
		CSDGBase*pNode=p->GetNode(i); 
		if(pNode&&pNode->m_sType=="SDGCALL"&&pNode->m_pinfo&&(pNode->m_pinfo->T.name=="scanf"|| pNode->m_pinfo->T.name=="printf" ))
		{
			i++;
		}
		else
		{
			bool flag1=true;
			bool flag2=true;
		
			CArray<TNODE,TNODE>BExp;
			if(p->m_pinfo!=NULL)
			{
			SearchExpReferData(p,p->m_pinfo,BExp,SArry);
			}
			
			for(int j=0;flag1&&j<pNode->m_aRefer.GetSize();j++)
			{
				TNODE T=pNode->m_aRefer[j];
				for(int k=0;flag1&&k<num;k++)
				{
					
					if(p->GetNode(k)&&FindTNODEInDArry(T ,p->GetNode(k)->m_aDefine))
						flag1=false;
				}				
			}
			int j;
			for( j=0;flag2&&j<pNode->m_aDefine.GetSize();j++)
			{
				for(int k=0;flag1&&k<num;k++)
				{
					
					if(p->GetNode(k)&&FindTNODEInDArry(pNode->m_aDefine[j],p->GetNode(k)->m_aRefer)||FindTNODEInDArry(pNode->m_aDefine[j],BExp))//要考虑到不要和条件表达式存在数据依赖关系
						flag2=false;
				}				
			}
			if(flag1&&flag2)
			{/////substract
				removeList.Add(i);
				i++;

			}
			else
			{
				i++;
			}

		}
	}
	if(removeList.GetSize()<=0)
		return;
	CSDGBase*	pf=p->GetFather();
	if(pf==NULL)
		return;
	int index=FindIndex(p);
	for(i=removeList.GetSize()-1;i>=0;i--)
	{
		if(p->GetNode(removeList[i]))
		{
		pf->InsertNode(index+1,p->GetNode(removeList[i]));
		p->GetNode(removeList[i])->SetFather(pf);
		p->Del(removeList[i]) ;
		}
	}
	modified=true;

}
/*void CVarRemove::RemoveSimpleStatement(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//wtt编写：//3.12 ////消除简单语句例如： c = 1; d = c; e = d + 2;--->c = 1；e = c + 2；
	///////////或z = 1； y = a + z；-->y := a + 1


	RemoveVarStatement(p,pHead,SArry);
	
	
	RemoveConstantStatement(p,pHead,SArry);
		

}

void CVarRemove::RemoveVarStatement(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{//wtt编写：//3.12 ////消除简单语句例如： c = 1; d = c; e = d + 2;--->c = 1；e = c + 2；
	if(p==NULL)
	{
		return ;
	}
	int m=p->GetRelateNum();
	for(int n=0;n<m;n++)
	{
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveSimpleStatement(p->GetNode(j),pHead,SArry);
		
	}
	}
	if(p->m_sType!="ASSIGNMENT")
		return;
	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	if(IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=1)
	{
	//	AfxMessageBox("IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=1");
		return ;
	}
	SetInOutDataFlow(pHead,SArry);
	CSDGBase*pf=p->GetFather();
	if(!pf)
		return ;
	int index=FindIndex(p);
	int i=index+1;
	CArray<int ,int>expList; 

	expList.RemoveAll(); 
	while(i<pf->GetRelateNum() )
	{
		if(pf->GetNode(i)->m_sType=="ASSIGNMENT"&&ContainVar(pas->m_pleft ,pf->GetNode(i)->m_pinfo,SArry )  )
			expList.Add(i);
		i++;
	}
	if(expList.GetSize()==0)
	{
	//	AfxMessageBox("no var2=exp that contain temp");
		return ;
	}
	int k=0;

	if(IsSimple(p->m_pinfo)==1)
	{
		AfxMessageBox("detect var assignment");
		i=index-1;
		while(i>=0)
		{
			CAssignmentUnit*pas1;
			if(pf->GetNode(i)->m_sType=="ASSIGNMENT")
				pas1=(CAssignmentUnit*)pf->GetNode(i); 

			if(pf->GetNode(i)->m_sType=="ASSIGNMENT"&&ContainVar(pas->m_pinfo ,pas1->m_pleft,SArry  )  )
				break;
			i--;
		}
		if(i<0)
		{
		//	AfxMessageBox("no var1=exp");
			return ;
		}
		int pos1=i;
		for(int j=pos1+1;j<index;j++)
		{
			if(FindTNODEInDArry(pas->m_pinfo->T,pf->GetNode(j)->m_aDefine))
			{
		//	AfxMessageBox("var1 in define");
			return ;
			}
		}
	}
	bool flag1=false;
	while(k<expList.GetSize())
	{
		int pos=expList[k];
		bool flag=true;
		for(int j=index+1;j<=pos;j++)
		{
			if(FindTNODEInDArry(pas->m_pleft->T,pf->GetNode(j)->m_aDefine)||FindTNODEInDArry(pas->m_pinfo->T,pf->GetNode(j)->m_aDefine))
			{
			//	AfxMessageBox("FindTNODEInDArry(pas->m_pleft->T,pf->GetNode(j)->m_aDefine)");
				flag=false;
			}
		}		
		if(flag)	
		{
			flag1=true;
			ReplaceVar(p->m_pinfo,pas->m_pleft,pf->GetNode(pos)->m_pinfo,SArry);
			AExpStandard(pf->GetNode(pos)->m_pinfo,SArry);
		}		

		k++;
	}
	if(flag1)
	{
	pf->Del(index);
	DeleteBranch(p);
	p=NULL;
	return ;
	}
	return ;


 }
void CVarRemove::RemoveConstantStatement(CSDGBase*p,CSDGBase*pHead,CArray<SNODE,SNODE>&SArry)
{	////////wtt编写：//3.12 ////消除简单语句例如:z = 1； y = a + z；-->y := a + 1

	if(p==NULL)
	{
		return ;
	}
	int m=p->GetRelateNum();
	for(int n=0;n<m;n++)
	{
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveConstantStatement(p->GetNode(j),pHead,SArry);
		
	}
	}
	if(p->m_sType!="ASSIGNMENT")
		return ;
	CAssignmentUnit*pas=(CAssignmentUnit*)p;
	if(IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=0)
	{
	//	AfxMessageBox("IsSimple(pas->m_pleft)!=1||IsSimple(pas->m_pinfo)!=0");
		return ;
	}
	SetInOutDataFlow(pHead,SArry);
	CSDGBase*pf=p->GetFather();
	if(!pf)
		return ;
	int index=FindIndex(p);
	int i=index+1;
	CArray<int ,int>expList; 

	expList.RemoveAll(); 
	while(i<pf->GetRelateNum() )
	{
		if(pf->GetNode(i)->m_sType=="ASSIGNMENT"&&ContainVar(pas->m_pleft ,pf->GetNode(i)->m_pinfo ,SArry )  )
			expList.Add(i);
		i++;
	}
	if(expList.GetSize()==0)
	{
	//	AfxMessageBox("no var2=exp that contain temp");
		return ;
	}
	int k=0;
	
//	AfxMessageBox(" can replace if do not in def");
	bool flag1=false;
	while(k<expList.GetSize())
	{
		int pos=expList[k];
		bool flag=true;
		for(int j=index+1;j<=pos;j++)
		{
			if(FindTNODEInDArry(pas->m_pleft->T,pf->GetNode(j)->m_aDefine))
			{
			//	AfxMessageBox("FindTNODEInDArry(pas->m_pleft->T,pf->GetNode(j)->m_aDefine)");
				flag=false;
			}
		}		
		if(flag)	
		{
			flag1=true;
		//	AfxMessageBox("flag==true can replace");
			ReplaceVar(p->m_pinfo,pas->m_pleft,pf->GetNode(pos)->m_pinfo,SArry);
			AExpStandard(pf->GetNode(pos)->m_pinfo,SArry);
		}

		k++;
	}
	if(flag1)
	{
	
	pf->Del(index);
	DeleteBranch(p);
	p=NULL;
	}
	
 }
*/
//int CVarRemove::IsSimple(ENODE*p)
int IsSimple(ENODE*p)
{////wtt编写：//3.12 ////判定是否是变量或常数
	if(p==NULL)
	{
	//	AfxMessageBox("p==NULL");//////////
		return -1;
	}
	if(p->T.key==CN_VARIABLE&&p->pleft==NULL&&p->pright==NULL)
	{
	//	AfxMessageBox("p->T.key==CN_VARIABLE&&p->pleft==NULL&&p->pright==NULL");//////////
		return 1;
	}
	if((p->T.key==CN_CINT||p->T.key==CN_CSTRING||p->T.key==CN_CFLOAT||p->T.key==CN_CCHAR||p->T.key==CN_CDOUBLE||p->T.key==CN_CLONG)&&p->pleft==NULL&&p->pright==NULL)
	{
	//	AfxMessageBox("p->T.key==CN_CINT||p->T.key==CN_CFLOAT||p->T.key==CN_CCHAR");//////////

		return 0;
	}
	return -1;
}
////////////////////////////////////
/*int CVarRemove::IsSimple1(ENODE*p)
{////wtt编写：//3.12 ////判定是否是变量或常数
	if(p==NULL)
	{
	//	AfxMessageBox("p==NULL");//////////
		return -1;
	}
	if(p->T.key==CN_VARIABLE&&p->pleft==NULL&&p->pright==NULL)
	{
	//	AfxMessageBox("p->T.key==CN_VARIABLE&&p->pleft==NULL&&p->pright==NULL");//////////
		return 1;
	}
	if((p->T.key==CN_CINT||p->T.key==CN_CSTRING||p->T.key==CN_CFLOAT||p->T.key==CN_CCHAR||p->T.key==CN_CDOUBLE||p->T.key==CN_CLONG)&&p->pleft==NULL&&p->pright==NULL)
	{
	//	AfxMessageBox("p->T.key==CN_CINT||p->T.key==CN_CFLOAT||p->T.key==CN_CCHAR");//////////

		return 0;
	}
	return -1;
}*/
///////////////////////////////
/*bool CVarRemove::ContainVar(ENODE*p ,ENODE*pExp,CArray<SNODE,SNODE>&SArry) 
{////wtt编写：//3.12 //判定pExp中是否包含p所指的变量
	if(p==NULL||pExp==NULL)
		return false;
	if(pExp->T.key==p->T.key&&pExp->T.name==p->T.name&&pExp->T.addr==p->T.addr&&pExp->info==p->info)
	{
		bool flag=true;
		if(p->info!=0)
		{
			for(int i=0;i<p->info;i++)//数组元素下标必须相同
			{
				if(!CompareTrees(p->pinfo[i],pExp->pinfo[i],SArry))
					flag=false;
			}

		}
		if(flag)
			return true;
	}

	if(ContainVar(p,pExp->pleft,SArry ))
		return true;
	if(ContainVar(p,pExp->pright,SArry ))
		return true;
	return false;
}
void CVarRemove::ReplaceVar(ENODE*p1,ENODE*p2,ENODE*pExp,CArray<SNODE,SNODE>&SArry)
{//////wtt编写：//3.12 //用p1替换表达式pExp中的p2

	if(pExp==NULL||p1==NULL||p2==NULL)
		return;
	if(pExp->T.key==p2->T.key&&pExp->T.name==p2->T.name&&pExp->T.addr==p2->T.addr&&pExp->info==p2->info)
	{
		
		bool flag=true;
		if(p2->info!=0)
		{
			for(int i=0;i<p2->info;i++)//数组元素下标必须相同
			{
				if(!CompareTrees(p2->pinfo[i],pExp->pinfo[i],SArry))
					flag=false;
			}

		}
		if(flag)
		{
		pExp->T.key=p1->T.key;
		pExp->T.addr=p1->T.addr;
		pExp->T.name=p1->T.name ;
		pExp->T.paddr=p1->T.paddr;
		pExp->T.value =p1->T.value;	
		pExp->info=p1->info;
		}
	}
	if(p1->T.key==CN_VARIABLE)
	{
		 //数组元素下标的拷贝
		for(int i=0;i<p1->info;i++)
		{
			pExp->pinfo[i]=p1->pinfo[i]; 
			p1->pinfo[i]=NULL;
		}

	}
	ReplaceVar(p1,p2,pExp->pleft,SArry );
	ReplaceVar(p1,p2,pExp->pright ,SArry);

}*/
/*void CVarRemove::FindRightSDGNode(CSDGBase*p,CArray<CSDGBase*,CSDGBase*>&prList)
{//wtt编写///3.13//查找p在usdg中右侧可能存在数据依赖关系的节点保存在prList中
	CSDGBase*pf;
	pf=p->GetFather();
	if(pf==NULL)
		return;
	if(pf->m_sType=="SDGENTRY" )
	{
		int i=FindIndex(p)+1;
		while(i<pf->GetRelateNum())
		{
			prList.Add(pf->GetNode(i));
			i++;
		}

	}
	else if(pf->m_sType=="SELECTOR")
	{
		int i=FindIndex(p)+1;
		while(i<pf->GetRelateNum())
		{
			prList.Add(pf->GetNode(i));
			i++;
		}
		pf=pf->GetFather();
		FindRightSDGNode(pf,prList);		

	}
	else if(pf->m_sType=="ITERATION")
	{
	//	AfxMessageBox("pf->m_sType==ITERATION");
		CSDGBase*p1=p;
		while(pf&&pf->m_sType=="ITERATION")
		{
			int index=FindIndex(p1);
			int i=index+1;
			while(i<pf->GetRelateNum())
			{
				prList.Add(pf->GetNode(i));
				i++;
			}
			p1=pf;
			pf=pf->GetFather();
		}
		
		int index=FindIndex(p1);
		int i=index+1;
		while(i<pf->GetRelateNum())
		{
			prList.Add(pf->GetNode(i));
			i++;
		}

		FindRightSDGNode(pf,prList);
	}
	
}*/

/*void CVarRemove::RemoveParaExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//wtt编写：//3.13///将函数调用中实参表达式用临时变量替换
	if(p==NULL)
	{
		return ;
	}

	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveParaExp(p->GetNode(j),SArry,FArry);
		
	}	

	if(!(p->m_sType=="SDGCALL"||p->m_sType=="ASSIGNMENT"&&p->m_pinfo&&p->m_pinfo->T.key==CN_DFUNCTION&&p->m_pinfo->pleft==NULL&&p->m_pinfo->pright==NULL ))
		return ;
	if(p->m_pinfo==NULL)
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int index;
	
	for(int i=0;i<10;i++)
	{
		if(p->m_pinfo->pinfo[i]!=NULL&&IsSimple(p->m_pinfo->pinfo[i])<0 )
		{
		
			SNODE SN;
	
			if(p->m_pinfo->T.key==CN_BFUNCTION)
			SN.type=CN_INT;
			else
			{
				SN.type= SArry[FArry[p->m_pinfo->T.addr].plist[i]].type;  
			}

			SN.kind=CN_VARIABLE;
			SN.arrdim=0;
			for(int j=0;j<4;j++)
				SN.arrsize[j]=0 ;
			SN.addr=-1;
			SN.value=0;
			CSDGBase*pf1;
			pf1=p;
		
			while(pf1&&pf1->m_sType!="SDGENTRY" )
			{
				pf1=pf1->GetFather(); 
			}
			if(pf1==NULL)
				return;
			if(pf1->TCnt.GetSize()<=0 )
				return;
			SN.layer=pf1->TCnt[0].addr+1;
			CString s;
			s.Format("%d",i); 
			SN.name="para_"+GetExpString(p->m_pinfo->pinfo[i])+s;	
	

			SArry.Add(SN);

			CSDGDeclare*pdec=new CSDGDeclare;
			pdec->m_sType="#DECLARE";		

			TNODE T;
			T.key=CN_VARIABLE;
			T.addr=SArry.GetSize()-1;
			T.name=SN.name;
			pdec->TCnt.Add(T); 
			pf1->InsertNode(0,pdec);
			pdec->SetFather(pf1); 

			CAssignmentUnit*pas=new CAssignmentUnit;
			pas->m_sType="ASSIGNMENT";
			ENODE*pExpl=new ENODE;
			InitialENODE(pExpl);
			pExpl->T.key=CN_VARIABLE;
			pExpl->T.addr=SArry.GetSize()-1;
			pExpl->T.name=SN.name;
			pExpl->info=0;
			pExpl->pleft=NULL;
			pExpl->pright=NULL; 
		
			pas->m_pleft=pExpl; 
			pas->m_pinfo=p->m_pinfo->pinfo[i];

			ENODE*pExp=new ENODE;
			InitialENODE(pExp);
			pExp->T.key=CN_VARIABLE;
			pExp->T.addr=SArry.GetSize()-1;
			pExp->T.name=SN.name;
			pExp->pleft=NULL;
			pExp->pright=NULL;

			p->m_pinfo->pinfo[i]=pExp;

			index=FindIndex(p);
			pf->InsertNode(index,pas);
			pas->SetFather(pf); 
			modified=true;

		}
	}

}
void CVarRemove::RemoveCallInExp(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//wtt编写：//3.14///将表达式中函数调用语句用临时变量替换
	if(p==NULL)
	{
		return ;
	}	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveCallInExp(p->GetNode(j),SArry,FArry);
		
	}
	
	if(!(p->m_sType=="ASSIGNMENT"||p->m_sType=="SDGRETURN"||p->m_sType=="SELECTOR"||p->m_sType=="ITERATION"))
		return ;
	if(p->m_pinfo==NULL)
		return;
	CSDGBase*pf=p->GetFather();
	if(pf==NULL)
		return;
	int index;


	if(pf==NULL)
		return;
	
	if(p->m_sType=="ASSIGNMENT"&&p->m_pinfo->pleft==NULL&&p->m_pinfo->pright==NULL )
		return;///只有一个函数调用的 赋值语句或不含函数调用的赋值语句不需处理
	else if((p->m_sType=="SDGRETURN"||p->m_sType=="SELECTOR"||p->m_sType=="ITERATION")&&p->m_pinfo->pleft==NULL&&p->m_pinfo->pright==NULL)
	{
		if(p->m_pinfo->T.key!=CN_DFUNCTION&& p->m_pinfo->T.key!=CN_BFUNCTION )
			return;

		if(pf->m_sType=="SELECTION")
		{
			AfxMessageBox("SELection sub declare");
		}
		SNODE SN;
		if(p->m_pinfo->T.key==CN_BFUNCTION)
		SN.type=CN_INT;
		else
		{
			SN.type= FArry[p->m_pinfo->T.addr].rtype;  
		}

		SN.kind=CN_VARIABLE;
		SN.arrdim=0;
		for( j=0;j<4;j++)
			SN.arrsize[j]=0 ;
		SN.addr=-1;
		SN.value=0;
		CSDGBase*pf1;
		pf1=p;
		
		while(pf1&&pf1->m_sType!="SDGENTRY" )
		{
		pf1=pf1->GetFather(); 
		}
		if(pf1==NULL)
			return;
		if(pf1->TCnt.GetSize()<=0 )
			return;
		SN.layer=pf1->TCnt[0].addr+1;
	
		SN.name="call_"+p->m_pinfo->T.name;	
		

		if(pf->m_sType!="SELECTION"||!(pf->m_sType=="SELECTION"&&pf1->GetNode(0)&&pf1->GetNode(0)->m_sType=="#DECLARE"&&pf1->GetNode(0)->TCnt[0].name==SN.name))
		{	
		SArry.Add(SN);
		CSDGDeclare*pdec=new CSDGDeclare;
		pdec->m_sType="#DECLARE";		
		TNODE T;
		T.key=CN_VARIABLE;
		T.addr=SArry.GetSize()-1;
		T.name=SN.name;
		pdec->TCnt.Add(T); 
		pf1->InsertNode(0,pdec);
		pdec->SetFather(pf1); 
		}
		CAssignmentUnit*pas=new CAssignmentUnit;
		pas->m_sType="ASSIGNMENT";
		ENODE*pExpl=new ENODE;
		InitialENODE(pExpl);
		pExpl->T.key=CN_VARIABLE;
		pExpl->T.addr=SArry.GetSize()-1;
		pExpl->T.name=SN.name;
		pExpl->info=0;
		pExpl->pleft=NULL;
		pExpl->pright=NULL; 
		
		pas->m_pleft=pExpl; 
		pas->m_pinfo=p->m_pinfo;

		ENODE*pExp=new ENODE;
		InitialENODE(pExp);
		pExp->T.key=CN_VARIABLE;
		pExp->T.addr=SArry.GetSize()-1;
		pExp->T.name=SN.name;
		pExp->pleft=NULL;
		pExp->pright=NULL;

		p->m_pinfo->pleft=pExp;
		
	
		index=FindIndex(p);	
		pf->InsertNode(index,pas);
		pas->SetFather(pf); 
		modified=true;
		return;

	}
	
	CArray<ENODE*,ENODE*>pCList;
	CArray<ENODE*,ENODE*> pFList;
	CArray<int,int>posList;
	pCList.RemoveAll();
	pFList.RemoveAll();
	posList.RemoveAll(); 
	FindCall(p->m_pinfo,pCList,pFList,posList);
	if(pCList.GetSize()==0)
		return;

	///////////////////////
	int num=pCList.GetSize();
	int i=0;
	while(i<num)
	{

	SNODE SN;
	if(p->m_pinfo->T.key==CN_BFUNCTION)
		SN.type=CN_INT;
	else
		{
			SN.type= FArry[pCList[i]->T.addr].rtype;  
		}

	SN.kind=CN_VARIABLE;
	SN.arrdim=0;
	for( j=0;j<4;j++)
		SN.arrsize[j]=0 ;
	SN.addr=-1;
	SN.value=0;
	CSDGBase*pf1;
	pf1=p;
		
	while(pf1&&pf1->m_sType!="SDGENTRY" )
	{
	pf1=pf1->GetFather(); 
	}
	if(pf1==NULL)
		return;
	if(pf1->TCnt.GetSize()<=0 )
		return;
	SN.layer=pf1->TCnt[0].addr+1;
	
	CString s;
	s.Format("_%d",i);
	SN.name="call_"+pCList[i]->T.name+GetExpString(pCList[i]->pinfo[0])+s;	
	


	if(pf->m_sType!="SELECTION"||!(pf->m_sType=="SELECTION"&&pf1->GetNode(0)&&pf1->GetNode(0)->m_sType=="#DECLARE"&&pf1->GetNode(0)->TCnt[0].name==SN.name))///////////
	{
	SArry.Add(SN);
	CSDGDeclare*pdec=new CSDGDeclare;
	pdec->m_sType="#DECLARE";		
	TNODE T;
	T.key=CN_VARIABLE;
	T.addr=SArry.GetSize()-1;
	T.name=SN.name;
	pdec->TCnt.Add(T); 
	pf1->InsertNode(0,pdec);
	pdec->SetFather(pf1); 
	}
	CAssignmentUnit*pas=new CAssignmentUnit;
	pas->m_sType="ASSIGNMENT";
	ENODE*pExpl=new ENODE;
	InitialENODE(pExpl);
	pExpl->T.key=CN_VARIABLE;
	pExpl->T.addr=SArry.GetSize()-1;
	pExpl->T.name=SN.name;
	pExpl->info=0;
	pExpl->pleft=NULL;
	pExpl->pright=NULL; 
		
	pas->m_pleft=pExpl; 
	pas->m_pinfo=pCList[i];

	ENODE*pExp=new ENODE;
	InitialENODE(pExp);
	pExp->T.key=CN_VARIABLE;
	pExp->T.addr=SArry.GetSize()-1;
	pExp->T.name=SN.name;
	pExp->pleft=NULL;
	pExp->pright=NULL;

	if(posList[i]==1)
		pFList[i]->pleft=pExp;
	else
		pFList[i]->pright=pExp; 

		index=FindIndex(p);		
	pf->InsertNode(index,pas);
	pas->SetFather(pf); 

	////////////////////////
	i++;
	}
	modified=true;


}
void CVarRemove::FindCall(ENODE*pExp,CArray<ENODE*,ENODE*>&pCList,CArray<ENODE*,ENODE*> &pFList,CArray<int,int>&posList)
{///wtt编写：//3.14///查找表达式中的函数调用节点保存到pCList中，其父节点保存在pFList中，其是父节点的左孩子还是右孩子保存在posList中
	if(pExp==NULL)
		return;
	FindCall(pExp->pleft,pCList,pFList,posList);
	if(pExp->pleft&&(pExp->pleft->T.key==CN_BFUNCTION||pExp->pleft->T.key==CN_DFUNCTION ) )
	{
		pCList.Add (pExp->pleft);
		pFList.Add (pExp);
		posList.Add(1); //是父节点的左孩子
	}
	if(pExp->pright&&(pExp->pright->T.key==CN_BFUNCTION||pExp->pright->T.key==CN_DFUNCTION ) )
	{
		pCList.Add (pExp->pright);
		pFList.Add (pExp);
		posList.Add(2); //是父节点的右孩子
	}
	FindCall(pExp->pright,pCList,pFList,posList);


}

void CVarRemove::RemoveExpInReturn(CSDGBase*p,CArray<SNODE,SNODE>&SArry,CArray<FNODE,FNODE>&FArry)
{//wtt编写：//3.14///将return语句后的表达式用临时变量替换
	if(p==NULL)
	{
		return ;
	}	
	for(int j=0;j<p->GetRelateNum();j++)
	{
		RemoveExpInReturn(p->GetNode(j),SArry,FArry);
		
	}
	
	if(p->m_sType!="SDGRETURN")
		return ;
	if(p->m_pinfo==NULL)
		return;
	if(IsSimple(p->m_pinfo)>=0)
		return;
	CSDGBase *pf=p->GetFather(); 
	if(pf==NULL)
		return;
	int index;
	SNODE SN;
	SN.kind=CN_VARIABLE;
	SN.arrdim=0;
	for( j=0;j<4;j++)
		SN.arrsize[j]=0 ;
	SN.addr=-1;
	SN.value=0;
	CSDGBase*pf1;
	pf1=p;
	while(pf1&&pf1->m_sType!="SDGENTRY" )
	{
		pf1=pf1->GetFather(); 
	}
	if(pf1==NULL)
		return;
	if(pf1->TCnt.GetSize()<=0 )
		return;
	SN.layer=pf1->TCnt[0].addr+1;
	
	SN.name="return_"+GetExpString(p->m_pinfo);
	if(p->m_pinfo->T.key==CN_DFUNCTION)
		
			SN.type= FArry[pf1->TCnt[0].addr].rtype;  
	else 
		SN.type=CN_INT; 
 
	SArry.Add(SN);

	CSDGDeclare*pdec=new CSDGDeclare;
	pdec->m_sType="#DECLARE";		
	TNODE T;
	T.key=CN_VARIABLE;
	T.addr=SArry.GetSize()-1;
	T.name=SN.name;
	pdec->TCnt.Add(T); 
	pf1->InsertNode(0,pdec);
	pdec->SetFather(pf1); 

	CAssignmentUnit*pas=new CAssignmentUnit;
	pas->m_sType="ASSIGNMENT";
	ENODE*pExpl=new ENODE;
	InitialENODE(pExpl);
	pExpl->T.key=CN_VARIABLE;
	pExpl->T.addr=SArry.GetSize()-1;
	pExpl->T.name=SN.name;
	pExpl->info=0;
	pExpl->pleft=NULL;
	pExpl->pright=NULL; 
		
	pas->m_pleft=pExpl; 
	pas->m_pinfo=p->m_pinfo;

	ENODE*pExp=new ENODE;
	InitialENODE(pExp);
	pExp->T.key=CN_VARIABLE;
	pExp->T.addr=SArry.GetSize()-1;
	pExp->T.name=SN.name;
	pExp->pleft=NULL;
	pExp->pright=NULL;

	p->m_pinfo=pExp;
	index=FindIndex(p);
	pf->InsertNode(index,pas);
	pas->SetFather(pf); 
	modified=true;	

}


////extern functions
/*void  ExDeleteUnrefered(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)////3.17
{
	CVarRemove varremove;
	varremove(pUSDGHead,SArry,FArry);
	varremove.DeleteUnrefered(pUSDGHead,SArry);
}*/
/*void ExRenameAndReorder(CSDGBase *pUSDGHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry)///wtt//3.17
{
		CVarRemove varremove;
		varremove(pUSDGHead,SArry,FArry);
		varremove.RenameAndReorder(pUSDGHead,SArry);

}*/