#pragma once


// CMovableButton

class CMovableButton : public CButton
{
	DECLARE_DYNAMIC(CMovableButton)

public:
	CMovableButton();
	virtual ~CMovableButton();

//Polaris-20150302
public:
	bool bMoving;
	int previous_x;
	int previous_y;
	int now_x;
	int now_y;
	int link_nodes[200];

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};


