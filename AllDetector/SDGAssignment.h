// SDGAssignment.h: interface for the CSDGAssignment class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDGASSIGNMENT_H__7EE7D5D6_B81E_4FF4_BDD7_2D5C8891DC68__INCLUDED_)
#define AFX_SDGASSIGNMENT_H__7EE7D5D6_B81E_4FF4_BDD7_2D5C8891DC68__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "SDGBase.h"

class CSDGAssignment : public CSDGBase
{

	// v=exp(x1,x2,x3,...)
public:
	CSDGAssignment();
	virtual ~CSDGAssignment();
public:
	int LV; // beginning position of "v"
	int OP; // position of "="
	int OPKIND;
	int vaddr;
	int expbeg; // beginning position of "exp(x1,x2,x3,...)"
	int expend; // end position of "exp(x1,x2,x3,...)"
	ENODE *m_pleft;
	CArray<TNODE,TNODE> TEXP;
};


#endif // !defined(AFX_SDGASSIGNMENT_H__7EE7D5D6_B81E_4FF4_BDD7_2D5C8891DC68__INCLUDED_)
