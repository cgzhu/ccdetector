// Latt_Node.cpp: implementation of the Latt_Node class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Latt_Node.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Latt_Node::Latt_Node()
{
	Item=_T("");
	Support=0;
	Ds_Size=0;
	Ds_Arry.RemoveAll();
	Father=NULL;
	Son.RemoveAll();
}//默认的构造函数

Latt_Node::Latt_Node(CString N_Item, int N_Support)
{
	Item=N_Item;
	Support=N_Support;
	Ds_Size=0;
	Ds_Arry.RemoveAll();
	Father=NULL;
	Son.RemoveAll();
}

Latt_Node::Latt_Node(class Latt_Node &LN)
{
	Item=LN.Item;
	Support=LN.Support;
	Ds_Size=LN.Ds_Size;
	Ds_Arry.Copy(LN.Ds_Arry);
	Son.Copy(LN.Son);
	Father=NULL;
}

Latt_Node& Latt_Node::operator=(Latt_Node &LN)
{
	Item=LN.Item;
	Support=LN.Support;
	Ds_Size=LN.Ds_Size;
	Ds_Arry.Copy(LN.Ds_Arry);
	Son.Copy(LN.Son);
	Father=NULL;
	return *this;
}

Latt_Node::~Latt_Node()
{
	Item=_T("");
	Support=0;
	Ds_Size=0;
	Ds_Arry.RemoveAll();
	Father=NULL;
	Son.RemoveAll();

}

CString Latt_Node::Get_Item()
{
	return Item;
}

int Latt_Node::Get_Sup()
{
	return Support;
}

int Latt_Node::Get_Dsize()
{
	return Ds_Size;
}

void Latt_Node::Add_Sup()
{
	Support++;
}

void Latt_Node::Add_DsNode(int ID, int Addr, int len)
{
	Ds_NODE DN;
	DN.Seq_ID=ID;
	DN.Subseq_Addr=Addr;
	Ds_Arry.Add(DN);
	if(len<Addr)
	{
		int a=1;
	}
	Ds_Size=Ds_Size+(len-Addr);//投影数据库规模增加的大小
	Support++;
}