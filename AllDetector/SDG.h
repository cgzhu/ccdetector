// SDG.h: interface for the CSDG class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDG_H__2F517CF0_260D_471B_A5BD_9763EA06078B__INCLUDED_)
#define AFX_SDG_H__2F517CF0_260D_471B_A5BD_9763EA06078B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Afxtempl.h"
#include "StdAfx.h"
#include "SDGAssignment.h"
#include "SDGDoWhile.h"
#include "SDGFor.h"
#include "SDGIf.h"
#include "SDGSwitch.h"
#include "SDGWhile.h"
#include "SDGBase.h"
#include "dataStruct.h"
#include "ConstData.h"

/*********************************************************************************************/
/*****************************************Class CSDG******************************************/
/************************************construct SDG of input c program*************************/
// NOTE: The SDG Constructed by this class is not completed,it only has the information of   //
//       control-flow,the information of data-flow is not set for the conveniences of program//
//       standarding,and after program standarding all the information will be set;          //
/*********************************************************************************************/
class CSDG
{
protected:

	CSDGBase *m_pSDG;
	CSDGBase *m_pHead;
	CArray<VIPCS,VIPCS> *m_pVArry;
	CList<CSDGBase *, CSDGBase *>  STACK;

protected:
	/******************************* Functions constructing SDG ******************************/

	void ConstructSDG(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	bool VariableType(int const key);
	int  IsImportant(CSDGBase *pNode);
	bool NotVDeclare(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry);
    //void GetDirectFather(CSDGBase *pDF,const int idx);//wtt
    void GetDirectFather(CSDGBase * &pDF,const int idx);//wtt

	void FindIfElse(CSDGBase *ptr,const int idx);
	void FindSelector(CSDGBase *ptr,const int idx);
	void FindFunction(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	/*********************************** End of this part ************************************/
	
    
	/************************* Functions dealing with "while" statement **********************/
	void WhileStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	bool FindWhileBody(CSDGWhile *pWhile,CArray<TNODE,TNODE> &TArry);
	/*********************************** End of this part ************************************/

	/************************* Functions dealing with declare statement **********************/
	void DeclareStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void AdjustDeclarePos(CSDGBase*pHead);

	/*********************************** End of this part ************************************/


	/************************ Functions dealing with "do-while" statement ********************/
	void DoWhileStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	bool FindDoWhileBody(CSDGDoWhile *pDo,CArray<TNODE,TNODE> &TArry);
	/*********************************** End of this part ************************************/


	/************************** Functions dealing with "for" statement ***********************/
	void ForStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	bool FindForBody(CSDGFor *pFor,CArray<TNODE,TNODE> &TArry);
	/*********************************** End of this part ************************************/


	/************************ Functions dealing with "switch" statement **********************/
	void SwitchStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	bool FindSwitchBody(CSDGSwitch *pSwitch,CArray<TNODE,TNODE> &TArry);
	void SetSelectors(CSDGSwitch *pSwitch);
	void SetSwitchCondition(CSDGBase*pHead,CArray<SNODE,SNODE>&SArry);//wtt//5.16//
	/*********************************** End of this part ************************************/
	


	/************************** Functions dealing with "if" statement ************************/
	void IfStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	bool FindIfBody(CSDGIf *pIf,CArray<TNODE,TNODE> &TArry);
	void SetIfElse(CSDGIf *pIf);
	/*********************************** End of this part ************************************/


	/************************* Functions dealing with "goto" statement ***********************/
	void GotoStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void FindGotoBody();
	/*********************************** End of this part ************************************/


	/*********************** Functions dealing with Assignment statement *********************/
	void AssignmentStatement(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void UnifyAssignment(CSDGAssignment *pAssign);//wtt//1.10
	void StandardofArrayInit(CSDGAssignment*pAssign,CSDGBase*pfather,CArray<SNODE,SNODE> &SArry);//wtt//3.1
	void FindAssignmentBody(CSDGAssignment *pAssign,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry);
	/*********************************** End of this part ************************************/

	/*********************** Functions dealing with "++" and "--" *********************/
	void IncAndDec(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	/*********************************** End of this part ************************************/


	/********************** Functions dealing with "continue" and "break" ********************/
	void BreakStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void ContinueStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	//void ReturnStatement(const int idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void ReturnStatement( int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);//wtt//3.13//

	/*********************************** End of this part ************************************/
    
	void CallNode(int &idx,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
  

    /************************************* set control flow ************************************/
    void SetBreakAndContinue(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
    void GetBreakAndContinue(CSDGBase *p);

	/*********************************** End of this part ************************************/

	/************************************array and pointer************************************/
	
	void StandardOfArray(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry);
	void StandardOfPointer(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry);
    
	/*********************************** end of this part ************************************/

	
	/************************************rename function**************************************/
	void RenameFunction(CSDGBase *pHead,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void SearchSDGRenameFunction(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void SearchExpRenameFunction(CArray<TNODE,TNODE> &TEXP,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	/*********************************** End of this part ************************************/


public:
	CSDGBase *operator()(CArray<VIPCS,VIPCS> &VArry,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
public:
	CSDG();
	virtual ~CSDG();

};

/************************************End of class SDG*****************************************/
/*********************************************************************************************/


#endif // !defined(AFX_SDG_H__2F517CF0_260D_471B_A5BD_9763EA06078B__INCLUDED_)
