#pragma once
#include "rc_type_tchart.h"


// CRCTypeDiagramDlg 对话框

class CRCTypeDiagramDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CRCTypeDiagramDlg)

public:
	CRCTypeDiagramDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRCTypeDiagramDlg();

	void ClearAllSeries();

// 对话框数据
	enum { IDD = IDD_RC_TYPE_DIAGRAM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRc_type_tchart m_RC_type_chart;
};
