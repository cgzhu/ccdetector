#pragma once


// CRCListCtrl
/*
 * 这个类是自己实现的显示冗余代码结果的List，支持双击打开对应文件
 */

class CRCListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CRCListCtrl)

public:
	CRCListCtrl();
	virtual ~CRCListCtrl();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
};


