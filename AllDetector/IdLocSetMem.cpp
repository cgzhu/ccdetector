// IdLocSetMem.cpp: implementation of the IdLocSetMem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IdLocSetMem.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IdLocSetMem::IdLocSetMem()
{
	idName = _T("");
	idLocArry.RemoveAll();
}

IdLocSetMem::~IdLocSetMem()
{
	idName = _T("");
	idLocArry.RemoveAll();
}

IdLocSetMem::IdLocSetMem(const IdLocSetMem& IPSM)
{
	idName = IPSM.idName;
	idLocArry.RemoveAll();
	idLocArry.Append(IPSM.idLocArry);
}

IdLocSetMem& IdLocSetMem::operator=(const IdLocSetMem& IPSM)
{
	idName = IPSM.idName;
	idLocArry.RemoveAll();
	idLocArry.Append(IPSM.idLocArry);
	return *this;
}

CString& IdLocSetMem::GetIdName()
{
	return idName;
}

CArray<IDENTF_LOCATION,IDENTF_LOCATION>& IdLocSetMem::GetIdLocArry()
{
	return idLocArry;
}

void IdLocSetMem::Assign(CString& p_idName,CArray<IDENTF_LOCATION,IDENTF_LOCATION>& p_idLocArry)
{
	idName = p_idName;
	idLocArry.RemoveAll();
	idLocArry.Append(p_idLocArry);
}