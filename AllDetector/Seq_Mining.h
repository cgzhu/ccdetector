// Seq_Mining.h: interface for the Seq_Mining class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SEQ_MINING_H__0AA02E2C_072C_4B25_A427_22E40B89A574__INCLUDED_)
#define AFX_SEQ_MINING_H__0AA02E2C_072C_4B25_A427_22E40B89A574__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"
#include "Latt_Node.h"

class Seq_Mining
{
public:
	CString out_file_path;//wq-20090402
	CArray <Seq_NODE, Seq_NODE> Seq_Arry_G;//数字挖掘的对象——序列数据库
	CArray <Latt_Node*, Latt_Node*> FItem_HASH[10000];//以项的后四位作为键值，存储指向频繁项（items）的指针
	CArray <CString, CString> FItem_Arry[10000];//以项的后四位作为键值，存储频繁项（items：长度为十的CString）
	class Latt_Node Latt_Root;//偏序前缀序列图的根节点
	CArray <Ds_Hash_Node, Ds_Hash_Node> Ds_HASH[10000];//基于Ds数据库的大小创建的hash表,键值是: (投影数据库大小+IDSum)%10000
	CArray <Ds_Hash_Node, Ds_Hash_Node> LS[10000];//用于存储LS候选集
	int CloSpanNum;
public:
	Seq_Mining();
	Seq_Mining(CArray <Seq_NODE,Seq_NODE> &Seq_Arry_Glob);
	virtual ~Seq_Mining();
	void ClosedMining();//闭合序列挖掘算法总函数
	CString LNode_To_Seq(class Latt_Node *LN);//根据偏序前缀序列图中的某个节点返回对应的序列


private:
//	int ContextInfoBuild(int &seqIndex,CArray<int,int> &ReadySeqIndexStack);//wq-20081115-寻找序列中的"{"或"}"，找到{返回x，找到}返回-x，否则返回0,x为不匹配{或}的个数
public:
	void OutputSeqToken();
	void DltSingleLnSeq();


private:
	void Find_Freqitems();	//找出数据库Seq_Arry_G中所有出现过的项（items），并存入FItem_HASH[10000]
	void Sort_Items();//删除FItem_HASH[10000]中出现次数小于2的项，并将频繁项（出现次数大于等于2）存入数组FItem_Arry对应的位置
	void CloSpan(class Latt_Node *Son_Node, class Latt_Node *Father_Node);//CloSpan挖掘算法
	int Check_ProDBSize(CString seq, class Latt_Node *S_Node);//Algorithm 4：查看候选集中是否存在影射数据库大小与S_Node相等，且与序列seq有包含关系的节点
	void Eliminate_NCS();//从Ds_HASH中去除非闭合的序列（non-closed sequences）

	int Is_Have_Items(int index, CString Item);//查找HASH表中以index为键值的桶否包含项Item,如找到则返回对应的索引;如没找到则返回-1	
	Temp_Seq_Node Init_TNode(int index, CString Item, int Seq_ID, int Subseq_Addr);//给节点TS_Node赋值
	
	void Add_DsHash(class Latt_Node *LN);//将指针LN存入Hash表中
	int Is_Expanded(const CArray<Ds_NODE,Ds_NODE> &Ds_Arry, CArray<Latt_Node*,Latt_Node*> &Exp_Node_Arry);
	//检查投影数据库Ds_Arry，查看是否可以继续扩展；如可以，将可扩展的项（item）信息存入Exp_Node_Arry
	bool Is_FreqItem(const CString Item);//通过查询数组FItem_Arry，判断Item是否为频繁项（Frequent Item）
	int Seq_Is_Contained(const CString seq1, const CString seq2);//判断数字序列seq1与seq2之间的包含关系
	
	void Trans_SubTrees(class Latt_Node *LN_From, class Latt_Node *LN_To);//将节点*LN_From的子孙移植给节点*LN_To，然后删除节点*LN_From
	void Del_Tree(class Latt_Node *LN, bool Is_delHash);//释放以LN为根节点的树所申请的内存，Is_delHash表示是否需要删除LN子孙节点在Ds_Hash中对应的节点
	void Del_Node(class Latt_Node *LN, bool Is_delHash);	//释放指针LN指向的节点内存
	bool Del_HashNode(class Latt_Node *LN);//删除Ds_HASH中指向*LN的节点
	
	float Get_IDSum(class Latt_Node *LN);//用数据库中支持该子序列的序列ID生成一个浮点数
	CString Get_SubSeq(int ID, int Addr);//根据ID，Addr生成子序列（该函数没用上！）
	void Remove_Hash(int index, CArray <int,int> &LS);//删除那些在DS_HASH表键值为index的桶中，且序号存于LS的元素（该函数也没用上！）
	void Test_Result(const CArray <Ds_Hash_Node, Ds_Hash_Node> Test_HASH[]);//测试函数
};

#endif // !defined(AFX_SEQ_MINING_H__0AA02E2C_072C_4B25_A427_22E40B89A574__INCLUDED_)