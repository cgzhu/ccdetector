// Lexical.h: interface for the CLexical class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEXICAL_H__1C8E3984_A28F_4037_B2FE_83AA3EEB57B1__INCLUDED_)
#define AFX_LEXICAL_H__1C8E3984_A28F_4037_B2FE_83AA3EEB57B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
//#include "datastruct.h"
#include "SeqNode.h"
#include "constdata.h"
#include "HeaderFileElem.h"

class CLexical  
{
	friend int FindPosition(CString name,int type,const CArray<SNODE,SNODE>&SArry);//wn
public:
	CLexical();
	virtual ~CLexical();
	CArray<SRCLINE,SRCLINE> srcCode;
	void OutputSrcFileAfterTokened( CArray<TNODE,TNODE> &TArry );
	CString Obj_file_path;//wq-20090319
	CString out_file_path;//wq-20090402
protected:

	
	CString program;	
	CArray<CERROR,CERROR> *m_pError;

private:
	
	int   m_nTLen;
	int   m_nType;
	VIPCS VD;

	/*************************************************************************************/
	// (^?^)
	// these functions below are used when construct token chain;
	// begin:

	void ConstructToken(const CList<CString,CString> &CT_List,CArray<TNODE,TNODE> &TArry,CArray<VIPCS,VIPCS> &VArry);
	//将CT_List中存储的源代码转化为token字，存入TArry中
	void ResetToken(int index,int end,CArray<TNODE,TNODE>&TArry);//wn/重置token表，实现结构类型定义的标准化
	//对与结构体、共用体和枚举类型的token字进行标准化
	void INIT_TNODE(TNODE &TD,int key,int addr,int line,double value,CString srcWord);	
	//为TD节点赋值
	int  WhichBFunction(const CString str);
	//返回基本库函数名对应的数值
	int  WhichKeyWord(const CString &str);
	//返回所有关键字对应的数值
	bool TranToLine(CString program, CList<CString,CString> &T_List);
	//将源程序program按行存入T_List
    void InvalidateLetter(int nline);
	void IsKuoHaoOK(const CArray<TNODE,TNODE> &TArry);
	// end
	// (^?^)
	
    /**************************************************************************************/

	// (^?^)
	// these functions below are used when construct symbal table;
	// begin:
	void PreConstructTable(int index,int end,CArray<TNODE,TNODE>&TArry,CArray<SNODE,SNODE>&SArry);//wn
	//建立符号表预处理：在建立符号表前，先将自定义结构类型加入符号表
	void FillMemberVar(int ix,int beg,int end,CArray<TNODE,TNODE>& TArry,CArray<SNODE,SNODE>&SArry);//wn
	void ConstructSTable(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void ReturnTandK(const int ix,SNODE &SD,FNODE &FD,const CArray<TNODE,TNODE> &TArry);
	void ParaAnalysis(const int nlayer,FNODE &FD,const CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry);
	void GlobalVariables(int &ix,SNODE &SD,const CArray<TNODE,TNODE> &TArry, CArray<SNODE,SNODE> &SArry);
	//处理全局变量的token
	void localVariables(const int nlayer,const FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry);
	//处理局部变量的token
	bool IsVariableType(int &ix,const CArray<TNODE,TNODE> &TArry);
	//判断TArry位置ix的地方是否为变量的类型名
	void ArrayDim(int &ix,SNODE &SD,const CArray<TNODE,TNODE> &TArry);
	bool VariableExisted(const SNODE SD,const CArray<SNODE,SNODE> &SArry);
//wtt	void VariableAssign(const int nlayer,const FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,const CArray<FNODE,FNODE> &FArry);
	void VariableAssign(const int nlayer, FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry, CArray<FNODE,FNODE> &FArry);

	//--------------------------Added Or Changed By LX(处理头文件替换和宏定义)---------------------------//	
	void INLBReplace(CString &cprogram,CString &hprogram,CArray<CString,CString> &Hfp_Array);
	//查找cprogram中的头文件名字，并将对应头文件的内容读入至hprogram中
	void Del_String(CString &program);
	//为便于后面的分析，将源程序中所有的字符串替换为"s"
	bool Delete_Comment(CString &program);
	//删除注释/*XXX*/
	int Find_Comment(CString &program, int Pos);
	//查找从count开始的下一个注释段的起始位置
	void Add_CH_Mark(CArray<TNODE,TNODE> &TArry,CArray<TNODE,TNODE> &INLB_TArry);
	//加入头文件和主文件token串之间的间隔节点		
	void DeleteHToken(CArray<TNODE,TNODE> &TArry);
	//删去曾加入的头文件token串
	void ReSet_LineValue(CArray<TNODE,TNODE> &TArry);
	//TArry中每个元素的line值加1
	CString READ_FILE_H(CString &hfilename,CArray<CString,CString> &Hfp_Array);
	//格式化hfilename生成正确的头文件名，并返回对应的头文件内容
	CString FIND_FILE_H(const CString filepath, const CString filename, const CString curFilePath);
	//在路径filepath下查找名为filename的头文件	
	void DEAL_FILE_H(CString &hprogram,CArray<CString,CString> &Hfp_Array);
	//处理存于hprogram中的头文件，数组Hfp_Array记录处理过的头文件路径
	bool Is_Cstr_Contain(CArray<CString,CString> &Hfp_Array,const CString &hfilepath);
	//判断是否已经加入路径名为hfilepath的头文件


	void DefineReplace(CString &hprogram, CString &cprogram);
	//在CString中通过字符串替换实现宏定义替换问题
	bool Genrate_Macro_NV(CString &program,int &count,CString &Macro_Name,CString &Macro_Value);
	//找到program中count所在的那一行，并生成宏定义名(Macro_Name)和宏定义值(Macro_Value)
	bool Genrate_Macro_Name(const CString &line_str,CString &Macro_Name,int &inter_mark);
	//从line_str里找出宏定义名Macro_Name，并用inter_mark记录Macro_Name与Macro_Value之间的位置
	void Span_Define(CString &program,const int start,const CString Macro_Name,const CString Macro_Value);
	//从start位置开始，用Macro_Name替换program中的Macro_Value	
	bool ParaDefine_Replace(CString &program,const int start,const CString Macro_Name,const CString Macro_Value);
	//替换带参数的宏定义，被Span_Define调用,如果成功替换则返回1	
	void Genrate_ParaArray(const CString &Macro_Name, CArray<CString,CString> &ParaN_Array);
	//根据宏定义Macro_Name名生成参数列表，存于数组ParaN_Array中

	bool ISHAVE_WELL(const CString &cprogram, const int &position,const int length);
	//检验position前面是否为'#'，position+length的后面是否为空格或'\t'；
	CString Deal_SomeLine(int &position, int &pos_inline, CString &program,bool del_flag);
	//返回program中position所在的那一行，pos_inline记录position指向的字符在此行中的位置，并根据del_flag的不同判断是否删除此行
	void Pro_to_Token(CString &program,CArray<TNODE,TNODE> &TArry,CArray<VIPCS,VIPCS> &VArry,bool HC_flag);
	//调用ConstructToken函数，将program的内容转换为token符存入数组TArry中
	//--------------------------Added Or Changed By LX(处理头文件替换和宏定义)---------------------------//		
	void TypedefReplace(CArray<TNODE,TNODE>&TArry);
	void IdentifyEnumConst(int beg,CArray<TNODE,TNODE>&TArry);
	// end
	// (^?^)

	/**************************************************************************************/

	// (^?^)
	// these functions below are used 
	// begin:
	void FormComplexOp(CArray<TNODE,TNODE> &TArry);
	void StatementBoundAnalyze(CArray<TNODE,TNODE> &TArry);
	bool InsertBoundSimple(CArray<TNODE,TNODE> &TArry);
	bool InsertBoundComplex(CArray<TNODE,TNODE> &TArry);
	bool FindBeg(int &ix,CArray<TNODE,TNODE> &TArry);//wtt
	bool FindSEnd(int &ix,CArray<TNODE,TNODE> &TArry);//wtt
	bool IsSentence(const int ix,CArray<TNODE,TNODE> &TArry);
	bool ExceptionElse(int &ix,CArray<TNODE,TNODE> &TArry);
	// end
	// (^?^)

	
public:
	void CountHFile(CString &hfilename,const CString filePath, CArray<CString,CString> &hFileNameArry, int &hfileCnt);
	int HFileSumCnt(CString &program,const CString cFilePath);
	void CFileMacroReplace(CString &program, CArray<HeaderFileElem,HeaderFileElem> &tempHFileArry);
	void GenerateCleanFilePath(CString &filepath);

	//wq-20090524-typedef定义替换
	void NewTypedefReplace(CArray<TNODE,TNODE> &TArry, HeaderFileElem &tempHFileElm);
	bool FindCompleteTypedef(int &i,CArray<TNODE,TNODE> &TArry, CArray<TNODE,TNODE> &tmpTpdfArry);
	void IncreaseTypedefRcdArry(CArray<TNODE,TNODE> &tmpTpdfArry,HeaderFileElem &tempHFileElm);//识别typedef定义的两个部分，添加到typedef记录中

	//wq-20090524-为了跳过头文件为函数定义和函数声明添加符号表
	void NewParaAnalysis(const int nlayer,FNODE &FD,const CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry);
	void NewLocalVariables(const int nlayer, const FNODE &FD,CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry);

	//wq-20090524-头文件符号表的生成，不对函数定义和函数声明进行存储
	void ConstructHFileSymTable(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);

	void HFileMacroReplace(CString &program, HeaderFileElem &tempHFileElm);
	void NewConstructSymTable(CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);
	void ReadHeaderFile(CString &filename,CString &program);
	void DefineReplaceFromSelf(CString &program,CString &macroLines);//wq-20090522-用program中的宏定义，进行program中的替换,并将宏定义行存储到macroLines
	void DefineReplaceFromHFile(CString &srcProgram, CString &dstProgram);//wq-20090522-用srcProgram中的宏定义，对dstProgram进行替换

	bool BuildHFileFullPath(CString &hfilename,CString curFilePath);

	//wq-20090522-递归处理C文件直接和间接包含的所有头文件
	void ProcessHFile(CString hfilename,
					  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
					  HeaderFileElem &tempHFileElm, CString filePath,
					  CArray<CString,CString> &hFileNameArry,
					  CH_FILE_INFO &fileInfo//wq-20090610
					  );

	//wq-20090522-头文件处理入口函数
	void DealHeaderFiles(CString &cProgram,
						CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
						CArray<HeaderFileElem,HeaderFileElem> &tempHFileArry,
						CString cFilePath,
						CH_FILE_INFO &fileInfo//wq-20090610
						);

	void OutputSymTable(CArray<SNODE,SNODE> &SArry);
	/*************************************************************************************/
	// (^?^)
	// specification of this function (bengin).
	// External interface of the class CLexical:
	// Operation: Lexical analyzing of the input c language program ,then generate its token chain and symbol table.
	// input parameters:
	//       cprogram: type--CString, function--store the input program or function.
	//       EArry   : type--CArray , function--reference parameter store the errors in the program. 
	//       TArry   : type--CArray , function--reference parameter store the token chain.
	//       SArry   : type--CArray , function--reference parameter store the symbol table.
	//       FArry   : type--CArray , function--reference parameter store the functions in the program.   
	int operator()(int nType,const CString cprogram,CArray<VIPCS,VIPCS> &VArry,CArray<CERROR,CERROR> &EArry,
		            CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);//返回值为分析的c文件的行数

	//wq-20090522-重载operator()函数
	int operator()(int nType,const CString cprogram,CArray<VIPCS,VIPCS> &VArry,CArray<CERROR,CERROR> &EArry,
		            CArray<TNODE,TNODE> &TArry,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry,
					CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
					const CString cFilePath,//wq-20090609
					CArray<CH_FILE_INFO,CH_FILE_INFO> &filesReadInfo//wq-20090610
					);//返回值为分析的c文件的行数

	//wq-20090522-重载INLBReplace()函数
	void INLBReplace(CString &cprogram,
					CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,//wq-20090522
					CArray<CString,CString> &Hfp_Array);

	//wq-20090522-重载READ_FILE_H()函数
	CString READ_FILE_H(CString &hfilename,
						CArray<CString,CString> &Hfp_Array,
						CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry//wq-20090522
						);

	bool IsHFileAdded(CString hFileName,
					  CArray<HeaderFileElem,HeaderFileElem> &headerFilesArry,
					  int &index);

	CERROR ER;
	// specification of this function (end).
	// (^?^)
	/************************************************************************************/

};

#endif // !defined(AFX_LEXICAL_H__1C8E3984_A28F_4037_B2FE_83AA3EEB57B1__INCLUDED_)