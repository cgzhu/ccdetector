// ProgramMatch.h: interface for the CProgramMatch class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROGRAMMATCH_H__8FE4CFEC_AE23_460B_83AD_0E5B7635846C__INCLUDED_)
#define AFX_PROGRAMMATCH_H__8FE4CFEC_AE23_460B_83AD_0E5B7635846C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SDGBase.h"

class CProgramMatch  
{
	friend CSDGBase*ExGetSDGStruct(CSDGBase *pHead);
	friend class CntxtInconsisDetect;

public:

	CProgramMatch();
	virtual ~CProgramMatch();

public:

	int operator()( int nType,int rArry[4],CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry,
					CArray<CERROR,CERROR> &EArry);
protected:

	CERROR ED;
	CArray<CERROR,CERROR> *m_pError;
	CSDGBase *m_pSS;
	CSDGBase *m_pST;
	int m_nType;
	int m_nDegree;

protected:


	// common operations
	int     GetSDGDepth(CSDGBase *pHead);
	int     FindIndex(CSDGBase *pson);
	CSDGBase *SearchFirstCertainNode(CSDGBase *pSDG,CString strNode);


	// size match
	int     SizeMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry);
	int     SizeOfSDG(CSDGBase *pHead,CArray<SNODE,SNODE> &SArry,CArray<FNODE,FNODE> &FArry);


	// struct match	
	int     StructMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry);
	int     MatchStructSDG(CSDGBase *pSSH,CSDGBase *pMSH);
	void    MatchStructNode(CSDGBase *pNode,CSDGBase *pSSH,CSDGBase *pMSH);
	void    MatchNodeTrace(CSDGBase *pS,CSDGBase *pT,CSDGBase *pSSH,CSDGBase *pMSH,
		                 CArray<CSDGBase*,CSDGBase*> &STRACE,CArray<CSDGBase*,CSDGBase*> &TTRACE);
	CSDGBase *GetSDGStruct(CSDGBase *pHead); 
	int     GetStructMatch(CSDGBase *pMSH);
	int     GetStructValue(const int depth,CSDGBase *pHead);


	// complete match
	int     CompletelyMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	
	void    CompleteMatchNode(CSDGBase *pSN,CSDGBase *pMN,
					CSDGBase *pSHead,CSDGBase *pMHead,
					CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	int     GetCompleteValue(CSDGBase *pMHead);
	CSDGBase *FindCorresSNode(const int ID,CSDGBase *pSH);
    int     OperatorKind(TNODE T);
	int     BoolExpressionMatch(ENODE *pSE,ENODE *pTE,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	int     ArithExpressionMatch(ENODE *pSE,ENODE *pTE,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	int     BoolPartMatch(const int ftype,ENODE *pSE,ENODE *pTN,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	int     ArithPartMatch(const int ftype,ENODE *pSE,ENODE *pTN,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	int     FunctionMatch(ENODE *pSF,ENODE *pTF,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	int     GetFatherType(ENODE *pEN,ENODE *pEH);
	bool    IsSameVariable(TNODE &TS,TNODE &TM,CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);

	
	// branch match
	int     BranchesMatch(CSDGBase *pSHead,CSDGBase *pMHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry,
					CArray<FNODE,FNODE> &SFArry,CArray<FNODE,FNODE> &MFArry);
	int     GetBranchValue(int ncount,CSDGBase *pSHead);
	int     NodeMatch(int &count,CSDGBase *pNode,CSDGBase *pMHead,CSDGBase *pSHead,
		            CArray<SNODE,SNODE> &SSArry,CArray<SNODE,SNODE> &MSArry);
	bool    NodeInfoMatch(CSDGBase *pSN,CSDGBase *pTN);


	// reset node value
	void    ResetSValue(CSDGBase *pSHead);

	int     NodeWeight(CSDGBase *pNode);
	CString GetExpStringEx(ENODE *pEHead,CArray<SNODE,SNODE> &SArry,bool bNIDX=true);
	CString GetENodeStr(ENODE *pENode);
	int     IsTerminal(ENODE *pENode);

};

#endif // !defined(AFX_PROGRAMMATCH_H__8FE4CFEC_AE23_460B_83AD_0E5B7635846C__INCLUDED_)
